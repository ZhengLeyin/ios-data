//
//  ChangeNameViewController.h
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeNameViewController : UIViewController

@property (nonatomic, copy) NSString *name;

@end
