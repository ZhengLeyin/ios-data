//
//  ShortVideoOfExcellentCourseViewController.h
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShortVideoOfExcellentCourseViewController : UIViewController
/** type */
@property (nonatomic, assign) NSUInteger  type;
/** model */
@property (nonatomic, strong) CourseModel *coursemodel;
/** 跳转 */
@property (nonatomic, copy) void (^PushBlock)(UIViewController *vc);

@end

NS_ASSUME_NONNULL_END
