//
//  ParentTopicCell.h
//  mingyu
//
//  Created by apple on 2018/4/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TopicInfoModel;

typedef void(^MoreButtonBlock)(void);

@interface ParentTopicCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *user_head_Image;
@property (weak, nonatomic) IBOutlet UILabel *user_name_Lab;
@property (weak, nonatomic) IBOutlet UILabel *follow_number_Lab;
@property (weak, nonatomic) IBOutlet UIButton *follow_Button;

@property (weak, nonatomic) IBOutlet UILabel *TopicTitleLab;


@property (weak, nonatomic) IBOutlet UIButton *topButton;
@property (weak, nonatomic) IBOutlet UILabel *topic_time_Lab;
@property (weak, nonatomic) IBOutlet UIButton *collect_Button;
@property (weak, nonatomic) IBOutlet UIButton *praise_button;


//@property (nonatomic, strong) TopicInfoModel *topicinfomodel;
//
//@property (nonatomic, copy) MoreButtonBlock morebuttonblock;





+ (instancetype)theUserinfoCell;

+ (instancetype)theTopicHeaderCell;

+ (instancetype)theCollertAndPraisCell;

+ (instancetype)theAboutTopicCell;

- (void)setuserinfocellWithModel:(TopicInfoModel *)topicinfomodel;




@end
