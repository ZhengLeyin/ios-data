//
//  VideoPlayerViewController.m
//  mingyu
//
//  Created by apple on 2018/3/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import "CLPlayerView.h"
#import "UINavigationController+NavAlpha.h"
//#import "VideoDetailViewController.h"
//#import "VideoContentViewController.h"
//#import "VideoInteractViewController.h"
//#import "VideoBaseViewController.h"
#import "VideoHeaderView.h"

#define KPlayerViewH 210
#define kHeaderViewH 118
#define kPageMenuH 40
#define kNaviH (isIPhoneX ? 84 : 64)

#define isIPhoneX kScreenHeight==812

@interface VideoPlayerViewController ()<SPPageMenuDelegate,UIScrollViewDelegate>

@property (nonatomic,weak) CLPlayerView *playerView;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) VideoHeaderView *headerView;
@property (nonatomic, strong) SPPageMenu *pageMenu;

@property (nonatomic, assign) CGFloat lastPageMenuY;

@property (nonatomic, assign) CGPoint lastPoint;

@end

@implementation VideoPlayerViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lastPageMenuY = kHeaderViewH;
    
    
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, KPlayerViewH)];
    _playerView = playerView;
    [self.view addSubview:_playerView];
//    _playerView.autoRotate = NO;
    _playerView.videoFillMode = VideoFillModeResizeAspect;
    _playerView.topToolBarHiddenType = TopToolBarHiddenSmall;
    _playerView.fullStatusBarHiddenType = FullStatusBarHiddenFollowToolBar;
    _playerView.strokeColor = [UIColor whiteColor];
    _playerView.progressPlayFinishColor = [UIColor colorWithHexString:@"50D0F4"];
    _playerView.videoTitleString = @"哼好看的视屏";

    //保存路径需要转换为url路径，才能播放
    NSURL *url = [NSURL fileURLWithPath:self.playerItem.savePath];
    _playerView.url = url;
    [_playerView playVideo];
    
    [_playerView backButton:^(UIButton *button) {
        ZPLog(@"返回");
    }];

    [_playerView endPlay:^{
        ZPLog(@"end");
    }];
    
//    // 添加一个全屏的scrollView
//    [self.view addSubview:self.scrollView];
//
//    // 添加头部视图
//    [self.view addSubview:self.headerView];
//
//    // 添加悬浮菜单
//    [self.view addSubview:self.pageMenu];
//
//    [self.view bringSubviewToFront:_playerView];
//
////     添加3个子控制器
//        VideoDetailViewController *VC1 = [[VideoDetailViewController alloc] init];
//        VC1.block = ^(NSInteger index) {
//            ZPLog(@"详情介绍第%zd行",index);
//        };
//        VideoContentViewController *VC2 = [[VideoContentViewController alloc] init];
//        VC2.block = ^(NSInteger index) {
//            ZPLog(@"内容目录第%zd行",index);
//        };
//        VideoInteractViewController *VC3 = [[VideoInteractViewController alloc] init];
//        VC3.block = ^(NSInteger index) {
//            ZPLog(@"课程互动第%zd行",index);
//        };
//
//        [self addChildViewController:VC1];
//        [self addChildViewController:VC2];
//        [self addChildViewController:VC3];
//
//
//        // 先将第一个子控制的view添加到scrollView上去
//        [self.scrollView addSubview:self.childViewControllers[0].view];
//
//        // 监听子控制器发出的通知
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subScrollViewDidScroll:) name:VideoPlayerViewDidScrollNSNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshState:) name:VideoPlayerViewRefreshStateNSNotification object:nil];
//
}
//
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//
//    if (@available(iOS 11.0, *)) {
//        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        // Fallback on earlier versions
//        self.automaticallyAdjustsScrollViewInsets = NO;
//    }
//
//    self.navBarTintColor = [UIColor whiteColor];
//    self.navAlpha = 0;
//    self.navTitleColor = [UIColor clearColor];
//
//}
//
//
//#pragma mark - 通知
//// 子控制器上的scrollView已经滑动的代理方法所发出的通知(核心)
//- (void)subScrollViewDidScroll:(NSNotification *)noti {
//
//    // 取出当前正在滑动的tableView
//    UIScrollView *scrollingScrollView = noti.userInfo[@"scrollingScrollView"];
//    CGFloat offsetDifference = [noti.userInfo[@"offsetDifference"] floatValue];
//
//    CGFloat distanceY;
//
//    // 取出的scrollingScrollView并非是唯一的，当有多个子控制器上的scrollView同时滑动时都会发出通知来到这个方法，所以要过滤
//    VideoBaseViewController *baseVc = self.childViewControllers[self.pageMenu.selectedItemIndex];
//
//    if (scrollingScrollView == baseVc.scrollView && baseVc.isFirstViewLoaded == NO) {
//
//        // 让悬浮菜单跟随scrollView滑动
//        CGRect pageMenuFrame = self.pageMenu.frame;
//
//        if (pageMenuFrame.origin.y >= KPlayerViewH) {
//            // 往上移
//            if (offsetDifference > 0 && scrollingScrollView.contentOffset.y+kScrollViewBeginTopInset > 0) {
//
//                if (((scrollingScrollView.contentOffset.y+kScrollViewBeginTopInset+self.pageMenu.frame.origin.y)>=kHeaderViewH) || scrollingScrollView.contentOffset.y+kScrollViewBeginTopInset < 0) {
//                    // 悬浮菜单的y值等于当前正在滑动且显示在屏幕范围内的的scrollView的contentOffset.y的改变量(这是最难的点)
//                    pageMenuFrame.origin.y += -offsetDifference;
//                    if (pageMenuFrame.origin.y <= KPlayerViewH) {
//                        pageMenuFrame.origin.y = KPlayerViewH;
//                    }
//                }
//            } else { // 往下移
//
//                if ((scrollingScrollView.contentOffset.y+kScrollViewBeginTopInset+self.pageMenu.frame.origin.y)-KPlayerViewH<kHeaderViewH) {
//                    pageMenuFrame.origin.y = -scrollingScrollView.contentOffset.y-kScrollViewBeginTopInset+kHeaderViewH+KPlayerViewH;
//
//                    ZPLog(@"pageMenuFrame.origin.y --- %f" ,pageMenuFrame.origin.y);
//
//                    if (pageMenuFrame.origin.y >= kHeaderViewH+KPlayerViewH) {
//                        pageMenuFrame.origin.y = kHeaderViewH+KPlayerViewH;
//                    }
//                }
//            }
//        }
//        self.pageMenu.frame = pageMenuFrame;
//
//        CGRect headerFrame = self.headerView.frame;
//        headerFrame.origin.y = self.pageMenu.frame.origin.y-kHeaderViewH;
//        self.headerView.frame = headerFrame;
//
//        // 记录悬浮菜单的y值改变量
//        distanceY = pageMenuFrame.origin.y - self.lastPageMenuY;
//        self.lastPageMenuY = self.pageMenu.frame.origin.y;
//
//        // 让其余控制器的scrollView跟随当前正在滑动的scrollView滑动
//        [self followScrollingScrollView:scrollingScrollView distanceY:distanceY];
//    }
//    baseVc.isFirstViewLoaded = NO;
//}
//
//- (void)followScrollingScrollView:(UIScrollView *)scrollingScrollView distanceY:(CGFloat)distanceY{
//    VideoBaseViewController *baseVc = nil;
//    for (int i = 0; i < self.childViewControllers.count; i++) {
//        baseVc = self.childViewControllers[i];
//        if (baseVc.scrollView == scrollingScrollView) {
//            continue;
//        } else {
//            CGPoint contentOffSet = baseVc.scrollView.contentOffset;
//            contentOffSet.y += -distanceY;
//            baseVc.scrollView.contentOffset = contentOffSet;
//        }
//    }
//}
//
//- (void)refreshState:(NSNotification *)noti {
//    BOOL state = [noti.userInfo[@"isRefreshing"] boolValue];
//    // 正在刷新时禁止self.scrollView滑动
//    self.scrollView.scrollEnabled = !state;
//}
//
//
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//
//    VideoBaseViewController *baseVc = self.childViewControllers[self.pageMenu.selectedItemIndex];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if (baseVc.scrollView.contentSize.height < kScreenHeight && [baseVc isViewLoaded]) {
//            [baseVc.scrollView setContentOffset:CGPointMake(0, -kScrollViewBeginTopInset) animated:YES];
//        }
//    });
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    VideoBaseViewController *baseVc = self.childViewControllers[self.pageMenu.selectedItemIndex];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if (baseVc.scrollView.contentSize.height < kScreenHeight && [baseVc isViewLoaded]) {
//            [baseVc.scrollView setContentOffset:CGPointMake(0, -kScrollViewBeginTopInset) animated:YES];
//        }
//    });
//}
//
//
//#pragma mark - SPPageMenuDelegate
//- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
//    if (!self.childViewControllers.count) { return;}
//    // 如果上一次点击的button下标与当前点击的buton下标之差大于等于2,说明跨界面移动了,此时不动画.
//    if (labs(toIndex - fromIndex) >= 2) {
//        [self.scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * toIndex, 0) animated:NO];
//    } else {
//        [self.scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * toIndex, 0) animated:YES];
//    }
//
//    VideoBaseViewController *targetViewController = self.childViewControllers[toIndex];
//    // 如果已经加载过，就不再加载
//    if ([targetViewController isViewLoaded]) return;
//
//    // 来到这里必然是第一次加载控制器的view，这个属性是为了防止下面的偏移量的改变导致走scrollViewDidScroll
//    targetViewController.isFirstViewLoaded = YES;
//
//    targetViewController.view.frame = CGRectMake(kScreenWidth*toIndex, 0, kScreenWidth, kScreenHeight);
//    UIScrollView *s = targetViewController.scrollView;
//    CGPoint contentOffset = s.contentOffset;
//    contentOffset.y = -self.headerView.frame.origin.y-kScrollViewBeginTopInset;
//    if (contentOffset.y + kScrollViewBeginTopInset >= kHeaderViewH) {
//        contentOffset.y = kHeaderViewH-kScrollViewBeginTopInset;
//    }
//    s.contentOffset = contentOffset;
//    [self.scrollView addSubview:targetViewController.view];
//}
//
//
//- (UIScrollView *)scrollView {
//
//    if (!_scrollView) {
//        _scrollView = [[UIScrollView alloc] init];
//        _scrollView.frame = CGRectMake(0, KPlayerViewH, kScreenWidth, kScreenHeight-bottomMargin-KPlayerViewH);
//        _scrollView.delegate = self;
//        _scrollView.pagingEnabled = YES;
//        _scrollView.showsVerticalScrollIndicator = NO;
//        _scrollView.showsHorizontalScrollIndicator = NO;
//        _scrollView.contentSize = CGSizeMake(kScreenWidth*3, 0);
//        _scrollView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
//    }
//    return _scrollView;
//}
//
//- (VideoHeaderView *)headerView {
//    if (!_headerView) {
//        NSArray  *apparray= [[NSBundle mainBundle]loadNibNamed:@"VideoHeaderView" owner:nil options:nil];
//        _headerView = [apparray firstObject];
//        _headerView.frame = CGRectMake(0, KPlayerViewH, kScreenWidth, kHeaderViewH);
//        _headerView.backgroundColor = [UIColor greenColor];
//
//    }
//    return _headerView;
//}
//
//- (SPPageMenu *)pageMenu {
//
//    if (!_pageMenu) {
//        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenWidth, kPageMenuH) trackerStyle:SPPageMenuTrackerStyleLineLongerThanItem];
//        [_pageMenu setItems:@[@"详情介绍",@"课程内容",@"课程互动"] selectedItemIndex:0];
//        _pageMenu.delegate = self;
//        _pageMenu.itemTitleFont = [UIFont systemFontOfSize:16];
//        _pageMenu.selectedItemTitleColor = [UIColor blackColor];
//        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithWhite:0 alpha:0.6];
//        _pageMenu.tracker.backgroundColor = [UIColor orangeColor];
//        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
//        _pageMenu.bridgeScrollView = self.scrollView;
//
//    }
//    return _pageMenu;
//}


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_playerView destroyPlayer];
}

@end
