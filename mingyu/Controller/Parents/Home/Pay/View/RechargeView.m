//
//  RechargeView.m
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "RechargeView.h"

@implementation RechargeView

-(NSMutableArray *)modelArray {
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

-(void)array:(NSMutableArray *)array type:(NSUInteger)type {
  //  [self.modelArray addObject:model];
    //ZPLog(@"id_shop=%@",model.ios_shop_id);
    _rechargeButton.userInteractionEnabled = YES;
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MingYuRechargeModel *model = array[idx];
        
        if (idx == 0) {
            _moneyLab1.text = [NSString stringWithFormat:@"%ld元",model.money_num];
            _yubiLab1.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view2.layer.borderWidth = _view3.layer.borderWidth =  _view4.layer.borderWidth = _view5.layer.borderWidth = _view6.layer.borderWidth = 0;
                _view2.layer.borderColor = _view3.layer.borderColor =  _view4.layer.borderColor = _view5.layer.borderColor = _view6.layer.borderColor = [UIColor clearColor].CGColor;
                _view1.layer.borderWidth = 2;
                _view1.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
                
            }];
            [_view1 addGestureRecognizer:tap];
           
        }else if (idx == 1) {
             _moneyLab2.text = [NSString stringWithFormat:@"%ld元",model.money_num];
             _yubiLab2.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view1.layer.borderWidth = _view3.layer.borderWidth =  _view4.layer.borderWidth = _view5.layer.borderWidth = _view6.layer.borderWidth = 0;
                _view1.layer.borderColor = _view3.layer.borderColor =  _view4.layer.borderColor = _view5.layer.borderColor = _view6.layer.borderColor = [UIColor clearColor].CGColor;
                _view2.layer.borderWidth = 2;
                _view2.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
            }];
            [_view2 addGestureRecognizer:tap];

        } else if (idx == 2) {
            _moneyLab3.text = [NSString stringWithFormat:@"%ld元",model.money_num];
            _yubiLab3.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view2.layer.borderWidth = _view1.layer.borderWidth =  _view4.layer.borderWidth = _view5.layer.borderWidth = _view6.layer.borderWidth = 0;
                _view2.layer.borderColor = _view1.layer.borderColor =  _view4.layer.borderColor = _view5.layer.borderColor = _view6.layer.borderColor = [UIColor clearColor].CGColor;
                _view3.layer.borderWidth = 2;
                _view3.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
            }];
            [_view3 addGestureRecognizer:tap];

        } else if (idx == 3) {
            
            _moneyLab4.text = [NSString stringWithFormat:@"%ld元",model.money_num];
            _yubiLab4.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];;
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view2.layer.borderWidth = _view3.layer.borderWidth =  _view1.layer.borderWidth = _view5.layer.borderWidth = _view6.layer.borderWidth = 0;
                _view2.layer.borderColor = _view3.layer.borderColor =  _view1.layer.borderColor = _view5.layer.borderColor = _view6.layer.borderColor = [UIColor clearColor].CGColor;
                _view4.layer.borderWidth = 2;
                _view4.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
            }];
            [_view4 addGestureRecognizer:tap];

        } else if (idx == 4) {
            _moneyLab5.text = [NSString stringWithFormat:@"%ld元",model.money_num];
            _yubiLab5.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view2.layer.borderWidth = _view3.layer.borderWidth =  _view4.layer.borderWidth = _view1.layer.borderWidth = _view6.layer.borderWidth = 0;
                _view2.layer.borderColor = _view3.layer.borderColor =  _view4.layer.borderColor = _view1.layer.borderColor = _view6.layer.borderColor = [UIColor clearColor].CGColor;
                _view5.layer.borderWidth = 2;
                _view5.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
            }];
            [_view5 addGestureRecognizer:tap];

        }else {
            _moneyLab6.text = [NSString stringWithFormat:@"%ld元",model.money_num];
            _yubiLab6.text = [NSString stringWithFormat:@"%ld个育币",model.money_virtual];
            /* 添加一个轻拍手势 */
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
            [[tap rac_gestureSignal] subscribeNext:^(id x) {
                _view2.layer.borderWidth = _view3.layer.borderWidth =  _view4.layer.borderWidth = _view5.layer.borderWidth = _view1.layer.borderWidth = 0;
                _view2.layer.borderColor = _view3.layer.borderColor =  _view4.layer.borderColor = _view5.layer.borderColor = _view1.layer.borderColor = [UIColor clearColor].CGColor;
                _view6.layer.borderWidth = 2;
                _view6.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
                self.returnValueBlock(model);
            }];
            [_view6 addGestureRecognizer:tap];

        }
    }];
}


- (void)drawRect:(CGRect)rect {
    if (kScreenWidth == 320) {
        _titleLab.font = FontSize(14);
        _topLab.font = FontSize(9);
        _constraint1.constant = _constraint2.constant = _constraint3.constant = _constraint4.constant = _constraint5.constant = _constraint6.constant = 6;
    }else {
        
    }
}

@end
