//
//  SearchVideoViewController.h
//  mingyu
//
//  Created by apple on 2018/11/15.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchVideoViewController : UIViewController

@property (nonatomic, copy) NSString *searchStr;

@end

NS_ASSUME_NONNULL_END
