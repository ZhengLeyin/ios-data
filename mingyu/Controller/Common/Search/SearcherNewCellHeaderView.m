//
//  SearcherNewCellHeaderView.m
//  mingyu
//
//  Created by MingYu on 2018/11/12.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "SearcherNewCellHeaderView.h"

@implementation SearcherNewCellHeaderView

+ (instancetype)theSearchNewCellHeaderView {
    return [[NSBundle mainBundle] loadNibNamed:@"SearcherNewCellHeaderView" owner:nil options:nil][0];
}

+ (instancetype)theSearchNewCellFooterView {
    return [[NSBundle mainBundle] loadNibNamed:@"SearcherNewCellHeaderView" owner:nil options:nil][1];
}




- (void)theSearchNewCellHeaderViewSetBaseUI:(NSInteger)section {
    _searchTitleBEL.textColor =  [UIColor colorWithHexString:@"A8A8A8"];
    if (section == 0) {
        [_historyBtn setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
        _historyBtn.hidden = NO;
        _searchTitleBEL.text = @"最近搜索";
        [_historyBtn addTarget:self action:@selector(historyBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }else {
        _searchTitleBEL.text = @"热门搜索";
        _historyBtn.hidden = YES;
    }
}

/** 清空历史Btn */
- (void)historyBtnAction:(UIButton *)sender {
    if (self.tabAction) {
        self.tabAction();
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
