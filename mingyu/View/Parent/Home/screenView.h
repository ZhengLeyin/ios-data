//
//  screenView.h
//  mingyu
//
//  Created by apple on 2018/5/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface screenView : UIView


@property (nonatomic, copy) void (^selfhidden)(void);

@property (nonatomic, copy) void (^finishBlock)(NSInteger section0,NSInteger section1,NSInteger section2);

/**
 初始化
 shareDic  分享内容
 */
- (instancetype)initWithfiletviewFrame:(CGRect)frame;



/**
 显示
 */
- (void)show;



@end
