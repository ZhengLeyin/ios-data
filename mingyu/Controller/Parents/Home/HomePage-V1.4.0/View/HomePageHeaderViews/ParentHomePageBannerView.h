//
//  ParentHomePageBannerView.h
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParentHomePageBannerView : UIView<SDCycleScrollViewDelegate>
+ (instancetype)TheParentHomePageBannerView;
@property (nonatomic, strong)UIView *bannerView;
- (void)getAdvertisement;
@end

NS_ASSUME_NONNULL_END
