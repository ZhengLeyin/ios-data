//
//  SystemMessageCell.h
//  mingyu
//
//  Created by apple on 2018/9/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SystemMessageModel;

@interface SystemMessageCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *timeLab;

//@property (weak, nonatomic) IBOutlet UILabel *sub_messageLab;
@property (weak, nonatomic) IBOutlet UILabel *sub_messageLab;

@property (weak, nonatomic) IBOutlet UILabel *messageLab;

@property (nonatomic, strong) SystemMessageModel *messageModel;



+ (instancetype)theSystemMessageCell1WithTableView:(UITableView *)tableview;

+ (instancetype)theSystemMessageCell2WithTableView:(UITableView *)tableview;

+ (instancetype)theSystemMessageCell3WithTableView:(UITableView *)tableview;

+ (CGFloat)cellHeight;


@end
