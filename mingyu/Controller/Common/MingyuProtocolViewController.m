//
//  MingyuProtocolViewController.m
//  mingyu
//
//  Created by apple on 2018/9/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MingyuProtocolViewController.h"
#import <WebKit/WebKit.h>


@interface MingyuProtocolViewController ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webview;


@end

@implementation MingyuProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupNavigation]
    ;
    _webview = [[WKWebView alloc] initWithFrame:CGRectMake(0,0,kScreenWidth,kScreenHeight-NaviH)];
    _webview.backgroundColor = [UIColor whiteColor];
    _webview.UIDelegate = self;
    _webview.navigationDelegate = self;

    //2.创建URL
    NSString *URL = [NSString stringWithFormat:@"%@#/agreement",KURL];
    //3.创建Request
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL]];
//    4.加载Request
    [_webview loadRequest:request];
    //5.添加到视图
    [self.view addSubview:_webview];
}

//  页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    // 禁止放大缩小
    NSString *injectionJSString = @"var script = document.createElement('meta');"
    "script.name = 'viewport';"
    "script.content=\"width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\";"
    "document.getElementsByTagName('head')[0].appendChild(script);";
    [webView evaluateJavaScript:injectionJSString completionHandler:nil];
}


- (void)setupNavigation{
    self.title = @"名育协议";
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 60, 40);
    [button setImage:ImageName(@"back_black") forState:0];
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftItem;
}


- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
