//
//  SearchViewController.m
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchView.h"
#import "SearchSuggestionViewController.h"
#import "SearchResultViewController.h"
/**新的搜索界面的格式Cell*/
#import "SearchNewCell.h"
#import "SearcherNewCellHeaderView.h"
@interface SearchViewController ()<UISearchBarDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,strong) UISearchBar *searchBar;
//@property (nonatomic,strong) SearchView *searchView;
@property (nonatomic,strong) NSMutableArray *hotArray;
@property (nonatomic,strong) NSMutableArray *historyArray;
@property (nonatomic,strong) SearchSuggestionViewController *searchSuggestVC;
@property (nonatomic, strong) SearchResultViewController *SearchResultVC;
@property (nonatomic, copy) NSString *historySearchPath;

@end

@implementation SearchViewController

- (NSMutableArray *)hotArray{
    if (!_hotArray) {
        _hotArray = [NSMutableArray array];
    }
    return _hotArray;
}
    
- (NSMutableArray *)historyArray{
    if (!_historyArray) {
        _historyArray = [NSKeyedUnarchiver unarchiveObjectWithFile:self.historySearchPath];
        if (!_historyArray) {
            self.historyArray = [NSMutableArray array];
        }
    }
    return _historyArray;
}

- (NSString *)historySearchPath{
    if (!_historySearchPath) {
        if (_searchType == 1) {
            _historySearchPath = KHistorySearchPath;
        } else if (_searchType == 2){
            _historySearchPath = KCourseHistorySearchPath;
        }
    }
    return _historySearchPath;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH) style: UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] init];
    }
    return _tableView;
}

//- (SearchView *)searchView{
//    if (!_searchView) {
//        self.searchView = [[SearchView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) hotArray:self.hotArray historyArray:self.historyArray];
//        __weak SearchViewController *weakSelf = self;
//        _searchView.tabAction = ^(NSString *str) {
//            [weakSelf pushToSearchResultWithSearchStr:str];
//        };
//    }
//    return _searchView;
//}

- (SearchSuggestionViewController *)searchSuggestVC{
    if (!_searchSuggestVC) {
        self.searchSuggestVC = [[SearchSuggestionViewController alloc] init];
        _searchSuggestVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        _searchSuggestVC.view.hidden = YES;
        __weak SearchViewController *weakSelf = self;
        _searchSuggestVC.searchBlock = ^(NSString *searchTest) {
            [weakSelf pushToSearchResultWithSearchStr:searchTest];
        };
    }
    return _searchSuggestVC;
}

- (SearchResultViewController *)SearchResultVC{
    if (!_SearchResultVC) {
        _SearchResultVC = [[SearchResultViewController alloc] init];
        _SearchResultVC.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        //        _SearchResultVC.view.hidden = YES;
        [self.view addSubview:_SearchResultVC.view];
        [self addChildViewController:_SearchResultVC];
    }
    return _SearchResultVC;
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //    if (!_searchBar.isFirstResponder) {
    //        [self.searchBar becomeFirstResponder];
    //    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
    [self.searchBar resignFirstResponder];
    _searchSuggestVC.view.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [MobClick event:@"SearchViewController"];  //友盟统计
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self setBarButtonItem];
    self.view.backgroundColor = [UIColor whiteColor];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.enableAutoToolbar = YES;
    
    [self setBaseUI];
    
    NSLog(@"%@----%ld",self.historyArray,self.historyArray.count);
    [self getHotLabelList];
}

-(void)setBaseUI {
    [self.view addSubview:self.tableView];
}

- (void)getHotLabelList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetHotLabelList];
    NSDictionary *parameters = @{
                                 @"start":@"0",
                                 @"size":@"20",
                                 @"search_type":@(_searchType)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            self.hotArray = json[@"data"];
#pragma mark 弃用原来view 新用cell代替View
            //[self.view addSubview:self.searchView];
            [self.view addSubview:self.searchSuggestVC.view];
            [self addChildViewController:_searchSuggestVC];
            
        }
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

- (void)setBarButtonItem{
    
    [self.navigationItem setHidesBackButton:YES];
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0,0,kScreenWidth-100,30)];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 100, 30)];
    self.searchBar.placeholder = @"请输入搜索内容";
    self.searchBar.layer.cornerRadius = 15;
    self.searchBar.layer.masksToBounds = YES;
    //设置背景色
    if (@available(iOS 11.0, *)) {
        self.searchBar.backgroundColor = [UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1];
    } else {
        [self.searchBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1]]];
    }
    self.searchBar.delegate=self;
    self.searchBar.tintColor = [UIColor colorWithHexString:@"50D0F4"];  //光标颜色
    
    //一下代码为修改placeholder字体的颜色和大小
    UITextField *searchField = [_searchBar valueForKey:@"_searchField"];
    //2. 设置圆角和边框颜色
    if(searchField) {
        searchField.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        searchField.font = FontSize(15);
        [searchField setBackgroundColor:[UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1]];
    }
    //只有编辑时出现出现那个叉叉
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [titleView addSubview:self.searchBar];
    self.navigationItem.titleView = titleView;
    self.navigationItem.leftBarButtonItem = nil;
    UIButton *cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancleBtn.frame =CGRectMake(0,0, 60, 44);
    [cancleBtn setTitle:@"取消" forState:0];
    [cancleBtn setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:0];
    [cancleBtn addTarget:self action:@selector(searchBarCancelButtonClicked:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *rightbarBut = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
    self.navigationItem.rightBarButtonItem = rightbarBut;
    
    //    [self.searchBar becomeFirstResponder];
}

#pragma mark tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_historyArray.count > 10) {
            return 10;
        }
        return _historyArray.count;
    } else{
        //return _hotArray.count;
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchNewCell *commentcell = [SearchNewCell theSearchNewCellWithTableView:tableView];
    if (indexPath.section == 0) {
        commentcell.searchContentBEL.text = _historyArray[indexPath.row];
        /**删除单个历史搜索cell*/
        __weak SearchViewController *weakSelf = self;
        [[commentcell.deleteBtn rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
            [weakSelf.historyArray removeObjectAtIndex:indexPath.row];
            [NSKeyedArchiver archiveRootObject:weakSelf.historyArray toFile:self.historySearchPath];
            [weakSelf.tableView reloadData];
        }];
    }else {
        //commentcell.searchContentBEL.text = _hotArray[indexPath.row];
    }
    return commentcell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SearcherNewCellHeaderView *view = [SearcherNewCellHeaderView theSearchNewCellHeaderView];
    view.backgroundColor = [UIColor whiteColor];
    if (section == 0) {
        __weak SearchViewController *weakSelf = self;
        if (self.historyArray &&self.historyArray.count > 0) {
            [view theSearchNewCellHeaderViewSetBaseUI:section];
            view.tabAction = ^{
                NSLog(@"haha1%@",weakSelf.historyArray);
                [weakSelf.historyArray removeAllObjects];
                [NSKeyedArchiver archiveRootObject:weakSelf.historyArray toFile:self.historySearchPath];
                [weakSelf.tableView reloadData];
            };
        }else {
            UIView *view = [UIView new];
            return view;
        }
    }else {
        if (self.hotArray && self.hotArray.count > 0) {
            view.userInteractionEnabled = NO;
            [view theSearchNewCellHeaderViewSetBaseUI:section];
        }else {
            UIView *view = [UIView new];
            return view;
        }
    }
    return view;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        SearcherNewCellHeaderView *view = [SearcherNewCellHeaderView theSearchNewCellFooterView];
        UIView *hotView = [UIView new];
       // hotView.backgroundColor = [UIColor yellowColor];
        [view addSubview:hotView];
        [hotView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.edges.equalTo(view).with.insets(UIEdgeInsetsMake(0, kScale(10), 0, kScale(10)));
        }];
        view.backgroundColor = [UIColor whiteColor];
        if (_hotArray && _hotArray.count > 0) {
            [self setButton:hotView];
        }
        ZPLog(@"count ===%ld",_hotArray.count);
        return view;
    } else {
        UIView *view = [UIView new];
        return view;
    }
}

-(void)setButton:(UIView *)view{
    NSInteger totalLoc = 3;
    CGFloat W =  (kScreenWidth - kScale(40)) / 3;
    CGFloat H = kScale(28);
    CGFloat margin= kScale(5);
    for (NSInteger i = 0; i < self.hotArray.count; i++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:self.hotArray[i] forState:UIControlStateNormal];
        [btn setTitleColor:KColor(44,44,44) forState:(UIControlStateNormal)];
        btn.backgroundColor = KColor(247,248,250);
        btn.titleLabel.font = [UIFont systemFontOfSize: kScale(12.0)];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 14;
        /*计算frame*/
        NSInteger row = i / totalLoc;//行号
        NSInteger loc = i % totalLoc;//列号
        CGFloat X= margin + (margin + W) * loc;
        CGFloat Y= margin + (margin + H) * row;
        btn.frame = CGRectMake(X, Y, W, H);
        //设置tag值
        btn.tag = i;
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
    }
}

-(void)clickBtn:(UIButton *)btn{
    
   // NSString *stringInt = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    ZPLog(@"%ld",(long)btn.tag);
    /** 热门搜索 */
    [self pushToSearchResultWithSearchStr:_hotArray[btn.tag]];
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak SearchViewController *weakSelf = self;
    if (indexPath.section == 0) {
        /** 最近搜索 */
        [weakSelf pushToSearchResultWithSearchStr:_historyArray[indexPath.row]];
    }else {
       
    }
    
}

#pragma mark 活动关闭键盘
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kScale(40);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (self.historyArray && self.historyArray.count > 0) {
            return kScale(30);
        }else {
            return 0;
        }
    }else {
        if (self.hotArray && self.hotArray.count > 0) {
            return kScale(30);
        }else {
            return 0;
        }
    }
    
}

#pragma mark  FootView 5:UIBtton 左右上下边距 28高度  15:最顶层UIButton 距离View 高度
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else {
        if (self.hotArray && self.hotArray.count > 0) {
            NSUInteger row = self.hotArray.count / 3;
            NSUInteger remainder = self.hotArray.count % 3;
            if (remainder == 0) {
                return row * (kScale(28+5)) + kScale(15);
            }else {
                return (row+1) * (kScale(28+5)) + kScale(15);
            }
        }else {
            return 0;
        }
    }
}

- (void)pushToSearchResultWithSearchStr:(NSString *)str{
    [self.searchBar resignFirstResponder];
    self.searchBar.text = str;
    
    [self setHistoryArrWithStr:str];
    self.SearchResultVC.searchType = _searchType;
    self.SearchResultVC.searchStr = str;
    [self.view bringSubviewToFront:self.SearchResultVC.view];
}

- (void)setHistoryArrWithStr:(NSString *)str {
    for (int i = 0; i < _historyArray.count; i++) {
        if ([_historyArray[i] isEqualToString:str]) {
            [_historyArray removeObjectAtIndex:i];
            break;
        }
    }
    [_historyArray insertObject:str atIndex:0];
    [NSKeyedArchiver archiveRootObject:_historyArray toFile:self.historySearchPath];
}

#pragma mark - UISearchBarDelegate -
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self pushToSearchResultWithSearchStr:searchBar.text];
    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    //    searchBar.showsCancelButton = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 255) {
        searchBar.text = [searchText substringWithRange:NSMakeRange(0, 255)];
    }
    [self.SearchResultVC removeFromParentViewController];
    self.SearchResultVC = nil;
    if (searchBar.text == nil || [searchBar.text length] <= 0) {
        _searchSuggestVC.view.hidden = YES;
        [self.view bringSubviewToFront:self.tableView];
        [self.tableView reloadData];

    } else {
        _searchSuggestVC.view.hidden = NO;
        [self.view bringSubviewToFront:_searchSuggestVC.view];
        [_searchSuggestVC.searchArray removeAllObjects];
        _searchSuggestVC.lastPage = NO;
        _searchSuggestVC.start = 0;
        [_searchSuggestVC searchTestChangeWithTest:searchBar.text];
    }
}

@end
