//
//  BookTypeViewController.m
//  mingyu
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BookTypeViewController.h"

@interface BookTypeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *BookList;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;


@end

@implementation BookTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    [self setupNavigationItem];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    
    [self getListenBookList];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.tableView reloadData];

}

- (void)getListenBookList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetListenBookList];
    NSDictionary *parameters = @{
                                 @"book_type":_booktype,
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
//            NSArray *bookMessageList = data[@"bookMessageList"];
            for (NSDictionary *dic in data) {
                BookListModel *model = [BookListModel modelWithJSON:dic];
                [self.BookList addObject:model];
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.BookList.count && self.BookList.count > 0) {
        return _BookList.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListenStoryCell *cell = [ListenStoryCell theCellWithTableView:tableView];
    if (self.BookList && self.BookList.count > indexPath.row) {
        BookListModel *model = [self.BookList objectAtIndex:indexPath.row];
        cell.booklistmodel = model;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.BookList && self.BookList.count > indexPath.row) {
        BookListModel *model = [self.BookList objectAtIndex:indexPath.row];
        ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
        VC.target_id = model.book_id;
        VC.audio_type = 2;
        [self.navigationController pushViewController:VC animated:YES];
    }
}




- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 120;
    }
    return _tableView;
}


- (NSMutableArray *)BookList{
    if (_BookList == nil) {
        _BookList = [NSMutableArray array];
    }
    return _BookList;
}


- (void)setupNavigationItem{
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame =CGRectMake(0,0, 60, 44);
    [but setImage:ImageName(@"back_black") forState:UIControlStateNormal];
    [but addTarget:self action:@selector(Back)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut = [[UIBarButtonItem alloc]initWithCustomView:but];
    self.navigationItem.leftBarButtonItem = barBut;
    
}


- (void)Back{
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
