//
//  LSDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LSDetailViewController.h"

@interface LSDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIWebView *webView1;

@property (nonatomic, strong) UIWebView *webView2;


@end

@implementation LSDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [self setBookmodel:_bookmodel];
}

- (void)setBookmodel:(BookListModel *)bookmodel{
    _bookmodel = bookmodel;
    [self.webView1 loadHTMLString:_bookmodel.content_abstract baseURL:nil];
    [self.webView2 loadHTMLString:_bookmodel.author_abstract baseURL:nil];
    
    [self.tableView reloadData];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.webView1.height = self.webView1.scrollView.contentSize.height;
    self.webView2.height = self.webView2.scrollView.contentSize.height;
    
    [self.tableView reloadData];
    
}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return 1;
    }
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lab = [[UILabel alloc] init];
    lab.font = FontSize(14);
    lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
    if (section == 0){
        lab.text = @"    内容简介";
        return lab;
    } else if (section == 1){
        lab.text = @"    作者简介";
        return lab;
    } else {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        return view;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2){
        return 10;
    }
    return 60;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return self.webView1;
    } else if (section == 1){
        return self.webView2;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return self.webView1.height;
    } else if (section == 1){
        return self.webView2.height;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return nil;
    } else if (indexPath.section == 1){
        return nil;
    } else{
        static NSString *identify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        }
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-60)/2,10 , 60, 60)];
        [imageview sd_setImageWithURL:[NSURL URLWithString:_bookmodel.user_head] placeholderImage:nil];
        imageview.layer.cornerRadius = 30;
        imageview.layer.masksToBounds = YES;
        [cell.contentView addSubview: imageview];
        
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, imageview.bottom+10, kScreenWidth, 30)];
        lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        lab.font = FontSize(14);
        lab.textAlignment = NSTextAlignmentCenter;
        lab.text = _bookmodel.user_name;
        [cell.contentView addSubview:lab];
        
        CGFloat H = [self calculateString:_bookmodel.author_comment Width:12];
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(30, lab.bottom+10, kScreenWidth-60, H)];
        lab2.numberOfLines = 0;
        lab2.font = FontSize(12);
        lab.textColor = [UIColor colorWithHexString:@"939393"];
        lab2.text = _bookmodel.author_comment;
        [cell.contentView addSubview:lab2];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        return 130;
    }
    return 0;
}


-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-60, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.height;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
//        TeacherViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"TeacherViewController"];
//        VC.coursemodel = _coursemodel;
//        [self.navigationController pushViewController:VC animated:YES];
    }
    
}




- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}

- (UIWebView *)webView1{
    if (!_webView1) {
        _webView1 = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
        _webView1.delegate = self;
        _webView1.userInteractionEnabled = NO;
        
    }
    return _webView1;
}

- (UIWebView *)webView2{
    if (!_webView2) {
        _webView2 = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
        _webView2.delegate = self;
        _webView2.userInteractionEnabled = NO;
    }
    return _webView2;
}

- (UIScrollView *)scrollView{
    return self.tableView;
}


@end
