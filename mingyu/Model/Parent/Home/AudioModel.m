//
//  AudioModel.m
//  mingyu
//
//  Created by apple on 2018/5/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AudioModel.h"

@implementation AudioModel


- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]){
        self.audioId = [aDecoder decodeIntegerForKey:@"audioId"];
        self.audio_id = [aDecoder decodeIntegerForKey:@"audio_id"];
        self.subject_id = [aDecoder decodeIntegerForKey:@"subject_id"];
        self.fk_parent_id = [aDecoder decodeIntegerForKey:@"fk_parent_id"];
        self.fk_parent_title = [aDecoder decodeObjectForKey:@"fk_parent_title"];
        self.audio_title = [aDecoder decodeObjectForKey:@"audio_title"];
        self.audio_image_url = [aDecoder decodeObjectForKey:@"audio_image_url"];
        self.audio_path = [aDecoder decodeObjectForKey:@"audio_path"];
        self.audio_price = [aDecoder decodeFloatForKey:@"audio_price"];
        self.audio_gold = [aDecoder decodeIntegerForKey:@"audio_gold"];
        self.time_length = [aDecoder decodeObjectForKey:@"time_length"];
        self.play_count = [aDecoder decodeIntegerForKey:@"play_count"];
        self.audio_type = [aDecoder decodeIntegerForKey:@"audio_type"];
        self.create_time = [aDecoder decodeObjectForKey:@"create_time"];
        self.praise_number = [aDecoder decodeIntegerForKey:@"praise_number"];
        self.click_praise_true = [aDecoder decodeIntegerForKey:@"click_praise_true"];
        self.audio_sort = [aDecoder decodeIntegerForKey:@"audio_sort"];
        self.timeWithValue = [aDecoder decodeFloatForKey:@"timeWithValue"];
        self.share_url = [aDecoder decodeObjectForKey:@"share_url"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeInteger:self.audioId              forKey:@"audioId"];
    [aCoder encodeInteger:self.audio_id             forKey:@"audio_id"];
    [aCoder encodeInteger:self.subject_id           forKey:@"subject_id"];
    [aCoder encodeInteger:self.fk_parent_id         forKey:@"fk_parent_id"];
    [aCoder encodeObject:self.fk_parent_title       forKey:@"fk_parent_title"];
    [aCoder encodeObject: self.audio_title          forKey:@"audio_title"];
    [aCoder encodeObject: self.audio_image_url      forKey:@"audio_image_url"];
    [aCoder encodeObject: self.audio_path           forKey:@"audio_path"];
    [aCoder encodeFloat:  self.audio_price          forKey:@"audio_price"];
    [aCoder encodeInteger:self.audio_gold           forKey:@"audio_gold"];
    [aCoder encodeObject: self.time_length          forKey:@"time_length"];
    [aCoder encodeInteger:self.play_count           forKey:@"play_count"];
    [aCoder encodeInteger:self.audio_type           forKey:@"audio_type"];
    [aCoder encodeObject: self.create_time          forKey:@"create_time"];
    [aCoder encodeInteger:self.praise_number        forKey:@"praise_number"];
    [aCoder encodeInteger:self.click_praise_true    forKey:@"click_praise_true"];
    [aCoder encodeInteger:self.audio_sort           forKey:@"audio_sort"];
    [aCoder encodeFloat:self.timeWithValue          forKey:@"timeWithValue"];
    [aCoder encodeObject:self.share_url             forKey:@"share_url"];
}


@end
