//
//  CourseVideoCell.h
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *video_img;

@property (weak, nonatomic) IBOutlet UILabel *video_title;

@property (weak, nonatomic) IBOutlet UILabel *time_length;

@property (weak, nonatomic) IBOutlet UILabel *tryLabel;

@property (nonatomic, strong) VideoModel *videomodel;



+ (instancetype)theCourseVideoCellWithtableView:(UITableView *)tableview;

@end
