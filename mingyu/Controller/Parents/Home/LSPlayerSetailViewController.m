//
//  LSPlayerDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LSPlayerDetailViewController.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"

@interface LSPlayerDetailViewController ()<UITableViewDelegate,UITableViewDataSource,DFPlayerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PlayerDatailCell *playerHeaderView;

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *commentListArray;   //评论列表数据源

@property (nonatomic, assign) NSInteger comment_number;    //总评论数

@end

@implementation LSPlayerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupNavigationItem];
    
    
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(46);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).with.offset(-46);
        
    }];
    
    self.playerHeaderView = [PlayerDatailCell theTableViewHeaderView];
//    self.playerHeaderView.dataArray = self.dataArray;
    
    [self.playerHeaderView initDFPlayer];
    
    self.tableView.tableHeaderView = self.playerHeaderView;
    
    [self setTextViewToolbar];
    
    [self getAudioCommentList];

    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.navigationController.navigationBar.translucent = YES;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    
}

- (void)getAudioCommentList{
    
    NSInteger audio_id = [DFPlayer shareInstance].currentAudioModel.audio_id;
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/%ld/%@/%@",KURL,KGetAudioCommentList,[userDefault valueForKey:@"user_id"],audio_id,@"0",@"10"];
    [HttpRequest getWithURLString:URL parameters:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            for (NSDictionary *dic in commentList) {
                commenModel *model = [commenModel modelWithJSON:dic];
                [self.commentListArray addObject:model];
            }
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else{
        if (self.commentListArray.count > 2) {
            return 2;
        } else{
            return self.commentListArray.count;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
//        view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        return nil;
    } else{
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        head.comment_number_Lab.text = [NSString stringWithFormat:@"全部评论 | %ld",_comment_number];
        return head;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    } else{
        return 48;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
        view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        return view;
    } else{
        CommentListCell *foot = [CommentListCell TheMoreCell];
        foot.MoreButtonBlock = ^{
            CommentListViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentListViewController"];
            VC.targetID = [DFPlayer shareInstance].currentAudioModel.audio_id;;
            VC.type = @"audio";
            VC.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            [self.navigationController pushViewController:VC animated:YES];
        };
        return foot;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    } else{
        return 48;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
//        UITableViewCell *cell;
//
//        return cell;
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        return commentcell;

    } else {
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        commenModel *commenmodel = [_commentListArray objectAtIndex:indexPath.row];
        [commentcell setcellWithModel:commenmodel praise_type:@"8"];
        return commentcell;
//        PlayerDatailCell *playdetailcell = [PlayerDatailCell theCellWithTableView:tableView];
//        playdetailcell.albumlistModel = [self.aboutAttay objectAtIndex:indexPath.row];
//        return playdetailcell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (indexPath.section == 1) {
//        AlbumListModel *albumodel =  [self.aboutAttay objectAtIndex:indexPath.row];
//        AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
//        VC.albumodel = albumodel;
//        [self.navigationController pushViewController:VC animated:YES];
//    }
}



-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        
        //        [self addCommentwithInputString:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
}



-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}




#pragma mark lazy loading...
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        self.tableView.estimatedRowHeight = 97.0f;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _bottomView = apparray.firstObject;
        __weak __typeof(self) weakSelf = self;
        _bottomView.commentblock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
    }
    return _bottomView;
}

- (void)setupNavigationItem{
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame =CGRectMake(0,0, 44, 44);
    [but setImage:ImageName(@"back_black") forState:UIControlStateNormal];
    [but addTarget:self action:@selector(Back)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut = [[UIBarButtonItem alloc]initWithCustomView:but];
    self.navigationItem.leftBarButtonItem = barBut;
    
    UIButton *but2 = [UIButton buttonWithType:UIButtonTypeCustom];
    but2.frame =CGRectMake(0,0, 44, 44);
    [but2 setImage:ImageName(@"share") forState:UIControlStateNormal];
    [but2 addTarget:self action:@selector(Share)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut2 = [[UIBarButtonItem alloc]initWithCustomView:but2];
    self.navigationItem.rightBarButtonItem = barBut2;
    
}


- (void)Back{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)Share{
    
}




- (NSMutableArray *)commentListArray{
    if (_commentListArray == nil) {
        _commentListArray = [NSMutableArray array];
    }
    return _commentListArray;
}




@end
