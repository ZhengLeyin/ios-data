//
//  CommentReplayCell.m
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CommentReplayCell.h"

static CGFloat cellHeight = 0;

@implementation CommentReplayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initControl];

}

+ (CGFloat)cellHeight {
    return cellHeight;
}

-(void)initControl{
    _replayContentLab.preferredMaxLayoutWidth = kScreenWidth-130;
    
}


- (void)setReplaymodel:(CommentReplayModel *)replaymodel{
    _replaymodel = replaymodel;
//    NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,_replaymodel.replay_head];
    [_userHeadButton zp_setImageWithURL:_replaymodel.replay_head forState:0 placeholderImage:ImageName(@"默认头像")];
    _replayTimeLab.text = _replaymodel.replay_time_status;
    
    if (replaymodel.replay_user_id == [USER_ID integerValue]) {
        _replayButton.hidden = YES;
    } else {
        _replayButton.hidden = NO;
    }
    [self changePraiseButton:_replaymodel];
    
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // 行间距设置为10
    [paragraphStyle  setLineSpacing:5];
    NSString *contentStr = [NSString string];
    if (_replaymodel.to_user_name) {
        contentStr = [NSString stringWithFormat:@"%@ > %@：%@",_replaymodel.replay_name,_replaymodel.to_user_name,_replaymodel.replay_content];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:contentStr];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"767676"] range:NSMakeRange(0,replaymodel.replay_name.length+replaymodel.to_user_name.length+4)];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentStr length])];
        _replayContentLab.attributedText = attrString;
    } else {
        contentStr = [NSString stringWithFormat:@"%@：%@",_replaymodel.replay_name,_replaymodel.replay_content];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:contentStr];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"767676"] range:NSMakeRange(0,replaymodel.replay_name.length+1)];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentStr length])];
        _replayContentLab.attributedText = attrString;
    }
    
    cellHeight = [self hideLabelLayoutHeight:contentStr withTextFontSize:12 andWidth:kScreenWidth-130]+45;
//    ZPLog(@"CommentReplayCellH-------%f",cellHeight);
}


-(CGFloat )hideLabelLayoutHeight:(NSString *)content withTextFontSize:(CGFloat)FontSize andWidth:(CGFloat)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;  // 段落高度
    NSMutableAttributedString *attributes = [[NSMutableAttributedString alloc] initWithString:content];
    [attributes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize] range:NSMakeRange(0, content.length)];
    [attributes addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];
    CGSize attSize = [attributes boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return attSize.height;
}

- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    return sizeToFit.height+15;
}


- (void)changePraiseButton:(CommentReplayModel *)replaymodel{
    if (_replaymodel.click_praise_true == 0) {
        [_praiseButton setImage:ImageName(@"评论赞") forState:0];
    } else {
        [_praiseButton setImage:ImageName(@"评论已赞") forState:0];
    }
    if (_replaymodel.praise_number) {
        [_praiseButton setTitle:[NSString stringWithFormat:@"%ld",_replaymodel.praise_number] forState:0];
    } else {
        [_praiseButton setTitle:@"赞" forState:0];
    }
}


- (IBAction)ReplayCellUserHeader:(id)sender {
    PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
    VC.user_id = _replaymodel.replay_user_id;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

//评论回复
- (IBAction)ReplayCellReplayButton:(id)sender {
    if (self.delegae && [self.delegae respondsToSelector:@selector(ReplayCellReplayButton:CommentCellIndex:ReplayCellIndex:)]) {
        [self.delegae ReplayCellReplayButton:_replaymodel CommentCellIndex:_CommentCellIndex ReplayCellIndex:_ReplayCellIndex];
    }
}


//评论点赞
- (IBAction)ReplayCellPraiseButton:(UIButton *)sender {
    if (self.delegae && [self.delegae respondsToSelector:@selector(ReplayCellPraiseButton:CommentCellIndex:ReplayCellIndex:)]) {
        [self.delegae ReplayCellPraiseButton:_replaymodel CommentCellIndex:_CommentCellIndex ReplayCellIndex:_ReplayCellIndex];
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
