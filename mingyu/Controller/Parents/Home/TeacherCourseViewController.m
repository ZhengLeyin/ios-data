//
//  TeacherCourseViewController.m
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TeacherCourseViewController.h"
/**讲师信息Cell*/
#import "TeacherIntroduceCell.h"
#import "TeacherViewController.h"
#import "CourseAudioViewController.h"
#import "JXPagerView.h"


@interface TeacherCourseViewController ()<UITableViewDelegate,UITableViewDataSource,JXPagerViewListViewDelegate>

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL showMore;

@property (nonatomic, strong) NSMutableArray *arrayTeacherRecommendData;
@end

@implementation TeacherCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        /**type == 1 精品课传过来的*/
        if (_type == 1) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 45);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);;
        }
    }];
    [self refreshTableview];
    
    _showMore = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
}

-(void)refreshTableview {
    [self getCourseUserIdGetTeacherCourse];
    /**获取推荐老师*/
    [self getTeacherTUserIdRecommendTeacher];
}

#pragma mark /** 获取老师课程视频  */
- (void)getCourseUserIdGetTeacherCourse {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@",KURL,KGetCourseUserId,@(_coursemodel.fk_user_id)];
    ZPLog(@"URL=%@",URL);
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.arrayData removeAllObjects];
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                CourseModel *model = [CourseModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma mark /** 获取推荐老师 */
- (void)getTeacherTUserIdRecommendTeacher {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/recommendTeacher",KURL,KGetTeacherTUserId,@(_coursemodel.fk_user_id)];
    ZPLog(@"URL=%@",URL);
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            [self.arrayTeacherRecommendData removeAllObjects];
            for (NSDictionary *dic in data) {
                CourseModel *model = [CourseModel modelWithJSON:dic];
                [self.arrayTeacherRecommendData addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_arrayData && _arrayData.count > 0) {
            if (_arrayData.count > 4) {
                if (self.showMore == NO) {
                    return 4;
                }else {
                    return _arrayData.count;
                }
            }else {
                return _arrayData.count;
            }
        }else {
            return 0;
        }
    }else {
        return _arrayTeacherRecommendData.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section  == 0) {
        ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
        if (self.arrayData && self.arrayData.count > indexPath.row) {
            CourseModel *model = self.arrayData[indexPath.row];
            cell.coursemodel = model;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }else {
        TeacherIntroduceCell *cell = [TeacherIntroduceCell theTeacherIntroduceCellOfDateilWithtableView:tableView];
        if (self.arrayTeacherRecommendData && self.arrayTeacherRecommendData.count > indexPath.row) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.teacherUserImageView.layer.cornerRadius = 30;
            cell.teacherUserImageView.layer.masksToBounds = YES;
            cell.lineBackView.hidden = NO;
            CourseModel *model = self.arrayTeacherRecommendData[indexPath.row];
            cell.model = model;
            cell.pushTeacherDetailsBlock = ^{
                self.pushTeacherDetailsBlock(model);
            };
            cell.followButtonBlock = ^{
                // [self followButtonAction];
            };
            
        }
        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (_arrayData && _arrayData.count > indexPath.row) {
            CourseModel *model = [self.arrayData objectAtIndex:indexPath.row];
            self.courseCagetoryBlock(model);
        }
    }else {
        
    }
}

/**获取更多视频View*/
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, kScreenWidth, 50);
        [button setTitleColor:[UIColor colorWithHexString:@"#60D5F5"] forState:(UIControlStateNormal)];
        button.titleLabel.font = FontSize(12.0);
        button.backgroundColor = [UIColor whiteColor];
        if (_showMore == NO) {
            [button setImage:[UIImage imageNamed:@"查看全部"] forState:UIControlStateNormal];
            [button setTitle:@"查看全部课程 " forState:(UIControlStateNormal)];
        }else {
            [button setImage:[UIImage imageNamed:@"收起全部"] forState:UIControlStateNormal];
            [button setTitle:@"收起更多课程 " forState:(UIControlStateNormal)];
        }
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, - button.imageView.image.size.width, 0, button.imageView.image.size.width)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, button.titleLabel.bounds.size.width, 0, -button.titleLabel.bounds.size.width)];
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithHexString:@"#F7F8FA"];
        [view addSubview:button];
        [[button rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
            if (self.arrayData.count > 4) {
                if (self.showMore == NO) {
                    self.showMore = YES;
                }else {
                    self.showMore = NO;
                }
                [self.tableView reloadData];
                NSIndexPath * index = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
        }];
        if (self.arrayData.count > 4) {
            return view;
        }else {
            return [UIView new];
        }
    }else {
        return [UIView new];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        if (self.arrayData.count > 4) {
            return 60;
        }else {
            return 1;
        }
    }else {
        return 1;
    }
}

/**推荐讲师*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (self.arrayData.count == 0) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 245)];
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 188, 154)];
            imageView.centerX = view.centerX;
            imageView.centerY = view.centerY;
            imageView.image = ImageName(@"img_course_nothing");
            [view addSubview:imageView];
            
            UILabel *titleContentLabel = [UILabel new];
            titleContentLabel.textAlignment = NSTextAlignmentCenter;
            titleContentLabel.textColor = [UIColor colorWithHexString:@"#D0D0D0"];
            titleContentLabel.font = FontSize(12.0);
            [view addSubview:titleContentLabel];
            [titleContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(imageView.mas_bottom).offset(0);
                make.centerX.equalTo(imageView);
            }];
            if (_coursemodel.fk_user_id == [USER_ID integerValue]) {
                titleContentLabel.text = @"请上传您的视频课程";
            } else {
                titleContentLabel.text = @"暂无课程";
            }
            
            
            UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 254 - 10, kScreenWidth, 10)];
            lineView.backgroundColor = [UIColor colorWithHexString:@"#F7F8FA"];
            [view addSubview:lineView];
            
            return view;
        }else {
            return [UIView new];
        }

    } else {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        UIView *topview = [UIView new];
        topview.backgroundColor = [UIColor colorWithRed:247/255.0 green:248/255.0 blue:250/255.0 alpha:1];
        topview.frame = CGRectMake(0, 0, kScreenWidth, 0);
        [view addSubview:topview];
        /**线条view*/
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(topview.frame)+24, 4, 17)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#50D0F4"];
        [view addSubview:lineView];
        /**讲师介绍Label*/
        UILabel *teacherIntroduceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lineView.frame) + 6, CGRectGetMaxY(topview.frame)+20, 80, 25)];
        teacherIntroduceLabel.text = @"推荐讲师";
        teacherIntroduceLabel.font = FontSize(18.0);
        teacherIntroduceLabel.textColor = [UIColor colorWithHexString:@"#2C2C2C"];
        [view addSubview:teacherIntroduceLabel];
        return view;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        if (self.arrayData.count == 0) {
              return 235;
        }else {
              return 1;
        }
      
    }else {
        return 0+20+25;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 155;
    }else {
        return 90;
    }
}

- (UIScrollView *)scrollView{
    return self.tableView;
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 155;
    }
    return _tableView;
}

-(NSMutableArray *)arrayTeacherRecommendData {
    if (_arrayTeacherRecommendData == nil) {
        _arrayTeacherRecommendData = [NSMutableArray array];
    }
    return _arrayTeacherRecommendData;
}

- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}
@end
