//
//  GoldHeaderView.m
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "GoldHeaderView.h"

@implementation GoldHeaderView


- (void)awakeFromNib{
    [super awakeFromNib];
    
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_head_iamge.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_head_iamge.bounds.size];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _head_iamge.bounds;
    maksLayer.path = maksPath.CGPath;
    _head_iamge.layer.mask = maksLayer;
//    NSString *head_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDuser_head]];
    [_head_iamge zp_setImageWithURL:[userDefault objectForKey:KUDuser_head] placeholderImage:ImageName(@"默认头像")];
    
}



@end
