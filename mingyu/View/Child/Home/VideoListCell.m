//
//  VideoListCell.m
//  mingyu
//
//  Created by apple on 2018/4/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoListCell.h"
#import <Masonry/Masonry.h>

@implementation VideoListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    
        [self setupUI];
    }
    return self;
}

- (void)setDownloadStatus:(YCDownloadStatus)status {
    NSString *statusStr = @"下载";
    _BGView.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
    switch (status) {
        case YCDownloadStatusWaiting:
            statusStr = @"gouxuan";
            break;
        case YCDownloadStatusDownloading:
            statusStr = @"gouxuan";
            break;
        case YCDownloadStatusPaused:
            statusStr = @"gouxuan";
            break;
        case YCDownloadStatusFinished:
            statusStr = @"gouxuan";
            break;
        case YCDownloadStatusFailed:
            statusStr = @"gouxuan";
            break;
            
        default:
            _BGView.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
            break;
    }
    self.chooseIMG.image = ImageName(statusStr);
}


- (void)setupUI{
    [self addSubview:self.BGView];
    
    [self addSubview:self.chooseIMG];
    
    [self addSubview:self.videotitleLib];

    [self.BGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-10);
    }];
    
    [self.chooseIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.BGView).offset(-20);
        make.centerY.mas_equalTo(self.BGView);
    }];
    
    [self.videotitleLib mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.BGView.mas_left).offset(10);
        make.right.equalTo(self.chooseIMG).offset(10);
        make.centerY.equalTo(self.BGView);
    }];
    
}



- (UIView *)BGView{
    if (_BGView == nil) {
        _BGView = [[UIView alloc] init];
        _BGView.backgroundColor = [UIColor whiteColor];
        _BGView.layer.cornerRadius = 15;
        _BGView.layer.borderWidth = 1;
        _BGView.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
    }
    return _BGView;
}


- (UILabel *)videotitleLib{
    if (_videotitleLib == nil) {
        _videotitleLib = [[UILabel alloc] init];
        _videotitleLib.font = FontSize(14);
    }
    return _videotitleLib;
}

- (UIImageView *)chooseIMG{
    if (_chooseIMG == nil) {
        _chooseIMG = [[UIImageView alloc] init];
        
    }
    return _chooseIMG;
}



@end
