//
//  CircleCollectionViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ParentCircleModel;

@interface CircleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *circle_img;

@property (weak, nonatomic) IBOutlet UILabel *circle_name_Lab;

@property (weak, nonatomic) IBOutlet UIButton *delete_button;


@property (nonatomic, strong) ParentCircleModel *circlemodel;


@end
