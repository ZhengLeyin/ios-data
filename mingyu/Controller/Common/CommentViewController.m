//
//  CommentViewController.m
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CommentViewController.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"
//#import "CommentListCell.h"
#import "CommentCell.h"

#define BottomVIewHeight 46

@interface CommentViewController ()<UITableViewDelegate,UITableViewDataSource,CommentCellDelegate,BottomCommentDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *commmentArray;

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, assign) CGPoint contentoffset;

@property (nonatomic, assign) AccusationType accusationtype;  //举报类型
@property (nonatomic, assign) PraiseType commentpraisetype;  //评论点赞类型
@property (nonatomic, assign) PraiseType replaypraisetype;   //回复点赞类型
@property (nonatomic, assign) NSInteger comment_number;      //帖子总数

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;

    if (_comment_type == childcare_comment) {
        _accusationtype = accusation_childcare_comment;
        _commentpraisetype = praise_childcare_comment;
        _replaypraisetype = praise_childcare_replay;
    } else if (_comment_type == topic_comment){
        _accusationtype = accusation_topic_comment;
        _commentpraisetype = praise_topic_comment;
        _replaypraisetype = praise_topic_replay;
    } else if (_comment_type == article_comment){
        _accusationtype = accusation_article_comment;
        _commentpraisetype = praise_article_comment;
        _replaypraisetype = praise_article_replay;
    } else if (_comment_type == video_comment) {
        _accusationtype = accusation_video_comment;
        _commentpraisetype = praise_video_comment;
        _replaypraisetype = praise_video_replay;
    } else if (_comment_type == audio_comment) {
        _accusationtype = accusation_audio_comment;
        _commentpraisetype = praise_audio_comment;
        _replaypraisetype = praise_audio_replay;
    } else if (_comment_type == course_comment) {
        _accusationtype = accusation_course_comment;
        _commentpraisetype = praise_course_comment;
        _replaypraisetype = praise_course_replay;
    } else if (_comment_type == listen_book_comment){
        _accusationtype = accusation_book_comment;
        _commentpraisetype = praise_book_comment;
        _replaypraisetype = praise_book_replay;
    }

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(BottomVIewHeight);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).with.offset(-BottomVIewHeight);
    }];
    
    
    [self setTextViewToolbar];
    
    [self refreshTableview];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage  imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.commmentArray.count>0) {
            [_commmentArray removeAllObjects];
        }
        _start = 0;
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self getCommentList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getCommentList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


//获取评论列表
- (void)getCommentList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":[NSString stringWithFormat:@"%ld",_targetID],
                                 @"comment_type":@(_comment_type),
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            _start += 20;
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            self.title = [NSString stringWithFormat:@"%@条评论",data[@"comment_number"]];
            NSArray *commentList = data[@"commentList"];
            if (commentList.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            NSInteger targetIndex = -1;
            for (NSDictionary *dic in commentList) {
                CommentModel *model = [CommentModel modelWithJSON:dic];
                [self.commmentArray addObject:model];
                if (_targetCommentmodel.comment_id == model.comment_id) {
                    targetIndex = self.commmentArray.count-1;
                }
            }
            [_tableView reloadData];
            
            if (_targetCommentmodel && targetIndex>= 0) {
                [self CommentCellCommentButton:_targetCommentmodel CommentCellIndex:targetIndex];
            }

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
    [self.maskView addSubview:self.inputToolbar];
}

-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
    self.targetCommentmodel = nil;
}



#pragma mark- tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commmentArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //在model中建个height参数存储高度
    if (_commmentArray && _commmentArray.count > indexPath.row) {
        CommentModel *model = [_commmentArray objectAtIndex:[indexPath row]];
        if (model.height==0) {
            return [CommentCell cellHeight];
        }else{
            return model.height;
        }
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CommentCell *cell = (CommentCell *) [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentCell" owner:nil options:nil] lastObject];
    }
    if (self.commmentArray && self.commmentArray.count > indexPath.row) {
        cell.CommentCellIndex =[indexPath row];
        cell.commentmodel = [_commmentArray objectAtIndex:[indexPath row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.commentpraisetype = _commentpraisetype;
        cell.replaypraisetype = _replaypraisetype;
        cell.delegate = self;
    }
    return cell;
}

//CommentCell 点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (!_commmentArray || _commmentArray.count == 0) {
        return;
    }
    CommentModel *commentmodel = [_commmentArray objectAtIndex:[indexPath row]];
    if (commentmodel.fk_user_id == [USER_ID integerValue]) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:commentmodel.comment_content
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* action1 = [UIAlertAction actionWithTitle:@"复制" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            //响应事件
                                                            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                            pasteboard.string = commentmodel.comment_content;
                                                            [UIView ShowInfo:@"复制成功" Inview:self.view];
                                                        }];
        
        UIAlertAction* action2 = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive
                                                        handler:^(UIAlertAction * action) {
                                                            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
                                                            NSDictionary *parameters = @{
                                                                                         @"user_id":USER_ID,
                                                                                         @"comment_id":[NSString stringWithFormat:@"%ld",(long)commentmodel.comment_id],
                                                                                         @"comment_type":@(_comment_type)
                                                                                         };
                                                            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                NSDictionary *json = responseObject;
                                                                ZPLog(@"%@",json);
                                                                ZPLog(@"%@",json[@"message"]);
                                                                BOOL success = [json[@"success"]boolValue];
                                                                if (success) {
                                                                    _comment_number -- ;
                                                                    self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
                                                                    [_commmentArray removeObjectAtIndex:indexPath.row];
                                                                    [_tableView reloadData];
                                                                } else {
                                                                    [UIView ShowInfo:json[@"message"] Inview:self.view];
                                                                }
                                                            } failure:^(NSError *error) {
                                                                
                                                            }];
                                                        }];
        
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:commentmodel.comment_content
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* action1 = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            //响应事件
                                                            [self CommentCellCommentButton:commentmodel CommentCellIndex:indexPath.row];

                                                        }];
        
        UIAlertAction* action2 = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            //响应事件
                                                            
                                                            [self addAccusationTargetid:commentmodel.comment_id];
                                                        }];
        
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];
        
        [self presentViewController:alert animated:YES completion:nil];
        
//        [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"回复" block1:^{
//            [self CommentCellCommentButton:commentmodel CommentCellIndex:indexPath.row];
//        } title2:@"举报" block2:^{
//            [self addAccusationTargetid:commentmodel.comment_id];
//
//        }];
    }
}


#pragma mark - BottomViewCellDelegate
- (void)commentButtonClick{
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addCommentwithInputString:text];
    }];
}

//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":[NSString stringWithFormat:@"%ld" ,_targetID]
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(_comment_type)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            CommentModel *model = [CommentModel modelWithJSON:json[@"data"]];
            [_commmentArray insertObject:model atIndex:0];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark - CommentCellDelegate
/** 查看更多 收起更多 */
- (void)CommentCellShowHiddeMore:(CommentModel *)commentmodel row:(NSInteger )index{
    commentmodel.height = 0.0;
    [_commmentArray replaceObjectAtIndex:index withObject:commentmodel];
    [_tableView reloadData];
    
    NSIndexPath * indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

/** ReplayCell 点击事件 */
- (void)CommentCellReplayCell:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex didSelectRow:(NSInteger)index{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (replaymodel.replay_user_id == [USER_ID integerValue]) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:replaymodel.replay_content
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* action1 = [UIAlertAction actionWithTitle:@"复制" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            //响应事件
                                                            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                            pasteboard.string = replaymodel.replay_content;
                                                            [UIView ShowInfo:@"复制成功" Inview:self.view];
                                                        }];
        
        UIAlertAction* action2 = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive
                                                        handler:^(UIAlertAction * action) {
                                                            //响应事件
                                                            
                                                            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteReplayById];
                                                            NSDictionary *parameters = @{
                                                                                         @"user_id":USER_ID,
                                                                                         @"replay_id":[NSString stringWithFormat:@"%ld",(long)replaymodel.replay_id],
                                                                                         @"replay_type":@(_comment_type)
                                                                                         };
                                                            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                NSDictionary *json = responseObject;
                                                                ZPLog(@"%@",json);
                                                                ZPLog(@"%@",json[@"message"]);
                                                                BOOL success = [json[@"success"]boolValue];
                                                                if (success) {
                                                                    _comment_number -- ;
                                                                    self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
                                                                    CommentModel *commentmodel = [_commmentArray objectAtIndex:commentindex];
                                                                    [commentmodel.replayList removeObjectAtIndex:index];
                                                                    commentmodel.height = 0;
                                                                    [_tableView reloadData];
                                                                } else {
                                                                    [UIView ShowInfo:json[@"message"] Inview:self.view];
                                                                }
                                                            } failure:^(NSError *error) {
                                                                
                                                            }];                                                        }];
        
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];
        
        [self presentViewController:alert animated:YES completion:nil];
        
//        [self showActionSheetWithmessage:replaymodel.replay_content Title1:@"复制" block1:^{
//            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//            pasteboard.string = replaymodel.replay_content;
//            [UIView ShowInfo:@"复制成功" Inview:self.view];
//        } title2:@"删除" block2:^{
//            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteReplayById];
//            NSDictionary *parameters = @{
//                                         @"user_id":USER_ID,
//                                         @"replay_id":[NSString stringWithFormat:@"%ld",(long)replaymodel.replay_id],
//                                         @"replay_type":@(_comment_type)
//                                         };
//            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
//                NSDictionary *json = responseObject;
//                ZPLog(@"%@",json);
//                ZPLog(@"%@",json[@"message"]);
//                BOOL success = [json[@"success"]boolValue];
//                if (success) {
//                    _comment_number -- ;
//                    self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
//                    CommentModel *commentmodel = [_commmentArray objectAtIndex:commentindex];
//                    [commentmodel.replayList removeObjectAtIndex:index];
//                    commentmodel.height = 0;
//                    [_tableView reloadData];
//                } else {
//                    [UIView ShowInfo:json[@"message"] Inview:self.view];
//                }
//            } failure:^(NSError *error) {
//
//            }];
//        }];
    } else {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:replaymodel.replay_content
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* action1 = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                 [self ReplayCellReplayButton:replaymodel CommentCellIndex:commentindex ReplayCellIndex:index];

                                                             }];
        
        UIAlertAction* action2 = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //响应事件
                                                                  
                                                                  [self addAccusationTargetid:replaymodel.replay_id];
                                                              }];
        
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];

        [self presentViewController:alert animated:YES completion:nil];
        
        
//        [self showActionSheetWithmessage:replaymodel.replay_content Title1:@"回复" block1:^{
//            [self ReplayCellReplayButton:replaymodel CommentCellIndex:commentindex ReplayCellIndex:index];
//        } title2:@"举报" block2:^{
//            [self addAccusationTargetid:replaymodel.replay_id];
//        }];
    }
}

/** CommentCell 回复 点击事件 */
- (void)CommentCellCommentButton:(CommentModel *)commentmodel CommentCellIndex:(NSInteger)commentindex{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
    NSString *commnentStr = [userDefault objectForKey:KUDLastCommentString];
    if (commnentStr.length == 0) {
        self.inputToolbar.placeholder = [NSString stringWithFormat:@"回复 %@:",commentmodel.comment_name];
    }
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf replayComment:commentmodel index:commentindex InputString:text];
    }];
}

/** ReplayCell 回复 点击事件 */
- (void)ReplayCellReplayButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
    NSString *commnentStr = [userDefault objectForKey:KUDLastCommentString];
    if (commnentStr.length == 0) {
        self.inputToolbar.placeholder = [NSString stringWithFormat:@"回复 %@:",replaymodel.replay_name];
    }
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf replayCommentReplay:replaymodel CommentCellIndex:commentindex ReplayCellIndex:replayindex InputString:text];
    }];
}



//对一级评论进行回复
- (void)replayComment:(CommentModel *)commentmodel index:(NSInteger )index InputString:(NSString *)inputstr{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCommentReplay];
    NSDictionary *dic = @{
                          @"replay_user_id":USER_ID,
                          @"replay_content":inputstr,
                          @"fk_comment_id":@(commentmodel.comment_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment_repaly":[NSString convertToJsonData:dic],
                                 @"replay_type":@(_comment_type)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            NSDictionary *data = json[@"data"];
            [commentmodel.replayList addObject:data];
            commentmodel.height = 0;
            [_commmentArray replaceObjectAtIndex:index withObject:commentmodel];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//对二级评论进行回复
- (void)replayCommentReplay:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex InputString:(NSString *)inputstr{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCommentReplay];
    NSDictionary *dic = @{
                          @"replay_user_id":USER_ID,
                          @"fk_to_user_id":@(replaymodel.replay_user_id),
                          @"replay_content":inputstr,
                          @"fk_comment_id":@(replaymodel.fk_comment_id),
                          @"parent_id":@(replaymodel.replay_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment_repaly":[NSString convertToJsonData:dic],
                                 @"replay_type":@(_comment_type)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            NSDictionary *data = json[@"data"];
            CommentModel *commentmodel = [_commmentArray objectAtIndex:commentindex];
            [commentmodel.replayList addObject:data];
            commentmodel.height = 0;
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

//举报评论
- (void)addAccusationTargetid:(NSInteger )targetid{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"from_id":@(targetid),
                                 @"type_id":@(_accusationtype)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"举报成功" Inview:self.view];
        }
    } failure:^(NSError *error) {
        
    }];
}

//-(void)addComment:(NSInteger)index{
//
//    CommentModel * detailModel = [_commmentArray objectAtIndex:index];
//    //height为0时高度更新
//    detailModel.height = 0.0;
//    [_commmentArray replaceObjectAtIndex:index withObject:detailModel];
//    //  CommentModel * commentModel = [[CommentModel alloc]init];
//    //  commentModel.message = @"这是新增的评论";
//    //  [detailModel.commentArray addObject:commentModel];
//    [_tableView reloadData];
//
//    NSIndexPath * indexpath = [NSIndexPath indexPathForRow:index inSection:0];
////    [self.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
//    [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:NO];
//
//
//    
//    //评论后cell滚动居中
//    
////    [self didCommentScrollToMiddle:indexpath];
//    
//}
////评论后cell滚动居中
//-(void)didCommentScrollToMiddle:(NSIndexPath *)indexpath{
//
//    //获取当前cell在tableview中的位置
//
//    CGRect rectintableview=[self.tableView rectForRowAtIndexPath:indexpath];
//
//    //获取当前cell在屏幕中的位置
//
//    CGRect rectinsuperview=[self.tableView convertRect:rectintableview fromView:[self.tableView superview]];
//
//    _contentoffset.x=self.tableView.contentOffset.x;
//
//    _contentoffset.y=self.tableView.contentOffset.y;
//
//    if ((rectintableview.origin.y+50-self.tableView.contentOffset.y)>200) {
//
//        [self.tableView setContentOffset:CGPointMake(self.tableView.contentOffset.x,((rectintableview.origin.y-self.tableView.contentOffset.y)-150)+self.tableView.contentOffset.y) animated:NO];
//
//    }
//}



-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil][1];
        _bottomView.frame = CGRectMake(0, kScreenHeight-BottomVIewHeight, kScreenWidth, BottomVIewHeight);
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (NSMutableArray *)commmentArray{
    if (!_commmentArray) {
        _commmentArray = [NSMutableArray array];
    }
    return _commmentArray;
}


- (IBAction)back:(id)sender {
    if (self.CommentListBlock) {
        self.CommentListBlock(_commmentArray, _comment_number);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didMoveToParentViewController:(UIViewController*)parent{
    [super didMoveToParentViewController:parent];
    ZPLog(@"%s,%@",__FUNCTION__,parent);
    if(!parent){
        //        ZPLog(@"页面pop成功了");
        if (self.CommentListBlock) {
            self.CommentListBlock(_commmentArray, _comment_number);
        }
    }
}

@end
