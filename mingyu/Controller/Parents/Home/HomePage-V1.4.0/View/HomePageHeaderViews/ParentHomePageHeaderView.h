//
//  ParentHomePageHeaderView.h
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentHomePageBannerView.h"
#import "ParentHomePageFunctionModuleView.h"
#import "HomePageAgeRelatedOfHeaderView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ParentHomePageHeaderView : UIView<SDCycleScrollViewDelegate,TZImagePickerControllerDelegate,UINavigationControllerDelegate>
#pragma mark 区分两岁前和两岁后的区别
/*不同点:
 *1.两岁前:banner 无;  两岁后:banner 有
 *2.两岁前:宝宝年月日,平均身高,体重;  两岁后:没有
 *3.两岁前:时光印记 有;  两岁后:时光印记 无
 *共同点:
 *1.功能选择模块暂时相同View
 */
#pragma mark 两岁前

@property (weak, nonatomic) IBOutlet UIView *ageRelatedView;

/**时光印记 背景View*/
@property (weak, nonatomic) IBOutlet UIView *timeRecordBackgroundView;
/**时光印记 去记录Label*/
@property (weak, nonatomic) IBOutlet UILabel *goToRecordLabel;
/**时光印记 Button*/
@property (weak, nonatomic) IBOutlet UIButton *timeRecordButton;

#pragma mark 两岁后
@property (weak, nonatomic) IBOutlet UIView *bannerView;


#pragma mark 公用部分View Select function module
@property (weak, nonatomic) IBOutlet UIView *functionSelectModuleView;

/** 两岁前View */
+ (instancetype)TheBeforeAgeOfTwoOfParentHomePageHeaderView;

/** 两岁后View */
+ (instancetype)TheAfterTwoYearsOldOfParentHomePageHeaderView;

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;

@property (nonatomic, strong) NSMutableArray *BabyRecordArray;

/**两岁后 广告位*/
@property (nonatomic, strong) ParentHomePageBannerView * homePageBannerViewbannerView;

/**功能模块 单独View*/
@property (nonatomic, strong) ParentHomePageFunctionModuleView * homePageFunctionModuleView;

/**两岁前和年龄相关 单独View*/
@property (nonatomic, strong) HomePageAgeRelatedOfHeaderView * ageHeaderView; 
@end

NS_ASSUME_NONNULL_END
