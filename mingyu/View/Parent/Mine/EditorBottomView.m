//
//  EditorBottomView.m
//  mingyu
//
//  Created by apple on 2018/9/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "EditorBottomView.h"

@implementation EditorBottomView


- (IBAction)chooseAllButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseAllButonClicked)]) {
        [self.delegate chooseAllButonClicked];
    }
}


- (IBAction)deleteButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteButtonClicked)]) {
        [self.delegate deleteButtonClicked];
    }
}


@end
