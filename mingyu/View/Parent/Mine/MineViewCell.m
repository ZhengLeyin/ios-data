//
//  MineViewCell.m
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineViewCell.h"

@implementation MineViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
//    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_icon_image.bounds cornerRadius:_icon_image.height/2];
//    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
//    maksLayer.frame = _icon_image.bounds;
//    maksLayer.path = maksPath.CGPath;
//    _icon_image.layer.mask = maksLayer;
//    
    UIBezierPath *maksPath2 = [UIBezierPath bezierPathWithRoundedRect:_user_image.bounds cornerRadius:_user_image.height/2];
    CAShapeLayer *maksLayer2 = [[CAShapeLayer alloc] init];
    maksLayer2.frame = _user_image.bounds;
    maksLayer2.path = maksPath2.CGPath;
    _user_image.layer.mask = maksLayer2;
    
    _VIP_lab.hidden = YES;
}


+ (instancetype)theMineCellWithTableview:(UITableView *)tableview{
    static NSString *cellID = @"MineViewCell";
    MineViewCell *cell = [tableview dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"MineViewCell" owner:nil options:nil][0];
    }
    return cell;
}



+ (instancetype)theMineHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"MineViewCell" owner:nil options:nil][1];
}



- (void)setUsermodel:(UserModel *)usermodel{

    _usermodel = usermodel;
    _username_lab.text = usermodel.user_name;
    if (_usermodel.area_name) {
        _introduce_lab.text = [NSString stringWithFormat:@"%@ | %@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈",_usermodel.area_name];
    } else {
        _introduce_lab.text = [NSString stringWithFormat:@"%@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈"];
    }
//    _introduce_lab.text = [NSString stringWithFormat:@"%@ | %@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈",_usermodel.area_name ? _usermodel.area_name : @""];
    _shoucangcount_lab.text = [NSString stringWithFormat:@"%ld",usermodel.collectNum];
    _tiezicount_lab.text = [NSString stringWithFormat:@"%ld",usermodel.topicNumber];
    _guanzhucount_lab.text = [NSString stringWithFormat:@"%ld",usermodel.userFollowNumber];
    _fensicount_lab.text = [NSString stringWithFormat:@"%ld",usermodel.followUserNumber];

}


//登录
- (IBAction)LoginAndRegistButton:(id)sender {
//    [self LoginView];
}

//用户信息
- (IBAction)UserInfoButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        UserInfoViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"UserInfoViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    } else {
        [self LoginView];
    }
}

//收藏
- (IBAction)ShouChangButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        CollectViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"CollectViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    } else {
        [self LoginView];
    }
}

//帖子
- (IBAction)TieZiButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        MineTopicViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineTopicViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    } else {
        [self LoginView];
    }
}

//关注
- (IBAction)GuanZhuButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        MineAttentionViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineAttentionViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    } else {
        [self LoginView];
    }
}

//粉丝
- (IBAction)FenSiButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        MineFansViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineFansViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    } else {
        [self LoginView];
    }
}


//跳转登录
- (void)LoginView{
    CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
    [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
