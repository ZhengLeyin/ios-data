
//  PlayerDatailCell.m
//  mingyu
//
//  Created by apple on 2018/5/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PlayerDatailCell.h"
#import "RateListView.h"
#import "TimeListView.h"

@interface PlayerDatailCell()

@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) PlayerDetailPopView *listView;
@property (nonatomic, strong) RateListView *rateListView;
@property (nonatomic, strong) TimeListView *timeListView;

@property (nonatomic, strong) NSMutableArray<AudioModel *> *dataArray;


@end

@implementation PlayerDatailCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_beisu_button setTitleEdgeInsets:UIEdgeInsetsMake(0, -_beisu_button.imageView.size.width, 0, _beisu_button.imageView.size.width)];
    [_beisu_button setImageEdgeInsets:UIEdgeInsetsMake(0, _beisu_button.titleLabel.bounds.size.width, 0, -_beisu_button.titleLabel.bounds.size.width)];
    
    [_dingshi_button setTitleEdgeInsets:UIEdgeInsetsMake(0, -_dingshi_button.imageView.size.width, 0, _dingshi_button.imageView.size.width)];
    [_dingshi_button setImageEdgeInsets:UIEdgeInsetsMake(0, _dingshi_button.titleLabel.bounds.size.width, 0, -_dingshi_button.titleLabel.bounds.size.width)];
    
    _beisu_button.layer.cornerRadius =_dingshi_button.layer.cornerRadius = 15;
    _beisu_button.layer.borderWidth = _dingshi_button.layer.borderWidth = 0.5;
    _beisu_button.layer.borderColor = _dingshi_button.layer.borderColor = [UIColor colorWithHexString:@"A8A8A8"].CGColor;
    
    _audio_image.layer.cornerRadius=5;
    _album_image.contentMode = _audio_image.contentMode = UIViewContentModeScaleAspectFill;
    _album_image.clipsToBounds = _audio_image.clipsToBounds = YES;
    
    _audio_view.layer.cornerRadius=5;
    _audio_view.layer.shadowColor=[UIColor blackColor].CGColor;
    _audio_view.layer.shadowOffset=CGSizeMake(0,0);
    _audio_view.layer.shadowOpacity=0.5;
}


+ (instancetype)theTableViewHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"PlayerDatailCell" owner:nil options:nil][0];
}

+ (instancetype)theSectionHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"PlayerDatailCell" owner:nil options:nil][1];
}


+ (instancetype)theCellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"PlayerDatailCell";
    PlayerDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil ) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PlayerDatailCell" owner:nil options:nil][2];
    }
    return cell;
}


#pragma mark - 初始化DFPlayer
- (void)initDFPlayer{
    [DFPlayer shareInstance].delegate = self;
//    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）

    [_TitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(20);
        make.right.mas_equalTo(-20);
        make.top.mas_offset(10);
    }];
    
    [_audio_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(50);
//        make.top.equalTo(_TitleLab.mas_bottom).width.offset(10);
        make.left.mas_offset(75);
        make.width.mas_offset(kScreenWidth-150);
        make.height.mas_offset(kScreenWidth-150);
    }];
    
    [_audio_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_audio_view);
    }];
    
    
    
    CGRect listRect = CGRectMake(15, 385, 30, 30);
    CGRect typeRect = CGRectMake(kScreenWidth-15-30, 385, 30, 30);
//    CGRect buffRect = CGRectMake(15, 385+22+12, kScreenWidth-30, 2);
    CGRect proRect  = CGRectMake(15,  385+22+10, kScreenWidth-30, 16);
    CGRect currRect = CGRectMake(15,  385+22+20, 50, 40);
    CGRect totaRect = CGRectMake(kScreenWidth-15-50,  385+22+20, 50, 40);
    CGRect playRect = CGRectMake(kScreenWidth/2-30,  385+22+50, 60, 60);
    CGRect lastRect = CGRectMake(playRect.origin.x-40-40,  385+22+60, 40, 40);
    CGRect nextRect = CGRectMake(playRect.origin.x+60+40,  385+22+60, 40, 40);
    if (kScreenHeight == 568) {
        lastRect = CGRectMake(playRect.origin.x-40-20,  385+22+60, 40, 40);
        nextRect = CGRectMake(playRect.origin.x+60+20,  385+22+60, 40, 40);
    }
    CGRect rewindRect = CGRectMake(20, 385+22+60, 40, 40);
    CGRect forwardRect = CGRectMake(kScreenWidth-20-40, 385+22+60, 40, 40);
    
    DFPlayerControlManager *manager = [DFPlayerControlManager shareInstance];
    
    //播放列表按钮
    _audiolist_button.frame = listRect;
    
    //播放模式按钮
    [manager df_typeControlBtnWithFrame:typeRect superView:self block:^{
        if ([DFPlayer shareInstance].playMode == DFPlayerModeSingleCycle) {
            [UIView ShowInfo:@"单曲循环" Inview: [self viewController].view];
        } else if ([DFPlayer shareInstance].playMode == DFPlayerModeOrderCycle) {
            [UIView ShowInfo:@"顺序播放" Inview: [self viewController].view];
        } else if ([DFPlayer shareInstance].playMode == DFPlayerModeShuffleCycle) {
            [UIView ShowInfo:@"随机播放" Inview: [self viewController].view];
        }

    }];
    //缓冲条
//    [manager df_bufferProgressViewWithFrame:buffRect trackTintColor:[UIColor colorWithHexString:@"F9F9F9"] progressTintColor:[UIColor colorWithHexString:@"F9F9F9"] superView:self];
    //进度条
    [manager df_sliderWithFrame:proRect minimumTrackTintColor:[UIColor colorWithHexString:@"50D0F4"] maximumTrackTintColor:[UIColor colorWithHexString:@"F9F9F9"] trackHeight:2 thumbSize:(CGSizeMake(16, 16)) superView:self];
    //当前时间
    UILabel *curLabel = [manager df_currentTimeLabelWithFrame:currRect superView:self];
    curLabel.textColor = [UIColor colorWithHexString:@"50D0F4"];
    //总时间
    UILabel *totLabel = [manager df_totalTimeLabelWithFrame:totaRect superView:self];
    totLabel.textColor = [UIColor colorWithHexString:@"50D0F4"];
    
    //播放暂停按钮
    [manager df_playPauseBtnWithFrame:playRect superView:self block:nil];
    //下一首按钮
    [manager df_nextAudioBtnWithFrame:nextRect superView:self block:nil];
    //上一首按钮
    [manager df_lastAudioBtnWithFrame:lastRect superView:self block:nil];
//    快退按钮
    [manager df_rewindBtnWithFrame:rewindRect superView:self block:nil];
//    快进按钮
    [manager df_forwardBtnWithFrame:forwardRect superView:self block:nil];
    
    
    [self getAudiolistArray];

//    加载
    if (![DFPlayer shareInstance].currentAudioModel) {
        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DFPlayerCurrentAudioInfoModelPlayNotiKey" object:nil];
        [[DFPlayer shareInstance] df_audioPlay];
    }

    [_audio_image sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url placeholderImage:ImageName(@"placeholderImage")];
    _TitleLab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
    
    //    if ([DFPlayer shareInstance].currentAudioModel) {
    //    }

}




#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.dataArray.count; i++) {
        AudioModel *yourModel    = self.dataArray[i];
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        if ([[yourModel.audio_path absoluteString] hasPrefix:@"http"]) {//网络音频
            model.audio_path  = yourModel.audio_path;
        }
//        else{//本地音频
//            NSString *path = [[NSBundle mainBundle] pathForResource:[yourModel.audio_path absoluteString] ofType:@""];
//            if (path) {model.audio_path = [NSURL fileURLWithPath:path];}
//        }
        [_df_ModelArray addObject:model];
    }
    return self.df_ModelArray;
}


- (void)getAudiolistArray{
    //创建一个归档文件夹
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *string = [NSString string];
    if (![DFPlayer shareInstance].currentAudioModel) {
        if ([DFPlayer shareInstance].previousAudioModel.subject_id) {
            string = [NSString stringWithFormat:@"archive_4_%ld",[DFPlayer shareInstance].previousAudioModel.subject_id];
        } else {
            string = [NSString stringWithFormat:@"archive_%ld_%ld",[DFPlayer shareInstance].previousAudioModel.audio_type,[DFPlayer shareInstance].previousAudioModel.fk_parent_id];
        }
    } else {
        if ([DFPlayer shareInstance].currentAudioModel.subject_id) {
            string = [NSString stringWithFormat:@"archive_4_%ld",[DFPlayer shareInstance].currentAudioModel.subject_id];
        } else {
            string = [NSString stringWithFormat:@"archive_%ld_%ld",[DFPlayer shareInstance].currentAudioModel.audio_type,[DFPlayer shareInstance].currentAudioModel.fk_parent_id];
        }
    }
    
    if (_isDownloadPaly) {
        string = [NSString stringWithFormat:@"downloadAudio"];
    }
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    _dataArray = array;

    [DFPlayer shareInstance].dataSource = self;
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）

}


#pragma mark - DFPlayer delegate
- (void)df_playerAudioWillAddToPlayQueue:(DFPlayer *)player{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        UIImage *audioImage = player.currentAudioModel.audio_image;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *audioName = player.currentAudioModel.audio_title;
            [_audio_image sd_setImageWithURL:player.currentAudioModel.audio_image_url placeholderImage:ImageName(@"placeholderImage")];
            _TitleLab.text = audioName;

        });
    });
}




- (void)df_playerReadyToPlay:(DFPlayer *)player{
    
    [_listView.tableView reloadData];
    if (self.ChangeAudio) {
        self.ChangeAudio();
    }

//    if (![DFPlayer shareInstance].currentAudioModel.subject_id) {
//        VideoModel *model = [DFPlayer shareInstance].currentAudioModel;
        if ([DFPlayer shareInstance].state == DFPlayerStatePlaying && [DFPlayer shareInstance].currentAudioModel.timeWithValue < 0.95) {
            [[DFPlayer shareInstance] df_seekToTimeWithValue:[DFPlayer shareInstance].currentAudioModel.timeWithValue];
        }
//    }
}

- (IBAction)AudiolistButton:(id)sender {
    
    [self listShow];
}



- (IBAction)rateListButton:(id)sender {
    [self rateListViewShow];
}


- (IBAction)timeListButton:(id)sender {
    [self timeListViewShow];
}


- (void)listShow{
    if (self.maskView.alpha == 0) {
        self.maskView.alpha = 0.7;
        self.listView.alpha = 1;
        [self.listView scrollToCurrentAudio];
    }
}


- (void)rateListViewShow{
    if (self.maskView.alpha == 0) {
        self.maskView.alpha = 0.7;
        self.rateListView.alpha = 1;
    }
}


- (void)timeListViewShow{
    if (self.maskView.alpha == 0) {
        self.maskView.alpha = 0.7;
        self.timeListView.alpha = 1;
        [self.timeListView.tableView reloadData];
    }
}


//所有提示都消失
- (void)listdisappeare{
    self.maskView.alpha = 0;
    if (self.listView.alpha != 0) {
        self.listView.alpha = 0;
    }
    if (self.rateListView.alpha != 0) {
        self.rateListView.alpha = 0;
    }
    if (self.timeListView.alpha != 0) {
        self.timeListView.alpha = 0;
    }
}


- (RateListView *)rateListView{
    if (_rateListView == nil) {
        _rateListView = [[RateListView alloc] initWithFrame:CGRectMake(0, kScreenHeight-kScreenHeight/2-effectViewH, kScreenWidth, kScreenHeight/2)];
        _rateListView.alpha = 0;
        [[self viewController].navigationController.view addSubview:_rateListView];
        __weak typeof(self) Weakself = self;
        _rateListView.clooseBlock = ^{
            [Weakself listdisappeare];

        };
    }
    return _rateListView;
}


- (TimeListView *)timeListView{
    if (_timeListView == nil) {
        _timeListView = [[TimeListView alloc] initWithFrame:CGRectMake(0, kScreenHeight-kScreenHeight/2-effectViewH, kScreenWidth, kScreenHeight/2)];
        _timeListView.alpha = 0;
        [[self viewController].navigationController.view addSubview:_timeListView];
        __weak typeof(self) Weakself = self;
        _timeListView.clooseBlock = ^{
            [Weakself listdisappeare];
        };
    }
    return _timeListView;
}


- (PlayerDetailPopView *)listView{
    if (_listView == nil) {
        _listView = [[NSBundle mainBundle] loadNibNamed:@"PlayerDetailPopView" owner:nil options:nil][0];
        _listView.frame = CGRectMake(0, kScreenHeight-kScreenHeight/2-effectViewH, kScreenWidth, kScreenHeight/2);
        _listView.backgroundColor = [UIColor whiteColor];
        _listView.arrayData = self.dataArray;
        if (_isDownloadPaly) {
            _listView.title_lab.text = @"我的下载";
        }
        __weak typeof(self) Weakself = self;
        _listView.clooseBlock = ^{
            [Weakself listdisappeare];
        };
        _listView.chooseCell = ^(NSInteger index) {
            AudioModel *model = Weakself.df_ModelArray[index];
            if (model.audio_id != [DFPlayer shareInstance].currentAudioModel.audio_id) {
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:model.audioId];
            }
        };
        _listView.alpha = 0;
        [[self viewController].navigationController.view addSubview:_listView];
    }
    return _listView;
}


- (UIView *)maskView{
    if (_maskView == nil) {
        _maskView = [[UIView alloc] initWithFrame:self.window.bounds];
        _maskView.alpha = 0;
        _maskView.backgroundColor = [UIColor colorWithHexString:@"202020"];
        [[self viewController].navigationController.view addSubview:_maskView];
        
        //单击的手势
        UITapGestureRecognizer *tapRecognize = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(listdisappeare)];
        tapRecognize.numberOfTapsRequired = 1;
        tapRecognize.delegate = self;
        [tapRecognize setEnabled :YES];
        [tapRecognize delaysTouchesBegan];
        [tapRecognize cancelsTouchesInView];
        [_maskView addGestureRecognizer:tapRecognize];
    }
    return _maskView;
}


- (void)setAlbumlistModel:(AlbumListModel *)albumlistModel{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,albumlistModel.album_image];
    [_album_image sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:ImageName(@"placeholderImage")];
    _album_title_lab.text = albumlistModel.album_title;
    _album_listen_lab.text = [NSString stringWithFormat:@"%ld人收听",(long)albumlistModel.play_count];
}







@end




