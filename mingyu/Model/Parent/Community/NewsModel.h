//
//  NewsModel.h
//  mingyu
//
//  Created by apple on 2018/8/14.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModel : NSObject


/** 新闻ID */
@property (nonatomic, assign) NSInteger news_id;

/** 用户ID */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 来源ID */
@property (nonatomic, assign) NSInteger fk_from_id;

/** 标题 */
@property (nonatomic, copy) NSString *news_title;

/** 副标题 */
@property (nonatomic, copy) NSString *news_subtitle;

/** 新闻封面图片 */
@property (nonatomic, copy) NSString *news_head;

/** 内容 */
@property (nonatomic, copy) NSString *news_content;

/** 新闻类型 1:资讯 2:育儿必读 3:帖子 */
@property (nonatomic, assign) NSInteger news_type;

/** 浏览量 */
@property (nonatomic, assign) NSInteger page_view;

/** 创建时间 */
@property (nonatomic, copy) NSString *create_time;

/** 发布时间状态 */
@property (nonatomic, copy) NSString *time_status;

/** 收藏ID */
@property (nonatomic, assign) NSInteger is_collect_true;

/** 点赞量 */
@property (nonatomic, assign) NSInteger praise_number;

/** 收藏量 */
@property (nonatomic, assign) NSInteger collect_number;

/** 是否点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 分享链接 */
@property (nonatomic, copy) NSString *share_url;



/** 用户名字 */
@property (nonatomic, copy) NSString *user_name;

/** 用户头像 */
@property (nonatomic, copy) NSString *user_head;

/** 关注状态 */
@property (nonatomic, assign) NSInteger follow_status;


/** 置顶ID */
@property (nonatomic, assign) NSInteger is_top_true;

/** 圈子名称 */
@property (nonatomic, copy) NSString *circle_name;

/** 所属圈子ID */
@property (nonatomic, assign) NSInteger fk_circle_id;



/** 新闻来源 */
@property (nonatomic, copy) NSString *article_source;

/** 新闻来源图标 */
@property (nonatomic, copy) NSString *source_head;


@end
