//
//  HomePageRemcommendOfAlbumCell.h
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubjectModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomePageRemcommendOfAlbumCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *albumView;

/**一键听图片*/
@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
/**曲子名称*/
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;
/**收听人数*/
@property (weak, nonatomic) IBOutlet UILabel *numberOfListenersLabel;

+ (instancetype)theHomePageRemcommendOfAlbumCell:(UITableView *)tableView;

/** model */
@property (nonatomic, strong) SubjectModel *subjectModel;
@end

NS_ASSUME_NONNULL_END
