//
//  ParentRecommenCell.m
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentRecommenCell.h"
#import "UIImageView+AFNetworking.h"

@implementation ParentRecommenCell

+ (instancetype)theParentRecommenCellWithTableView:(UITableView *)tableView{
    static NSString *identify = @"ParentRecommenCell";
    ParentRecommenCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentRecommenCell" owner:self options:nil][0];
        cell.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    }
    return cell;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_user_head_Image.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_user_head_Image.bounds.size];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _user_head_Image.bounds;
    maskLayer.path = maskPath.CGPath;
    _user_head_Image.layer.mask = maskLayer;
    
    _topic_head_Image.contentMode = UIViewContentModeScaleAspectFill;
    _topic_head_Image.layer.cornerRadius = 5;
    _topic_head_Image.layer.borderWidth = 0.5;
    _topic_head_Image.layer.borderColor = [UIColor colorWithHexString:@"e2e2e2"].CGColor;
    _topic_head_Image.layer.masksToBounds = YES;
}


- (void)setNewsmodel:(NewsModel *)newsmodel{
    _newsmodel = newsmodel;
    
    _topic_title_Lab.text = _newsmodel.news_title;
//    如果用户头像存在，显示用户头像；用户头像不存在，显示来源头像
    NSString *headimageURL = [NSString stringWithFormat:@"%@",_newsmodel.user_head ? _newsmodel.user_head:_newsmodel.source_head];
    [_user_head_Image zp_setImageWithURL:headimageURL placeholderImage:ImageName(@"默认头像")];
//    如果用户名称存在，显示用户名称；用户名称不存在，显示来源名称
    _user_name_Lab.text = _newsmodel.user_name ? _newsmodel.user_name : _newsmodel.article_source;
    
    if (_newsmodel.news_head.length > 0) {
        _TitleLabConstraintRight.constant = 134;
        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_newsmodel.news_head];
        [_topic_head_Image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
        _topic_head_Image.hidden = NO;
    } else {
        _TitleLabConstraintRight.constant = 20;
        _topic_head_Image.hidden = YES;
        [_topic_head_Image sd_setImageWithURL:nil placeholderImage:nil];
    }
}


//搜索时用到
- (void)showSearch{
    NSString *title = _newsmodel.news_title;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _topic_title_Lab.attributedText = attrStr;
    _topic_title_Lab.lineBreakMode = NSLineBreakByTruncatingTail;
    
    
    //    NSString *content = _topicmodel.topic_abstract;
    //    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
    //    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    //    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    //    content = [content stringByAppendingString:@"</font>"];
    //
    //    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    _topic_abstract_Lab.attributedText = attrStr2;
    //    _topic_abstract_Lab.lineBreakMode = NSLineBreakByTruncatingTail;
    
}

- (IBAction)DisLikeButton:(id)sender {
    if (self.dislikeblock) {
        self.dislikeblock(_newsmodel);
    }
}


-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}

@end
