//
//  ChildListenTableViewCell.h
//  TZWY
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MoreBlock)(NSInteger index);

@interface ChildListenTableViewCell : UITableViewCell



@property (nonatomic,copy) MoreBlock block;

@property (weak, nonatomic) IBOutlet UILabel *ClasssLab;

@property (weak, nonatomic) IBOutlet UIButton *MoreButton;

@property (weak, nonatomic) IBOutlet UIImageView *IconImage;

@property (weak, nonatomic) IBOutlet UILabel *Titlelab;

@property (weak, nonatomic) IBOutlet UILabel *DescribeLab;

@property (weak, nonatomic) IBOutlet UILabel *TimeLab;

@property (weak, nonatomic) IBOutlet UILabel *BuyNumLab;

@property (weak, nonatomic) IBOutlet UILabel *MoneyLab;

@property (nonatomic,assign) NSInteger index;


@end
