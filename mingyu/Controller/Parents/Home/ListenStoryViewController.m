//
//  ListenStoryViewController.m
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ListenStoryViewController.h"
#import "ListenStoreDetailViewController.h"

@interface ListenStoryViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *headerArray;

@property (nonatomic, strong) NSMutableArray *ArrayData;

@property (nonatomic, strong) NSArray *headerText;


@end

@implementation ListenStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    _headerText = @[@"早教育儿",@"时尚美妆",@"亲子家教",@"婚姻生活"];
    [self.view addSubview:self.tableView];
    
    [self getListenBookList];
}


- (void)getListenBookList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetListenBookList];
    NSDictionary *parameters = @{
                                 @"book_type":@"0",
                                 @"start":@"0",
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                NSNumber *book_type = dic[@"book_type"];
                if (![self.headerArray containsObject:book_type]) {
                    [_headerArray addObject:book_type];
                }
            }
            for (int i = 0; i < self.headerArray.count; i++) {
                NSMutableArray * array2 = [NSMutableArray new];
                for (NSDictionary *dic2 in data) {
                    NSNumber *book_type = dic2[@"book_type"];
                    if (_headerArray[i] == book_type) {
                        BookListModel *model = [BookListModel modelWithJSON:dic2];
                        [array2 addObject:model];
                    }
                }
                [self.ArrayData addObject:array2];
            }
            
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.headerArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.ArrayData[section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    ListenStoryCell *headerview = [ListenStoryCell theHeaderView];
    headerview.head_title_lab.text = _headerText[section];
//    __block typeof(self) WeakSelf = self;
    headerview.MoreButtonBlock = ^{
        BookTypeViewController *vc = [[BookTypeViewController alloc] init];
        vc.booktype = _headerArray[section];
        vc.title = _headerText[section];
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    return headerview;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithHexString:@"f9F9F9"];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return 10;  
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ListenStoryCell *cell = [ListenStoryCell theCellWithTableView:tableView];
    if (self.ArrayData && self.ArrayData.count > indexPath.section) {
        BookListModel *model = [self.ArrayData[indexPath.section] objectAtIndex:indexPath.row];
        cell.booklistmodel = model;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.ArrayData && self.ArrayData.count > indexPath.section) {
        BookListModel *model = [self.ArrayData[indexPath.section] objectAtIndex:indexPath.row];
        ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
        VC.target_id = model.book_id;
        VC.audio_type = 2;
        [self.navigationController pushViewController:VC animated:YES];
    }
}



- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 120;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NSMutableArray *)ArrayData{
    if (_ArrayData == nil) {
        _ArrayData = [NSMutableArray array];
    }
    return _ArrayData;
}


- (NSMutableArray *)headerArray{
    if (_headerArray == nil) {
        _headerArray = [NSMutableArray array];
    }
    return _headerArray;
}
@end
