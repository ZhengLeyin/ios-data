//
//  CommentViewController.h
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "CommentModel.h"

@interface CommentViewController : UIViewController

@property (nonatomic, assign) NSInteger targetID;

@property (nonatomic, assign) GetAndAddCommentType comment_type;

@property (nonatomic, strong) CommentModel *targetCommentmodel;


@property (nonatomic, copy) void (^CommentListBlock)(NSMutableArray *commentListArray, NSInteger comment_number);


@end
