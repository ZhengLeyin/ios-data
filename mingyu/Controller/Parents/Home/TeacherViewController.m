//
//  TeacherViewController.m
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TeacherViewController.h"
#import "teacherHeadView.h"
#import "TeacherCourseViewController.h"
#import "TeacherTopicViewController.h"
#import "TeacherModel.h"
/**精品课的短视频*/
#import "ShortVideoOfExcellentCourseViewController.h"
#import "TitleNavigationView.h"
#import "PrivateMessageDetailViewController.h"
#import "JXCategoryView.h"
#import "JXPagerView.h"
#import "CourseAudioViewController.h"

@interface TeacherViewController ()<JXCategoryViewDelegate,JXPagerViewDelegate>
{
    UIImageView *userImageView;
    UILabel *teacherNameLabel;
}
@property (nonatomic, strong) teacherHeadView *teacherheadview;

@property (nonatomic, strong) UIView *titleNavigationView;

@property (nonatomic, strong) UIButton *attention_button;

@property (nonatomic, strong) TeacherModel *teachermodel;

@property (nonatomic, strong) UIButton *sendMessageButton;

@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property (nonatomic, strong) JXPagerView *pagerView;

@property (nonatomic, strong) NSArray *listViewArray;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, assign) NSInteger type;
@end

@implementation TeacherViewController


-(UIView *)titleNavigationView {
    if (!_titleNavigationView) {
        _titleNavigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 44)];
    }
    return _titleNavigationView;
}

-(UIButton *)sendMessageButton {
    if (!_sendMessageButton) {
        _sendMessageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _sendMessageButton.frame = CGRectMake(0, kScreenHeight- effectViewH - 46 - NaviH, kScreenWidth, 46);
        _sendMessageButton.backgroundColor = [UIColor whiteColor];
        _sendMessageButton.titleLabel.font = FontSize(15.0);
        [_sendMessageButton setTitleColor:[UIColor colorWithHexString:@"#50D0F4"] forState:UIControlStateNormal];
        [_sendMessageButton setImage:[UIImage imageNamed:@"私信"] forState:(UIControlStateNormal)];
        [_sendMessageButton setTitle:@"  发私信" forState:(UIControlStateNormal)];
    }
    return _sendMessageButton;
}

- (void)titleNavigationViewUI {
    self.navigationItem.titleView = self.titleNavigationView;
  
    userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 22 - 15, 30, 30)];
    userImageView.backgroundColor = [UIColor whiteColor];
    [_titleNavigationView addSubview:userImageView];
    userImageView.layer.cornerRadius = 15;
    userImageView.layer.masksToBounds = YES;
    
    teacherNameLabel = [UILabel new];
    teacherNameLabel.frame = CGRectMake(CGRectGetMaxX(userImageView.frame) + 13, 22 - 11, 100, 22) ;
    [_titleNavigationView addSubview:teacherNameLabel];
    teacherNameLabel.font = FontSize(16.0);
    
    _attention_button = [UIButton buttonWithType:UIButtonTypeCustom];
    _attention_button.frame = CGRectMake(CGRectGetMaxX(teacherNameLabel.frame), 22 - 12, 52, 24);
    //[_attention_button setTitle:@"＋ 关注" forState:(UIControlStateNormal)];
    //[_attention_button setTitleColor:[UIColor colorWithHexString:@"#50D0F4"] forState:(UIControlStateNormal)];
    [_titleNavigationView addSubview:self.attention_button];
    self.attention_button.titleLabel.font = FontSize(10.0);
    self.titleNavigationView.hidden = YES;
    
    if (_coursemodel.fk_user_id == [USER_ID integerValue]) {
        _attention_button.hidden = YES;
    } else {
        _attention_button.hidden = NO;
    }
}


-(void)titleNavigationViewModel {
    [userImageView zp_setImageWithURL:_teachermodel.user_head placeholderImage:ImageName(@"默认头像")];
    teacherNameLabel.text = _teachermodel.teacher_name;
    [self changeAttentionButton:_teachermodel];
    
    [[_attention_button rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self changeAttention:_teachermodel];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self titleNavigationViewUI];
   // [self.view addSubview:self.teacherheadview];
    if (_coursemodel.fk_user_id == [USER_ID integerValue]) {
        _type = 0;
    } else {
        _type = 1;
        [self.view addSubview:self.sendMessageButton];
    }
    
    [self getTeacherMessage];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}

- (void)initViewControllers {
    
    __weak typeof(self) weakSelf = self;
    NSArray *titles = [NSArray array];
    if (_teachermodel.is_post_video > 0) {
        titles = @[@"课程", @"视频",@"关注", @"粉丝"];
    }else {
        titles = @[@"课程", @"关注", @"粉丝"];
    }
    /**课程视频界面*/
    TeacherCourseViewController *teacherCourseVC = [TeacherCourseViewController new];
    teacherCourseVC.type = _type;
    teacherCourseVC.coursemodel = _coursemodel;
    #pragma mark 跳转老师详情
    teacherCourseVC.pushTeacherDetailsBlock = ^(CourseModel *coursemodel) {
        TeacherViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"TeacherViewController"];
        VC.coursemodel = coursemodel;
        [weakSelf.navigationController pushViewController:VC animated:YES];
    };
    #pragma mark 跳转音频,视频界面
    teacherCourseVC.courseCagetoryBlock = ^(CourseModel *coursemodel) {
        if (coursemodel.course_type == 1) {
            CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
            VC.coursemodel = coursemodel;
            [weakSelf.navigationController pushViewController:VC animated:YES];
        } else{
            CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
            VC.coursemodel = coursemodel;
            [weakSelf.navigationController pushViewController:VC animated:YES];
        }
    };
    
    /**视频界面*/
    ShortVideoOfExcellentCourseViewController *eliteVC = [ShortVideoOfExcellentCourseViewController new];
    eliteVC.type = _type;
    eliteVC.coursemodel = _coursemodel;
    //跳转视频详情
    eliteVC.PushBlock = ^(UIViewController * _Nonnull vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    /**关注*/
    MineAttentionViewController *AttentionVC = [[MineAttentionViewController alloc] init];
    AttentionVC.from_id = _coursemodel.fk_user_id;
    AttentionVC.type = _type;
    //跳转关注页面
    AttentionVC.PushBlock = ^(UIViewController *vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    /**粉丝*/
    MineFansViewController *FansVC = [[MineFansViewController alloc] init];
    FansVC.from_id = _coursemodel.fk_user_id;
    FansVC.type = _type;
    //跳转粉丝页面
    FansVC.PushBlock = ^(UIViewController *vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    if (_teachermodel.is_post_video > 0) {
          _listViewArray = @[teacherCourseVC, eliteVC, AttentionVC ,FansVC];
        
    }else {
        _listViewArray = @[teacherCourseVC, AttentionVC ,FansVC];
    }
  
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    self.categoryView.titles = titles;
    self.categoryView.titleLabelZoomScale = 1;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = [UIColor colorWithHexString:@"50D0F4"];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"767676"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorLineViewColor = [UIColor colorWithHexString:@"50D0F4"];
    lineView.indicatorLineWidth = 30;
    self.categoryView.indicators = @[lineView];
    
    _pagerView = [self preferredPagingView];
    self.pagerView.frame = CGRectMake(0,0, kScreenWidth, kScreenHeight-NaviH);
    [self.view addSubview:self.pagerView];
    [self.view bringSubviewToFront:self.sendMessageButton];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-effectViewH-NaviH, kScreenWidth, effectViewH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    self.categoryView.contentScrollView = self.pagerView.listContainerView.collectionView;
    
    //扣边返回处理，下面的代码要加上
    [self.pagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    [self.pagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    [[self.sendMessageButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        PrivateMessageDetailViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PrivateMessageDetailViewController"];
        VC.user_id = _teachermodel.fk_user_id;
        VC.user_head = _teachermodel.user_head;
        VC.title = _teachermodel.teacher_name;
        [self.navigationController pushViewController:VC animated:YES];
    }];
}

#pragma mark 获取老师详情信息
- (void)getTeacherMessage {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetTeacherMessage];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"t_user_id":@(_coursemodel.fk_user_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _teachermodel = [TeacherModel modelWithJSON:json[@"data"]];
            self.teacherheadview.teachermodel = _teachermodel;
            CGFloat H = [self calculateString:_teachermodel.teacher_profile Width:12];
            _teacherheadview.height = 180+H;
            [self titleNavigationViewModel];
            #pragma mark 先获取参数在判断短视频
            [self initViewControllers];
           
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-40, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.height;
}

- (UIView *)headerView{
    return self.teacherheadview;
}


- (teacherHeadView *)teacherheadview{
    if (_teacherheadview == nil) {
        _teacherheadview = [[NSBundle mainBundle] loadNibNamed:@"teacherHeadView" owner:nil options:nil][0];
        if (kScreenWidth == 320.0) {
            _teacherheadview.frame = CGRectMake(0, 0, kScreenWidth, 120);
        }
    
    }
    return _teacherheadview;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)changeAttentionButton:(TeacherModel *)teachermodel {
    _attention_button.layer.cornerRadius = 3;
    _attention_button.layer.borderWidth = 1;
    if (_teachermodel.follow_status == 1) {
        _attention_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_attention_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_attention_button setTitle:@" + 关注 " forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
//        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
//        [_attention_button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
//        [_attention_button setTitle:@"+关注" forState:UIControlStateNormal];
    } else if (_teachermodel.follow_status == 2){
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"已关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    } else {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"互相关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    }
}

-(void)changeAttention:(TeacherModel *)teachermodel {
    _attention_button.userInteractionEnabled = NO;
    if (_teachermodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.fk_user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _teachermodel.follow_status = follow_status;
                _teacherheadview.teachermodel = _teachermodel;
                [self changeAttentionButton:_teachermodel];
            } else {
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
            //            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.fk_user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _teachermodel.follow_status = 1;
                _teacherheadview.teachermodel = _teachermodel;
                [self changeAttentionButton:_teachermodel];
            } else {
                //                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
            //            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}



- (JXPagerView *)preferredPagingView {
    return [[JXPagerView alloc] initWithDelegate:self];
}

#pragma mark - JXPagerViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.teacherheadview;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return self.teacherheadview.height;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 40;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSArray<id<JXPagerViewListViewDelegate>> *)listViewsInPagerView:(JXPagerView *)pagerView {
    return self.listViewArray;
}


- (void)mainTableViewDidScroll:(UIScrollView *)scrollView{
    CGFloat  offsetY = scrollView.contentOffset.y;
    if (offsetY > 80) {
        self.titleNavigationView.hidden = NO;
        [self titleNavigationViewModel];
    }else if (offsetY < 79) {
        self.titleNavigationView.hidden = YES;
    }else {
        
    }
}

@end
