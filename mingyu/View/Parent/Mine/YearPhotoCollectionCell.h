//
//  YearPhotoCollectionCell.h
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YearPhotoCollectionCell : UICollectionViewCell


@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UIImageView *selectImage;

@end
