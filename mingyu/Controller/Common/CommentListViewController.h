//
//  CommentListViewController.h
//  mingyu
//
//  Created by apple on 2018/4/23.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentListViewController : UIViewController

@property (nonatomic, assign) NSInteger targetID;    //目标ID

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) void (^CommentListBlock)(NSMutableArray *commentListArray, NSInteger comment_number);

@end


