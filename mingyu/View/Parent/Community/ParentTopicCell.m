//
//  ParentTopicCell.m
//  mingyu
//
//  Created by apple on 2018/4/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentTopicCell.h"

@implementation ParentTopicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

+ (instancetype)theUserinfoCell{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentTopicCell" owner:nil options:nil][0];
}

+ (instancetype)theTopicHeaderCell{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentTopicCell" owner:nil options:nil][1];
}

+ (instancetype)theCollertAndPraisCell{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentTopicCell" owner:nil options:nil][2];
}

+ (instancetype)theAboutTopicCell{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentTopicCell" owner:nil options:nil][3];
}


//- (void)setuserinfocellWithModel:(TopicInfoModel *)topicinfomodel{
//    _topicinfomodel = topicinfomodel;
////    _TitleLab.text = topicinfomodel.topic_title;
//    
//    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_user_head_Image.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_user_head_Image.bounds.size];
//    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
//    maksLayer.frame = _user_head_Image.bounds;
//    maksLayer.path = maksPath.CGPath;
//    _user_head_Image.layer.mask = maksLayer;
//    
//    [self changefollowButtonState:topicinfomodel];
//    
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,topicinfomodel.user_head];
//    [_user_head_Image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:nil];
//    _user_name_Lab.text = topicinfomodel.user_name;
//    _follow_number_Lab.text = [NSString stringWithFormat:@"%ld人关注",topicinfomodel.follow_number ];
//}
//
////根据状态判断是否已关注
//- (void)changefollowButtonState:(TopicInfoModel*)topicinfomodel{
//    _follow_Button.layer.cornerRadius = 3;
//    _follow_Button.layer.borderWidth = 1;
//    if (topicinfomodel.follow_status == 1) {
//        _follow_Button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
//        [_follow_Button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
//        [_follow_Button setTitle:@"  +关注  " forState:UIControlStateNormal];
//    } else if (topicinfomodel.follow_status == 2){
//        _follow_Button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
//        [_follow_Button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
//        [_follow_Button setTitle:@"  已关注  " forState:UIControlStateNormal];
//    } else {
//        _follow_Button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
//        [_follow_Button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
//        [_follow_Button setTitle:@"互相关注" forState:UIControlStateNormal];
//    }
//}
//
//
//- (void)setTopicinfomodel:(TopicInfoModel *)topicinfomodel{
//    _topicinfomodel = topicinfomodel;
//    
//    [self changTopButtonState:_topicinfomodel];
//    
//    _topic_time_Lab.text = [NSString compareTimeForNow:[_topicinfomodel.topic_time integerValue]];
//    
//    [self changefollowButtonState:_topicinfomodel];
//    
//    [self changCollectButtonState:_topicinfomodel];
//    
//    [self changPraiseButtonState:_topicinfomodel];
//}
//
//
//
////根据状态判断是否已置顶
//- (void)changTopButtonState:(TopicInfoModel *)topicinfommodel{
//    
//    _topButton.layer.cornerRadius = 5;
//    _topButton.layer.borderWidth = 1;
//    
//    if (topicinfommodel.is_top_true == 0) {
//        _topButton.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
//        [_topButton setTitle:@"  置顶该帖  " forState:UIControlStateNormal];
//        [_topButton setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
//    } else {
//        _topButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
//        [_topButton setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
//        [_topButton setTitle:@"  已置顶  " forState:UIControlStateNormal];
//    }
//}
//
////根据状态判断是否收藏
//- (void)changCollectButtonState:(TopicInfoModel *)topinfomodel{
//    //收藏
//    if (_topicinfomodel.is_collect_true == 0) {
//        [_collect_Button setImage:ImageName(@"收藏") forState:UIControlStateNormal];
//    } else {
//        [_collect_Button setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
//    }
//    [_collect_Button setTitle:[NSString stringWithFormat:@"  收藏(%ld)  ",_topicinfomodel.collect_number] forState:UIControlStateNormal];
//}
//
////根据状态判断是否点赞
//- (void)changPraiseButtonState:(TopicInfoModel *)topinfomodel{
//    //点赞
//    if (_topicinfomodel.click_praise_true == 0) {
//        [_praise_button setImage:ImageName(@"点赞") forState:UIControlStateNormal];
//    } else {
//        [_praise_button setImage:ImageName(@"已赞") forState:UIControlStateNormal];
//    }
//    [_praise_button setTitle:[NSString stringWithFormat:@"  赞(%ld)  ",_topicinfomodel.praise_number] forState:UIControlStateNormal];
//}
//
////关注与取消关注
//- (IBAction)followButton:(UIButton *)sender {
//    if (_topicinfomodel.follow_status == 1) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
//        NSDictionary *parameters = @{
//                                     @"user_id":USER_ID,
//                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_topicinfomodel.fk_user_id]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger follow_status = [json[@"data"] integerValue];
//                _topicinfomodel.follow_status = follow_status;
//                [self changefollowButtonState:_topicinfomodel];
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else{
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
//        NSDictionary *parameters = @{
//                                     @"user_id":USER_ID,
//                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_topicinfomodel.fk_user_id]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                _topicinfomodel.follow_status = 1;
//                [self changefollowButtonState:_topicinfomodel];
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    }
//}
//
//
//
////置顶取消置顶
//- (IBAction)topButton:(UIButton *)sender {
//    if (_topicinfomodel.is_top_true == 0) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KSetTop];
//        NSDictionary *parameters = @{
//                                     @"topic_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.topic_id],
//                                     @"user_id":USER_ID
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger is_top_true = [json[@"data"] integerValue];
//                _topicinfomodel.is_top_true = is_top_true;
//                [self changTopButtonState:_topicinfomodel];
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KCancelTop];
//        NSDictionary *parameters = @{
//                                     @"top_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.is_top_true]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                _topicinfomodel.is_top_true = 0;
//                [self changTopButtonState:_topicinfomodel];
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    }
//}
//
//
////收藏帖子
//- (IBAction)collectButton:(UIButton *)sender {
//    
//    if (_topicinfomodel.is_collect_true == 0) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
//        NSDictionary *parameters = @{
//                                     @"from_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.topic_id],
//                                     @"user_id":USER_ID,
//                                     @"collect_type":@(collect_type_news)
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger is_collect_true = [json[@"data"] integerValue];
//                _topicinfomodel.is_collect_true = is_collect_true;
//                _topicinfomodel.collect_number++;
//                [self changCollectButtonState:_topicinfomodel];
//                
//                [UIView ShowInfo:TipCollectSuccess Inview:[self viewController].view];
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
//        NSDictionary *parameters = @{
//                                     @"collect_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.is_collect_true]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                _topicinfomodel.is_collect_true = 0;
//                _topicinfomodel.collect_number--;
//                [self changCollectButtonState:_topicinfomodel];
//                [UIView ShowInfo:TipUnCollectSuccess Inview:[self viewController].view];
//
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    }
//}
//
//
////帖子点赞
//- (IBAction)praiseButton:(UIButton *)sender {
//    
//    if (_topicinfomodel.click_praise_true == 0) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
//        NSDictionary *parameters = @{
//                                     @"from_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.topic_id],
//                                     @"user_id":USER_ID,
//                                     @"praise_type":@"2"
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger click_praise_true = [json[@"data"] integerValue];
//                _topicinfomodel.click_praise_true = click_praise_true;
//                _topicinfomodel.praise_number++;
//                [self changPraiseButtonState:_topicinfomodel];
//                [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];
//
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else {
////        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelClickPraise];
////        NSDictionary *parameters = @{
////                                     @"praise_id":[NSString stringWithFormat:@"%ld", _topicinfomodel.click_praise_true]
////                                     };
////        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
////            NSDictionary *json = responseObject;
////            ZPLog(@"%@\n%@",json,json[@"message"]);
////            BOOL success = [json[@"success"] boolValue];
////            if (success) {
////                _topicinfomodel.click_praise_true = 0;
////                _topicinfomodel.praise_number--;
////                [self changPraiseButtonState:_topicinfomodel];
////            } else {
////                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
////            }
////        } failure:^(NSError *error) {
////            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
////        }];
//        [UIView ShowInfo:@"请勿重复点赞" Inview:[self viewController].view];
//    }
//}


- (IBAction)AboutTopicButton:(UIButton *)sender {
    
}

@end
