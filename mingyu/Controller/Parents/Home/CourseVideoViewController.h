//
//  CourseVideoViewController.h
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LWXDetailController.h"

@class CourseModel;

@interface CourseVideoViewController : UIViewController

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CourseModel *coursemodel;


@end
