//
//  ParentCourseViewController.m
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentCourseViewController.h"
#import "CourseBaseViewController.h"
#import "JMColumnMenu.h"
#import "AdvertisementModel.h"
#import "ExcellentClassModel.h"
#import "CustomTitleView.h"
#define pageMenuH 50
#define bannerH  kScale(90)
#define scrollViewHeight (kScreenHeight-pageMenuH-NaviH)

@interface ParentCourseViewController ()<SPPageMenuDelegate, UIScrollViewDelegate,JMColumnMenuDelegate,SDCycleScrollViewDelegate> {
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;
    int i;
}
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@property (nonatomic, assign) NSInteger toIndex;
@property (nonatomic, strong) UIView *bannerView;
/** 一级目录总的数据 */
@property (nonatomic, strong) NSMutableArray *arrayAllData;
/** 一级目录已添加标签数组 */
@property (nonatomic, strong) NSMutableArray *arrayMenuNameAgeRelatedData;
/** 一级目录其余标签未添加数组 */
@property (nonatomic, strong) NSMutableArray *arrayMenuNameAllRestData;
/** 一级目录其余标签未添加数组 */
@property (nonatomic, strong) NSMutableArray *arrayMenuNameRestData;
/** titleBackView */
@property (nonatomic, strong) UIView *titleBackView;
/** titleViewButton */
@property (nonatomic, strong)UIButton *titleViewButton;

/** 和年龄段相关不能删除的数组 */
@property (nonatomic, strong) NSMutableArray *arrayAgeData;
@end

@implementation ParentCourseViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shrinkNotification) name:NSNotificationShrinkNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(expandNotification) name:NSNotificationExpandNotification object:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.bannerView];
    /** banner位 */
    [self setSDCycleScrollViewUI];
    /**获取已添加的一级目录*/
    [self getAppMeunAgeRelatedType];
    /**获取未添加的一级目录*/
    [self getAppMeunTypeEditMenu];
    /**设置rightItem*/
    [self setRightNavigationItem];
}

/** 搜索🔍 */
-(void)setRightNavigationItem {
    UIImage *rightImage = [[UIImage imageNamed:@"ExcellentClass搜索icon"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(SearchButton)];
}

//监听回调
-(void)clickRightBarButtonItem {
    ZPLog(@"监听");
}

- (void)setNavigationbar {
    _titleViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
     _titleViewButton.userInteractionEnabled = YES;
    [_titleViewButton setBackgroundColor:[UIColor colorWithHexString:@"F8F9FB"]];
    _titleViewButton.frame = CGRectMake(0, 0 , kScreenWidth - 60, 30);
    _titleViewButton.layer.cornerRadius = 15;
    _titleViewButton.layer.masksToBounds = YES;
    [_titleViewButton setImage:ImageName(@"搜索icon") forState:UIControlStateNormal];
    [_titleViewButton setTitle:@" 想找课程/专家…" forState:UIControlStateNormal];
    [_titleViewButton setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
    _titleViewButton.titleLabel.font = FontSize(12);
    [_titleViewButton addTarget:self action:@selector(SearchButton) forControlEvents:UIControlEventTouchUpInside];
    
    _titleBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth - 60, 30)];
    [_titleBackView addSubview:self.titleViewButton];
    self.navigationItem.titleView = self.titleBackView;
}

//搜索
- (void)SearchButton {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchCourse_type;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark 设置banner
-(void)setSDCycleScrollViewUI {
    cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth, _bannerView.height) delegate:self placeholderImage:ImageName(@"placeholderImage")];
    cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    cycleView.pageDotImage = [UIImage imageWithColor:[UIColor colorWithHexString:@"ffffff" alpha:0.5] forSize:CGSizeMake(20, 3)];
    cycleView.backgroundColor = [UIColor whiteColor];
    // 一个Category给用颜色做ImageView 用15宽2高做一个长方形图片 当前图片
    cycleView.currentPageDotImage = [UIImage imageWithColor:[UIColor whiteColor] forSize:CGSizeMake(15, 2)];
    [_bannerView addSubview:cycleView];
    [self getAdvertisement];
}

#pragma mark /** banner 收缩 */
-(void)shrinkNotification {
    [UIView animateWithDuration:0.4 animations:^{
        
        self.bannerView.frame = CGRectMake(0, 0, kScreenWidth, 0);
        self.bannerView.hidden = YES;
        self.pageMenu.frame = CGRectMake(0, CGRectGetMaxY(_bannerView.frame),  kScreenWidth, pageMenuH);
        _scrollView.frame = CGRectMake(0, CGRectGetMaxY(_pageMenu.frame), kScreenWidth, scrollViewHeight);
    }];
    
    //self.navigationItem.titleView.hidden = NO;
    self.navigationItem.rightBarButtonItem = nil;
    [self setNavigationbar];
    
}

#pragma mark /** banner 扩张 */
-(void)expandNotification {
    [UIView animateWithDuration:0.4 animations:^{
        self.bannerView.hidden = NO;
        self.bannerView.frame = CGRectMake(0, 0, kScreenWidth,  kScale(90));
        self.pageMenu.frame = CGRectMake(0, CGRectGetMaxY(_bannerView.frame),  kScreenWidth, pageMenuH);
        _scrollView.frame = CGRectMake(0, CGRectGetMaxY(_pageMenu.frame), kScreenWidth, scrollViewHeight);
    }];
     _titleViewButton.frame = CGRectMake(0, 0 , kScreenWidth - 100, 30);
     _titleBackView.frame = CGRectMake(0, 0, kScreenWidth - 100, 30);
    [self setRightNavigationItem];
    //self.navigationItem.titleView.hidden = YES;
    [_titleViewButton setImage:nil forState:UIControlStateNormal];
    [self.titleViewButton setTitle:@"精品课" forState:(UIControlStateNormal)];
    [self.titleViewButton setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    self.titleViewButton.titleLabel.font = FontSize(18.0);
    self.titleViewButton.backgroundColor = [UIColor clearColor];
    self.titleViewButton.userInteractionEnabled = NO;
    
   // [self.titleBackView removeAllSubviews];
   // [self.navigationItem.titleView removeAllSubviews];
    //self.navigationItem.title = @"精品课";
}

- (void)setuppageMenu {
    [self.view addSubview:self.pageMenu];
    if (_toIndex > [self.arrayMenuNameAgeRelatedData count]-1 ) {
        _toIndex = 0;
    }
    [_pageMenu setItems:self.arrayMenuNameAgeRelatedData selectedItemIndex:_toIndex];
    for (int i = 0; i < self.arrayMenuNameAgeRelatedData.count; i++) {
        CourseBaseViewController *baseVc = [[CourseBaseViewController alloc] init];
        ExcellentClassModel *model = self.arrayAllData[i];
        baseVc.meunId = model.menu_id;
        [self addChildViewController:baseVc];
        // 控制器本来自带childViewControllers,但是遗憾的是该数组的元素顺序永远无法改变，只要是addChildViewController,都是添加到最后一个，而控制器不像数组那样，可以插入或删除任意位置，所以这里自己定义可变数组，以便插入(删除)(如果没有插入(删除)功能，直接用自带的childViewControllers即可)
        [self.myChildViewControllers addObject:baseVc];
    }
    
    [self.view addSubview:self.scrollView];
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = _scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.myChildViewControllers.count) {
        CourseBaseViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:baseVc.view];
        baseVc.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth, scrollViewHeight);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.arrayMenuNameAgeRelatedData.count*kScreenWidth, 0);
    }
//    [self.view layoutIfNeeded];
//    [self.view layoutSubviews];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-effectViewH-NaviH, kScreenWidth, effectViewH)];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = 1000;
    if ([[self.view viewWithTag:1000] isKindOfClass:[UIView class]] == NO) {
        [self.view addSubview:view];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark 获取所属年龄段与用户已添加的分类标签
-(void)getAppMeunAgeRelatedType {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KAppMenuType,@(5)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
             //ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            [self.arrayMenuNameAgeRelatedData removeAllObjects];
            [self.arrayAllData removeAllObjects];
            [self.arrayAgeData removeAllObjects];
            for (NSDictionary *dic in data) {
                @autoreleasepool {
                    ExcellentClassModel *model = [ExcellentClassModel modelWithJSON:dic];
                    [self.arrayAllData addObject:model];
                    [self.arrayMenuNameAgeRelatedData addObject:model.menu_name];
                    if (model.is_ban == 1) {
                        [self.arrayAgeData addObject:[NSString stringWithFormat:@"%ld",model.is_ban]];
                    }
                }
            }
            if (self.arrayMenuNameAgeRelatedData && self.arrayMenuNameAgeRelatedData.count > 0) {
                [self setuppageMenu];
            }
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

#pragma mark 获取其余标签
-(void)getAppMeunTypeEditMenu {
    NSString  *editMenuURL = @"/editMenu";
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@%@?",KURL,KAppMenuType,@(5),editMenuURL];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        //ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            [self.arrayMenuNameAllRestData removeAllObjects];
            [self.arrayMenuNameRestData removeAllObjects];
            for (NSDictionary *dic in data) {
                @autoreleasepool {
                    ExcellentClassModel *model = [ExcellentClassModel modelWithJSON:dic];
                    [self.arrayMenuNameAllRestData addObject:model];
                    [self.arrayMenuNameRestData addObject:model.menu_name];
                    //ZPLog(@"剩余=%@",model.menu_name);
                }
            }
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];

}

#pragma mark 添加标签
-(void)getUserMenu:(NSUInteger )menu_id {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KUserMenu];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"menu_id":@(menu_id),
                                 @"type":@5,
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            //[SVProgressHUD showSuccessWithStatus:@"栏目添加成功"];
            [SVProgressHUD dismiss];
              //ZPLog(@"添加成功%ld",menu_id);
        } else {
             [SVProgressHUD showErrorWithStatus:@"栏目添加失败"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"栏目添加失败,请稍后重试..."];
    }];
}

#pragma mark 删除添加标签
-(void)getDeleteUserMenu:(NSUInteger )menu_id {

    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUserMenu];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"menu_id":@(menu_id),
                                 };
    [HttpRequest deleteWithURLString:URL parameters:parameters viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [SVProgressHUD dismiss];
            //ZPLog(@"删除成功%ld",menu_id);
        } else {
            [SVProgressHUD showErrorWithStatus:@"栏目删除失败"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"栏目删除失败,请稍后重试..."];
    }];
}

#pragma mark 广告位
- (void)getAdvertisement {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAdvertisement];
    NSDictionary *parameters = @{
                                 @"ad_type":@"3",
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        //        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            advertisementArray = [NSMutableArray array];
            NSMutableArray *imageArray = [NSMutableArray array];
            for (NSDictionary *dic in data) {
                @autoreleasepool {
                    AdvertisementModel *model = [AdvertisementModel modelWithJSON:dic];
                    [advertisementArray addObject:model];
                    [imageArray addObject:[NSString stringWithFormat:@"%@%@",KKaptcha,model.ad_url]];
                }
                
            }
            cycleView.imageURLStringsGroup = imageArray;
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

#pragma mark SDCycleScrollViewDeletegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
//    if (advertisementArray && advertisementArray.count > 0) {
//        AdvertisementModel *model = advertisementArray[index];
//        if (model.to_type == 1) {
//            //1- 一键听 2- 育儿必读 3- 推荐 4- 视频 5-圈子 6- 时光印记
//            [self.navigationController pushViewController:[HandPickViewController new] animated:YES];
//        } else if (model.to_type == 2){
//            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
//            VC.title = @"育儿必读";
//            [self.navigationController pushViewController:VC animated:YES];
//        } else if (model.to_type == 3){
//            self.tabBarController.selectedIndex = 2;
//        } else if (model.to_type == 4){
//            self.tabBarController.selectedIndex = 1;
//        } else if (model.to_type == 5){
//
//        } else if (model.to_type == 6){
//            if (![HttpRequest NetWorkIsOK]) {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                [alert show];
//                return;
//            }
//            TimeRecordViewController *VC = [TimeRecordViewController new];
//            [self.navigationController pushViewController:VC animated:YES];
//        }
//    }
}

#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _toIndex = toIndex;
    //ZPLog(@"%zd------->%zd",fromIndex,toIndex);
//    if (self.arrayAllData && self.arrayAllData.count > 0) {
//        ExcellentClassModel *model = self.arrayAllData[toIndex];
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//        [dic setObject:[NSString stringWithFormat:@"%ld",model.menu_id] forKey:@"menu_id"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"zlyHHA" object:nil userInfo:dic];
//    }

    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    if (self.myChildViewControllers.count <= toIndex) {return;}
    
    UIViewController *targetViewController = self.myChildViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([targetViewController isViewLoaded]) return;
    
    targetViewController.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth, scrollViewHeight);
    [_scrollView addSubview:targetViewController.view];
    
}

- (void)pageMenu:(SPPageMenu *)pageMenu functionButtonClicked:(UIButton *)functionButton{
    NSMutableArray *allCourseAlab = [NSMutableArray arrayWithCapacity:100];
    [allCourseAlab addObjectsFromArray:self.arrayMenuNameAgeRelatedData];
    [allCourseAlab addObjectsFromArray:self.arrayMenuNameRestData];

    NSMutableSet *set1 = [NSMutableSet setWithArray:self.arrayMenuNameAgeRelatedData];
    NSMutableSet *set2 = [NSMutableSet setWithArray:allCourseAlab];
    
    [set2 minusSet:set1];
    NSArray *sortDesc = @[[[NSSortDescriptor alloc] initWithKey:nil ascending:YES]];
    NSArray *sortSetArray = [set2 sortedArrayUsingDescriptors:sortDesc];
    
    [self.pageMenu removeFromSuperview];
    self.pageMenu  = nil;
    [self.scrollView removeFromSuperview];
    self.scrollView  = nil;
    UIView *view = [self.view viewWithTag:1000];
    [view removeFromSuperview];
    [self.myChildViewControllers removeAllObjects];
    JMColumnMenu *menuVC = [JMColumnMenu columnMenuWithTagsArrM:self.arrayMenuNameAgeRelatedData OtherArrM:sortSetArray Type:JMColumnMenuTypeTouTiao ageCount:self.arrayAgeData.count Delegate:self];
    menuVC.count = self.arrayAgeData.count;
    menuVC.refreshView = ^{
        /**获取已添加的一级目录*/
        [self getAppMeunAgeRelatedType];
        /**获取未添加的一级目录*/
        [self getAppMeunTypeEditMenu];
    };
    [self.navigationController pushViewController:menuVC animated:YES];

}

#pragma mark - JMColumnMenuDelegate
- (void)columnMenuTagsArr:(NSMutableArray *)tagsArr OtherArr:(NSMutableArray *)otherArr {
    //ZPLog(@"选择数组---%@",tagsArr);
    
    #pragma mark 根据数组个数比较
    if (tagsArr.count > self.arrayMenuNameAgeRelatedData.count) {
         NSString *name = tagsArr.lastObject;
        [self.arrayMenuNameAllRestData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ExcellentClassModel *model0 = self.arrayMenuNameAllRestData[idx];
            if ([name isEqualToString:model0.menu_name]) {
                [SVProgressHUD showWithStatus:@"栏目添加中，请稍等···"];
               [self getUserMenu:model0.menu_id];
                *stop = YES;
            }
        }];
         self.arrayMenuNameAgeRelatedData = tagsArr;
        
    }else {
        NSString *name = otherArr.firstObject;
        [self.arrayAllData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ExcellentClassModel *model0 = self.arrayAllData[idx];
            if ([name isEqualToString:model0.menu_name]) {
                [SVProgressHUD showWithStatus:@"栏目删除中，请稍等···"];
                [self getDeleteUserMenu:model0.menu_id];
                *stop = YES;
            }
        }];
         self.arrayMenuNameAgeRelatedData = tagsArr;
    }
    //[self getAppMeunTypeEditMenu];
  
    /**添加标签*/

}

- (void)columnMenuDidSelectTitle:(NSString *)title Index:(NSInteger)index {
    //ZPLog(@"点击的标题---%@  对应的index---%zd",title, index);
    if (self.arrayMenuNameAgeRelatedData.count > index) {
        _toIndex = index;
    }
}

- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, CGRectGetMaxY(_pageMenu.frame), kScreenWidth, scrollViewHeight);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, CGRectGetMaxY(_bannerView.frame),  kScreenWidth, pageMenuH) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;
        _pageMenu.dividingLine.hidden = NO;
        // 同时设置图片和文字，如果只想要文字，image传nil，如果只想要图片，title传nil，imagePosition和ratio传0即可
        [_pageMenu setFunctionButtonTitle:nil image:[UIImage imageNamed:@"添加栏目"] imagePosition:SPItemImagePositionTop imageRatio:0.5 forState:UIControlStateNormal];
        _pageMenu.showFuntionButton = YES;
        _pageMenu.bridgeScrollView.delegate = self;
    }
    return _pageMenu;
}

-(UIView *)bannerView {
    if (!_bannerView) {
        _bannerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScale(90))];
        _bannerView.backgroundColor = [UIColor redColor];
    }
    return  _bannerView;
}

#pragma mark - getter
- (NSMutableArray *)myChildViewControllers {
    if (!_myChildViewControllers) {
        _myChildViewControllers = [NSMutableArray array];
    }
    return _myChildViewControllers;
}

-(NSMutableArray *)arrayAllData {
    if (!_arrayAllData) {
        _arrayAllData = [NSMutableArray array];
    }
    return _arrayAllData;
}

-(NSMutableArray *)arrayMenuNameAgeRelatedData {
    if (!_arrayMenuNameAgeRelatedData) {
        _arrayMenuNameAgeRelatedData = [NSMutableArray array];
    }
    return _arrayMenuNameAgeRelatedData;
}

-(NSMutableArray *)arrayMenuNameRestData {
    if (!_arrayMenuNameRestData) {
        _arrayMenuNameRestData = [NSMutableArray array];
    }
    return _arrayMenuNameRestData;
}

-(NSMutableArray *)arrayMenuNameAllRestData {
    if (!_arrayMenuNameAllRestData) {
        _arrayMenuNameAllRestData = [NSMutableArray array];
    }
    return _arrayMenuNameAllRestData;
}

-(NSMutableArray *)arrayAgeData {
    if (!_arrayAgeData) {
        _arrayAgeData = [NSMutableArray array];
    }
    return _arrayAgeData;
}

- (void)dealloc {
    ZPLog(@"父控制器被销毁了");
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)myCourse:(id)sender {
    
}


@end
