//
//  CommentListViewController.m
//  mingyu
//
//  Created by apple on 2018/4/23.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CommentListViewController.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"
#import "CommentListCell.h"

#define BottomVIewHeight 46


@interface CommentListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *commentListArray;
@property (nonatomic, assign) NSInteger comment_number;   //总评论数

@property (nonatomic, assign) GetAndAddCommentType comment_type;

@property (nonatomic, strong) NSString *type_id;    //举报类型

@property (nonatomic, assign) PraiseType praisetype;    //点赞类型


@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;
@end

@implementation CommentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    if ([_type isEqual: @"topic"]) {
        _type_id = @"2";
        _comment_type = topic_comment;
        _praisetype = praise_topic_comment;
        
    } else if ([_type isEqual: @"article"]){
        _type_id = @"3";
        _comment_type = article_comment;
        _praisetype = praise_article_comment;
    }
    else if ([_type isEqual:@"audio"]){
        _type_id = @"4";
        _comment_type = audio_comment;
        _praisetype = praise_audio_comment;
    } else if ([_type isEqual:@"course"]){
        _type_id = @"6";
        _comment_type = course_comment;
        _praisetype = praise_course_comment;
    } else if ([_type isEqual:@"listen_book"]){
        _type_id = @"7";
        _comment_type = listen_book_comment;
        _praisetype = praise_book_comment;

    } else if ([_type  isEqual: @"childcare"]){
        _type_id = @"7";
        _comment_type = childcare_comment;
        _praisetype = praise_childcare_comment;

    }
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    
    [self.view addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.mas_equalTo(BottomVIewHeight);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).with.offset(-BottomVIewHeight);
        
    }];


    [self setTextViewToolbar];
    
    [self refreshTableview];

}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage  imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];

    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.commentListArray.count>0) {
            [_commentListArray removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self getCommentList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getCommentList];

        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}




//获取评论列表
- (void)getCommentList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":[NSString stringWithFormat:@"%ld",_targetID],
                                 @"comment_type":@(_comment_type),
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            _start += 10;
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
//            self.title = [NSString stringWithFormat:@"%@条评论",data[@"comment_number"]];
            NSArray *commentList = data[@"commentList"];
            if (commentList.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in commentList) {
                CommentModel *model = [CommentModel modelWithJSON:dic];
                [self.commentListArray addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addCommentwithInputString:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
}



-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}



//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":[NSString stringWithFormat:@"%ld" ,_targetID]
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(_comment_type)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
            CommentModel *model = [CommentModel modelWithJSON:json[@"data"]];
            [_commentListArray insertObject:model atIndex:0];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}




#pragma tableView--delegate
#pragma tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.commentListArray && self.commentListArray.count > 0) {
        return self.commentListArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
    if (_commentListArray && _commentListArray.count > indexPath.row) {
        CommentModel *commenmodel = [_commentListArray objectAtIndex:indexPath.row];
        [commentcell setcellWithModel:commenmodel praiseType:_praisetype];
    }
    commentcell.selectionStyle = UITableViewCellSelectionStyleNone;
    return commentcell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CommentModel *commenmodel = [_commentListArray objectAtIndex:indexPath.row];
    if (commenmodel.fk_user_id == [USER_ID integerValue]) {
        //显示弹出框列表选择
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:commenmodel.comment_content
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* copyAction = [UIAlertAction actionWithTitle:@"复制" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //复制评论内容
                                                               UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                               pasteboard.string = commenmodel.comment_content;
                                                               [UIView ShowInfo:@"复制成功" Inview:self.view];
                                                           }];
        UIAlertAction* deleteAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //删除自己的评论
                                                                 [self deletecommen:commenmodel AtIndexPath:indexPath];
                                                             }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                 
                                                             }];
        [cancelAction setValue:[UIColor colorWithHexString:@"939393"] forKey:@"_titleTextColor"];
        
        [alert addAction:copyAction];
        [alert addAction:deleteAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else{
        //显示弹出框列表选择
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //举报别人评论
                                                                 [self reportcommenm:commenmodel];
                                                             }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                             }];
        [cancelAction setValue:[UIColor colorWithHexString:@"939393"] forKey:@"_titleTextColor"];
        
        [alert addAction:reportAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}


//删除自己的评论
- (void)deletecommen:(CommentModel *)commenmodel AtIndexPath:(NSIndexPath *)indexPath{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
    NSDictionary *parameters = @{
                                 @"comment_id":[NSString stringWithFormat:@"%ld",(long)commenmodel.comment_id],
                                 @"comment_type":@(_comment_type)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"]boolValue];
        if (success) {
            [self.commentListArray removeObjectAtIndex:indexPath.row];
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            _comment_number -- ;
            self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
        }
    } failure:^(NSError *error) {
        
    }];
}

//举报别人评论
- (void)reportcommenm:(CommentModel *)commenmodel{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"from_id":[NSString stringWithFormat:@"%ld", (long)commenmodel.comment_id],
                                 @"type_id":_type_id
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"举报成功" Inview:self.view];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
        _tableView.height = kScreenHeight-BottomVIewHeight;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //预计高度为143--即是cell高度
        _tableView.estimatedRowHeight = 150.0f;
        //自适应高度
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    return _tableView;
}

- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil][1];
        _bottomView.frame = CGRectMake(0, kScreenHeight-BottomVIewHeight, kScreenWidth, BottomVIewHeight);
        __weak __typeof(self) weakSelf = self;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
    }
    return _bottomView;
}


- (NSMutableArray *)commentListArray{
    if (_commentListArray == nil) {
        _commentListArray = [NSMutableArray array];
    }
    return _commentListArray;
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didMoveToParentViewController:(UIViewController*)parent{
    [super didMoveToParentViewController:parent];
    ZPLog(@"%s,%@",__FUNCTION__,parent);
    if(!parent){
//        ZPLog(@"页面pop成功了");
        if (self.CommentListBlock) {
            self.CommentListBlock(_commentListArray, _comment_number);
        }
    }
}

@end
