//
//  MineInterestViewController.h
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineInterestViewController : UIViewController

@end



@interface MineInterestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name_lab;

@property (weak, nonatomic) IBOutlet UISwitch * choose_switch;

@property (nonatomic, strong) LabelListModel *labModel;


@end
