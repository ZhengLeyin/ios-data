//
//  HomePageMoreViewController.h
//  mingyu
//
//  Created by MingYu on 2018/12/26.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomePageMoreViewController : UIViewController

@property (nonatomic, assign) NSUInteger menuId;
@end

NS_ASSUME_NONNULL_END
