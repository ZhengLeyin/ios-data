//
//  YearPhotoCollectionCell.m
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "YearPhotoCollectionCell.h"

@implementation YearPhotoCollectionCell


- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self == [super initWithFrame:frame]) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        _imgView.backgroundColor = [UIColor whiteColor];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds = YES;
        _imgView.image = ImageName(@"pic");
        [self.contentView addSubview:_imgView];
        
        _selectImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.width-25, 5, 20, 20)];
        _selectImage.image = [UIImage imageNamed:@"weixuanz"];
        [self.contentView addSubview:_selectImage];

    }
    
    return self;
}
@end
