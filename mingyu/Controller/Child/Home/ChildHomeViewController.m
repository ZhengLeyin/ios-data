//
//  ChildHomeViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "ChildHomeViewController.h"
#import "DataSource.h"
#import "SearchViewController.h"
#import "UINavigationController+NavAlpha.h"

@interface ChildHomeViewController ()<UISearchBarDelegate,YANScrollMenuDelegate,YANScrollMenuDataSource>
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *TopView;
@property (weak, nonatomic) IBOutlet UIView *IconView;

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic, strong) NSMutableArray *tableviewArray;


/**
 * 菜单
 */
@property (nonatomic, strong) YANScrollMenu *menu;
/**
 *  dataSource
 */
@property (nonatomic, strong) NSMutableArray<DataSource *> *dataSource;
/**
 *  分区数
 */
@property (nonatomic, assign) NSUInteger number;



@end

@implementation ChildHomeViewController

- (NSMutableArray<DataSource *> *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (NSMutableArray *)tableviewArray{
    if (_tableviewArray == nil) {
        _tableviewArray = [NSMutableArray array];
    }
    return _tableviewArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIView *titleView = [[UIView alloc] init];
    titleView.frame = CGRectMake(0, 0, self.view.width-kScale(120), 30);
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:titleView.bounds];
    searchBar.width = titleView.width-kScale(20);
    searchBar.placeholder = @"请输入搜索内容";
    searchBar.center = titleView.center;
    searchBar.delegate = self;
    [titleView addSubview:searchBar];
    self.navigationItem.titleView = titleView;
    
    [self AddIcon];
    
//    [self RequestTableviewData];

    //GCD
    dispatch_async(dispatch_get_global_queue(0, 0), ^{

        [self createData];

        dispatch_async(dispatch_get_main_queue(), ^{

            [self.menu reloadData];

        });
    });

}

//- (void)RequestTableviewData{
//    
//    NSString *URL =[ NSString stringWithFormat:@"%@%@/0/5",KURL,KGetVideoList];
//    [HttpRequest getWithURLString:URL parameters:nil success:^(id responseObject) {
//        NSDictionary *json = responseObject;
//        ZPLog(@"%@",json);
//        ZPLog(@"%@",json[@"message"]);
//        if ([responseObject[@"message"]  isEqual: @"token已失效!"]) {
////            [self RequestTableviewData];
//        } else{
//            NSArray *list = json[@"data"];
//            for (NSDictionary *dic in list) {
//                CourseModel *video = [CourseModel modelWithJSON:dic];
//
//                [self.tableviewArray addObject:video];
//            }
//            [_tableview reloadData];
//        }
//
//    } failure:^(NSError *error) {
//        
//    }];
//    
//}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navAlpha = 1;
    self.navBarTintColor = [CYTabBarConfig shared].selectedTextColor;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}


#pragma mark - Prepare UI
- (void)AddIcon{
    
    self.number = 1;
    
    self.menu = [[YANScrollMenu alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,kScale(175)) delegate:self];
    self.menu.currentPageIndicatorTintColor = [UIColor colorWithRed:107/255.f green:191/255.f blue:255/255.f alpha:1.0];
    [self.IconView addSubview:self.menu];
    
    [YANMenuItem appearance].textFont = [UIFont systemFontOfSize:12];
    [YANMenuItem appearance].space = 5;
    [YANMenuItem appearance].textColor = [UIColor colorWithRed:51/255.f green:51/255.f blue:51/255.f alpha:1.0];
    
}


#pragma mark -  Data
- (void)createData{
    
    NSArray *images = @[ImageName(@"icon_cate"),
                        ImageName(@"icon_drinks"),
                        ImageName(@"icon_movie"),
                        ImageName(@"icon_recreation"),
                        ImageName(@"icon_stay"),
                        ImageName(@"icon_ traffic"),
                        ImageName(@"icon_ scenic"),
                        ImageName(@"icon_fitness"),
                        ImageName(@"icon_fitment"),
                        ImageName(@"icon_hairdressing"),
                        ImageName(@"icon_mom"),
                        ImageName(@"icon_study"),
                        ImageName(@"icon_travel"),
                        @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498711713465&di=d986d7003deaae41342dd9885c117e38&imgtype=0&src=http%3A%2F%2Fs9.rr.itc.cn%2Fr%2FwapChange%2F20168_3_0%2Fa86hlk59412347762310.GIF"];
    NSArray *titles = @[@"美食",
                        @"休闲娱乐",
                        @"电影/演出",
                        @"KTV",
                        @"酒店住宿",
                        @"火车票/机票",
                        @"旅游景点",
                        @"运动健身",
                        @"家装建材",
                        @"美容美发",
                        @"母婴",
                        @"学习培训",
                        @"旅游出行",
                        @"动态图\n从网络获取"];
    
    for (NSUInteger idx = 0; idx< images.count; idx ++) {
        
        DataSource *object = [[DataSource alloc] init];
        object.itemDescription = titles[idx];
        object.itemImage = images[idx];
        object.itemPlaceholder = ImageName(@"placeholder");
        
        [self.dataSource addObject:object];
    }
}

#pragma mark - YANScrollMenuProtocol
- (NSUInteger)numberOfSectionsInScrollMenu:(YANScrollMenu *)menu{
    return self.number;
}

- (NSUInteger)scrollMenu:(YANScrollMenu *)menu numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (id<YANObjectProtocol>)scrollMenu:(YANScrollMenu *)scrollMenu objectAtIndexPath:(NSIndexPath *)indexPath{
    return self.dataSource[indexPath.item];
}

- (CGSize)itemSizeOfScrollMenu:(YANScrollMenu *)menu{
    return CGSizeMake(kScale(75), kScale(70));
}

- (void)scrollMenu:(YANScrollMenu *)menu didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ZPLog(@"IndexPath:%@",indexPath);
    
    ViewController *VC = [ViewController new];
//    VC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:VC animated:YES];
    
}

- (CGFloat)heightOfHeaderInScrollMenu:(YANScrollMenu *)menu{
    return  0;
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];

    SearchViewController *VC = [SearchViewController new];
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tableviewArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"ChildHomeTableViewCell";
    ChildHomeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[ ChildHomeTableViewCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    __block typeof (self) WeakSelf = self;
    cell.block = ^(NSInteger index) {
        ZPLog(@"%ld",(long)index);
        ViewController *VC = [ViewController new];
        [WeakSelf.navigationController pushViewController:VC animated:YES];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_tableviewArray && _tableviewArray.count > indexPath.row) {
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kScale(130);
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}


@end
