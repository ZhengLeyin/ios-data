//
//  TopicInfoModel.h
//  mingyu
//
//  Created by apple on 2018/4/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParentTopicModel.h"

@interface TopicInfoModel : NSObject


/**
 话题ID
 */
@property (nonatomic, assign) NSInteger topic_id;

/**
 用户ID
 */
@property (nonatomic, assign) NSInteger fk_user_id;

/**
 圈子ID
 */
@property (nonatomic, assign) NSInteger fk_circle_id;

/**
 帖子标题
 */
@property (nonatomic, copy) NSString *topic_title;

/**
 帖子描述
 */
@property (nonatomic, copy) NSString *topic_abstract;

/**
 帖子头像
 */
@property (nonatomic, copy) NSString *topic_head;

/**
 帖子内容
 */
@property (nonatomic, copy) NSString *topic_content;

/**
 帖子发布时间
 */
@property (nonatomic, copy) NSString *topic_time;

/**
 发帖用户名
 */
@property (nonatomic, copy) NSString *user_name;

/**
 发帖用户头像
 */
@property (nonatomic, copy) NSString *user_head;

/**
 帖子点赞数
 */
@property (nonatomic, assign) NSInteger praise_number;

/**
 帖子点赞ID
 */
@property (nonatomic, assign) NSInteger click_praise_true;

/**
 帖子收藏量
 */
@property (nonatomic, assign) NSInteger collect_number;

/**
 帖子收藏ID
 */
@property (nonatomic, assign) NSInteger is_collect_true;

/**
 帖子关注量
 */
@property (nonatomic, assign) NSInteger follow_number;

/**
 关注ID
 */
//@property (nonatomic, assign) NSInteger is_follow_true;

/**
 置顶ID
 */
@property (nonatomic, assign) NSInteger is_top_true;


/**
 与帖子主人 关系。1、无关系。 2、已关注  3、互相关注
 */
@property (nonatomic, assign) NSInteger follow_status;

@end
