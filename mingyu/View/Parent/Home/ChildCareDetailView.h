//
//  ChildCareDetailView.h
//  mingyu
//
//  Created by apple on 2018/7/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel;

@interface ChildCareDetailView : UIView

@property (weak, nonatomic) IBOutlet UILabel *tilte_lab;

@property (weak, nonatomic) IBOutlet UIButton *user_icon_button;

@property (weak, nonatomic) IBOutlet UILabel *name_lab;

@property (weak, nonatomic) IBOutlet UIButton *attention_button;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;



@property (weak, nonatomic) IBOutlet UIButton *praise_button;

@property (weak, nonatomic) IBOutlet UIButton *collect_button;

@property (weak, nonatomic) IBOutlet UIButton *top_button;

@property (weak, nonatomic) IBOutlet UIView *circle_view;

@property (weak, nonatomic) IBOutlet UIButton *circle_button;


@property (nonatomic, strong) NewsModel *model;


+ (instancetype)theChildCareDetailView1;

+ (instancetype)theChildCareDetailView2;


- (IBAction)CollectButton:(id)sender;


@end
