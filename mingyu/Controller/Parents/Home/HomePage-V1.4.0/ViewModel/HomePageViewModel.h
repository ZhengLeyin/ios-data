//
//  HomePageViewModel.h
//  mingyu
//
//  Created by MingYu on 2018/12/24.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/** 首页标签获取 */
@protocol HomePageViewModelDetegate <NSObject>
- (void)setValue:(NSArray *)dataArray;
@end
/**数据传值*/
typedef void(^returnReturnDataOfHomePageBlock)(NSArray  *data);
typedef void(^returnReturnDataOfHomePageMoreBlock)(NSArray  *data,BOOL lastPage,NSUInteger start,NSUInteger startAlbum);
@interface HomePageViewModel : NSObject

@property (weak,nonatomic) id<HomePageViewModelDetegate>homePageViewModelDetegate;

@property(nonatomic,copy) returnReturnDataOfHomePageBlock returnReturnDataBlock;

@property(nonatomic,copy) returnReturnDataOfHomePageMoreBlock returnReturnDataMoreBlock;

/**获取首页标签   type =8 **/
-(void)getAppMeunAgeRelatedType:(NSUInteger)type;

/** 根据菜单id获取信息 */
-(void)getKHomePageMenuId:(NSUInteger)menu_id;

/** 获取首页更多获取信息 */
-(void)getKHomePageMoreMenuId:(NSUInteger)menu_id start:(NSUInteger)start startAlbum:(NSUInteger)startAlbum  lastPage:(BOOL)lastPage;

/** 获取功能模块信息 */
-(void)getHomePageApiIcon;
@end

NS_ASSUME_NONNULL_END
