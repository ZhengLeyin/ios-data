//
//  CommentCell.h
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"
#import "CommentReplayModel.h"

@protocol CommentCellDelegate <NSObject>

@optional
/** 查看更多 收起更多 */
- (void)CommentCellShowHiddeMore:(CommentModel *)commentmodel row:(NSInteger )index;

/** ReplayCell 点击事件 */
- (void)CommentCellReplayCell:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex didSelectRow:(NSInteger )index;

/** CommentCell 回复 点击事件 */
- (void)CommentCellCommentButton:(CommentModel *)commentmodel CommentCellIndex:(NSInteger)commentindex;

/** ReplayCell  回复 点击事件 */
- (void)ReplayCellReplayButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex;


@end


@interface CommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UITableView *replayTableview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replayTableviewHeight;

@property (weak, nonatomic) IBOutlet UIButton *userHeadButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *commentContentLab;

@property (weak, nonatomic) IBOutlet UILabel *commentTimeLab;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UIButton *praiseButton;



@property (nonatomic, weak) id <CommentCellDelegate> delegate;

@property(nonatomic ,strong)CommentModel *commentmodel;
@property(nonatomic ,assign)NSInteger CommentCellIndex;

@property (nonatomic, assign) PraiseType commentpraisetype;  //评论点赞类型
@property (nonatomic, assign) PraiseType replaypraisetype;   //回复点赞类型
//@property (nonatomic, copy) void (^sendBlock)(NSInteger index);

+ (CGFloat)cellHeight;

@end
