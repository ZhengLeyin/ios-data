//
//  ChildCareDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/7/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel,BottomCommetView;

@interface ChildCareDetailViewController : UIViewController

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) NewsModel *newsmodel;


@end
