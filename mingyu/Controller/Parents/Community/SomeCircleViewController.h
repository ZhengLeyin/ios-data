//
//  SomeCircleViewController.h
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LWXDetailController.h"
@class ParentCircleModel;

@interface SomeCircleViewController : LWXDetailController

@property (nonatomic, strong) ParentCircleModel *circlemodel;



@end
