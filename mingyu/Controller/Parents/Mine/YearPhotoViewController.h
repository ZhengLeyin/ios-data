//
//  YearPhotoViewController.h
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DiaryPhotoModel;

@interface YearPhotoViewController : UIViewController

@property (nonatomic, strong) DiaryPhotoModel *photomodel;


@end
