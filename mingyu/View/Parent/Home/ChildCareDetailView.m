//
//  ChildCareDetailView.m
//  mingyu
//
//  Created by apple on 2018/7/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChildCareDetailView.h"
#import "ChildCareDetailViewController.h"

@implementation ChildCareDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)theChildCareDetailView1{
    ChildCareDetailView *view = [[NSBundle mainBundle] loadNibNamed:@"ChildCareDetailView" owner:nil options:nil][0];
    return view;
}

+ (instancetype)theChildCareDetailView2{
    ChildCareDetailView *view = [[NSBundle mainBundle] loadNibNamed:@"ChildCareDetailView" owner:nil options:nil][1];
    return view;
}


- (void)setModel:(NewsModel *)model{
    _model = model;
   
    _tilte_lab.text = model.news_title;
    _time_lab.text = [NSString stringWithFormat:@"%@ | %ld阅读", model.time_status,model.page_view];
   
    
    _attention_button.layer.cornerRadius = 3;
    _attention_button.layer.borderWidth = 1;
    _attention_button.layer.masksToBounds = YES;
    _user_icon_button.layer.cornerRadius = 20;
    _user_icon_button.layer.masksToBounds = YES;
    
    [self changCollectButtonState:_model];
    
    [self changPraiseButtonState:_model];
    
    if (model.news_type == 1 && _model.fk_user_id == 0) {
//        NSString *imageURL =[NSString stringWithFormat:@"%@%@",KKaptcha,model.source_head];
        [_user_icon_button zp_setImageWithURL:model.source_head forState:0 placeholderImage:ImageName(@"默认头像")];
        _name_lab.text = model.article_source;
        _attention_button.hidden = YES;
    } else {
//        NSString *imageURL =[NSString stringWithFormat:@"%@%@",KKaptcha,model.user_head];
        [_user_icon_button zp_setImageWithURL:model.user_head forState:0 placeholderImage:ImageName(@"默认头像")];
        _name_lab.text = model.user_name;
        _attention_button.hidden = NO;
        if (model.fk_user_id == [USER_ID integerValue]) {
            _attention_button.hidden = YES;
        }
        [self changefollowButtonState:_model];
    }
    
    if (model.news_type == 3) {
        _top_button.hidden = NO;
        [_circle_button setTitle:model.circle_name forState:0];
        [self changTopButtonState:_model];
    } else {
        _top_button.hidden = YES;
        _circle_view.hidden = YES;
    }
    
    self.height = [self calculateString:_model.news_title Width:24]+20+100;
}



-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-40, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.height;
}

//根据状态判断是否已关注
- (void)changefollowButtonState:(NewsModel *)model{

    if (model.follow_status == 1) {
        _attention_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_attention_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_attention_button setTitle:@"+关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    } else if (model.follow_status == 2) {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"已关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    } else {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"相互关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    }
}


//根据状态判断是否收藏
- (void)changCollectButtonState:(NewsModel *)model{
    //收藏
    if (model.is_collect_true == 0) {
        [_collect_button setImage:ImageName(@"收藏") forState:UIControlStateNormal];
    } else {
        [_collect_button setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
    }
    
    if (model.collect_number) {
        [_collect_button setTitle:[NSString stringWithFormat:@" %ld收藏 ",model.collect_number] forState:UIControlStateNormal];
    } else {
        [_collect_button setTitle:@" 收藏 " forState:UIControlStateNormal];
    }
}

//根据状态判断是否点赞
- (void)changPraiseButtonState:(NewsModel *)model{
    //点赞
    if (model.click_praise_true == 0) {
        [_praise_button setImage:ImageName(@"点赞") forState:UIControlStateNormal];
    } else {
        [_praise_button setImage:ImageName(@"已赞") forState:UIControlStateNormal];
    }
    
    if (model.praise_number) {
        [_praise_button setTitle:[NSString stringWithFormat:@" %ld赞 ",model.praise_number] forState:UIControlStateNormal];
    } else {
        [_praise_button setTitle:@" 赞 " forState:UIControlStateNormal];
    }
}

////根据状态判断是否已置顶
- (void)changTopButtonState:(NewsModel *)model{
    _top_button.layer.cornerRadius = 5;
    _top_button.layer.borderWidth = 1;

    if (model.is_top_true == 0) {
        _top_button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
        [_top_button setTitle:@"  置顶该帖  " forState:UIControlStateNormal];
        [_top_button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
    } else {
        _top_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_top_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_top_button setTitle:@"  已置顶  " forState:UIControlStateNormal];
    }
}


//关注
- (IBAction)AttentionButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _attention_button.userInteractionEnabled = NO;
    if (_model.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id": @(_model.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _model.follow_status = follow_status;
                [self changefollowButtonState:_model];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":@(_model.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _model.follow_status = 1;
                [self changefollowButtonState:_model];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}

//点赞
- (IBAction)PraiseButton:(id)sender {
//    if (![userDefault boolForKey:KUDhasLogin]) {
//        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
//        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
//        return;
//    }
    if (_model.click_praise_true == 0) {
        _praise_button.userInteractionEnabled = NO;
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _model.news_id],
                                     @"user_id":USER_ID,
                                     @"praise_type":@(praise_type_news)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _praise_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger click_praise_true = [json[@"data"] integerValue];
                _model.click_praise_true = click_praise_true;
                _model.praise_number++;
                [self changPraiseButtonState:_model];
                [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _praise_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else {
        [UIView ShowInfo:TipDoNotPraise Inview:[self viewController].view];
    }
}

//收藏
- (IBAction)CollectButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _collect_button.userInteractionEnabled = NO;
    if (_model.is_collect_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _model.news_id],
                                     @"user_id":USER_ID,
                                     @"collect_type":@(collect_type_news)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _collect_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _model.is_collect_true = is_collect_true;
                _model.collect_number++;
                [self changCollectButtonState:_model];
                [UIView ShowInfo:TipCollectSuccess Inview:[self viewController].view];
                ChildCareDetailViewController *VC = (ChildCareDetailViewController *)[self viewController];
                VC.bottomView.haveCollect = _model.is_collect_true;
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _collect_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        NSDictionary *parameters = @{
                                     @"idList":@(_model.is_collect_true)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _collect_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _model.is_collect_true = 0;
                _model.collect_number--;
                [self changCollectButtonState:_model];
                [UIView ShowInfo:TipUnCollectSuccess Inview:[self viewController].view];
                ChildCareDetailViewController *VC = (ChildCareDetailViewController *)[self viewController];
                VC.bottomView.haveCollect = _model.is_collect_true;
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _collect_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}

//置顶取消置顶
- (IBAction)TopButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _top_button.userInteractionEnabled = NO;
    if (_model.is_top_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KSetTop];
        NSDictionary *parameters = @{
                                     @"news_id":[NSString stringWithFormat:@"%ld", _model.news_id],
                                     @"user_id":USER_ID
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _top_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_top_true = [json[@"data"] integerValue];
                _model.is_top_true = is_top_true;
                [self changTopButtonState:_model];
                //发送通知
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationSetTopTopic object:nil userInfo:@{@"newsmodel":_model}];
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _top_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KCancelTop];
        NSDictionary *parameters = @{
                                     @"top_id":[NSString stringWithFormat:@"%ld", _model.is_top_true]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _top_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _model.is_top_true = 0;
                [self changTopButtonState:_model];
                //发送通知
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCancelTopTopic object:nil userInfo:nil];
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _top_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}


- (IBAction)circleButton:(id)sender {
    SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
    ParentCircleModel *circlemodel = [[ParentCircleModel alloc] init];
    circlemodel.circle_name = _model.circle_name;
    circlemodel.circle_id = _model.fk_circle_id;
    VC.circlemodel = circlemodel;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

- (IBAction)userIconButton:(id)sender {
    if (_model.fk_user_id != [USER_ID integerValue] && _model.fk_user_id!= 0) {
        PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
        VC.user_id = _model.fk_user_id;
        [[self  viewController].navigationController pushViewController:VC animated:YES];
    }
}


@end
