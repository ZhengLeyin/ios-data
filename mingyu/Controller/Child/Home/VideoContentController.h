//
//  VideoContentController.h
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ContentVCBlock)(VideoModel *model );

@interface VideoContentController : UIViewController


@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, copy) ContentVCBlock contentblock;

@end
