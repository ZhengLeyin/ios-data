//
//  AddRecordViewController.m
//  mingyu
//
//  Created by apple on 2018/8/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AddRecordViewController.h"
#import "AddPhotoCollectionCell.h"
#import "ChooseRecordTimeViewController.h"
#import "HXPhotoPicker.h"
#import "SDWebImageManager.h"
#import "QiniuSDK.h"

static const CGFloat kPhotoViewMargin = 10.0;

@interface AddRecordViewController ()<HXPhotoViewDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *publicButton;

@property (nonatomic, strong) YYTextView *textView;

@property (strong, nonatomic) HXPhotoManager *manager;
@property (weak, nonatomic) HXPhotoView *photoView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) HXDatePhotoToolManager *toolManager;

@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) UILabel *timeLab;

@property (nonatomic ,strong) NSMutableArray * imageUrlsArr; // 存放图片url

@property (nonatomic, copy) NSString *photoTime;

@property (nonatomic, assign) BOOL uploadImageFlag;

@end

@implementation AddRecordViewController

- (HXPhotoManager *)manager
{
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.showDeleteNetworkPhotoAlert = NO;
        if (iOS10Later) {
            _manager.configuration.cameraCellShowPreview = YES;
        } else {
            _manager.configuration.cameraCellShowPreview = NO;
        }
        _manager.configuration.clarityScale = 1.4;
        _manager.configuration.hideOriginalBtn = YES;
        _manager.configuration.saveSystemAblum = YES;
        _manager.configuration.photoMaxNum = 9;
        _manager.configuration.maxNum = 9;
        _manager.configuration.navBarBackgroudColor = [UIColor whiteColor];
        _manager.configuration.photoCanEdit = NO;
        _manager.configuration.showDateSectionHeader = NO;
        _manager.configuration.reverseDate = NO;
        _manager.configuration.downloadICloudAsset = NO;
        _manager.configuration.filtrationICloudAsset = YES;
        _manager.configuration.themeColor = [UIColor colorWithHexString:@"2C2C2C"];
        _manager.configuration.selectedTitleColor = [UIColor blackColor];
//        [UIColor colorWithHexString:@"5BD3F5"];
        _manager.configuration.cellSelectedBgColor = [UIColor colorWithHexString:@"FF8585"];
        
    }
    return _manager;
}
- (HXDatePhotoToolManager *)toolManager {
    if (!_toolManager) {
        _toolManager = [[HXDatePhotoToolManager alloc] init];
    }
    return _toolManager;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.alwaysBounceVertical = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    _textView = [[YYTextView alloc] initWithFrame:CGRectMake(20, 0, kScreenWidth-40, 110)];
    _textView.font = FontSize(16);
    _textView.textColor = [UIColor colorWithHexString:@"2c2c2c"];
    [self.scrollView  addSubview:_textView];
    
    CGFloat width = scrollView.frame.size.width;
    HXPhotoView *photoView = [HXPhotoView photoManager:self.manager];
    photoView.frame = CGRectMake(kPhotoViewMargin*2, 110+kPhotoViewMargin*2, width - kPhotoViewMargin * 4, 0);
    photoView.spacing = 10;
    photoView.lineCount = 3;
    photoView.delegate = self;
    photoView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:photoView];
    
    [self.scrollView addSubview:self.footerView];
    
    self.photoView = photoView;
    if (_diarymodel) {
        NSMutableArray *array = [NSMutableArray array];
        for (int i = 0; i < _diarymodel.imageList.count; i++) {
            NSDictionary *dic = (NSDictionary *)_diarymodel.imageList[i];
            NSString *url = [NSString stringWithFormat:@"%@%@",KKaptcha,dic[@"image_name"]];
            [array addObject:url];
        }
        [self.manager addNetworkingImageToAlbum:array selected:YES];
        _timeLab.text = [NSString stringWithFormat:@"%@年%@月%@日",_diarymodel.year,_diarymodel.month,_diarymodel.day];
        _textView.text = _diarymodel.diary_content;
    } else {
        _textView.placeholderText = @"想说点什么呢？";
        [photoView directGoPhotoViewController];
    }
    
    [self.photoView refreshView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.alpha = 1;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)dealloc{
    ZPLog(@"----dealloc---");
}

- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    for (HXPhotoModel *model in photos) {
        if (!model.networkPhotoUrl) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            // 设置日期格式
            [formatter setDateFormat:@"yyyy年MM月dd日"];
            _photoTime = [formatter stringFromDate:model.creationDate];
//            self.timeLab.text = _photoTime;
            break;
        }
    }

    _imageUrlsArr = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
    
    __weak typeof(self) weakSelf = self;
    [self.view showLoadingHUDText:nil];
    [self.toolManager writeSelectModelListToTempPathWithList:allList requestType:0 success:^(NSArray<NSURL *> *allURL, NSArray<NSURL *> *photoURL, NSArray<NSURL *> *videoURL) {
        ZPLog(@"allURL------%@",allURL);
//        weakSelf.imageUrlsArr = [[NSMutableArray alloc] initWithArray:allURL];
        [weakSelf.toolManager getSelectedImageList:allList success:^(NSArray<UIImage *> *imageList) {
            [weakSelf.view handleLoading];
            [allURL enumerateObjectsUsingBlock:^(NSURL*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (![[obj absoluteString] hasPrefix:@"http"] ) {
                    UIImage *image = imageList[idx];
                    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
                    [weakSelf getUpToken:imageData location:idx];
                } else {
                    NSString *URLstr = [[obj absoluteString] componentsSeparatedByString:@"com/"][1];
                    NSURL *URL = [NSURL URLWithString:URLstr];
                    [weakSelf.imageUrlsArr replaceObjectAtIndex:idx withObject:URL];
                }
            }];
        } failed:^{
            [weakSelf.view handleLoading];
        }];
    } failed:^{
        
    }];
}


- (void)photoView:(HXPhotoView *)photoView deleteNetworkPhoto:(NSString *)networkPhotoUrl {
    ZPLog(@"%@",networkPhotoUrl);
}


- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    ZPLog(@"%@",NSStringFromCGRect(frame));
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, CGRectGetMaxY(frame) + kPhotoViewMargin);
    self.footerView.frame = CGRectMake(0, CGRectGetMaxY(frame)+10 , kScreenWidth, 50);
}

//获取七牛云凭证
- (void)getUpToken:(NSData *)imageData location:(NSInteger)location{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetUpToken];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@%ld.png", str,(long)location];
    NSDictionary *parameters = @{
                                 @"bucketName":bucketName,
                                 @"type":@"diary",
                                 @"fileName":fileName
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            [self uploadImageToQNPutData:imageData key:data[@"key"] token:data[@"token"] location:location];
        } else {
//            [UIView ShowInfo:json[@"message"] Inview:self.view];
            [self.photoView deleteModelWithIndex:location];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:@"图片上传失败" Inview:self.view];
        [self.photoView deleteModelWithIndex:location];
    }];
}

//上传图片到七牛云
- (void)uploadImageToQNPutData:(NSData *)data key:(NSString *)key token:(NSString *)token location:(NSInteger)location{
    QNUploadManager *upManger = [[QNUploadManager alloc] init];
    QNUploadOption *uploadoption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
        ZPLog(@"location----:%ld  percent == %.2f",(long)location ,percent);
        _uploadImageFlag = YES;
    } params:nil checkCrc:YES cancellationSignal:nil];

    [upManger putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        ZPLog(@"info ===== %@", info);
        ZPLog(@"resp ===== %@", resp);
        _uploadImageFlag = NO;
        NSURL *URL = [NSURL URLWithString:resp[@"key"]];
        if (URL) {
            [self.imageUrlsArr replaceObjectAtIndex:location withObject:URL];
        } else {
//            [UIView ShowInfo:@"图片上传失败" Inview:self.view];
            [self.photoView deleteModelWithIndex:location];
        }
    } option:uploadoption];
    
   
}


- (NSMutableArray *)imageUrlsArr{
    if (!_imageUrlsArr) {
        _imageUrlsArr = [NSMutableArray array];
    }
    return _imageUrlsArr;
}

- (UIView *)footerView{
    if (!_footerView ) {
        _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0.5)];
        line.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        [_footerView addSubview:line];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 1, 100, 49)];
        lab.text = @"记录时间";
        lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        lab.font = FontSize(15);
        [_footerView addSubview:lab];
        
        UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth-12-15, 19, 12, 12)];
        arrow.contentMode = UIViewContentModeScaleAspectFit;
        arrow.image = ImageName(@"选择时间");
        [_footerView addSubview:arrow];
        
        _timeLab = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth/2, 1, kScreenWidth/2-35, 49)];
        _timeLab.textColor = [UIColor colorWithHexString:@"50D0F4"];
        _timeLab.font = FontSize(12);
        _timeLab.textAlignment = NSTextAlignmentRight;
        _timeLab.text = [NSString getCurrentTimes:@"yyyy年MM月dd日"];
        [_footerView addSubview:_timeLab];
        
        UIButton *choosetime = [UIButton buttonWithType:UIButtonTypeCustom];
        choosetime.frame = CGRectMake(0, 0, kScreenWidth, 50);
        [choosetime addTarget:self action:@selector(chooseTime) forControlEvents:UIControlEventTouchUpInside];
        [_footerView addSubview:choosetime];
    }
    return _footerView;
}


- (void)chooseTime{
    if (_uploadImageFlag == YES) return;
    ChooseRecordTimeViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ChooseRecordTimeViewController"];
    @weakify(self)
    VC.DownBlock = ^(NSString *timeStr) {
        @strongify(self)
        self.timeLab.text = timeStr;
    };
    VC.photoTime = _photoTime?_photoTime:self.timeLab.text;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)back:(id)sender {
    if (_diarymodel) {
        UIViewController *VC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
        [self.navigationController popToViewController:VC animated:YES];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)addRecord:(UIButton *)sender {
    [self.view endEditing:YES];
    if (_uploadImageFlag == YES) {
        [UIView ShowInfo:@"图片上传中" Inview:self.view];
        return;
    }
    [self.imageUrlsArr removeObject:@""];
//    NSArray *array = [self.imageUrlsArr copy];
//    [array enumerateObjectsUsingBlock:^(NSURL*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([[obj absoluteString] containsString:@"file://"]) {
//            [self.imageUrlsArr removeObject:obj];
//        }
//    }];
    if (self.textView.text.length == 0 && _imageUrlsArr.count == 0) {
        [UIView ShowInfo:@"内容不能为空" Inview:self.view];
        return;
    }
    [self showAlertWithTitle:nil message:@"确认发布吗？" noBlock:nil yseBlock:^{
        _publicButton.userInteractionEnabled = NO;
        [self confirmAddDiary];
    }];
}


- (void)confirmAddDiary{
    NSString *URL = [NSString string];
    NSDictionary *parameters = [NSDictionary dictionary];
    
//    [self.imageUrlsArr removeObject:@""];
    NSString *time = [_timeLab.text stringByReplacingOccurrencesOfString:@"年" withString:@"-"];
    time = [time stringByReplacingOccurrencesOfString:@"月" withString:@"-"];
    time = [time stringByReplacingOccurrencesOfString:@"日" withString:@""];
    NSString *string = [_imageUrlsArr componentsJoinedByString:@","];
    if (_diarymodel) {
        URL = [NSString stringWithFormat:@"%@%@",KURL, KUpdateDiary];
        parameters =  @{
                        @"user_id":USER_ID,
                        @"diary_content":_textView.text,
                        @"imageList":[NSString stringWithFormat:@"%@",string],
                        @"create_time":time,
                        @"diary_id":@(_diarymodel.diary_id)
                        };
    } else {
        URL = [NSString stringWithFormat:@"%@%@",KURL,KAddDiary];
        parameters =  @{
                        @"user_id":USER_ID,
                        @"diary_content":_textView.text,
                        @"imageList":[NSString stringWithFormat:@"%@",string],
                        @"create_time":time
                        };
    }
    
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"发布成功" Inview:self.view];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationAddTimeRecord object:nil userInfo:nil];
            [self performSelector:@selector(back:) withObject:nil afterDelay:1];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
        _publicButton.userInteractionEnabled = NO;
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        _publicButton.userInteractionEnabled = YES;
        
    }];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == _scrollView) {
        [self.view endEditing:YES];
    }
}


@end
