//
//  BaseViewController.h
//  mingyu
//
//  Created by apple on 2018/3/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MyHeaderView.h"

#define isIPhoneX kScreenHeight==812
#define bottomMargin (isIPhoneX ? 34 : 0)
#define KPlayerViewH 210

UIKIT_EXTERN NSNotificationName const VideoPlayerViewDidScrollNSNotification;
UIKIT_EXTERN NSNotificationName const VideoPlayerViewRefreshStateNSNotification;

@interface VideoBaseViewController : UIViewController
@property (nonatomic, strong) UIScrollView *scrollView;
//@property (nonatomic, strong) MyHeaderView *headerView;
@property (nonatomic, assign) CGPoint lastContentOffset;

@property (nonatomic, assign) BOOL isFirstViewLoaded;

@property (nonatomic, assign) BOOL refreshState;

@end

