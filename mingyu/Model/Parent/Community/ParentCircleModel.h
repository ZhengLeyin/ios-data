//
//  ParentCircleModel.h
//  mingyu
//
//  Created by apple on 2018/4/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParentCircleModel : NSObject

/**
 圈子图片
 */
@property (nonatomic, copy) NSString *circle_img;

/**
 圈子名称
 */
@property (nonatomic, copy) NSString *circle_name;


/**
 圈子id
 */
@property (nonatomic, assign) NSInteger circle_id;


/**
 圈子描述
 */
@property (nonatomic, copy) NSString *circle_abstract;

/**
 是否加入该圈子
 */
@property (nonatomic, assign) BOOL is_join_true;


/**
 该圈子帖子数
 */
@property (nonatomic, assign) NSInteger topicNum;

@end



@interface ClassifyModel : NSObject

@property (nonatomic, assign) NSInteger classify_id;

@property (nonatomic, copy) NSString *classify_name;

@property (nonatomic, assign) NSInteger fk_classify_id;

@property (nonatomic, copy) NSString *circle_name;

@property (nonatomic, assign) BOOL show;

@property (nonatomic, strong) NSArray *circleList;

@end
