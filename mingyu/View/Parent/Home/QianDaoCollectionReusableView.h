//
//  QianDaoCollectionReusableView.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QianDaoActionModel;


@interface QianDaoCollectionReusableView : UICollectionReusableView <SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *user_gold_Lab;

@property (weak, nonatomic) IBOutlet UILabel *count_day_Lab;


@property (weak, nonatomic) IBOutlet UISwitch *remindSwitch;

@property (weak, nonatomic) IBOutlet UIButton *qiandaoButton;

@property (weak, nonatomic) IBOutlet UIView *adView;

@property (weak, nonatomic) IBOutlet UIView *timeContentView;


@property (nonatomic, strong) UIImageView *yiqiandao;


@property (nonatomic, strong) QianDaoActionModel *actionmodel;


@end
