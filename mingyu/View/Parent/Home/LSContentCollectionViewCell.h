//
//  LSContentCollectionViewCell.h
//  mingyu
//
//  Created by apple on 2018/5/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSContentCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UILabel *lab;


@end
