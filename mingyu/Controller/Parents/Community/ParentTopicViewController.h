//
//  ParentTopicViewController.h
//  mingyu
//
//  Created by apple on 2018/4/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomCommetView.h"
@class NewsModel;


@interface ParentTopicViewController : UIViewController

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) NewsModel *newsmodel;

@property (nonatomic, assign) BOOL hiddenCircle;   //是否隐藏所属圈子

@end
