//
//  AddPhotoCollectionCell.m
//  mingyu
//
//  Created by apple on 2018/8/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AddPhotoCollectionCell.h"

@implementation AddPhotoCollectionCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.500];
//        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.layer.cornerRadius = 5;
        _imageView.layer.masksToBounds = YES;
        [self addSubview:_imageView];
        self.clipsToBounds = YES;
        
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setImage:ImageName(@"删图") forState:UIControlStateNormal];
        _deleteBtn.imageEdgeInsets = UIEdgeInsetsMake(-20, 0, 0, -20);
//        _deleteBtn.alpha = 0.6;
        [self addSubview:_deleteBtn];
//        [self bringSubviewToFront:_deleteBtn];

    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    _imageView.frame = self.bounds;
    _deleteBtn.frame = CGRectMake(self.width - 40, 0, 40, 40);
}

- (void)setRow:(NSInteger)row {
    _row = row;
    _deleteBtn.tag = row;
}

@end
