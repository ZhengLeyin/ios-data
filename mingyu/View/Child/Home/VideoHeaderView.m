//
//  VideoHeaderView.m
//  mingyu
//
//  Created by apple on 2018/4/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoHeaderView.h"

@implementation VideoHeaderView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}


- (void)setupUI{
    
    self.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    
    [self addSubview:self.video_titleLab];
    [self addSubview:self.video_abstractLab];
    [self addSubview:self.prerogativeButton];
    [self addSubview:self.collectButton];
    
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.mas_equalTo(-10);
    }];
    
    [self.video_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.equalTo(view).offset(15);
        make.right.equalTo(view).offset(-15);
    }];
    
    [self.prerogativeButton setImage:ImageName(@"icon_medal") forState:(UIControlStateNormal)];
    [self.prerogativeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.video_titleLab.mas_bottom).offset(10);
        make.left.equalTo(self.video_titleLab);
        //make.height.equalTo(@24);
    }];
    
    [self.video_abstractLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.prerogativeButton.mas_bottom).offset(20);
        make.left.equalTo(self.video_titleLab);
    }];
    
    [self.collectButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-15);
        make.centerY.equalTo(self.video_abstractLab);
        make.height.mas_equalTo(40);
    }];
}

- (void)setCoursemodel:(CourseModel *)coursemodel{
    _coursemodel = coursemodel;

    self.video_titleLab.text = coursemodel.course_title;
    self.video_abstractLab.text = [NSString stringWithFormat:@"%ld人已购 | 已更新%ld期",_coursemodel.hold_num,_coursemodel.section_num];

    if (coursemodel.course_prerogative == 1) {
        _prerogativeButton.hidden  = NO;
        _prerogativeButton.height = 20;
        [self.prerogativeButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.video_titleLab.mas_bottom).offset(5);
        }];
        [self.video_abstractLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.prerogativeButton.mas_bottom).offset(10);
        }];
        [self.collectButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.video_abstractLab);
        }];
    } else{
        _prerogativeButton.hidden  = YES;
        [self.prerogativeButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0.01);
        }];
    }
    
    [self changeCollectButton];
}

- (void)changeCollectButton{
    //收藏
    if (_coursemodel.is_collect_true == 0) {
        [_collectButton setImage:ImageName(@"收藏") forState:UIControlStateNormal];
    } else {
        [_collectButton setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
    }
    if (_coursemodel.collect_number > 0) {
        [_collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_coursemodel.collect_number] forState:UIControlStateNormal];
    } else {
        [_collectButton setTitle:@"  收藏  " forState:UIControlStateNormal];
    }
}


//收藏  取消收藏
- (void)CollectButton:(UIButton *)sender {
    NSString *URL = [NSString string];
    NSDictionary *parameter = [NSDictionary dictionary];
    if (_coursemodel.is_collect_true == 0) {      //收藏视频
        _collectButton.userInteractionEnabled = NO;
        URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        parameter = @{
                      @"user_id":USER_ID,
                      @"from_id":[NSString stringWithFormat:@"%ld",(long)_coursemodel.course_id],
                      @"collect_type":@(collect_type_course)
                      };
        [HttpRequest postWithURLString:URL parameters:parameter viewcontroller:[self viewController] success:^(id responseObject) {
            _collectButton.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@",json);
            ZPLog(@"%@",json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _coursemodel.is_collect_true = is_collect_true;
                _coursemodel.collect_number++;
                [self changeCollectButton];
                [UIView ShowInfo:TipCollectSuccess Inview:[self viewController].view];
                CourseVideoViewController *VC = (CourseVideoViewController *)[self viewController];
                VC.bottomView.haveCollect = _coursemodel.is_collect_true;
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _collectButton.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
        
    }else{   //取消收藏视频
        _collectButton.userInteractionEnabled = NO;
        URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        parameter = @{
                      @"idList":@(_coursemodel.is_collect_true)
                      };
        [HttpRequest postWithURLString:URL parameters:parameter viewcontroller:[self viewController] success:^(id responseObject) {
            _collectButton.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@",json);
            ZPLog(@"%@",json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _coursemodel.is_collect_true = 0;
                _coursemodel.collect_number--;
                [self changeCollectButton];
                [UIView ShowInfo:TipUnCollectSuccess Inview:[self viewController].view];
                CourseVideoViewController *VC = (CourseVideoViewController *)[self viewController];
                VC.bottomView.haveCollect = _coursemodel.is_collect_true;
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _collectButton.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}


- (void)DownButton:(UIButton *)sender {
    if (self.downblock) {
        self.downblock();
    }
}

- (UILabel *)video_titleLab{
    if (_video_titleLab == nil) {
        _video_titleLab = [[UILabel alloc] init];
        _video_titleLab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        _video_titleLab.font = [UIFont systemFontOfSize:18.0];
        _video_titleLab.numberOfLines = 0;
    }
    return _video_titleLab;
}


- (UILabel *)video_abstractLab{
    if (_video_abstractLab == nil) {
        _video_abstractLab = [[UILabel alloc]init];
        _video_abstractLab.textColor = [UIColor colorWithHexString:@"767676"];
        _video_abstractLab.font = FontSize(13);
        _video_abstractLab.numberOfLines = 1;
    }
    return _video_abstractLab;
}


- (UIButton *)collectButton{
    if (_collectButton == nil) {
        _collectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectButton setTitle:@" 收藏" forState:UIControlStateNormal];
        [_collectButton setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
        [_collectButton.titleLabel setFont:FontSize(14)];
        [_collectButton setImage:ImageName(@"soucang") forState:UIControlStateNormal];
        [_collectButton addTarget:self action:@selector(CollectButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _collectButton;
}

- (UIButton *)prerogativeButton {
    if (_prerogativeButton == nil) {
        _prerogativeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_prerogativeButton setTitle:@" 名校堂用户特权课" forState:UIControlStateNormal];
        [_prerogativeButton setTitleColor:[UIColor colorWithHexString:@"#50D0F4"] forState:UIControlStateNormal];
        [_prerogativeButton.titleLabel setFont:FontSize(16)];
        [_prerogativeButton setImage:ImageName(@"soucang") forState:UIControlStateNormal];
        //[_prerogativeButton addTarget:self action:@selector(CollectButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _prerogativeButton;
}
@end
