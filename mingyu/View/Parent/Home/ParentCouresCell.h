//
//  ParentCouresCell.h
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "screenView.h"
#import "CourseModel.h"

@interface ParentCouresCell : UITableViewCell
#pragma mark 精品课首页cell
/** 老师头像 */
@property (weak, nonatomic) IBOutlet UIImageView *course_img;
/** 课程名称 */
@property (weak, nonatomic) IBOutlet UILabel *course_title;
/** 课程描述 */
@property (weak, nonatomic) IBOutlet UILabel *course_abstract;
/** 课程购买量 */
@property (weak, nonatomic) IBOutlet UILabel *purchaseQuantityLabel;
/** 现价 */
@property (weak, nonatomic) IBOutlet UILabel *present_price;
/** 原始价格或者积分抵扣 */
@property (weak, nonatomic) IBOutlet UILabel *originalPriceOrIntegralLabel;
/** 播放按钮ImageView */
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
/** 试听Label */
@property (weak, nonatomic) IBOutlet UILabel *listeningTestLabel;
/** 播放类型 */
@property (weak, nonatomic) IBOutlet UIImageView *course_typeImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerConstraint;
@property (nonatomic, strong) CourseModel *coursemodel;
+ (instancetype)theCellWithTableView:(UITableView *)tableView;
/**特权课ImageView*/
@property (weak, nonatomic) IBOutlet UIImageView *privilegedClassImageView;
/**特权课ImageView*/
@property (weak, nonatomic) IBOutlet UILabel *privilegedClassLabel;


/**
 搜索
 */
- (void)showSearch;

#pragma mark 已购课程cell 约束有所改变
+ (instancetype)theCellWithPurchaseCourseTableView:(UITableView *)tableView;
/** 购买课程时间 */
@property (weak, nonatomic) IBOutlet UILabel *purchaseDateLabel;
/**已购课程界面传入过来*/
//-(void)purchasedCourseViewControllerPushModel:(CourseModel *)coursemodel
-(void)purchasedCourseViewControllerPushModel:(CourseModel *)coursemodel;

@end
