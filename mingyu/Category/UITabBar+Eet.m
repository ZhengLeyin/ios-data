//
//  UITabBar+Eet.m
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "UITabBar+Eet.h"
#define TabbarItemNums  3.0    //tabbar的数量 如果是5个设置为5

@implementation UITabBar (Eet)

//显示小红点
- (void)showBadgeOnItemIndex:(NSInteger)index{
    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    
    //新建小红点
    UIView *badgeView = [[UIView alloc]init];
    badgeView.tag = 888 + index;
    badgeView.layer.cornerRadius = 5.0;//圆形
    badgeView.backgroundColor = [UIColor colorWithHexString:@"E9524B"];//颜色：红色
    CGRect tabFrame = self.frame;
//    ZPLog(@"%f,%f",tabFrame.size.width,tabFrame.size.height);
    
    if (kScreenHeight == 568) {
        tabFrame.size.width = 320;
    }else if (kScreenHeight ==667){
        tabFrame.size.width = 375;
    }else if(kScreenHeight == 736){
        tabFrame.size.width = 414;

    }
    //确定小红点的位置
    CGFloat percentX = (index + 0.6) / TabbarItemNums;
    CGFloat x = ceilf(percentX * (tabFrame.size.width));
    CGFloat y = ceilf(0.1 * tabFrame.size.height);
    badgeView.frame = CGRectMake(x-5, y+3, 10.0, 10.0);//圆形大小为10
    badgeView.clipsToBounds = YES;
    [self addSubview:badgeView];
}

//隐藏小红点
- (void)hideBadgeOnItemIndex:(NSInteger)index{
    //移除小红点
    [self removeBadgeOnItemIndex:index];
}

//移除小红点
- (void)removeBadgeOnItemIndex:(NSInteger)index{
    //按照tag值进行移除
    for (UIView *subView in self.subviews) {
        if (subView.tag == 888+index) {
            [subView removeFromSuperview];
        }
    }
}


@end
