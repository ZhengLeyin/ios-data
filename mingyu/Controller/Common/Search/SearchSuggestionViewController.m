//
//  SearchSuggestionViewController.m
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchSuggestionViewController.h"

@interface SearchSuggestionViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *contentView;

@property (nonatomic, copy) NSString *searchstr;



@end

@implementation SearchSuggestionViewController


- (UITableView *)contentView
{
    if (!_contentView) {
        self.contentView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH) style:UITableViewStylePlain];
        _contentView.delegate = self;
        _contentView.dataSource = self;
        _contentView.estimatedRowHeight = 0;
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.tableFooterView = [UIView new];
    }
    return _contentView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.contentView];
    [self moreSearchAssociation];
}

- (void)moreSearchAssociation{
    //上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_contentView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self searchTestChangeWithTest:_searchstr];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _contentView.mj_footer = footer;
}



- (void)searchTestChangeWithTest:(NSString *)test {
    _searchstr = test;
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KSearchAssociation];
    NSDictionary *parameters = @{
                                 @"key":test,
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
//    URL = [URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self parentViewController] success:^(id responseObject) {
        [_contentView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            _start += 20;
            if (data && data.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            [self.searchArray addObjectsFromArray:data];
            [_contentView reloadData];
        }
    } failure:^(NSError *error) {
        [_contentView.mj_footer endRefreshing];
    }];
}


#pragma mark - UITableViewDataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchArray && self.searchArray.count > 0) {
        return _searchArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"CellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    if (self.searchArray && self.searchArray.count > indexPath.row) {
        NSString *text = _searchArray[indexPath.row];

        text = [@"<font style=\"font-size:13px\" color=\"#2C2C2C\">" stringByAppendingString:text];
        text = [text stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
        text = [text stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
        text = [text stringByAppendingString:@"</font>"];

        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        cell.textLabel.attributedText = attrStr;
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;

    }
    return cell;
}


#pragma mark - UITableViewDelegate -
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchBlock) {
        if (_searchArray && _searchArray.count > indexPath.row) {
            NSString *text = _searchArray[indexPath.row];
            text = [text stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
            text = [text stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
            self.searchBlock(text);
        }
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [NSMutableArray array];
    }
    return _searchArray;
}

@end
