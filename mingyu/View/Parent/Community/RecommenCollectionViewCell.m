//
//  RecommenCollectionViewCell.m
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "RecommenCollectionViewCell.h"
#import "UIView+CLSetRect.h"

@implementation RecommenCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];

   //设置圆角
    [_userHeadButton setAllCorner:_userHeadButton.bounds.size.height];
    [_attentionButton setAllCorner:3];
    [self setAllCorner:3];
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithHexString:@"F3F3F3"].CGColor;
    
//    self.layer.shadowColor = [UIColor colorWithHexString:@"DDDDDD"].CGColor;
//    self.layer.shadowOpacity = 0.5f;
//    self.layer.shadowRadius = 15.f;
//    self.layer.shadowOffset = CGSizeMake(0,4);
    //[self.userHeadButton setImage:[UIImage imageNamed:@"默认头像"] forState:(UIControlStateNormal)];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.userHeadButton  setImage:[UIImage imageWithRoundCorner:self.userHeadButton .imageView.image cornerRadius:25.0 size:CGSizeMake(50.0, 50.0)] forState:(UIControlStateNormal)];
//    });
    
}


- (void)setUsermodel:(UserModel *)usermodel{
    _usermodel = usermodel;
     
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,usermodel.user_head];
    [_userHeadButton zp_setImageWithURL:usermodel.user_head forState:0 placeholderImage:ImageName(@"默认头像")];
    _user_nameLab.text = usermodel.user_name;
    _fansCountLab.text = [NSString stringWithFormat:@"粉丝数：%ld",usermodel.follow_number];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.userHeadButton   setImage:[UIImage imageWithRoundCorner:self.userHeadButton .imageView.image cornerRadius:25.0 size:CGSizeMake(50.0, 50.0)] forState:(UIControlStateNormal)];
//        self.userHeadButton.layer.masksToBounds = YES;
//    });
}


- (IBAction)attentionButton:(UIButton *)sender {
    if (self.attentionblock) {
        self.attentionblock(_usermodel);
    }
}


- (IBAction)userHeaderButton:(id)sender {
    PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
    VC.user_id = _usermodel.user_id;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


//- (void)drawRect:(CGRect)rect{
//    
//
//}

@end
