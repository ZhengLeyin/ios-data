//
//  RotateView.m
//  mingyu
//
//  Created by apple on 2018/9/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "RotateView.h"

@implementation RotateView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
//    self.layer.cornerRadius = 32;
//    self.layer.masksToBounds = YES;
//    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.imageView.clipsToBounds = YES;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}


- (void)initUI{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 32;
    self.layer.masksToBounds = YES;
    

    
    _playbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    _playbutton.frame = CGRectMake(0, 0, 60, 60);
    [_playbutton setImage:ImageName(@"placeholderImage") forState:0];
    _playbutton.center = self.center;
    _playbutton.layer.cornerRadius = 32;
    _playbutton.contentMode = UIViewContentModeScaleAspectFill;
    _playbutton.layer.masksToBounds = YES;
    [self addSubview:_playbutton];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    imageV.image = ImageName(@"播放阴影");
    [self addSubview:imageV];
}



// 开始旋转
-(void) startRotating {
    CABasicAnimation* rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotateAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    rotateAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2];   // 旋转一周
    rotateAnimation.duration = 20.0;                                 // 旋转时间20秒
    rotateAnimation.repeatCount = MAXFLOAT;                      // 重复次数，这里用最大次数
    
    [self.layer addAnimation:rotateAnimation forKey:nil];
    
}

// 停止旋转
-(void) stopRotating {
    
    CFTimeInterval pausedTime = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.layer.speed = 0.0;                                          // 停止旋转
    self.layer.timeOffset = pausedTime;                              // 保存时间，恢复旋转需要用到
}

// 恢复旋转
-(void) resumeRotate {
    
    if (self.layer.timeOffset == 0) {
        [self startRotating];
        return;
    }
    
    CFTimeInterval pausedTime = self.layer.timeOffset;
    self.layer.speed = 1.0;                                         // 开始旋转
    self.layer.timeOffset = 0.0;
    self.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;  // 恢复时间
    self.layer.beginTime = timeSincePause;                          // 从暂停的时间点开始旋转
}


@end
