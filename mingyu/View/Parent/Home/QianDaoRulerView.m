//
//  QianDaoRulerView.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "QianDaoRulerView.h"

@implementation QianDaoRulerView


- (void)awakeFromNib{
    [super awakeFromNib];
    _knowButton.layer.cornerRadius = 15;
    _knowButton.layer.masksToBounds = YES;
}

- (IBAction)knowButton:(id)sender {
    if (self.knowButtonBlock) {
        self.knowButtonBlock();
    }
}


@end
