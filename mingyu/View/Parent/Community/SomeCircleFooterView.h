//
//  SomeCircleFooterView.h
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SomeCircleFooterView : UIView

@property (nonatomic, strong) UIButton *MoreButton;

@property (nonatomic, strong) UIView *contetview;

@property (nonatomic, copy) NSString *upTitle;

@property (nonatomic, copy) NSString *downTitle;

@property (nonatomic, copy) void(^MoreTopic) (BOOL more);



@end
