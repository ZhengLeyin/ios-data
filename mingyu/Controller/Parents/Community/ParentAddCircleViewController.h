//
//  ParentAddCircleViewController.h
//  mingyu
//
//  Created by apple on 2018/7/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ParentCircleModel;

@interface ParentAddCircleViewController : UIViewController

//发帖后更改圈子相关
@property (nonatomic, assign) NSInteger news_id;
@property (nonatomic, copy) void (^changeCircleBlock)(ParentCircleModel *circlemodel);

@end
