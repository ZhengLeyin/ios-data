//
//  HandPickCell.m
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "HandPickCell.h"

@implementation HandPickCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.leftCellchooseView.hidden = YES;
    self.leftCellTitle.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    self.leftCellTitle.highlightedTextColor = [UIColor colorWithHexString:@"50D0F4"];

}


+ (instancetype)theLeftCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"HandPickLeftCell";
    HandPickCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HandPickCell" owner:nil options:nil][0];
    }
    return cell;
}

+ (instancetype)theRightCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"HandPickRightCell";
    HandPickCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HandPickCell" owner:nil options:nil][1];
    }
    return cell;
}

- (void)setSubjectmodel:(SubjectModel *)subjectmodel{
    _subjectmodel = subjectmodel;
    _album_title.text = subjectmodel.subject_title;
    NSString *imagURL = [NSString stringWithFormat:@"%@%@",KKaptcha,subjectmodel.subject_image];
    [_album_image sd_setImageWithURL:[NSURL URLWithString:imagURL] placeholderImage:ImageName(@"placeholderImage")];
    _play_count.text = [NSString stringWithFormat:@"%ld人听过",subjectmodel.play_count];
    
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying && [DFPlayer shareInstance].currentAudioModel.subject_id == subjectmodel.subject_id) {
//    if ([DFPlayer shareInstance].currentAudioModel.audio_type == 1 && subjectmodel.subject_id == [DFPlayer shareInstance].currentAudioModel.fk_parent_id) {
        [_playButton setImage:ImageName(@"一键听_主题列表_开") forState:UIControlStateNormal];
    } else {
        [_playButton setImage:ImageName(@"一键听_主题列表_关") forState:UIControlStateNormal];
    }
}


- (IBAction)playSubjict:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(playSubjict:)]) {
        [self.delegate playSubjict:_subjectmodel];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
    self.leftCellTitle.highlighted = selected;
    self.leftCellchooseView.hidden = !selected;

}

@end
