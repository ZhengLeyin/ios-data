//
//  MingYuLogger.h
//  MingYuXIbDemo
//
//  Created by MingYu on 2018/12/11.
//  Copyright © 2018 com.mingyu.www. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    MingYuLoggerLevelNone = 0,
    MingYuLoggerLevelInfo = 1,
    MingYuLoggerLevelDebug = 1 << 1,
    MingYuLoggerLevelError = 1 << 2,
    MingYuLoggerLevelAll = MingYuLoggerLevelInfo | MingYuLoggerLevelDebug | MingYuLoggerLevelError,
} MingYuLoggerLevel;

extern NSString *const MingYuLoggerDomainCURL;
extern NSString *const MingYuLoggerDomainNetwork;
extern NSString *const MingYuLoggerDomainIM;
extern NSString *const MingYuLoggerDomainStorage;
extern NSString *const MingYuLoggerDomainDefault;

@interface MingYuLogger : NSObject
+ (void)setAllLogsEnabled:(BOOL)enabled;
+ (void)setLoggerLevelMask:(NSUInteger)levelMask;
+ (void)addLoggerDomain:(NSString *)domain;
+ (void)removeLoggerDomain:(NSString *)domain;
+ (void)logFunc:(const char *)func line:(const int)line domain:(nullable NSString *)domain level:(MingYuLoggerLevel)level message:(NSString *)fmt, ... NS_FORMAT_FUNCTION(5, 6);
+ (BOOL)levelEnabled:(MingYuLoggerLevel)level;
+ (BOOL)containDomain:(NSString *)domain;
@end

NS_ASSUME_NONNULL_END

#define _MingYuLoggerInfo(_domain, ...) [MingYuLogger logFunc:__func__ line:__LINE__ domain:_domain level:MingYuLoggerLevelInfo message:__VA_ARGS__]
#define _MingYuLoggerDebug(_domain, ...) [MingYuLogger logFunc:__func__ line:__LINE__ domain:_domain level:MingYuLoggerLevelDebug message:__VA_ARGS__]
#define _MingYuLoggerError(_domain, ...) [MingYuLogger logFunc:__func__ line:__LINE__ domain:_domain level:MingYuLoggerLevelError message:__VA_ARGS__]

#define MingYuLoggerInfo(domain, ...) _MingYuLoggerInfo(domain, __VA_ARGS__)
#define MingYuLoggerDebug(domain, ...) _MingYuLoggerDebug(domain, __VA_ARGS__)
#define MingYuLoggerError(domain, ...) _MingYuLoggerError(domain, __VA_ARGS__)

#define MingYuLoggerI(...)  MingYuLoggerInfo(MingYuLoggerDomainDefault, __VA_ARGS__)
#define MingYuLoggerD(...) MingYuLoggerDebug(MingYuLoggerDomainDefault, __VA_ARGS__)
#define MingYuLoggerE(...) MingYuLoggerError(MingYuLoggerDomainDefault, __VA_ARGS__)
