//
//  FirstLaunchViewController.m
//  TZWY
//
//  Created by apple on 2018/3/27.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "FirstLaunchViewController.h"
#import <UMPush/UMessage.h>

@interface FirstLaunchViewController ()<UIScrollViewDelegate>


@property(nonatomic, strong)UIScrollView *scrollView;
@property(nonatomic, strong)NSArray *array;
@property(nonatomic, strong)UIPageControl *pageControl;


@end


@implementation FirstLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    _scrollView.showsHorizontalScrollIndicator = FALSE;
    _scrollView.bounces = NO;//设置边界是否反弹
    _scrollView.pagingEnabled = YES;//设置自动滚动到边界
    _scrollView.delegate = self;//设置委托代理
    
    _array = [NSArray arrayWithObjects:@"欢迎页1",@"欢迎页2" ,nil];  //图片名称
    
    _scrollView.contentSize = CGSizeMake(kScreenWidth*_array.count, kScreenHeight);
    [self.view addSubview:_scrollView];
    
    for (int i = 0; i < _array.count; i++) {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(i*kScreenWidth, 0, kScreenWidth, kScreenHeight)];
        image.contentMode = UIViewContentModeScaleAspectFit;
        image.image = [UIImage imageNamed:_array[i]];
        [_scrollView addSubview:image];
    }
    
    //立即进入
    UIButton *kickOffButton = [UIButton buttonWithType:UIButtonTypeCustom];
    kickOffButton.frame = CGRectMake(1.5*kScreenWidth-55, kScreenHeight-effectViewH-130, 110, 32);
    [kickOffButton setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
    kickOffButton.layer.cornerRadius = 16.0;
    kickOffButton.layer.borderWidth = 1;
    kickOffButton.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
    [kickOffButton setTitle:@"立即体验" forState:UIControlStateNormal];
    [kickOffButton setBackgroundColor:[UIColor clearColor]];
    kickOffButton.alpha = 0.75;
    [kickOffButton addTarget:self action:@selector(startButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:kickOffButton];
    
    
//    UIButton *kickOffButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    kickOffButton.frame = CGRectMake(1.5*kScreenWidth-75, kScreenHeight-80, 150, 45);
//    kickOffButton.layer.cornerRadius = 3.0;
//    [kickOffButton setTitle:@"k" forState:UIControlStateNormal];
//    [kickOffButton setBackgroundColor:[UIColor colorWithHexString:@"E9524B"]];
//    kickOffButton.alpha = 0.75;
//    [kickOffButton addTarget:self action:@selector(startButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [_scrollView addSubview:kickOffButton];
//
//    _pageControl = [[UIPageControl alloc] init];
//    _pageControl.center = CGPointMake(kScreenWidth/2, kScreenHeight-25);
//    _pageControl.numberOfPages = _array.count;
//    _pageControl.currentPage = 0;
//    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
//    _pageControl.currentPageIndicatorTintColor =  [UIColor colorWithHexString:@"f6696a"];
//    [self.view addSubview:_pageControl];
}


//开始按钮响应事件
- (void)startButtonPressed {
    //添加标签
    [UMessage addTags:@"签到" response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
    }];
    
    BabyInfoViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"BabyInfoViewController"];
    [self presentViewController:VC animated:YES completion:nil];

}



#pragma mark - UISvrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _pageControl.currentPage = scrollView.contentOffset.x/kScreenWidth;
}


@end
