//
//  TabBarController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TabBarController.h"
#import "ViewController.h"
#import "TabBarController2.h"
#import "ParentNewHomeViewController.h"

/**全新首页-V1.4.0*/
#import "ParentHomePageViewController.h"
@interface TabBarController ()<CYTabBarDelegate>

//@property (nonatomic,strong) UIButton *AddButton;
@property (nonatomic, strong) TabBarController2 *tabbar1;

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabbar.delegate = self;
}


- (ContentView *)contentView{
    return nil;
}

#pragma mark - CYTabBarDelegate
//中间按钮点击
- (void)tabbar:(CYTabBar *)tabbar clickForCenterButton:(CYCenterButton *)centerButton{
    
    if (!_tabbar1) {
        _tabbar1 = [[TabBarController2 alloc]init];
        // 配置
        [CYTabBarConfig shared].selectIndex = 0;
        [CYTabBarConfig shared].centerBtnIndex = 4;
        [CYTabBarConfig shared].bulgeHeight = 4;
    }
    [CYTabBarConfig shared].HidesBottomBarWhenPushedOption = HidesBottomBarWhenPushedAlone;
    NSString *role = [userDefault valueForKey:KUDrole];
    if ([role  isEqual: @"kid"]) {
        [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithHexString:@"50D0F4"];
        [CYTabBarConfig shared].textColor = [UIColor colorWithHexString:@"A8A8A8"];
        [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
        [TabBarController style2:_tabbar1];
        [userDefault setObject:@"parent" forKey:KUDrole];
    }else{
        [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithRed:249/255.0f green:218/255.0f blue:97/255.0f alpha:1];
        [CYTabBarConfig shared].textColor = [UIColor grayColor];
        [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
        [userDefault setObject:@"kid" forKey:KUDrole];
        [TabBarController style1:_tabbar1];
    }
    [userDefault synchronize];
    
    
    [self presentViewController:_tabbar1 animated:YES completion:nil];
}


- (void)AnimateBegin{
    //centet button rotation
    
    //创建动画
    CATransition *animation = [CATransition animation];
    //设置运动轨迹的速度
    //    animation.timingFunction = UIViewAnimationCurveEaseIn;
    //设置动画类型为立方体动画
    animation.type = kCATransitionMoveIn;
    //设置动画时长
    animation.duration =0.3f;
    //设置运动的方向
    animation.subtype =kCATransitionFromTop;
    //控制器间跳转动画
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:animation forKey:nil];
}


//是否允许切换
- (BOOL)tabBar:(CYTabBar *)tabBar willSelectIndex:(NSInteger)index{
//    ZPLog(@"将要切换到---> %ld",index);
    
    return YES;
}
//通知切换的下标
- (void)tabBar:(CYTabBar *)tabBar didSelectIndex:(NSInteger)index{
    if (index == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationSelectIndex1 object:nil userInfo:nil];
    }
//    ZPLog(@"切换到---> %@",tabBar.items);
//    NSString *role = [userDefault valueForKey:KUDrole];
//    if ([role  isEqual: @"kid"]) {
//        if (index == 2) {
//            self.AddButton.hidden = NO;
//        } else{
//            self.AddButton.hidden = YES;
//        }
//    } else{
//        self.AddButton.hidden = YES;
//    }
}


- (UIButton *)AddButton{
    if (!_AddButton) {
        _AddButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat Y = self.view.height-64;
        if (IS_iPhoneXStyle) {
            Y = self.view.height-98;
        }
        _AddButton.frame = CGRectMake((self.view.width-60)/2, Y, 60, 60);
//        _AddButton.backgroundColor = [UIColor redColor];
        [_AddButton setTitle:@"fabu" forState:UIControlStateNormal];
        [_AddButton setTitleColor:[CYTabBarConfig shared].selectedTextColor forState:UIControlStateNormal];
        _AddButton.titleLabel.font = FontSize(10);
        [_AddButton setImage:[UIImage imageNamed:@"menu_index_gray"] forState:UIControlStateNormal];
        // button标题的偏移量
        _AddButton.titleEdgeInsets = UIEdgeInsetsMake(_AddButton.imageView.frame.size.height+20, -_AddButton.imageView.bounds.size.width, 0,0);
        // button图片的偏移量
        _AddButton.imageEdgeInsets = UIEdgeInsetsMake(0, _AddButton.titleLabel.frame.size.width/2, _AddButton.titleLabel.frame.size.height+5, -_AddButton.titleLabel.frame.size.width/2);
        
        [_AddButton addTarget:self action:@selector(fabu) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_AddButton];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        [window addSubview:_AddButton];
    }
    return _AddButton;
}


- (void)fabu{
    ZPLog(@"hahahah");
    
    [self presentViewController:[ViewController new] animated:YES completion:nil];
}

+ (void)style1:(CYTabBarController *)tabbar {
    ChildHomeViewController *childHomeVC = [KChildHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildHomeViewController"];
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:childHomeVC];
    [tabbar addChildController:nav1 title:@"首页" imageName:@"menu_index_gray" selectedImageName:@"menu_index_gray"];
    [nav1.navigationBar setBarTintColor:[CYTabBarConfig shared].selectedTextColor];
    
    childListenViewController *childListenVC = [KChildListenStoryboard instantiateViewControllerWithIdentifier:@"childListenViewController"];
    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:childListenVC];
    [tabbar addChildController:nav2 title:@"听一听" imageName:@"Btn02" selectedImageName:@"SelectBtn02"];
    [nav2.navigationBar setBarTintColor:[CYTabBarConfig shared].selectedTextColor];
    
    ChildShowViewController *childShowVC = [KChildShowStoryboard instantiateViewControllerWithIdentifier:@"ChildShowViewController"];
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:childShowVC];
    [tabbar addChildController:nav3 title:@"秀场" imageName:@"Btn01" selectedImageName:@"SelectBtn01"];
    [nav3.navigationBar setBarTintColor:[CYTabBarConfig shared].selectedTextColor];
    
    MineViewController *mineVC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
    UINavigationController *nav4 = [[UINavigationController alloc]initWithRootViewController:mineVC];
    [tabbar addChildController:nav4 title:@"我的" imageName:@"Btn02" selectedImageName:@"SelectBtn02"];
//    [tabbar addCenterController:nil bulge:YES  title:nil imageName:@"jiankang" selectedImageName:@"jiankang"];
    
}

+ (void)style2:(CYTabBarController *)tabbar {
    
//    ParentNewHomeViewController *parentHomeVC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentNewHomeViewController"];
//    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:parentHomeVC];
//    [tabbar addChildController:nav1 title:@"首页" imageName:@"tabbar_home" selectedImageName:@"tabbar_home_select"];
    
    
    ParentHomePageViewController *parentHomeVC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentHomePageViewController"];
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:parentHomeVC];
    [tabbar addChildController:nav1 title:@"首页" imageName:@"tabbar_home" selectedImageName:@"tabbar_home_select"];
    
    
    ParentVideoViewController *parentVideoVC = [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoViewController"];
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:parentVideoVC];
    [tabbar addChildController:nav3 title:@"视频" imageName:@"tabbar_video" selectedImageName:@"tabbar_video_select"];
    
//    ParentCommunityViewController *parettCommunityVC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentCommunityViewController"];
//    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:parettCommunityVC];
//    [tabbar addChildController:nav2 title:@"发现" imageName:@"tabbar_community" selectedImageName:@"tabbar_community_select"];
    
    MineViewController *mineVC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
    UINavigationController *nav4 = [[UINavigationController alloc]initWithRootViewController:mineVC];
    [tabbar addChildController:nav4 title:@"我的" imageName:@"tabbar_mine" selectedImageName:@"tabbar_mine_select"];
//    [tabbar addCenterController:nil bulge:YES title:nil imageName:@"footer" selectedImageName:@"footer"];
}

@end
