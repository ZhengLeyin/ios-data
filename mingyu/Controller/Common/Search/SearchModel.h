//
//  SearchModel.h
//  mingyu
//
//  Created by apple on 2018/6/27.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchModel : NSObject

@property (nonatomic, copy) NSString *result_type;

@property (nonatomic, strong) NSMutableDictionary *data;


@end
