//
//  BookAudioCell.m
//  mingyu
//
//  Created by apple on 2018/5/19.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BookAudioCell.h"

@implementation BookAudioCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (instancetype)theCellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"BookAudioCell";
    BookAudioCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil ) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"BookAudioCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (void)setAudiomodel:(AudioModel *)audiomodel{
    
    _audiomodel = audiomodel;
    _audio_title_lab.text = audiomodel.audio_title;
    _create_time_lab.text = [NSString transformTime2:audiomodel.create_time];
    [_play_count_button setTitle:[NSString stringWithFormat:@"%ld",audiomodel.play_count] forState:UIControlStateNormal];
    [_time_length_button setTitle:audiomodel.time_length forState:UIControlStateNormal];
    if (_totleNum < 10) {
        _audio_sort_lab.text = [NSString stringWithFormat:@"%01ld",audiomodel.audio_sort];
    } else if (10 <= _totleNum && _totleNum < 100) {
        _audio_sort_lab.text = [NSString stringWithFormat:@"%02ld",audiomodel.audio_sort];
    } else {
        _audio_sort_lab.text = [NSString stringWithFormat:@"%03ld",audiomodel.audio_sort];
    }
    if ([DFPlayer shareInstance].currentAudioModel.audio_id == audiomodel.audio_id) {
        self.audio_title_lab.textColor = [UIColor colorWithHexString:@"50D0F4"];
        self.state_imaeg.hidden = NO;
        self.audio_sort_lab.hidden = YES;
    } else{
        self.audio_title_lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        self.state_imaeg.hidden = YES;
        self.audio_sort_lab.hidden = NO;
    }
    
//    if (_audiomodel.timeWithValue == 0.0) {
//        _play_history_lab.text = @"";
//    } else if (_audiomodel.timeWithValue > 0.95){
//        _play_history_lab.text = @"已播放";
//        _play_history_lab.textColor = [UIColor colorWithHexString:@"D0D0D0"];
//
//    } else{
//        _play_history_lab.text = [NSString stringWithFormat:@"已播 %.0f%%",_audiomodel.timeWithValue*100];
//        _play_history_lab.textColor = [UIColor colorWithHexString:@"F7A429"];
//    }
    
    if (_audiomodel.is_free == 1) {
        _try_lab.hidden = NO;
    }else {
        _try_lab.hidden = YES;
    }
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
