//
//  AttentionFansCell.h
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttentionFansCell : UITableViewCell

@property (nonatomic, copy) void (^attentButtonBlock)(NSInteger state);

@property (weak, nonatomic) IBOutlet UIImageView *head_image;

@property (weak, nonatomic) IBOutlet UILabel *name_lab;

@property (weak, nonatomic) IBOutlet UILabel *introduce_lab;

@property (weak, nonatomic) IBOutlet UIButton *attentionState_button;

@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (nonatomic, strong) UserModel *usermodel;



+ (instancetype)theAttentionFansCellWithtableView:(UITableView *)tableview;


/**
 搜索
 */
- (void)showSearch;


@end
