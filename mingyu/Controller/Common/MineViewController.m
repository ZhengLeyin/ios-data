//
//  MineViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MineViewController.h"
#import "VideoHeaderView.h"


@interface MineViewController ()



@end

@implementation MineViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    [button setFrame:CGRectMake(100, 100, 60, 60)];
    
    [button setBackgroundColor:[UIColor greenColor]];
    
    [button addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *image = [UIImage imageNamed:@"menu_index_gray"];
    
    [button setTitle:@"微信" forState:UIControlStateNormal];
    
    [button setImage:image forState:UIControlStateNormal];
    
    // 如果如下设置，则title向下移动40个单位，在下，image向上移动20个单位，在上，居中显示
    
    button.titleEdgeInsets = UIEdgeInsetsMake(40.0, -image.size.width, 0.0, 0.0);
    
    button.imageEdgeInsets = UIEdgeInsetsMake(-20.0, 0.0, 0.0, -button.titleLabel.bounds.size.width);
    
    [self.view addSubview:button];
    

}

- (void)login{
    CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
}


//- (UIView *)titleHeaderView{
//    if (!_titleHeaderView) {
//
//        NSArray  *apparray= [[NSBundle mainBundle]loadNibNamed:@"VideoHeaderView" owner:nil options:nil];
//        _titleHeaderView =[apparray firstObject];
//
//        //        _titleHeaderView = UIView.new;
//        _titleHeaderView.frame = CGRectMake(0, KPlayerViewH, kScreenWidth, kHeaderViewH);
//        _titleHeaderView.backgroundColor = [UIColor yellowColor];
//        //        _titleHeaderView.titleLabel.text = @"-----";
//        //        _titleHeaderView.subTitleLabel.text = @"----";
//    }
//    return _titleHeaderView;
//}



@end
