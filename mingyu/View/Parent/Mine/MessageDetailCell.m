//
//  MessageDetailCell.m
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MessageDetailCell.h"
#import "QQMessageModel.h"
#import "MessageModelFrame.h"

@interface MessageDetailCell()
{
    UILabel *labelTime;
    UIImageView *imageView;
    UIButton *btnContent;
}

@end

@implementation MessageDetailCell

+(instancetype)theMessageDetailCellWithTableView:(UITableView *)tableView{
    static NSString *cellid = @"MessageDetailCell";
    MessageDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[MessageDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    [cell setBackgroundColor:[UIColor colorWithHexString:@"F9F9F9"]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createView];
    }
    return self;
}


//创建各个控件
-(void)createView{
    labelTime=[[UILabel alloc]init]; //添加显示时间的Label
    labelTime.font=FontSize(11);
    labelTime.textAlignment=NSTextAlignmentCenter;
    labelTime.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
    labelTime.layer.cornerRadius = 5;
    labelTime.layer.masksToBounds = YES;
    [self.contentView addSubview:labelTime];
    
    imageView=[[UIImageView alloc]init]; //添加显示头像的ImageView
    imageView.userInteractionEnabled = YES;//打开用户交互
    //初始化一个手势
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //为图片添加手势
    [imageView addGestureRecognizer:singleTap];
    
    imageView.layer.cornerRadius = 25;
    imageView.layer.masksToBounds = YES;
    [self.contentView addSubview:imageView];
    
//    btnContent=[[UIButton alloc]init]; //添加显示文字的按钮
    btnContent = [UIButton buttonWithType:UIButtonTypeCustom];
    btnContent.titleLabel.font=FontSize(15);
    btnContent.titleLabel.numberOfLines=0;
    btnContent.titleEdgeInsets=UIEdgeInsetsMake(15, 15, 15, 20);//设置按钮文字的的上 左 下 右的边距
    [self.contentView addSubview:btnContent];
    btnContent.enabled=NO;
    //    [btnContent setBackgroundColor:[UIColor whiteColor]]; //去除背景颜色即可
}

-(void)setFrameModel:(MessageModelFrame *)frameModel{
    _frameModel = frameModel;
    labelTime.frame=frameModel.timeFrame;//设置坐标
    imageView.frame=frameModel.headImageFrame;
    btnContent.frame=frameModel.btnFrame;
    
    if (frameModel.messageModel.from_user_id == [USER_ID integerValue]) {
        [btnContent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        UIImage *bgImage=[UIImage imageNamed:@"气泡_lan"]; //设置背景图片的
        [btnContent setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20) resizingMode:UIImageResizingModeStretch] forState:UIControlStateDisabled]; //拉伸图片的方法(固定图片的位置,其他部分被拉伸)
//        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDuser_head]];
        [imageView zp_setImageWithURL:[userDefault objectForKey:KUDuser_head] placeholderImage:ImageName(@"默认头像")];
    }
    else{
        [btnContent setTitleColor:[UIColor colorWithHexString:@"2C2C2C"] forState:UIControlStateNormal];
        UIImage *bgImage=[UIImage imageNamed:@"气泡_bai"]; //设置背景图片
        [btnContent setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(28, 32, 28, 32) resizingMode:UIImageResizingModeStretch] forState:UIControlStateDisabled];
//        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,frameModel.messageModel.user_header];
        [imageView zp_setImageWithURL:frameModel.messageModel.user_header placeholderImage:ImageName(@"默认头像")];
    }
    labelTime.text = frameModel.messageModel.timeStatus;
//    labelTime.text = frameModel.messageModel.create_time;
    [btnContent setTitle:frameModel.messageModel.message_content forState:UIControlStateNormal];//设置内容
    
}


- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    if (_frameModel.messageModel.from_user_id != [USER_ID integerValue]) {
        PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
        VC.user_id = _frameModel.messageModel.from_user_id;
        [[self viewController].navigationController pushViewController:VC animated:YES];
        
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
