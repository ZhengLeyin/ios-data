//
//  ParentVideoCell.h
//  mingyu
//
//  Created by apple on 2018/6/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *BGImage;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UIImageView *play_image;

/**V1.4.0 首页视频改动 加入最新标签*/
@property (weak, nonatomic) IBOutlet UILabel *contentNewLabel;
/**type == 0  其他正常界面  type == 1 首页*/
@property(nonatomic, assign) NSUInteger homePageVCOrOtherVCType;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *title_labLeftConstraint;

//@property (weak, nonatomic) IBOutlet UIButton *totle_time_Button;
//
//@property (weak, nonatomic) IBOutlet UIButton *play_count_Button;
//
//@property (weak, nonatomic) IBOutlet UILabel *source_lab;

@property (nonatomic, strong) VideoModel *videomodel;


+ (instancetype)theParentVideoCellWithTableView:(UITableView *)tableview;


@end
