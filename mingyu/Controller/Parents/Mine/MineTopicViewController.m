//
//  MineTopicViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineTopicViewController.h"

@interface MineTopicViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger PageMenuIndex;

@end

@implementation MineTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.pageMenu];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kScreenWidth, 0.5)];
    view.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    [self.view addSubview:view];
    
    MinePublicViewController *PublicVC = [[MinePublicViewController alloc] init];
    [self addChildViewController:PublicVC];
    MineCommentViewController *CommentVC = [[MineCommentViewController alloc] init];
    [self addChildViewController:CommentVC];
   
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
    [_scrollView addSubview:VC.view];
}




- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 44, kScreenWidth, kScreenHeight-NaviH-effectViewH-44);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"我的帖子",@"我的评论"];
        
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake( 40, 0, kScreenWidth-80, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:0];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}


- (IBAction)topicDraftsButton:(id)sender {
    MineTopicDraftsViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineTopicDraftsViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
