//
//  AddLocationViewController.m
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AddLocationViewController.h"
#import "STPickerArea.h"

@interface AddLocationViewController ()<STPickerAreaDelegate>


@property (weak, nonatomic) IBOutlet UITextField *NameTF;

@property (weak, nonatomic) IBOutlet UITextField *NumberTF;

@property (weak, nonatomic) IBOutlet UIButton *AreaButton;

@property (weak, nonatomic) IBOutlet UITextView *AddressTV;

@property (weak, nonatomic) IBOutlet UISwitch * Defaultswitch;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;



@end

@implementation AddLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _Defaultswitch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放

    if (_addressmodel) {
        _NameTF.text = _addressmodel.address_name;
        _NumberTF.text = _addressmodel.address_phone;
        _AddressTV.text = _addressmodel.address_detail;
        [_AreaButton setTitle:_addressmodel.area_name forState:UIControlStateNormal];
        [_Defaultswitch setOn:_addressmodel.is_default];
    }
    [self judgeUserInputStatus];
   
    // 给TextField注册内容变动通知,添加监听方法
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(judgeUserInputStatus)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(judgeUserInputStatus)
                                                 name:UITextViewTextDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}



// TextField监听方法
- (void)judgeUserInputStatus {
    if (_NameTF.text.length > 0 && _NumberTF.text.length > 0 && _AddressTV.text.length > 0 && _AreaButton.titleLabel.text.length > 0) {
        [_saveButton setBackgroundColor:[UIColor colorWithHexString:@"50D0F4"]];
        _saveButton.userInteractionEnabled = YES;
    } else{
        [_saveButton setBackgroundColor:[UIColor colorWithHexString:@"C6C6C6"]];
        _saveButton.userInteractionEnabled = NO;
    }
}


- (IBAction)AreaButton:(id)sender {
    [self.view endEditing:YES];

    STPickerArea *pickerArea = [[STPickerArea alloc]init];
    pickerArea.heightPickerComponent = 60;
    [pickerArea setDelegate:self];
    [pickerArea setContentMode:STPickerContentModeBottom];
    [pickerArea show];
}

#pragma mark - --- STPickerAreaDelegate ---
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area {
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@", province, city,area];
    [_AreaButton setTitle:text forState:UIControlStateNormal];
    
    [self judgeUserInputStatus];

}



- (IBAction)Defaultswitch:(UISwitch *)sender {
    
}


- (IBAction)saveButton:(id)sender {
    
    if (_NumberTF.text.length != 11) {
        [UIView ShowInfo:@"错误的电话号码" Inview:self.view];
        return;
    }
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL, _addressmodel ? KUpdateAddress: KAddAddress];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"address_name":_NameTF.text,
                          @"address_phone":_NumberTF.text,
                          @"area_name":_AreaButton.titleLabel.text,
                          @"address_detail":_AddressTV.text,
                          @"is_default": _Defaultswitch.on?@"1":@"0",
                          @"address_id" : @(_addressmodel.address_id)
                          };

    NSDictionary *parameters = @{
                                 @"address":[NSString convertToJsonData:dic]
                                 };
    _saveButton.userInteractionEnabled = NO;
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        _saveButton.userInteractionEnabled = YES;
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];

//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
//            UserInfoViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
//            cell.value_lab.text = text;

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        _saveButton.userInteractionEnabled = YES;

    }];

}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
