//
//  TitleNavigationView.m
//  mingyu
//
//  Created by MingYu on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "TitleNavigationView.h"

@implementation TitleNavigationView

+ (instancetype)theTitleNavigationView {
    TitleNavigationView *view = [[NSBundle mainBundle] loadNibNamed:@"TitleNavigationView" owner:nil options:nil][0];
    return view;
}

-(void)drawRect:(CGRect)rect {
 
}


- (void)setTeachermodel:(TeacherModel *)teachermodel {
    self.teacherUserImageView.layer.cornerRadius = 15;
    self.teacherUserImageView.layer.masksToBounds = YES;
    _teachermodel = teachermodel;
    [_teacherUserImageView zp_setImageWithURL:teachermodel.user_head placeholderImage:ImageName(@"默认头像")];
    _teacherNameLabel.text = teachermodel.teacher_name;
    [_attention_button addTarget:self action:@selector(handleAction:) forControlEvents:(UIControlEventTouchUpInside)];

    
   // [self changeAttentionButton];
}

-(void)handleAction:(UIButton *)sender {
    
}

- (void)changeAttentionButton {
    _attention_button.layer.cornerRadius = 3;
    _attention_button.layer.borderWidth = 1;
    if (_teachermodel.follow_status == 1) {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"+关注" forState:UIControlStateNormal];
    } else if (_teachermodel.follow_status == 2){
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"已关注" forState:UIControlStateNormal];
    } else {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"互相关注" forState:UIControlStateNormal];
    }
}


- (IBAction)attentionButton01:(id)sender {
    
    _attention_button.userInteractionEnabled = NO;
    if (_teachermodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.teacher_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _teachermodel.follow_status = follow_status;
                [self changeAttentionButton];
            } else {
                //                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
            //            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.teacher_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _teachermodel.follow_status = 1;
                [self changeAttentionButton];
            } else {
                //                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
            //            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}



@end
