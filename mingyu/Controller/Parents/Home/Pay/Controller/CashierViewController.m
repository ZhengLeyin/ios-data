//
//  CashierViewController.m
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//
/**收银台*/
#import "CashierViewController.h"
/** 收银台View */
#import "MingYuCashierView.h"
/** 收银台Model */
#import "MingYuRechargeModel.h"
/** 育币充值界面 */
#import "MingYuRechargeViewController.h"
API_AVAILABLE(ios(9.0))
@interface CashierViewController ()
/** view */
@property (nonatomic, strong) MingYuCashierView *mingYuCashierView;
/** model */
@property (nonatomic, strong) MingYuRechargeModel *model;
/**积分*/
/** model */
@property (nonatomic, assign) NSUInteger integral;
@end

@implementation CashierViewController

-(MingYuCashierView *)mingYuCashierView  API_AVAILABLE(ios(9.0)){
    if (!_mingYuCashierView) {
        _mingYuCashierView =  [[NSBundle mainBundle] loadNibNamed:@"MingYuCashierView"owner:self options:nil][0];
       _mingYuCashierView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - effectViewH);
    }
    return _mingYuCashierView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *rightImage = [[UIImage imageNamed:@"Combined Shape"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemBack)];
    
    self.title = @"收银台";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mingYuCashierView];

    #pragma mark 确认支付
    [self readyPayButton];
    
    /** 获取用户---育币和积分数*/
    [self getUserBalance];
    
}


-(void)leftBarButtonItemBack {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)readyPayButton {
    [[self.mingYuCashierView.payRechargeButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
#pragma mark 确认支付两种状态:1.余额不足 2.确认充值
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        if (self.mingYuCashierView.realPrice <0) {
            MingYuRechargeViewController *vc = [MingYuRechargeViewController new];
            vc.type = 0;
            vc.needToBuyMoney = -self.mingYuCashierView.realPrice;
            @weakify(self)
            vc.BackBlock = ^{
                @strongify(self)
                [self getUserBalance];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            NSString *integral = [NSString stringWithFormat:@"%.0f",_coursemodel.max_integral_scale *_coursemodel.present_price * 100];

            /**首先判断是积分*/
            if ([self.mingYuCashierView.integarlLabel.text integerValue] > 0 &&  (self.mingYuCashierView.integralDeductionSwitchButton.selected == YES) && ([self.mingYuCashierView.integarlLabel.text integerValue] <= [integral integerValue])) {
                _integral = [self.mingYuCashierView.integarlLabel.text integerValue];
                /** 确认支付扣除育币 */
                [self promptPurchaseAlertView];
            }else  {
                _integral = 0;
                /** 确认支付扣除育币 */
                [self promptPurchaseAlertView];
            }
        }
    }];
}

/** 提示购买弹框 */
-(void)promptPurchaseAlertView {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"您确认支付?" delegate:self cancelButtonTitle:@"取消支付" otherButtonTitles:@"立即支付", nil];
    [alertView show];
}

//监听点击事件 代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"你点击了取消");
    }else {
        /** 确认支付扣除育币 */
        [self getServerCreateoOrder];
    }
}


#pragma mark 确认支付扣除育币
-(void)getServerCreateoOrder {
    self.mingYuCashierView.payRechargeButton.userInteractionEnabled = NO;
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KServerCreateoOrder];
    NSLog(@"%@",[NSNumber numberWithFloat:self.mingYuCashierView.realPrice]);
    CGFloat money = self.mingYuCashierView.realPrice;
    NSMutableDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"commodity_id":@(_coursemodel.course_id),
                                 @"ios_money":[NSString stringWithFormat:@"%f",money],
                                 @"device_type":@"ios",
                                 @"commodity_type":@(1)
                                 }.mutableCopy;
    if (_integral > 0) {
        [parameters setObject:@(_integral) forKey:@"ideal_money"];
    }
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        ZPLog(@"%@",responseObject[@"data"][@"order_item"][@"commodity_abstract"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            self.mingYuCashierView.payRechargeButton.userInteractionEnabled = YES;
            /**通知中心 --支付成功通知*/
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationPaymentSuccessNotification object:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            self.mingYuCashierView.payRechargeButton.userInteractionEnabled = YES;
                     [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
         self.mingYuCashierView.payRechargeButton.userInteractionEnabled = YES;
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma mark 获取用户---育币和积分数
- (void)getUserBalance {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/wallet",KURL,KGetUserBalance,KUDuser_id];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _model = [MingYuRechargeModel modelWithJSON:responseObject[@"data"]];
            [_mingYuCashierView mingYuModel:_model courseModel:_coursemodel];
        } else {
//            如果没有登录 移除当前controller 登录成功或者取消登录直接回到课程详情
            if ([responseObject[@"errorCode"] isEqual: @"DEVICE_ID_ERROR"] || [responseObject[@"errorCode"] isEqual: @"ILLEGAL_TOKEN_INVALID"] || [responseObject[@"errorCode"] isEqual:@"INVALID_TOKEN"] || [responseObject[@"errorCode"] isEqual:@"OVERDUE_TOKEN"]) {
                NSMutableArray *array = self.navigationController.viewControllers.mutableCopy;
                [array removeObjectAtIndex:array.count-1];
                [self.navigationController setViewControllers:array animated:NO];
            }
        }
    } failure:^(NSError *error) {
        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

@end
