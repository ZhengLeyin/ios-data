//
//  DownloadCell.h
//  mingyu
//
//  Created by apple on 2018/4/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "downloadInfo.h"



@interface DownloadCell : UITableViewCell<YCDownloadItemDelegate>

@property (nonatomic, strong) downloadInfo *downloadinfo;

@property (weak, nonatomic) IBOutlet UIImageView *videoImage;

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (weak, nonatomic) IBOutlet UIButton *stateButton;

//@property (weak, nonatomic) IBOutlet UIImageView *stateImage;


@property (nonatomic, strong) UIView *progressview;

//- (void)setDownloadStatus:(YCDownloadStatus)status;

@end
