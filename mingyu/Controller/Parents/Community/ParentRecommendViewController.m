//
//  ParentRecommendViewController.m
//  mingyu
//
//  Created by apple on 2018/4/16.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentRecommendViewController.h"
#import "ParentArticleModel.h"
#import "BaseRecommendViewController.h"

#define pageMenuH 50
#define scrollViewHeight (kScreenHeight-pageMenuH-NaviH-TabbarH)

@interface ParentRecommendViewController ()<SPPageMenuDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@property (nonatomic, strong) NSMutableArray *labelArray;

@end

@implementation ParentRecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
    
    if (self.labelArray.count == 0) {
        [self getArticleLabelList];
    }
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationBirthdayChange object:self];
    
}

- (void)getArticleLabelList{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetArticleLabelList];
    NSDictionary *parameters = @{
                                 @"birthday":[userDefault objectForKey:KUDuser_birthday],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                ArticleLabelModel *model = [ArticleLabelModel modelWithJSON:dic];
                [self.labelArray addObject:model];
            }
            [self setuppageMenu];

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {

    }];
}

- (void)refreshTableview{
    BaseRecommendViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
    [baseVc refreshTableview];
}

- (void)setuppageMenu{
    
    [self.view addSubview:self.pageMenu];
    
    for (ArticleLabelModel *model in self.labelArray) {
        BaseRecommendViewController *baseVc = [[BaseRecommendViewController alloc] init];
        baseVc.labModel = model;
        [self addChildViewController:baseVc];
        [self.myChildViewControllers addObject:baseVc];
    }
    
    [self.view addSubview:self.scrollView];
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = _scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.myChildViewControllers.count) {
        BaseRecommendViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:baseVc.view];
        baseVc.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth, scrollViewHeight);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.labelArray.count*kScreenWidth, 0);
    }
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    //    _toIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    if (self.myChildViewControllers.count <= toIndex) {return;}
    
    UIViewController *targetViewController = self.myChildViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([targetViewController isViewLoaded]) return;
    
    targetViewController.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth, scrollViewHeight);
    [_scrollView addSubview:targetViewController.view];
    
}


- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, pageMenuH, kScreenWidth, scrollViewHeight);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.scrollEnabled = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        NSMutableArray *array = [NSMutableArray array];
        for (LabelListModel *model in self.labelArray) {
            [array addObject:model.label_name];
        }
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, pageMenuH) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        [_pageMenu setItems:array selectedItemIndex:0];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(14);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}




#pragma mark - getter
- (NSMutableArray *)myChildViewControllers {
    
    if (!_myChildViewControllers) {
        _myChildViewControllers = [NSMutableArray array];
        
    }
    return _myChildViewControllers;
}


- (NSMutableArray *)labelArray{
    if (!_labelArray) {
        _labelArray = [NSMutableArray array];
    }
    return _labelArray;
}



@end
