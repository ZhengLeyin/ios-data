//
//  HomePageModel.m
//  mingyu
//
//  Created by MingYu on 2018/12/25.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageModel.h"

@implementation HomePageModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"data" : HomePageData.class};
}

@end
@implementation HomePageData

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"subMenuMessageList" : Submenumessagelist.class};
}

@end


@implementation Submenumessagelist

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"objectList" : Objectlist.class};
}

@end


@implementation Objectlist

@end


