//
//  SearchView.m
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchView.h"


@interface SearchView()

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *hotArray;
@property (nonatomic,strong) NSMutableArray *historyArray;
@property (nonatomic,strong) UIView *searchHistoryView;
@property (nonatomic,strong) UIView *hotSearchView;
@end

@implementation SearchView

- (instancetype)initWithFrame:(CGRect)frame hotArray:(NSMutableArray *)hotArr historyArray:(NSMutableArray *)historyArr{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.historyArray = historyArr;
        self.hotArray = hotArr;
        [self addSubview:self.searchHistoryView];
        [self addSubview:self.hotSearchView];
    }
    return self;
}


- (UIView *)hotSearchView{
    if (!_hotSearchView) {
        self.hotSearchView = [self setViewWithOriginY:CGRectGetHeight(_searchHistoryView.frame) title:@"热门搜索" textArr:self.hotArray];
//        self.hotSearchView.backgroundColor = [UIColor redColor];
    }
    return _hotSearchView;
}



- (UIView *)searchHistoryView{
    if (!_searchHistoryView) {
        if (_historyArray.count > 0) {
            self.searchHistoryView = [self setViewWithOriginY:0 title:@"最近搜索" textArr:self.historyArray];
        } else{
            self.searchHistoryView = [self setNoHistoryView];
        }
    }
    return _searchHistoryView;
}


- (UIView *)setViewWithOriginY:(CGFloat)riginY title:(NSString *)title textArr:(NSMutableArray *)textArr{
    
    UIView *view = [[UIView alloc]init];
    UILabel *titleL = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, kScreenWidth-30-45, 30)];
    titleL.text = title;
    titleL.font = FontSize(12);
    titleL.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    titleL.textAlignment = NSTextAlignmentLeft;
    [view addSubview:titleL];
    
    if ([title isEqualToString:@"最近搜索"]) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(kScreenWidth-90, 10, 80, 30);
        [btn setImage:ImageName(@"搜索清除") forState:UIControlStateNormal];
        [btn setTitle:@" 清空历史" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
        btn.titleLabel.font = FontSize(12);
        [btn addTarget:self action:@selector(clearnSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
    }
    
    CGFloat y = 10+40;
    CGFloat leftWidth = 15;
    for (int i = 0; i < textArr.count; i++) {
        if (i > 10) {
            break;
        }
        NSString *text = textArr[i];
        if (text.length > 10) {
            text = [text substringToIndex:10];
           
        }
        CGFloat width = [self getWidthWithStr:text]+30;
        if (leftWidth + width + 15 > kScreenWidth) {
            if (y >= 130 && [title isEqualToString:@"最近搜索"]) {
                [self removeTestDataWithTextArr:textArr index:i];
                break;
            }
            y += 40;
            leftWidth = 15;
        }
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(leftWidth, y, width, 30)];
        label.userInteractionEnabled = YES;
        label.font = FontSize(12);
        label.text = text;
//        label.layer.borderWidth = 0.5;
        label.backgroundColor = [UIColor colorWithHexString:@"F6F6F6"];
        label.layer.cornerRadius = 15;
        label.layer.masksToBounds = YES;
        label.textAlignment = NSTextAlignmentCenter;
//        if (i % 2 == 0 && [title isEqualToString:@"热门搜索"]) {
//            label.layer.borderColor = KColor(255, 148, 153).CGColor;
//            label.textColor = KColor(255, 148, 153);
//        } else {
//            label.textColor = KColor(111, 111, 111);
//            label.layer.borderColor = KColor(227, 227, 227).CGColor;
//        }
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tagDidCLick:)] ];
        [view addSubview:label];
        leftWidth += width +10;
    }
    
    if ([title isEqualToString:@"最近搜索"]) {
        UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(0, y+40, kScreenWidth, 10)];
        lineview.backgroundColor = [UIColor colorWithHexString:@"f9f9f9"];
        [view addSubview:lineview];
    }
    
    view.frame = CGRectMake(0, riginY, kScreenWidth, y+40+10);
    return view;
}


- (UIView *)setNoHistoryView{
    
//    UIView *historyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 80)];
//    UILabel *titleL = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, kScreenWidth-30, 30)];
//    titleL.text = @"最近搜索";
//    titleL.font = FontSize(12);
//    titleL.textColor = [UIColor blackColor];
//    titleL.textAlignment = NSTextAlignmentLeft;
//
//    UILabel *notextL = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(titleL.frame)+10, 100, 20)];
//    notextL.text = @"无搜索记录";
//    notextL.font = FontSize(12);
//    notextL.textColor = [UIColor blackColor];
//    notextL.textAlignment = NSTextAlignmentLeft;
//    [historyView addSubview:titleL];
//    [historyView addSubview:notextL];
//    UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(notextL.frame)+10 , kScreenWidth, 10)];
//    lineview.backgroundColor = [UIColor colorWithHexString:@"f9f9f9"];
//    [historyView addSubview:lineview];
    UIView *historyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
    
    return historyView;
    
}


- (void)tagDidCLick:(UITapGestureRecognizer *)tap{
    UILabel *lab = (UILabel *)tap.view;
    if (self.tabAction) {
        self.tabAction(lab.text);
    }
}

- (CGFloat)getWidthWithStr:(NSString *)text{
    CGFloat width = [text boundingRectWithSize:CGSizeMake(kScreenWidth, 40) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:FontSize(12)} context:nil].size.width;
    
    return width;
}


-(void)clearnSearchHistory:(UIButton *)sender{
    [self.searchHistoryView removeFromSuperview];
    self.searchHistoryView = [self setNoHistoryView];
    [_historyArray removeAllObjects];
    [NSKeyedArchiver archiveRootObject:_historyArray toFile:KHistorySearchPath];
    [self addSubview:self.searchHistoryView];
    CGRect frame = _searchHistoryView.frame;
    frame.origin.y = CGRectGetHeight(_searchHistoryView.frame);
//    _hotSearchView.frame = frame;
    _hotSearchView.mj_y = frame.origin.y;
}

- (void)removeTestDataWithTextArr:(NSMutableArray *)testArr index:(int)index{
    
    NSRange range = {index, testArr.count-index-1};
    [testArr removeObjectsInRange:range];
    [NSKeyedArchiver archiveRootObject:testArr toFile:KHistorySearchPath];
}

@end
