//
//  ParentCouresCell.m
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentCouresCell.h"

@implementation ParentCouresCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_course_img.bounds cornerRadius:4];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _course_img.bounds;
    maksLayer.path = maksPath.CGPath;
    _course_img.layer.mask = maksLayer;

    _originalPriceOrIntegralLabel.hidden = NO;
    _privilegedClassLabel.hidden = YES;
    _privilegedClassImageView.hidden = YES;
 
}

#pragma mark    /**现在价格处理 ¥:12 px 价格: 18px 免费字体: 16px*/  免费暂时不用处理
-(NSMutableAttributedString *)attStrlabelText:(NSString *)labelText {
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:labelText];
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],
                            NSFontAttributeName:[UIFont boldSystemFontOfSize:18]} range:NSMakeRange(0,labelText.length)];//添加属性
    
    //添加图片
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"育币"];
    attach.bounds = CGRectMake(0, -1, 12, 12);
    NSAttributedString *attributeStr2 = [NSAttributedString attributedStringWithAttachment:attach];
    [attStr insertAttributedString:attributeStr2 atIndex:0];
    return attStr;
}

#pragma mark    /** 原价划删除线处理: 12px  */
-(NSMutableAttributedString *)attStrOriginalPriceOrIntegralLabel:(NSString *)labelText {
     _originalPriceOrIntegralLabel.font = FontSize(12.0);
    NSString *oldPrice = labelText;
    NSUInteger length = [oldPrice length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:oldPrice];
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor colorWithHexString:@"#D0D0D0"] range:NSMakeRange(0, length)];
    
    _originalPriceOrIntegralLabel.textColor = [UIColor colorWithHexString:@"#D0D0D0"];
    _originalPriceOrIntegralLabel.layer.borderColor = [UIColor clearColor].CGColor;
    _originalPriceOrIntegralLabel.layer.borderWidth = 0;
    
    return attri;
}

#pragma mark 积分抵扣: 11px
-(void)setintegralLabel:(CGFloat )max_integral_scale {
    _originalPriceOrIntegralLabel.text = [NSString stringWithFormat:@"积分抵%.0f%% ",max_integral_scale*100];
    _originalPriceOrIntegralLabel.textColor = [UIColor colorWithHexString:@"#F4744E"];
    _originalPriceOrIntegralLabel.layer.masksToBounds = YES;
    _originalPriceOrIntegralLabel.layer.cornerRadius = 3;
    _originalPriceOrIntegralLabel.font = FontSize(11.0);
    _originalPriceOrIntegralLabel.layer.borderColor = [UIColor colorWithHexString:@"#F4744E"].CGColor;
    _originalPriceOrIntegralLabel.layer.borderWidth = 0.5;
}

+ (instancetype)theCellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"ParentCouresCell";
    ParentCouresCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ParentCouresCell" owner:nil options:nil][0];
    }
    return cell;
}

+ (instancetype)theCellWithPurchaseCourseTableView:(UITableView *)tableView {
    static NSString *cellID = @"ParentCouresCell";
    ParentCouresCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"ParentCouresCell" owner:nil options:nil][1];
    }
    return cell;
}

/**精品课界面*/
- (void)setCoursemodel:(CourseModel *)coursemodel{
    _coursemodel = coursemodel;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    [_course_img sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _course_title.text = coursemodel.course_title;
    _course_abstract.text = [NSString stringWithFormat:@"%@ %@ %@",_coursemodel.teacher_name,_coursemodel.teacher_organize,_coursemodel.teacher_position];
    if (_coursemodel.course_type == 1) {//视频
        _course_typeImageView.image = [UIImage imageNamed:@"ExcellentClassVideo"];
        _listeningTestLabel.text = @"试看";
    } else { // 音频
        _listeningTestLabel.text = @"试听";
       _course_typeImageView.image = [UIImage imageNamed:@"ExcellentClassVoice"];
    }

    if (_coursemodel.hold_num) {
        _purchaseQuantityLabel.text = [NSString stringWithFormat:@"购买人数:%ld",_coursemodel.hold_num];
    }else {
        _purchaseQuantityLabel.text = @"购买人数:0";
    }
    
    if (_coursemodel.max_integral_scale > 0.0) {
#pragma mark 现价  积分抵扣
        _present_price.hidden = _originalPriceOrIntegralLabel.hidden = NO;
        _originalPriceOrIntegralLabel.hidden = NO;
        _present_price.hidden = NO;
        [_present_price  setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@" %.1f", _coursemodel.present_price]]];
        [self setintegralLabel:_coursemodel.max_integral_scale];
    }else {
        if (_coursemodel.present_price && _coursemodel.original_price) {
#pragma mark 现价 原价
            _present_price.hidden = _originalPriceOrIntegralLabel.hidden = NO;
            [_present_price  setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@" %.1f", _coursemodel.present_price]]];
            [_originalPriceOrIntegralLabel setAttributedText:[self attStrOriginalPriceOrIntegralLabel:[NSString stringWithFormat:@" %.1f", _coursemodel.original_price]]];
            self.centerConstraint.constant = 1.5;
            
        }else if (_coursemodel.present_price) {
#pragma mark 现价  不打折
            _originalPriceOrIntegralLabel.hidden = YES;
            _present_price.hidden = NO;
            [_present_price  setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@" %.1f", _coursemodel.present_price]]];
        } else {
#pragma mark 免费
            _originalPriceOrIntegralLabel.hidden = YES;
            //_purchaseQuantityLabel.text = [NSString stringWithFormat:@":%ld",_coursemodel.hold_num];
            _present_price.hidden = NO;
            _present_price.text = @"免费";
            _present_price.font = FontSize(16.0);
            _present_price.textColor = [UIColor colorWithHexString:@"#60E3A1"];
        }
    }
#pragma mark 特权课
    //MingYuLoggerD(@"%@",_coursemodel.user_prerogative ? @"YES":@"NO");
    if (_coursemodel.course_prerogative == 1) {
        _privilegedClassLabel.hidden = NO;
        _privilegedClassImageView.hidden = NO;
    }else {
        _privilegedClassLabel.hidden = YES;
        _privilegedClassImageView.hidden = YES;
    }
    
}

/**已购课程界面*/

-(void)purchasedCourseViewControllerPushModel:(CourseModel *)coursemodel {
    _coursemodel = coursemodel;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    [_course_img sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _course_title.text = coursemodel.course_title;
    _course_abstract.text = [NSString stringWithFormat:@"%@ %@ %@",coursemodel.teacher_name,coursemodel.teacher_organize,coursemodel.teacher_position];
    if (coursemodel.course_type == 1) {//视频
        _course_typeImageView.image = [UIImage imageNamed:@"ExcellentClassVideo"];
        _listeningTestLabel.text = @"试看";
    } else { // 音频
        _listeningTestLabel.text = @"试听";
        _course_typeImageView.image = [UIImage imageNamed:@"ExcellentClassVoice"];
    }
    _purchaseDateLabel.text = coursemodel.time_tatus;
    MingYuLoggerD(@"price==%lf",coursemodel.present_price);
    [_present_price  setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@" %.1f", coursemodel.present_price]]];
    
#pragma mark 特权课
    //MingYuLoggerD(@"%@",_coursemodel.user_prerogative ? @"YES":@"NO");
    if (_coursemodel.course_prerogative == 1) {
        _privilegedClassLabel.hidden = NO;
        _privilegedClassImageView.hidden = NO;
    }else {
        _privilegedClassLabel.hidden = YES;
        _privilegedClassImageView.hidden = YES;
    }
    
}

//搜索时用到
- (void)showSearch{
    NSString *title = _coursemodel.course_title;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _course_title.attributedText = attrStr;
    _course_title.lineBreakMode = NSLineBreakByTruncatingTail;

    
    NSString *content = [NSString stringWithFormat:@"%@ %@ %@",_coursemodel.teacher_name,_coursemodel.teacher_organize,_coursemodel.teacher_position];;
    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    content = [content stringByAppendingString:@"</font>"];
    
    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _course_abstract.attributedText = attrStr2;
    _course_abstract.lineBreakMode = NSLineBreakByTruncatingTail;

}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
