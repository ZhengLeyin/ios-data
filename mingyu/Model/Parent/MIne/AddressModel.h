//
//  AddressModel.h
//  mingyu
//
//  Created by apple on 2018/6/6.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

@property (nonatomic, assign) NSInteger address_id;

@property (nonatomic, assign) NSInteger fk_user_id;

@property (nonatomic, copy) NSString *address_name;

@property (nonatomic, copy) NSString *address_phone;

@property (nonatomic, copy) NSString *area_name;

@property (nonatomic, copy) NSString *address_detail;

@property (nonatomic, assign) NSInteger is_default;

//private Integer address_id;//收货地址id
//private Integer fk_user_id;//外键
//private String address_name;//收货人
//private String address_phone;//收货电话
//private String area_name;//地区名
//private Integer area_id;//地区ID
//private String address_detail;//详细地址
//private Integer is_default;//地址类型 1-表示默认地址


@end
