//
//  AlbumDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SubjectModel;

@interface AlbumDetailViewController : UIViewController

@property (nonatomic, assign) NSInteger target_id;

@property (nonatomic, assign) NSInteger album_id;

@end
