//
//  TopicListViewController.h
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ParentCircleModel;

@interface TopicListViewController : UIViewController

@property (nonatomic, strong) ParentCircleModel *circlemodel;

@property (nonatomic, copy) NSString *topic_type;


@end
