//
//  RotateButton.h
//  mingyu
//
//  Created by apple on 2018/5/22.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotateButton : UIButton

@property (nonatomic, copy) void (^RotateButtonBlock)(void);

/**单利方法*/
//+ (RotateButton *)shareInstance;


-(void) startRotating;
-(void) stopRotating;
-(void) resumeRotate;  


@end
