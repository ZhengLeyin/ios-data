//
//  ParentHomePageFunctionModuleCell.m
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ParentHomePageFunctionModuleCell.h"

@implementation ParentHomePageFunctionModuleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)parentHomePageFunctionModuleCellOfModel:(ExcellentClassModel *)model {
    
    _functionModelLabel.text = model.icon_name;
    
    [_functionModuleImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,model.icon_image]] placeholderImage:ImageName(@"icon_childread-V1.4.0")];
}

@end
