//
//  PlayerTimerManager.m
//  mingyu
//
//  Created by apple on 2018/5/16.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PlayerTimerManager.h"
#import "TimeListView.h"
@implementation PlayerTimerManager


+(PlayerTimerManager *)shareInstance{
    static PlayerTimerManager *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}


- (void)creatTiemr{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    
   _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,0, 0,queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL,0),1.0*NSEC_PER_SEC,0); //每秒执行
}


- (void)countDown{
    if (_timeout >0) {
        
        dispatch_source_set_event_handler(_timer, ^{
            
            if(_timeout<=0){//倒计时结束，关闭
                if (_timeout == 0) {
                    [[DFPlayer shareInstance] df_audioPause];
                     AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    app.PlayTime = 0;
                }
                dispatch_source_cancel(_timer);
            }else{
                _timeout--;
//                ZPLog(@"%ld",(long)_timeout);
            }
        });
        dispatch_resume(_timer);
    }
}

@end
