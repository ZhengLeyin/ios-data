//
//  VideoContentCell.m
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoContentCell.h"



@implementation VideoContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setVideomodel:(VideoModel *)videomodel{
    _video_title.text = videomodel.video_title;
    _time_length.text = [NSString stringWithFormat:@"时长 %@", videomodel.time_length];
    NSURL *URL = [NSURL URLWithString:videomodel.video_img];
    [_video_img sd_setImageWithURL:URL placeholderImage:ImageName(@"placeholderImage")];
}

@end
