//
//  UIImage+Extension.m
//  mingyu
//
//  Created by MingYu on 2018/11/17.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "UIImage+Extension.h"

@implementation UIImage (Extension)

+ (UIImage *)imageWithRoundCorner:(UIImage *)sourceImage cornerRadius:(CGFloat)cornerRadius size:(CGSize)size {
    CGFloat scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGRect bounds = CGRectMake(0, 0, size.width, size.height);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:cornerRadius];
    [path addClip];
    [sourceImage drawInRect:bounds];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)imageWithSize:(CGSize)sizeToFit radius:(CGFloat)radius  Color:(UIColor *)color {
    UIGraphicsBeginImageContextWithOptions(sizeToFit, true, UIScreen.mainScreen.scale);
    //设置上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    ///首先为了不让其有像素混合的问题,先把背景绘制出来
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, sizeToFit.width, sizeToFit.height));
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    //边框大小
    CGContextSetLineWidth(context, 1.0f);
    //边框颜色
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    //矩形填充颜色
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetAlpha(context, 1.0f);
    CGFloat height = sizeToFit.height;
    CGFloat width = sizeToFit.width;
    CGContextMoveToPoint(context, width/2 , 0);  // 开始上线中间开始
    CGContextAddArcToPoint(context, width , 0, width , height/2 , radius);  // 右上角角度
    CGContextAddArcToPoint(context, width, height , width/2, height , radius); // 右下角角度
    CGContextAddArcToPoint(context, 0, height, 0 , height/2, radius); // 左下角
    CGContextAddArcToPoint(context, 0 , 0, width/2 , 0 , radius); // 左上角
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    UIImage *outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outImage;
}

// UIBezierPath 裁剪
+ (UIImage *)UIBezierPathClip:(UIImage *)img cornerRadius:(CGFloat)c {
    int w = img.size.width * img.scale;
    int h = img.size.height * img.scale;
    CGRect rect = CGRectMake(0, 0, w, h);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), true, 1.0);
    //设置上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    ///首先为了不让其有像素混合的问题,先把背景绘制出来
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, w, h));
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:c] addClip];
    [img drawInRect:rect];
    UIImage *ret = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return ret;
}

/** CGContext 裁剪 切圆角  优先推荐:滑动流畅  */
+ (UIImage *)CGContextClip:(UIImage *)img cornerRadius:(CGFloat)c {
    int w = img.size.width * img.scale;
    int h = img.size.height * img.scale;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), true, 1.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    ///首先为了不让其有像素混合的问题,先把背景绘制出来
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, w, h));
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    CGContextMoveToPoint(context, 0, c);
    CGContextAddArcToPoint(context, 0, 0, c, 0, c);
    CGContextAddLineToPoint(context, w-c, 0);
    CGContextAddArcToPoint(context, w, 0, w, c, c);
    CGContextAddLineToPoint(context, w, h-c);
    CGContextAddArcToPoint(context, w, h, w-c, h, c);
    CGContextAddLineToPoint(context, c, h);
    CGContextAddArcToPoint(context, 0, h, 0, h-c, c);
    CGContextAddLineToPoint(context, 0, c);
    CGContextClosePath(context);
    
    CGContextClip(context);     // 先裁剪 context，再画图，就会在裁剪后的 path 中画
    [img drawInRect:CGRectMake(0, 0, w, h)];       // 画图
    CGContextDrawPath(context, kCGPathFill);
    
    UIImage *ret = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return ret;
}

// 非等比缩放，生成的图片可能会被拉伸
+(UIImage *)scaleNoImage:(UIImage *)image size:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, true, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return outImage;
}

// 等比缩放
+ (UIImage *)scaleImage:(UIImage *)image size:(CGSize)size {
    CGFloat scale =  [UIScreen mainScreen].scale;
    
    // 这一行至关重要
    // 不要直接使用UIGraphicsBeginImageContext(size);方法
    // 因为控件的frame与像素是有倍数关系的
    // 比如@1x、@2x、@3x图，因此我们必须要指定scale，否则黄色去不了
    // 因为在5以上，scale为2，6plus scale为3，所生成的图是要合苹果的
    // 规格才能正常
    UIGraphicsBeginImageContextWithOptions(size, true, scale);
    
    CGFloat rateWidth = size.width / image.size.width;
    CGFloat rateHeight = size.height / image.size.height;
    
    CGFloat rate = MIN(rateHeight, rateWidth);
    CGSize aspectFitSize = CGSizeMake(image.size.width * rate, image.size.height * rate);
    [image drawInRect:CGRectMake(0, 0, aspectFitSize.width, aspectFitSize.height)];
    UIImage *outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outImage;
}



+ (UIImage *)imageWithColor:(UIColor *)color forSize:(CGSize)size{
    if (size.width <= 0 || size.height<= 0 )
    {
        return nil;
    }
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end
