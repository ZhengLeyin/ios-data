//
//  IdentityViewController.m
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "IdentityViewController.h"

@interface IdentityViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *mamImage;

@property (weak, nonatomic) IBOutlet UIImageView *dadImage;

@end

@implementation IdentityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_user_identity == 1) {
        _dadImage.image = ImageName(@"一键听_xuanz_gou");
        _mamImage.image = ImageName(@"一键听_xuanz");
        _user_identity  = 1;
    } else {
        _dadImage.image = ImageName(@"一键听_xuanz");
        _mamImage.image = ImageName(@"一键听_xuanz_gou");
        _user_identity  = 2;
    }
}




- (IBAction)mamButton:(id)sender {
    _dadImage.image = ImageName(@"一键听_xuanz");
    _mamImage.image = ImageName(@"一键听_xuanz_gou");
    _user_identity  = 2;

}


- (IBAction)dadButton:(id)sender {
    _dadImage.image = ImageName(@"一键听_xuanz_gou");
    _mamImage.image = ImageName(@"一键听_xuanz");
    _user_identity  = 1;

}


- (IBAction)saveButton:(id)sender {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"user_identity": [NSString stringWithFormat:@"%ld",_user_identity] ,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}




- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
