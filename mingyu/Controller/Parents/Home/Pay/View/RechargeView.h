//
//  RechargeView.h
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MingYuRechargeModel.h"
NS_ASSUME_NONNULL_BEGIN

/**
 *  类型自定义
 */
typedef void (^ReturnValueBlock) (MingYuRechargeModel *model);
@interface RechargeView : UIView
/**
 *  声明一个ReturnValueBlock属性，这个Block是获取传值的界面传进来的
 */
@property(nonatomic, copy) ReturnValueBlock returnValueBlock;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet UIView *view5;
@property (weak, nonatomic) IBOutlet UIView *view6;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint5;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint6;


@property (weak, nonatomic) IBOutlet UILabel *moneyLab1;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab2;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab3;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab4;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab5;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab6;


@property (weak, nonatomic) IBOutlet UILabel *yubiLab1;
@property (weak, nonatomic) IBOutlet UILabel *yubiLab2;
@property (weak, nonatomic) IBOutlet UILabel *yubiLab3;
@property (weak, nonatomic) IBOutlet UILabel *yubiLab4;
@property (weak, nonatomic) IBOutlet UILabel *yubiLab5;
@property (weak, nonatomic) IBOutlet UILabel *yubiLab6;

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *topLab;

/** 充值按钮 */
@property (weak, nonatomic) IBOutlet UIButton *rechargeButton;

@property(nonatomic ,strong) MingYuRechargeModel *model;

@property(nonatomic ,strong) NSMutableArray *modelArray;

-(void)array:(NSMutableArray *)array type:(NSUInteger )type;
@end

NS_ASSUME_NONNULL_END
