//
//  CourseVideoCell.m
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CourseVideoCell.h"

@implementation CourseVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tryLabel.hidden = YES;
//    _video_img
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_video_img.bounds cornerRadius:3];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _video_img.bounds;
    maskLayer.path = maskPath.CGPath;
    _video_img.layer.mask = maskLayer;
}

+ (instancetype)theCourseVideoCellWithtableView:(UITableView *)tableview{
    static NSString *cellid = @"CourseVideoCell";
    CourseVideoCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CourseVideoCell" owner:nil options:nil][0];
    }
    return cell;
}

- (void)setVideomodel:(VideoModel *)videomodel {
    if (videomodel.is_free == 1) {
        self.tryLabel.hidden = NO;
    }else {
        self.tryLabel.hidden = YES;
    }
    _video_title.text = videomodel.section_title;
    _time_length.text = [NSString stringWithFormat:@"时长 %@", videomodel.time_length];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,videomodel.section_img]];
    [_video_img sd_setImageWithURL:URL placeholderImage:ImageName(@"placeholderImage")];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

  
}


@end
