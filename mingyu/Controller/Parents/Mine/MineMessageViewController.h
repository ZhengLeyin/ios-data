//
//  MineMessageViewController.h
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineMessageViewController : UIViewController

@property (nonatomic, assign) NSInteger selectedItem;

@property (nonatomic, assign) NSInteger qq_unread_number;

@end
