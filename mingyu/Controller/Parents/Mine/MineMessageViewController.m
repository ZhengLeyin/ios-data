//
//  MineMessageViewController.m
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineMessageViewController.h"
#import "PPBadgeView.h"

@interface MineMessageViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger PageMenuIndex;

@end

@implementation MineMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_qq_unread_number) {
        [self.navigationController.navigationBar pp_addDotWithColor:[UIColor redColor]];
        if (IS_iPhone_5) {
            [self.navigationController.navigationBar pp_moveBadgeWithX:-60 Y:10];
        } else {
            [self.navigationController.navigationBar pp_moveBadgeWithX:-90 Y:10];
        }
    }

    UIView *titleView = [[UIView alloc] init];
    titleView.frame = CGRectMake(0, 0, self.view.width-kScale(120), 44);
    self.navigationItem.titleView = titleView;
    [titleView addSubview:self.pageMenu];
    
    SystemMessageViewController *SystemMessageVC = [[SystemMessageViewController alloc] init];
    [self addChildViewController:SystemMessageVC];
    PrivateMessageViewController *PrivateMessageVC = [[PrivateMessageViewController alloc] init];
    [self addChildViewController:PrivateMessageVC];
    
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar pp_hiddenBadge];
}

#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
//    if (toIndex == 1) {
//        [_rightButton setImage:ImageName(@"刷新") forState:UIControlStateNormal];
//    } else{
//        [_rightButton setImage:ImageName(@"发帖") forState:UIControlStateNormal];
//    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH);
    [_scrollView addSubview:VC.view];
}



- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"系统消息",@"私信"];
        
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  self.view.width-kScale(120), 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor clearColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:_selectedItem];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
        _pageMenu.dividingLine.hidden = YES;

    }
    return _pageMenu;
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
