//
//  HomePageViewModel.m
//  mingyu
//
//  Created by MingYu on 2018/12/24.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageViewModel.h"
#import "ExcellentClassModel.h"
#import "HomePageModel.h"
#import "HomePageMoreModel.h"
@implementation HomePageViewModel

/**获取首页标签   type =8   type == 9 头部信息**/
-(void)getAppMeunAgeRelatedType:(NSUInteger)type {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KAppMenuType,@(type)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        BOOL success = [responseObject[@"success"] boolValue];
        if (success) {
            NSArray *data = responseObject[@"data"];
            [self.homePageViewModelDetegate setValue:data];
        }
    } failure:^(NSError *error) {
       
    }];
}

/** 根据菜单id获取信息 */
-(void)getKHomePageMenuId:(NSUInteger)menu_id  {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@",KURL,KHomePageMenuId,@(menu_id)];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        BOOL success = [responseObject[@"success"] boolValue];
        if (success) {
            HomePageModel *model = [HomePageModel modelWithJSON:responseObject];
            MingYuLoggerI(@"%@",responseObject);
            self.returnReturnDataBlock(model.data);
        }
    } failure:^(NSError *error) {
        
    }];
}

/** 获取首页更多获取信息 */
/**1.视频 2.资讯 3.主题(一键听) 4.课程 5.育儿必读*/
-(void)getKHomePageMoreMenuId:(NSUInteger)menu_id start:(NSUInteger)start startAlbum:(NSUInteger)startAlbum  lastPage:(BOOL)lastPage  {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/subMenuMessage?",KURL,KHomePageMenuId,@(menu_id)];
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 1; i < 6; i++) {
        if (i == 3) {
            NSDictionary *dic = @{
                                  @"type":@(i),
                                  @"start":@(startAlbum),
                                  @"size":@1,
                                  };
              [array addObject:dic];
        }else {
            NSDictionary *dic = @{
                                  @"type":@(i),
                                  @"start":@(start),
                                  @"size":@2,
                                  };
               [array addObject:dic];
        }
    }
    NSData *data=[NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    MingYuLoggerI(@"%@",jsonStr);
    NSDictionary *parameters = @{
                                 @"paging":jsonStr
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        BOOL success = [responseObject[@"success"] boolValue];
        if (success) {
            NSMutableArray *array = [NSMutableArray array];
            HomePageMoreModel *model = [HomePageMoreModel modelWithJSON:responseObject];
            MingYuLoggerD(@"model%@",responseObject);
            [array addObjectsFromArray:model.data.subMenuMessageList];
            self.returnReturnDataMoreBlock(array.mutableCopy, lastPage, start+2, startAlbum+1);
            //self.returnReturnDataBlock(array.mutableCopy);
        }
    } failure:^(NSError *error) {

    }];
}

/** 获取功能模块信息 */
-(void)getHomePageApiIcon {
    NSMutableArray *array = [NSMutableArray array];
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KHomePageApiIcon];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        BOOL success = [responseObject[@"success"] boolValue];
        if (success) {
            for (NSDictionary *dic in responseObject[@"data"]) {
                ExcellentClassModel *model = [ExcellentClassModel modelWithJSON:dic];
                [array addObject:model];
            }
          [self.homePageViewModelDetegate setValue:array.mutableCopy];
        }
    } failure:^(NSError *error) {
    }];
}

@end
