//
//  CVContentViewController.m
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CVContentViewController.h"
#import "CourseVideoCell.h"

@interface CVContentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;



@end

@implementation CVContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    NSLog(@"id===================%ld",_courseId);
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (_type == 0) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 50);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);;
        }
    }];
    
    [self GetCourseSection];
}


#pragma mark  获取课程目录
- (void)GetCourseSection{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KGetCourseSection,@(_courseId)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@"0",
                                 @"size":@"10000"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data =  json[@"data"];
            if (data.count > 0) {
                [self.dataArray removeAllObjects];
                for (NSDictionary *dic in data) {
                    VideoModel *listmodel = [VideoModel modelWithJSON:dic];
                    [self.dataArray addObject:listmodel];
                }
                VideoModel *firstmodel = _dataArray[0];
                firstmodel.selectvideo = YES;
                [self.tableView reloadData];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_dataArray && _dataArray.count > 0) {
        return _dataArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CourseVideoCell *cell = [CourseVideoCell theCourseVideoCellWithtableView:tableView];
    if (self.dataArray && self.dataArray.count > indexPath.row) {
        VideoModel *model = [_dataArray objectAtIndex:indexPath.row];
        cell.videomodel = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (model.selectvideo) {
            cell.video_title.textColor = [UIColor colorWithHexString:@"#50D0F4"];
        } else {
            cell.video_title.textColor = [UIColor blackColor];
        }
    }
    return cell;
}

// 点击cell--取消上一次选择的cell
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray && self.dataArray.count > indexPath.row) {
        VideoModel *model = [_dataArray objectAtIndex:indexPath.row];
        model.selectvideo = NO;
        CourseVideoCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.video_title.textColor  = [UIColor blackColor];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray && self.dataArray.count > indexPath.row) {
        VideoModel *model = [_dataArray objectAtIndex:indexPath.row];
        CourseVideoCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        CourseVideoCell *firstcell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        VideoModel *firstmodel = _dataArray[0];

        if (model.section_path.length == 0) {
            firstcell.video_title.textColor  = [UIColor colorWithHexString:@"#50D0F4"];
            firstmodel.selectvideo = YES;
            model.selectvideo = NO;
        } else {
            firstcell.video_title.textColor = [UIColor blackColor];
            firstmodel.selectvideo = NO;
            cell.video_title.textColor  = [UIColor colorWithHexString:@"#50D0F4"];
            model.selectvideo = YES;
        }
        if (self.selectVideoBlock) {
            self.selectVideoBlock(model);
        }
    }
}


-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 79;
        
    }
    return _tableView;
}

- (NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

@end
