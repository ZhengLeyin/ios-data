//
//  MineFansViewController.m
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineFansViewController.h"
#import "JXPagerView.h"
#import "TeacherViewController.h"

@interface MineFansViewController ()<UITableViewDelegate,UITableViewDataSource,JXPagerViewListViewDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, strong) NothingView *nothingView;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;


@end

@implementation MineFansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        /**type == 1 精品课传过来的*/
        if (_type == 1) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 45);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);;
        }
    }];
    
    [self refreshTableview];
}




- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //    ZPLog(@"%@",_topic_type);
}


//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.arrayData.count>0) {
            [self.arrayData removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self getFollowUserList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getFollowUserList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


- (void)getFollowUserList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetFollowUserList];
    if (_from_id > 0) {
        NSDictionary *parameters = @{
                                     @"user_id":@(_from_id),
                                     @"from_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"start":@(_start),
                                     @"size":@"20"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else{
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"from_id":@(_from_id),
                                     @"start":@(_start),
                                     @"size":@"20"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 20;
            NSArray *data = json[@"data"];
            if (data && data.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                UserModel *model = [UserModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            if (self.arrayData && self.arrayData.count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        if (self.arrayData && self.arrayData .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return self.arrayData.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AttentionFansCell *cell = [AttentionFansCell theAttentionFansCellWithtableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        cell.usermodel = [_arrayData objectAtIndex:indexPath.row];
    } 
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData.count > indexPath.row) {
        UserModel *model = [_arrayData objectAtIndex:indexPath.row];
        if (model.role_id == 1 && model.user_id != [USER_ID integerValue]) {
            TeacherViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"TeacherViewController"];
            CourseModel *coursemodel = [[CourseModel alloc] init];
            coursemodel.fk_user_id = model.user_id;
            VC.coursemodel = coursemodel;
            if (self.PushBlock) {
                self.PushBlock(VC);
            } else {
                [self.navigationController pushViewController:VC animated:YES];
            }
        } else if (model.user_id != [USER_ID integerValue]) {
            PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
            VC.user_id = model.user_id;
            if (self.PushBlock) {
                self.PushBlock(VC);
            } else {
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
}

- (UIScrollView *)scrollView{
    return self.tableView;
}


- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 98;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}

- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无粉丝";
        _nothingView.imageView.image = ImageName(@"资讯 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}


@end
