//
//  TeacherCourseViewController.h
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseModel;


@interface TeacherCourseViewController : UIViewController
/** type */
@property (nonatomic, assign) NSUInteger  type;
@property (nonatomic, strong) CourseModel *coursemodel;
/**跳转老师详情*/
@property (nonatomic, copy) void (^pushTeacherDetailsBlock)(CourseModel *coursemodel);
/**跳转音频视频详情*/
@property (nonatomic, copy) void (^courseCagetoryBlock)(CourseModel *coursemodel);
@end
