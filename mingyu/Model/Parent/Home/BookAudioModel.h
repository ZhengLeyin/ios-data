//
//  BookAudioModel.h
//  mingyu
//
//  Created by apple on 2018/5/19.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookAudioModel : NSObject


/** 音频id */
@property (nonatomic, assign) NSInteger audio_id;

/** 外键  */
@property (nonatomic, assign) NSInteger fk_parent_id;

/** 音频标题  */
@property (nonatomic, copy) NSString *audio_title;

/** 音频图片  */
@property (nonatomic, copy) NSString *audio_image;

/** 音频地址  */
@property (nonatomic, copy) NSString *audio_path;

/** 音频价格  */
@property (nonatomic, assign) CGFloat audio_price;

/** 虚拟币数量  */
@property (nonatomic, assign) NSInteger audio_gold;

/** 音频时长题  */
@property (nonatomic, copy) NSString *time_length;

/** 播放次数  */
@property (nonatomic, assign) NSInteger play_count;

/** 音频类型  */
@property (nonatomic, assign) NSInteger audio_type;

/** 创建时间  */
@property (nonatomic, copy) NSString *create_time;

/** 点赞数  */
@property (nonatomic, assign) NSInteger praise_number;

/** 已点赞  */
@property (nonatomic, assign) NSInteger click_praise_true;



@end
