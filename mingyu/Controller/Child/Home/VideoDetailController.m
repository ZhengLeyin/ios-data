//
//  VideoDetailController.m
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoDetailController.h"
#import <Masonry/Masonry.h>

@interface VideoDetailController ()

@end

@implementation VideoDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor redColor];
    UIView *view =[[ UIView alloc] init];
    view.backgroundColor = [UIColor greenColor];
    [self.view addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self);
//        make.left.mas_equalTo(20);
//        make.right.mas_equalTo(-20);
//        make.bottom.mas_equalTo(-10);
        make.top.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-300);
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
