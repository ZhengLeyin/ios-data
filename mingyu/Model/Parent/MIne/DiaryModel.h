//
//  DiaryModel.h
//  mingyu
//
//  Created by apple on 2018/8/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DiaryPhotoModel.h"
@interface DiaryModel : NSObject


/** 印记id */
@property (nonatomic, assign) NSInteger diary_id;

/** 用户id */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 印记内容 */
@property (nonatomic, copy) NSString *diary_content;

/** 创建时间 */
@property (nonatomic, copy) NSString *create_time;

/** 年份 */
@property (nonatomic, copy) NSString *year;

/** 月份 */
@property (nonatomic, copy) NSString *month;

/** 日 */
@property (nonatomic, copy) NSString *day;

/** 图片集合 */
@property (nonatomic, strong) NSArray <DiaryPhotoModel*> *imageList;


@property (nonatomic, strong) NSArray <DiaryPhotoModel*> *messageList;


@property (nonatomic, assign) CGFloat height;

@end
