///**************************应用头文件**********************************/
//
///*-------------------ViewController.h-------------------------------*/


#pragma - mark - ViewController.h

//#pragma - mark - commom
#import "FirstLaunchViewController.h"
#import "BabyInfoViewController.h"
#import "CodeLoginViewController.h"
#import "PassWordLoginViewController.h"
#import "SetPasswordViewController.h"
#import "MYRefreshGifHeader.h"

#import "ViewController.h"

#import "TabBarController.h"
#import "ChildHomeViewController.h"

//parents  Home  ViewController
#import "ParentHomePageViewController.h"
//#import "ParentHomeViewController.h"
#import "ParentNewHomeViewController.h"
#import "ParentChildCareViewController.h"
#import "QianDaoViewController.h"
#import "GoldDetailViewController.h"
#import "GoldRulerViewController.h"
#import "GetGoldController.h"
#import "QuickListenViewController.h"
#import "HandPickViewController.h"
#import "ListenStoryViewController.h"
#import "AlbumDetailViewController.h"
#import "ListenStoreDetailViewController.h"
#import "BookTypeViewController.h"
#import "ParentCourseViewController.h"
#import "AudioPlayerViewController.h"
#import "BabyVaccineViewController.h"
#import "ChildCareDetailViewController.h"



//parents  Community  ViewController
#import "ParentCommunityViewController.h"
#import "ParentAtentionViewController.h"
#import "ParentRecommendViewController.h"
#import "ParentTopicViewController.h"
#import "ParentArticleViewController.h"
#import "CommentListViewController.h"
#import "CommentViewController.h"
#import "SomeCircleViewController.h"
#import "TopicListViewController.h"
#import "TopicEditorViewController.h"
#import "TopicEditorDownViewController.h"



//parents  Mine  ViewController
#import "MineViewController.h"
#import "MineMessageViewController.h"
#import "SystemMessageViewController.h"
#import "PrivateMessageViewController.h"
#import "UserInfoViewController.h"
#import "ChangeNameViewController.h"
#import "LocationViewController.h"
#import "AddLocationViewController.h"
#import "IdentityViewController.h"
#import "ChangePhoneViewController.h"
#import "ParentSetViewController.h"
#import "ChangePasswordViewController.h"
#import "CollectViewController.h"
#import "CollectTopicViewController.h"
#import "CollectBookViewController.h"
#import "CollectCourseViewController.h"
#import "CollectArticleViewController.h"
#import "CollectChildcareViewController.h"
#import "MineTopicViewController.h"
#import "MinePublicViewController.h"
#import "MineCommentViewController.h"
#import "MineTopicDraftsViewController.h"
#import "MineAttentionViewController.h"
#import "MineFansViewController.h"
#import "PersonalHomepageViewController.h"
#import "MineWalletViewController.h"
#import "MineJinBiViewController.h"
#import "MineCouresViewController.h"
#import "AboutUsViewController.h"
#import "MineInterestViewController.h"
#import "CourseVideoViewController.h"
#import "CourseAudioViewController.h"
#import "SearchViewController.h"
#import "AddRecordViewController.h"
#import "TimeRecordViewController.h"


//parents  Video  ViewController
#import "ParentVideoViewController.h"
#import "ParentVideoDetailViewController.h"


//child  Mine  ViewController
#import "childListenViewController.h"
#import "UserInfoViewController.h"



#import "ChildShowViewController.h"
#import "ShowBaseViewController.h"
#import "ShowFirstViewController.h"
#import "ShowSecondViewController.h"
#import "ShowThirdViewController.h"
#import "ShowFourViewController.h"







/*-------------------View && Cell-------------------------------*/
#pragma - mark - View && Cell
#import "CommentListCell.h"
#import "BottomCommetView.h"
#import "ShareView.h"
#import "NothingView.h"


//Parent  Home View && Cell
#import "ParentHomeCell.h"
#import "ParentHomeHeaderView.h"
#import "ParentChildCareCell.h"
#import "ChildCareClassifyView.h"
#import "QiandaoCollectionViewCell.h"
#import "QianDaoCollectionReusableView.h"
#import "QianDaoRulerView.h"
#import "GoldDetailCell.h"
#import "GoldHeaderView.h"
#import "ListenStoryCell.h"
#import "AlbumDetailCell.h"
#import "PlayerDatailCell.h"
#import "PlayerDetailPopView.h"
#import "TimeListView.h"
#import "RateListView.h"
#import "LSDHeaderView.h"
#import "BookAudioCell.h"
#import "ParentCouresCell.h"
#import "BabyVaccineCell.h"


//Parent  Community View && Cell
#import "RecommenCollectionViewCell.h"
#import "VCollectionReusableView.h"
#import "ParentRecommenCell.h"
#import "ParentCicleCell.h"
#import "CircleCollectionViewCell.h"
#import "ParentTopicCell.h"
#import "ParentArticleCell.h"
#import "LeftTableViewCell.h"
#import "RightTableViewCell.h"
#import "SomeCircleFooterView.h"
#import "TagView.h"
#import "HandPickCell.h"


//Parent  Mine View && Cell
#import "MineViewCell.h"
#import "AttentionFansCell.h"
#import "MineCommentCell.h"
#import "EditorBottomView.h"
#import "SystemMessageCell.h"


//Parent  Viedeo View && Cell
#import "ParentVideoCell.h"
#import "VideoPlayEndVIew.h"


#import "ChildHomeTableViewCell.h"


#import "ChildListenTableViewCell.h"



#import "ShowCollectionViewCell.h"
#import "MyHeaderView.h"
#import "SPPageMenu.h"



/*----------------------Model-------------------------------*/
#pragma - mark - Model

//Parent  Home Model
//#import "ChildcareMessageModel.h"
#import "LabelListModel.h"
#import "QianDaoActionModel.h"
#import "QianDaoTaskModel.h"
#import "AlbumListModel.h"
#import "SubjectModel.h"
#import "AudioModel.h"
#import "BookListModel.h"
#import "CourseModel.h"



#import "ParentCircleModel.h"




//Parent  Mine Model
#import "AddressModel.h"
#import "DiaryModel.h"
#import "DiaryPhotoModel.h"
#import "MyCommentMessageModel.h"
#import "SystemMessageModel.h"
#import "QQMessageModel.h"
#import "QQUserModel.h"



#import "UserModel.h"
#import "VideoModel.h"
//#import "VideoListModel.h"
#import "commenModel.h"
#import "NewsModel.h"
#import "ShareModel.h"


#pragma  -mark-  第三方头文件
/**************************第三方头文件**********************************/
#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "MJRefresh.h"
#import "CYTabBarController.h"
#import <YYKit.h>
//#import "LMJScrollTextView.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import <Masonry/Masonry.h>
#import "YBPopupMenu.h"
#import "IQKeyboardManager.h"
#import "DFPlayer.h"
#import "YCDownloadManager.h"
#import "downloadInfo.h"
#import <UMAnalytics/MobClick.h>

#pragma  -mark-  扩展头文件
/**************************扩展头文件**********************************/
#import "UIView+Extention.h"
#import "UIColor+Hex.h"
#import "UITextView+YLTextView.h"
#import "UIButton+Extension.h"
#import "UIImageView+Extension.h"
#import "UILabel+Extension.h"
#import "HttpRequest.h"
#import "NSString+Extension.h"
#import "UIImage+Extension.h"
#import "UITabBar+Eet.h"
#import "AVSpeech.h"
#import "NSObject+Alert.h"
#import "PlayerTimerManager.h"
#import "MingYuLogger.h"


