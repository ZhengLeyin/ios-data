//
//  NothingView.h
//  mingyu
//
//  Created by apple on 2018/10/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NothingView : UIView


@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *tipLab;


@end
