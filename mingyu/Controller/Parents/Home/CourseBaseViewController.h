//
//  CourseBaseViewController.h
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseBaseViewController : UIViewController
@property (nonatomic, strong) SPPageMenu *pageMenu;
/**精品课id*/
@property (nonatomic, assign) NSUInteger meunId;
@end

