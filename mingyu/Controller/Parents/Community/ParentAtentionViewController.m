//
//  ParentAtentionViewController.m
//  mingyu
//
//  Created by apple on 2018/4/16.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentAtentionViewController.h"

@interface ParentAtentionViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,YBPopupMenuDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, strong) NSMutableArray *topicArrayData;

@property (nonatomic, strong) UICollectionView *HcollectionView;  //横向collectionView

@property (nonatomic, strong) UICollectionView *VcollectionView;   //纵向collectionView
@property (nonatomic, strong) NSMutableArray *CollectArrayData;  //纵向collectionView 数据源

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@end

@implementation ParentAtentionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.estimatedRowHeight = 0;
    self.tableView.rowHeight = 130;
    self.headerView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    self.headerView.height = 180;
    [self.headerView addSubview:self.HcollectionView];
    [self.view addSubview:self.VcollectionView];
    
    self.VcollectionView.hidden = YES;
    self.tableView.hidden = YES;

    [self.topicArrayData removeAllObjects];
    [self.CollectArrayData removeAllObjects];
    
//    刷新tableview
    [self refreshTableview];

}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_CollectArrayData.count == 0 && _topicArrayData.count == 0) {
        [self refreshTableview];
    }
}


-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.topicArrayData.count>0) {
            [_topicArrayData removeAllObjects];
        }
        _start = 0;

        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self getFollowTopicList];  //加载cell数据

        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getFollowTopicList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
}



//    获取关注的话题帖子
- (void)getFollowTopicList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetFollowTopicList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSDictionary *data = json[@"data"];
            if (data.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            NSInteger follow_user_number = [data[@"follow_user_number"] integerValue];
            [userDefault setInteger:follow_user_number forKey:KUDfollow_number];
            [userDefault synchronize];
            
//            if (follow_user_number == 0) {
//                self.VcollectionView.hidden = NO;
//                self.tableView.hidden = YES;
//            } else if (follow_user_number >= 3 && self.CollectArrayData.count > 0){
//                self.VcollectionView.hidden = YES;
//                self.tableView.hidden = NO;
//                self.headerView.height = 180;
//                self.headerView.hidden = NO;
//            }else{
//                self.VcollectionView.hidden = YES;
//                self.tableView.hidden = NO;
//                self.headerView.height = 0;
//                self.headerView.hidden = YES;
//            }
            
            //    获取推荐相关的用户信息
            [self getRecommendFollowList];
            NSArray *topicList = data[@"topicList"];
                for (NSDictionary *dic in topicList) {
                    NewsModel *model = [NewsModel modelWithJSON:dic];
                    [self.topicArrayData addObject:model];
                }
                [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)getRecommendFollowList{
    [_CollectArrayData removeAllObjects];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendFollowList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                UserModel *user = [UserModel modelWithJSON:dic];
                [self.CollectArrayData addObject:user];
            }
            NSInteger follow_user_number = [userDefault integerForKey:KUDfollow_number];
            if (follow_user_number == 0) {
                    self.VcollectionView.hidden = NO;
                    self.tableView.hidden = YES;
                } else if (follow_user_number >= 3 && self.CollectArrayData.count > 0){
                    self.VcollectionView.hidden = YES;
                    self.tableView.hidden = NO;
                    self.headerView.frame = CGRectMake(0, 0, kScreenWidth, 180);
                    self.headerView.hidden = NO;
                    [self.tableView setTableHeaderView: self.headerView];
                }else{
                    self.VcollectionView.hidden = YES;
                    self.tableView.hidden = NO;
                    self.headerView.frame = CGRectMake(0, 0, kScreenWidth, 0);
                    self.headerView.hidden = YES;
                    [self.tableView setTableHeaderView: self.headerView];
                }

            if (follow_user_number == 0) {
                [self.VcollectionView reloadData];
            } else{
                [self.HcollectionView reloadData];
                [self.tableView reloadData];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)AddFollowWithUserModel:(UserModel *)usermodel{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)usermodel.user_id]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start = 0;
            //    获取关注的话题帖子
            [self getFollowTopicList];

        } else {
//            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma -mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.CollectArrayData && self.CollectArrayData.count > 0) {
        return _CollectArrayData.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RecommenCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecommenCollectionViewCell" forIndexPath:indexPath];
    if (self.CollectArrayData && self.CollectArrayData.count > indexPath.row) {
        cell.usermodel = [self.CollectArrayData objectAtIndex:indexPath.row];
        cell.attentionblock = ^(UserModel *usermodel) {
            [self.topicArrayData removeAllObjects];
            [self.CollectArrayData removeAllObjects];
            [self AddFollowWithUserModel:usermodel];
        };
        // 主线程执行：
//        NSRunLoop *loop = [NSRunLoop  mainRunLoop];
//        [loop addPort:nil forMode:NSRunLoopCommonModes];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.userHeadButton  setImage:[UIImage imageWithRoundCorner:cell.userHeadButton.imageView.image cornerRadius:25.0 size:CGSizeMake(50.0, 50.0)] forState:(UIControlStateNormal)];
        });
    
       
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.VcollectionView) {
        VCollectionReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"VCollectionReusableView" forIndexPath:indexPath];
        return headView;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ZPLog(@"%ld",indexPath.row);
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.topicArrayData && self.topicArrayData.count > 0) {
        return self.topicArrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsModel *model = [[NewsModel alloc] init];
    if (self.topicArrayData && self.topicArrayData.count > indexPath.row) {
        model = [self.topicArrayData objectAtIndex:indexPath.row];
    }
    if (model.news_type == 1) {
        ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
        cell.newsmodel = model;
        return cell;
    } else if (model.news_type == 2){
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    } else {
        ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_topicArrayData && _topicArrayData.count > indexPath.row) {
        NewsModel *model = [_topicArrayData objectAtIndex:indexPath.row];
        if (model.news_type == 1) {
            ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.news_type == 2){
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}

#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenuDidSelectedAtIndex:(NSInteger)index ybPopupMenu:(YBPopupMenu *)ybPopupMenu {
    
    ZPLog(@"dislike");
    
}

#pragma -mark- layz collectionView
- (UICollectionView *)HcollectionView{
    if (_HcollectionView == nil) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake((kScreenWidth-40)/3.25, _headerView.height-30);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(0,10, 0,10);
        
        _HcollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 14, kScreenWidth,_headerView.height-30) collectionViewLayout:layout];
        _HcollectionView.backgroundColor = [UIColor clearColor];
        _HcollectionView.delegate = self;
        _HcollectionView.dataSource = self;
        _HcollectionView.scrollsToTop = NO;
        _HcollectionView.showsVerticalScrollIndicator = NO;
        _HcollectionView.showsHorizontalScrollIndicator = NO;
        [_HcollectionView registerNib:[UINib nibWithNibName:@"RecommenCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RecommenCollectionViewCell"];
    }
    return _HcollectionView;
}


- (UICollectionView *)VcollectionView{
    if (_VcollectionView == nil) {
        
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake((kScreenWidth-60)/3, _headerView.height-30);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.sectionInset = UIEdgeInsetsMake(10,20, 10,20);
        layout.headerReferenceSize = CGSizeMake(kScreenWidth, 320);
        
        _VcollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH) collectionViewLayout:layout];
        _VcollectionView.backgroundColor = [UIColor whiteColor];
        _VcollectionView.delegate = self;
        _VcollectionView.dataSource = self;
        _VcollectionView.scrollsToTop = NO;
        _VcollectionView.showsVerticalScrollIndicator = NO;
        _VcollectionView.showsHorizontalScrollIndicator = NO;
        [_VcollectionView registerNib:[UINib nibWithNibName:@"RecommenCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RecommenCollectionViewCell"];
        [_VcollectionView registerNib:[UINib nibWithNibName:@"VCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"VCollectionReusableView"];

    }
    return _VcollectionView;
}

- (NSMutableArray *)CollectArrayData{
    if (_CollectArrayData == nil) {
        _CollectArrayData = [NSMutableArray array];
    }
    return _CollectArrayData;
}

//- (NSMutableArray *)HCollectArrayData{
//    if (_HCollectArrayData == nil) {
//        _HCollectArrayData = [NSMutableArray array];
//    }
//    return _HCollectArrayData;
//}

- (NSMutableArray *)topicArrayData{
    if (_topicArrayData == nil) {
        _topicArrayData = [NSMutableArray array];
    }
    return _topicArrayData;
}

@end
