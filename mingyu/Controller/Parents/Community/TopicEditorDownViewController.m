//
//  TopicEditorDownViewController.m
//  mingyu
//
//  Created by apple on 2018/4/27.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TopicEditorDownViewController.h"
#import "ParentAddCircleViewController.h"

@interface TopicEditorDownViewController ()<TagViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic ,strong)TagView * tagView;

@property (weak, nonatomic) IBOutlet UIButton *ToTopicButton;

@property (nonatomic, strong) NSMutableArray *circleArray;


@end

@implementation TopicEditorDownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    _ToTopicButton.layer.cornerRadius = 6;
    _ToTopicButton.layer.borderColor = [UIColor colorWithHexString:@"FF96A4"].CGColor;
    _ToTopicButton.layer.borderWidth = 1;
    [_ToTopicButton setTitle:[NSString stringWithFormat:@"  %@  ",_newsmodel.circle_name] forState:UIControlStateNormal];
    
    [self.view addSubview:self.tableView];

    
    [self getCircleList];
}

- (void)getCircleList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAddTopicCircle];
    
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"circle_id":@(_newsmodel.fk_circle_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                ParentCircleModel *model = [ParentCircleModel modelWithJSON:dic];
                [self.circleArray addObject:model];
            }
            ParentCircleModel *model = [[ParentCircleModel alloc] init];
            model.circle_name = @"更多";
            model.circle_id = 0;
            [self.circleArray addObject:model];
            
            self.tagView.arr = self.circleArray;
            self.tableView.tableHeaderView = self.tagView;

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(TagView *)tagView{
    if (!_tagView) {
        _tagView = [[TagView alloc]initWithFrame:CGRectMake(0, 100, kScreenWidth, 0)];
        _tagView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        _tagView.delegate = self;
    }
    return _tagView;
}

#pragma mark - CCTagViewDelegate
-(void)handleSelectModel:(ParentCircleModel *)selectmodel{
    if (selectmodel.circle_id == 0) {
        ParentAddCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentAddCircleViewController"];
        VC.news_id = _newsmodel.news_id;
        @weakify(self)
        VC.changeCircleBlock = ^(ParentCircleModel *circlemodel) {
            @strongify(self)
            [self.ToTopicButton setTitle:[NSString stringWithFormat:@"  %@  ",circlemodel.circle_name] forState:UIControlStateNormal];
            [UIView ShowInfo:@"更改成功" Inview:self.view];
        };
        [self.navigationController pushViewController:VC animated:YES];
        
        return;
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"更改圈子"
                                                                   message:[NSString stringWithFormat:@"确定将帖子更改至：%@",selectmodel.circle_name]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self changCircleWithModel:selectmodel];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                         }];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)changCircleWithModel:(ParentCircleModel *)model{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateTopicIsCircle];
    NSDictionary *parameters = @{
                                 @"news_id":@(_newsmodel.news_id),
                                 @"circle_id":@(model.circle_id)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [_ToTopicButton setTitle:[NSString stringWithFormat:@"  %@  ",model.circle_name] forState:UIControlStateNormal];
            [UIView ShowInfo:@"更改成功" Inview:self.view];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _ToTopicButton.bottom+15, kScreenWidth, kScreenHeight-_ToTopicButton.bottom-15-NaviH-effectViewH) style:UITableViewStylePlain];
        //        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}


- (NSMutableArray *)circleArray{
    if (_circleArray == nil) {
        _circleArray = [NSMutableArray array];
    }
    return _circleArray;
}

- (IBAction)checkTopicButton:(id)sender {
    if (_newsmodel) {
        ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
        VC.newsmodel = _newsmodel;
        [self.navigationController pushViewController:VC animated:YES];
    }
}

- (IBAction)back:(id)sender {
    
    UIViewController *VC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
    ZPLog(@"%ld",self.navigationController.viewControllers.count);
    [self.navigationController popToViewController:VC animated:YES];
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}


@end
