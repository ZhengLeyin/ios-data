//
//  ParentHomePageHeaderView.m
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ParentHomePageHeaderView.h"
#import "AdvertisementModel.h"
#import "QiniuSDK.h"


@implementation ParentHomePageHeaderView {
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;
    int i;
}

-(ParentHomePageBannerView *)homePageBannerViewbannerView {
    if (!_homePageBannerViewbannerView) {
        _homePageBannerViewbannerView = [[ParentHomePageBannerView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth - 40, 135)];
        _homePageBannerViewbannerView.backgroundColor = [UIColor purpleColor];
        _homePageBannerViewbannerView.layer.cornerRadius = 5;
        _homePageBannerViewbannerView.layer.masksToBounds = YES;
        //_bannerView.frame = _homePageBannerViewbannerView.frame;
    }
    return _homePageBannerViewbannerView;
}

-(ParentHomePageFunctionModuleView *)homePageFunctionModuleView {
    if (!_homePageFunctionModuleView) {
        _homePageFunctionModuleView = [[ParentHomePageFunctionModuleView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 130)];
        //_homePageFunctionModuleView.backgroundColor = [UIColor yellowColor];
        //_bannerView.frame = _homePageBannerViewbannerView.frame;
    }
    return _homePageFunctionModuleView;
}

-(HomePageAgeRelatedOfHeaderView *)ageHeaderView {
    if (!_ageHeaderView) {
        _ageHeaderView = [[HomePageAgeRelatedOfHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 135)];
    }
    return _ageHeaderView;
}


/**数据处理*/
- (void)awakeFromNib{
    [super awakeFromNib];
    /*1.bannerView 单独View
     *2.功能选择模块 单独View
     *3.两岁前年龄相关 单独View
     */
    /**两岁前年龄相关 View*/
    [self.ageRelatedView addSubview:self.ageHeaderView];
    
    /**bannerView*/
    [self.bannerView addSubview:self.homePageBannerViewbannerView];
    
    /** 功能选择模块 */
    [self.functionSelectModuleView addSubview:self.homePageFunctionModuleView];
    
#pragma mark 两岁前 属性处理
    _timeRecordBackgroundView.userInteractionEnabled = YES;
    _timeRecordButton.layer.cornerRadius = 5;
    _timeRecordButton.layer.masksToBounds = YES;
    _goToRecordLabel.layer.cornerRadius = 10;
    _goToRecordLabel.layer.masksToBounds = YES;
    _goToRecordLabel.layer.borderColor = [UIColor colorWithHexString:@"#90D3FF"].CGColor;
    _goToRecordLabel.layer.borderWidth = 1;

    //[self layoutIfNeeded];
}

#pragma mark 两岁前
+ (instancetype)TheBeforeAgeOfTwoOfParentHomePageHeaderView {
    return [[NSBundle mainBundle] loadNibNamed:@"ParentHomePageHeaderView" owner:nil options:nil][0];
}

#pragma mark 两岁后
+ (instancetype)TheAfterTwoYearsOldOfParentHomePageHeaderView {
    return [[NSBundle mainBundle] loadNibNamed:@"ParentHomePageHeaderView" owner:nil options:nil][1];
}


#pragma mark 时光印记跳转
- (IBAction)TimeRecord:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    TimeRecordViewController *VC = [TimeRecordViewController new];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


@end
