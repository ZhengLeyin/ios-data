//
//  DropDownList.m
//  DropDownList(下拉框选择)
//
//  Created by Mr_zhang on 17/1/9.
//  Copyright © 2017年 Mr_zhang. All rights reserved.
//

#import "DropDownList.h"

@interface DropDownList()

@property (nonatomic, strong) UITableView *dropDownTableView;

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UIView *coverView;

@end

@implementation DropDownList

- (instancetype)initWithFrame:(CGRect)frame dataArray:(NSArray *)dataArray onTheView:(UIView *)view
{
    if (self = [super initWithFrame:frame])
    {
        /* 0.弹出遮挡页面遮挡界面 */
        self.coverView = [[UIView alloc] initWithFrame:view.frame];
        _coverView.backgroundColor = [UIColor whiteColor];
        _coverView.alpha = 0.1f;
        UITapGestureRecognizer *coverTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveTheCoverView)];
        [_coverView addGestureRecognizer:coverTap];
        [view addSubview:_coverView];
        
        /* 1.设置view的背景图片，背景图片为imageView担任 */
        // 图片拉伸处理
        UIImage *image = [UIImage imageNamed:@"DropDownLis.png"];
        // 上左下右拉伸不拉伸部分的距离
        CGFloat top = 20;
        CGFloat left = 10;
        CGFloat bottom = 20;
        CGFloat right = 40;
        UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
        image = [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
        
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        imageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:imageView];
        
        /* 2.定义tableview */
        self.dropDownTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, frame.size.width, frame.size.height - 10) style:UITableViewStylePlain];
        self.dropDownTableView.layer.cornerRadius = 5;
        self.dropDownTableView.layer.masksToBounds = YES;
        self.dropDownTableView.delegate = self;
        self.dropDownTableView.dataSource = self;
        self.dropDownTableView.backgroundColor = [UIColor whiteColor];
        [self.dropDownTableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self addSubview:self.dropDownTableView];
        
        /* 3.传值 */
        // 赋值
        self.dataArray = dataArray;
        
    }
    return self;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 选中时的颜色不显示
    [self.dropDownTableView deselectRowAtIndexPath:indexPath animated:NO];
    
    // 调用block传值
    _maximumDiscount = 0;
    self.myBlock(indexPath.row,self.dataArray[indexPath.row],[NSString stringWithFormat:@"%ld",_maximumDiscount]);
    
    // 移除遮挡页面和tableview
    [self moveTheCoverView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, self.frame.size.width, 29.5);
    [button setTitle:[NSString stringWithFormat:@"最大抵扣:%ld",_maximumDiscount] forState:(UIControlStateNormal)];
    [button setTitleColor:[UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1.0] forState:UIControlStateNormal];
    button.titleLabel.font= [UIFont systemFontOfSize:11.0];
    button.backgroundColor = [UIColor whiteColor];
    [view addSubview:button];
//        UIView *lineView = [UIView new];
//        lineView.backgroundColor = [UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1.0];
//        lineView.frame = CGRectMake(0, CGRectGetMaxY(button.frame), self.frame.size.width, 0.5);
//        [view addSubview:lineView];
    
    [[button rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
        //_maximumDiscount = 0;
        self.myBlock(-1,@"最大抵扣",[NSString stringWithFormat:@"%ld",_maximumDiscount]);
        [self moveTheCoverView];
    }];
    
//
//    UILabel *label = [UILabel new];
//    label.text = @"最大抵扣:1000";
//    label.font = [UIFont systemFontOfSize:11.0];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1.0];
//
//    label.frame = CGRectMake(0, 0, self.frame.size.width, 29.5);
//    [view addSubview:label];
//    UIView *lineView = [UIView new];
//    lineView.backgroundColor = [UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1.0];
//    lineView.frame = CGRectMake(0, CGRectGetMaxY(label.frame), self.frame.size.width, 0.5);
//    [view addSubview:lineView];
    return view;
}

#pragma mark 移除遮挡页面和tableview
- (void)moveTheCoverView
{
    [self.coverView removeFromSuperview];
    [self removeFromSuperview];
}

@end
