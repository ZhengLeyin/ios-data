//
//  MingYuCashierView.h
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MingYuRechargeModel.h"
NS_ASSUME_NONNULL_BEGIN

API_AVAILABLE(ios(9.0))
@interface MingYuCashierView : UIView
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UIView *threeView;
/** 需要购买育币个数 */
@property (weak, nonatomic) IBOutlet UILabel *needToBuyYuBiCountLabel;
/** 育币图标 */
@property (weak, nonatomic) IBOutlet UIImageView *yuBiImageView;
/** 育币支付 标题 */
@property (weak, nonatomic) IBOutlet UILabel *yuBiTitleLabel;
/** 用户拥有育币个数 */
@property (weak, nonatomic) IBOutlet UILabel *yuBiCountLabel;
/** 购买Button */
@property (weak, nonatomic) IBOutlet UIButton *payRechargeButton;
/** 积分抵扣选中的View */
@property (weak, nonatomic) IBOutlet UIView *integralDeductionView;
/** 积分抵扣的金额 */
@property (weak, nonatomic) IBOutlet UILabel *integralDeductionPriceLabel;
/** 积分Label */
@property (weak, nonatomic) IBOutlet UILabel *integarlLabel;
/** StackView */
@property (weak, nonatomic) IBOutlet UIStackView *integralCategoryStackView;
/** 积分抵扣开关 */
@property (weak, nonatomic) IBOutlet UIButton *integralDeductionSwitchButton;
/** model */
@property (nonatomic, strong)  MingYuRechargeModel *model;

/** courseModel */
@property (nonatomic, strong)  CourseModel *courseModel;

/** 真实支付的育币数 */
@property (nonatomic, assign)  CGFloat realPrice;

-(void)mingYuModel:(MingYuRechargeModel *)model courseModel:(CourseModel *)courseModel;
@end

NS_ASSUME_NONNULL_END
