//
//  TabBarController.h
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "CYTabBarController.h"

@interface TabBarController : CYTabBarController

@property (nonatomic,strong) UIButton *AddButton;

+ (void)style1:(CYTabBarController *)tabbar ;

+ (void)style2:(CYTabBarController *)tabbar ;


@end
