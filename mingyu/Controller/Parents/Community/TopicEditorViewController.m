//
//  TopicEditorViewController.m
//  mingyu
//
//  Created by apple on 2018/4/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TopicEditorViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "YYKit.h"
#import "Tool.h"
#import "TZImagePickerController.h"
#import "TZImageManager.h"
#import "QiniuSDK.h"


@interface TopicEditorViewController ()<YYTextViewDelegate,TZImagePickerControllerDelegate,UINavigationControllerDelegate>


//@property (nonatomic ,strong)UIButton * publishBtn;
@property (weak, nonatomic) IBOutlet UIButton *publicButton;
@property (nonatomic, strong) YYTextView *contentTextView;
@property (nonatomic, strong) YYTextView *titleTextView;

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;
@property (nonatomic ,assign) CGFloat keyboardHeight;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *bottomView;


@property (nonatomic ,strong)NSMutableArray * imagesArr;    //存放图片
@property (nonatomic ,strong)NSMutableArray * imageUrlsArr; // 存放图片url
@property (nonatomic ,strong)NSMutableArray * desArr;       //存放图片描述
@property (nonatomic ,copy)NSString * contentStr;       // 带有标签的文章内容
@property (nonatomic, copy) NSString *titleSrt;       //文章标题

@property (nonatomic ,assign) NSInteger count;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) dispatch_group_t  group;
@property (nonatomic, assign) NSInteger imageCount;

@end

@implementation TopicEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _imageUrlsArr = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"发帖";
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [self setupSubViews];
    
    _count = 30;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_timer invalidate];
    _timer = nil;
}

/**
 设置UI布局
 */
- (void)setupSubViews {

    // 图文正文输入框
    [self.view addSubview:self.contentTextView];
    [self.contentTextView addSubview:self.titleView];
    [self.view addSubview:self.bottomView];

}

/**
 获取键盘高度
 */
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    self.keyboardHeight = keyboardRect.size.height;
}

/**
 插入图片
 
 @param image 图片image
 */
- (void)setupImage:(UIImage *)image {
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:self.contentTextView.attributedText];
    UIFont *font = [UIFont systemFontOfSize:15];
    
    [text appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:nil]];

    NSData *imgData = UIImageJPEGRepresentation(image, 0.9);
    YYImage *img = [YYImage imageWithData:imgData];
    img.preloadAllAnimatedImageFrames = YES;
    YYAnimatedImageView *imageView = [[YYAnimatedImageView alloc] initWithImage:image];
    imageView.autoPlayAnimatedImage = NO;
    imageView.clipsToBounds = YES;
    [imageView startAnimating];
    CGSize size = imageView.size;
    CGFloat textViewWidth = kScreenWidth - 32.0;
    size = CGSizeMake(textViewWidth, size.height * textViewWidth / size.width);
    NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:imageView contentMode:UIViewContentModeScaleAspectFit attachmentSize:size alignToFont:font alignment:YYTextVerticalAlignmentCenter];
    
    
    YYTextView * desTextView = [YYTextView new];
    desTextView.delegate = self;
    desTextView.contentInset = UIEdgeInsetsMake(5, 50, -5, -50);
    desTextView.text = @" ";
    desTextView.bounds = CGRectMake(0, 0,1, 10);
    desTextView.font = [UIFont systemFontOfSize:12];
    desTextView.textAlignment = NSTextAlignmentCenter;
    desTextView.textColor = [UIColor grayColor];
    desTextView.scrollEnabled = NO;
    NSMutableAttributedString *attachText2 = [NSMutableAttributedString attachmentStringWithContent:desTextView contentMode:UIViewContentModeCenter attachmentSize:desTextView.size alignToFont:[UIFont systemFontOfSize:12] alignment:YYTextVerticalAlignmentCenter];
    [attachText appendAttributedString:attachText2];
    
    //绑定图片和描述输入框
    [attachText setTextBinding:[YYTextBinding bindingWithDeleteConfirm:NO] range:attachText.rangeOfAll];
    
    [text insertAttributedString:attachText atIndex:self.contentTextView.selectedRange.location];
    
    text.font = font;
    self.contentTextView.attributedText = text;
    [self.contentTextView becomeFirstResponder];
    self.contentTextView.selectedRange = NSMakeRange(self.contentTextView.text.length, 0);
}


//存草稿
- (void)saveTopicDraft{

    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"TopicDrafts.plist"];

    NSDictionary *dic = @{
                          @"topic_title": _titleTextView.text ? _titleTextView.text : @"",
                          @"contentStr":_contentStr ? _contentStr : @"",
                          @"imageUrlsArr":_imageUrlsArr ? _imageUrlsArr : [NSMutableArray array],
                          @"time": [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]],
                          };
    
    if (_topicDraftsArray == nil) {
        _topicDraftsArray =  [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
        if (_topicDraftsArray == nil) {
            _topicDraftsArray = [NSMutableArray array];
        }
        [_topicDraftsArray insertObject:dic atIndex:0];
    }
    
    if (_topicDraftsDic && [_topicDraftsArray containsObject:_topicDraftsDic]) {
        [_topicDraftsArray removeObject:_topicDraftsDic];
        [_topicDraftsArray insertObject:dic atIndex:0];
    }
    
    [_topicDraftsArray replaceObjectAtIndex:0 withObject:dic];

    if ([_topicDraftsArray writeToFile:filePatch atomically:YES]){
        ZPLog(@"sucess");
    }
}


- (void)dealloc{
    ZPLog(@"deallocdeallocdeallocdeallocdealloc");
}

- (IBAction)publicButton:(id)sender {
    
    [self.view endEditing:YES];
    [self.view endEditing:YES];

    if ( self.titleTextView.text.length < 6 ) {
        [UIView ShowInfo:@"帖子标题不能少于6个字" Inview:self.view];
        return ;
    }
    NSString *HTMLString = [Tool makeHtmlString:_imageUrlsArr contentStr:_contentStr];
    ZPLog(@"%@",HTMLString);
    if (HTMLString.length < 10) {
        [UIView ShowInfo:@"帖子内容不能少于10个字" Inview:self.view];
        return ;
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"确认发布帖子吗？"]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             //响应事件
                                                             
                                                         }];
    [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
    
    UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              //响应事件
                                                              
                                                              [self confirmPublic];
                                                          }];
    [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
    
    [alert addAction:cancelAction];
    [alert addAction:confirmAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)confirmPublic{
    NSString *topic_abstract = @"";
    NSString *topic_head = @"";
    
    topic_abstract = [_contentStr stringByReplacingOccurrencesOfString:@"<我是图片>" withString:@"\n"];
    if (topic_abstract.length > 100) {
        topic_abstract = [topic_abstract substringToIndex:99];
    }
    
    if (_imageUrlsArr.count > 0) {
        topic_head = _imageUrlsArr[0];
    }
    
    _publicButton.userInteractionEnabled = NO; //设置发布按钮不可用
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddTopic];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"fk_circle_id":@(_circle_id),
                          @"news_title":[self.titleTextView.text copy],
                          @"news_subtitle":topic_abstract,
                          @"news_head":topic_head,
                          @"news_content": [Tool makeHtmlString:_imageUrlsArr contentStr:_contentStr]
                          };
    NSDictionary *parameters = @{
                                 @"news":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        _publicButton.userInteractionEnabled = YES;  //设置发布按钮可用
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
            //发帖成功，如果草稿箱中有这条记录 删掉
            if (_topicDraftsArray && _topicDraftsArray.count > 0) {
                [_topicDraftsArray removeObjectAtIndex:0];
                NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"TopicDrafts.plist"];
                if ([_topicDraftsArray writeToFile:filePatch atomically:YES]){
                    ZPLog(@"sucess");
                }
            }
            
            NSDictionary *data = json[@"data"];
            NewsModel *model = [NewsModel modelWithJSON:data];
            TopicEditorDownViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"TopicEditorDownViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        _publicButton.userInteractionEnabled = YES;  //设置发布按钮可用
    }];
}

#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    self.group = dispatch_group_create();
    for (NSInteger i = 0; i < photos.count; i++) {
        [self setupImage:photos[i]];
        
        dispatch_group_enter(_group);

        NSData *imageData = UIImageJPEGRepresentation(photos[i], 1.0 );
        [self getUpToken:imageData location:i+_imageCount];
        [self.imageUrlsArr addObject:[NSNull null]];
    }
    _imageCount = _imageCount + photos.count;

    dispatch_group_notify(_group, dispatch_get_main_queue(), ^{
        ZPLog(@"上传完成!");
        [self saveTopicDraft];

        for (id response in _imageUrlsArr) {
            ZPLog(@"%@", response);
        }
    });
    
//    [self fileUploadBatch:photos];
//    for (UIImage *image in photos) {
//        [self setupImage:image];
//    }
   
//    for (NSInteger i = 0; i < assets.count; i++) {
//        PHAsset *asset = assets[i];
//        CGSize size = CGSizeMake(asset.pixelWidth / scale, asset.pixelHeight / scale);
//        if (isSelectOriginalPhoto) {
//            //原图
//            [[TZImageManager manager] getOriginalPhotoWithAsset:asset completion:^(UIImage *photo, NSDictionary *info) {
//                if ([info[@"PHImageResultIsDegradedKey"] boolValue] == NO) {
//                    [self uploadImage:photo index:i];
//                }
//            }];
//        } else {
//            //非原图
//
//            [[PHImageManager defaultManager] requestImageForAsset:asset
//                                                       targetSize:size
//                                                      contentMode:PHImageContentModeDefault
//                                                          options:options
//                                                    resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//                                                        /** 刷新*/
//                                                        [self uploadImage:result index:i];
//                                                    }];
//        }
//    }
}


//获取七牛云凭证
- (void)getUpToken:(NSData *)imageData location:(NSInteger)location{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetUpToken];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@%ld.png", str,(long)location];
    NSDictionary *parameters = @{
                                 @"bucketName":bucketName,
                                 @"type":@"topic",
                                 @"fileName":fileName
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            
            QNUploadManager *upManger = [[QNUploadManager alloc] init];
            QNUploadOption *uploadoption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
                ZPLog(@"percent == %.2f", percent);
            } params:nil checkCrc:NO cancellationSignal:nil];
            
            [upManger putData:imageData key:data[@"key"] token:data[@"token"] complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                ZPLog(@"info ===== %@", info);
                ZPLog(@"resp ===== %@", resp);
//                @synchronized (_imageUrlsArr) { // NSMutableArray 是线程不安全的，所以加个同步锁
                    _imageUrlsArr[location] = resp[@"key"];
//                }
                dispatch_group_leave(_group);
                
            } option:uploadoption];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
            dispatch_group_leave(_group);
        }
    } failure:^(NSError *error) {
        dispatch_group_leave(_group);
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//- (void)fileUploadBatch:(NSArray *)imageArray{
//
//    NSString *URL = [NSString stringWithFormat:@"%@%@?type=topic",KURL,KFileUploadBatch];
//    // 1.创建请求管理者
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
//                                                         @"text/html",
//                                                         @"image/jpeg",
//                                                         @"image/png",
//                                                         @"application/octet-stream",
//                                                         @"text/json",
//                                                         @"multipart/form-data",
//                                                         nil];
//    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
//
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"token"] forHTTPHeaderField:@"token"];   //token
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"device_id"] forHTTPHeaderField:@"device_id"];
//    [manager.requestSerializer setValue:[HttpRequest getsignwithURL:URL] forHTTPHeaderField:@"sign"];
//
//    [manager POST:URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        for (UIImage *image in imageArray) {
//            NSData *imageData = UIImageJPEGRepresentation(image, 1.0 );
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            formatter.dateFormat = @"yyyyMMddHHmmss";
//            NSString *str = [formatter stringFromDate:[NSDate date]];
//            NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
//            NSString *name = @"files";
//            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
//            ZPLog(@"%@",formData);
//        }
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        ZPLog(@"%@",json);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//            NSArray *array = json[@"data"];
//            [self.imageUrlsArr addObjectsFromArray:array];
//
//            [self saveTopicDraft];
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        ZPLog(@"%@",error);
//    }];
//}



#pragma mark YYTextViewDelegate
- (void)textViewDidChange:(YYTextView *)textView{
    if (textView.text.length > 30 && textView.tag != 1000) {
        textView.text = [textView.text substringToIndex:30];
        [UIView ShowInfo:@"帖子标题不能大于30个字" Inview:self.view];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self changeImage];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self saveTopicDraft];
        });
    });
    
    if (!_timer && _count == 30) {
            _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(showCountdown)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}


- (void)showCountdown{
    _count--;
    if (_count == 0) {
        ZPLog(@"存草稿");
        [UIView ShowInfo:@"已自动保存草稿" Inview:self.view];
        _count = 30;
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)changeImage{
    
    NSMutableArray *changeImgarray = [NSMutableArray array];
    NSAttributedString *content = self.contentTextView.attributedText;
    NSString *text = [self.contentTextView.text copy];
    
    [content enumerateAttributesInRange:NSMakeRange(0, text.length) options:0 usingBlock:^(NSDictionary<NSString *,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        YYTextAttachment *att = attrs[@"YYTextAttachment"];
        if (att) {
            if ([att.content isKindOfClass:[YYTextView class]]) {
//                YYTextView * textView = att.content;
//                [self.desArr addObject:textView.text];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    YYAnimatedImageView *imgView = att.content;
                    [changeImgarray addObject:imgView.image];
//                    ZPLog(@"changeImgarray--------%@",changeImgarray);
                });
            }
        }
    }];
    self.contentStr = [text stringByReplacingOccurrencesOfString:@"\U0000fffc\U0000fffc" withString:@"<我是图片>"];
    self.contentStr = [self.contentStr stringByReplacingOccurrencesOfString:@"\U0000fffc" withString:@"<我是图片>"];


    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (int i = 0; i < changeImgarray.count; i++) {
            UIImage *image = changeImgarray[i];
            if ([self.imagesArr containsObject: image]) {
            } else{
                [self.imagesArr addObject:image];
            }
        }
        
        if (changeImgarray.count < self.imagesArr.count) {
            for (int i = 0; i < self.imagesArr.count; i++) {
                UIImage *image = self.imagesArr[i];
                if ([changeImgarray containsObject: image]) {
                    
                } else{
                    [self.imagesArr removeObjectAtIndex:i];
                    //            移除照片
                    [self.imageUrlsArr removeObjectAtIndex:i];
                    ZPLog(@"移除照片%d",i);
                    _imageCount--;
                    
                }
            }
        }
    });
    
    
}



//- (void)uploadImage:(UIImage *)image index:(NSInteger )index{
//
//    NSString *URL = [NSString stringWithFormat:@"%@%@?type=topic",KURL,KImgUpload];
//    // 1.创建请求管理者
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
//                                                         @"text/html",
//                                                         @"image/jpeg",
//                                                         @"image/png",
//                                                         @"application/octet-stream",
//                                                         @"text/json",
//                                                         @"multipart/form-data",
//                                                         nil];
//    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
//
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"token"] forHTTPHeaderField:@"token"];   //token
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"device_id"] forHTTPHeaderField:@"device_id"];
//    [manager.requestSerializer setValue:[HttpRequest getsignwithURL:URL] forHTTPHeaderField:@"sign"];
//
//    [manager POST:URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        NSData *imageData = UIImageJPEGRepresentation(image, 1.0 );
//
////        CGFloat maxQuality = 0.9f;
////        while (imageData.length > 5*1024*1024 && maxQuality > 0.1f) {
////            maxQuality -= 0.08f;
////            imageData = UIImageJPEGRepresentation(image, maxQuality);
////        }
////        ZPLog(@"%lu,%f",(unsigned long)imageData.length/1024,maxQuality);
//
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.dateFormat = @"yyyyMMddHHmmss";
//        NSString *str = [formatter stringFromDate:[NSDate date]];
//        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
//        NSString *name = @"file";
//        [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
//        ZPLog(@"%@",formData);
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        ZPLog(@"%@",json);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//            NSString *imageURL = [NSString stringWithFormat:@"%@",json[@"data"]];
//            [self.imageUrlsArr addObject:imageURL];
//            ZPLog(@"%ld",(long)index);
//            [self setupImage:image];
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        ZPLog(@"%@",error);
//
//    }];
//}



//防止输入图片描述的输入框被键盘遮挡
-(void)textViewDidBeginEditing:(YYTextView *)textView{
    if (textView.tag != 1000) {
        if ((textView.origin.y + 50) >(kScreenHeight - self.keyboardHeight)) {
            [self.contentTextView setContentOffset:CGPointMake(0, (textView.origin.y + 50) - (kScreenHeight - self.keyboardHeight)) animated:YES];
        }
    }
}


- (void)endEditing{
    [self.view endEditing:YES];
}

#pragma mark - setter
- (YYTextView *)contentTextView {
    if (!_contentTextView) {
        YYTextView *textView = [[YYTextView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-45-NaviH)];
        
        textView.height = self.view.bounds.size.height-45-NaviH;
        textView.tag = 1000;
        textView.textContainerInset = UIEdgeInsetsMake(70, 16, 20, 16);
        textView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
        textView.scrollIndicatorInsets = textView.contentInset;
        textView.delegate = self;
        textView.placeholderText = @"想说点什么呢...";
        textView.placeholderFont = [UIFont systemFontOfSize:13];
        textView.font = [UIFont systemFontOfSize:14];
        textView.textColor = [UIColor colorWithHexString:@"767676"];
        textView.selectedRange = NSMakeRange(textView.text.length, 0);
        textView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        textView.allowsPasteImage = YES;
        textView.allowsPasteAttributedString = YES;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;// 字体的行间距
        textView.typingAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15],
                                      NSParagraphStyleAttributeName:paragraphStyle
                                      };
        textView.inputAccessoryView = [self textViewBar];
        _contentTextView = textView;
        
    }
    return _contentTextView;
}


//读草稿
- (void)readTopicDraft{

    if (_topicDraftsDic) {
        _contentStr = _topicDraftsDic[@"contentStr"];
        _imageUrlsArr = _topicDraftsDic[@"imageUrlsArr"];
        _imageCount = _imageUrlsArr.count;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSMutableAttributedString *attstr =  [Tool makeAttstring:_imageUrlsArr contentAtr:_contentStr];
                dispatch_async(dispatch_get_main_queue(), ^{
                    _contentTextView.attributedText = attstr;
                    _contentTextView.font = [UIFont systemFontOfSize:15];
                });
        });
        _titleTextView.text = _topicDraftsDic[@"topic_title"];
    }
}


- (UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 52)];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, 51, kScreenWidth-20, 1)];
        line.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        [_titleView addSubview:line];
        _titleTextView = [[YYTextView alloc] initWithFrame:CGRectMake(15, 6, kScreenWidth-30, 40)];
        _titleTextView.scrollIndicatorInsets = _titleTextView.contentInset;
        _titleTextView.delegate = self;
        _titleTextView.placeholderText = @"帖子标题（必填）";
        _titleTextView.placeholderFont = [UIFont systemFontOfSize:17];
        _titleTextView.font = [UIFont systemFontOfSize:17];
        _titleTextView.selectedRange = NSMakeRange(_titleTextView.text.length, 0);
        _titleTextView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _titleTextView.allowsPasteImage = YES;
        _titleTextView.allowsPasteAttributedString = YES;
        _titleTextView.typingAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17]};
        [_titleView addSubview:_titleTextView];
        
        [self readTopicDraft];

    }
    return _titleView;
}



- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-effectViewH-NaviH-45, kScreenWidth, 45)];
        _bottomView.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
        line.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        [_bottomView addSubview:line];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(20, 0, 19, 45);
        [btn setImage:[UIImage imageNamed:@"选项"] forState:UIControlStateNormal];
        [btn  addTarget:self action:@selector(endEditing) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:btn];
        // 添加图片
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame = CGRectMake(kScreenWidth-60, 0, 40, 45);
        [btn1 setImage:[UIImage imageNamed:@"发帖_拍照"] forState:UIControlStateNormal];
        [btn1 addTarget:self action:@selector(presentimagePickerVC) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:btn1];

    }
    return _bottomView;
}


- (void)presentimagePickerVC {
    [self.navigationController presentViewController:self.imagePickerVc animated:YES completion:nil];

}


- (UIToolbar *)textViewBar {
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    // 空白格
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    // 关闭箭头
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 40 , 40);
    [btn setImage:[UIImage imageNamed:@"选项"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(endEditing) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:btn];
    // 添加图片
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, 40, 45);
    [btn1 setImage:[UIImage imageNamed:@"发帖_拍照"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(presentimagePickerVC) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:btn1];
    bar.items = @[left, space, right];
    return bar;
}



-(TZImagePickerController *)imagePickerVc {
    _imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    _imagePickerVc.allowPickingOriginalPhoto = NO;
    _imagePickerVc.naviTitleColor = [UIColor blackColor];
    _imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"50D0F4"];
    _imagePickerVc.isStatusBarDefault = YES;
    _imagePickerVc.delegate = self;
    _imagePickerVc.allowTakePicture = YES;
    _imagePickerVc.allowPickingVideo = NO;
    _imagePickerVc.allowPickingGif = NO;
    _imagePickerVc.naviBgColor = [UIColor whiteColor];
    return _imagePickerVc;
}


-(NSMutableArray *)imageUrlsArr{
    if (!_imageUrlsArr) {
        _imageUrlsArr = [NSMutableArray array];
    }
    return _imageUrlsArr;
}

-(NSMutableArray *)imagesArr{
    if (!_imagesArr) {
        _imagesArr = [NSMutableArray array];
    }
    return _imagesArr;
}

-(NSMutableArray *)desArr{
    if (!_desArr) {
        _desArr = [NSMutableArray array];
    }
    return _desArr;
}


- (IBAction)back:(id)sender {
    
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
