//
//  ChildCareClassifyView.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChildCareClassifyView.h"

//#define HeaderLabWidth 67

@implementation ChildCareClassifyView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _ageArray = @[@"0-3月",@"3-12月",@"1-2岁",@"2-3岁",@"3-6岁",@"6-13岁"];
        [self uploadUI];
//        [self getChildCareLabelList];
    }
    return self;
}

//
//- (void)getChildCareLabelList{
//    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,@"LabelController/getChildcareLabelList"];
//    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] success:^(id responseObject) {
//        NSDictionary *json = responseObject;
//        ZPLog(@"%@\n%@",json,json[@"message"]);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//            NSArray *data = json[@"data"];
//            for (NSDictionary *dic in data) {
//                ChildCareClassifyModel *classifymodel = [ChildCareClassifyModel modelWithJSON:dic];
//                [self.classifyArray addObject:classifymodel];
//
//                NSMutableArray *labelList = [NSMutableArray array];
//                for (NSDictionary *dict in classifymodel.labelList) {
//                    LabelListModel *lablistmodel = [LabelListModel modelWithJSON:dict];
//                    [labelList addObject:lablistmodel];
//                }
//                [self.labelArray addObject:labelList];
//            }
//            [self.collectionView reloadData];
//        } else {
//            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//        }
//
//    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//
//    }];
//}


- (void)uploadUI{
    self.backgroundColor =[UIColor whiteColor];
    [self addSubview:self.collectionView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.ageArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    UILabel *label = [[UILabel alloc] initWithFrame:cell.bounds];
    label.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
    label.layer.cornerRadius = 5;
    label.layer.masksToBounds = YES;
    label.tag = 2000;
    label.textColor = [UIColor colorWithHexString:@"575757"];
    label.font = FontSize(12);
    label.textAlignment = NSTextAlignmentCenter;
    [cell.contentView addSubview:label];
    label.text = _ageArray[indexPath.row];
    return cell;
}


/**************修改选中cell的背景颜色*******************/
//选中时的操作
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    //选中之后的cell变颜色
    [self updateCellStatus:cell selected:YES];
    
    if (self.chooseItemBlock) {
        self.chooseItemBlock(indexPath.row,_ageArray[indexPath.row]);
    }
}
//取消选中操作
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self updateCellStatus:cell selected:NO];
    
}
// 改变cell的背景颜色
-(void)updateCellStatus:(UICollectionViewCell *)cell selected:(BOOL)selected {
    UILabel *lab = (UILabel *)[cell viewWithTag:2000];
    lab.backgroundColor = selected ? [UIColor colorWithHexString:@"50D0F4"]:[UIColor colorWithHexString:@"F1F1F1"];
    lab.textColor = selected ? [UIColor whiteColor]:[UIColor colorWithHexString:@"575757"];

}
    



- (UICollectionView *)collectionView{
    if (_collectionView == nil) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init]; 
        layout.itemSize = CGSizeMake((kScreenWidth-50)/4, 40);
        layout.minimumLineSpacing = 10.0;
        layout.minimumInteritemSpacing = 0.0;
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.headerReferenceSize = CGSizeMake(self.frame.size.width, 10);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.frame collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator = YES;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
//        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];

    }
    return _collectionView;
}

//- (NSMutableArray *)classifyArray{
//    if (_classifyArray == nil) {
//        _classifyArray = [NSMutableArray array];
//    }
//    return _classifyArray;
//}
//
//
//- (NSMutableArray *)labelArray{
//    if (_labelArray == nil) {
//        _labelArray = [NSMutableArray array];
//    }
//    return _labelArray;
//}
@end
