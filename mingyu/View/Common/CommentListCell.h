//
//  CommentListCell.h
//  mingyu
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"


@protocol CommentListCellDelegate <NSObject>

/** 回复 */
- (void)commentReplayButtonClicked:(CommentModel*)commentmodel;

/** 查看评论 */
- (void)commentDetailButtonClicked:(CommentModel*)commentmodel;

/** 点击用户头像 查看用户信息 */
- (void)commentUserheaderClicked:(NSInteger )user_id;

@end

@interface CommentListCell : UITableViewCell

@property (nonatomic, weak) id <CommentListCellDelegate>delegate;

//评论cell
@property (weak, nonatomic) IBOutlet UIButton *userhead_Button;
@property (weak, nonatomic) IBOutlet UILabel *comment_name_Lab;
@property (weak, nonatomic) IBOutlet UILabel *comment_time_Lab;
@property (weak, nonatomic) IBOutlet UIButton *praise_number_Button;
@property (weak, nonatomic) IBOutlet UIButton *replay_Button;
@property (weak, nonatomic) IBOutlet UILabel *comment_content_Lab;
@property (weak, nonatomic) IBOutlet UIView *replay_View;
@property (weak, nonatomic) IBOutlet UILabel *replay_content_Lab;
@property (weak, nonatomic) IBOutlet UILabel *replay_count_Lab;



//总评论数
@property (weak, nonatomic) IBOutlet UILabel *comment_number_lab;
@property (weak, nonatomic) IBOutlet UIButton *comment_number_Button;
@property (weak, nonatomic) IBOutlet UIImageView *arrowimage;



//更多
@property (weak, nonatomic) IBOutlet UIButton *MoreButton;


@property (nonatomic, copy) void (^MoreButtonBlock)(void);


@property (nonatomic,readonly, strong) CommentModel *commentmodel;
@property (nonatomic, assign) NSInteger praise_type;


/**

 @param commentmodel model
 @param praisetype 点赞类型  视频评论点赞 1
                            帖子点赞   2
                            帖子评论点赞 3
                            资讯点赞    4
                            资讯评论点赞  5
                            育儿必读点赞  6
                            音频点赞    7
                            音频评论点赞  8
                            音频书的点赞  9
                            育儿必读评论点赞 10
                            视频评论点赞    11
 */
- (void)setcellWithModel:(CommentModel *)commentmodel praiseType:(NSInteger )praisetype;

+ (instancetype)theCommentCellWithTableView:(UITableView *)tableView;

+ (instancetype)theCommentNumberCell;

+ (instancetype)TheMoreCell;

+ (instancetype)theNoCommentCell;

+ (instancetype)theRecommendHeaderView;


+ (CGFloat)cellHeight;


@end
