//
//  ParentChildCareViewController.m
//  mingyu
//
//  Created by apple on 2018/5/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentChildCareViewController.h"
#import "ChildCareBaseViewController.h"

#define pageMenuH 50
#define scrollViewHeight (kScreenHeight-pageMenuH-NaviH-effectViewH)

@interface ParentChildCareViewController ()<SPPageMenuDelegate, UIScrollViewDelegate,UIGestureRecognizerDelegate>

//@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;

@property (nonatomic, strong) NSMutableArray *labelArray;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) ChildCareClassifyView *classifyView;

@property (nonatomic, strong) UIView *maskView;

@end

@implementation ParentChildCareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _index = [userDefault integerForKey:KUDchildCareAgeIndex];
    NSArray *ageArray = @[@"0-3月",@"3-12月",@"1-2岁",@"2-3岁",@"3-6岁",@"6-13岁"];
    [_rightItem setTitle:ageArray[_index] forState:0];
    [_rightItem setImage:ImageName(@"展开_green") forState:0];
    _rightItem.frame = CGRectMake(0, 0, 70, 40);
    [_rightItem setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0,0)];
    [_rightItem setImageEdgeInsets:UIEdgeInsetsMake(0,55, 0,0)];
    
    [self getChildCareLabelList];
}


- (void)getChildCareLabelList{
    self.labelArray = [NSMutableArray array];
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KClassifyId,@(20)];  //x查育儿必读标签接口
    NSDictionary *parameters = @{
                                 @"age_group":@(_index+1),
                                 };
    ZPLog(@"%@",parameters);
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                LabelListModel *model = [LabelListModel modelWithJSON:dic];
                [self.labelArray addObject:model];
            }
            [self archiveArray:self.labelArray index:_index];

            [self setuppageMenu];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
    }];
}


- (void) archiveArray:(NSMutableArray *)array index:(NSInteger )index{
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib withIntermediateDirectories:NO attributes:nil error:nil];
    }
    NSString *fileName = [NSString stringWithFormat:@"ChildCareTag%ld",(long)index];
    NSString *entityArchive = [strLib stringByAppendingPathComponent:fileName];
    NSMutableArray *archArray = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *newArray = [NSMutableArray array];
    for (LabelListModel *model in array) {
        __block LabelListModel *midModel = model;
        [archArray enumerateObjectsUsingBlock:^(LabelListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.label_id == model.label_id) {//数组中已经存在该对象
                midModel = obj;
                *stop = YES;
            }
        }];
        [newArray addObject:midModel];
    }
    [NSKeyedArchiver archiveRootObject:newArray toFile:entityArchive];//归档(序列化)
}



- (void)setuppageMenu{
    
    [self.view addSubview:self.pageMenu];
    
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib withIntermediateDirectories:NO attributes:nil error:nil];
    }
    NSString *fileName = [NSString stringWithFormat:@"ChildCareTag%ld",(long)_index];
    NSString *entityArchive = [strLib stringByAppendingPathComponent:fileName];
    NSArray *archArray = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    
    NSMutableArray *array = [NSMutableArray array];
    for (LabelListModel *lablistmodel in archArray) {
        [array addObject:lablistmodel.label_name];
    }
    [_pageMenu setItems:array selectedItemIndex:0];

    for (LabelListModel *labellistmodel in archArray) {
        ChildCareBaseViewController *baseVc = [[ChildCareBaseViewController alloc] init];
        baseVc.labelModel = labellistmodel;
        baseVc.agesection = _index;
        [self addChildViewController:baseVc];
        [self.myChildViewControllers addObject:baseVc];
    }
    
    [self.view addSubview:self.scrollView];
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = _scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.myChildViewControllers.count) {
        ChildCareBaseViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:baseVc.view];
        baseVc.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth, scrollViewHeight);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(archArray.count*kScreenWidth, 0);
    }
}




#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    if (self.myChildViewControllers.count <= toIndex) {return;}
    
    UIViewController *targetViewController = self.myChildViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([targetViewController isViewLoaded]) return;
    
    targetViewController.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth, scrollViewHeight);
    [_scrollView addSubview:targetViewController.view];
}


- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, pageMenuH, kScreenWidth, scrollViewHeight);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, pageMenuH) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(14);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}


- (void)classifyviewShow{
    [self.view bringSubviewToFront: self.maskView];
    [self.view bringSubviewToFront: self.classifyView];

    if (self.maskView.alpha == 0) {
        self.maskView.alpha = 0.7;
        self.classifyView.alpha = 1;
        [self.rightItem setImage:ImageName(@"收起_green") forState:0];
    } else{
        self.maskView.alpha = 0;
        self.classifyView.alpha = 0;
        [self.rightItem setImage:ImageName(@"展开_green") forState:0];
    }
}


- (ChildCareClassifyView *)classifyView{
    if (_classifyView == nil) {
        _classifyView = [[ChildCareClassifyView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,150)];
        __weak typeof(self) Weakself = self;
        _classifyView.chooseItemBlock = ^(NSInteger index, NSString *agesection) {
            [userDefault setInteger:index forKey:KUDchildCareAgeIndex];
            [userDefault synchronize];
            _index = index;
            [Weakself.rightItem setTitle:agesection forState:0];
            [Weakself classifyviewShow];
            
            [Weakself.pageMenu removeFromSuperview];
            Weakself.pageMenu  = nil;
            [Weakself.scrollView removeFromSuperview];
            Weakself.scrollView  = nil;
            [Weakself.myChildViewControllers removeAllObjects];
            
//            [Weakself setuppageMenu];

            [Weakself getChildCareLabelList];
        };
        _classifyView.alpha = 0;
        [self.view addSubview:_classifyView];
        
    }
    return _classifyView;
}




- (UIView *)maskView{
    if (_maskView == nil) {
        _maskView = [[UIView alloc] initWithFrame:self.view.bounds];
        _maskView.alpha = 0;
        _maskView.backgroundColor = [UIColor colorWithHexString:@"202020"];
        [self.view addSubview:_maskView];
        
        //单击的手势
        UITapGestureRecognizer *tapRecognize = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(classifyviewShow)];
        tapRecognize.numberOfTapsRequired = 1;
        tapRecognize.delegate = self;
        [tapRecognize setEnabled :YES];
        [tapRecognize delaysTouchesBegan];
        [tapRecognize cancelsTouchesInView];
        
        [_maskView addGestureRecognizer:tapRecognize];
    }
    return _maskView;
}


#pragma mark - getter
- (NSMutableArray *)myChildViewControllers {
    
    if (!_myChildViewControllers) {
        _myChildViewControllers = [NSMutableArray array];
        
    }
    return _myChildViewControllers;
}

- (IBAction)classifyButton:(id)sender {
    [self classifyviewShow];
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
