//
//  VideoBaseViewController.m
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoBaseViewController.h"
#import "ParentVideoDetailViewController.h"

@interface VideoBaseViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayData;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, strong) NSMutableArray *timeArray;
@property (nonatomic, assign) BOOL isScrollUp;
@property (nonatomic, strong) NSNumber *lastMaxtime;
@property (nonatomic, strong) NSNumber *lastMintime;
@property (nonatomic, assign) BOOL otherCircle;
@property (nonatomic, strong) NSNumber *record_min_time;
@property (nonatomic, strong) NothingView *nothingView;
@property (nonatomic, assign) dispatch_once_t onceToken;

@property (nonatomic, assign) BOOL firstHttpRequest;

@end

@implementation VideoBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];

    _lastMintime = _labModel.min_time?[_labModel.min_time copy]:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    _lastMaxtime = _labModel.max_time?_labModel.max_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    
    [self refreshTableview];
    _timeArray = [NSMutableArray array];
}



//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        [_tableView.mj_footer resetNoMoreData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _isScrollUp = NO;
            [self getShortVideoList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            _isScrollUp = YES;
            [self getShortVideoList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


- (void)getShortVideoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KGetShortVideoList,@(_labModel.label_id)];
    NSNumber *min = _labModel.min_time?_labModel.min_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    NSNumber *max = _labModel.max_time?_labModel.max_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    if ([min floatValue] > [max floatValue]) {
        min = max;
    }
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"min_time":min,
                                 @"max_time":max,
                                 @"record_max_time":_lastMaxtime
                                 };
    ZPLog(@"%@",parameters);
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        _firstHttpRequest = YES;
        if (success) {
            NSDictionary *data = json[@"data"];
            NSArray *videoList = data[@"videoList"];
            _record_min_time = [NSNumber numberWithString:[NSString stringWithFormat:@"%@", data[@"record_min_time"]]];
            if (_record_min_time) {
                _otherCircle = YES;
                [_timeArray addObject:_record_min_time];
                for (NSNumber *num in [_timeArray copy]) {
                     if ([num floatValue] < [_record_min_time floatValue]) {
                         [_timeArray removeObject:num];
                     }
                }
            }
            for (NSDictionary *dic in videoList) {
                VideoModel *model = [VideoModel modelWithJSON:dic];
                NSNumber *videoTime = [NSNumber numberWithString:model.create_time];
                
                if (_otherCircle && [videoTime floatValue] < [_lastMintime floatValue]) {
                    _lastPage = YES;
                    break;
                }
                
                if (_isScrollUp) {
                    [self.arrayData addObject:model];
                } else {
                    [self.arrayData insertObject:model atIndex:0];
                }
                [_timeArray addObject:videoTime];
            }

            [self changeVideoTagMaxMineTime];
            if (self.arrayData.count <= 3) {
                dispatch_once(&_onceToken, ^{
                    [self getShortVideoList];
                });
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        _firstHttpRequest = YES;
        [self.tableView reloadData];
    }];
}


- (void)changeVideoTagMaxMineTime{
    NSNumber *max = [_lastMaxtime floatValue]>[[_timeArray valueForKeyPath:@"@max.self"] floatValue] ? _lastMaxtime:[_timeArray valueForKeyPath:@"@max.self"];
    NSNumber *min = _record_min_time?_record_min_time:[_timeArray valueForKeyPath:@"@min.self"];
    
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *entityArchive = [strLib stringByAppendingPathComponent:@"VideoTag"];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    [array enumerateObjectsUsingBlock:^(LabelListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.label_id == _labModel.label_id) {//数组中已经存在该对象
            obj.max_time = max;
            obj.min_time = min;
            _labModel.min_time = min;
            _labModel.max_time = max;
        }
    }];
    [NSKeyedArchiver archiveRootObject:array toFile:entityArchive];//归档(序列化)
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return self.arrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_arrayData && _arrayData.count > indexPath.row) {
        cell.videomodel = [_arrayData objectAtIndex:indexPath.row];
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData.count > indexPath.row) {
        [MobClick event:@"VideoBaseViewController" label:_labModel.label_name];  //友盟统计

        VideoModel *model = _arrayData[indexPath.row];
        ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
        VC.video_id = model.video_id;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.arrayData && self.arrayData.count > 0) {
        return [UIView new];
    } else {
        return self.nothingView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (!_firstHttpRequest) {
        return 0;
    }
    if (self.arrayData && self.arrayData.count > 0) {
        return 0;
    } else {
        return self.tableView.height;
    }
}

- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 230;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text =  @"暂无视频";
        _nothingView.imageView.image = ImageName(@"短视频 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}

@end
