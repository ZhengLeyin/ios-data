//
//  MineViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "MineViewController.h"
#import "TimeRecordViewController.h"
#import "DownloadViewController.h"
/**已购课程*/
#import "PurchasedCourseViewController.h"
#import "PPBadgeView.h"

@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MineViewCell *headerView;

@property (nonatomic, strong) NSArray *imageArrayData;
@property (nonatomic, strong) NSArray *titleArrayData;

@property (nonatomic, strong) UserModel *usermodel;


@end

@implementation MineViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableHeaderView.height = 172;

    NSArray *titlearray1  = @[@"我的钱包"];
    NSArray *titlearray2 = @[@"已购课程",@"兴趣选择",@"时光印记",@"下载管理"];
    NSArray *titlearray3 = @[@"关于我们"];
    _titleArrayData = @[titlearray1,titlearray2,titlearray3];
    
    NSArray *imagearray1 = @[@"我的钱包"];
    NSArray *imagearray2 = @[@"我的课程",@"xingqu",@"时光印记",@"我的下载"];
    NSArray *imagearray3 = @[@"关于我们"];
    _imageArrayData = @[imagearray1,imagearray2,imagearray3];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHomeUserMessage) name:NSNotificationReceivePriVateMessage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHomeUserMessage) name:NSNotificationReceiveSystemMessage object:nil];

//    小红点
    [self.leftButton pp_addDotWithColor:[UIColor redColor]];
    [self.leftButton pp_moveBadgeWithX:-30 Y:0];
    [self.leftButton pp_hiddenBadge];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认

    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.view.backgroundColor = [UIColor whiteColor];
    if ([userDefault boolForKey:KUDhasLogin]) { 
        self.headerView.haslogin_view.hidden = NO;
        [self.headerView.user_image zp_setImageWithURL:[userDefault objectForKey:KUDuser_head] placeholderImage:ImageName(@"默认头像")];
        [self getHomeUserMessage];
    } else {
        self.headerView.haslogin_view.hidden = YES;
        self.headerView.user_image.image = ImageName(@"默认头像");
        self.headerView.usermodel = nil;
    }
}


- (void)getHomeUserMessage{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetHomeUserMessage];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _usermodel = [UserModel modelWithJSON:json[@"data"]];
            _headerView.usermodel = _usermodel;
            if (_usermodel.qq_unread_number + _usermodel.system_unread_number > 0) {
                [self.leftButton pp_showBadge];
            } else {
                [self.leftButton pp_hiddenBadge];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
}





#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titleArrayData.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_titleArrayData[section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MineViewCell *cell = [MineViewCell theMineCellWithTableview:tableView];
//    if (indexPath.section == 0 && indexPath.row == 1 && [userDefault boolForKey:KUDhasLogin]) {
//        cell.VIP_lab.hidden = NO;
//    }
    cell.title_lab.text = _titleArrayData[indexPath.section][indexPath.row];
    cell.icon_image.image = [UIImage imageNamed: _imageArrayData[indexPath.section][indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            MineWalletViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineWalletViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        }
    } else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            PurchasedCourseViewController *VC = [PurchasedCourseViewController new];
            [self.navigationController pushViewController:VC animated:YES];
        } else if (indexPath.row == 1){
            MineInterestViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineInterestViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        } else if (indexPath.row == 2){
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            if (![HttpRequest NetWorkIsOK]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            TimeRecordViewController *VC = [TimeRecordViewController new];
            [self.navigationController pushViewController:VC animated:YES];
        } else if(indexPath.row == 3){
            DownloadViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"DownloadViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        }
    } else{
        AboutUsViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
        [self.navigationController pushViewController:VC animated:YES];
    }
}


//pragma mark lazy loading...
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 53;
        _tableView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}


- (MineViewCell *)headerView{
    if (_headerView == nil) {
        _headerView = [MineViewCell theMineHeaderView];
    }
    return _headerView;
}



- (IBAction)newsButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    MineMessageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineMessageViewController"];
    VC.qq_unread_number = _usermodel.qq_unread_number;
    [self.navigationController pushViewController:VC animated:YES];
}


- (IBAction)setButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    ParentSetViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ParentSetViewController"];
    VC.is_true_password = _usermodel.is_true_password;
    [self.navigationController pushViewController:VC animated:YES];
}




// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}


@end
