//
//  TimeListView.m
//  mingyu
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TimeListView.h"

//static NSInteger PlayTime = 0;

@implementation TimeListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self updateUI];
    }
    return self;
}


- (void)updateUI{
    self.backgroundColor = [UIColor whiteColor];
    
    _timeArrayData = @[@0,@1,@10,@20,@30,@60,@90];

    _textArrayData = @[@"不开启",@"播完当前声音",@"10分钟",@"20分钟",@"30分钟",@"60分钟",@"90分钟"];
    
    [self addSubview:self.tableView];
    
    [self addSubview:self.clooseButton];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _textArrayData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
//    if (!cell) {
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
//        }
    cell.textLabel.text = self.textArrayData[indexPath.row];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth-35,0, 17, 45)];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:imageview];
    NSInteger time = [_timeArrayData[indexPath.row] integerValue];
    
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (app.PlayTime == time) {
        imageview.image = ImageName(@"一键听_xuanzhong");
    } else{
        imageview.image = ImageName(@"weixuan");
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_textArrayData.count > indexPath.row) {
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        app.PlayTime = [_timeArrayData[indexPath.row] integerValue];
        [_tableView reloadData];
        
        PlayerTimerManager *timemanger = [PlayerTimerManager shareInstance];
        [timemanger creatTiemr];
        if (indexPath.row == 0) {
            timemanger.timeout = 99999999999;
        } else if (indexPath.row == 1) {
            timemanger.timeout = 9999999;
        } else {
            timemanger.timeout = app.PlayTime*60;
        }
        [timemanger countDown];
        NSString *timeStr = _textArrayData[indexPath.row];
        [self addAudioTiming:timeStr];
    }
}

//添加定时日志
- (void)addAudioTiming:(NSString *)timestr{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KRecordLog];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resources_type":@(resources_audio),
                                 @"status":@(1),
                                 @"message_type":[constant TransactionState:(MODULE_AUDIO_TIMER)],
                                 @"other":timestr
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
        } else {
        }
    } failure:^(NSError *error) {
    }];
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, self.height-50) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 45;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


- (UIButton *)clooseButton{
    if (_clooseButton == nil) {
        _clooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _clooseButton.frame = CGRectMake(0, self.height-50, kScreenWidth, 50);
        [_clooseButton setBackgroundColor:[UIColor whiteColor]];
        [_clooseButton setTitle:@"关闭" forState:UIControlStateNormal];
        _clooseButton.titleLabel.font = FontSize(15);
        [_clooseButton setTitleColor:[UIColor colorWithHexString:@"2C2C2C"] forState:UIControlStateNormal];
        [_clooseButton addTarget:self action:@selector(clooseButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _clooseButton;
}

- (void)clooseButtonClicked{
    if (self.clooseBlock) {
        self.clooseBlock();
    }
}

@end
