//
//  PersonalHomepageViewController.m
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PersonalHomepageViewController.h"
#import "PrivateMessageDetailViewController.h"

@interface PersonalHomepageViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *head_image;
@property (weak, nonatomic) IBOutlet UILabel *name_lab;
@property (weak, nonatomic) IBOutlet UILabel *introduce_lab;
@property (weak, nonatomic) IBOutlet UIButton *attention_button;
@property (weak, nonatomic) IBOutlet UIButton *sendMessage;


@property (nonatomic, strong) UserModel *usermodel;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) NSInteger PageMenuIndex;

@end

@implementation PersonalHomepageViewController

- (void)awakeFromNib{
    [super awakeFromNib];
    _attention_button.layer.borderWidth = 0.5;
    _attention_button.layer.borderColor = [UIColor redColor].CGColor;
    _attention_button.layer.cornerRadius = 3;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    if (_user_id == [USER_ID integerValue]) {
        _sendMessage.hidden = YES;
    }
    [self.view addSubview:self.pageMenu];
    
    MinePublicViewController *PublicVC = [[MinePublicViewController alloc] init];
   PublicVC.user_id = _user_id;
    [self addChildViewController:PublicVC];

    MineAttentionViewController *AttentionVC = [[MineAttentionViewController alloc] init];
    AttentionVC.from_id = _user_id;
    [self addChildViewController:AttentionVC];
    
    MineFansViewController *FansVC = [[MineFansViewController alloc] init];
    FansVC.from_id = _user_id;
    [self addChildViewController:FansVC];
    
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-44-105-45);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
    [self getHomeUserMessage];
    [self.view bringSubviewToFront:_sendMessage];
}


- (void)getHomeUserMessage{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetOtherUserMessage];
    NSDictionary *parameters = @{
                                 @"user_id":@(_user_id),
                                 @"from_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _usermodel = [UserModel modelWithJSON:json[@"data"]];
            [self refreshHeadwithModel:_usermodel];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)refreshHeadwithModel:(UserModel *)model{
    _head_image.layer.cornerRadius = _head_image.height/2;
    _head_image.layer.masksToBounds = YES;
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.user_head];
    [_head_image zp_setImageWithURL:model.user_head placeholderImage:ImageName(@"默认头像")];
    _name_lab.text = model.user_name;
    if (_usermodel.area_name) {
        _introduce_lab.text = [NSString stringWithFormat:@"%@ | %@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈",_usermodel.area_name];
    } else {
        _introduce_lab.text = [NSString stringWithFormat:@"%@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈"];
    }
    
    _attention_button.layer.cornerRadius = 3;
    _attention_button.layer.borderWidth = 0.5;
    if (model.follow_status == 1) {
        _attention_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_attention_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_attention_button setTitle:@"+关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    } else if (model.follow_status == 2){
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"已关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    } else{
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"相互关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    }
    _attention_button.layer.masksToBounds = YES;
    
    
    self.dataArr = @[[NSString stringWithFormat:@"帖子(%ld)",model.topicNumber],[NSString stringWithFormat:@"关注(%ld)",model.userFollowNumber],[NSString stringWithFormat:@"粉丝(%ld)",model.followUserNumber]];
//    self.dataArr = @[@"帖子",@"关注",@"粉丝"];

    for (int i = 0; i < self.dataArr.count; i++) {
        [_pageMenu setTitle:self.dataArr[i] forItemAtIndex:i];
    }

    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-44-105-45);
    [_scrollView addSubview:VC.view];
}




- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 44+105, kScreenWidth, kScreenHeight-NaviH-44-105-45);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"             帖子              ",@"                关注              ",@"                粉丝              "];
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 105,  kScreenWidth, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:0];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollAdaptContent;
    }
    return _pageMenu;
}


- (IBAction)attention_button:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _attention_button.userInteractionEnabled = NO;
    if (_usermodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_usermodel.user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _usermodel.follow_status = follow_status;
                [self refreshHeadwithModel:_usermodel];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_usermodel.user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _usermodel.follow_status = 1;
                [self refreshHeadwithModel:_usermodel];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}



- (IBAction)sendMessage:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    PrivateMessageDetailViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PrivateMessageDetailViewController"];
    VC.user_id = _user_id;
    VC.user_head = _usermodel.user_head;
    VC.title = _usermodel.user_name;
    [self.navigationController pushViewController:VC animated:YES];

}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}



@end
