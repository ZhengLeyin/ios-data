////
////  commenModel.h
////  mingyu
////
////  Created by apple on 2018/4/14.
////  Copyright © 2018年 TZWY. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@interface commenModel : NSObject
//
///**
//评论ID
// */
//@property (nonatomic, assign) NSInteger comment_id;
//
///**
// 用户ID
// */
//@property (nonatomic, assign) NSInteger fk_user_id;
//
///**
// 目标ID
// */
//@property (nonatomic, assign) NSInteger fk_from_id;
//
///**
// 评论内容
// */
//@property (nonatomic, copy) NSString *comment_content;
//
///**
// 用户名字
// */
//@property (nonatomic, copy) NSString *comment_name;
//
///**
// 用户头像
// */
//@property (nonatomic, copy) NSString *comment_head;
//
///**
// 评论时间
// */
//@property (nonatomic, assign) NSInteger comment_time;
//
///**
// 是否点赞
// */
//@property (nonatomic, assign) NSInteger click_praise_true;
//
///**
// 点赞数
// */
//@property (nonatomic, assign) NSInteger praise_number;
//
//
////我的帖子的评论列表中用到
//
//
///**
// 帖子ID
// */
//@property (nonatomic, assign) NSInteger topic_id;
//
///**
// 帖子标题
// */
//@property (nonatomic, copy) NSString *topic_title;
//
//
//
//@end

