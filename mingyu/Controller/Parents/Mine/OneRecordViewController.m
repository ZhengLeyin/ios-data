//
//  OneRecordViewController.m
//  mingyu
//
//  Created by apple on 2018/8/6.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "OneRecordViewController.h"

@interface OneRecordViewController ()<SDCycleScrollViewDelegate,UITextViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *cycleView;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *pageLab;

@property (nonatomic, strong) DiaryModel *diarymodel;
@property (nonatomic, strong) UIImageView *navigationBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *moreButton;

@end

@implementation OneRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReceiveNotification) name:NSNotificationReceiveNotification object:nil];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)ReceiveNotification{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    
//    [self.navigationController.navigationBar setBackgroundImage:ImageName(@"图片遮罩") forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    self.navigationController.navigationBar.translucent = YES;

    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceiveNotification object:nil];

//    self.navigationController.navigationBar.alpha = 0;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self getDiary];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
//    self.navigationController.navigationBar.translucent = NO;

//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    self.navigationController.navigationBar.alpha = 1;
}


- (void)getDiary {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetDiary];
    NSDictionary *parameters = @{
                                 @"diary_id":@(_diary_id),
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _diarymodel = [DiaryModel modelWithJSON:json[@"data"]];
            _diarymodel.diary_id = _diary_id;
            [self setupSubView];

        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)setupSubView{
    
    NSArray *array = _diarymodel.imageList;
  
    NSMutableArray *images = [NSMutableArray array];
    for ( NSDictionary *dic in array) {
        DiaryPhotoModel *model = [DiaryPhotoModel modelWithJSON:dic];
        model.image_name = [NSString stringWithFormat:@"%@%@",KKaptcha,model.image_name];
        [images addObject:model.image_name];
    }

    if (_diarymodel.imageList && _diarymodel.imageList.count > 0) {
        [self.view addSubview:self.cycleView];
        [_cycleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.view);
            make.height.offset(kScreenHeight*2/3);
            
        }];
        @weakify(self)
        _cycleView.itemDidScrollOperationBlock = ^(NSInteger currentIndex) {
            @strongify(self)
            NSMutableAttributedString *pageStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%ld /%ld",currentIndex+1 ,images.count]];
            NSRange range = NSMakeRange(0, 1);
            [pageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:range];
            [self.pageLab setAttributedText:pageStr];
        };
        _cycleView.imageURLStringsGroup = images;
        
        [self.view addSubview:self.pageLab];
        [_pageLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(kScreenHeight*2/3-40);
            make.left.offset(kScreenWidth-60);
            make.width.offset(50);
            make.height.offset(25);
        }];
        NSMutableAttributedString *pageStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"1 /%ld",images.count]];
        NSRange range = NSMakeRange(0, 1);
        [pageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20] range:range];
        [self.pageLab setAttributedText:pageStr];
        
        [self.view addSubview:self.textView];
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(kScreenHeight*2/3);
            make.left.right.bottom.equalTo(self.view);
        }];
        
    } else {
        [self.view addSubview:self.textView];
        [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.top.offset(100);
        }];
    }
    [self.view addSubview:self.navigationBar];
    if (array.count > 0) {
        _navigationBar.image = [UIImage imageNamed:@"图片遮罩"];
        [_backButton setImage:ImageName(@"back_white") forState:0];
        [_moreButton setImage:ImageName(@"更多_白色") forState:0];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    } else {
//        _navigationBar.image = [UIImage imageNamed:@""];
        [_backButton setImage:ImageName(@"back_black") forState:0];
        [_moreButton setImage:ImageName(@"更多_黑色") forState:0];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    }
}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return NO;
}

//删除时光记录
- (void)delDiary{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelDiary];
   
    NSDictionary *parameters = @{
                                 @"diary_id":@(_diary_id)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
            [UIView ShowInfo:@"删除成功" Inview:self.view];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationDeleteTimeRecord object:nil userInfo:nil];
            [self performSelector:@selector(back:) withObject:nil afterDelay:1];

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



- (IBAction)moreButton:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        AddRecordViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddRecordViewController"];
        VC.diarymodel = _diarymodel;
        [self.navigationController pushViewController:VC animated:YES];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        ZPLog(@"点击了取消");
    }];
    [action2 setValue:[UIColor colorWithHexString:@"2C2C2C"] forKey:@"_titleTextColor"];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        //跳到创建alertview的方法，一般在点击删除这里按钮之后，都需要一个提示框，提醒用户是否真的删除
        [self creatAlertController_alert];
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

//创建一个alertview
-(void)creatAlertController_alert {
    
    [self showAlertWithTitle:nil message:@"确定删除该条时光印记吗" noBlock:nil yseBlock:^{
        [self delDiary];
    }];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (UILabel *) pageLab {
    if (!_pageLab) {
        _pageLab = [[UILabel alloc] init];
        _pageLab.backgroundColor = [UIColor colorWithHexString:@"202020" alpha:0.4];
        _pageLab.textAlignment = NSTextAlignmentCenter;
        _pageLab.textColor = [UIColor whiteColor];
        _pageLab.font = FontSize(12);
        _pageLab.layer.cornerRadius = 12;
        _pageLab.layer.masksToBounds  = YES;
    }
    return _pageLab;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc] init];
        _textView.delegate = self;
        _textView.text = _diarymodel.diary_content;
        _textView.textContainerInset = UIEdgeInsetsMake(20, 20, 20, 20);
        _textView.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        _textView.font = FontSize(20);
        [_textView setContentOffset:CGPointZero];
    }
    return _textView;
}

- (SDCycleScrollView *)cycleView{
    if (!_cycleView) {
        _cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
        _cycleView.backgroundColor = [UIColor blackColor];
        _cycleView.infiniteLoop = NO;
        _cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
        _cycleView.layer.masksToBounds  = YES;
        _cycleView.autoScroll = NO;
        _cycleView.showPageControl = NO;

    }
    return _cycleView;
}


- (UIImageView *)navigationBar{
    if (!_navigationBar) {
        _navigationBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
        _navigationBar.userInteractionEnabled = YES;
//        _navigationBar.image = [UIImage ]
//        _navigationBar.backgroundColor = [UIColor whiteColor];
//        _navigationBar.alpha = 0;
        
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(back:)forControlEvents:UIControlEventTouchUpInside];
        [_navigationBar addSubview:_backButton];
        [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.bottom.equalTo(_navigationBar);
            make.width.mas_offset(60);
            make.height.mas_offset(40);
        }];
        
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton addTarget:self action:@selector(moreButton:)forControlEvents:UIControlEventTouchUpInside];
        [_navigationBar addSubview:_moreButton];
        [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-15);
            make.bottom.equalTo(_navigationBar);
            make.width.mas_offset(60);
            make.height.mas_offset(40);
        }];
    }
    return _navigationBar;
}


@end
