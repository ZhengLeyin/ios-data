//
//  AlbumDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AlbumDetailViewController.h"
#import "WRNavigationBar.h"
#import "RotateButton.h"


@interface AlbumDetailViewController ()<UITableViewDelegate, UITableViewDataSource,DFPlayerDelegate,DFPlayerDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) AlbumDetailCell *headerView;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *df_ModelArray;

@property (nonatomic, strong) RotateButton *playbutton;

@property (nonatomic, strong) NSMutableArray *playerModelArray;

@property (nonatomic, strong) UIView *navigationBar;

@property (nonatomic, strong) UILabel *titleLab;

@property (nonatomic, strong) UIButton *backButton;

@property (nonatomic, strong) UIButton *shareButton;

@property (nonatomic, strong) dispatch_semaphore_t semaphore;



@end

@implementation AlbumDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.backButton];
//    [self.view addSubview:self.shareButton];
    [self.view addSubview:self.navigationBar];
    
//    _semaphore = dispatch_semaphore_create(0);

    _headerView = [AlbumDetailCell theHeaderView];
    if (_album_id) {
        _headerView.maskView.backgroundColor = [UIColor colorWithHexString:@"2C2C2C" alpha:0.3];
    } else {
        _headerView.maskView.backgroundColor = [UIColor clearColor];
    }
    @weakify(self)
    _headerView.playBlockButtonBlock = ^{
        @strongify(self)
        [self.playbutton startRotating];
        [self.playbutton resumeRotate];
        if (self.album_id) {
            if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
//                AudioModel *model = [DFPlayer shareInstance].currentAudioModel;
                if ( [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _album_id) {
                    [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_audioPause];
                    [self.playbutton stopRotating];
                } else{
                    if (self.dataArray && self.dataArray.count > 0) {
                        [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                        [self archiveObject:self.dataArray];    // 归档
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    }
                }
            } else {
                [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                if ([DFPlayer shareInstance].currentAudioModel.fk_parent_id == _album_id) {
                    [[DFPlayer shareInstance] df_audioPlay];
                } else{
                    if (self.dataArray && self.dataArray.count > 0) {
                        [self archiveObject:self.dataArray];    // 归档
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    }
                }
            }
        } else {
            if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 1 && [DFPlayer shareInstance].currentAudioModel.subject_id == self.target_id) {
                    [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_audioPause];
                    [self.playbutton stopRotating];
                } else{
                    if (self.dataArray && self.dataArray.count > 0) {
                        [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                        [self archiveObject:self.dataArray];    // 归档
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    }
                }
            } else {
                [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 1 && [DFPlayer shareInstance].currentAudioModel.subject_id == self.target_id) {
                    [[DFPlayer shareInstance] df_audioPlay];
                } else{
                    if (self.dataArray && self.dataArray.count > 0) {
                        [self archiveObject:self.dataArray];    // 归档
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    }
                }
            }
        }
    };
    _headerView.frame = CGRectMake(0, -180, kScreenWidth, 180);
    _headerView.tag = 101;
    [self.tableView addSubview:_headerView];

    [self getAudioList];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
//    ZPLog(@"%f",offsetY);
    
    //    if (offsetY > 0) {
    CGFloat alpha = (offsetY+160) / 64;
    self.navigationBar.alpha = alpha;
    if (offsetY > -160) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    }
    
    if (offsetY < -180) {
        CGRect rect = [self.tableView viewWithTag:101].frame;
        rect.origin.y = offsetY;
        rect.size.height = -offsetY;
        [self.tableView viewWithTag:101].frame = rect;
        
    }
}

//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat offsetY = scrollView.contentOffset.y;
//    if (offsetY > 0) {
//        CGFloat alpha = offsetY / 64;
//        self.navigationController.navigationBar.alpha = alpha;
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"ffffff" alpha:alpha]] forBarMetrics:UIBarMetricsDefault];
//
//        self.title = [DFPlayer shareInstance].currentAudioModel.audio_title;
//    } else {
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
//        self.title = @"";
//    }
//}



- (void)getAudioList{
    NSString *URL = [NSString string];
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    parameters[@"user_id"] = [NSString stringWithFormat:@"%@",USER_ID];
    parameters[@"start"] = @"0";
    parameters[@"size"] = @"200";

    if (_album_id) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAlbumAudioList];
        parameters[@"album_id"] = @(_album_id);
    } else {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetSubjectAudioList];
        parameters[@"subject_id"] = @(_target_id);
    }

    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [self.dataArray removeAllObjects];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,data[@"img"]];
            [_headerView.album_image  sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
            _headerView.album_title_lab.text = data[@"title"];

            NSArray *audioList = data[@"audioList"];
            for (NSDictionary *dic in audioList) {
                AudioModel *audiomodel = [AudioModel modelWithJSON:dic];
                audiomodel.fk_parent_title = data[@"title"];
                NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,dic[@"audio_image"]]];
                audiomodel.audio_image_url = url;
                audiomodel.subject_id = [data[@"subject_id"] integerValue];
                
                [self.dataArray addObject:audiomodel];
            }
            [self.tableView reloadData];
            [self archiveObject:self.dataArray];
            
            if (!_album_id) {
                if ([DFPlayer shareInstance].currentAudioModel.subject_id != self.target_id) {
                    [self.headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    [_playbutton startRotating];
                    [self.playbutton resumeRotate];

                }
            }
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)archiveObject:(NSMutableArray *)dataArray{
//创建一个归档文件夹
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }

    NSString *string = [NSString string];
    if (_album_id) {   //专辑
        string = [NSString stringWithFormat:@"archive_1_%ld",_album_id];
    } else {            //主题
        string = [NSString stringWithFormat:@"archive_4_%ld",_target_id];
    }
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *savearray = [NSMutableArray array];
    for (int i = 0; i < dataArray.count; i++) {
        AudioModel *model = dataArray[i];
        for (int j = 0; j < array.count; j++) {
            AudioModel *model2 = array[j];
            if (model.audio_id == model2.audio_id) {
                model.timeWithValue = model2.timeWithValue;
            }
        }
        [savearray addObject:model];
        self.dataArray = savearray;
    }
    if (  [NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive]) {
        ZPLog(@"success");
    }
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self initDFPlayer];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    if (![DFPlayer shareInstance].currentAudioModel) {
        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
        if ([DFPlayer shareInstance].previousAudioModel.audio_image_url) {
            [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].previousAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
            self.playbutton.hidden = NO;
        } else {
            self.playbutton.hidden = YES;
        }
    } else{
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
        self.playbutton.hidden = NO;
    }

    if (_album_id) {
        if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
            [_playbutton startRotating];
            [_playbutton resumeRotate];
            if ([DFPlayer shareInstance].currentAudioModel.fk_parent_id == _album_id) {
                [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                self.headerView.album_play_lab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
            } else{
                [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                self.headerView.album_play_lab.text = @"";
            }
        } else {
            [_playbutton stopRotating];
            [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
//            self.headerView.album_play_lab.text =@"";
        }
    } else {
        if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
            [_playbutton startRotating];
            [_playbutton resumeRotate];
            if ([DFPlayer shareInstance].currentAudioModel.audio_type == 1 && [DFPlayer shareInstance].currentAudioModel.subject_id == _target_id) {
                [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                self.headerView.album_play_lab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
            } else{
                [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                self.headerView.album_play_lab.text = @"";
            }
        } else {
            [_playbutton stopRotating];
            [_headerView.album_play_button setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
        }
    }
  
    if (_dataArray && self.dataArray.count > 0) {
        [_tableView reloadData];
    }
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    self.navigationController.navigationBar.alpha = 1;
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault; //设置状态栏颜色为黑色

}


#pragma mark - 初始化DFPlayer
- (void)initDFPlayer{
    [DFPlayer shareInstance].dataSource  = self;
    [DFPlayer shareInstance].delegate    = self;
    
}

#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.dataArray.count; i++) {
        AudioModel *yourModel    = self.dataArray[i];
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        
        if ([[yourModel.audio_path absoluteString] hasPrefix:@"http"]) {//网络音频
            model.audio_path  = yourModel.audio_path;
        }
//        else{//本地音频
//            NSString *path = [[NSBundle mainBundle] pathForResource:[yourModel.audio_path absoluteString] ofType:@""];
//            if (path) {model.audio_path = [NSURL fileURLWithPath:path];}
//        }
        [_df_ModelArray addObject:model];
    }
    if (_semaphore) {
        dispatch_semaphore_signal(_semaphore);
    }
    return self.df_ModelArray;
}



- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self tableViewReloadData];

    NSString *audioName = player.currentAudioModel.audio_title;
    self.navigationItem.title = [NSString stringWithFormat:@"%@",audioName];

    self.headerView.album_play_lab.text = player.currentAudioModel.audio_title;
    [self.playbutton sd_setImageWithURL:player.currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    self.playbutton.hidden = NO;

    if (_target_id == player.currentAudioModel.subject_id) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:player.currentAudioModel.audioId inSection:0];
        AlbumDetailCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.isplay_image.hidden = NO;
    }
    
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying && [DFPlayer shareInstance].currentAudioModel.timeWithValue < 0.95) {
        [[DFPlayer shareInstance] df_seekToTimeWithValue:[DFPlayer shareInstance].currentAudioModel.timeWithValue];
    }
}


//状态信息代理
- (void)df_player:(DFPlayer *)player didGetStatusCode:(DFPlayerStatusCode)statusCode{
    if (statusCode == 0) {
        [self showAlertWithTitle:@"没有网络连接" message:nil yesBlock:nil];
    }else if(statusCode == 1){
        [self showAlertWithTitle:@"继续播放将产生流量费用" message:nil noBlock:nil yseBlock:^{
            player.isObserveWWAN = NO;
            [player df_playerPlayWithAudioId:player.currentAudioModel.audioId];
        }];
        return;
    }else if(statusCode == 2){
        [self showAlertWithTitle:@"请求超时" message:nil yesBlock:nil];
    }else if(statusCode == 8){
        [self tableViewReloadData];return;
    }
}



- (void)tableViewReloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}



#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumDetailCell *cell = [AlbumDetailCell theCellWithTableView:tableView];
    if (self.dataArray && self.dataArray.count > indexPath.row) {
        AudioModel *audiomodel = [self.dataArray objectAtIndex:indexPath.row];
        cell.audiomodel = audiomodel;
        [cell setDownloadStatus:[YCDownloadManager downloasStatusWithId:[NSString stringWithFormat:@"%ld",audiomodel.audio_id]]];
    }
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _semaphore = dispatch_semaphore_create(0);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self archiveObject:self.dataArray];    // 归档
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
        dispatch_async(dispatch_get_main_queue(), ^{
            AlbumDetailCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.audio_title_lab.highlighted = YES;
            cell.isplay_image.hidden = NO;
            AudioModel *model = self.dataArray[indexPath.row];
            [[DFPlayer shareInstance] df_audioPlay];
            if ([DFPlayer shareInstance].currentAudioModel.audio_id == model.audio_id) {
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [self.navigationController presentViewController:VC animated:YES completion:nil];
            } else if (self.df_ModelArray.count > indexPath.row) {
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:model.audioId];
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [self.navigationController presentViewController:VC animated:YES completion:nil];
            }
            [self tableViewReloadData];
        });
    });
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -20, kScreenWidth, kScreenHeight+20) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 85;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(164, 0, 0, 0);
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 80)];
        _tableView.tableFooterView = view;
//        _tableView.tableFooterView.height = 80;
    }
    return _tableView;
}


- (NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}



- (IBAction)back:(id)sender {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
 
//    if (self.navigationController == nil) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    } else {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    
}

- (RotateButton *)playbutton{
    if (_playbutton == nil) {
        _playbutton = [RotateButton buttonWithType:UIButtonTypeCustom];
        _playbutton.frame = CGRectMake(kScreenWidth-85, kScreenHeight-effectViewH-85, 65, 65);
        [_playbutton setImage:ImageName(@"一键听_banner") forState:UIControlStateNormal];
//        [_playbutton addTarget:self action:@selector(presentDetail) forControlEvents:UIControlEventTouchUpInside];
        @weakify(self)
        _playbutton.RotateButtonBlock = ^{
            @strongify(self)
            [self presentDetail];
        };
        [self.view addSubview:_playbutton];
//        [_playbutton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.height.mas_equalTo(65);
//            make.right.bottom.offset(-20);
//        }];
    }
    return _playbutton;
}


- (void)presentDetail{
    AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
    UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
}


- (UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(15, NaviH-40, 60, 40);
        [_backButton setImage:ImageName(@"back_white") forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(back:)forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton *)shareButton{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(kScreenWidth-75, NaviH-40, 60, 40);
        [_shareButton setImage:ImageName(@"更多_白色") forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(Share)forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (UIView *)navigationBar{
    if (!_navigationBar) {
        _navigationBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
        _navigationBar.backgroundColor = [UIColor whiteColor];
        _navigationBar.alpha = 0;
        
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        [back setImage:ImageName(@"back_black") forState:UIControlStateNormal];
        [back addTarget:self action:@selector(back:)forControlEvents:UIControlEventTouchUpInside];
        [_navigationBar addSubview:back];
        [back mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.bottom.equalTo(_navigationBar);
            make.height.mas_offset(40);
            make.width.mas_offset(60);
        }];
        
//        UIButton *share = [UIButton buttonWithType:UIButtonTypeCustom];
//        [share setImage:ImageName(@"更多_黑色") forState:UIControlStateNormal];
//        [share addTarget:self action:@selector(Share)forControlEvents:UIControlEventTouchUpInside];
//        [_navigationBar addSubview:share];
//        [share mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_offset(kScreenWidth-75);
//            make.bottom.equalTo(_navigationBar);
//            make.height.mas_offset(40);
//            make.width.mas_offset(60);
//        }];
//
    }
    return _navigationBar;
}

- (void)Share{

//    ShareView *shareview = [[ShareView alloc] initWithshareModel:nil];
//    shareview.type = more_type;
//    [shareview show];
}

@end
