//
//  ParentVideoViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "ParentVideoViewController.h"
#import "VideoBaseViewController.h"
//#import "ParentVideoPlayerViewController.h"
//#import "ParentVideoDetailViewController.h"

#define pageMenuH 50
//#define scrollViewHeight (kScreenHeight-pageMenuH-NaviH-TabbarH)

@interface ParentVideoViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;
@property (nonatomic, strong) NSMutableArray *labelArray;

@property (nonatomic, assign) CGFloat scrollViewHeight;
@end

@implementation ParentVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getVideoLabelList) name:NSNotificationBirthdayChange object:nil];

    if (_is_task) {
        _scrollViewHeight = kScreenHeight-pageMenuH-NaviH;
        UIImage *leftImage = [[UIImage imageNamed:@"Combined Shape Back"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    } else {
        _scrollViewHeight = kScreenHeight-pageMenuH-NaviH-TabbarH;
    }
}

-(void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)publicVideo:(id)sender {

}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
    
    if (self.labelArray.count == 0) {
        [self getVideoLabelList];
    }
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationBirthdayChange object:self];

}

- (void)getVideoLabelList {
    if (self.labelArray.count > 0) {
        [self.labelArray removeAllObjects];
    }
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetVideoLabelList];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                LabelListModel *model = [LabelListModel modelWithJSON:dic];
                [self.labelArray addObject:model];
            }
            [self archiveArray:self.labelArray];
            [self setuppageMenu];
        }
    } failure:^(NSError *error) {
        
    }];
}

//归档所有lab
- (void)archiveArray:(NSMutableArray *)array{
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *entityArchive = [strLib stringByAppendingPathComponent:@"VideoTag"];
    NSMutableArray *archArray = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *newArray = [NSMutableArray array];
    for (LabelListModel *model in array) {
        __block LabelListModel *midModel = model;
        [archArray enumerateObjectsUsingBlock:^(LabelListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.label_id == model.label_id) {//数组中已经存在该对象
                midModel = obj;
                *stop = YES;
            }
        }];
        [newArray addObject:midModel];
    }
    [NSKeyedArchiver archiveRootObject:newArray toFile:entityArchive];//归档(序列化)
}


- (void)setuppageMenu{
  
    [self.view addSubview:self.pageMenu];

    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *entityArchive = [strLib stringByAppendingPathComponent:@"VideoTag"];
    self.labelArray = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    
    for (LabelListModel *model in self.labelArray) {
        VideoBaseViewController *baseVc = [[VideoBaseViewController alloc] init];
        baseVc.labModel = model;
        [self addChildViewController:baseVc];
        [self.myChildViewControllers addObject:baseVc];
    }
    
    [self.view addSubview:self.scrollView];
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = _scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.myChildViewControllers.count) {
        VideoBaseViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:baseVc.view];
        baseVc.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth, _scrollViewHeight);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.labelArray.count*kScreenWidth, 0);
    }
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
//    _toIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    if (self.myChildViewControllers.count <= toIndex) {return;}
    
    UIViewController *targetViewController = self.myChildViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([targetViewController isViewLoaded]) return;
    
    targetViewController.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth, _scrollViewHeight);
    [_scrollView addSubview:targetViewController.view];
    
}


- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, pageMenuH, kScreenWidth, _scrollViewHeight);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        NSMutableArray *array = [NSMutableArray array];
        for (LabelListModel *model in self.labelArray) {
            [array addObject:model.label_name];
        }
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, pageMenuH) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        [_pageMenu setItems:array selectedItemIndex:0];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(14);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}




#pragma mark - getter
- (NSMutableArray *)myChildViewControllers {
    
    if (!_myChildViewControllers) {
        _myChildViewControllers = [NSMutableArray array];
        
    }
    return _myChildViewControllers;
}


- (NSMutableArray *)labelArray{
    if (!_labelArray) {
        _labelArray = [NSMutableArray array];
    }
    return _labelArray;
}



// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}

@end
