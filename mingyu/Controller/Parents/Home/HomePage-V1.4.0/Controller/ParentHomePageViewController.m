//
//  ParentHomePageViewController.m
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ParentHomePageViewController.h"
#import "ParentCourseViewController.h"
#import "SearchViewController.h"
/**全新-V1.4.0 View*/
#import "ParentHomePageHeaderView.h"
#import "HomePageRecommendView.h"
#import "HomePageOtherView.h"
#import "HomePageViewModel.h"
#import "ExcellentClassModel.h"
#import "JXPagerView.h"
#import "JXCategoryView.h"
#import "HomePageNewHeaderView.h"

@interface ParentHomePageViewController ()<JXPagerViewDelegate, JXCategoryViewDelegate,HomePageViewModelDetegate>
@property (nonatomic, strong) JXPagerView *pagingView;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray *listViewArray;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) CGFloat childAge;
@property (nonatomic, strong) HomePageViewModel *homePageVM;
@property (nonatomic, strong) NSMutableArray *arrayTitle;
@property (nonatomic, strong) HomePageNewHeaderView *homePageHeaderView;
@end

@implementation ParentHomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    /** 初始化音频播放器 */
    [[DFPlayer shareInstance] df_initPlayerWithUserId:nil];
    [DFPlayer shareInstance].isNeedCache = NO;
    [DFPlayer shareInstance].category = DFPlayerAudioSessionCategoryPlayback;
    /**年龄变化 headerView改变*/
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(BirthdayChange) name:NSNotificationBirthdayChange object:nil];
    /**获取年龄相关数据*/
    [self.homePageVM getAppMeunAgeRelatedType:8];
    self.homePageVM.homePageViewModelDetegate = self;
    
}

-(void)setValue:(NSArray *)dataArray {
    NSMutableArray *arrayTitle = [NSMutableArray array];
    NSMutableArray *arrayViews = [NSMutableArray array];
    NSMutableArray *arrayIDArray = [NSMutableArray array];
    for (NSDictionary *dic in dataArray) {
        ExcellentClassModel *model = [ExcellentClassModel modelWithJSON:dic];
        [arrayTitle addObject:model.menu_name];
        [arrayIDArray addObject:model];
    }
    [arrayIDArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ExcellentClassModel *model = arrayIDArray[idx];
        if (idx == 0) {
            HomePageRecommendView *homePageRecommendView = [[HomePageRecommendView alloc] init];
            homePageRecommendView.menu_id = model.menu_id;
            [arrayViews addObject:homePageRecommendView];
        }else {
            HomePageOtherView *homePageOtherView = [[HomePageOtherView alloc] init];
            homePageOtherView.menu_id = model.menu_id;
            [arrayViews addObject:homePageOtherView];
        }
    }];
    _titles = arrayTitle.mutableCopy;
    _listViewArray = arrayViews.mutableCopy;
    
    [self ageJudgment];
}

/**年龄判断切换*/
-(void)ageJudgment {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    _childAge = (currentTime - data)/3600/24/30/12;
    if (_childAge > 2) {
        _homePageHeaderView = [[HomePageNewHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 135+130)];
    }else {
        _homePageHeaderView = [[HomePageNewHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 135+130+70+ 30)];
    }
    [self JXView];
}

- (void)BirthdayChange {
    [self.homePageHeaderView removeAllSubviews];
    [self.categoryView removeAllSubviews];
    [self.pagingView removeAllSubviews];
    self.categoryView = nil;
    self.pagingView = nil;
    /**获取年龄相关数据*/
    [self.homePageVM getAppMeunAgeRelatedType:8];
    self.homePageVM.homePageViewModelDetegate = self;
}

-(void)JXView {
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = [UIColor blackColor];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"575757"];
    self.categoryView.titleFont = [UIFont systemFontOfSize:16.0];
    self.categoryView.titleSelectedFont = [UIFont systemFontOfSize:16.0];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    [self.view addSubview:self.pagingView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorLineViewColor = [UIColor blackColor];
    lineView.indicatorLineWidth = 20;
    self.categoryView.indicators = @[lineView];
    
    _pagingView = [[JXPagerView alloc] initWithDelegate:self];
    //self.pagingView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight- NaviH - TabbarH);
    [self.view addSubview:self.pagingView];
    self.categoryView.contentScrollView = self.pagingView.listContainerView.collectionView;
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.pagingView.frame = self.view.bounds;
}

- (JXPagerView *)preferredPagingView {
    return [[JXPagerView alloc] initWithDelegate:self];
}

#pragma mark - JXPagerViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.homePageHeaderView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {

     return self.homePageHeaderView.height;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 40;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSArray<id<JXPagerViewListViewDelegate>> *)listViewsInPagerView:(JXPagerView *)pagerView {
    return self.listViewArray;
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark NavigationItem Action跳转方法 智能硬件Action暂时关闭
#pragma mark 搜索🔍
- (IBAction)searchButton:(id)sender {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchAll_type;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark 签到
- (IBAction)qiandaoButton:(id)sender {
    if ([userDefault boolForKey:KUDhasLogin]) {
        QianDaoViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"QianDaoViewController"];
        [self.navigationController pushViewController:VC animated:YES];
    } else {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
    }
}

#pragma mark 智能硬件
- (IBAction)ficilityButton:(id)sender {
    //    if ([userDefault boolForKey:KUDhasLogin]) {
    ParentCourseViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentCourseViewController"];
    [self.navigationController pushViewController:VC animated:YES];
    //    } else {
    //        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
    //        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
    //    }
}


#pragma mark viewAppear 相关方法
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.view.backgroundColor = [UIColor whiteColor];
    BOOL showLoginView = [userDefault boolForKey:@"showLoginView"];
    if (!showLoginView) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        VC.RightItemButton.hidden = NO;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:NO completion:nil];
        [userDefault setBool:YES forKey:@"showLoginView"];
        [userDefault synchronize];
    }
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    _childAge = (currentTime - data)/3600/24/365;
    //    获取一键听
    if (_childAge > 2) {
        // [self getUserSubjectHistory];
    }
    [SVProgressHUD dismiss];
}

- (void)viewWillDisappear:(BOOL)animated {
    //    如果不想让其他页面的导航栏变为透明 需要重置
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationBirthdayChange object:self];
}

-(HomePageViewModel *)homePageVM {
    if (!_homePageVM) {
        _homePageVM = [HomePageViewModel new];
    }
    return  _homePageVM;
}


//// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1) {
        return NO;
    }
    return YES;
}

@end
