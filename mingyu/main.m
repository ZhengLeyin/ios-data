//
//  main.m
//  mingyu
//
//  Created by apple on 2018/3/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DFPlayer.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, [[DFPlayer shareInstance] df_remoteControlClass], NSStringFromClass([AppDelegate class]));
    }
}
