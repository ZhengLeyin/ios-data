//
//  ChooseRecordTimeViewController.h
//  mingyu
//
//  Created by apple on 2018/8/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseRecordTimeViewController : UIViewController

@property (nonatomic, copy) NSString *photoTime;

@property (nonatomic, copy) void (^DownBlock)(NSString *timeStr);

@end
