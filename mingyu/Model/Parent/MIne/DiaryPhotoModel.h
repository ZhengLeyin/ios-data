//
//  DiaryPhotoModel.h
//  mingyu
//
//  Created by apple on 2018/8/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiaryPhotoModel : NSObject


/** 照片ID */
@property (nonatomic, assign) NSInteger photo_id;

/** 日记ID */
@property (nonatomic, assign) NSInteger fk_diary_id;

/** /图片名 */
@property (nonatomic, copy) NSString *image_name;

/** 年 */
@property (nonatomic, copy) NSString *year;

/** 月 */
@property (nonatomic, copy) NSString *month;

/** 日 */
@property (nonatomic, copy) NSString *day;

/** 创建时间 */
@property (nonatomic, copy) NSString *create_time;



@property (nonatomic, copy) NSString *first_img;

@property (nonatomic, assign) NSInteger count_img_number;

@end
