//
//  RightTableViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "RightTableViewCell.h"

@interface RightTableViewCell ()


@end

@implementation RightTableViewCell


+ (instancetype)theCircleCellWithTableView:(UITableView *)tableview{
    static NSString *identify = @"RightTableViewCell";
    RightTableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RightTableViewCell" owner:self options:nil] lastObject];
    }
    return cell;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_circle_img.bounds cornerRadius:3];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _circle_img.bounds;
    maksLayer.path = maksPath.CGPath;
    _circle_img.layer.mask = maksLayer;
    
    _add_button.layer.cornerRadius = 3;
    _add_button.layer.borderWidth = 0.5;
    
    _quit_button.hidden = YES;
}



- (void)setCirclemodel:(ParentCircleModel *)circlemodel {
    _circlemodel = circlemodel;
    _circle_img.backgroundColor = [UIColor lightGrayColor];
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,circlemodel.circle_img];
    [self.circle_img sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:nil];
    
    if (circlemodel.topicNum) {
        self.circle_name_Lab.numberOfLines = 2;
        self.circle_name_Lab.text = circlemodel.circle_abstract;
        NSString *number = [NSString stringWithFormat:@"%ld",circlemodel.topicNum-1];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld个话题",circlemodel.topicNum-1]];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"50D0F4"] range:NSMakeRange(0,number.length)];
        self.circle_abstract_Lab.attributedText = attrString;
        
    } else {
        self.circle_name_Lab.numberOfLines = 1;
        self.circle_name_Lab.text = circlemodel.circle_name;
        self.circle_abstract_Lab.text = circlemodel.circle_abstract;
    }
    
    [self changeAddButton];
}



- (void)changeAddButton{
    if (_circlemodel.is_join_true) {
        _add_button.layer.borderColor = [UIColor colorWithHexString:@"A8A8A8"].CGColor;
        [_add_button setTitle:@"已加入" forState:UIControlStateNormal];
        [_add_button setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
    } else {
        _add_button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
        [_add_button setTitle:@"加入" forState:UIControlStateNormal];
        [_add_button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
    }
}


- (void)showSearch{
    
    NSString *title = _circlemodel.circle_name;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font style=\"margin-right:20px\" color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _circle_name_Lab.attributedText = attrStr;
    _circle_name_Lab.lineBreakMode = NSLineBreakByTruncatingTail;

    NSString *abstract = _circlemodel.circle_abstract; 
    abstract = [@"<font style=\"font-size:11px\" color=\"#2C2C2C\">" stringByAppendingString:abstract];
    abstract = [abstract stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font style=\"margin-right:20px\" color=\"#50D0F4\">"];
    abstract = [abstract stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    abstract = [abstract stringByAppendingString:@"</font>"];
    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[abstract dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    _circle_abstract_Lab.attributedText = attrStr2;
    _circle_abstract_Lab.lineBreakMode = NSLineBreakByTruncatingTail;

}

- (IBAction)addButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addCircle:)]) {
        [self.delegate addCircle:_circlemodel];
    } else {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        if (_circlemodel.is_join_true) {
            NSString *str = [_circlemodel.circle_name stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
            str = [str stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"确认退出%@吗？",str]
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * action) {
                                                                     //响应事件
                                                                     
                                                                 }];
            [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
            
            UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      //响应事件
                                                                      
                                                                      NSString *URL =[NSString stringWithFormat:@"%@%@",KURL,KDelUserCircle];
                                                                      NSDictionary *parameters = @{
                                                                                                   @"user_id":USER_ID,
                                                                                                   @"circle_id":@(_circlemodel.circle_id)
                                                                                                   };
                                                                      [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                                                                          NSDictionary *json = responseObject;
                                                                          ZPLog(@"%@\n%@",json,json[@"message"]);
                                                                          BOOL success = [json[@"success"] boolValue];
                                                                          if (success) {
                                                                              [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationAddCircle object:nil userInfo:nil];
                                                                              _circlemodel.is_join_true = NO;
                                                                              if (self.addButonBlock) {
                                                                                  self.addButonBlock(_circlemodel);
                                                                              }
                                                                              [self changeAddButton];
                                                                              [UIView ShowInfo:TipDelCircleSuccess Inview:[self viewController].view];
                                                                          } else {
                                                                              [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                                                                          }
                                                                      } failure:^(NSError *error) {
//                                                                          [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                                                                      }];
                                                                  }];
            [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
            
            [alert addAction:cancelAction];
            [alert addAction:confirmAction];
            
            [[self viewController] presentViewController:alert animated:YES completion:nil];
            
        } else {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddUserCircle];
            NSDictionary *parameters = @{
                                         @"user_id" : USER_ID,
                                         @"circle_id" : @(_circlemodel.circle_id)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationAddCircle object:nil userInfo:nil];
                    _circlemodel.is_join_true = YES;
                    if (self.addButonBlock) {
                        self.addButonBlock(_circlemodel);

                    }
                    [self changeAddButton];
                    [UIView ShowInfo:TipAddCircleSuccess Inview:[self viewController].view];

                } else {
                    [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                }
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
            }];
        }
    }
}

- (IBAction)quitButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(quitCircle:)]) {
        [self.delegate quitCircle:_circlemodel];
    }
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
