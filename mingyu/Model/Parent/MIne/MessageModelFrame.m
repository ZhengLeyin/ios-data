//
//  MessageModelFrame.m
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MessageModelFrame.h"
#import "QQMessageModel.h"
#define distance 15 //边距
#define imageH 50 //头像的宽高

@implementation MessageModelFrame

-(instancetype)initWithFrameModel:(QQMessageModel *)messageModel timeIsEqual:(BOOL)timeBool{
    if (self=[super init]) {
        self.messageModel=messageModel;
//        CGFloat iphoneW=[UIScreen mainScreen].bounds.size.width;//获取屏幕的宽度
        
        CGSize timeSize=[messageModel.timeStatus sizeWithAttributes:@{NSFontAttributeName:FontSize(11)}]; //通过时间NSString计算出宽度
        if(messageModel.timeStatus){//前后时间不相等
            self.timeFrame=CGRectMake((kScreenWidth-timeSize.width-10)/2, 0, timeSize.width+10, timeSize.height+5);//显示时间的Frame
        }
        else{
            self.timeFrame=CGRectMake(0, 0, kScreenWidth, 0);
        }
        
        CGRect btnRect=[messageModel.message_content boundingRectWithSize:CGSizeMake(kScreenWidth-180, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:FontSize(15)} context:nil];//发送内容的Frame
        
        if(messageModel.from_user_id == [USER_ID integerValue]){ //如果是男的发送的话,头像就在右边,文字也在左边,否则头像就在左边,文字也在右边
            self.headImageFrame=CGRectMake(kScreenWidth-distance-imageH, CGRectGetMaxY(self.timeFrame)+distance, imageH, imageH);//头像的Frame
            self.btnFrame=CGRectMake(kScreenWidth-distance-imageH-btnRect.size.width-15-20, CGRectGetMaxY(self.timeFrame)+distance, btnRect.size.width+15+20, btnRect.size.height+15*2);//按钮内容的Frame(因为在CustomTableViewCell 里面设置了btnFrame里面文字的titleEdgeInsets边距都为15,所以,宽和高都要+15*2,X-15*2)
        }
        else{
            self.headImageFrame=CGRectMake(distance, CGRectGetMaxY(self.timeFrame)+distance,imageH, imageH);
            self.btnFrame=CGRectMake(distance+imageH, CGRectGetMaxY(self.timeFrame)+distance, btnRect.size.width+15+20, btnRect.size.height+15*2);
        }
        
        CGFloat  cellH=MAX(CGRectGetMaxY(self.btnFrame), CGRectGetMaxY(self.headImageFrame));//比较输入的内容和头像的Y值哪个大
        self.cellHeight=cellH+distance;//返回Cell的高度
    }
    return self;
}
+(instancetype)modelFrame:(QQMessageModel *)messageModel timeIsEqual:(BOOL)timeBool{
    //    return [[modelFrame alloc]initWithFrameModel:modelData];
    return [[self alloc]initWithFrameModel:messageModel timeIsEqual:timeBool];//和上面是一样的
}

@end
