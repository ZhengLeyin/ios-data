//
//  HandPickViewController.m
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "HandPickViewController.h"
#import "RotateButton.h"

#define leftTableWidth [UIScreen mainScreen].bounds.size.width * 0.3
#define rightTableWidth [UIScreen mainScreen].bounds.size.width * 0.7

@interface HandPickViewController ()<UITableViewDelegate,UITableViewDataSource,HandPickCellDelegate,DFPlayerDelegate,DFPlayerDataSource>
//DFPlayerDelegate,DFPlayerDataSource>

@property (nonatomic, strong) UITableView *lefttableView;
@property (nonatomic, strong) UITableView *righttableView;

@property (nonatomic, strong) NSMutableArray *leftArrayData;
@property (nonatomic, strong) NSMutableArray *rightArrayData;


@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *df_ModelArray;

@property (nonatomic, strong) RotateButton *playbutton;

@end

@implementation HandPickViewController
{
    NSInteger _selectIndex;
    BOOL _isScrollDown;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [MobClick event:@"HandPickViewController"];  //友盟统计

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupNavigationItem];
    
    _selectIndex = 0;
    _isScrollDown = YES;
    
    [self.view addSubview:self.lefttableView];
    [self.view addSubview:self.righttableView];
//    self.righttableView.tableFooterView = [UIView new];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightTableWidth, 80)];
    view.backgroundColor = [UIColor whiteColor];
    self.righttableView.tableFooterView = view;
//    self.righttableView.tableFooterView.height = 80;

}




- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    [_righttableView reloadData];
    [self initDFPlayer];

    [self getSubjectList];

    ZPLog(@"%ld",(long)[DFPlayer shareInstance].state);
    if (![DFPlayer shareInstance].currentAudioModel) {
        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
        if ([DFPlayer shareInstance].previousAudioModel.audio_image_url) {
            [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].previousAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
            self.playbutton.hidden = NO;
        } else {
            self.playbutton.hidden = YES;
        }
    } else{
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
        self.playbutton.hidden = NO;
    }
    
    if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [self.playbutton startRotating];
        [self.playbutton resumeRotate];
    } else {
        [self.playbutton stopRotating];
    }
}


- (void)getSubjectList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetSubjectList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.leftArrayData removeAllObjects];
            [self.rightArrayData removeAllObjects];
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                SubjectclassifyModel *model = [SubjectclassifyModel modelWithJSON:dic];
                [self.leftArrayData addObject:model];
                
                NSMutableArray *datas = [NSMutableArray array];
                for (NSDictionary *dic in model.subjectList)
                {
                    SubjectModel *circlemodel = [SubjectModel modelWithJSON:dic];
                    [datas addObject:circlemodel];
                }
                [self.rightArrayData addObject:datas];
            }
            [self.lefttableView reloadData];
            [self.righttableView reloadData];
            [self.lefttableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                                animated:YES
                                          scrollPosition:UITableViewScrollPositionNone];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma mark - 初始化DFPlayer
- (void)initDFPlayer{
    [DFPlayer shareInstance].dataSource  = self;
    [DFPlayer shareInstance].delegate    = self;
    
}

#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.dataArray.count; i++) {
        AudioModel *yourModel    = self.dataArray[i];
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        if ([[yourModel.audio_path absoluteString] hasPrefix:@"http"]) {//网络音频
            model.audio_path  = yourModel.audio_path;
        }
//        else{//本地音频
//            NSString *path = [[NSBundle mainBundle] pathForResource:[yourModel.audio_path absoluteString] ofType:@""];
//            if (path) {model.audio_path = [NSURL fileURLWithPath:path];}
//        }
        [_df_ModelArray addObject:model];
    }
    return self.df_ModelArray;
}

- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    self.playbutton.hidden = NO;
    
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying && [DFPlayer shareInstance].currentAudioModel.timeWithValue < 0.95) {
        [[DFPlayer shareInstance] df_seekToTimeWithValue:[DFPlayer shareInstance].currentAudioModel.timeWithValue];
    }
}


#pragma mark HandPickCellDelegate
- (void)playSubjict:(SubjectModel *)subjmodel{
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying && [DFPlayer shareInstance].currentAudioModel.subject_id == subjmodel.subject_id) {
        [[DFPlayer shareInstance] df_audioPause];
        [self.playbutton stopRotating];
    } else if ([DFPlayer shareInstance].currentAudioModel.subject_id == subjmodel.subject_id){
        [[DFPlayer shareInstance] df_audioPlay];
        [self.playbutton startRotating];
        [self.playbutton resumeRotate];
    } else {
        NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
        parameters[@"user_id"] = [NSString stringWithFormat:@"%@",USER_ID];
        parameters[@"start"] = @"0";
        parameters[@"size"] = @"100";
        parameters[@"subject_id"] = @(subjmodel.subject_id);
        NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetSubjectAudioList];
        
        URL = [NSString connectUrl:parameters url:URL];
        [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
            [self.dataArray removeAllObjects];
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSDictionary *data = json[@"data"];
                
                NSArray *audioList = data[@"audioList"];
                for (NSDictionary *dic in audioList) {
                    AudioModel *audiomodel = [AudioModel modelWithJSON:dic];
                    audiomodel.fk_parent_title = data[@"title"];
                    NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,dic[@"audio_image"]]];
                    audiomodel.audio_image_url = url;
                    audiomodel.subject_id = [data[@"subject_id"] integerValue];
                    
                    [self.dataArray addObject:audiomodel];
                }
                [self archiveObject:self.dataArray subj:subjmodel];
                if (self.dataArray.count > 0) {
                    [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                    [self.playbutton startRotating];
                    [self.playbutton resumeRotate];
                    
                    [self getSubjectList];
                }
            } else {
                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
    [self performSelector:@selector(reloadRightTableview) withObject:nil afterDelay:0.5];
    ;
}

- (void)reloadRightTableview{
    [self.righttableView reloadData];
}

- (void)archiveObject:(NSMutableArray *)dataArray subj:(SubjectModel *)subjmodel{
    //创建一个归档文件夹
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *string = [NSString stringWithFormat:@"archive_4_%ld",subjmodel.subject_id];
    
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *savearray = [NSMutableArray array];
    for (int i = 0; i < dataArray.count; i++) {
        AudioModel *model = dataArray[i];
        for (int j = 0; j < array.count; j++) {
            AudioModel *model2 = array[j];
            if (model.audio_id == model2.audio_id) {
                model.timeWithValue = model2.timeWithValue;
            }
        }
        [savearray addObject:model];
        self.dataArray = savearray;
    }
    if ( [NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive]) {
        ZPLog(@"success");
    }
    //    [NSKeyedArchiver archiveRootObject:dataArray toFile:entityArchive];//归档(序列化)
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）
}



#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == _lefttableView) {
        return 1;
    } else{
        return self.leftArrayData.count;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _lefttableView) {
         return self.leftArrayData.count;
    } else {
        return [self.rightArrayData[section] count];
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == _righttableView) {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor whiteColor];
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 50, 10)];
        lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
        lab.font = FontSize(10);
        SubjectclassifyModel *model = [_leftArrayData objectAtIndex: section];
        lab.text = [NSString stringWithFormat:@"%@（%ld）",model.classify_name, [self.rightArrayData[section] count]];
        [view addSubview:lab];
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == _righttableView) {
        return 30;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _lefttableView) {
        HandPickCell *cell = [HandPickCell theLeftCellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        SubjectclassifyModel *model = [_leftArrayData objectAtIndex: indexPath.row];
        cell.leftCellTitle.text = model.classify_name;
        return cell;

    } else {
        HandPickCell *cell = [HandPickCell theRightCellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        SubjectModel *model = self.rightArrayData[indexPath.section][indexPath.row];
        cell.subjectmodel = model;
        cell.delegate = self;
        return cell;
    }
}

// TableView分区标题即将展示
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(nonnull UIView *)view forSection:(NSInteger)section
{
    // 当前的tableView是RightTableView，RightTableView滚动的方向向上，RightTableView是用户拖拽而产生滚动的（（主要判断RightTableView用户拖拽而滚动的，还是点击LeftTableView而滚动的）
    if ((_righttableView == tableView)
        && !_isScrollDown
        && (_righttableView.dragging || _righttableView.decelerating))
    {
        [self selectRowAtIndexPath:section];
    }
}

// TableView分区标题展示结束
- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // 当前的tableView是RightTableView，RightTableView滚动的方向向下，RightTableView是用户拖拽而产生滚动的（（主要判断RightTableView用户拖拽而滚动的，还是点击LeftTableView而滚动的）
    if ((_righttableView == tableView)
        && _isScrollDown
        && (_righttableView.dragging || _righttableView.decelerating))
    {
        [self selectRowAtIndexPath:section + 1];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (_lefttableView == tableView) {
        
        _selectIndex = indexPath.row;
        NSArray *array = self.rightArrayData[_selectIndex];
        if (array.count > 0 ) {
            [_righttableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_selectIndex] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            [_lefttableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_selectIndex inSection:0]
                                  atScrollPosition:UITableViewScrollPositionMiddle
                                          animated:YES];
        }
    } else {
        
        NSArray *array = [self.rightArrayData objectAtIndex:indexPath.section];
        SubjectModel *subjectmodel =  [array objectAtIndex:indexPath.row];
        AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
        VC.target_id = subjectmodel.subject_id;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


// 当拖动右边TableView的时候，处理左边TableView
- (void)selectRowAtIndexPath:(NSInteger)index
{
    if (index < self.leftArrayData.count) {
        [_lefttableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionMiddle];
    }
}

#pragma mark - UISrcollViewDelegate
// 标记一下RightTableView的滚动方向，是向上还是向下
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static CGFloat lastOffsetY = 0;
    
    UITableView *tableView = (UITableView *) scrollView;
    if (_righttableView == tableView)
    {
        _isScrollDown = lastOffsetY < scrollView.contentOffset.y;
        lastOffsetY = scrollView.contentOffset.y;
    }
}


- (UITableView *)lefttableView{
    if (_lefttableView == nil) {
        _lefttableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, leftTableWidth, kScreenHeight-NaviH-effectViewH) style:UITableViewStylePlain];
        _lefttableView.delegate = self;
        _lefttableView.dataSource = self;
        _lefttableView.rowHeight = 94;
        _lefttableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _lefttableView.showsVerticalScrollIndicator = NO;
    }
    return _lefttableView;
}

- (UITableView *)righttableView{
    if (_righttableView == nil) {
        _righttableView = [[UITableView alloc] initWithFrame:CGRectMake(leftTableWidth, 0, rightTableWidth, kScreenHeight-NaviH-effectViewH)];
        _righttableView.delegate = self;
        _righttableView.dataSource = self;
        _righttableView.rowHeight = 90;
        _righttableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _righttableView.backgroundColor = [UIColor whiteColor];
    }
    return _righttableView;
}

- (RotateButton *)playbutton{
    if (_playbutton == nil) {
        _playbutton = [RotateButton buttonWithType:UIButtonTypeCustom];
        [_playbutton setImage:ImageName(@"一键听_banner") forState:UIControlStateNormal];
//        [_playbutton addTarget:self action:@selector(presentDetail) forControlEvents:UIControlEventTouchUpInside];
        @weakify(self)
        _playbutton.RotateButtonBlock = ^{
            @strongify(self)
            [self presentDetail];
        };
        [self.view addSubview:_playbutton];
        [_playbutton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(65);
            make.right.bottom.offset(-20);
        }];
        
    }
    return _playbutton;
}


- (void)presentDetail{
    AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
    UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
    
}
- (NSMutableArray *)leftArrayData{
    if (_leftArrayData == nil) {
        _leftArrayData = [NSMutableArray array];
    }
    return _leftArrayData;
}


- (NSMutableArray *)rightArrayData{
    if (_rightArrayData == nil) {
        _rightArrayData = [NSMutableArray array];
    }
    return _rightArrayData;
}

- (NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (void)setupNavigationItem{
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame =CGRectMake(0,0, 60, 44);
    [but setImage:ImageName(@"back_black") forState:UIControlStateNormal];
    [but addTarget:self action:@selector(Back)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut = [[UIBarButtonItem alloc]initWithCustomView:but];
    self.navigationItem.leftBarButtonItem = barBut;
    
    self.title = @"一键听";
}

- (void)Back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
