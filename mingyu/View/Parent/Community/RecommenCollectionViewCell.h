//
//  RecommenCollectionViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserModel;

typedef void(^attentionBlock)(UserModel *usermodel);

@interface RecommenCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIButton *userHeadButton;

@property (weak, nonatomic) IBOutlet UILabel *user_nameLab;

@property (weak, nonatomic) IBOutlet UILabel *fansCountLab;

@property (weak, nonatomic) IBOutlet UIButton *attentionButton;

@property (nonatomic, copy) attentionBlock attentionblock;

@property (nonatomic, strong) UserModel *usermodel;


@end
