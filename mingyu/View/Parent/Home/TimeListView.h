//
//  TimeListView.h
//  mingyu
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeListView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIButton *clooseButton;

@property (nonatomic, strong) NSArray *textArrayData;
@property (nonatomic, strong) NSArray *timeArrayData;


@property (nonatomic, copy) void (^clooseBlock)(void);

@end
