//
//  SomeCircleFooterView.m
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "SomeCircleFooterView.h"

@implementation SomeCircleFooterView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setupUI];
        _upTitle = @"收起全部";
        _downTitle = @"查看全部";
    }
    return self;
}


- (void)setupUI{
    
    self.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    [self addSubview:self.contetview];
    [self addSubview:self.MoreButton];
    
    [self.contetview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    [self.MoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.contetview);
    }];
}



- (void)MoreTopic:(UIButton *)button{
    if (button.selected) {
        button.selected = NO;
        UIImage *image = [UIImage imageNamed:@"查看全部"];
        [_MoreButton setImage:image forState:UIControlStateNormal];
        [_MoreButton setTitle:_downTitle forState:UIControlStateNormal];
        if (self.MoreTopic) {
            self.MoreTopic(NO);
        }
    } else {
        button.selected = YES;
        UIImage *image = [UIImage imageNamed:@"收起全部"];
        [_MoreButton setImage:image forState:UIControlStateNormal];
        [_MoreButton setTitle:_upTitle forState:UIControlStateNormal];
        if (self.MoreTopic) {
            self.MoreTopic(YES);
        }
    }
}


- (void)setUpTitle:(NSString *)upTitle{
    _upTitle = upTitle;
    [_MoreButton setTitle:upTitle forState:UIControlStateNormal];
    [_MoreButton setTitleColor:[UIColor colorWithHexString:@"60D5F5"] forState:UIControlStateNormal];
    [_MoreButton setImageEdgeInsets:UIEdgeInsetsMake(0, 80, 0, -80)];
    [_MoreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 20)];
}


- (void)setDownTitle:(NSString *)downTitle{
    _downTitle = downTitle;
    [_MoreButton setTitle:downTitle forState:UIControlStateNormal];
    [_MoreButton setTitleColor:[UIColor colorWithHexString:@"60D5F5"] forState:UIControlStateNormal];
    [_MoreButton setImageEdgeInsets:UIEdgeInsetsMake(0, 80, 0, -80)];
    [_MoreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 20)];
}






- (UIView *)contetview{
    if (_contetview == nil) {
        _contetview = [[UIView alloc] init];
        _contetview.backgroundColor = [UIColor whiteColor];
    }
    return _contetview;
}


- (UIButton *)MoreButton{
    if (_MoreButton == nil) {
        _MoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_MoreButton.titleLabel setFont:FontSize(12)];
        [_MoreButton addTarget:self action:@selector(MoreTopic:) forControlEvents:UIControlEventTouchUpInside];
        UIImage *image = [UIImage imageNamed:@"查看全部"];
        [_MoreButton setImage:image forState:UIControlStateNormal];
        [_MoreButton setTitle:@"查看全部" forState:UIControlStateNormal];
        [_MoreButton setTitleColor:[UIColor colorWithHexString:@"60D5F5"] forState:UIControlStateNormal];
        [_MoreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -70, 0, 0)];
        [_MoreButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -70)];
    }
    return _MoreButton;
}



@end
