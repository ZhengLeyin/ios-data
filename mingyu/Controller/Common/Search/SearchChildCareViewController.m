//
//  SearchChildCareViewController.m
//  mingyu
//
//  Created by apple on 2018/6/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "SearchChildCareViewController.h"
#import "SearchModel.h"
#import "ChildCareDetailViewController.h"

@interface SearchChildCareViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayData;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, strong) NothingView *nothingView;

@end

@implementation SearchChildCareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self refreshTableview];

}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        if (self.arrayData.count>0) {
            [self.arrayData removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getChildcareList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getChildcareList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
}

- (void)getChildcareList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KSearch];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"key":_searchStr,
                                 @"indices":@"childcare_index",
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 20;
            NSArray *data = json[@"data"];
            if (data.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                SearchModel *model = [SearchModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            if (self.arrayData && self.arrayData .count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        if (self.arrayData && self.arrayData .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.arrayData && self.arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerview = [UIView new];
    headerview.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    return headerview;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerview = [UIView new];
    footerview.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    return footerview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_arrayData && _arrayData.count > indexPath.section) {
        SearchModel *model = self.arrayData[indexPath.section];
        if ([model.result_type  isEqual: @"childcare"]) {
            NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
            cell.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
            cell.newsmodel = newsmodel;
            [cell showSearch];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData && _arrayData.count > indexPath.section) {
        ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
        SearchModel *model = self.arrayData[indexPath.section];
        if ([model.result_type  isEqual: @"childcare"]) {
            NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
            VC.newsmodel = newsmodel;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}


- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}


- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无搜索内容";
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

@end
