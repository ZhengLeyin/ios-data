//
//  TeacherModel.h
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeacherModel : NSObject


/** 主键 */
@property (nonatomic, assign) NSInteger teacher_id;

/** 外键 */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 所属组织 */
@property (nonatomic, copy) NSString *teacher_organize;

/** 职位 */
@property (nonatomic, copy) NSString *teacher_position;

/** 简介 */
@property (nonatomic, copy) NSString *teacher_profile;

/** 作者名字 */
@property (nonatomic, copy) NSString *teacher_name;

/** 头像 */
@property (nonatomic, copy) NSString *user_head;

/** 粉丝数 */
@property (nonatomic, assign) NSInteger followUserNumber;

/** 老师总播放数 */
@property (nonatomic, assign) NSInteger playCount;

/** 短视频判断 */
@property (nonatomic, assign) NSInteger is_post_video;


@property (nonatomic, assign) NSInteger follow_status;


@end
