//
//  screenView.m
//  mingyu
//
//  Created by apple on 2018/5/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "screenView.h"

@implementation screenView{
    UIView *_darkView;
    UIView *_filetview;
    
    UIButton *_cancleButton;
    UIButton *_finishButton;
    
    UIButton *_section1selectButton;
    UIButton *_section2selectButton;
    UIButton *_section3selectButton;
    
    NSInteger section0;
    NSInteger section1;
    NSInteger section2;
    
    NSArray *_section1array;
    NSArray *_section2array;
    NSArray *_section3array;

}



- (instancetype)initWithfiletviewFrame:(CGRect)frame{
    if (self = [super init]) {
//        _shareDic = shareDic;
        [self updatUI:frame];
    }
    return self;
}


- (void)updatUI:(CGRect)frame{
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [app.window addSubview:self];
    
    UIView *darkView = [[UIView alloc]init];
    darkView.backgroundColor = [UIColor colorWithHexString:@"202020" alpha:0.7];
    darkView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [self addSubview:darkView];
    _darkView = darkView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_darkView addGestureRecognizer:tap];
    
    _filetview = [[UIView alloc]init];
    _filetview.backgroundColor = [UIColor whiteColor];
    [self addSubview:_filetview];
    [_filetview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(frame.origin.y+40);
        make.height.mas_offset(210);
    }];
    
    
    _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancleButton setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
    [_cancleButton.titleLabel setFont:FontSize(15)];
    [_cancleButton addTarget:self action:@selector(cancelbutton) forControlEvents:UIControlEventTouchUpInside];
    _cancleButton.layer.borderWidth = 0.5;
    _cancleButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
    [_filetview addSubview:_cancleButton];
    
    _finishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [_finishButton setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
    [_finishButton.titleLabel setFont:FontSize(15)];
    [_finishButton addTarget:self action:@selector(finishbutton) forControlEvents:UIControlEventTouchUpInside];
    _finishButton.layer.borderWidth = 0.5;
    _finishButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
    [_filetview addSubview:_finishButton];
    
    [_cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.mas_equalTo(_filetview);
        make.height.mas_offset(43);
        make.width.mas_offset(kScreenWidth/2);
    }];
    [_finishButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(_filetview);
        make.height.mas_offset(43);
        make.width.mas_offset(kScreenWidth/2);
    }];
    
    
    CGFloat Width_Space = 15.0f;
    CGFloat Height_Space = 15.0f;
    CGFloat labX = 0;
    CGFloat labY = 34;
    CGFloat itemH = 30;
    CGFloat itemW = (kScreenWidth-4*Width_Space)/5;
    NSArray *array =@[@"年级",@"付费",@"形式"];
    for (int i = 0 ; i < 3; i++) {
        UILabel *lab = [[UILabel alloc] init];
        lab.frame = CGRectMake(labX, labY + i*(Height_Space+itemH), itemW, itemH);
        lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
        lab.font = FontSize(12);
        lab.text = array[i];
        lab.textAlignment = NSTextAlignmentCenter;
        [_filetview addSubview:lab];
    }
    
    _section1array = @[@"0-3岁",@"3-6岁",@"8-10岁",@"小升初"];
    _section2array = @[@"全部",@"免费",@"付费"];
    _section3array = @[@"全部",@"音频",@"视频"];

    for (int row = 0; row < _section1array.count; row ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(itemW + row*(itemW+Width_Space), labY, itemW, itemH);
        [button setTitle:_section1array[row] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
        [button.titleLabel setFont:FontSize(12)];
        button.tag = 1000+row;
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = YES;
        [button addTarget:self action:@selector(section1chooseitem:) forControlEvents:UIControlEventTouchUpInside];
        [_filetview addSubview:button];
    }
    
    for (int row = 0; row < _section2array.count; row ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(itemW + row*(itemW+Width_Space), labY + Height_Space+itemH, itemW, itemH);
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitle:_section2array[row] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
        [button.titleLabel setFont:FontSize(12)];
        button.tag = 2000+row;
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = YES;
        [button addTarget:self action:@selector(section2chooseitem:) forControlEvents:UIControlEventTouchUpInside];
        [_filetview addSubview:button];
    }
    
    for (int row = 0; row < _section3array.count; row ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(itemW + row*(itemW+Width_Space), labY + 2*(Height_Space+itemH), itemW, itemH);
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitle:_section3array[row] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
        [button.titleLabel setFont:FontSize(12)];
        button.tag = 3000+row;
        button.layer.cornerRadius = 3;
        button.layer.masksToBounds = YES;
        [button addTarget:self action:@selector(section3chooseitem:) forControlEvents:UIControlEventTouchUpInside];
        [_filetview addSubview:button];
    }
    
}

- (void)section1chooseitem:(UIButton*)button{
    for (int i = 0; i < _section1array.count; i++) {
        if (button.tag == i + 1000) {
            [button setBackgroundColor:[UIColor colorWithHexString:@"575757"]];
            [button setTitleColor: [UIColor colorWithHexString:@"F1F1F1"] forState:UIControlStateNormal];
            continue;
        }
        UIButton *button = [(UIButton *)self viewWithTag:(1000+i)];
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
    }
    section0 = button.tag-1000+1;
}


- (void)section2chooseitem:(UIButton*)button{
    for (int i = 0; i < _section2array.count; i++) {
        if (button.tag == i + 2000) {
            [button setBackgroundColor:[UIColor colorWithHexString:@"575757"]];
            [button setTitleColor: [UIColor colorWithHexString:@"F1F1F1"] forState:UIControlStateNormal];
            continue;
        }
        UIButton *button = [(UIButton *)self viewWithTag:(2000+i)];
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
    }
    section1 = button.tag-2000;

}

- (void)section3chooseitem:(UIButton*)button{
    for (int i = 0; i < _section3array.count; i++) {
        if (button.tag == i + 3000) {
            [button setBackgroundColor:[UIColor colorWithHexString:@"575757"]];
            [button setTitleColor: [UIColor colorWithHexString:@"F1F1F1"] forState:UIControlStateNormal];
            continue;
        }
        UIButton *button = [(UIButton *)self viewWithTag:(3000+i)];
        button.backgroundColor = [UIColor colorWithHexString:@"F1F1F1"];
        [button setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
    }
    section2 = button.tag-3000;

}




- (void)cancelbutton{
    [self hidden];
}


- (void)finishbutton{
    [self hidden];
    if (self.finishBlock) {
        self.finishBlock(section0, section1, section2);
    }
    
}


- (void)show{
    
    _darkView.alpha = 1;
    CGRect frame = _filetview.frame;
    frame.origin.y -=frame.size.height;
    _filetview.frame = frame;
}


- (void)hidden{

    _darkView.alpha = 0;
    CGRect frame = _filetview.frame;
    frame.origin.y += frame.size.height;
    _filetview.frame = frame;
    [self removeFromSuperview];
    
    if (self.selfhidden) {
        self.selfhidden();
    }
}

- (void)tapClick{
    
    [self hidden];
    
}



@end
