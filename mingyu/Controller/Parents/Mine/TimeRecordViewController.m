//
//  TimeRecordViewController.m
//  mingyu
//
//  Created by apple on 2018/8/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TimeRecordViewController.h"
#import "TimeRecordCell.h"
#import "OneRecordViewController.h"
#import "AddRecordViewController.h"

@interface TimeRecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *navigationBar;

@property (nonatomic, strong) UIButton *addRecordButton;

@property (nonatomic, strong) UIButton *backButton;

@property (nonatomic, strong) UILabel *titleLab;

@property (nonatomic, strong) NSMutableArray *headerArray;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, strong) UIImageView *loadImage;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, assign) NSInteger start;

@end

@implementation TimeRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [MobClick event:@"TimeRecordViewController"];  //友盟统计

    self.title = @"时光印记";
    //注册通知：
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:NSNotificationAddTimeRecord object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:NSNotificationDeleteTimeRecord object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:NSNotificationDeletePhotoRecord object:nil];
    
    [self setUpView];
    TimeRecordCell *view = [TimeRecordCell theTimeRecordHeaderView];
    view.frame = CGRectMake(0, -180, kScreenWidth, 180);
    view.tag = 101;
     [self.tableView addSubview:view];
    [self.view addSubview:self.loadImage];
    self.tableView.scrollEnabled = NO;
    [self getDiaryList];
    
    [self getMoreTimeRecord];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //    self.navigationController.navigationBar.alpha = 0;
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.alpha = 1;
    //移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationAddTimeRecord object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationDeleteTimeRecord object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationDeletePhotoRecord object:self];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认

}

- (void)refresh {
    [self.arrayData removeAllObjects];
    [self.headerArray removeAllObjects];
    _start = 0;
    [self getDiaryList];
}

- (void)getMoreTimeRecord{
    //上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getDiaryList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
}


- (void)getDiaryList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetDiaryList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"5"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_footer endRefreshing];
        self.loadImage.hidden = YES;
        self.tableView.scrollEnabled = YES;
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            NSInteger totlecount = 0;
            if (data.count > 0) {
                for (NSDictionary *dic in data) {
                    DiaryModel *headermodel = [DiaryModel modelWithJSON:dic];
                    if (![self.headerArray containsObject:headermodel]) {
                        [self.headerArray addObject:headermodel];
                    }
                    NSMutableArray *array = [NSMutableArray array];
                    for (NSDictionary *dict in dic[@"diaryList"]) {
                        DiaryModel *diarymodel = [DiaryModel modelWithJSON:dict];
                        [array addObject:diarymodel];
                        totlecount ++;
                    }
                    [self.arrayData addObject:array];
                }
                _start += 5;
                if (data && totlecount == 5) {
                    _lastPage = NO;
                } else {
                    _lastPage = YES;
                }
                self.tableView.tableFooterView = [UIView new];
                [self.tableView reloadData];
                self.addRecordButton.hidden = NO;
            } else if (self.arrayData.count < 1 ){
                self.tableView.tableFooterView = [TimeRecordCell theTimeRecordFooterView];
                [self.tableView reloadData];
                self.addRecordButton.hidden = YES;
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)photoAlbumBtnClick{
    UIViewController *VC = [[UIViewController alloc] init];
    VC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:VC  animated:YES];
}




- (void)setUpView{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.backButton];
    [self.view addSubview:self.titleLab];
    [self.view addSubview:self.navigationBar];
    [self.view addSubview:self.addRecordButton];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
//    ZPLog(@"%f",offsetY);
    
//    if (offsetY > 0) {
        CGFloat alpha = (offsetY+160) / 64;
        self.navigationBar.alpha = alpha;
    if (offsetY > -160) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    }
    
    if (offsetY < -180) {
        CGRect rect = [self.tableView viewWithTag:101].frame;
        rect.origin.y = offsetY;
        rect.size.height = -offsetY;
        [self.tableView viewWithTag:101].frame = rect;
    }
}

#pragma tableView--delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.headerArray && self.headerArray.count > 0) {
        return self.headerArray.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > section) {
        return [self.arrayData[section] count];
    }
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TimeRecordCell *sectionView = [TimeRecordCell theTimeRecordCellSectionHeaderView];
    if (self.headerArray && self.headerArray.count > section) {
        DiaryModel *model = self.headerArray[section];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        // 获取当前日期
        NSDate* dt = [NSDate date];
        // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
        // 获取不同时间字段的信息
        NSDateComponents* comp = [gregorian components: unitFlags fromDate:dt];
        // 获取各时间字段的数值
        if (comp.year == [model.year integerValue]) {
            sectionView.timeLab.text = [NSString stringWithFormat:@"%@月%@日",model.month,model.day];
        } else {
            sectionView.timeLab.text = [NSString stringWithFormat:@"%@年%@月%@日",model.year,model.month,model.day];
        }
    }
    return sectionView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TimeRecordCell *cell = [TimeRecordCell theTimeRecordCellWithTableView:tableView];
   
    if (indexPath.row == 0) {
        cell.markImage.hidden = YES;
    } else {
        cell.markImage.hidden = NO;
    }
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        DiaryModel *model = self.arrayData[indexPath.section][indexPath.row];
        cell.diarymodel = model;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        DiaryModel *model = self.arrayData[indexPath.section][indexPath.row];
        if (model.height) {
            return model.height;
        }
        return [TimeRecordCell cellHeight];
    }
    return 0;
}


-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-65, 55) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    return size.height;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        OneRecordViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"OneRecordViewController"];
        DiaryModel *model = self.arrayData[indexPath.section][indexPath.row];
        VC.diary_id = model.diary_id;
        [self.navigationController pushViewController:VC animated:YES];
    }


}


- (void) addRecordButtonClicked{
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    AddRecordViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddRecordViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}


-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -20, kScreenWidth, kScreenHeight+20-effectViewH) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(180, 0, 0, 0);

    }
    return _tableView;
}


- (UIView *)navigationBar{
    if (!_navigationBar) {
        _navigationBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
        _navigationBar.backgroundColor = [UIColor whiteColor];
        _navigationBar.alpha = 0;
        
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        [back setImage:ImageName(@"back_black") forState:UIControlStateNormal];
        [back addTarget:self action:@selector(back)forControlEvents:UIControlEventTouchUpInside];
        [_navigationBar addSubview:back];
        [back mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.bottom.equalTo(_navigationBar);
            make.height.mas_offset(40);
            make.width.mas_offset(60);
        }];
        
        UILabel *lab = [[UILabel alloc] init];
        lab.textAlignment = NSTextAlignmentCenter;
        lab.text = @"时光印记";
        lab.textColor = [UIColor blackColor];
        lab.font = FontSize(16);
        [_navigationBar addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_navigationBar);
            make.bottom.equalTo(_navigationBar);
            make.height.mas_offset(40);
        }];
    }
    return _navigationBar;
}

- (UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(15, NaviH-40, 60, 40);
        [_backButton setImage:ImageName(@"back_white") forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(back)forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake((kScreenWidth-100)/2, NaviH-40, 100, 40)];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.text = @"时光印记";
        _titleLab.textColor = [UIColor whiteColor];
        _titleLab.font = FontSize(16);
    }
    return _titleLab;
}

- (UIButton *)addRecordButton{
    if (!_addRecordButton) {
        _addRecordButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _addRecordButton.frame = CGRectMake(kScreenWidth-74-30, kScreenHeight-74-50, 74, 74);
        [_addRecordButton setImage:ImageName(@"发布") forState:0];
        [_addRecordButton addTarget:self action:@selector(addRecordButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addRecordButton;
}


- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (NSMutableArray *)headerArray{
    if(!_headerArray){
        _headerArray = [NSMutableArray array];
    }
    return _headerArray;
}


- (UIImageView *)loadImage{
    if (!_loadImage) {
        _loadImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 180, kScreenWidth, kScreenHeight-180)];
        _loadImage.image = ImageName(@"时光印记过渡");
    }
    return _loadImage;
}


- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
