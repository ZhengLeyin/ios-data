//
//  QianDaoViewController.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "QianDaoViewController.h"

@interface QianDaoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) QianDaoRulerView *rulerView;

@property (nonatomic, strong) UIButton *maskView;

@property (nonatomic, strong) QianDaoActionModel *actionmodel;


@end

@implementation QianDaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.collectionView];
    
    [self getRegisterAction];
    
}



- (void)getRegisterAction{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRegisterAction];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            
            _actionmodel = [QianDaoActionModel modelWithJSON:data];
            
            [self.collectionView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageWithColor:[UIColor colorWithHexString:@"3FE3DF"]] init] forBarMetrics:UIBarMetricsDefault];
//去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    //    如果不想让其他页面的导航栏变为透明 需要重置
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:nil];
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 0;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    QiandaoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QiandaoCollectionViewCell" forIndexPath:indexPath];
//    if (self.CollectArrayData && self.CollectArrayData.count > indexPath.row) {
//        cell.usermodel = [self.CollectArrayData objectAtIndex:indexPath.row];
//        cell.attentionblock = ^(UserModel *usermodel) {
//            [self AddFollowWithUserModel:usermodel];
//        };
//    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    QianDaoCollectionReusableView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"QianDaoCollectionReusableView" forIndexPath:indexPath];
    headView.actionmodel = _actionmodel;
    return headView;
}


//禁止上啦
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = _collectionView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    _collectionView.contentOffset = offset;
}


- (UICollectionView *)collectionView{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.itemSize = CGSizeMake((kScreenWidth-30)/2, 300);
        layout.minimumLineSpacing = 10;
        layout.headerReferenceSize = CGSizeMake(kScreenWidth, 630);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerNib:[UINib nibWithNibName:@"QiandaoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"QiandaoCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"QianDaoCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"QianDaoCollectionReusableView"];
        
    }
    return _collectionView;
}


//- (QianDaoRulerView *)rulerView{
//    if (_rulerView == nil) {
//        _rulerView = [[NSBundle mainBundle] loadNibNamed:@"QianDaoRulerView" owner:nil options:nil][0];
//        _rulerView.center = self.navigationController.view.center;
//        __weak typeof(self) Weakself = self;
//        _rulerView.knowButtonBlock = ^{
//            Weakself.maskView.hidden = YES;
//            Weakself.rulerView.hidden = YES;
//        };
//        [self.navigationController.view addSubview:_rulerView];
//    }
//    return _rulerView;
//}
//
//- (UIButton *)maskView{
//    if (_maskView == nil) {
////        _maskView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
//        _maskView = [UIButton buttonWithType:UIButtonTypeCustom];
//        _maskView.frame = self.navigationController.view.bounds;
//        _maskView.backgroundColor = [UIColor colorWithHexString:@"202020"];
//        _maskView.alpha = 0.7;
//        [_maskView addTarget:self action:@selector(hidden) forControlEvents:UIControlEventTouchUpInside];
//        [self.navigationController.view addSubview:_maskView];
//    }
//    return _maskView;
//}

//- (void)hidden{
//    self.maskView.hidden = YES;
//    self.rulerView.hidden = YES;
//}

//- (IBAction)rightButton:(id)sender {
//    self.maskView.hidden = NO;
//    self.rulerView.hidden = NO;
//
//}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}




@end
