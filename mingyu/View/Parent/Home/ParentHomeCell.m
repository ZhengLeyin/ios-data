//
//  ParentHomeCell.m
//  mingyu
//
//  Created by apple on 2018/4/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentHomeCell.h"

@implementation ParentHomeCell


+ (instancetype)theFirstCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"FirstCell";
    ParentHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentHomeCell" owner:nil options:nil][0];
    }
    return cell;
}


+ (instancetype)theSecondCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"SecondCell";
    ParentHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentHomeCell" owner:nil options:nil][1];
    }
    return cell;
}

+ (instancetype)theThirdCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"ThirdCell";
    ParentHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentHomeCell" owner:nil options:nil][2];
    }
    return cell;
}


+ (instancetype)TheHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentHomeCell" owner:nil options:nil][3];

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_more_Btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -_more_Btn.imageView.size.width, 0, _more_Btn.imageView.size.width)];
    [_more_Btn setImageEdgeInsets:UIEdgeInsetsMake(0, _more_Btn.titleLabel.bounds.size.width, 0, -_more_Btn.titleLabel.bounds.size.width)];
    
}



- (void)setFirstCellWithModel:(NSArray *)array{
    
}

- (void)setSecondCellWithModel:(ChildcareMessageModel *)childcaremessagemodel{

    
}

- (void)setThirdCellWithModel:(VideoModel *)videomodel{
    
}



- (IBAction)MoreButton:(id)sender {
    if (self.MoreButtonBlock) {
        self.MoreButtonBlock();
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}










@end
