//
//  ParentVideoCell.m
//  mingyu
//
//  Created by apple on 2018/6/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoCell.h"

@implementation ParentVideoCell

+ (instancetype)theParentVideoCellWithTableView:(UITableView *)tableview{
    static NSString *cellid = @"ParentVideoCell";
    ParentVideoCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentVideoCell" owner:nil options:nil][0];
    }
    return cell;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
 
    
}


- (void)setVideomodel:(VideoModel *)videomodel{
    _videomodel = videomodel;
    if (_homePageVCOrOtherVCType == 0) {
        _contentNewLabel.width = 0;
        _contentNewLabel.hidden = YES;
        _title_labLeftConstraint.constant = 5 - 38 - 5;
    }else {
        _contentNewLabel.layer.cornerRadius = 8.5;
        _contentNewLabel.layer.masksToBounds = YES;
    }
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    [_BGImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _title_lab.text = _videomodel.video_title;
//    [_totle_time_Button setTitle:_videomodel.time_length forState:UIControlStateNormal];
//    [_play_count_Button setTitle:[NSString stringWithFormat:@"%ld",videomodel.play_count] forState:UIControlStateNormal];
//    _source_lab.text = _videomodel.source;
}

-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}


@end
