//
//  ParentArticleViewController.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomCommetView.h"
@class NewsModel;
//@class ParentArticleModel;

@interface ParentArticleViewController : UIViewController

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) NewsModel *newsmodel;


@end
