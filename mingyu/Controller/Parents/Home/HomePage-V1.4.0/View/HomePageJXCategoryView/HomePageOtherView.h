//
//  HomePageOtherView.h
//  mingyu
//
//  Created by MingYu on 2018/12/24.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomePageOtherView : UIView
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray <NSString *> *dataSource;
@property (nonatomic, assign) NSUInteger menu_id;
@end

NS_ASSUME_NONNULL_END
