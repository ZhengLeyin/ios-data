//
//  MineTopicDraftsViewController.m
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineTopicDraftsViewController.h"

@interface MineTopicDraftsViewController ()

@property (weak, nonatomic) IBOutlet UIButton *editorButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *chooseAllButton;
@property (weak, nonatomic) IBOutlet UILabel *chooseCountLab;

@property (nonatomic, strong) NSMutableArray *TopicDraftsArry;
@property (assign, nonatomic) NSInteger deleteNum;
@property (nonatomic,strong)NSMutableArray *selectorArray;//存放选中数据

@property (nonatomic, strong) NothingView *nothingView;

@end

@implementation MineTopicDraftsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _bottomView.hidden = YES;
    
    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"TopicDrafts.plist"];
    _TopicDraftsArry = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    
    if (_TopicDraftsArry && _TopicDraftsArry > 0) {
        self.nothingView.hidden = YES;
    } else{
        self.nothingView.hidden = NO;
    }
    [self.tableView reloadData];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_TopicDraftsArry && _TopicDraftsArry.count > 0) {
        return _TopicDraftsArry.count;
    }
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"TopicDraftsCell";
    TopicDraftsCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[TopicDraftsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    if (_TopicDraftsArry && _TopicDraftsArry.count > indexPath.row) {
        NSDictionary *dic = [_TopicDraftsArry objectAtIndex:indexPath.row];
        cell.draftsDic = dic;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.editing == YES) {
         NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                }
            }
        }
        [self.selectorArray addObject:[self.TopicDraftsArry objectAtIndex:indexPath.row]];
        self.deleteNum += 1;
        _chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
    } else {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        } else {
            TopicEditorViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"TopicEditorViewController"];
            VC.topicDraftsArray = _TopicDraftsArry;
            VC.topicDraftsDic = _TopicDraftsArry[indexPath.row];
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing == YES) {
      NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"weixuanz"];
                }
            }
        }
        [self.selectorArray removeObject:[self.TopicDraftsArry objectAtIndex:indexPath.row]];
        self.deleteNum -= 1;
        _chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_chooseAllButton setTitle:@"  全选" forState:0];
        [_chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    } else {
        
    }
}



//全选
- (IBAction)deleteAllButton:(UIButton *)sender {
    if ([_chooseAllButton.titleLabel.text  isEqual: @"  全选"]) {
        for (int i = 0; i < _TopicDraftsArry.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                    }
                }
            }
        }
        self.selectorArray = [NSMutableArray arrayWithArray:self.TopicDraftsArry];
        self.deleteNum  = self.TopicDraftsArry.count;
        _chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_chooseAllButton setTitle:@"  全选 " forState:0];
        [_chooseAllButton setImage:ImageName(@"xuanzhe") forState:0];
    } else {
        for (int i = 0; i< _TopicDraftsArry.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView deselectRowAtIndexPath:indexPath animated:NO];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
        [self.selectorArray removeAllObjects];
        self.deleteNum  = 0;
        _chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_chooseAllButton setTitle:@"  全选" forState:0];
        [_chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    }
}


//删除已选
- (IBAction)deleteSeletButton:(id)sender {
    if (self.tableView.editing && self.deleteNum > 0) {
        [self showAlertWithTitle:@"确认删除吗？" message:nil noBlock:nil yseBlock:^{
            //删除
            [self.TopicDraftsArry removeObjectsInArray:self.selectorArray];
            
            if (_TopicDraftsArry && _TopicDraftsArry > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [self.tableView reloadData];
            
            self.deleteNum = 0;
            _chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];

            NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"TopicDrafts.plist"];
            if ([self.TopicDraftsArry writeToFile:filePatch atomically:YES]){
                ZPLog(@"sucess");
            }
        }];
    }
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


//编辑
- (IBAction)editor:(id)sender {
    [_chooseAllButton setTitle:@"  全选" forState:0];
    [_chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    _selectorArray = [NSMutableArray array];
    self.deleteNum = 0;
    _chooseCountLab.text = @"已选0项";
    if (_bottomView.hidden == YES) {
        _tableView.editing = YES;
        _bottomView.hidden = NO;
        [_editorButton setTitle:@"取消" forState:0];
    } else{
        _tableView.editing = NO;
        _bottomView.hidden = YES;
        [_editorButton setTitle:@"编辑" forState:0];
    }
}


- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无草稿";
        _nothingView.imageView.image = ImageName(@"资讯 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}


@end



@implementation TopicDraftsCell


- (void)setDraftsDic:(NSDictionary *)draftsDic{
    _title_lab.text = draftsDic[@"topic_title"];
    _time_lab.text = [NSString compareTimeForNow:[draftsDic[@"time"] integerValue]*1000];
}

-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}

@end

