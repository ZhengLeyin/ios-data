//
//  BabyVaccineViewController.m
//  mingyu
//
//  Created by apple on 2018/6/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BabyVaccineViewController.h"
#import "BabyVaccineCell.h"
#import "VaccineInfoViewController.h"
#import "VaccineMessageViewController.h"


@interface BabyVaccineViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *headerArray;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UILabel *topLab;

@property (nonatomic, assign) NSInteger nextVaccine;

@end

@implementation BabyVaccineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [MobClick event:@"BabyVaccineViewController"];  //友盟统计

    self.title = @"疫苗时间表";
    
    [self.view addSubview:self.topLab];
    [self.view addSubview:self.tableView];
    
    [self.topLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_offset(50);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.topLab).offset(50);
        make.bottom.mas_offset(-effectViewH);
    }];
    
    [self getdatatableViewscroll];
}


//获取数据
- (void)getdatatableViewscroll{
    
    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyVaccine1.plist"];
    //读取文件目录下 plist文件的内容
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    if (!array || array.count == 0) {
//        若果目录下的plist文件不存在，则读取项目里的plist 文件 存入到文件目录下以便后续操作
        NSString *path = [[NSBundle mainBundle] pathForResource:@"BabyVaccine" ofType:@"plist"];
        array = [[NSMutableArray alloc]initWithContentsOfFile:path];
        NSMutableArray *saveArray = [NSMutableArray array];
        for (NSDictionary *dic in array) {
            [saveArray addObject:dic];
        }
        if ([saveArray writeToFile:filePatch atomically:YES]) {
            ZPLog(@"success");
        }
    }
    
    for (NSDictionary *dic in array) {
        NSNumber *days = dic[@"days"];
        if (![self.headerArray containsObject:days]) {
            [self.headerArray addObject:days];
        }
    }
    for (int i = 0; i < self.headerArray.count; i++) {
        NSMutableArray * array2 = [NSMutableArray new];
        for (NSDictionary *dic2 in array) {
            NSNumber *days = dic2[@"days"];
            if (self.headerArray[i] == days) {
                [array2 addObject:dic2];
            }
        }
        [self.dataArray addObject:array2];
    }
    
    [self.tableView reloadData];
    
    
    for (int i = 0; i < self.headerArray.count; i++) {
        NSInteger days = [self.headerArray[i] integerValue];
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval newdata = (data + days*60*60*24);
        NSTimeInterval toda = [[NSDate date] timeIntervalSince1970];

        if (newdata > toda) {
            NSDate *toDate = [NSDate date];
            NSTimeZone *zone = [NSTimeZone systemTimeZone];
            NSInteger interval = [zone secondsFromGMTForDate:toDate];
            toDate = [toDate  dateByAddingTimeInterval:interval];
            
            NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:data];

            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents* comp = [calendar components:NSCalendarUnitDay
                                                 fromDate:timeDate
                                                   toDate:toDate
                                                  options:NSCalendarWrapComponents];
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"距离下次疫苗还有%ld天",days-comp.day]];
            [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"50D0F4"] range:NSMakeRange(8,attrString.length-9)];
            _topLab.attributedText = attrString;
            _nextVaccine = i;
            [self.tableView scrollToRow:0 inSection:i atScrollPosition:UITableViewScrollPositionTop animated:YES];
            return;
        }
    }
}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.headerArray && self.headerArray.count > 0) {
        return _headerArray.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataArray[section] && [self.dataArray[section] count] > 0) {
        return [self.dataArray[section] count];
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSArray *array = @[@"出生当天",@"1月龄",@"2月龄",@"3月龄",@"4月龄",@"5月龄",@"6月龄",@"8月龄",@"9月龄",@"1岁龄",@"1.5岁龄",@"2岁龄",@"3岁龄",@"4岁龄",@"6岁龄"];
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];

    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, kScreenWidth-30, 40)];
    lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    if (_nextVaccine == section) {
        lab.textColor = [UIColor colorWithHexString:@"F4B450"];
    }
    
    lab.font = FontSize(15);
//    宝宝出生时间+days
    NSInteger days = [self.headerArray[section] integerValue];
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSString *newdata = [NSString stringWithFormat:@"%f", (data + days*60*60*24)*1000];
    NSString *timestr = [NSString getCurrentTimes:@"yyyy年MM月dd日 " Time:newdata];
    NSString *headstr = [NSString stringWithFormat:@"%@     %@",timestr,array[section]];
    lab.text = headstr;
    [view addSubview:lab];
    
    return view;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithHexString:@"f9F9F9"];
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
//    return 10;
//}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BabyVaccineCell *cell = [BabyVaccineCell theBabyVaccineCellWithtableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.dataArray && self.dataArray.count > indexPath.section) {
        NSDictionary *dic = [self.dataArray[indexPath.section] objectAtIndex:indexPath.row];
        cell.dic = dic;
        
    }
    cell.chooseButtonBlock = ^{
        if (self.dataArray && self.dataArray.count > indexPath.row) {
            NSMutableDictionary *dic = [self.dataArray[indexPath.section] objectAtIndex:indexPath.row];
            NSString *down = dic[@"down"];
            if ([down  isEqual: @"1"]) {
                [dic setValue:@"0" forKey:@"down"];
            } else {
                [dic setValue:@"1" forKey:@"down"];
                [UIView ShowInfo:@"完成了一个疫苗，棒棒哒~" Inview:self.view];
            }
            [self.dataArray[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:dic];
        }
    
        //将修改的后的文件重新保存
        NSMutableArray *array = [NSMutableArray array];
        for (NSArray *array1  in self.dataArray) {
            for ( NSDictionary *dic in array1) {
                [array addObject:dic];
            }
        }
        NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyVaccine1.plist"];
        //写入plist里面
        if ([array writeToFile:filePatch atomically:YES]){
            ZPLog(@"sucess");
        }
    };
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray && self.dataArray.count > indexPath.section) {
        NSDictionary *dic = [self.dataArray[indexPath.section] objectAtIndex:indexPath.row];
        VaccineInfoViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"VaccineInfoViewController"];
        VC.vaccineDic = dic;
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    
//    if (self.dataArray && self.dataArray.count > indexPath.section) {
//        NSMutableDictionary *dic = [self.dataArray[indexPath.section] objectAtIndex:indexPath.row];
//        [dic setValue:@"1" forKey:@"down"];
//        [self.dataArray[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:dic];
//    }
//
//    //将修改的后的文件重新保存
//    NSMutableArray *array = [NSMutableArray array];
//    for (NSArray *array1  in self.dataArray) {
//        for ( NSDictionary *dic in array1) {
//            [array addObject:dic];
//        }
//    }
//    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyVaccine1.plist"];
//    //写入plist里面
//    if ([array writeToFile:filePatch atomically:YES]){
//        ZPLog(@"sucess");
//    }
}



-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 88;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}


- (UILabel *)topLab{
    if (_topLab == nil) {
        _topLab = [[UILabel alloc] init];
        _topLab.backgroundColor = [UIColor colorWithHexString:@"EFF9FD"];
        _topLab.textColor = [UIColor colorWithHexString:@"2b2b2b"];
        _topLab.font = FontSize(18);
        _topLab.textAlignment = NSTextAlignmentCenter;
    }
    return _topLab;
}

- (NSMutableArray *)headerArray{
    if (_headerArray == nil) {
        _headerArray = [NSMutableArray array];
    }
    return _headerArray;
}

- (NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)remind:(id)sender {
    VaccineMessageViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"VaccineMessageViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}


@end
