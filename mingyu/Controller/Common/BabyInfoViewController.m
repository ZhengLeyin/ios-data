//
//  BabyInfoViewController.m
//  mingyu
//
//  Created by apple on 2018/4/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BabyInfoViewController.h"
#import <PGDatePickManager.h>
#import <Security/Security.h>
#import "FCUUID.h"
#import "ParentHomePageViewController.h"

@interface BabyInfoViewController ()<PGDatePickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *BirthView;

@property (weak, nonatomic) IBOutlet UIButton *BirthButton;

@property (weak, nonatomic) IBOutlet UILabel *DadLab;
@property (weak, nonatomic) IBOutlet UIImageView *DadChooseImage;

@property (weak, nonatomic) IBOutlet UILabel *MomLab;
@property (weak, nonatomic) IBOutlet UIImageView *MomChooseImage;

@property (weak, nonatomic) IBOutlet UIButton *SaveButton;

@property (nonatomic, copy) NSString *relationship;

@end

@implementation BabyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    //隐藏导航栏下面的黑线
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    _BirthView.layer.cornerRadius = _BirthView.height/2;
    _BirthView.layer.borderWidth = 1;
    _BirthView.layer.borderColor = [UIColor colorWithHexString:@"e2e2e2"].CGColor;
    
    _DadChooseImage.hidden = _MomChooseImage.hidden = YES;
    
    //    // button标题的偏移量
    //    _MomButton.titleEdgeInsets = UIEdgeInsetsMake(_MomButton.imageView.frame.size.height+20, -_MomButton.imageView.bounds.size.width, 0,0);
    //    // button图片的偏移量
    //    _MomButton.imageEdgeInsets = UIEdgeInsetsMake(0, _MomButton.titleLabel.frame.size.width/2, _MomButton.titleLabel.frame.size.height+5, -_MomButton.titleLabel.frame.size.width/2);
    //
    //    // button标题的偏移量
    //    _DadButton.titleEdgeInsets = UIEdgeInsetsMake(_DadButton.imageView.frame.size.height+20, -_DadButton.imageView.bounds.size.width, 0,0);
    //    // button图片的偏移量
    //    _DadButton.imageEdgeInsets = UIEdgeInsetsMake(0, _DadButton.titleLabel.frame.size.width/2, _DadButton.titleLabel.frame.size.height+5, -_DadButton.titleLabel.frame.size.width/2);
    //
    //    NSString *data = [NSString getCurrentTimes:@"YYYY-MM-dd"];;
    //    [_BirthButton setTitle:data forState:UIControlStateNormal];
    
}




- (IBAction)BirthButton:(UIButton *)sender{
   
    //    _today_image.image  = ImageName(@"未选");
    //    _create_image.image = ImageName(@"未选");
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    datePicker.datePickerMode = PGDatePickerModeDate;
    [self presentViewController:datePickManager animated:false completion:nil];
    
    //    datePickManager.titleLabel.text = @"PGDatePicker";
    //设置半透明的背景颜色
    datePickManager.isShadeBackgroud = true;
    //设置头部的背景颜色
    datePickManager.headerViewBackgroundColor = [UIColor whiteColor];
    //设置线条的颜色
    datePicker.lineBackgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow = [UIColor colorWithHexString:@"2c2c2c"];
    //设置未选中行的字体颜色
    //    datePicker.textColorOfOtherRow = [UIColor colorWithHexString:@"2c2c2c"];
    //设置取消按钮的字体颜色
    datePickManager.cancelButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
    //设置取消按钮的字
    //    datePickManager.cancelButtonText = @"取消";
    //    //设置取消按钮的字体大小
    //    datePickManager.cancelButtonFont = [UIFont boldSystemFontOfSize:17];
    //
    //    //设置确定按钮的字体颜色
    datePickManager.confirmButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
    //    //设置确定按钮的字
    //    datePickManager.confirmButtonText = @"确定";
    //    //设置确定按钮的字体大小
    //    datePickManager.confirmButtonFont = [UIFont boldSystemFontOfSize:17];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    datePicker.minimumDate = [dateFormatter dateFromString: @"2003-01-01"];
    datePicker.maximumDate = [dateFormatter dateFromString: [NSString getCurrentTimes:@"yyyy-MM-dd"]];
}


#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
//    ZPLog(@"dateComponents = %@", dateComponents);
    NSString *text = [NSString stringWithFormat:@"%zd-%02ld-%02ld",dateComponents.year,(long)dateComponents.month,(long)dateComponents.day];
    [self.BirthButton setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
    [self.BirthButton setTitle:text forState:UIControlStateNormal];
    
}
//    STPickerDate *pickerDate = [[STPickerDate alloc]init];
//    [pickerDate setDelegate:self];
//    [pickerDate show];
//
//}
//
//#pragma STPickerDateDelegate
//- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
//{
//    NSString *text = [NSString stringWithFormat:@"%zd-%02ld-%02ld", year, month, day];
//    [self.BirthButton setTitleColor:[UIColor colorWithHexString:@"575757"] forState:UIControlStateNormal];
//    [self.BirthButton setTitle:text forState:UIControlStateNormal];
//}


- (IBAction)DadButton:(UIButton *)sender{
    
    _DadLab.textColor = [UIColor colorWithHexString:@"9CB0EF"];
    _DadChooseImage.hidden = NO;
    
    _MomLab.textColor = [UIColor colorWithHexString:@"CECECE"]
    ;
    _MomChooseImage.hidden = YES;
    
    _relationship = @"1";

}


- (IBAction)MomButton:(UIButton *)sender{
    
    _DadLab.textColor = [UIColor colorWithHexString:@"CECECE"];
    _DadChooseImage.hidden = YES;
    
    _MomLab.textColor = [UIColor colorWithHexString:@"9CB0EF"]
    ;
    _MomChooseImage.hidden = NO;
    
    _relationship = @"2";
}





- (IBAction)SaveButton:(UIButton *)sender{
    
    if ([_BirthButton.titleLabel.text  isEqual: @"请选择出生年月"]) {
        [UIView ShowInfo:@"请选择出生年月" Inview:self.view];
        return;
    }
    
    if (!_relationship) {
        [UIView ShowInfo:@"请选择与宝宝关系" Inview:self.view];
        return;
    }
    
    _SaveButton.userInteractionEnabled = NO;
    [userDefault setObject:_BirthButton.titleLabel.text forKey:KUDuser_birthday];
    [userDefault setObject:_relationship forKey:KUDrelationship];
    [userDefault synchronize];
    NSString *device_id = [userDefault objectForKey:KUDdevice_id];
    if (!device_id) {
         [userDefault setObject:[self uuid] forKey:KUDdevice_id];
    }
    
    NSString *URL =[NSString stringWithFormat:@"%@%@",KURL,KUser];
    NSDictionary *parameters = @{
                          @"user_identity":_relationship,
                          @"user_birthday":_BirthButton.titleLabel.text,
                          @"device_id": [userDefault objectForKey:KUDdevice_id],
                          };

    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            UserModel *model = [UserModel modelWithJSON:json[@"data"]];
            [userDefault setBool:YES forKey:KUDhasLaunch];
            [userDefault setInteger:model.user_id forKey:KUDuser_id];
            [userDefault synchronize];
            [self User];
            
            [UIView ShowInfo:@"宝宝信息保存成功" Inview:self.view];

        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
            _SaveButton.userInteractionEnabled = YES;
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        _SaveButton.userInteractionEnabled = YES;

    }];
}

#pragma mark 获取设备号
-(NSString*) uuid {
    // 获取唯一标示符
    NSString *result = [FCUUID uuidForDevice];
    return result;
}


- (void)User {
    
    TabBarController * tabbar = [[TabBarController alloc]init];
    // 配置
    [CYTabBarConfig shared].selectIndex = 0;
    [CYTabBarConfig shared].centerBtnIndex = 4;
    [CYTabBarConfig shared].bulgeHeight = 4;
    [CYTabBarConfig shared].HidesBottomBarWhenPushedOption = HidesBottomBarWhenPushedAlone;
    
    [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithHexString:@"50D0F4"];
    [CYTabBarConfig shared].textColor = [UIColor colorWithHexString:@"A8A8A8"];
    [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
    [self style2:tabbar];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.rootViewController = tabbar;
}


- (void)style2:(CYTabBarController *)tabbar {
    ParentHomePageViewController *parentHomeVC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentHomePageViewController"];
    UINavigationController *nav1 = [[UINavigationController alloc]initWithRootViewController:parentHomeVC];
    [tabbar addChildController:nav1 title:@"首页" imageName:@"tabbar_home" selectedImageName:@"tabbar_home_select"];
    
    ParentVideoViewController *parentVideoVC = [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoViewController"];
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:parentVideoVC];
    [tabbar addChildController:nav3 title:@"视频" imageName:@"tabbar_video" selectedImageName:@"tabbar_video_select"];
    
//    ParentCommunityViewController *parettCommunityVC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentCommunityViewController"];
//    UINavigationController *nav2 = [[UINavigationController alloc]initWithRootViewController:parettCommunityVC];
//    [tabbar addChildController:nav2 title:@"发现" imageName:@"tabbar_community" selectedImageName:@"tabbar_community_select"];
    
    MineViewController *mineVC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineViewController"];
    UINavigationController *nav4 = [[UINavigationController alloc]initWithRootViewController:mineVC];
    [tabbar addChildController:nav4 title:@"我的" imageName:@"tabbar_mine" selectedImageName:@"tabbar_mine_select"];
//    [tabbar addCenterController:nil bulge:YES title:nil imageName:@"footer" selectedImageName:@"footer"];
}



@end
