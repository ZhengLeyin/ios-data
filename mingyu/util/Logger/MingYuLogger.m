//
//  MingYuLogger.m
//  MingYuXIbDemo
//
//  Created by MingYu on 2018/12/11.
//  Copyright © 2018 com.mingyu.www. All rights reserved.
//

#import "MingYuLogger.h"


NSString *const MingYuLoggerDomainCURL = @"LOG_CURL";
NSString *const MingYuLoggerDomainNetwork = @"LOG_NETWORK";
NSString *const MingYuLoggerDomainStorage = @"LOG_STORAGE";
NSString *const MingYuLoggerDomainIM = @"LOG_IM";
NSString *const MingYuLoggerDomainDefault = @"LOG_DEFAULT";

static NSMutableSet *loggerDomain = nil;
static NSUInteger loggerLevelMask = MingYuLoggerLevelNone;
static NSArray *loggerDomains = nil;

@implementation MingYuLogger

+ (void)load {
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        loggerDomains = @[
                          MingYuLoggerDomainCURL,
                          MingYuLoggerDomainNetwork,
                          MingYuLoggerDomainIM,
                          MingYuLoggerDomainStorage,
                          MingYuLoggerDomainDefault
                          ];
    });
#ifdef DEBUG
    [self setAllLogsEnabled:YES];
#else
    [self setAllLogsEnabled:NO];
#endif
}

+ (void)setAllLogsEnabled:(BOOL)enabled {
    if (enabled) {
        for (NSString *loggerDomain in loggerDomains) {
            [MingYuLogger addLoggerDomain:loggerDomain];
        }
        [MingYuLogger setLoggerLevelMask:MingYuLoggerLevelAll];
    } else {
        for (NSString *loggerDomain in loggerDomains) {
            [MingYuLogger removeLoggerDomain:loggerDomain];
        }
        [MingYuLogger setLoggerLevelMask:MingYuLoggerLevelNone];
    }
    
    [self setCertificateInspectionEnabled:enabled];
}

+ (void)setCertificateInspectionEnabled:(BOOL)enabled {
    if (enabled) {
        setenv("CURL_INSPECT_CERT", "YES", 1);
    } else {
        unsetenv("CURL_INSPECT_CERT");
    }
}

+ (void)setLoggerLevelMask:(NSUInteger)levelMask {
    loggerLevelMask = levelMask;
}

+ (void)addLoggerDomain:(NSString *)domain {
    if (!loggerDomain) {
        loggerDomain = [[NSMutableSet alloc] init];
    }
    [loggerDomain addObject:domain];
}

+ (void)removeLoggerDomain:(NSString *)domain {
    [loggerDomain removeObject:domain];
}

+ (BOOL)levelEnabled:(MingYuLoggerLevel)level {
    return loggerLevelMask & level;
}

+ (BOOL)containDomain:(NSString *)domain {
    return [loggerDomain containsObject:domain];
}

+ (void)logFunc:(const char *)func line:(int)line domain:(NSString *)domain level:(MingYuLoggerLevel)level message:(NSString *)fmt, ... {
    if (!domain || [loggerDomain containsObject:domain]) {
        if (level & loggerLevelMask) {
            NSString *levelString = nil;
            switch (level) {
                case MingYuLoggerLevelInfo:
                    levelString = @"💙INFO";
                    break;
                case MingYuLoggerLevelDebug:
                    levelString = @"💚DEBUG";
                    break;
                case MingYuLoggerLevelError:
                    levelString = @"❤️ERROR";
                    break;
                default:
                    levelString = @"🧡UNKNOW";
                    break;
            }
            va_list args;
            va_start(args, fmt);
            NSString *message = [[NSString alloc] initWithFormat:fmt arguments:args];
            va_end(args);
            NSLog(@"[%@] %s [Line %d] %@", levelString, func, line, message);
        }
    }
}

@end

