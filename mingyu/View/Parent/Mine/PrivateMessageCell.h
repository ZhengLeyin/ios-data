//
//  PrivateMessageCell.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QQUserModel.h"

@interface PrivateMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *head_image;

@property (weak, nonatomic) IBOutlet UILabel *name_lab;

@property (weak, nonatomic) IBOutlet UILabel *content_lab;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;

@property (weak, nonatomic) IBOutlet UILabel *unread_lab;


@property (nonatomic, strong) QQUserModel *userModel;


+ (instancetype)thePrivateMessageCellWithTableView:(UITableView *)tableview;

@end
