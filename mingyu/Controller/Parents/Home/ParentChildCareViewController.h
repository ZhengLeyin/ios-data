//
//  ParentChildCareViewController.h
//  mingyu
//
//  Created by apple on 2018/5/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LabelListModel;

@interface ParentChildCareViewController : UIViewController

@property (nonatomic, strong) LabelListModel *lablistmodel;


@property (weak, nonatomic) IBOutlet UIButton *rightItem;

@end
