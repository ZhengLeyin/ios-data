//
//  MyHeaderView.m
//  HVScrollView
//
//  Created by Libo on 17/6/14.
//  Copyright © 2017年 iDress. All rights reserved.
//

#import "MyHeaderView.h"

@interface MyHeaderView()
@end

@implementation MyHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self addview];

    }
    return self;
}


- (void)addview{
    
    for (int i = 0; i < 4; i++) {
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake((kScreenWidth/4)*i+10, 10, kScreenWidth/4-20, 60);
        self.button.backgroundColor = [UIColor yellowColor];
        [self.button setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
        // button标题的偏移量
        self.button.titleEdgeInsets = UIEdgeInsetsMake(self.button.imageView.frame.size.height+5, -self.button.imageView.bounds.size.width, 0,0);
        // button图片的偏移量
        self.button.imageEdgeInsets = UIEdgeInsetsMake(0, self.button.titleLabel.frame.size.width/2, self.button.titleLabel.frame.size.height+5, -self.button.titleLabel.frame.size.width/2);
        
        NSString *str = [NSString stringWithFormat:@"按钮%d",i];
        [self.button setTitle:str forState:UIControlStateNormal];
        _button.tag = i;
        [self.button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self addSubview:self.button];
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0,self.frame.size.height-1, kScreenWidth, 1)];
    view.backgroundColor = [UIColor blackColor];
    [self addSubview:view];
    
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//    self.button.center = CGPointMake(self.bounds.size.width*0.5, self.bounds.size.height*0.5);
//}

@end
