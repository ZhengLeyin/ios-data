//
//  PrivateMessageCell.m
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PrivateMessageCell.h"

@implementation PrivateMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

+ (instancetype)thePrivateMessageCellWithTableView:(UITableView *)tableview{
    static NSString *cellId=@"PrivateMessageCell";
    PrivateMessageCell *cell = [tableview dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PrivateMessageCell" owner:nil options:nil][0];
    }
    return cell;
}


- (void)setUserModel:(QQUserModel *)userModel{
    _userModel = userModel;
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,userModel.user_head];
    [_head_image zp_setImageWithURL:userModel.user_head placeholderImage:ImageName(@"默认头像")];
    
    _name_lab.text = userModel.user_name;
    _content_lab.text = userModel.message_content;
    
    _time_lab.text = userModel.time_status;
//    [NSString compareTimeForNow:[userModel.message_time integerValue]];
    
    if (_userModel.unread_number) {
        _unread_lab.hidden = NO;
        _unread_lab.text = [NSString stringWithFormat:@" %ld ",_userModel.unread_number];
    } else {
        _unread_lab.hidden = YES;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
