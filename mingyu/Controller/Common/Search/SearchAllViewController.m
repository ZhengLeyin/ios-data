//
//  SearchAllViewController.m
//  mingyu
//
//  Created by apple on 2018/6/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "SearchAllViewController.h"
#import "SearchModel.h"
#import "ChildCareDetailViewController.h"

@interface SearchAllViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayData;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, strong) NothingView *nothingView;

@end

@implementation SearchAllViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self refreshTableview];
}


//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        if (self.arrayData.count>0) {
            [self.arrayData removeAllObjects];
        }
        _start = 0;
//        [_tableView.mj_footer resetNoMoreData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getSearchAllList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getSearchAllList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
}


- (void)getSearchAllList{
    NSString *URLstr = [NSString stringWithFormat:@"%@%@?",KURL,KSearch];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"key":_searchStr,
                                 @"indices":@"*index",
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URLstr = [NSString connectUrl:parameters url:URLstr];
    [HttpRequest getWithURLString:URLstr parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 20;
            NSArray *data = json[@"data"];
            if (data.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                SearchModel *model = [SearchModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            if (self.arrayData && self.arrayData .count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        if (self.arrayData && self.arrayData .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.arrayData && self.arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerview = [UIView new];
    headerview.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    return headerview;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
     UIView *footerview = [UIView new];
    footerview.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    return footerview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchModel *model = [[SearchModel alloc] init];
    if (_arrayData && _arrayData.count > indexPath.section) {
        model = self.arrayData[indexPath.section];
    }
//    if ([model.result_type  isEqual: @"topic"]) {           //帖子
//        ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
//        NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
//        cell.newsmodel = newsmodel;
//        [cell showSearch];
//        return cell;
//    } else
    if ([model.result_type  isEqual: @"childcare"]) {      //育儿必读
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
        NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
        cell.newsmodel = newsmodel;
        [cell showSearch];
        return cell;
    } else if ([model.result_type  isEqual: @"article"]) {       //资讯
        ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
        NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
        cell.newsmodel = newsmodel;
        [cell showSearch];
        return cell;
    } else if ([model.result_type  isEqual: @"course"]) {        //课程
        ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
        CourseModel *coursemodel = [CourseModel modelWithJSON:model.data];
        cell.coursemodel = coursemodel;
        [cell showSearch];
        return cell;
    } else if ([model.result_type  isEqual: @"user"]) {        //用户
        AttentionFansCell *cell = [AttentionFansCell theAttentionFansCellWithtableView:tableView];
        UserModel *usermodel = [UserModel modelWithJSON:model.data];
        cell.usermodel = usermodel;
        cell.attentButtonBlock = ^(NSInteger state) {
            NSMutableDictionary *dic = model.data;
            [dic setValue:@(state) forKey:@"follow_status"];
        };
        [cell showSearch];
        cell.lineView.hidden = YES;
        return cell;
    }
//    else if ([model.result_type  isEqual: @"circle"]) {        //圈子
//        RightTableViewCell *cell = [RightTableViewCell theCircleCellWithTableView:tableView];
//        ParentCircleModel *circlemodel = [ParentCircleModel modelWithJSON:model.data];
//        cell.circlemodel = circlemodel;
//        [cell showSearch];
//        return cell;
//    }
    else if ([model.result_type isEqual:@"video"]){       //画短视频
        ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
        VideoModel *videomodel = [VideoModel modelWithJSON:model.data];
        cell.videomodel = videomodel;
        return  cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_arrayData && _arrayData.count > indexPath.section) {
        SearchModel *model = self.arrayData[indexPath.section];
//        if ([model.result_type  isEqual: @"topic"]) {       //帖子
//            return 130;
//        } else
        if ([model.result_type  isEqual: @"childcare"]){         //育儿必读
            return 130;
        } else if ([model.result_type  isEqual: @"article"]){         //资讯
            return 130;
        } else if ([model.result_type  isEqual: @"course"]){      //课程
            return 155;
        } else if ([model.result_type  isEqual: @"user"]){         //用户
            return 98;
        }
//        else if ([model.result_type  isEqual: @"circle"]) {          //圈子
//            return 82;
//        }
        else if ([model.result_type isEqual:@"video"]) {    //短视频
            return 230;
        }
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_arrayData && _arrayData.count > indexPath.section) {
        SearchModel *model = self.arrayData[indexPath.section];
//        if ([model.result_type  isEqual: @"topic"]) {       //帖子
//            ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
//            NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
//            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
//            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
//            VC.newsmodel = newsmodel;
//            [self.navigationController pushViewController:VC animated:YES];
//        } else
        if ([model.result_type  isEqual: @"childcare"]){         //育儿必读
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
            VC.newsmodel = newsmodel;
            [self.navigationController pushViewController:VC animated:YES];
        } else if ([model.result_type  isEqual: @"article"]){         //资讯
            ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
            NewsModel *newsmodel = [NewsModel modelWithJSON:model.data];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
            newsmodel.news_title = [newsmodel.news_title stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
            VC.newsmodel = newsmodel;
            [self.navigationController pushViewController:VC animated:YES];
        } else if ([model.result_type  isEqual: @"course"]){      //课程
            CourseModel *coursemodel = [CourseModel modelWithJSON:model.data];
            if (coursemodel.course_type == 1) {                  //视频课程
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = coursemodel;
                [self.navigationController pushViewController:VC animated:YES];
            } else{              //音频课程
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = coursemodel;
                [self.navigationController pushViewController:VC animated:YES];
            }
        } else if ([model.result_type  isEqual: @"user"]){         //用户
            UserModel *usermodel = [UserModel modelWithJSON:model.data];
            if (usermodel.user_id != [USER_ID integerValue] || ![userDefault boolForKey:KUDhasLogin]) {
                PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
                VC.user_id = usermodel.user_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
//        else if ([model.result_type  isEqual: @"circle"]) {            //圈子
//            ParentCircleModel *circlemodel = [ParentCircleModel modelWithJSON:model.data];
//            SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
//            VC.circlemodel = circlemodel;
//            [self.navigationController pushViewController:VC animated:YES];
//        }
        else if ([model.result_type isEqual:@"video"]){       //画短视频
            VideoModel *videomodel = [VideoModel modelWithJSON:model.data];
            ParentVideoDetailViewController *VC = [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
            VC.video_id = videomodel.video_id;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}



- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无搜索内容";
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

@end
