//
//  VideoDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/3/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoBaseViewController.h"

typedef void(^selectDetaailCell)(NSInteger index);

@interface VideoDetailViewController : VideoBaseViewController


@property (nonatomic, copy) selectDetaailCell block;
@end
