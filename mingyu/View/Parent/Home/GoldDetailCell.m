//
//  GoldDetailCell.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "GoldDetailCell.h"

@implementation GoldDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDetailmodel:(IntegralDetailModel *)detailmodel{
    _action_explain_lab.text = detailmodel.details_explain;
    _action_time_lab.text = detailmodel.time_status;
    if (detailmodel.score > 0) {
        _action_score_lab.text = [NSString stringWithFormat:@"+%ld",(long)detailmodel.score];
        _action_score_lab.textColor = [UIColor colorWithHexString:@"F7A429"];
    } else {
        _action_score_lab.text = [NSString stringWithFormat:@"%ld",(long)detailmodel.score];
        _action_score_lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
    }
}



@end
