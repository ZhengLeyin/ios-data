//
//  MineAttentionViewController.h
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineAttentionViewController : UIViewController

@property (nonatomic, assign) NSInteger from_id;

/** type */
@property (nonatomic, assign) NSUInteger  type;

@property (nonatomic, copy) void (^PushBlock) (UIViewController *vc);

@end
