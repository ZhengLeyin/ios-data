//
//  SystemMessageCell.m
//  mingyu
//
//  Created by apple on 2018/9/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "SystemMessageCell.h"

static CGFloat cellHeight = 0;

@implementation SystemMessageCell

+ (CGFloat)cellHeight {
    return cellHeight;
}

+ (instancetype)theSystemMessageCell1WithTableView:(UITableView *)tableview{
    static NSString *cellId=@"SystemMessageCell";
    SystemMessageCell *cell = [tableview dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SystemMessageCell" owner:nil options:nil][0];
    }
    return cell;
}


+ (instancetype)theSystemMessageCell2WithTableView:(UITableView *)tableview{
    static NSString *cellId=@"SystemMessageCell";
    SystemMessageCell *cell = [tableview dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SystemMessageCell" owner:nil options:nil][1];
    }
    return cell;
}

+ (instancetype)theSystemMessageCell3WithTableView:(UITableView *)tableview{
    static NSString *cellId=@"SystemMessageCell";
    SystemMessageCell *cell = [tableview dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SystemMessageCell" owner:nil options:nil][2];
    }
    return cell;
}


- (void)setMessageModel:(SystemMessageModel *)messageModel{
    _messageModel = messageModel;
    _timeLab.text = _messageModel.time_status;
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle  setLineSpacing:5];
    if (messageModel.type == 1 || messageModel.type == 2 || messageModel.type == 3 || messageModel.type == 21) {  //评论
        NSString *type = [NSString string];
        if (messageModel.type == 21) type = @"课程";
        else if (messageModel.type == 1) type = @"帖子";
        else if (messageModel.type == 3) type = @"视频";
        NSString *subMessageStr = [NSString stringWithFormat:@"%@ 对您的%@“%@”进行了评论",_messageModel.from_user_name,type,_messageModel.sub_message];
        NSMutableAttributedString *subMessageattrString = [[NSMutableAttributedString alloc] initWithString:subMessageStr];
        [subMessageattrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"2C2C2C"] range:NSMakeRange(0,_messageModel.from_user_name.length+6)];
        [subMessageattrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"2C2C2C"] range:NSMakeRange(subMessageStr.length-5,5)];
        [subMessageattrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [subMessageStr length])];
        _sub_messageLab.attributedText = subMessageattrString;
        
        NSString *messageStr = [NSString stringWithFormat:@"%@",_messageModel.message];
        NSMutableAttributedString *messageattrString = [[NSMutableAttributedString alloc] initWithString:messageStr];
        [messageattrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [messageStr length])];
        _messageLab.attributedText = messageattrString;
        
        cellHeight =  [self hideLabelLayoutHeight:subMessageStr withTextFontSize:14 andWidth:kScreenWidth-40]+100;
        cellHeight += [self hideLabelLayoutHeight:messageStr withTextFontSize:14 andWidth:kScreenWidth-40];

    } else if (messageModel.type == 4 || messageModel.type == 5 || messageModel.type == 6 || messageModel.type == 7 || messageModel.type == 8 || messageModel.type == 22) {  //回复评论
        
        NSString *subMessageStr = [NSString stringWithFormat:@"%@ 对您的评论“%@”进行了回复",_messageModel.from_user_name,_messageModel.sub_message];
        NSMutableAttributedString *subMessageattrString = [[NSMutableAttributedString alloc] initWithString:subMessageStr];
        [subMessageattrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"2C2C2C"] range:NSMakeRange(0,_messageModel.from_user_name.length+6)];
        [subMessageattrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"2C2C2C"] range:NSMakeRange(subMessageStr.length-5,5)];
        [subMessageattrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [subMessageStr length])];
        _sub_messageLab.attributedText = subMessageattrString;
        
        NSString *messageStr = [NSString stringWithFormat:@"%@",_messageModel.message];
        NSMutableAttributedString *messageattrString = [[NSMutableAttributedString alloc] initWithString:messageStr];
        [messageattrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [messageStr length])];
        _messageLab.attributedText = messageattrString;
        
        cellHeight =  [self hideLabelLayoutHeight:subMessageStr withTextFontSize:14 andWidth:kScreenWidth-40]+100;
        cellHeight += [self hideLabelLayoutHeight:messageStr withTextFontSize:14 andWidth:kScreenWidth-40-30];
        
    } else if (messageModel.type == 9 || messageModel.type == 10 || messageModel.type == 11 || messageModel.type == 25) { //收藏
        NSString *type = [NSString string];
        if (messageModel.type == 9) type = @"帖子";
        else type = @"视频";
        _sub_messageLab.text = [NSString stringWithFormat:@"%@ 收藏了你的%@",_messageModel.from_user_name,type];
        NSString *messageStr = [NSString stringWithFormat:@"“%@”",_messageModel.message];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:messageStr];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [messageStr length])];
        _messageLab.attributedText = attrString;
        cellHeight =  [self hideLabelLayoutHeight:messageStr withTextFontSize:14 andWidth:kScreenWidth-40]+80;
        
    } else if (messageModel.type == 12) { //关注
        _sub_messageLab.text = [NSString stringWithFormat:@"%@ 关注了你",_messageModel.from_user_name];
        cellHeight = 80;
    } else if (messageModel.type == 13 || messageModel.type == 14 || messageModel.type == 15 ||messageModel.type == 23) {  //赞帖子
        NSString *type = [NSString string];
        if (messageModel.type == 13) type = @"帖子";
        else type = @"视频";
        _sub_messageLab.text = [NSString stringWithFormat:@"%@ 赞了你的%@",_messageModel.from_user_name,type];
        NSString *messageStr = [NSString stringWithFormat:@"“%@”",_messageModel.message];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:messageStr];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [messageStr length])];
        _messageLab.attributedText = attrString;
        cellHeight =  [self hideLabelLayoutHeight:messageStr withTextFontSize:14 andWidth:kScreenWidth-40]+80;

    } else if (messageModel.type == 16 || messageModel.type == 17 || messageModel.type == 18 || messageModel.type == 19 || messageModel.type == 20 ||messageModel.type == 24) { //赞评论
        _sub_messageLab.text = [NSString stringWithFormat:@"%@ 赞了您的评论",_messageModel.from_user_name];
        NSString *messageStr = [NSString stringWithFormat:@"“%@”",_messageModel.message];

        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:messageStr];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [messageStr length])];
        _messageLab.attributedText = attrString;
        cellHeight =  [self hideLabelLayoutHeight:messageStr withTextFontSize:14 andWidth:kScreenWidth-40]+80;
    } else {
        cellHeight = 90;
    }
    
    _messageModel.height = cellHeight;
}


-(CGFloat )hideLabelLayoutHeight:(NSString *)content withTextFontSize:(CGFloat)FontSize andWidth:(CGFloat)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;  // 段落高度
    NSMutableAttributedString *attributes = [[NSMutableAttributedString alloc] initWithString:content];
    [attributes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize] range:NSMakeRange(0, content.length)];
    [attributes addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];
    CGSize attSize = [attributes boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return attSize.height;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
