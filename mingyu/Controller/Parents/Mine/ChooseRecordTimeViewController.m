//
//  ChooseRecordTimeViewController.m
//  mingyu
//
//  Created by apple on 2018/8/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChooseRecordTimeViewController.h"
#import <PGDatePickManager.h>

@interface ChooseRecordTimeViewController ()<PGDatePickerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *today_lab;
@property (weak, nonatomic) IBOutlet UIImageView *today_image;

@property (weak, nonatomic) IBOutlet UILabel *create_lab;
@property (weak, nonatomic) IBOutlet UIImageView *create_image;

@property (weak, nonatomic) IBOutlet UILabel *choose_lab;

@property (weak, nonatomic) IBOutlet UIView *createView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *createViewH;

@property (nonatomic, strong) NSString *recordTime;


@end

@implementation ChooseRecordTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _today_lab.text = [NSString getCurrentTimes:@"yyyy年MM月dd日"];
    if (_photoTime) {
        _create_lab.text = _photoTime;
        _recordTime = _photoTime;
    } else {
        _createView.hidden = YES;
        _createViewH.constant = 0;
    }

    _today_image.image  = ImageName(@"已选");
    _create_image.image = ImageName(@"未选");
}


- (IBAction)todayButton:(id)sender {
    _today_image.image  = ImageName(@"已选");
    _create_image.image = ImageName(@"未选");
    _recordTime = _today_lab.text;
}

- (IBAction)createButton:(id)sender {
    _today_image.image  = ImageName(@"未选");
    _create_image.image = ImageName(@"已选");
    _recordTime = _create_lab.text;

}

- (IBAction)chooseButton:(id)sender {
//    _today_image.image  = ImageName(@"未选");
//    _create_image.image = ImageName(@"未选");
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    datePicker.datePickerMode = PGDatePickerModeDate;
    [self presentViewController:datePickManager animated:false completion:nil];
    
//    datePickManager.titleLabel.text = @"PGDatePicker";
    //设置半透明的背景颜色
    datePickManager.isShadeBackgroud = true;
    //设置头部的背景颜色
    datePickManager.headerViewBackgroundColor = [UIColor whiteColor];
    //设置线条的颜色
    datePicker.lineBackgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow = [UIColor colorWithHexString:@"2c2c2c"];
    //设置未选中行的字体颜色
//    datePicker.textColorOfOtherRow = [UIColor colorWithHexString:@"2c2c2c"];
    //设置取消按钮的字体颜色
    datePickManager.cancelButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
    //设置取消按钮的字
//    datePickManager.cancelButtonText = @"取消";
//    //设置取消按钮的字体大小
//    datePickManager.cancelButtonFont = [UIFont boldSystemFontOfSize:17];
//
//    //设置确定按钮的字体颜色
    datePickManager.confirmButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
//    //设置确定按钮的字
//    datePickManager.confirmButtonText = @"确定";
//    //设置确定按钮的字体大小
//    datePickManager.confirmButtonFont = [UIFont boldSystemFontOfSize:17];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    datePicker.minimumDate = [dateFormatter dateFromString: @"2003-01-01"];
    datePicker.maximumDate = [dateFormatter dateFromString: [NSString getCurrentTimes:@"yyyy-MM-dd"]];
}


#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    ZPLog(@"dateComponents = %@", dateComponents);
    _today_image.image  = ImageName(@"未选");
    _create_image.image = ImageName(@"未选");
    _choose_lab.text = [NSString stringWithFormat:@"%ld年%02ld月%02ld日",dateComponents.year,(long)dateComponents.month,(long)dateComponents.day];
    _choose_lab.textColor = [UIColor colorWithHexString:@"50D0F4"];
    _recordTime = _choose_lab.text;
}


- (IBAction)doneButton:(id)sender {
    if (self.DownBlock) {
        self.DownBlock(_recordTime);
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
