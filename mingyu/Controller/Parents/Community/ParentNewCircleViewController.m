//
//  ParentNewCircleViewController.m
//  mingyu
//
//  Created by apple on 2018/7/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentNewCircleViewController.h"
#import "ParentAddCircleViewController.h"
#import "ZFSpeedLoadingView.h"

@interface ParentNewCircleViewController ()<UITableViewDelegate,UITableViewDataSource,RightTableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *myCircleArray;

@property (nonatomic, strong) NSMutableArray *recommentArray;


@end

@implementation ParentNewCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
//    [self getUserCircleList];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getUserCircleList];

}

- (void)getUserCircleList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserCircleList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"birthday":[userDefault objectForKey:KUDuser_birthday]

                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.myCircleArray removeAllObjects];
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                ParentCircleModel *model = [ParentCircleModel modelWithJSON:dic];
                [self.myCircleArray addObject:model];
            }
//            [self.tableView reloadData];
            [self getRecommendCircleList];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)getRecommendCircleList{
    NSString *URL1 = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendCircleList];
    NSDictionary *parameters1 = @{
                                  @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                  @"birthday":[userDefault objectForKey:KUDuser_birthday]
                                  };
    URL1 = [NSString connectUrl:parameters1 url:URL1];
    [HttpRequest getWithURLString:URL1 parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.recommentArray removeAllObjects];
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                ParentCircleModel *model = [ParentCircleModel modelWithJSON:dic];
                [self.recommentArray addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.myCircleArray.count;
    } else {
        return self.recommentArray.count;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        ParentCicleCell *header = [ParentCicleCell theHeaderView];
        header.header_label.text = @"我的圈子";
        return header;
    } else {
        if (self.recommentArray.count > 0) {
            ParentCicleCell *header = [ParentCicleCell theHeaderView];
            header.header_label.text = @"推荐圈子";
            return header;
        } else {
            return nil;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1 && self.recommentArray.count == 0) {
        return 0;
    }
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *footview  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 100)];
        footview.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 60, 60)];
        imageview.image = [UIImage imageNamed:@"添加圈子"];
        [footview addSubview:imageview];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(60+15+13, 0, 100, 90)];
        lab.text = @"添加圈子";
        lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        lab.font = FontSize(17);
        [footview addSubview:lab];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, kScreenWidth, 10)];
        line.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        [footview addSubview:line];
        
        //添加轻扣手势
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreCircle)];
        [footview addGestureRecognizer:tap];
        return footview;
    } else {
        return nil;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 100;
    }
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RightTableViewCell *cell = [RightTableViewCell theCircleCellWithTableView:tableView];
    cell.delegate = self;
    if (indexPath.section == 0) {
        cell.add_button.hidden = YES;
        cell.quit_button.hidden = NO;
        if (_myCircleArray.count > indexPath.row) {
            cell.circlemodel = _myCircleArray[indexPath.row];
        }
    } else {
        cell.add_button.hidden = NO;
        cell.quit_button.hidden = YES;
        if (_recommentArray.count > indexPath.row) {
            cell.circlemodel = _recommentArray[indexPath.row];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if (indexPath.section == 0 && _myCircleArray.count > indexPath.row ) {
        SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
        VC.circlemodel = [_myCircleArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:VC animated:YES];
    } else if (indexPath.section == 1 && self.recommentArray.count > indexPath.row) {
        SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
        VC.circlemodel = [_recommentArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:VC animated:YES];
    }
}



#pragma mark - action
- (void)moreCircle {
    ParentAddCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentAddCircleViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}


#pragma -mark- RightTableViewDelegate
//添加圈子
- (void)addCircle:(ParentCircleModel *)circlemodel{
    if (circlemodel.is_join_true) {
        
    } else {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddUserCircle];
        NSDictionary *parameters = @{
                                     @"user_id" : USER_ID,
                                     @"circle_id" : @(circlemodel.circle_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                circlemodel.is_join_true = YES;
                [_myCircleArray addObject:circlemodel];
                [_recommentArray removeObject:circlemodel];
                [_tableView reloadData];
                [UIView ShowInfo:TipAddCircleSuccess Inview:self.view];
            } else {
                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}

//退出圈子
- (void)quitCircle:(ParentCircleModel *)circlemodel{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"确认退出%@吗？",circlemodel.circle_name]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             //响应事件

                                                         }];
    [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];

    UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              //响应事件
                                                              
                                                              NSString *URL =[NSString stringWithFormat:@"%@%@",KURL,KDelUserCircle];
                                                              NSDictionary *parameters = @{
                                                                                           @"user_id":USER_ID,
                                                                                           @"circle_id":@(circlemodel.circle_id)
                                                                                           };
                                                              [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                  NSDictionary *json = responseObject;
                                                                  ZPLog(@"%@\n%@",json,json[@"message"]);
                                                                  BOOL success = [json[@"success"] boolValue];
                                                                  if (success) {
                                                                      circlemodel.is_join_true = NO;
                                                                      [_myCircleArray removeObject:circlemodel];
                                                                      [_recommentArray addObject:circlemodel];
                                                                      [_tableView reloadData];
                                                                      
                                                                      [UIView ShowInfo:TipDelCircleSuccess Inview:self.view];
                                                                  } else {
                                                                      [UIView ShowInfo:json[@"message"] Inview:self.view];
                                                                  }
                                                              } failure:^(NSError *error) {
//                                                                  [UIView ShowInfo:TipWrongMessage Inview:self.view];
                                                              }];
                                                          }];
    [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];

    [alert addAction:cancelAction];
    [alert addAction:confirmAction];

    [self presentViewController:alert animated:YES completion:nil];
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 82;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}


- (NSMutableArray *)myCircleArray{
    if (!_myCircleArray) {
        _myCircleArray = [NSMutableArray array];
    }
    return _myCircleArray;
}


- (NSMutableArray *)recommentArray{
    if (!_recommentArray) {
        _recommentArray = [NSMutableArray array];
    }
    return _recommentArray;
}



@end
