//
//  VideoInteractCell.h
//  mingyu
//
//  Created by apple on 2018/4/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

@protocol VideoInteractCellDelegate <NSObject>

- (void)zanButtonClicked:(CommentModel *)commenmodel button:(UIButton *)button;

@end


#import <UIKit/UIKit.h>


@interface VideoInteractCell : UITableViewCell

/**
 用户图像
 */
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

/**
 用户名
 */
@property (weak, nonatomic) IBOutlet UILabel *userNameLab;

/**
 品论时间
 */
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

/**
 点赞
 */
@property (weak, nonatomic) IBOutlet UIButton *zanButton;

/**
 评论内容
 */
@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@property (nonatomic, strong) CommentModel *commenmodel;

@property (nonatomic, weak) id<VideoInteractCellDelegate> delegate;

@end
