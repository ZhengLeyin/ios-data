//
//  MineJinDouViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineJinDouViewController.h"

@interface MineJinDouViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *ArrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@end

@implementation MineJinDouViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self refreshTableview];
    
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.ArrayData.count>0) {
            [_ArrayData removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self getUserActionList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getUserActionList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}



- (void)getUserActionList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KIntegralDetail];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSDictionary *data = json[@"data"];
            NSArray *actionList = data[@"actionList"];
            if (actionList && actionList.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }

            for (NSDictionary *dic in actionList) {
                IntegralDetailModel *detailmodel = [IntegralDetailModel modelWithJSON:dic];
                [self.ArrayData addObject:detailmodel];
            }
            [_tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.ArrayData && self.ArrayData.count > 0) {
        return _ArrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"MineJinDouCell";
    MineJinDouCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[MineJinDouCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (_ArrayData && _ArrayData.count > indexPath.row) {
        cell.detailmodel = [_ArrayData objectAtIndex:indexPath.row];
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (NSMutableArray *)ArrayData{
    if (_ArrayData == nil) {
        _ArrayData = [NSMutableArray array];
    }
    return _ArrayData;
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end



@implementation MineJinDouCell


- (void)setDetailmodel:(IntegralDetailModel *)detailmodel{
    _detailmodel = detailmodel;
    
    _title_lab.text = detailmodel.details_explain;
    _jindou_lab.text = [NSString stringWithFormat:@"+%ld",(long)detailmodel.score];
    _time_lab.text = detailmodel.time_status;
    
    //    _time_lab.text = [NSString getCurrentTimes:@"yyyy/MM/dd" Time:detailmodel.time_status];
}



@end
