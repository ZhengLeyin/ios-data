//
//  ParentAddCircleViewController.m
//  mingyu
//
//  Created by apple on 2018/7/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentAddCircleViewController.h"

#define leftTableWidth [UIScreen mainScreen].bounds.size.width * 0.25
#define rightTableWidth [UIScreen mainScreen].bounds.size.width * 0.75

@interface ParentAddCircleViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *leftArrayData;
@property (nonatomic, strong) NSMutableArray *rightArrayData;
@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UITableView *rightTableView;

@property (nonatomic, strong) ClassifyModel *choosemodel;


@end

@implementation ParentAddCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.leftTableView];
    [self.view addSubview:self.rightTableView];
    
    [self getClassifyList];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getCircleList) name:NSNotificationAddCircle object:nil];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationAddCircle object:self];

}

- (void)getClassifyList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetClassifyList];
    NSDictionary *parameters = @{
                                 @"classify_type":@"1"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            [self.leftArrayData removeAllObjects];
            for (NSDictionary *dict in data) {
                ClassifyModel *classifymodel = [ClassifyModel modelWithJSON:dict];
                [self.leftArrayData addObject:classifymodel];
            }
            [self.leftTableView reloadData];
            
            [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
            
            if (_leftArrayData && _leftArrayData.count > 0) {
                _choosemodel = _leftArrayData[0];
//                _choosemodel = classifymodel;
                [self getCircleList];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark - TableView DataSource Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_leftTableView == tableView) {
        return 1;
    } else {
        if (_choosemodel.classify_id == 6) {
            return self.rightArrayData.count;
        }
        return 1;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_leftTableView == tableView) {
        return self.leftArrayData.count;
    }
    else {
        if (_choosemodel.classify_id == 6) {
            ClassifyModel *model = self.rightArrayData[section];
            if (model.show == NO) {
                return 0;
            }else {
                return model.circleList.count;
            }
        }
        return self.rightArrayData.count;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == _rightTableView && _choosemodel.classify_id == 6) {
        
        UIView *headerView = (UIView *)[tableView viewWithTag:100];
        if (!headerView) {
            //创建header的view
            headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightTableWidth, 50)];
            headerView.tag = 100;
            headerView.backgroundColor = [UIColor whiteColor];
            headerView.tag = 2016 + section;
            ClassifyModel *model = self.rightArrayData[section];
    
            UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, rightTableWidth-100, 50)];
            label.textColor = [UIColor colorWithHexString:@"2C2C2C"];
            label.font = FontSize(15);
            label.text = [NSString stringWithFormat:@"%@(%ld)",model.circle_name,model.circleList.count];
            [headerView addSubview:label];
    
            //添加imageview
            UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(rightTableWidth-35, 15, 20, 20)];
            //三目运算选择展开或者闭合时候的图标
            iv.image = model.show ? [UIImage imageNamed:@"圈子收起"] : [UIImage imageNamed:@"圈子展开"];
            [headerView addSubview:iv];
    
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(18, 49, rightTableWidth-18, 1)];
            line.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
            [headerView addSubview:line];
    
            //添加轻扣手势
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGR:)];
            [headerView addGestureRecognizer:tap];
        }
        return headerView;
    } else {
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == _rightTableView && _choosemodel.classify_id == 6) {
        return 50;
    } else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_leftTableView == tableView) {
        static NSString *identify = @"LeftTableViewCell";
        LeftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"LeftTableViewCell" owner:self options:nil] lastObject];
        }
        if (self.leftArrayData.count > indexPath.section) {
            ClassifyModel *model = self.leftArrayData[indexPath.row];
            cell.classify_name_Lab.text = model.classify_name;
        }
        cell.contentView.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        return cell;
    }
    else {
        RightTableViewCell *cell = [RightTableViewCell theCircleCellWithTableView:tableView];
//        cell.delegate = self;
        if (_news_id) {
            cell.add_button.hidden = YES; //隐藏加入按钮
            cell.ConstraintRight.constant = 20;
        }
        cell.quit_button.hidden = YES;  //隐藏退出按钮
        if (_choosemodel.classify_id == 6) {
            if (self.rightArrayData && self.rightArrayData.count > indexPath.row ) {
                ClassifyModel *model = self.rightArrayData[indexPath.section];
                NSMutableArray *array = [NSMutableArray arrayWithArray:model.circleList];
                ParentCircleModel *circlemodel = [ParentCircleModel modelWithJSON:array[indexPath.row]];
                if (!circlemodel) {
                    circlemodel = array[indexPath.row];
                }
                cell.circlemodel = circlemodel;
//                @weakify(self)
//                cell.addButonBlock = ^(ParentCircleModel *circlemodel) {
//                    @strongify(self)
//                    [array replaceObjectAtIndex:indexPath.row withObject:circlemodel];
//                    model.circleList = array;
//                    [self.rightArrayData replaceObjectAtIndex:indexPath.section withObject:model];
//                };
            }
        } else {
            if (self.rightArrayData && self.rightArrayData.count > indexPath.row ) {
                ParentCircleModel *circlemodel = self.rightArrayData[indexPath.row];
                cell.circlemodel = circlemodel;
//                @weakify(self)
//                cell.addButonBlock = ^(ParentCircleModel *circlemodel) {
//                    @strongify(self)
//                    [self.rightArrayData replaceObjectAtIndex:indexPath.row withObject:circlemodel];
//                };
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==_leftTableView) {
        _choosemodel = self.leftArrayData[indexPath.row];
//        _choosemodel = model;
        [self getCircleList];
    } else {
        if (self.rightArrayData && self.rightArrayData.count > indexPath.section) {
            if (_choosemodel.classify_id == 6) {
                ClassifyModel *model = self.rightArrayData[indexPath.section];
                NSArray *array = model.circleList;
                if (array && array.count > indexPath.row) {
                    ParentCircleModel *circlemodel = [ParentCircleModel modelWithJSON:array[indexPath.row]];
                    if (!circlemodel) {
                        circlemodel = array[indexPath.row];
                    }
                    if (_news_id) {
                        //更改圈子
                        [self changCircleWithModel:circlemodel];
                    } else {
                        SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
                        VC.circlemodel = circlemodel;
                        [self.navigationController pushViewController:VC animated:YES];
                    }
                }
            } else {
                if (self.rightArrayData && self.rightArrayData.count > indexPath.row) {
                    ParentCircleModel *circlemodel =  [self.rightArrayData objectAtIndex:indexPath.row];
                    if (_news_id) {
                        //更改圈子
                        [self changCircleWithModel:circlemodel];
                    } else {
                        SomeCircleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"SomeCircleViewController"];
                        VC.circlemodel = circlemodel;
                        [self.navigationController pushViewController:VC animated:YES];
                    }
                }
            }
        }
    }
}



- (void)changCircleWithModel:(ParentCircleModel *)model{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"更改圈子"
                                                                   message:[NSString stringWithFormat:@"确定将帖子更改至：%@",model.circle_name]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateTopicIsCircle];
                                                              NSDictionary *parameters = @{
                                                                                           @"news_id":@(_news_id),
                                                                                           @"circle_id":@(model.circle_id)
                                                                                           };
                                                              [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                  NSDictionary *json = responseObject;
                                                                  ZPLog(@"%@\n%@",json,json[@"message"]);
                                                                  BOOL success = [json[@"success"] boolValue];
                                                                  if (success) {
                                                                      if (self.changeCircleBlock) {
                                                                          self.changeCircleBlock(model);
                                                                      }
                                                                      [self.navigationController popViewControllerAnimated:YES];
                                                                  } else {
                                                                      [UIView ShowInfo:json[@"message"] Inview:self.view];
                                                                  }
                                                              } failure:^(NSError *error) {
//                                                                  [UIView ShowInfo:TipWrongMessage Inview:self.view];
                                                              }];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                         }];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - action
- (void)tapGR:(UITapGestureRecognizer *)tapGR {
    //获取section
    NSInteger section = tapGR.view.tag - 2016;
    ClassifyModel *model = self.rightArrayData[section];

    //判断改变bool值
    if (model.show == YES) {
        model.show = NO;
    }else {
        model.show = YES;
    }

    [_rightTableView reloadData];
}


//获取righttableview数据
- (void)getCircleList{
    [self.rightArrayData removeAllObjects];
//    if (!model) {
//        model = _choosemodel;
//    }
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCircleList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"classify_id":@(_choosemodel.classify_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dict in data) {
                if (_choosemodel.classify_id == 6) {
                    ClassifyModel *model = [ClassifyModel modelWithJSON:dict];
                    [self.rightArrayData addObject:model];
                } else {
                    ParentCircleModel *circlemodel = [ParentCircleModel modelWithJSON:dict];
                    [self.rightArrayData addObject:circlemodel];
                }
            }
            [self.rightTableView reloadData];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}






#pragma mark - Getters
- (NSMutableArray *)leftArrayData {
    if (!_leftArrayData) {
        _leftArrayData = [NSMutableArray array];
    }
    return _leftArrayData;
}

- (NSMutableArray *)rightArrayData {
    if (!_rightArrayData) {
        _rightArrayData = [NSMutableArray array];
    }
    return _rightArrayData;
}

- (UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, leftTableWidth, kScreenHeight-NaviH-effectViewH)];
        _leftTableView.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.rowHeight = 74;
        _leftTableView.tableFooterView = [UIView new];
        _leftTableView.showsVerticalScrollIndicator = NO;
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _leftTableView;
}

- (UITableView *)rightTableView {
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(leftTableWidth, 0, rightTableWidth, kScreenHeight-NaviH-effectViewH)];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.estimatedRowHeight = 0;
        _rightTableView.rowHeight = 82;
        _rightTableView.showsVerticalScrollIndicator = NO;
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _rightTableView;
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
