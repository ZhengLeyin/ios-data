//
//  FooterBottomLineView.m
//  mingyu
//
//  Created by MingYu on 2018/12/15.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "FooterBottomLineView.h"

@implementation FooterBottomLineView

/**我是有底线的View*/
+ (instancetype)TheFooterBottomLineView {
    return [[NSBundle mainBundle] loadNibNamed:@"FooterBottomLineView" owner:nil options:nil][0];
}
@end
