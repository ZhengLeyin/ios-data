//
//  LSDHeaderView.h
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BookListModel,CourseModel;

@interface LSDHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (nonatomic, copy) void (^playButtonBlock)(void);

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *playcountLab;

@property (weak, nonatomic) IBOutlet UIButton *collectButton;


@property (nonatomic, strong) BookListModel *booklistmodel;

@property (nonatomic, strong) CourseModel *coursemodel;


+ (instancetype)theTopView;

+ (instancetype)theContentView;


@end
