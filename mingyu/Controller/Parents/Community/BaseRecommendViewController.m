//
//  BaseRecommendViewController.m
//  mingyu
//
//  Created by apple on 2018/11/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "BaseRecommendViewController.h"
#import "ParentArticleModel.h"

@interface BaseRecommendViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *ArticleListArray;
@property (nonatomic, strong) NSMutableArray *laberArray;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, assign) NSInteger sort_id;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, strong) NSMutableArray *allArray;
@property (nonatomic, assign) BOOL isScrollUp;
@property (nonatomic, strong) NothingView *nothingView;


@end

@implementation BaseRecommendViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _allArray = [NSMutableArray array];
    for (int i = 1 ; i < _labModel.article_number+1 ; i++) {
        [_allArray addObject:@(i)];
    }
    [self.view addSubview:self.tableView];
    
    [self refreshTableview];  //刷新
}



//刷新
-(void)refreshTableview{
    if (_tableView.mj_header.state == MJRefreshStateRefreshing) {
        return;
    }
   _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getArticleList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            _isScrollUp = YES;
            [self getArticleList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}


- (NSString *)random_number {
    
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 10; i++) {
        if (_allArray.count > 0) {
            int r = arc4random() % [_allArray count];
            [array addObject:[_allArray objectAtIndex:r]];
            [_allArray removeObjectAtIndex:r];
        }
    }
    ZPLog(@"%@",array);
    NSString *string = [array componentsJoinedByString:@","];
    return string;
}


- (void) getArticleList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetArticleList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"label_id":@(_labModel.label_id),
                                 @"random_number":[self random_number],
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        self.tableView.tableFooterView = nil;
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            if (data.count == 10 || _allArray.count > 0) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            _start += 10;
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                if (_isScrollUp) {
                    [self.ArticleListArray addObject:model];
                } else {
                    [self.ArticleListArray insertObject:model atIndex:0];
                }
            }
            if (self.ArticleListArray && self.ArticleListArray .count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        if (self.ArticleListArray && self.ArticleListArray .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.ArticleListArray && self.ArticleListArray.count > 0) {
        return self.ArticleListArray.count;
    }
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsModel *model = [[NewsModel alloc] init];
    if (self.ArticleListArray && self.ArticleListArray.count > indexPath.row) {
        model = [self.ArticleListArray objectAtIndex:indexPath.row];
    }
    if (model.news_type == 1) {
        ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
        cell.newsmodel = model;
        cell.disLikeBtton.hidden = YES;
        //        cell.dislikeblock = ^(NewsModel *model) {
        //            ParentCicleCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        //            YBPopupMenu *popupMenu = [YBPopupMenu showRelyOnView:cell.disLikeBtton titles:@[@"不感兴趣"] icons:nil menuWidth:118 delegate:self];
        //            popupMenu.fontSize =14;
        //            popupMenu.textColor = [UIColor colorWithHexString:@"575757"];
        //        };
        return cell;
    } else if (model.news_type == 2){
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    } else {
        ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return <#expression#>
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_ArticleListArray.count > indexPath.row) {
        NewsModel *model = [_ArticleListArray objectAtIndex:indexPath.row];
        if (model.news_type == 1) {
            [MobClick event:@"ArticleBaseViewController" label:_labModel.label_name];  //友盟统计
            ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.news_type == 2){
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}

#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenuDidSelectedAtIndex:(NSInteger)index ybPopupMenu:(YBPopupMenu *)ybPopupMenu {
    
    ZPLog(@"dislike");
}


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH-50) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}



- (NSMutableArray *)ArticleListArray{
    if (_ArticleListArray == nil) {
        _ArticleListArray = [NSMutableArray array];
    }
    return _ArticleListArray;
}


- (NSMutableArray *)laberArray{
    if (_laberArray == nil) {
        _laberArray = [NSMutableArray array];
    }
    return _laberArray;
}

- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text =  @"暂无资讯";
        _nothingView.imageView.image = ImageName(@"短视频 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}

@end
