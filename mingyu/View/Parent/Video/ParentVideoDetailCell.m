//
//  ParentVideoDetailCell.m
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoDetailCell.h"

@implementation ParentVideoDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;

}

+(instancetype)theParentVideoDetailCellWithTableView:(UITableView *)tableview{
    static NSString *cellid = @"ParentVideoDetailCell";
    ParentVideoDetailCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentVideoDetailCell" owner:nil options:nil][0];
    }
    return cell;
}


- (void)setModel:(VideoModel *)model{
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.video_img];
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _title_lab.text = model.video_title;
    if (model.fk_user_id) {
        _source_lab.text = model.user_name;
        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.user_head];
        [_source_image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:nil];
    } else {
        _source_lab.text = model.video_source;
        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.source_head];
        [_source_image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:nil];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
