//
//  VideoPlayViewController.m
//  mingyu
//
//  Created by apple on 2018/4/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoPlayViewController.h"
#import "CLPlayerView.h"
#import "ParentVideoDetailBottomView.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"
#import "VideoPlayEndVIew.h"
#import "ParentVideoDetailHeaderView.h"
#import "ParentVideoDetailCell.h"


#import "ZFPlayerControlView.h"
#import "ZFAVPlayerManager.h"

#define PlayerHeight  kScreenWidth*9/16
#define bottomViewHeight   60
#define commentviewHeight  46

@interface VideoPlayViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) BottomCommetView *commentview;
@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;


@property (nonatomic, strong) VideoPlayEndVIew *playendview;

@property (nonatomic, strong) ParentVideoDetailHeaderView *headerView;

@property (nonatomic, strong) SomeCircleFooterView *footerView;

@property (nonatomic, assign) BOOL showMore;

@property (nonatomic, strong) VideoModel *videomodel;

@property (nonatomic, strong) NSMutableArray *commentListArray;
@property (nonatomic, assign) NSInteger comment_number;

@property (nonatomic, strong) NSMutableArray *recommendArray;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;


@end

@implementation VideoPlayViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
//    如果音频在播放 先暂停音频
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [[DFPlayer shareInstance] df_audioPause];
    }
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    [self getShortVideo];
    
}

//获取视频详情
- (void)getShortVideo{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetShortVideo];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"video_id":@(_video_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _videomodel = [VideoModel modelWithJSON:json[@"data"]];
            [self setupSubiew];
            [self setTextViewToolbar];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)setupSubiew{
    
    [self.view addSubview:self.playerView];
    
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    // 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:playerManager containerView:self.playerView];
    self.player.controlView = self.controlView;
    [self.view addSubview:self.playendview];
    @weakify(self)
    self.playendview.playAgainBlock = ^{
        @strongify(self)
        self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
        self.playendview.hidden = YES;
    };
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
    };
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
        if (self.player.isFullScreen) {
            [self.player enterFullScreen:NO animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.player.orientationObserver.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.playendview.hidden = NO;
            });
        } else {
            self.playendview.hidden = NO;
        }
    };

    self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
    [self.controlView showTitle:self.videomodel.video_title coverURLString:self.videomodel.video_img fullScreenMode:ZFFullScreenModeLandscape];
    
    [self.view addSubview:self.tableView];
    
    [self.view addSubview:self.commentview];
    
    typeof(self) weakSelf = self;
    
    if (_videomodel.fk_user_id) {
        _headerView = [ParentVideoDetailHeaderView haveUserVideoDetailHeaderView];
        _headerView.height = 189;
        _headerView.videomodel = _videomodel;
        
        _headerView.showAbstractBlock = ^{
            if (weakSelf.headerView.time_lab.hidden == NO ) {
                weakSelf.tableView.tableHeaderView.height = 189;
                weakSelf.headerView.abstract_lab.hidden = YES;
                weakSelf.headerView.time_lab.hidden = YES;
                [weakSelf.headerView.more_button setImage:ImageName(@"展开") forState:0];
            } else {
                CGFloat height = 190+[self calculateString:_videomodel.video_title Width:kScreenWidth-80 Font:20]+[self calculateString:_videomodel.video_abstract Width:kScreenWidth-30 Font:13];
                weakSelf.tableView.tableHeaderView.height = height;
                
                weakSelf.headerView.abstract_lab.hidden = NO;
                weakSelf.headerView.time_lab.hidden = NO;
                [weakSelf.headerView.more_button setImage:ImageName(@"收起") forState:0];
            }
            [weakSelf.tableView reloadData];
        };
    } else {
        _headerView = [ParentVideoDetailHeaderView noUserVideoDetailHeaderView];
        _headerView.height = 125;
        _headerView.videomodel = _videomodel;
        
        _headerView.showAbstractBlock = ^{
            if (weakSelf.headerView.time_lab.hidden == NO ) {
                weakSelf.tableView.tableHeaderView.height = 125;
                weakSelf.headerView.abstract_lab.hidden = YES;
                weakSelf.headerView.time_lab.hidden = YES;
                [weakSelf.headerView.more_button setImage:ImageName(@"展开") forState:0];
            } else {
                CGFloat height = 140+[self calculateString:_videomodel.video_title Width:kScreenWidth-80 Font:20]+[self calculateString:_videomodel.video_abstract Width:kScreenWidth-30 Font:13];
                weakSelf.tableView.tableHeaderView.height = height;
                weakSelf.headerView.abstract_lab.hidden = NO;
                weakSelf.headerView.time_lab.hidden = NO;
                [weakSelf.headerView.more_button setImage:ImageName(@"收起") forState:0];
            }
            [weakSelf.tableView reloadData];
        };
    }
    
    _headerView.abstract_lab.hidden = YES;
    _headerView.time_lab.hidden = YES;
    
    self.tableView.tableHeaderView = _headerView;

    [self getRecommendVideoList];
    
    [self getCommentList]; //获取评论列表
    
    [self refreshTableview];
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)width Font:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.height;
}



-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        // 清空输入框文字
        [weakSelf.inputToolbar bounceToolbar];
        weakSelf.maskView.hidden = YES;
        
        [weakSelf addCommentwithInputString:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
    
}


-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    //去掉导航栏底部的黑线
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    if (self.player.currentPlayerManager.isPreparedToPlay) {
        [self.player addDeviceOrientationObserver];
        if (self.player.isPauseByEvent) {
            self.player.pauseByEvent = NO;
        }
    }
}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    
    if (self.player.currentPlayerManager.isPreparedToPlay) {
        [self.player removeDeviceOrientationObserver];
        if (self.player.currentPlayerManager.isPlaying) {
            self.player.pauseByEvent = YES;
        }
    }

}



//刷新
-(void)refreshTableview {
    //上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getCommentList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


//获取评论列表
- (void)getCommentList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":[NSString stringWithFormat:@"%ld",_video_id],
                                 @"comment_type":@(video_comment),
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        [self.tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            if (commentList && commentList.count == 10) {
                _lastPage = NO;
            } else{
                _lastPage = YES;
            }
            for (NSDictionary *dic in commentList) {
                CommentModel *commentmodel = [CommentModel modelWithJSON:dic];
                [self.commentListArray addObject:commentmodel];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        
        [self.tableView.mj_footer endRefreshing];
    }];
}


//获取相关推荐
- (void)getRecommendVideoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendVideo];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@"0",
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                VideoModel *model = [VideoModel modelWithJSON:dic];
                [self.recommendArray addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":[NSString stringWithFormat:@"%ld",_video_id]
                          };
    
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(video_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.commentListArray removeAllObjects];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            _start = 0;
            [self getCommentList];   //话题评论
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
#pragma tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_showMore) {
            return _recommendArray.count;
        } else {
            return 2;
        }
    } else {
        if (_commentListArray && _commentListArray.count > 0) {
            return _commentListArray.count;
        }
        return 0;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        CommentListCell *head = [CommentListCell theRecommendHeaderView];
//        head.title_lab.font = FontSize(18);
        return head;
    } else {
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
//            head.comment_number_Lab.text = [NSString stringWithFormat:@"评论(%ld)",_comment_number];
            [head.comment_number_Button setTitle:[NSString stringWithFormat:@"共%ld条评论",_comment_number] forState:0];
            head.arrowimage.hidden = NO;
            head.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
//                VC.targetID = _newsmodel.news_id;
                VC.comment_type = video_comment;
                [self.navigationController pushViewController:VC animated:YES];
            };
        }
        return head;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 60;
    } else {
        return 55;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return self.footerView;
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 50;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ParentVideoDetailCell *detailcell = [ParentVideoDetailCell theParentVideoDetailCellWithTableView:tableView];
        if (_recommendArray && _recommendArray.count > indexPath.row) {
            detailcell.model = [_recommendArray objectAtIndex:indexPath.row];
            detailcell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return detailcell;
    } else {
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        if (_commentListArray && _commentListArray.count > indexPath.row) {
            CommentModel *commenmodel = [_commentListArray objectAtIndex:indexPath.row];
            [commentcell setcellWithModel:commenmodel praiseType:praise_video_comment];
            commentcell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.row != _commentListArray.count-1) {
//                commentcell.line.hidden = NO;
            } else {
//                commentcell.line.hidden = YES;
            }
        }
        return commentcell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 105;
    }
    return _tableView.rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (_recommendArray.count > indexPath.row) {
            VideoModel *model = _recommendArray[indexPath.row];
//            ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
            VideoPlayViewController *VC = [[VideoPlayViewController alloc] init];
            VC.video_id = model.video_id;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)share:(id)sender {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _videomodel.video_title;
    model.descr = _videomodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    model.webpageUrl = _videomodel.share_url;
    model.accusationType = accusation_type_video;
    model.from_id = _video_id;
    model.shareType = share_video;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
    }
    return _controlView;
}

//- (UIView *)containerView {
//    if (!_containerView) {
//        _containerView = [UIView new];
//        _containerView.backgroundColor = [UIColor orangeColor];
//    }
//    return _containerView;
//}


- (VideoPlayEndVIew *)playendview{
    if (!_playendview) {
        _playendview = [[NSBundle mainBundle] loadNibNamed:@"VideoPlayEndVIew" owner:nil options:nil][0];
        _playendview.frame = CGRectMake(0, 0, kScreenWidth, PlayerHeight);
        ShareModel *model = [[ShareModel alloc] init];
        model.titleStr = _videomodel.video_title;
        model.descr = _videomodel.video_abstract;
        model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
        model.webpageUrl = _videomodel.share_url;
        model.shareType = share_video;
        _playendview.sharemodel = model;
        _playendview.hidden = YES;
    }
    return _playendview;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, PlayerHeight, kScreenWidth, kScreenHeight-PlayerHeight-commentviewHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //预计高度为143--即是cell高度
        _tableView.estimatedRowHeight = 150.0f;
        //自适应高度
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


- (BottomCommetView *)commentview{
    if (_commentview == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _commentview = apparray.firstObject;
        _commentview.frame = CGRectMake(0,  kScreenHeight-commentviewHeight, kScreenWidth, commentviewHeight);
        __weak __typeof(self) weakSelf = self;
        _commentview.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
    }
    return _commentview;
}


- (SomeCircleFooterView *)footerView{
    if (!_footerView) {
        if (_footerView == nil) {
            _footerView = [[SomeCircleFooterView alloc] init];
            _footerView.frame = CGRectMake(0, 0, kScreenWidth, 50);
            __weak typeof(self) weakSelf = self;
            _footerView.MoreTopic = ^(BOOL more) {
                _showMore = more;
                [weakSelf.tableView reloadData];
            };
        }
    }
    return _footerView;
}


//- (CLPlayerView *)clplayerView{
//    if (_clplayerView == nil) {
//        _clplayerView = [[CLPlayerView alloc] initWithFrame:_playerView.frame];
//        _clplayerView.videoFillMode = VideoFillModeResizeAspect;
//        _clplayerView.topToolBarHiddenType = TopToolBarHiddenSmall;
//        _clplayerView.fullStatusBarHiddenType = FullStatusBarHiddenFollowToolBar;
//        _clplayerView.strokeColor = [UIColor whiteColor];
//        _clplayerView.progressPlayFinishColor = [UIColor colorWithHexString:@"50D0F4"];
//        _clplayerView.videoTitleString = _videomodel.video_title;
//        _clplayerView.url = [NSURL URLWithString:_videomodel.video_path];
//        [_clplayerView playVideo];
//        [_clplayerView backButton:^(UIButton *button) {
//            ZPLog(@"返回");
//        }];
//
//
//    }
//    return _clplayerView;
//}


- (UIView *)playerView{
    if (!_playerView) {
        _playerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, PlayerHeight)];
    }
    return _playerView;
}


- (NSMutableArray *)commentListArray{
    if (!_commentListArray) {
        _commentListArray = [NSMutableArray array];
    }
    return _commentListArray;
}

- (NSMutableArray *)recommendArray{
    if (!_recommendArray) {
        _recommendArray = [NSMutableArray array];
    }
    return _recommendArray;
}



@end
