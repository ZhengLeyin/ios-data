
//
//  AdvertisementModel.h
//  mingyu
//
//  Created by apple on 2018/10/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdvertisementModel : NSObject


/** 广告id */
@property (nonatomic, assign) NSInteger ad_id;

/** 图片链接 */
@property (nonatomic, copy) NSString *ad_url;

/** 广告类型  1-首页  2-签到页 */
@property (nonatomic, assign) NSInteger ad_type;

/** 目标类型 */ //1- 一键听 2- 育儿必读 3- 推荐 4- 视频 5-圈子 6- 时光印记
@property (nonatomic, assign) NSInteger to_type;

@property (nonatomic, assign) NSInteger menu_id;

@property (nonatomic, assign) NSInteger from_id;




@end
