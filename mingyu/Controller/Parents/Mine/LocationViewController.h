//
//  LocationViewController.h
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationViewController : UIViewController

@end


@class AddressModel;
@interface LocationCell :UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name_lab;

@property (weak, nonatomic) IBOutlet UILabel *phone_lab;

@property (weak, nonatomic) IBOutlet UILabel *address_lab;

@property (weak, nonatomic) IBOutlet UIButton *default_btn;

@property (nonatomic, strong) AddressModel *addressmodel;

@property (nonatomic, copy) void (^refreshTableview)(NSInteger type);   //删除：0 ，设置默认：1






@end
