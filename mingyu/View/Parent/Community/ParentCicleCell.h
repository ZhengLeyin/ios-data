//
//  ParentCicleCell.h
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel;

typedef void(^disLikeButtonBlock)(NewsModel *model);

@interface ParentCicleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *user_head_Image;

@property (weak, nonatomic) IBOutlet UILabel *user_name_Lab;

@property (weak, nonatomic) IBOutlet UILabel *topic_title_Lab;

@property (weak, nonatomic) IBOutlet UIImageView *topic_head_Image;

@property (weak, nonatomic) IBOutlet UIButton *disLikeBtton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TitleLabConstraintRight;


@property (weak, nonatomic) IBOutlet UILabel *header_label;



@property (nonatomic, strong) NewsModel *newsmodel;

@property (nonatomic, copy) disLikeButtonBlock dislikeblock;


+ (instancetype)theParentCicleCellwithtableView:(UITableView *)tableView;


+(instancetype)theHeaderView;


/**
 搜索
 */
- (void)showSearch;


@end
