//
//  VideoInteractCell.m
//  mingyu
//
//  Created by apple on 2018/4/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoInteractCell.h"

@implementation VideoInteractCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_iconImage.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_iconImage.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = _iconImage.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    _iconImage.layer.mask = maskLayer;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
//    self.backgroundColor = [UIColor yellowColor];
    
}

- (void)setCommenmodel:(CommentModel *)commenmodel{
    _commenmodel = commenmodel;
    [_iconImage sd_setImageWithURL:[NSURL URLWithString:commenmodel.comment_head] placeholderImage:ImageName(@"placeholderImage")];
    _userNameLab.text = commenmodel.comment_name;
    _timeLab.text = commenmodel.comment_time_status;
//    [NSString compareTimeForNow:commenmodel.comment_time];
//    commenmodel.comment_time;
    _contentLab.text = commenmodel.comment_content;
    
    if (commenmodel.click_praise_true == 0) {
        [_zanButton setImage:ImageName(@"评论赞") forState:UIControlStateNormal];
        [_zanButton setTitle:@" 赞 " forState:UIControlStateNormal];
    } else{
        [_zanButton setImage:ImageName(@"评论已赞") forState:UIControlStateNormal];
        [_zanButton setTitle:[NSString stringWithFormat:@" %ld ",commenmodel.praise_number] forState:UIControlStateNormal];
    }

}


- (IBAction)zanButton:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(zanButtonClicked:button:)]) {
        [self.delegate zanButtonClicked:_commenmodel button:sender];
    }
}



@end
