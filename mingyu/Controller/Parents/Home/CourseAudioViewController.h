//
//  CourseAudioViewController.h
//  mingyu
//
//  Created by apple on 2018/6/5.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LWXDetailController.h"

@class CourseModel;

@interface CourseAudioViewController : UIViewController

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CourseModel *coursemodel;


@end
