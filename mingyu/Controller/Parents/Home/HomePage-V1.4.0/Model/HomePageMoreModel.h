//
//  HomePageMoreModel.h
//  mingyu
//
//  Created by MingYu on 2018/12/26.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class HomePageMoreData,MoreSubmenumessagelist,MoreObjectlist;
@interface HomePageMoreModel : NSObject


/** <#description#> */
@property (nonatomic, assign) BOOL success;


/** <#description#> */
@property (nonatomic, copy) NSString *message;


/** <#description#> */
@property (nonatomic, strong) HomePageMoreData *data;

@end
@interface HomePageMoreData : NSObject


/** <#description#> */
@property (nonatomic, assign) NSInteger menu_id;


/** <#description#> */
@property (nonatomic, copy) NSString *menu_name;


/** <#description#> */
@property (nonatomic, strong) NSArray *subMenuMessageList;

@end

@interface MoreSubmenumessagelist : NSObject


/** <#description#> */
@property (nonatomic, assign) NSInteger type;


/** <#description#> */
@property (nonatomic, strong) NSArray *objectList;

@end

@interface MoreObjectlist : NSObject
@property (nonatomic, assign) NSInteger type;


/** <#description#> */
@property (nonatomic, copy) NSString *video_img;


/** <#description#> */
@property (nonatomic, assign) NSInteger video_id;


/** <#description#> */
@property (nonatomic, copy) NSString *video_title;

/** 新闻ID */
@property (nonatomic, assign) NSInteger news_id;

/** 用户ID */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 来源ID */
@property (nonatomic, assign) NSInteger fk_from_id;

/** 标题 */
@property (nonatomic, copy) NSString *news_title;

/** 副标题 */
@property (nonatomic, copy) NSString *news_subtitle;

/** 新闻封面图片 */
@property (nonatomic, copy) NSString *news_head;

/** 内容 */
@property (nonatomic, copy) NSString *news_content;

/** 新闻类型 1:资讯 2:育儿必读 3:帖子 */
@property (nonatomic, assign) NSInteger news_type;

/** 浏览量 */
@property (nonatomic, assign) NSInteger page_view;

/** 创建时间 */
@property (nonatomic, copy) NSString *create_time;

/** 发布时间状态 */
@property (nonatomic, copy) NSString *time_status;

/** 收藏ID */
@property (nonatomic, assign) NSInteger is_collect_true;

/** 点赞量 */
@property (nonatomic, assign) NSInteger praise_number;

/** 收藏量 */
@property (nonatomic, assign) NSInteger collect_number;

/** 是否点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 分享链接 */
@property (nonatomic, copy) NSString *share_url;

/** 用户名字 */
@property (nonatomic, copy) NSString *user_name;

/** 用户头像 */
@property (nonatomic, copy) NSString *user_head;

/** 关注状态 */
@property (nonatomic, assign) NSInteger follow_status;


/** 置顶ID */
@property (nonatomic, assign) NSInteger is_top_true;

/** 圈子名称 */
@property (nonatomic, copy) NSString *circle_name;

/** 所属圈子ID */
@property (nonatomic, assign) NSInteger fk_circle_id;

/** 新闻来源 */
@property (nonatomic, copy) NSString *article_source;

/** 新闻来源图标 */
@property (nonatomic, copy) NSString *source_head;


/** 课程ID */
@property (nonatomic, assign) NSInteger fk_course_id;


/** 视频简介 */
@property (nonatomic, copy) NSString *video_abstract;


/** 视频路径 */
@property (nonatomic, copy) NSString *video_path;

/** 视频价格 */
@property (nonatomic, assign) CGFloat video_price;

/** 视频介绍 */
@property (nonatomic, copy) NSString *video_introduce;

/** 视频收获 */
@property (nonatomic, copy) NSString *video_harvest;

/** 课程排序 */
@property (nonatomic, assign) NSInteger video_sort;

/** 来源 */
@property (nonatomic, copy) NSString *video_source;


/** 播放人数 */
@property (nonatomic, assign) NSInteger play_number;

/** 播放次数 */
@property (nonatomic, assign) NSInteger play_count;

/** 时长 */
@property (nonatomic, copy) NSString *time_length;



/** 被关注数 */
@property (nonatomic, assign) NSInteger follow_number;

/** 已播放时长 */
@property (nonatomic, assign) NSInteger seekTime;


/**视频+音频 播放地址*/
@property (nonatomic, strong) NSString *section_path;

/**视频+音频 图片地址*/
@property (nonatomic, strong) NSString *section_img;

/** 章节标题 */
@property (nonatomic, strong) NSString *section_title;

/** 试听 */
@property (nonatomic, assign) NSUInteger is_free;

/** 选择的视频 */
//@property (nonatomic, assign) BOOL selectvideo;

@property (nonatomic, assign) NSInteger subject_id;

@property (nonatomic, copy) NSString *subject_title;


@property (nonatomic, copy) NSString *subject_image;

@property (nonatomic, assign) NSInteger fk_classify_id;

@property (nonatomic, assign) NSInteger ident_type;

/** 一级目录id */
@property (nonatomic, assign) NSInteger menu_id;

/** 获取和年龄相关的组 */
@property (nonatomic, assign) NSInteger age_group;

/** 一级目录名称 */
@property (nonatomic, copy) NSString *menu_name;

/** 证明和年龄有关? */
@property (nonatomic, assign) NSInteger is_ban;

/** 课程ID */
@property (nonatomic, assign) NSInteger course_id;

///**  用户ID  */
//@property (nonatomic, assign) NSInteger fk_user_id;

/** 课程标题 */
@property (nonatomic, copy) NSString *course_title;

/**  课程类型  1.视频课    2.音频课 */
@property (nonatomic, assign) NSInteger course_type;

/**  课程简介  */
@property (nonatomic, copy) NSString *course_abstract;

/**  课程图片  */
@property (nonatomic, copy) NSString *course_img;

/** 课程分类 */
@property (nonatomic, assign) NSInteger course_sort;

///** 创建时间 */
//@property (nonatomic, copy) NSString *create_time;

/** 你的收获*/
@property (nonatomic, copy) NSString *course_harvest;

/** 课程介绍  */
@property (nonatomic, copy) NSString *course_introduce;

/** 作者名字 */
@property (nonatomic, copy) NSString *real_name;

/**组织 */
@property (nonatomic, copy) NSString *teacher_organize;


///** 头像 */
//@property (nonatomic, strong) NSString *user_head;

/** 职位 */
@property (nonatomic, copy) NSString *teacher_position;

/** 老师名字 */
@property (nonatomic, copy) NSString *teacher_name;

/** 老师ID */
@property (nonatomic, assign) NSInteger teacher_id;

/**简介 */
@property (nonatomic, copy) NSString *teacher_profile;

///**  点赞数 */
//@property (nonatomic, assign) NSInteger praise_number;
//
///**  收藏数 */
//@property (nonatomic, assign) NSInteger collect_number;
//
///**  已收藏 */
//@property (nonatomic, assign) NSInteger is_collect_true;
//
///**  已点赞 */
//@property (nonatomic, assign) NSInteger click_praise_true;
//
///** 总播放数 */
//@property (nonatomic, assign) NSInteger play_count;

/** 折扣 */
@property (nonatomic, assign) CGFloat discount;

/** 最大金币折扣 */
@property (nonatomic, assign) CGFloat max_integral_scale;

/** 原价 */
@property (nonatomic, assign) CGFloat original_price;

/** 现价 */
@property (nonatomic, assign) CGFloat present_price;

/** 购买数量 */
@property (nonatomic, assign) NSUInteger hold_num;

/** e更新几期 */
@property (nonatomic, assign) NSInteger section_num;

@property (nonatomic, assign) NSInteger directoryNumber;

///**视频+音频 播放地址*/
//@property (nonatomic, strong) NSString *section_path;
//
///**视频+音频 图片地址*/
//@property (nonatomic, strong) NSString *section_img;

/** 是否已购买*/
@property (nonatomic, assign) BOOL is_hold;

///** 关注 */
//@property (nonatomic, assign) NSUInteger follow_status;
//
///** 分享链接 */
//@property (nonatomic, copy) NSString *share_url;

/** 已购课程-购买时间 */
@property (nonatomic, copy) NSString *time_tatus;

/** 已购课程-月份 */
@property (nonatomic, assign) NSInteger month;

/** 已购课程-年份 */
@property (nonatomic, assign) NSInteger year;

/**特权课 ---该课程判断*/
@property (nonatomic, assign) NSInteger course_prerogative;
/**特权课 ---该用户是不是名校堂用户*/
@property (nonatomic, assign) NSUInteger user_prerogative;

@end


NS_ASSUME_NONNULL_END
