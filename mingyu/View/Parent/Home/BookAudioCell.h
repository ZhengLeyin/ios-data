//
//  BookAudioCell.h
//  mingyu
//
//  Created by apple on 2018/5/19.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AudioModel;

@interface BookAudioCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *state_imaeg;

@property (weak, nonatomic) IBOutlet UILabel *audio_title_lab;

@property (weak, nonatomic) IBOutlet UILabel *create_time_lab;

@property (weak, nonatomic) IBOutlet UIButton *play_count_button;

@property (weak, nonatomic) IBOutlet UIButton *time_length_button;

@property (weak, nonatomic) IBOutlet UILabel *play_history_lab;

@property (weak, nonatomic) IBOutlet UIButton *downLoad_button;

@property (weak, nonatomic) IBOutlet UILabel *audio_sort_lab;

@property (weak, nonatomic) IBOutlet UILabel *try_lab;

@property (nonatomic, assign) NSInteger totleNum;
@property (nonatomic, strong) AudioModel *audiomodel;


+ (instancetype)theCellWithTableView:(UITableView *)tableView;

@end
