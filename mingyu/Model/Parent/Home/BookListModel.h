//
//  BookListModel.h
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookListModel : NSObject


/** 听书ID */
@property (nonatomic, assign) NSInteger book_id;

/** 朗诵者 */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 书标题 */
@property (nonatomic, copy) NSString *book_title;

/** 书图片 */
@property (nonatomic, copy) NSString *book_head;

/**书作者 */
@property (nonatomic, copy) NSString *book_author;

/**作者简介 */
@property (nonatomic, copy) NSString *author_abstract;

/**朗诵者评论 */
@property (nonatomic, copy) NSString *author_comment;

/**内容简介 */
@property (nonatomic, copy) NSString *content_abstract;

/**听书类型 */
@property (nonatomic, assign) NSInteger book_type;

/** 原价 */
@property (nonatomic, assign) CGFloat original_price;

/**折扣*/
@property (nonatomic, assign) CGFloat discount;

/** 最高金币抵扣百分比 */
@property (nonatomic, assign) CGFloat max_gold_scale;

/** 现价 */
@property (nonatomic, assign) CGFloat present_price;

/**朗诵者*/
@property (nonatomic, copy) NSString *user_name;

/**朗诵者头像*/
@property (nonatomic, copy) NSString *user_head;

/**用户粉丝数*/
@property (nonatomic, assign) NSInteger followUserNumber;

/**播放次数 */
@property (nonatomic, assign) NSInteger play_count;

/**点赞数*/
@property (nonatomic, assign) NSInteger praise_number;

/**收藏数 */
@property (nonatomic, assign) NSInteger collect_number;

/**已收藏*/
@property (nonatomic, assign) NSInteger is_collect_true;

/**已点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 总章节数 */
@property (nonatomic, assign) NSInteger directoryNumber;


@end
