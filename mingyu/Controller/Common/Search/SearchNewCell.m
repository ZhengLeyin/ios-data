//
//  SearchNewCell.m
//  mingyu
//
//  Created by MingYu on 2018/11/9.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "SearchNewCell.h"

@implementation SearchNewCell

+(instancetype)theSearchNewCellWithTableView:(UITableView *)tableview {
    static NSString *cellid = @"SearchNewCell";
    SearchNewCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SearchNewCell" owner:nil options:nil][0];
    }
    return cell;
}

#pragma mark - deleteButton click
//- (void)deleteButtonClick:(UIButton *)deleteButton
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(searchNewCell:deleteHistoryButtonClick:)])
//    {
//        [self.delegate searchNewCell:self deleteHistoryButtonClick:deleteButton];
//    }
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    //[_deleteBtn addTarget:self action:@selector(deleteButtonClick:) forControlEvents:(UIControlEventTouchDragInside)];
    [_deleteBtn setEnlargeEdge:20];  //增加点击区域
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
