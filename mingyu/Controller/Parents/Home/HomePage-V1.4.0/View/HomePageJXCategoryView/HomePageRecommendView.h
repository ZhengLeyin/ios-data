//
//  HomePageRecommendView.h
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomePageRecommendView : UIView<UITableViewDelegate,UITableViewDataSource,JXPagerViewListViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSUInteger menu_id;
@end

NS_ASSUME_NONNULL_END
