//
//  GetGoldController.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class QianDaoTaskModel;

@interface GetGoldController : UIViewController

@end



@interface GetGoldCell : UITableViewCell

// 任务名称
@property (nonatomic, strong) UILabel *titleLab;
// 奖励数
@property (nonatomic, strong) UILabel *goldLabel;
// 状态
@property (nonatomic, strong) UILabel *stateLabel;


@property (nonatomic, strong) QianDaoTaskModel *model;


@end
