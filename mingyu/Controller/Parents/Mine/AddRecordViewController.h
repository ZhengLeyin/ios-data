//
//  AddRecordViewController.h
//  mingyu
//
//  Created by apple on 2018/8/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DiaryModel;

@interface AddRecordViewController : UIViewController

@property (nonatomic, strong) DiaryModel *diarymodel;

@end
