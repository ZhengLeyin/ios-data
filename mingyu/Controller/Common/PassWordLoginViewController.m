//
//  PassWordLoginViewController.m
//  TZWY
//
//  Created by apple on 2018/3/29.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "PassWordLoginViewController.h"
#import "SetPasswordViewController.h"
#import "BoundPhoneViewController.h"
#import <UMShare/UMShare.h>
#import <UMPush/UMessage.h>

@interface PassWordLoginViewController ()<UITextFieldDelegate>

{
    NSString *PhoneTFContent;
    UITextRange *PhoneTFSelection;
}
@property (weak, nonatomic) IBOutlet UILabel *WellcomeLab;

@property (weak, nonatomic) IBOutlet UITextField *PhoneTF;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTF;

@property (weak, nonatomic) IBOutlet UIButton *SecureButton;

@property (weak, nonatomic) IBOutlet UIView *Line1;
@property (weak, nonatomic) IBOutlet UIView *Line2;

@property (weak, nonatomic) IBOutlet UIButton *LoginButton;



//@property (nonatomic, assign) BOOL Secure;

@end

@implementation PassWordLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *tenDigitNumber = [userDefault objectForKey:KUDuser_phone];
    tenDigitNumber = [tenDigitNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{4})(\\d{4})" withString:@"$1 $2 $3" options:NSRegularExpressionSearch range:NSMakeRange(0, [tenDigitNumber length])];
    _PhoneTF.text = tenDigitNumber;
    _PhoneTF.tintColor = _PasswordTF.tintColor = [UIColor colorWithHexString:@"50D0F4"];
    
    _LoginButton.userInteractionEnabled = NO;

    // 给TextField注册内容变动通知,添加监听方法
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldEditingChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [_PhoneTF addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];

    if(IS_IPHONE) {
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]) {
            _wechatLogin.hidden = YES;
        }
    } else {
        _wechatLogin.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

// TextField监听方法
- (void)textFieldEditingChanged:(UITextField *)textField {
    
    if (_PhoneTF.text.length == 13 && _PasswordTF.text.length > 0) {
        [_LoginButton setBackgroundColor:[UIColor colorWithHexString:@"50D0F4"]];
        _LoginButton.userInteractionEnabled = YES;
    } else {
        [_LoginButton setBackgroundColor:[UIColor colorWithHexString:@"C6C6C6"]];
        _LoginButton.userInteractionEnabled = NO;
    }
    if (textField == _PhoneTF) {
        //限制手机账号长度（有两个空格）
        if (textField.text.length > 13) {
            textField.text = [textField.text substringToIndex:13];
        }
        
        NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
        
        NSString *currentStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *preStr = [PhoneTFContent stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        //正在执行删除操作时为0，否则为1
        char editFlag = 0;
        if (currentStr.length <= preStr.length) {
            editFlag = 0;
        }
        else {
            editFlag = 1;
        }
        
        NSMutableString *tempStr = [NSMutableString new];
        
        int spaceCount = 0;
        if (currentStr.length < 3 && currentStr.length > -1) {
            spaceCount = 0;
        }else if (currentStr.length < 7 && currentStr.length > 2) {
            spaceCount = 1;
        }else if (currentStr.length < 12 && currentStr.length > 6) {
            spaceCount = 2;
        }
        
        for (int i = 0; i < spaceCount; i++) {
            if (i == 0) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(0, 3)], @" "];
            }else if (i == 1) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(3, 4)], @" "];
            }else if (i == 2) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(7, 4)], @" "];
            }
        }
        
        if (currentStr.length == 11) {
            [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(7, 4)], @" "];
        }
        if (currentStr.length < 4) {
            [tempStr appendString:[currentStr substringWithRange:NSMakeRange(currentStr.length - currentStr.length % 3, currentStr.length % 3)]];
        }else if(currentStr.length > 3 && currentStr.length <12) {
            NSString *str = [currentStr substringFromIndex:3];
            [tempStr appendString:[str substringWithRange:NSMakeRange(str.length - str.length % 4, str.length % 4)]];
            if (currentStr.length == 11) {
                [tempStr deleteCharactersInRange:NSMakeRange(13, 1)];
            }
        }
        textField.text = tempStr;
        // 当前光标的偏移位置
        NSUInteger curTargetCursorPosition = targetCursorPosition;
        
        if (editFlag == 0) {
            //删除
            if (targetCursorPosition == 9 || targetCursorPosition == 4) {
                curTargetCursorPosition = targetCursorPosition - 1;
            }
        }else {
            //添加
            if (currentStr.length == 8 || currentStr.length == 4) {
                curTargetCursorPosition = targetCursorPosition + 1;
            }
        }
        UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:curTargetCursorPosition];
        [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition toPosition :targetPosition]];
        
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    PhoneTFContent = textField.text;
    PhoneTFSelection = textField.selectedTextRange;
    
    return YES;
}



#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _PhoneTF) {
        _Line1.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
        _Line2.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    } else if (textField == _PasswordTF){
        _Line1.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        _Line2.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    _Line1.backgroundColor = _Line2.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    
}


//明文密码
- (IBAction)SecureButton:(id)sender {
    
    if (_PasswordTF.secureTextEntry) {
        [_SecureButton setImage:ImageName(@"see") forState:UIControlStateNormal];
        _PasswordTF.secureTextEntry = NO;
    } else{
        [_SecureButton setImage:ImageName(@"close") forState:UIControlStateNormal];
        _PasswordTF.secureTextEntry = YES;
    }
}

//登录
- (IBAction)LoginButton:(id)sender {
    _LoginButton.userInteractionEnabled = NO;
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KLoginByPassword];
    NSMutableDictionary *param =[NSMutableDictionary dictionary];
    param[@"user_phone"] = [_PhoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    param[@"user_password"] = [NSString MD5:_PasswordTF.text];
//    [_PasswordTF.text md5HexDigest];
    param[@"device_id"] = [userDefault objectForKey:KUDdevice_id];

    [HttpRequest postWithURLString:URL parameters:param viewcontroller:self success:^(id responseObject) {
        _LoginButton.userInteractionEnabled = NO;
        NSDictionary *json = (NSDictionary *)responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            UserModel *user = [UserModel modelWithJSON:data];
            [userDefault setObject:user.token forKey:KUDtoken];
            [userDefault setObject:user.user_name forKey:KUDuser_name];
            [userDefault setObject:user.user_phone forKey:KUDuser_phone];
            [userDefault setObject:user.user_password forKey:KUDuser_password];
            [userDefault setInteger:user.user_id forKey:KUDuser_id];
            [userDefault setObject:user.user_head forKey:KUDuser_head];
            [userDefault setInteger:user.follow_number forKey:KUDfollow_number];
            [userDefault setObject:user.baby_head forKey:KUDbabyhead];
            [userDefault setObject:user.user_birthday forKey:KUDuser_birthday];
            [userDefault setBool:YES forKey:KUDhasLogin];
            [userDefault synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationBirthdayChange object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationUserLogin object:nil userInfo:nil];

            NSString *alias = [NSString stringWithFormat:@"mingyu%@",USER_ID];
            ZPLog(@"%@",alias);
            [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
            [self dismissViewControllerAnimated:YES completion:nil];

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
            _LoginButton.userInteractionEnabled = YES;
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        _LoginButton.userInteractionEnabled = YES;
    }];
    
}

//设置密码
- (IBAction)SetPassword:(id)sender {
    
    SetPasswordViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"SetPasswordViewController"];
    VC.title = @"设置密码";
    [self.navigationController pushViewController:VC animated:YES];
}


//忘记密码
- (IBAction)FindPassword:(id)sender {
    SetPasswordViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"SetPasswordViewController"];
    VC.title = @"找回密码";
    [self.navigationController pushViewController:VC animated:YES];
}



//微信登录
- (IBAction)WeiXinLogin:(id)sender {
    
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            
        } else {
            UMSocialUserInfoResponse *resp = result;
            // 授权信息
            ZPLog(@"Wechat uid: %@", resp.uid);
            ZPLog(@"Wechat openid: %@", resp.openid);
            ZPLog(@"Wechat accessToken: %@", resp.accessToken);
            ZPLog(@"Wechat refreshToken: %@", resp.refreshToken);
            ZPLog(@"Wechat expiration: %@", resp.expiration);
            // 用户信息
            ZPLog(@"Wechat name: %@", resp.name);
            ZPLog(@"Wechat iconurl: %@", resp.iconurl);
            ZPLog(@"Wechat gender: %@", resp.gender);
            // 第三方平台SDK源数据
            ZPLog(@"Wechat originalResponse: %@", resp.originalResponse);
            if (resp.openid) {
                [self WeChatLogin:resp];
            }
        }
    }];
}


- (void)WeChatLogin:(UMSocialUserInfoResponse *)resp{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KCheckUserOpenId];
    NSDictionary *parameters = @{
                                 @"open_id" : resp.openid,
                                 @"device_id" : [userDefault objectForKey:KUDdevice_id],
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            if (data) {
                UserModel *user = [UserModel modelWithJSON:data];
                [userDefault setObject:user.token forKey:KUDtoken];
                [userDefault setObject:user.user_name forKey:KUDuser_name];
                [userDefault setObject:user.user_phone forKey:KUDuser_phone];
                [userDefault setObject:user.user_password forKey:KUDuser_password];
                [userDefault setInteger:user.user_id forKey:KUDuser_id];
                [userDefault setObject:user.user_head forKey:KUDuser_head];
                [userDefault setInteger:user.follow_number forKey:KUDfollow_number];
                [userDefault setObject:user.baby_head forKey:KUDbabyhead];
                [userDefault setObject:user.user_birthday forKey:KUDuser_birthday];
                [userDefault setBool:YES forKey:KUDhasLogin];
                [userDefault synchronize];
                NSString *alias = [NSString stringWithFormat:@"mingyu%@",USER_ID];
                ZPLog(@"%@",alias);
                [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationBirthdayChange object:nil userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationUserLogin object:nil userInfo:nil];

                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                BoundPhoneViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"BoundPhoneViewController"];
                VC.resp = resp;
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        
    }];
}

//验证码登录
- (IBAction)CodeLogin:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

//返回
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//去首页
- (IBAction)HomeViewButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}


@end
