//
//  GoldDetailCell.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IntegralDetailModel;

@interface GoldDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *action_explain_lab;

@property (weak, nonatomic) IBOutlet UILabel *action_score_lab;

@property (weak, nonatomic) IBOutlet UILabel *action_time_lab;


@property (nonatomic, strong) IntegralDetailModel *detailmodel;


@end
