//
//  Tool.m
//  RichTextEditorDemo
//
//  Created by za4tech on 2017/12/15.
//  Copyright © 2017年 Junior. All rights reserved.
//

#import "Tool.h"

@implementation Tool
//+(NSString *)makeHtmlString:(NSMutableArray *)imageUrlArr desArr:(NSMutableArray *)desArr contentStr:(NSString *)contentStr{
//    NSString * htmlStr = @"";
//    //组装图片标签
//    NSMutableArray * imgTagArr = [NSMutableArray array];
//    for (int i = 0; i < imageUrlArr.count; i ++) {
//        NSString * urlStr = imageUrlArr[i];
//        NSString * imgTag  = [@"<img>\n" stringByAppendingString:[[@"<url>" stringByAppendingString:urlStr] stringByAppendingString:@"</url>\n"]];
//        for (int j = 0; j < desArr.count; j ++) {
//            if (i == j) {
//                NSString * desStr = desArr[j];
//                imgTag = [[imgTag stringByAppendingString:[[@"<des>" stringByAppendingString:desStr] stringByAppendingString:@"</des>\n"]] stringByAppendingString:@"</img>\n"];
//            }
//        }
//        [imgTagArr addObject:imgTag];
//    }
//
//    //组装文字标签 和图片标签
//    NSArray * textArr = [contentStr componentsSeparatedByString:@"<我是图片>"];
//    for (int i= 0; i < textArr.count; i ++) {
//        NSString * pTag = [[@"<p>" stringByAppendingString:textArr[i]]stringByAppendingString:@"</p>\n"];
//        htmlStr = [NSString stringWithFormat:@"%@%@",htmlStr,pTag];
//        for (int j = 0; j < imgTagArr.count; j ++) {
//            if (i == j) {
//                htmlStr = [NSString stringWithFormat:@"%@%@",htmlStr,imgTagArr[j]];
//            }
//        }
//    }
//
//    htmlStr = [htmlStr stringByReplacingOccurrencesOfString:@"<p>\n</p>" withString:@""];
//    htmlStr = [htmlStr stringByReplacingOccurrencesOfString:@"<p></p>" withString:@""];
//    ZPLog(@"这是转化后的html格式的图文内容----%@",htmlStr);
//    return htmlStr;
//}


+(NSString *)makeHtmlString:(NSMutableArray *)imageUrlArr contentStr:(NSString *)contentStr{
    NSString *htmlStr = @"";
    
    NSMutableArray * imgTagArr = [NSMutableArray array];
    for (int i = 0; i < imageUrlArr.count; i ++) {
        NSString * urlStr = imageUrlArr[i];
        NSString *imgTag = [@"![ ](" stringByAppendingString:[urlStr stringByAppendingString:@")\n" ]];
        [imgTagArr addObject:imgTag];
    }
    
    NSArray * textArr = [contentStr componentsSeparatedByString:@"<我是图片>"];
    for (int i= 0; i < textArr.count; i ++) {
        NSString *pTag = textArr[i];
        htmlStr = [NSString stringWithFormat:@"%@%@",htmlStr,pTag];
        for (int j = 0; j < imgTagArr.count; j++) {
            if (i == j) {
                htmlStr = [NSString stringWithFormat:@"%@%@",htmlStr,imgTagArr[j]];
            }
        }
    }
    
    return htmlStr;
}


+ (NSMutableAttributedString *)makeAttstring:(NSArray *)image contentAtr:(NSString *)content{
    NSMutableAttributedString *attstr = [[NSMutableAttributedString alloc] init];
    UIFont *font = [UIFont systemFontOfSize:15];
    NSArray *array = [content componentsSeparatedByString:@"<我是图片>"];
    for (int i = 0; i < array.count; i++) {        
        [attstr appendString:array[i]];
        if (i == array.count-1) {
            return attstr;
        }
        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,image[i]];
        YYImage *img = [YYImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]] scale:1.0];
        if (!img) {
            img = [YYImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://img.mingyu100.com/404.png"]] scale:1.0];
        }
        img.preloadAllAnimatedImageFrames = YES;
        
        YYAnimatedImageView *imageView = [[YYAnimatedImageView alloc] initWithImage:img];
        imageView.autoPlayAnimatedImage = NO;
        imageView.clipsToBounds = YES;
        [imageView startAnimating];
        CGSize size = imageView.size;
        CGFloat textViewWidth = kScreenWidth - 32.0;
        size = CGSizeMake(textViewWidth, size.height * textViewWidth / size.width);
        NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:imageView contentMode:UIViewContentModeScaleAspectFit attachmentSize:size alignToFont:font alignment:YYTextVerticalAlignmentCenter];

        //绑定图片和述输入框
        [attachText setTextBinding:[YYTextBinding bindingWithDeleteConfirm:NO] range:attachText.rangeOfAll];
    
        [attstr appendAttributedString:attachText];

    }
    
    return attstr;
}



@end
