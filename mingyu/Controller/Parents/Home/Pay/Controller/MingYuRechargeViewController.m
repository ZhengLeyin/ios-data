//
//  MingYuRechargeViewController.m
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "MingYuRechargeViewController.h"
/**育币充值View*/
#import "RechargeView.h"
#import "MingYuRechargeModel.h"
#import <StoreKit/StoreKit.h>
#import <IAPShare.h>
#import <NSString+Base64.h>
#import "OrderModel.h"

#define WeakSelf(weakSelf)      __weak __typeof(&*self)    weakSelf  = self;

@interface MingYuRechargeViewController ()

/** 育币支付View */
@property (nonatomic,strong)  RechargeView *rechargeView;
/** 育币支付View */
@property (nonatomic,strong)  NSMutableArray *arrayData;
/** 选中充值项 */
@property (nonatomic, strong) MingYuRechargeModel *model;
/**后台订单 */
@property (nonatomic, strong) OrderModel *ordermodel;


@end

@implementation MingYuRechargeViewController

-(NSMutableArray *)arrayData {
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setMinimumDismissTimeInterval:2];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    UIImage *rightImage = [[UIImage imageNamed:@"Combined Shape"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemBack)];
    
    self.title = @"育币充值";
    self.view.backgroundColor = [UIColor whiteColor];
    _rechargeView = [[[NSBundle mainBundle] loadNibNamed:@"RechargeView"owner:self options:nil] lastObject];
    _rechargeView.rechargeButton.userInteractionEnabled = NO;
    _rechargeView.rechargeButton.backgroundColor = [UIColor colorWithHexString:@"#E2E1E2"];
    WeakSelf(weakSelf);
    [[_rechargeView.rechargeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [weakSelf recharge];
    }];
    _rechargeView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - effectViewH);
    [self.view addSubview:self.rechargeView];

    [self GetAmountOptions];

#pragma mark is_shop_id 回调
    [self returnValueBlock];
    
}

#pragma mark 获取充值金额
- (void)GetAmountOptions {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAmountOptions];
    NSDictionary *parameters = @{
                                 @"type":@"2"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _rechargeView.rechargeButton.userInteractionEnabled  = YES;
            for (NSDictionary *dic in responseObject[@"data"]) {
                MingYuRechargeModel *model = [MingYuRechargeModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            [_rechargeView array:self.arrayData type:_type];
            
            if (_type == 0) {
                [_rechargeView.topLab setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",self.needToBuyMoney]]];
            }else {
                _rechargeView.topLab.hidden = YES;
            }
        } else {
            _rechargeView.rechargeButton.userInteractionEnabled  = YES;
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        _rechargeView.rechargeButton.userInteractionEnabled  = YES;
        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(NSMutableAttributedString *)attStrlabelText:(NSString *)labelText {
    NSString *string = [NSString stringWithFormat:@"当前余额不足，还差%@个育币",labelText];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:string];
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#E75858"],
                            NSFontAttributeName:FontSize(15.0)} range:NSMakeRange(9,string.length - 12)];//添加属性
    return attStr;
}

-(void)leftBarButtonItemBack {
    if (self.BackBlock) {
        self.BackBlock();
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 回调Block
-(void)returnValueBlock {
    WeakSelf(weakSelf)
    _rechargeView.returnValueBlock = ^(MingYuRechargeModel * _Nonnull model) {
       weakSelf.rechargeView.rechargeButton.backgroundColor = [UIColor colorWithHexString:@"#F4744E"];
        weakSelf.model = model;
        NSLog(@"%ld --- %@",model.money_num,model.ios_shop_id);
//        _ios_shop_id = model.ios_shop_id;
    };
}


#pragma mark 立即充值
- (void)recharge{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:AppStoreInfoLocalFilePath]) {//如果在改路下存在文件，存在购买凭证，说明发送凭证失败，再次发起验证
        [self sendFailedIapFiles];
        [SVProgressHUD showErrorWithStatus:@"存在未完成订单\n请稍后再试～～"];
        return;
    }

    if([SKPaymentQueue canMakePayments]){
        [self serverCreateoOrder];
        [SVProgressHUD show];
    }else{
        NSLog(@"不允许程序内付费");
    }
}

#pragma mark 后台创建订单
- (void)serverCreateoOrder{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KOrderRecharge];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"money_id":@(_model.money_id),
                                 @"device_type":@"ios",
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _ordermodel = [OrderModel modelWithJSON:json[@"data"]];
            // 后台创建订单成功后  去苹果服务器请求商品
            [self requestProductData:_model.ios_shop_id];
        } else {
            [SVProgressHUD showErrorWithStatus:@"创建订单失败"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"创建订单失败"];
    }];
}

#pragma mark 去苹果服务器请求商品
- (void)requestProductData:(NSString *)shop_id{
    NSSet* dataSet = [[NSSet alloc] initWithArray:@[shop_id]];
    // 请求商品信息
    [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
    [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response) {
        if(response.products.count > 0 ) {
            //取出第一件商品id
            SKProduct *product = response.products[0];
            [SVProgressHUD showWithStatus:@"正在充值，请稍等···"];

            [[IAPShare sharedHelper].iap buyProduct:product onCompletion:^(SKPaymentTransaction* trans){
                if(trans.error) {
                    [SVProgressHUD showErrorWithStatus:@"充值失败"];
                }
                if(trans.transactionState == SKPaymentTransactionStatePurchased) {
                    NSLog(@"充值成功");
                    [SVProgressHUD showSuccessWithStatus:@"充值成功"];

                    // 这个 receipt 就是内购成功 苹果返回的收据
                    NSData* receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
                    /******这里我将receipt base64加密，把加密的收据 和 产品id，一起发送到app服务器********/
                    NSString *receiptBase64 = [NSString base64StringFromData:receipt length:[receipt length]];
                    //保存凭证
                    [self saveReceipt:receipt];
                    [self sendCheckReceiptWithBase64:receiptBase64 orderNumber:_ordermodel.order_number];
                    
                } else if(trans.transactionState == SKPaymentTransactionStateFailed) {
                    if (trans.error.code == SKErrorPaymentCancelled) {
                        [SVProgressHUD showErrorWithStatus:@"取消充值"];
                    }else if (trans.error.code == SKErrorClientInvalid) {
                        [SVProgressHUD showErrorWithStatus:@"充值失败"];
                    }else if (trans.error.code == SKErrorPaymentInvalid) {
                        [SVProgressHUD showErrorWithStatus:@"充值失败"];
                    }else if (trans.error.code == SKErrorPaymentNotAllowed) {
                        [SVProgressHUD showErrorWithStatus:@"充值失败"];
                    }else if (trans.error.code == SKErrorStoreProductNotAvailable) {
                        [SVProgressHUD showErrorWithStatus:@"充值失败"];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"充值失败"];
                    }
                } else {
                    [SVProgressHUD showErrorWithStatus:@"充值失败"];
                }
            }];
        }else{
            // ..未获取到商品
            NSLog(@"..未获取到商品");
            [SVProgressHUD showErrorWithStatus:@"未获取到商品"];
        }
    }];
}


#pragma mark 订单传给后台做二次验证
- (void)sendCheckReceiptWithBase64:(NSString *)Base64Str orderNumber:(NSString *)orderNumber{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KIAPOrderVerify];
    NSDictionary *parameters = @{
                                 @"order_number":orderNumber,
                                 @"payload":Base64Str
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self sendAppStoreRequestSucceededWithData];
            NSInteger data = [json[@"data"] integerValue];
            self.needToBuyMoney = self.needToBuyMoney-data;
            if (self.needToBuyMoney > 0) {
                [_rechargeView.topLab setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",self.needToBuyMoney]]];
            } else {
                [self leftBarButtonItemBack];
            }
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma mark 持久化存储用户充值凭证(这里最好还要存储当前日期，用户id等信息，用于区分不同的凭证)
- (void)saveReceipt:(NSData *)receipt{
    if(receipt && receipt.length){
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:AppStoreInfoLocalFilePath]) {//如果在改路下不存在文件，说明就没有保存验证失败后的购买凭证，也就是说发送凭证成功。
                [fileManager createDirectoryAtPath:AppStoreInfoLocalFilePath//创建目录
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:nil];
        }
        NSString *fileName = [[NSUUID UUID] UUIDString];
        NSString *savedPath = [NSString stringWithFormat:@"%@%@.plist", AppStoreInfoLocalFilePath, fileName];
        NSDictionary *dic = @{
                              @"IAP_RECEIPT":receipt,
                              @"USER_ID":USER_ID,
                              @"order_number":_ordermodel.order_number
                              };
        BOOL result = [dic writeToFile:savedPath atomically:YES];
        NSLog(@"%d",result);
        receipt = nil;
    }
}


#pragma mark 验证成功就从plist中移除凭证
-(void)sendAppStoreRequestSucceededWithData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:AppStoreInfoLocalFilePath]) {
        [fileManager removeItemAtPath:AppStoreInfoLocalFilePath error:nil];
        
    }
}


//存在购买凭证，说明发送凭证失败，再次发起验证
- (void)sendFailedIapFiles{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    
    //搜索该目录下的所有文件和目录
    NSArray *cacheFileNameArray = [fileManager contentsOfDirectoryAtPath:AppStoreInfoLocalFilePath error:&error];
    if (cacheFileNameArray.count == 0) {
        [self sendAppStoreRequestSucceededWithData];
    }
    if (error == nil){
        for (NSString *name in cacheFileNameArray){
            if ([name hasSuffix:@".plist"]){//如果有plist后缀的文件，说明就是存储的购买凭证
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", AppStoreInfoLocalFilePath, name];
                NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:filePath];
                //这里的参数请根据自己公司后台服务器接口定制，但是必须发送的是持久化保存购买凭证
                NSData *receipt = [dic objectForKey:@"IAP_RECEIPT"];
                if(receipt==nil){
                    [self sendAppStoreRequestSucceededWithData];
                    return;
                }
                NSString *receiptBase64 = [NSString base64StringFromData:receipt length:[receipt length]];
                NSString *order_number = [dic objectForKey:@"order_number"];
                [self sendCheckReceiptWithBase64:receiptBase64 orderNumber:order_number];
            }
        }
    }
}



- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return NO;
}



@end
