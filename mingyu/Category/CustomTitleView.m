//
//  CustomTitleView.m
//  mingyu
//
//  Created by MingYu on 2018/11/20.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "CustomTitleView.h"

@implementation CustomTitleView

// 如果这个没有调用父类的方法可以不用写该方法
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {  }
    return self;
}
// 这个重写方法为重点
- (void)setFrame:(CGRect)frame {
    [super setFrame:CGRectMake(0, 0, self.superview.frame.size.width,         self.superview.bounds.size.height)];
}



@end
