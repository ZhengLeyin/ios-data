//
//  ParentVideoPlayerViewController.m
//  mingyu
//
//  Created by apple on 2018/6/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoPlayerViewController.h"
#import "ParentVideoPlayerCell.h"

@interface ParentVideoPlayerViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, assign) CGFloat offer;

@end

@implementation ParentVideoPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    if (self.arrayData && self.arrayData.count > 0) {
    //        return self.arrayData.count;
    //    }
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentVideoPlayerCell *cell = [ParentVideoPlayerCell theParentVideoPlayerCellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_arrayData && _arrayData.count > indexPath.row) {
//        cell.videomodel = [_arrayData objectAtIndex:indexPath.row];
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (_arrayData.count > indexPath.row) {
//        //        ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
//        //        VC.parentarticlemodel = [_arrayData objectAtIndex:indexPath.row];
//        //        [self.navigationController pushViewController:VC animated:YES];
//    }
}



//系统动画停止是刷新当前偏移量_offer是我定义的全局变量
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    _offer = scrollView.contentOffset.y;
    
    ZPLog(@"end========%f",_offer);
    
}


//滑动减速是触发的代理，当用户用力滑动或者清扫时触发
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    
    if (fabs(scrollView.contentOffset.y -_offer) > 5) {
        if (scrollView.contentOffset.y > _offer) {
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i < 10) {
                
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
        }else{
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i > 0) {
                
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i-1 inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
        }
    } else {
        
    }
}


//用户拖拽是调用
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (fabs(scrollView.contentOffset.y -_offer) > (kScreenHeight-NaviH)/2) {
        if (scrollView.contentOffset.y > _offer) {   //加载下面
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i < 10) {
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
        }else{  //加载上面
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i > 0) {
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i-1 inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
            
        }
    } else {
        if (scrollView.contentOffset.y > _offer) {   //加载下面
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i < 10) {
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i-1 inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
        }else{  //加载上面
            
            int i = scrollView.contentOffset.y/(kScreenHeight-NaviH)+1;
            if (i > 0) {
                NSIndexPath * index =  [NSIndexPath indexPathForRow:i inSection:0];
                [_tableView scrollToRowAtIndexPath:index
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
            }
            
        }
    }
}



- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = kScreenHeight-NaviH;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}



@end
