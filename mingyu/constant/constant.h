//
//  constant.h
//  mingyu
//
//  Created by apple on 2018/7/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSNotificationName const NSNotificationPaymentSuccessNotification; /**< 精品课 收银台付款成功回调 */

UIKIT_EXTERN NSNotificationName const NSNotificationShrinkNotification;     /**< 精品课 barnner 收缩监听通知 */

UIKIT_EXTERN NSNotificationName const NSNotificationExpandNotification;     /**< 精品课 barnner 扩张监听通知 */

UIKIT_EXTERN NSNotificationName const NSNotificationReceiveNotification;     //有消息通知

UIKIT_EXTERN NSNotificationName const NSNotificationReceiveSystemMessage;     //系统消息通知

UIKIT_EXTERN NSNotificationName const NSNotificationReceivePriVateMessage;    //私信通知

UIKIT_EXTERN NSNotificationName const NSNotificationSetTopTopic;    //置顶帖子通知

UIKIT_EXTERN NSNotificationName const NSNotificationCancelTopTopic;   //取消置顶帖子通知

UIKIT_EXTERN NSNotificationName const NSNotificationAddCircle;    //加入圈子

UIKIT_EXTERN NSNotificationName const NSNotificationSelectIndex1;    //点击tabbar 发现

UIKIT_EXTERN NSNotificationName const NSNotificationBirthdayChange;    //更改年龄通知

UIKIT_EXTERN NSNotificationName const NSNotificationAddTimeRecord;    //添加时光印记通知

UIKIT_EXTERN NSNotificationName const NSNotificationDeleteTimeRecord;   //删除时光印记通知

UIKIT_EXTERN NSNotificationName const NSNotificationDeletePhotoRecord;   //删除时光印记图片通知

UIKIT_EXTERN NSNotificationName const NSNotificationUserLogin;   //用户登录时发出通知




@interface constant : NSObject

//添加评论 获取评论类型
typedef NS_ENUM(NSUInteger, GetAndAddCommentType) {
    childcare_comment = 1,      //育儿必读
    article_comment = 2,        //资讯
    topic_comment = 3,          //帖子
    video_comment = 4,          //视频
    audio_comment = 5,          //音频
    course_comment = 6,         //课程
    listen_book_comment = 7,    //听书
};


//收藏类型
typedef NS_ENUM(NSUInteger, CollectType) {
    collect_type_video = 1,         //视频的收藏
    collect_type_news = 2,          //新闻的收藏
//    collect_type_topic = 2,         //帖子的收藏
//    collect_type_article = 3,       //资讯的收藏
//    collect_type_childcare = 4,     //育儿必读的收藏
    collect_type_book = 5,          //音频书的收藏
    collect_type_course = 6,        //课的收藏
};

//点赞类型
typedef NS_ENUM(NSUInteger, PraiseType) {
    praise_video_comment = 1,            //视频评论的点赞
    praise_type_news = 2,               //新闻的点赞
    praise_topic_comment = 3,            //帖子评论的点赞
    praise_article_comment = 5,          //资讯评论的点赞
    praise_type_audio = 7,              //音频的点赞
    praise_audio_comment = 8,           //音频评论的点赞
    praise_type_book = 9,               //音频书的点赞
    praise_type_course = 10,            //课的点赞
    praise_course_comment = 11,         //课程评论的点赞
    praise_book_comment = 12,           //音频书评论的点赞
    praise_type_video = 13,             //短视频的点赞
    praise_childcare_comment = 14,      //育儿必读评论点赞
    praise_childcare_replay = 15,       //关于育儿必读评论回复的点赞
    praise_topic_replay = 16,           //关于帖子评论回复的点赞
    praise_video_replay = 17,           //关于短视频评论回复的点赞
    praise_book_replay = 18,            //关于听书评论回复的点赞
    praise_audio_replay = 19,           //关于音频评论回复的点赞
    praise_article_replay = 20,         //关于资讯评论回复的点赞
    praise_course_replay = 21,          //关于课程评论回复的点赞
    praise_news_comment = 22,           //关于新闻的评论点赞
    
};

//举报类型
typedef NS_ENUM(NSUInteger, AccusationType) {
    accusation_book_comment = 1,            //听书评论举报
    accusation_topic_comment = 2,           //帖子评论举报
    accusation_article_comment = 3,         //资讯评论举报
    accusation_audio_comment = 4,           //音频评论举报
    accusation_type_news = 5,               //育儿必读 帖子 资讯举报
    accusation_course_comment = 6,          //课程评论举报
    accusation_childcare_comment = 7,       //育儿必读评论举报
    accusation_video_comment = 8,           //视频评论举报
    accusation_type_video = 9,              //视频举报
    accusation_type_audio = 10,              //音频举报
    accusation_video_replay = 11,            //视频回复举报
    accusation_audio_replay = 12,            //音频回复举报
    accusation_topic_replay = 13,           //帖子评论回复举报
    accusation_article_replay = 14,          //资讯评论回复举报
    accusation_childcare_replay = 15,        //育儿必读评论回复举报
    accusation_type_course = 16,             //课程举报

};


//系统消息类型
typedef NS_ENUM(NSUInteger, SystemMessageType) {
    
    systemMessage_topic_comment = 1,
    systemMessage_childcare_comment = 2,
    systemMessage_video_comment = 3,
    
    systemMessage_topic_replay =  4,
    systemMessage_childcare_replay =  5,
    systemMessage_video_replay =  6,
    systemMessage_audio_replay = 7,
    systemMessage_article_replay = 8,
    
    systemMessage_topic_collect = 9,
    systemMessage_childcare_collect = 10,
    systemMessage_video_collect = 11,
    
    systemMessage_follow =  12,
    
    systemMessage_topic_praise = 13,
    systemMessage_video_praise = 14,
    systemMessage_childcare_praise = 15,
    
    systemMessage_topic_comment_praise = 16,
    systemMessage_video_comment_praise = 17,
    systemMessage_article_comment_praise = 18,
    systemMessage_audio_comment_praise = 19,
    systemMessage_childcare_comment_praise = 20,
    
    systemMessage_course_comment = 21,
    systemMessage_course_replay = 22,
    systemMessage_course_praise = 23,
    systemMessage_course_comment_praise = 24,
    systemMessage_course_collect = 25,
};


//系统消息跳转类型
typedef NS_ENUM(NSUInteger, NSNotificationType) {
    jump_type_qqUserList = 1,
    jump_type_systemMessage = 2,
    
};



/** 分享类型 */
typedef NS_ENUM(NSUInteger, ShareType){
    share_article = 1,     //资讯分享
    share_childcare = 2,    //育儿必读分享
    share_topic = 3,        //帖子分享
    share_video = 4,        //视频分享
    share_audio = 5,        //音频分享
    share_course = 6,       //课程分享
};



/** 日志类型 */
typedef NS_ENUM(NSUInteger, TransactionState) {
    MODULE_AUDIO_TIMER = 1,  //使用音频定时功能
    MODULE_ARTICLE_CLOSE, //关闭资讯文章
    MODULE_CHILDCARE_CLOSE,  //关闭育儿必读文章
    MODULE_VIDEO_CLOSE,     //关闭视频
    MODULE_RECOMMEND_CLICK,  //点击相关推荐模块
};


/** Log类型 */
typedef NS_ENUM(NSUInteger, ResourcesType){
    resources_article = 1,     //资讯
    resources_childcare = 2,    //育儿
    resources_topic = 3,        //帖子
    resources_video = 4,        //视频
    resources_audio = 5,        //音频
};


/** 搜索类型 */
typedef NS_ENUM(NSUInteger, SearchType){
    SearchAll_type = 1,     //搜索所有内容
    SearchCourse_type = 2,   //搜索课程
};

/** 首页功能选择类型 跨年龄区分 */
typedef NS_ENUM(NSUInteger, HomePageType){
    HomePageType_BeforeAgeOfTwo = 1,     //两岁前
    HomePageType_AfterTwoYearsOld = 2,   //两岁后
};

+ (NSString *)TransactionState:(TransactionState )state;

@end
