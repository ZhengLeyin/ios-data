//
//  ShareModel.h
//  mingyu
//
//  Created by apple on 2018/9/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareModel : NSObject

//图片地址
@property (nonatomic, copy) NSString *thumbURL;

//分享标题
@property (nonatomic, copy) NSString *titleStr;

//分享副标题
@property (nonatomic, copy) NSString *descr;

//分享网址
@property (nonatomic, copy) NSString *webpageUrl;

//目标id
@property (nonatomic, assign) NSInteger from_id;

//举报类型
@property (nonatomic, assign) AccusationType accusationType;

//分享类型
@property (nonatomic, assign) ShareType shareType;

//目标URL
@property (nonatomic, copy) NSString *targetURL;



@end
