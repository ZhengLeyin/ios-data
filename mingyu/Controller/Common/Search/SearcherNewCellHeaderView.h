//
//  SearcherNewCellHeaderView.h
//  mingyu
//
//  Created by MingYu on 2018/11/12.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^HistoryActionBlock)(void);
@interface SearcherNewCellHeaderView : UIView
/** 清空历史回调 */
@property (nonatomic,copy) HistoryActionBlock tabAction;
/** 搜索类型BEl */
@property (weak, nonatomic) IBOutlet UILabel *searchTitleBEL;
/** 清空历史Btn */
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;
/** 1.最近搜索,2.热门搜索 headerView */
+ (instancetype)theSearchNewCellHeaderView;
- (void)theSearchNewCellHeaderViewSetBaseUI:(NSInteger)section;

/**改版-1.0 育儿搜索 热门搜索FooterView*/
+ (instancetype)theSearchNewCellFooterView;
@end

NS_ASSUME_NONNULL_END
