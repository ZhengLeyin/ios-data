//
//  MingYuRechargeViewController.h
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MingYuRechargeViewController : UIViewController
/** 需要购买的育币数量提示 */
@property (nonatomic, assign) CGFloat needToBuyMoney;
/** 判断钱包和精品课区别 0是精品课  1是钱包 */
@property (nonatomic, assign) NSUInteger type;

@property (nonatomic, copy) void (^BackBlock) (void);
@end

NS_ASSUME_NONNULL_END
