//
//  TopicEditorViewController.h
//  mingyu
//
//  Created by apple on 2018/4/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicEditorViewController : UIViewController

@property (nonatomic, assign) NSInteger circle_id;


@property (nonatomic, strong) NSMutableArray *topicDraftsArray;

@property (nonatomic, strong) NSDictionary *topicDraftsDic;


//- (void)readDrafts:(NSMutableArray *)array atIndex:(NSInteger )index;


@end
