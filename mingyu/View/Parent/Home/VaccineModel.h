//
//  VaccineModel.h
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VaccineModel : NSObject


@property (nonatomic, assign) NSInteger realId;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *function;

@property (nonatomic, copy) NSString *vaccinecount;

@property (nonatomic, copy) NSString *must;

@property (nonatomic, assign) NSInteger days;

@property (nonatomic, copy) NSString *mark;

@property (nonatomic, assign) BOOL down;

//realId": 2,
//"title": "乙肝疫苗\n",
//"function": "预防乙型肝炎",
//"count": "第一次",
//"must": "必打",
//"days": 0,
//"mark": "接种原因：\n\n有很多乙肝病毒携带者都是在新生儿或儿童时期就受到了感染。刚出生的宝宝免疫功能尚不健全，对乙肝病毒的免疫力极低，一旦受到感染便很难清除病毒，最终成为乙肝病毒的携带者。因此新生儿的乙肝预防非常重要，所有宝宝都应当接种乙肝疫苗。\n\n接种原理：\n\n乙肝疫苗通过制备乙肝病毒表面的某些有效蛋白，并将这些蛋白接种到人体，使人体内的免疫细胞产生可对抗该病毒的抗体，从而达到预防乙肝疾病的目的。而被接种者本身并不会受到感染。接种疫苗后，当人体再次接触到乙肝病毒时，其已存在于体内的抗体就会立即开始工作，清除病毒，抵御感染。\n\n接种方法：\n\n乙肝疫苗全程接种共3针，接种时间分别是：宝宝出生后的24小时内注射第一针，宝宝1个月大时注射第二针，宝宝6个月大时注射第3针。新生儿的接种部位为大腿前部外侧肌肉，儿童和成人则注射在上臂三角肌中部的肌肉。"
//},

@end
