//
//  AudioModel.h
//  mingyu
//
//  Created by apple on 2018/5/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioModel : NSObject<NSCoding>

/**音频Id。从0开始，仅标识当前音频在数组中的位置*/
@property (nonatomic, assign) NSUInteger audioId;

/** 音频id */
@property (nonatomic, assign) NSInteger audio_id;

/** 主题id */
@property (nonatomic, assign) NSInteger subject_id;

/** 外键id  */
@property (nonatomic, assign) NSInteger fk_parent_id;

/** 外键 标题 */
@property (nonatomic, copy) NSString *fk_parent_title;

/** 外键 图像 */
@property (nonatomic, copy) NSString *fk_parent_head;
/** 音频标题  */
@property (nonatomic, copy) NSString *audio_title;

/** 音频图片  */
//@property (nonatomic, strong) UIImage *audio_image;
/** 音频配图地址 */
@property (nonatomic, strong) NSURL *audio_image_url;

/** 音频地址  */
@property (nonatomic, strong) NSURL *audio_path;

/** 音频价格  */
@property (nonatomic, assign) CGFloat audio_price;

/** 虚拟币数量  */
@property (nonatomic, assign) NSInteger audio_gold;

/** 音频时长题  */
@property (nonatomic, copy) NSString *time_length;

/** 播放次数  */
@property (nonatomic, assign) NSInteger play_count;

/** 音频类型  */
@property (nonatomic, assign) NSInteger audio_type;

/** 创建时间  */
@property (nonatomic, copy) NSString *create_time;

/** 点赞数  */
@property (nonatomic, assign) NSInteger praise_number;

/** 已点赞  */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 序号 */
@property (nonatomic, assign) NSInteger audio_sort;

/** 播放时长 */
@property (nonatomic, assign) CGFloat timeWithValue;

/** 分享地址 */
@property (nonatomic, copy) NSString *share_url;

/** 试听 */
@property (nonatomic, assign) NSUInteger is_free;


@end
