//
//  VideoBaseViewController.h
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LabelListModel;

@interface VideoBaseViewController : UIViewController

//@property (nonatomic, assign) NSInteger label_id;
@property (nonatomic, strong) LabelListModel *labModel;


@end
