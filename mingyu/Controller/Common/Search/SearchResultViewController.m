//
//  SearchResultViewController.m
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SearchAllViewController.h"
#import "SearchChildCareViewController.h"
#import "SearchTopicViewController.h"
#import "SearchArticleViewController.h"
#import "SearchCourseViewController.h"
#import "SearchUserViewController.h"
#import "SearchCircleViewController.h"
#import "SearchVideoViewController.h"

@interface SearchResultViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger PageMenuIndex;

@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
   
}


- (void)setSearchStr:(NSString *)searchStr{
    _searchStr = searchStr;
    if (_searchType == SearchAll_type) {
        self.dataArr = @[@"全部",@"精品课",@"视频",@"育儿必读",@"资讯",@"作者"];

        SearchAllViewController *SearchAllVC = [[SearchAllViewController alloc] init];
        SearchAllVC.searchStr = _searchStr;
        [self addChildViewController:SearchAllVC];
        
        SearchCourseViewController * SearchCourseVC = [[SearchCourseViewController alloc] init];
        SearchCourseVC.searchStr = _searchStr;
        [self addChildViewController:SearchCourseVC];
        
        SearchVideoViewController *searchVideoVC = [[SearchVideoViewController alloc] init];
        searchVideoVC.searchStr = _searchStr;
        [self addChildViewController:searchVideoVC];
        
        SearchChildCareViewController * SearchChildCareVC = [[SearchChildCareViewController alloc] init];
        SearchChildCareVC.searchStr = _searchStr;
        [self addChildViewController:SearchChildCareVC];
        
//        SearchTopicViewController * SearchTopicVC = [[SearchTopicViewController alloc] init];
//        SearchTopicVC.searchStr = _searchStr;
//        [self addChildViewController:SearchTopicVC];
        
        SearchArticleViewController *SearchArticleVC = [[SearchArticleViewController alloc] init];
        SearchArticleVC.searchStr = _searchStr;
        [self addChildViewController:SearchArticleVC];
        
//        SearchCircleViewController *SearchCircleVC = [[SearchCircleViewController alloc] init];
//        SearchCircleVC.searchStr = _searchStr;
//        [self addChildViewController:SearchCircleVC];
        
        SearchUserViewController *SearchUserVC = [[SearchUserViewController alloc] init];
        SearchUserVC.searchStr = _searchStr;
        [self addChildViewController:SearchUserVC];
        
        [self.view addSubview:self.pageMenu];
        self.pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;

    } else if (_searchType == SearchCourse_type){
        self.dataArr = @[@"课程"];
        SearchCourseViewController * SearchCourseVC = [[SearchCourseViewController alloc] init];
        SearchCourseVC.searchStr = _searchStr;
        [self addChildViewController:SearchCourseVC];
        
    }

//    [self.view addSubview:self.pageMenu];
//    _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollAdaptContent;

    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        if (_searchType == SearchCourse_type){
            VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH);
        } else {
            VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
        }
//        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
    [_scrollView addSubview:VC.view];
}



- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        if (_searchType == SearchCourse_type){
            _scrollView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH);
        } else {
            _scrollView.frame = CGRectMake(0, 44, kScreenWidth, kScreenHeight-NaviH-effectViewH-44);
        }
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:0];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
//        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollAdaptContent;
    }
    return _pageMenu;
}



@end
