//
//  VideoPlayEndVIew.h
//  mingyu
//
//  Created by apple on 2018/7/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ShareModel;

@interface VideoPlayEndVIew : UIView


@property (weak, nonatomic) IBOutlet UIButton *shareButton1;

@property (weak, nonatomic) IBOutlet UIButton *shareButton2;

@property (weak, nonatomic) IBOutlet UIButton *shareButton3;

@property (weak, nonatomic) IBOutlet UIButton *shareButton4;

@property (weak, nonatomic) IBOutlet UIButton *shareButton5;

@property (weak, nonatomic) IBOutlet UIButton *jubaoButton;

@property (nonatomic, strong) ShareModel *sharemodel;


@property (nonatomic, copy) void (^playAgainBlock)(void);

@property (nonatomic, copy) void (^cancleShareBlock)(void);


//- (instancetype)initWithDictionary:(NSDictionary *)shareDic;


/**
 初始化
 shareDic  分享内容
 */
//- (instancetype)initWithDictionary:(NSDictionary *)shareDic;
//- (instancetype)initWithshareModel:(ShareModel *)sharemodel;

///**
// 显示
// */
//- (void)show;
//
//
///**
// 隐藏
// */
//- (void)hidden;

@end
