//
//  MessageDetailCell.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessageModelFrame;

@interface MessageDetailCell : UITableViewCell

/**
 *  模型数据
 */
@property (nonatomic,strong)MessageModelFrame *frameModel;
/**
 *  创建Cell
 *
 *  @param tableView 表对象
 *
 *  @return Cell
 */
+(instancetype)theMessageDetailCellWithTableView:(UITableView *)tableView;

@end
