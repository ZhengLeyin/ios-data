//
//  MingYuRechargeModel.h
//  mingyu
//
//  Created by MingYu on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MingYuRechargeModel : NSObject
/** money_id */
@property (nonatomic, assign) NSUInteger money_id;
/** money_num */
@property (nonatomic, assign) NSUInteger money_num;
/** money_virtual */
@property (nonatomic, assign) NSUInteger money_virtual;
/** ios_shop_id */
@property (nonatomic, strong) NSString *ios_shop_id;

#pragma mark 收银台
/** ideal_money 积分 */
@property (nonatomic, assign) NSUInteger ideal_money;
/** user_gold 育币 */
@property (nonatomic, assign) CGFloat ios_money;
/** state */
@property (nonatomic, assign) NSUInteger state;
/** is_true_binding */
@property (nonatomic, assign) BOOL is_true_binding;
/** is_true_password */
@property (nonatomic, assign) BOOL is_true_password;

@end

NS_ASSUME_NONNULL_END
