//
//  QianDaoCollectionReusableView.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "QianDaoCollectionReusableView.h"
#import "AdvertisementModel.h"
#import <UMPush/UMessage.h>

@implementation QianDaoCollectionReusableView

{
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _remindSwitch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放
//    [_remindSwitch setOn:![userDefault boolForKey:KUDQianDaoSwitch]];
    [self getUserRemind];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -kScreenWidth, kScreenWidth, kScreenWidth)];
    view.backgroundColor = [UIColor redColor];
    [self addSubview:view];
    
    [self getAdvertisement];
    cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth, _adView.height) delegate:self placeholderImage:ImageName(@"placeholderImage")];
    cycleView.backgroundColor = [UIColor whiteColor];
    cycleView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
    cycleView.showPageControl = NO;
    [_adView addSubview:cycleView];
    _adView.layer.masksToBounds  = YES;

}


//获取提醒状态
- (void)getUserRemind{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserRemind];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"type":@"1"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            BOOL data = [json[@"data"] boolValue];
            [_remindSwitch setOn:data];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (advertisementArray && advertisementArray.count > 0) {
        AdvertisementModel *model = advertisementArray[index];
        if (model.to_type == 1) {
            //1- 一键听 2- 育儿必读 3- 推荐 4- 视频 5-圈子 6- 时光印记
            [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
        } else if (model.to_type == 2){
            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
            VC.title = @"育儿必读";
            [[self viewController].navigationController pushViewController:VC animated:YES];
        } else if (model.to_type == 3){
            if (iOS10Later) {
                [self viewController].tabBarController.selectedIndex = 1;
                [[self viewController].navigationController popViewControllerAnimated:YES];
            }
        } else if (model.to_type == 4){
            if (iOS10Later) {
                [self viewController].tabBarController.selectedIndex = 2;
                [[self viewController].navigationController popViewControllerAnimated:YES];
            }
        } else if (model.to_type == 5){
//圈子
        } else if (model.to_type == 6){
            TimeRecordViewController *VC = [TimeRecordViewController new];
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }
    }
}

//获取广告
- (void)getAdvertisement {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAdvertisement];
    NSDictionary *parameters = @{
                                 @"ad_type":@"2",
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            advertisementArray = [NSMutableArray array];
            NSMutableArray *imageArray = [NSMutableArray array];
            for (NSDictionary *dic in data) {
                AdvertisementModel *model = [AdvertisementModel modelWithJSON:dic];
                [advertisementArray addObject:model];
                [imageArray addObject:[NSString stringWithFormat:@"%@%@",KKaptcha,model.ad_url]];
            }
            cycleView.imageURLStringsGroup = imageArray;
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}


- (void)setActionmodel:(QianDaoActionModel *)actionmodel{
    _actionmodel = actionmodel;
    _user_gold_Lab.text = [NSString stringWithFormat:@"%ld",(long)actionmodel.ideal_money];
    _count_day_Lab.text = [NSString stringWithFormat:@"已连续签到 %ld 天",(long)actionmodel.count_day];
    
    if (actionmodel.is_register_true) {
        [_qiandaoButton setImage:ImageName(@"已签到") forState:UIControlStateNormal];
    } else{
        [_qiandaoButton setImage:ImageName(@"buttn_01") forState:UIControlStateNormal];
    }
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    [_timeContentView addSubview:lineView];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_timeContentView);
        make.left.centerY.equalTo(_timeContentView);
        make.height.mas_offset(0.5);
    }];
    
    CGFloat goldlabWH = 30;
    CGFloat pading = (kScreenWidth - 30*7)/8;
    for (int i = 0; i < 7; i ++ ) {
        
        UILabel *goldlab = [[UILabel alloc] init];
        goldlab.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
        goldlab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
        goldlab.textAlignment = NSTextAlignmentCenter;
        goldlab.font = FontSize(13);
        goldlab.layer.cornerRadius = goldlabWH/2;
        goldlab.layer.borderColor = [UIColor colorWithHexString:@"D9D9D9"].CGColor;
        goldlab.layer.borderWidth = 0.5;
        goldlab.layer.masksToBounds = YES;
        NSInteger score = actionmodel.action_score+i;
        if (score > 5) {
            score = 5;
        }
        goldlab.text = [NSString stringWithFormat:@"+%ld",(long)score];
        [_timeContentView addSubview:goldlab];
        
        UILabel *timelab = [[UILabel alloc] init];
        timelab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
        timelab.font = FontSize(10);
        NSString *data = [NSString string];
        if (i == 0) {
            data = @"今天";
        } else{
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            currentTime = currentTime +i*24*3600;
            NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MM.dd"];
            NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:currentTime];
            data = [formatter stringFromDate:timeDate];
        }
        
        timelab.text = data;
        [_timeContentView addSubview:timelab];
        
        if (i == 0 && _actionmodel.is_register_true) {
            goldlab.backgroundColor = [UIColor colorWithHexString:@"FFC7C6"];
            goldlab.textColor = [UIColor whiteColor];
            goldlab.layer.borderWidth = 0;
            
            timelab.textColor = [UIColor colorWithHexString:@"FFB2AD"];
            
            _yiqiandao = [[UIImageView alloc] init];
            _yiqiandao.image = [UIImage imageNamed:@"发帖_照片选择"];
            _yiqiandao.layer.cornerRadius = 5;
            _yiqiandao.layer.borderWidth = 1;
            _yiqiandao.layer.borderColor = [UIColor whiteColor].CGColor;
            _yiqiandao.layer.masksToBounds = YES;
            [_timeContentView addSubview:_yiqiandao];
            
            [_yiqiandao mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_offset(11);
                make.top.mas_offset(40);
                make.left.mas_offset(pading+goldlabWH-6);
            }];
        }
        
        [goldlab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_offset(goldlabWH);
            make.centerY.equalTo(_timeContentView);
            make.left.mas_offset(pading+pading*i+goldlabWH*i);
        }];
        
        [timelab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(goldlab);
            make.bottom.mas_equalTo(goldlab.bottom).mas_offset(-5);
        }];
    }
}


- (IBAction)checkButton:(id)sender {
    
    GoldDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"GoldDetailViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


- (IBAction)goldRuleButton:(id)sender {
    GoldRulerViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"GoldRulerViewController"];
    VC.ideal_money = _actionmodel.ideal_money;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


- (IBAction)qiandaoButton:(id)sender {
    
    if (_actionmodel.is_register_true) {
        
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddRegisterAction];
        
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                
                QianDaoViewController *VC = (QianDaoViewController *)[self viewController];
                [VC getRegisterAction];
                
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
            
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];

        }];
    }
}


- (IBAction)remindSwitch:(UISwitch *)sender {
    if (!sender.on) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"关闭签到提醒？"
                                                                       message:@"\n关闭后，手机将不再收到签到提醒，\n当心错过积分哦~～\n"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                 [sender setOn:YES];
//                                                                 [userDefault setBool:NO forKey:KUDQianDaoSwitch];
                                                             }];
        [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
        
        UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //响应事件
                                                                  ZPLog(@"关闭签到提醒");
                                                                  [self ChangeUserRemindOn:NO];
//                                                                  [userDefault setBool:YES forKey:KUDQianDaoSwitch];
                                                              }];
        [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];

        [alert addAction:cancelAction];
        [alert addAction:confirmAction];
        
        [[self viewController] presentViewController:alert animated:YES completion:nil];
    } else{
//        [userDefault setBool:NO forKey:KUDQianDaoSwitch];
        ZPLog(@"打开签到提醒");
        [self ChangeUserRemindOn:YES];
    }
}


- (void)ChangeUserRemindOn:(BOOL)on{
    if (on) { //开启
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"1"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_remindSwitch setOn:YES];
                //添加标签
                [UMessage addTags:@"签到" response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
                 }];
            } else {
                [_remindSwitch setOn:NO];
            }
        } failure:^(NSError *error) {
            [_remindSwitch setOn:NO];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"1"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_remindSwitch setOn:NO];
                //删除标签
                [UMessage deleteTags:@"签到" response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
                 }];
            } else{
                [_remindSwitch setOn:YES];
            }
        } failure:^(NSError *error) {
            [_remindSwitch setOn:YES];
        }];
    }
}


@end
