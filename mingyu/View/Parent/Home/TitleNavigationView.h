//
//  TitleNavigationView.h
//  mingyu
//
//  Created by MingYu on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TitleNavigationView : UIView
/**老师头像*/
@property (weak, nonatomic) IBOutlet UIImageView *teacherUserImageView;
/**老师姓名*/
@property (weak, nonatomic) IBOutlet UILabel *teacherNameLabel;
/**关注*/
@property (weak, nonatomic) IBOutlet UIButton *attention_button;
@property(nonatomic, assign) CGSize intrinsicContentSize;
+ (instancetype)theTitleNavigationView ;

@property (nonatomic, strong) TeacherModel *teachermodel;
@end

NS_ASSUME_NONNULL_END
