//
//  User.h
//  mingyu
//
//  Created by apple on 2018/4/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, copy) NSString *user_id;

@property (nonatomic, copy) NSString *user_name;

@property (nonatomic, copy) NSString *user_sex;

@property (nonatomic, copy) NSString *user_head;

@property (nonatomic, copy) NSString *user_grade;

@property (nonatomic, assign) CGFloat user_money;

@property (nonatomic, assign) NSInteger user_gold;

@property (nonatomic, copy) NSString *user_phone;

@property (nonatomic, copy) NSString *user_password;

@property (nonatomic, assign) NSInteger user_join_time;

@property (nonatomic, copy) NSString *salt;

@property (nonatomic, assign) NSInteger state;

@end
