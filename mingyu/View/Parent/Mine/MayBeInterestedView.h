//
//  MayBeInterestedView.h
//  mingyu
//
//  Created by MingYu on 2018/12/7.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MayBeInterestedView : UIView

+ (instancetype)theMayBeInterestedView;

@end

NS_ASSUME_NONNULL_END
