//
//  BoundPhoneViewController.m
//  mingyu
//
//  Created by apple on 2018/5/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BoundPhoneViewController.h"
#import <UMPush/UMessage.h>

@interface BoundPhoneViewController ()

{
    NSString *PhoneTFContent;
    UITextRange *PhoneTFSelection;
}
@property (weak, nonatomic) IBOutlet UITextField *PhoneTF;
@property (weak, nonatomic) IBOutlet UIView *Line1;

@property (weak, nonatomic) IBOutlet UITextField *CodeTF;
@property (weak, nonatomic) IBOutlet UIButton *GetCodeButton;
@property (weak, nonatomic) IBOutlet UIView *Line2;

@property (weak, nonatomic) IBOutlet UIButton *FinishButton;

@end

@implementation BoundPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *tenDigitNumber = [userDefault objectForKey:KUDuser_phone];
    tenDigitNumber = [tenDigitNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{4})(\\d{4})" withString:@"$1 $2 $3" options:NSRegularExpressionSearch range:NSMakeRange(0, [tenDigitNumber length])];
    _PhoneTF.text = tenDigitNumber;
    _PhoneTF.tintColor = _CodeTF.tintColor = [UIColor colorWithHexString:@"50D0F4"];
    _FinishButton.userInteractionEnabled = NO;
    _GetCodeButton.userInteractionEnabled = NO;
    
//    _PhoneTF.text = [userDefault objectForKey:@"user_phone"];
    if (_PhoneTF.text.length == 13) {
        [_GetCodeButton setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
        _GetCodeButton.userInteractionEnabled = YES;
    }
    
    // 给TextField注册内容变动通知,添加监听方法
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldEditingChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [_PhoneTF addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

// TextField监听方法
- (void)textFieldEditingChanged:(UITextField *)textField {

    //    设置登录按钮
    if (_PhoneTF.text.length == 13 && _CodeTF.text.length == 4) {
        [_FinishButton setBackgroundColor:[UIColor colorWithHexString:@"50D0F4"]];
        _FinishButton.userInteractionEnabled = YES;
    } else {
        [_FinishButton setBackgroundColor:[UIColor colorWithHexString:@"C6C6C6"]];
        _FinishButton.userInteractionEnabled = NO;
    }
    
    //    设置获取验证码
    if (_PhoneTF.text.length == 13) {
        [_GetCodeButton setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
        _GetCodeButton.userInteractionEnabled = YES;
    } else{
        [_GetCodeButton setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
        _GetCodeButton.userInteractionEnabled = NO;
    }
    if (textField == _PhoneTF) {
        //限制手机账号长度（有两个空格）
        if (textField.text.length > 13) {
            textField.text = [textField.text substringToIndex:13];
        }
        
        NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
        
        NSString *currentStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *preStr = [PhoneTFContent stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        //正在执行删除操作时为0，否则为1
        char editFlag = 0;
        if (currentStr.length <= preStr.length) {
            editFlag = 0;
        }
        else {
            editFlag = 1;
        }
        
        NSMutableString *tempStr = [NSMutableString new];
        
        int spaceCount = 0;
        if (currentStr.length < 3 && currentStr.length > -1) {
            spaceCount = 0;
        }else if (currentStr.length < 7 && currentStr.length > 2) {
            spaceCount = 1;
        }else if (currentStr.length < 12 && currentStr.length > 6) {
            spaceCount = 2;
        }
        
        for (int i = 0; i < spaceCount; i++) {
            if (i == 0) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(0, 3)], @" "];
            }else if (i == 1) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(3, 4)], @" "];
            }else if (i == 2) {
                [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(7, 4)], @" "];
            }
        }
        
        if (currentStr.length == 11) {
            [tempStr appendFormat:@"%@%@", [currentStr substringWithRange:NSMakeRange(7, 4)], @" "];
        }
        if (currentStr.length < 4) {
            [tempStr appendString:[currentStr substringWithRange:NSMakeRange(currentStr.length - currentStr.length % 3, currentStr.length % 3)]];
        }else if(currentStr.length > 3 && currentStr.length <12) {
            NSString *str = [currentStr substringFromIndex:3];
            [tempStr appendString:[str substringWithRange:NSMakeRange(str.length - str.length % 4, str.length % 4)]];
            if (currentStr.length == 11) {
                [tempStr deleteCharactersInRange:NSMakeRange(13, 1)];
            }
        }
        textField.text = tempStr;
        // 当前光标的偏移位置
        NSUInteger curTargetCursorPosition = targetCursorPosition;
        
        if (editFlag == 0) {
            //删除
            if (targetCursorPosition == 9 || targetCursorPosition == 4) {
                curTargetCursorPosition = targetCursorPosition - 1;
            }
        }else {
            //添加
            if (currentStr.length == 8 || currentStr.length == 4) {
                curTargetCursorPosition = targetCursorPosition + 1;
            }
        }
        UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument] offset:curTargetCursorPosition];
        [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition toPosition :targetPosition]];
        
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    PhoneTFContent = textField.text;
    PhoneTFSelection = textField.selectedTextRange;
    
    return YES;
}



#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _PhoneTF) {
        _Line1.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
        _Line2.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    } else if (textField == _CodeTF){
        _Line1.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        _Line2.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    _Line1.backgroundColor = _Line2.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    
}


- (IBAction)GetCodeButton:(id)sender {
    _GetCodeButton.userInteractionEnabled = NO;
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KSendSMSCode];
    NSDictionary *parameters = @{@"user_phone":[_PhoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""]};
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self receiveCheckNumButton];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
            _GetCodeButton.userInteractionEnabled = YES;
        }
    } failure:^(NSError *error) {
        _GetCodeButton.userInteractionEnabled = YES;
    }];
    
}

//获取验证码倒计时
- (void)receiveCheckNumButton{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0); // 每秒执行一次
    NSTimeInterval seconds = 60.f;
    NSDate *endTime = [NSDate dateWithTimeIntervalSinceNow:seconds]; // 最后期限
    dispatch_source_set_event_handler(_timer, ^{ int interval = [endTime timeIntervalSinceNow];
        if (interval > 0) {
            // 更新倒计时
            NSString *timeStr = [NSString stringWithFormat:@"(%d)s重新获取", interval];
            dispatch_async(dispatch_get_main_queue(), ^{
                _GetCodeButton.userInteractionEnabled = NO;
                [_GetCodeButton setTitle:timeStr forState:UIControlStateNormal];
            });
        } else {
            // 倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                _GetCodeButton.userInteractionEnabled = YES;
                [_GetCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
            });
        }
    });
    dispatch_resume(_timer);
}


- (IBAction)FinishButton:(id)sender {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KWeChatLoginUpdate];
    NSDictionary *parameters = @{
                                 @"user_name":_resp.name,
                                 @"user_head":_resp.iconurl,
                                 @"open_id":_resp.openid,
                                 @"user_phone":[_PhoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""],
                                 @"user_code":_CodeTF.text,
                                 @"device_type" :IS_IPHONE ? @"ios":@"iosPad",
                                 @"device_id" : [userDefault objectForKey:KUDdevice_id],
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"]boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            UserModel *user = [UserModel modelWithJSON:data];
            [userDefault setObject:user.token forKey:KUDtoken];
            [userDefault setObject:user.user_name forKey:KUDuser_name];
            [userDefault setObject:user.user_phone forKey:KUDuser_phone];
            [userDefault setObject:user.user_password forKey:KUDuser_password];
            [userDefault setInteger:user.user_id forKey:KUDuser_id];
            [userDefault setObject:user.user_head forKey:KUDuser_head];
            [userDefault setInteger:user.follow_number forKey:KUDfollow_number];
            [userDefault setObject:user.baby_head forKey:KUDbabyhead];
            [userDefault setObject:user.user_birthday forKey:KUDuser_birthday];
            [userDefault setBool:YES forKey:KUDhasLogin];
            [userDefault synchronize];

            NSString *alias = [NSString stringWithFormat:@"mingyu%@",USER_ID];
            ZPLog(@"%@",alias);
            [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationBirthdayChange object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationUserLogin object:nil userInfo:nil];

            [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        } else {
        [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
     } failure:^(NSError *error) {
//         [UIView ShowInfo:TipWrongMessage Inview:self.view];
     }];
}


- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
