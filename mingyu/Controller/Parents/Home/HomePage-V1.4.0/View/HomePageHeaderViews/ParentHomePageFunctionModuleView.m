//
//  ParentHomePageFunctionModuleView.m
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ParentHomePageFunctionModuleView.h"
#import "ParentHomePageFunctionModuleCell.h"
#import "ExcellentClassModel.h"

#import "HomePageMoreViewController.h"
static NSString * FunctionModuleCellID = @"ParentHomePageFunctionModuleCell";

@interface ParentHomePageFunctionModuleView ()<HomePageViewModelDetegate>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) HomePageViewModel *homePageVM;
@end

@implementation ParentHomePageFunctionModuleView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        /**头部信息请求*/
        [self.homePageVM getHomePageApiIcon];
        self.homePageVM.homePageViewModelDetegate = self;
       
        [self createUI];
    }
    return self;
}

/** 懒加载 */
-(UICollectionView *)collectionView {
    if (_collectionView == nil) {
        //创建一个layout布局类
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //设置每个item的大小为100*100
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat wh = (kScreenWidth - 30) / 4.0;
        layout.itemSize = CGSizeMake(wh, 130);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        //创建collectionView 通过一个布局策略layout来创建
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0,kScreenWidth, 130) collectionViewLayout:layout];
        //_collectionView.bounces = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        //代理设置
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

#pragma mark /**设置基本变量UICollectionView基本变量**/
-(void)setCollectionView {
    [self addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:FunctionModuleCellID bundle:nil] forCellWithReuseIdentifier:FunctionModuleCellID];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
}

-(void)createUI {
    [self setCollectionView];
}

-(void)setValue:(NSArray *)dataArray {
    [self.dataArray removeAllObjects];
    [self.dataArray addObjectsFromArray:dataArray];
    [self.collectionView reloadData];
}

#pragma mark DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ParentHomePageFunctionModuleCell *cell =  [collectionView dequeueReusableCellWithReuseIdentifier:FunctionModuleCellID forIndexPath:indexPath];
     ExcellentClassModel *model = self.dataArray[indexPath.row];
    [cell parentHomePageFunctionModuleCellOfModel:model];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
         ExcellentClassModel *model = self.dataArray[indexPath.row];
    if ([model.icon_name isEqualToString:@"精品课"]) {
//        ParentCourseViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentCourseViewController"];
//        [[self viewController].navigationController pushViewController:VC animated:YES];
        [[self viewController].navigationController pushViewController:[HomePageMoreViewController new] animated:YES];
    }
    if ( [model.icon_name isEqualToString:@"一键听"]){
           [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
    }
    if ( [model.icon_name isEqualToString:@"育儿必读"]){
        ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
        VC.title = @"育儿必读";
        [[self viewController].navigationController pushViewController:VC animated:YES];;
    }
    if ( [model.icon_name isEqualToString:@"视频"]){
        [self viewController].tabBarController.selectedIndex = 1;
    }
    if ( [model.icon_name isEqualToString:@"食谱"]){
        
    }
    if ( [model.icon_name isEqualToString:@"宝宝疫苗"]){
        BabyVaccineViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"BabyVaccineViewController"];
        [[self viewController].navigationController pushViewController:VC animated:YES];
    }
    
}

-(HomePageViewModel *)homePageVM {
    if (!_homePageVM) {
        _homePageVM = [HomePageViewModel new];
    }
    return  _homePageVM;
}

-(NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
