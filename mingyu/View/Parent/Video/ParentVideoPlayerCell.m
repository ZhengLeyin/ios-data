//
//  ParentVideoPlayerCell.m
//  mingyu
//
//  Created by apple on 2018/6/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoPlayerCell.h"

@implementation ParentVideoPlayerCell

+ (instancetype)theParentVideoPlayerCellWithTableView:(UITableView *)tableview{
    static NSString *cellid = @"ParentVideoPlayerCell";
    ParentVideoPlayerCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentVideoPlayerCell" owner:nil options:nil][0];
    }
    return cell;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    _playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, _playerContentView.height)];
//    _playerView.videoFillMode = VideoFillModeResizeAspect;
//    _playerView.topToolBarHiddenType = TopToolBarHiddenSmall;
//    _playerView.fullStatusBarHiddenType = FullStatusBarHiddenFollowToolBar;
//    _playerView.strokeColor = [UIColor whiteColor];
//    _playerView.progressPlayFinishColor = [UIColor colorWithHexString:@"50D0F4"];
//    _playerView.videoTitleString = @"哼好看的视屏";
//    _playerView.url = [NSURL URLWithString:@"https://vd1.bdstatic.com/mda-hiqmm8s10vww26sx/mda-hiqmm8s10vww26sx.mp4"];
//    [_playerView playVideo];
//    [_playerView backButton:^(UIButton *button) {
//        ZPLog(@"返回");
//    }];
//    
//    [_playerView endPlay:^{
//        ZPLog(@"end");
//    }];
//    
//    [self.playerContentView addSubview:self.playerView];
//    
}






@end
