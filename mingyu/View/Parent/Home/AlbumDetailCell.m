//
//  AlbumDetailCell.m
//  mingyu
//
//  Created by apple on 2018/5/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AlbumDetailCell.h"
#import "YCDownloadManager.h"
#import "downloadInfo.h"

@implementation AlbumDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    self.album_image.contentMode = UIViewContentModeScaleAspectFill;
    self.album_image.clipsToBounds = YES;
//    self.audio_title_lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
//    self.audio_title_lab.highlightedTextColor = [UIColor colorWithHexString:@"50D0F4"];
//    self.isplay_image.hidden = YES;

}


+ (instancetype)theHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"AlbumDetailCell" owner:nil options:nil][0];
    
}



+ (instancetype)theCellWithTableView:(UITableView *)tableView{
    static NSString *cellID = @"AlbumDetailCell";
    AlbumDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"AlbumDetailCell" owner:nil options:nil][1];
    }
    return cell;
}


- (void)setAudiomodel:(AudioModel *)audiomodel{
    _audiomodel = audiomodel;
    _audio_title_lab.text = audiomodel.audio_title;
    
    [_play_count_button setTitle:[NSString stringWithFormat:@"%ld",audiomodel.play_count] forState:UIControlStateNormal];
    
    [_time_length_button setTitle:audiomodel.time_length forState:UIControlStateNormal];
    if ([DFPlayer shareInstance].currentAudioModel.audio_id == audiomodel.audio_id) {
        self.audio_title_lab.textColor = [UIColor colorWithHexString:@"50D0F4"];
        self.isplay_image.hidden = NO;
    } else{
        self.audio_title_lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        self.isplay_image.hidden = YES;
    }
}


- (void)setDownloadStatus:(YCDownloadStatus)status {
    NSString *statusStr = @"一键听_down";
    switch (status) {
        case YCDownloadStatusWaiting:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusDownloading:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusPaused:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusFinished:
            statusStr = @"一键听_down_已下载";
            break;
        case YCDownloadStatusFailed:
            statusStr = @"一键听_down_下载中";
            break;
            
        default:
            break;
    }
    [_download_button setImage:ImageName(statusStr) forState:0];
}

- (IBAction)album_play_button:(id)sender {
    if (self.playBlockButtonBlock) {
        self.playBlockButtonBlock();
    }
}

//下载
- (IBAction)downLoadbutton:(id)sender {
    
    if (![YCDownloadManager isDownloadWithId:[NSString stringWithFormat:@"%ld",_audiomodel.audio_id]]) {
        downloadInfo *info = [[downloadInfo alloc] initWithUrl:[_audiomodel.audio_path absoluteString] fileId:[NSString stringWithFormat:@"%ld",_audiomodel.audio_id]];
        info.delegate = self;
        info.thumbImageUrl = [_audiomodel.audio_image_url absoluteString];
        info.downloadType = AudioType;
        info.fileName = _audiomodel.audio_title;
        info.date = [NSDate date];
        info.totletime = _audiomodel.time_length;
        info.audiomodel =_audiomodel;
        info.enableSpeed = true;
        [YCDownloadManager startDownloadWithItem:info priority:0.8];
    }
}

#pragma -mark- YCDownloadItemDelegate
- (void)downloadItemStatusChanged:(YCDownloadItem *)item {
    [self setDownloadStatus:item.downloadStatus];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
//        self.audio_title_lab.highlighted = selected;
//        self.isplay_image.hidden = !selected;
}

@end
