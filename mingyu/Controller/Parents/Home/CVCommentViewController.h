//
//  CVCommentViewController.h
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCommentViewController : UIViewController
/** type 0 为需要支付  1 不需要支付 */
@property (nonatomic, assign) NSUInteger  type;
@property (nonatomic, assign) NSInteger course_id;
@property (nonatomic, copy) void (^commentBlock)(void);

@property (nonatomic, copy) void (^PushBlock)(UIViewController *vc, BOOL toCourseList);


- (void)locaToComment;

@end
