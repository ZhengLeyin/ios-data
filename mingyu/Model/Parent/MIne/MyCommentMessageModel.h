//
//  MyCommentMessageModel.h
//  mingyu
//
//  Created by apple on 2018/9/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCommentMessageModel : NSObject

/** 标题 */
@property (nonatomic, copy) NSString *title;

/** 内容 */
@property (nonatomic, copy) NSString *message;

/** 回复的用户名 */
@property (nonatomic, copy) NSString *user_name;

/** 时间状态 */
@property (nonatomic, copy) NSString *time_status;

/** 帖子ID */
@property (nonatomic, assign) NSInteger topic_id;


//private String title;//标题
//private String message;//内容
//private Timestamp create_time;//创建时间
//private Integer id;//id
//private Integer user_id;//回复的用户id
//private String user_name;//回复的用户名
//
//private String time_status;//时间状态

@end
