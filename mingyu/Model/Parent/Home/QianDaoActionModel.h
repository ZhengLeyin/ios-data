//
//  QianDaoActionModel.h
//  mingyu
//
//  Created by apple on 2018/5/5.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QianDaoActionModel : NSObject

/**
 是否已签到
 */
@property (nonatomic, assign) BOOL is_register_true;

/**
 积分余额
 */
@property (nonatomic, assign) NSInteger ideal_money;

/**
 连续签到天数
 */
@property (nonatomic, assign) NSInteger count_day;

/**
 签到奖励
 */
@property (nonatomic, assign) NSInteger action_score;


@end


@interface UserActionModel : NSObject

/**
 连续签到天数
 */
@property (nonatomic, assign) NSInteger count_day;

/**
 签到奖励
 */
@property (nonatomic, assign) NSInteger action_score;

/**
 时间
 */
@property (nonatomic, copy) NSString *action_time;

/**
 动作类型
 */
@property (nonatomic, assign) NSInteger action_type;

/**
 动作说明
 */
@property (nonatomic, copy) NSString *action_explain;

/**
 时间
 */
@property (nonatomic, copy) NSString *time_status;


@property (nonatomic, assign) NSInteger fk_user_id;

@property (nonatomic, assign) NSInteger action_id;

@end


@interface IntegralDetailModel : NSObject

@property (nonatomic, assign) NSInteger detail_id;

@property (nonatomic, assign) NSInteger fk_user_id;

@property (nonatomic, copy) NSString *details_explain;

@property (nonatomic, assign) NSInteger score;

@property (nonatomic, copy) NSString *create_time;

@property (nonatomic, assign) NSInteger ideal_money;

/**
 时间
 */
@property (nonatomic, copy) NSString *time_status;


@end
