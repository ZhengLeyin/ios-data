//
//  ImageTag.m
//  MarkNoteParserOC
//
//  Created by marknote on 5/10/15.
//  Copyright © 2015 marknote. All rights reserved.
//

#import "ImageTag.h"
#import "URLTag.h"

@implementation ImageTag

- (NSString*) toHtmlImageWidth:(CGFloat)width{
    

//    CGFloat ImgWidth = 0;
//    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.url.url]];
//    UIImage *image = [UIImage imageWithData:data];
//    if (image.size.width < kScreenWidth-20) {
//        ImgWidth = image.size.width;
//    } else {
//        ImgWidth = kScreenWidth-20;
//    }
    self.url.url  = [self.url.url stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self.url.url hasPrefix:@"http"]) {
        self.url.url = [KKaptcha stringByAppendingString:self.url.url];
    }
    
    if (self.url.title.length > 0 ){
        return [NSString stringWithFormat:  @"<img src=\"%@\" width = %f alt=\"%@\" title=\"%@\"/>",self.url.url , width ,self.alt, self.url.title];
    } else {
        return [NSString stringWithFormat:  @"<img src=\"%@\" width = %f alt=\"%@\" />",self.url.url , width ,self.alt];
    }
}




@end
