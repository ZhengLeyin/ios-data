//
//  SearchCircleViewController.h
//  mingyu
//
//  Created by apple on 2018/6/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCircleViewController : UIViewController

@property (nonatomic, copy) NSString *searchStr;

@end
