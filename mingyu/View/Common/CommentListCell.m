//
//  CommentListCell.m
//  mingyu
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CommentListCell.h"
#import "CommentReplayModel.h"

static CGFloat cellHeight = 0;

@implementation CommentListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage *image = [UIImage imageNamed:@"右"];
    [_MoreButton setImage:image forState:UIControlStateNormal];
    [_MoreButton setTitle:@"查看全部评论" forState:UIControlStateNormal];
    [_MoreButton setTitleColor:[UIColor colorWithHexString:@"60D5F5"] forState:UIControlStateNormal];
    [_MoreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    [_MoreButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -150)];

    [_comment_number_Button setEnlargeEdgeWithTop:0 right:30 bottom:0 left:30];
}


+ (instancetype)theCommentCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"CommentListCell";
    CommentListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:nil options:nil][0];
    }
    return cell;
}

+ (instancetype)theCommentNumberCell{
    return [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:nil options:nil][1];
}

+ (instancetype)TheMoreCell{
    return [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:nil options:nil][2];
}

+ (instancetype)theNoCommentCell{
    return [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:nil options:nil][3];
}

+ (instancetype)theRecommendHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:nil options:nil][4];

}

+ (CGFloat)cellHeight {
    return cellHeight;
}


- (void)setcellWithModel:(CommentModel *)commentmodel praiseType:(NSInteger )praisetype{
    _praise_type = praisetype;
    _commentmodel = commentmodel;
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_userhead_Button.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_userhead_Button.bounds.size];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _userhead_Button.bounds;
    maksLayer.path = maksPath.CGPath;
    _userhead_Button.layer.mask = maksLayer;
    
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_commentmodel.comment_head];
    [_userhead_Button zp_setImageWithURL:_commentmodel.comment_head forState:0 placeholderImage:ImageName(@"默认头像")];
    
    _comment_name_Lab.text = _commentmodel.comment_name;
    _comment_time_Lab.text = _commentmodel.comment_time_status;
    _comment_content_Lab.text = _commentmodel.comment_content;
    
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle  setLineSpacing:5];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:_commentmodel.comment_content];
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_commentmodel.comment_content length])];
    _comment_content_Lab.attributedText = attrString;
    _comment_content_Lab.lineBreakMode = NSLineBreakByTruncatingTail;

    NSString *contentStr = [NSString string];
    if (_commentmodel.replayList.count > 0) {
        NSDictionary *dic = _commentmodel.replayList[0];
        CommentReplayModel *replaymodel = [CommentReplayModel modelWithJSON:dic];
        _replay_count_Lab.text = [NSString stringWithFormat:@"共%ld条回复 >",_commentmodel.replayNum];

        if (replaymodel.to_user_name) {
            contentStr = [NSString stringWithFormat:@"%@ > %@：%@",replaymodel.replay_name,replaymodel.to_user_name,replaymodel.replay_content];
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:contentStr];
            [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"767676"] range:NSMakeRange(0,replaymodel.replay_name.length+replaymodel.to_user_name.length+4)];
            _replay_content_Lab.attributedText = attrString;
        } else {
            contentStr = [NSString stringWithFormat:@"%@：%@",replaymodel.replay_name,replaymodel.replay_content];
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:contentStr];
            [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"767676"] range:NSMakeRange(0,replaymodel.replay_name.length+1)];
            _replay_content_Lab.attributedText = attrString;
        }
       
    }
    
    if (commentmodel.fk_user_id == [USER_ID integerValue]) {
        _replay_Button.hidden = YES;
    } else {
        _replay_Button.hidden = NO;
    }
    
    [self changPraise_number_Button:_commentmodel];
    
    if (_commentmodel.replayList.count > 0) {
        _replay_View.hidden = NO;
        cellHeight = [self hideLabelLayoutHeight:_commentmodel.comment_content withTextFontSize:14 andWidth:kScreenWidth-82]+190;
    } else {
        _replay_View.hidden = YES;
        cellHeight = [self hideLabelLayoutHeight:_commentmodel.comment_content withTextFontSize:14 andWidth:kScreenWidth-82]+110;
    }
//    ZPLog(@"CommentReplayCellH-------%f",cellHeight);
}


-(CGFloat )hideLabelLayoutHeight:(NSString *)content withTextFontSize:(CGFloat)FontSize andWidth:(float)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;  // 段落高度
    NSMutableAttributedString *attributes = [[NSMutableAttributedString alloc] initWithString:content];
    [attributes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize] range:NSMakeRange(0, content.length)];
    [attributes addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];
    CGSize attSize = [attributes boundingRectWithSize:CGSizeMake(width, 50) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return attSize.height;
}


- (void)changPraise_number_Button:(CommentModel *)commentmodel{
    if (commentmodel.click_praise_true == 0) {
        [_praise_number_Button setImage:ImageName(@"评论赞") forState:UIControlStateNormal];
        [_praise_number_Button setTitle:@" 赞 " forState:UIControlStateNormal];
    } else{
        [_praise_number_Button setImage:ImageName(@"评论已赞") forState:UIControlStateNormal];
        [_praise_number_Button setTitle:[NSString stringWithFormat:@" %ld ",commentmodel.praise_number] forState:UIControlStateNormal];
    }
}

//回复
- (IBAction)commentReplayButton:(UIButton *)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentReplayButtonClicked:)]) {
        [self.delegate commentReplayButtonClicked:_commentmodel];
    }
}

//查看评论
- (IBAction)commentDetailButton:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentDetailButtonClicked:)]) {
        [self.delegate commentDetailButtonClicked:_commentmodel];
    }
}

//点击用户头像
- (IBAction)userheaderButton:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentUserheaderClicked:)]) {
        [self.delegate commentUserheaderClicked:_commentmodel.fk_from_id];
    } else {
        PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
        VC.user_id = _commentmodel.fk_user_id;
        [[self viewController].navigationController pushViewController:VC animated:YES];
    }
}



//评论点赞
- (IBAction)commentPraiseButton:(UIButton *)sender {
//    if (![userDefault boolForKey:KUDhasLogin]) {
//        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
//        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
//        return;
//    }
    if (_commentmodel.click_praise_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"from_id":[NSString stringWithFormat:@"%ld",_commentmodel.comment_id],
                                     @"praise_type":@(_praise_type)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger click_praise_true = [json[@"data"] integerValue];
                _commentmodel.click_praise_true = click_praise_true;
                _commentmodel.praise_number++;
                [self changPraise_number_Button:_commentmodel];
                [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];

            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self];
        }];
    } else{
        [UIView ShowInfo:TipDoNotPraise Inview:[self viewController].view];
    }
}


//更多
- (IBAction)MoreButton:(UIButton *)sender {
    if (self.MoreButtonBlock) {
        self.MoreButtonBlock();
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
