//
//  PrivateMessageDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivateMessageDetailViewController : UIViewController

@property (nonatomic, assign) NSInteger user_id;
@property (nonatomic, copy) NSString *user_head;

@property (nonatomic, assign) NSInteger qq_id;

@end
