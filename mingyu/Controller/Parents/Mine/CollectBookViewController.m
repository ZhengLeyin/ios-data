//
//  CollectBookViewController.m
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CollectBookViewController.h"

@interface CollectBookViewController ()<UITableViewDelegate,UITableViewDataSource,EditorBottomViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) EditorBottomView *bottomView;
@property (nonatomic, strong) NSMutableArray *arrayData;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, strong) NothingView *nothingView;
@property (assign, nonatomic) NSInteger deleteNum;
@property (nonatomic,strong)NSMutableArray *selectorArray;//存放选中数据


@end

@implementation CollectBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    self.bottomView.hidden = YES;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_offset(50);
    }];
    [self refreshTableview];
}


//刷新
-(void)refreshTableview
{
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.arrayData.count>0) {
            [_arrayData removeAllObjects];
        }
        _start = 0;
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self getListenBookList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getListenBookList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
    
}

- (void)getListenBookList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetMyCollectBookList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"100"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            _start += 100;
            if (data && data.count == 100) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                BookListModel *model = [BookListModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            if (self.arrayData && self.arrayData .count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        if (self.arrayData && self.arrayData .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return self.arrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListenStoryCell *cell = [ListenStoryCell theCellWithTableView:tableView];
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        BookListModel *model = [self.arrayData objectAtIndex:indexPath.row];
        cell.booklistmodel = model;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.editing == YES) {
        NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                }
            }
        }
        [self.selectorArray addObject:[self.arrayData objectAtIndex:indexPath.row]];
        self.deleteNum += 1;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
    } else {
        if (_arrayData.count > indexPath.row) {
            BookListModel *model = [self.arrayData objectAtIndex:indexPath.row];
            ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
            //        VC.booklistmodel = model;
            VC.target_id = model.book_id;
            VC.audio_type = 2;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing == YES) {
        NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"weixuanz"];
                }
            }
        }
        [self.selectorArray removeObject:[self.arrayData objectAtIndex:indexPath.row]];
        self.deleteNum -= 1;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    } else {
        
    }
}



- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 120;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


- (EditorBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"EditorBottomView" owner:nil options:nil][0];
        _bottomView.delegate = self;
    }
    return _bottomView;
}


- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text =  @"暂无听书内容";
        _nothingView.imageView.image = ImageName(@"听书 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}


- (void)editorButton:(BOOL )iseditor{
    [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
    [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    _selectorArray = [NSMutableArray array];
    self.deleteNum = 0;
    _bottomView.chooseCountLab.text = @"已选0项";
    if (iseditor) {
        _tableView.editing = NO;
        _bottomView.hidden = YES;
    } else {
        _tableView.editing = YES;
        _bottomView.hidden = NO;
    }
}


#pragma -mark- EditorBottomViewDelegate
- (void)chooseAllButonClicked{
    if ([_bottomView.chooseAllButton.titleLabel.text  isEqual: @"  全选"]) {
        for (int i = 0; i < _arrayData.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                    }
                }
            }
        }
        self.selectorArray = [NSMutableArray arrayWithArray:self.arrayData];
        self.deleteNum  = self.arrayData.count;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选 " forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"xuanzhe") forState:0];
    } else {
        for (int i = 0; i< _arrayData.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView deselectRowAtIndexPath:indexPath animated:NO];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
        [self.selectorArray removeAllObjects];
        self.deleteNum  = 0;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    }
}

- (void)deleteButtonClicked{
    if (self.tableView.editing && self.deleteNum > 0) {
        [self showAlertWithTitle:@"确认删除吗？" message:nil noBlock:nil yseBlock:^{
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
            NSMutableArray *idList = [NSMutableArray array];
            for (NewsModel *model in _selectorArray) {
                [idList addObject:@(model.is_collect_true)];
            }
            NSString *string = [idList componentsJoinedByString:@","];
            NSDictionary *parameters = @{
                                         @"idList":string
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    [_arrayData removeObjectsInArray:_selectorArray];
                    [_tableView reloadData];
                    
                    _selectorArray = [NSMutableArray array];
                    self.deleteNum = 0;
                    [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
                    [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
                    _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%lu项",self.deleteNum];

                } else {
                    [UIView ShowInfo:json[@"message"] Inview:self.view];
                }
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:self.view];
            }];
        }];
    }
}


@end
