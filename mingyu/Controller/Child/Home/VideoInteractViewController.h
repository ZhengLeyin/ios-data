//
//  VideoInteractViewController.h
//  mingyu
//
//  Created by apple on 2018/3/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoBaseViewController.h"


typedef void(^selectInteractCell)(NSInteger index);

@interface VideoInteractViewController : VideoBaseViewController


@property (nonatomic, copy) selectInteractCell block;

@end
