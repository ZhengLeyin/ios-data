//
//  AppDelegate.h
//  mingyu
//
//  Created by apple on 2018/3/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) NSInteger PlayTime;

@property (nonatomic,assign) BOOL allowPlay;

@end

