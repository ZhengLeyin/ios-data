//
//  PlayerDatailCell.h
//  mingyu
//
//  Created by apple on 2018/5/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DFPlayer.h"
@class AlbumListModel;

@interface PlayerDatailCell : UITableViewCell<DFPlayerDelegate,DFPlayerDataSource>

@property (nonatomic, assign) BOOL isDownloadPaly;

@property (nonatomic, strong) NSMutableArray    *df_ModelArray;

@property (weak, nonatomic) IBOutlet UILabel *TitleLab;
@property (weak, nonatomic) IBOutlet UIView *audio_view;
@property (weak, nonatomic) IBOutlet UIImageView *audio_image;
@property (weak, nonatomic) IBOutlet UIButton *beisu_button;
@property (weak, nonatomic) IBOutlet UIButton *dingshi_button;
@property (weak, nonatomic) IBOutlet UIButton *audiolist_button;

@property (nonatomic, copy) void (^ChangeAudio) (void);


@property (weak, nonatomic) IBOutlet UILabel *title_lab;


@property (weak, nonatomic) IBOutlet UIImageView *album_image;
@property (weak, nonatomic) IBOutlet UILabel *album_title_lab;
@property (weak, nonatomic) IBOutlet UILabel *album_listen_lab;

@property (nonatomic, strong) AlbumListModel *albumlistModel;



+ (instancetype)theTableViewHeaderView;

+ (instancetype)theCellWithTableView:(UITableView *)tableView;



- (void)initDFPlayer;


@end
