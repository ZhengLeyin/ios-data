//
//  childListenViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "childListenViewController.h"
#import "DataSource.h"

@interface childListenViewController ()<UISearchBarDelegate,YANScrollMenuDelegate,YANScrollMenuDataSource>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 * 菜单
 */
@property (nonatomic, strong) YANScrollMenu *menu;
/**
 *  dataSource
 */
@property (nonatomic, strong) NSMutableArray<DataSource *> *dataSource;
/**
 *  分区数
 */
//@property (nonatomic, assign) NSUInteger number;


@end

@implementation childListenViewController

- (NSMutableArray<DataSource *> *)dataSource{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.headerView.size = CGSizeMake(kScreenWidth, kScale(175));
    self.tableView.tableHeaderView = self.headerView;
    
    UIView *titleView = [[UIView alloc] init];
    titleView.frame = CGRectMake(0, 0, self.view.width-kScale(120), 25);
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:titleView.bounds];
    searchBar.width = titleView.width-kScale(20);
    searchBar.center = titleView.center;
    searchBar.delegate = self;
    [titleView addSubview:searchBar];
    self.navigationItem.titleView = titleView;
    
    
    [self AddIcon];
    
    //GCD
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [self createData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.menu reloadData];
            
        });
    });
    
}


#pragma mark - Prepare UI
- (void)AddIcon{
    
//    self.number = 1;
    
    self.menu = [[YANScrollMenu alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,kScale(175)) delegate:self];
    self.menu.currentPageIndicatorTintColor = [UIColor colorWithRed:107/255.f green:191/255.f blue:255/255.f alpha:1.0];
    [self.headerView addSubview:self.menu];
    
    [YANMenuItem appearance].textFont = [UIFont systemFontOfSize:12];
    [YANMenuItem appearance].space = 5;
    [YANMenuItem appearance].textColor = [UIColor colorWithRed:51/255.f green:51/255.f blue:51/255.f alpha:1.0];
    
}

#pragma mark -  Data
- (void)createData{
    
    
    NSArray *images = @[ImageName(@"icon_cate"),
                        ImageName(@"icon_drinks"),
                        ImageName(@"icon_movie"),
                        ImageName(@"icon_recreation"),
                        ImageName(@"icon_stay"),
                        ImageName(@"icon_ traffic"),
                        ImageName(@"icon_ scenic"),
                        ImageName(@"icon_fitness"),
                        ImageName(@"icon_fitment"),
                        ImageName(@"icon_hairdressing"),
                        ImageName(@"icon_mom"),
                        ImageName(@"icon_study"),
                        ImageName(@"icon_travel"),
                        @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498711713465&di=d986d7003deaae41342dd9885c117e38&imgtype=0&src=http%3A%2F%2Fs9.rr.itc.cn%2Fr%2FwapChange%2F20168_3_0%2Fa86hlk59412347762310.GIF"];
    NSArray *titles = @[@"美食",
                        @"休闲娱乐",
                        @"电影/演出",
                        @"KTV",
                        @"酒店住宿",
                        @"火车票/机票",
                        @"旅游景点",
                        @"运动健身",
                        @"家装建材",
                        @"美容美发",
                        @"母婴",
                        @"学习培训",
                        @"旅游出行",
                        @"动态图\n从网络获取"];
    
    for (NSUInteger idx = 0; idx< images.count; idx ++) {
        
        DataSource *object = [[DataSource alloc] init];
        object.itemDescription = titles[idx];
        object.itemImage = images[idx];
        object.itemPlaceholder = ImageName(@"placeholder");
        
        [self.dataSource addObject:object];
    }
}


#pragma mark - YANScrollMenuProtocol
- (NSUInteger)numberOfSectionsInScrollMenu:(YANScrollMenu *)menu{
    return 1;
}

- (NSUInteger)scrollMenu:(YANScrollMenu *)menu numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (id<YANObjectProtocol>)scrollMenu:(YANScrollMenu *)scrollMenu objectAtIndexPath:(NSIndexPath *)indexPath{
    return self.dataSource[indexPath.item];
}

- (CGSize)itemSizeOfScrollMenu:(YANScrollMenu *)menu{
    return CGSizeMake(kScale(75), kScale(70));
}

- (void)scrollMenu:(YANScrollMenu *)menu didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ZPLog(@"IndexPath:%@",indexPath);
    UIViewController *VC = [UIViewController new];
    VC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:VC animated:YES];
}

- (CGFloat)heightOfHeaderInScrollMenu:(YANScrollMenu *)menu{
    return  0;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:[CYTabBarConfig shared].selectedTextColor];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    UIViewController *VC = [UIViewController new];
    VC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark -
#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"ChildListenTableViewCell";
    ChildListenTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[ ChildListenTableViewCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    
    cell.block = ^(NSInteger index) {
        ZPLog(@"%ld",(long)index);
        ViewController *VC = [ViewController new];
        //        VC.view.backgroundColor = [UIColor whiteColor];
        [self.navigationController pushViewController:VC animated:YES];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController *VC = [UIViewController new];
    VC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:VC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kScale(130);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
