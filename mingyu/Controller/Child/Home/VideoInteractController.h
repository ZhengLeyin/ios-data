//
//  VideoInteractController.h
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^commenButtonBlock)(void);

@interface VideoInteractController : UIViewController

@property (nonatomic, assign) NSInteger video_id;
@property (nonatomic, copy)commenButtonBlock  commenblock;

@end
