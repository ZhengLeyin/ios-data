//
//  ParentHomeHeaderView.m
//  mingyu
//
//  Created by apple on 2018/4/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentHomeHeaderView.h"


@implementation ParentHomeHeaderView
{
    SDCycleScrollView *cycleView;
    int i;

}

- (void)awakeFromNib{
    [super awakeFromNib];
    NSArray *images = @[@"http://twt.img.iwala.net/upload/118c59e1b27fd575.jpg",
                        @"http://twt.img.iwala.net/upload/357797c67b3f7033.jpg",
                        @"http://twt.img.iwala.net/upload/a9a69883ba624e67.jpg",
                        @"http://twt.img.iwala.net/upload/858913320bff6162.jpg",
                        @"http://twt.img.iwala.net/upload/21359667942b4065.jpg",
                        @"http://twt.img.iwala.net/upload/93f0689ec10d5033.jpg"];
    cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 0, kScreenWidth-30, _bannerView.height) delegate:self placeholderImage:ImageName(@"placeholderImage")];

    cycleView.layer.cornerRadius = 5;
    cycleView.layer.masksToBounds  = YES;
    cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    // 同上做一个 其他图片
    cycleView.pageDotImage = [UIImage imageWithColor:[UIColor colorWithHexString:@"ffffff" alpha:0.5] forSize:CGSizeMake(15, 2)];
    // 一个Category给用颜色做ImageView 用15宽2高做一个长方形图片 当前图片
    cycleView.currentPageDotImage = [UIImage imageWithColor:[UIColor whiteColor] forSize:CGSizeMake(15, 2)];
    // 加载网络数组图片 我个人认为这个就有点坑了，理论上这里只能给网络加载的图片实现轮播，但是如果你要DIY一些图片上的文字，就要修改源码了
    cycleView.imageURLStringsGroup = images;
    
    [_bannerView addSubview:cycleView];
    
    
    _recordView.layer.cornerRadius = 18;
    _recordView.layer.borderWidth = 0.5;
    _recordView.layer.borderColor = [UIColor colorWithHexString:@"eeeeee"].CGColor;
    _recordView.layer.masksToBounds = YES;
    
    NSString *baby_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDbabyhead]];
    [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:baby_url] forState:0 placeholderImage:ImageName(@"默认头像")];
//    if([userDefault objectForKey:KUDbabyhead]){
//        [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:[userDefault objectForKey:KUDbabyhead]] forState:UIControlStateNormal];
//    }

    _BabyRecordArray = [NSMutableArray array];
    
    [self getReasource];
}



- (IBAction)Button1:(id)sender {
    ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
    VC.title = @"育儿必读";
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

- (IBAction)Button2:(id)sender {
//    [[self viewController].navigationController pushViewController:[QuickListenViewController new] animated:YES];
    [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
}

- (IBAction)Button3:(id)sender {
    ParentCourseViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentCourseViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

- (IBAction)Button4:(id)sender {
    
}

- (IBAction)Button5:(id)sender {
    BabyVaccineViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"BabyVaccineViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}



//跟换宝宝图像
- (IBAction)babyiconButton:(id)sender {
    
    [[self viewController].navigationController presentViewController:self.imagePickerVc animated:YES completion:nil];
}




- (IBAction)leftButton:(id)sender {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    NSInteger day = (currentTime - data)/3600/24;
    
    if (day+i+260 > 0) {
        i--;
        [self setViewwithDic:_BabyRecordArray[day+i+260]];
    }
}



- (IBAction)rightButton:(id)sender {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    NSInteger day = (currentTime - data)/3600/24;
    if (day+i+260 < _BabyRecordArray.count ) {
        [self setViewwithDic:_BabyRecordArray[day+i+260]];
        i++;
    }
}



- (void)getReasource{

    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    
    if (!array) {
        [self getBabyRecordList];
    } else {
        _BabyRecordArray = array;
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        NSInteger day = (currentTime - data)/3600/24;
        if (_BabyRecordArray.count > 260+day ) {
            [self setViewwithDic:_BabyRecordArray[260+day]];
        }
    }
}



- (void)getBabyRecordList{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetBabyRecordList];
    NSDictionary *parameters = @{
                                 @"start":@"-260",
                                 @"size":@"800"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _BabyRecordArray = json[@"data"];
            
            NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
            if ([self.BabyRecordArray writeToFile:filePatch atomically:YES]){
                ZPLog(@"sucess");
            }

            NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            // 创建时间戳(后台返回的时间 一般是13位数字)
            NSInteger day = (currentTime - data)/3600/24;
            if (_BabyRecordArray.count > 260+day ) {
                [self setViewwithDic:_BabyRecordArray[260+day]];
            }
        } else{
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
     } failure:^(NSError *error) {
//         [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
     }];
}


- (void)setViewwithDic:(NSDictionary *)dic{

    _babyLength_lab.text = [NSString stringWithFormat:@"平均身高:%@",dic[@"baby_length"]?dic[@"baby_length"]:@"暂无"];
    _babyWeight_lab.text = [NSString stringWithFormat:@"平均体重:%@",dic[@"baby_weight"]?dic[@"baby_weight"]:@"暂无"];
    _content_lab.text = dic[@"content"];
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSString *datastr = [NSString stringWithFormat:@"%f",(currentTime+i*24*60*60)*1000];
    NSString *str = [NSString getCurrentTimes:@"MM月dd日" Time:datastr];
    _date_lab.text = str;
    

    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSDate *babydate = [NSDate dateWithTimeIntervalSince1970:data];
    
    NSTimeInterval time = currentTime+i*24*60*60;
    NSString *newstr = [NSString transformTime2:[NSString stringWithFormat:@"%f",time*1000]];
    NSTimeInterval newdata = [NSString transformDate:newstr];
    NSDate *newdatas = [NSDate dateWithTimeIntervalSince1970:newdata];
    
    NSInteger day = [dic[@"day_num"] integerValue];

//    ZPLog(@"%ld",day);
    if (day == 0) {
        _dayNum_lab.text = @"宝宝今天出生";
    } else if (day >= 0) {
        _dayNum_lab.text = [self numberOfDaysWithFromDate:babydate toDate:newdatas];
    } else {
        NSInteger weak = floorf(-day/7);
        NSInteger days = fmodl(-day, 7);
        NSString *weakstr = @"";
        NSString *daysstr = @"";

        if (weak) {
            weakstr = [NSString stringWithFormat:@"%ld周",weak];
        }
        if (days) {
            daysstr = [NSString stringWithFormat:@"%ld天",days];
        }
        _dayNum_lab.text = [NSString stringWithFormat:@"还剩%@%@出生",weakstr,daysstr];
    }
}



- (NSString *)numberOfDaysWithFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate { // 创建一个标准国际时间的日历
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierChinese]; // 可根据需要自己设置时区.
    NSDateComponents *comp = [calendar components: NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate:fromDate toDate:toDate options:NSCalendarWrapComponents];
    
    NSString *year = @"";
    NSString *month = @"";
    NSString *day = @"";
    if (comp.year) {
        year = [NSString stringWithFormat:@"%ld岁",(long)comp.year];
    }
    if (comp.month) {
        month = [NSString stringWithFormat:@"%ld月",(long)comp.month];
    }
    if (comp.day) {
        day =  [NSString stringWithFormat:@"%ld天",(long)comp.day];
    }
    return [NSString stringWithFormat:@"%@%@%@",year,month,day];
}
    


- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    
}



-(TZImagePickerController *)imagePickerVc {
    _imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    _imagePickerVc.allowPickingOriginalPhoto = NO;
    _imagePickerVc.naviTitleColor = [UIColor blackColor];
    _imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"50D0F4"];
    _imagePickerVc.isStatusBarDefault = YES;
    _imagePickerVc.delegate = self;
    _imagePickerVc.allowTakePicture = YES;
    _imagePickerVc.allowPickingVideo = NO;
    _imagePickerVc.allowPickingGif = NO;
    _imagePickerVc.naviBgColor = [UIColor whiteColor];
    _imagePickerVc.maxImagesCount = 1;
    _imagePickerVc.showSelectBtn = NO;
    _imagePickerVc.allowCrop = YES;
    _imagePickerVc.cropRect = CGRectMake((kScreenWidth-240)/2 , (kScreenHeight-240)/2, 240, 240);
    return _imagePickerVc;
}



#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    if (photos) {
        [self uploadImage:photos[0]];
    }
}



//上传图像
- (void)uploadImage:(UIImage *)image{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?type=baby",KURL,KImgUpload];
    
    // 1.创建请求管理者
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",
                                                         @"multipart/form-data",
                                                         nil];
    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:[userDefault objectForKey:@"token"] forHTTPHeaderField:@"token"];   //token
    [manager.requestSerializer setValue:[userDefault objectForKey:@"device_id"] forHTTPHeaderField:@"device_id"];
    [manager.requestSerializer setValue:[HttpRequest getsignwithURL:URL] forHTTPHeaderField:@"sign"];
    
    [manager POST:URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0 );
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        NSString *name = @"file";
        [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
        ZPLog(@"%@",formData);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        ZPLog(@"%@",json);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self uploadUserHead:json[@"data"]];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ZPLog(@"%@",error);
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        
    }];
}


//更新用户图像
- (void)uploadUserHead:(NSString *)userhead{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"baby_head":userhead,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSString *url = [NSString stringWithFormat:@"%@%@",KKaptcha,userhead];
            [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:ImageName(@"默认头像")];
            [userDefault setObject:userhead forKey:KUDbabyhead];
            [userDefault synchronize];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}


@end
