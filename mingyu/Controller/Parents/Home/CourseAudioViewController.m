//
//  CourseAudioViewController.m
//  mingyu
//
//  Created by apple on 2018/6/5.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CourseAudioViewController.h"
#import "VideoHeaderView.h"
#import "LSContentViewController.h"
#import "LSCommentViewController.h"
#import "UINavigationController+NavAlpha.h"
#import "LSDHeaderView.h"
#import "RotateButton.h"
#import "UIView+CLSetRect.h"
#import "CLInputToolbar.h"
#import "CVDetailViewController.h"
#import "CVCommentViewController.h"
/**收银台*/
#import "CashierViewController.h"
/**育币充值*/
#import "MingYuRechargeViewController.h"
#import "JXCategoryView.h"
#import "JXPagerView.h"

#define TopViewH    kScreenWidth*9/16
#define contentViewH 118
@interface CourseAudioViewController ()<DFPlayerDelegate,DFPlayerDataSource,UIAlertViewDelegate,BottomCommentDelegate,JXCategoryViewDelegate,JXPagerViewDelegate>
@property (nonatomic, strong) JXPagerView *pagerView;
@property (nonatomic, strong) NSArray *listViewArray;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property (nonatomic, strong) LSDHeaderView *topView;
@property (nonatomic, strong) VideoHeaderView *contentView;   //标题 介绍 收藏 下载 view
@property (nonatomic, strong) NSMutableArray *audioListArray;
@property (nonatomic, strong) NSMutableArray    *df_ModelArray;
@property (nonatomic, strong) LSContentViewController *ContentViewC;
@property (nonatomic, strong) RotateButton *playbutton;
@property (nonatomic, strong) NSMutableArray *playerModelArray;

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong)CVDetailViewController *CVDetailVC;
@property (nonatomic, strong) UIButton *payButton;  //支付按钮
/**头部信息Model*/
@property (nonatomic, strong) CourseModel *courseHeadermodel;

@end

@implementation CourseAudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /**监听支付成功返回刷新界面*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentSuccessNotification) name:NSNotificationPaymentSuccessNotification object:nil];
    /**监听登录成功返回刷新界面*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetCourseCourseId) name:NSNotificationUserLogin object:nil];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    self.edgesForExtendedLayout = UIRectEdgeAll;

    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.topView];
    [self setupNavigationItem];
    @weakify(self)
    _topView.playButtonBlock = ^{
        @strongify(self)
        [self.playbutton startRotating];
        [self.playbutton resumeRotate];

        if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
            if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _coursemodel.course_id) {
                [self.topView.playButton setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                [[DFPlayer shareInstance] df_audioPause];
                [self.playbutton stopRotating];
            } else if (self.audioListArray && self.audioListArray.count > 0){
                [self.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                [self archiveObjectwithlistenbook:self.audioListArray];    // 归档
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
            }
        } else{
            if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _coursemodel.course_id) {
                [self.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                [[DFPlayer shareInstance] df_audioPlay];
            } else if (self.audioListArray && self.audioListArray.count > 0){
                [self archiveObjectwithlistenbook:self.audioListArray];    // 归档
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                [self.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
            }
        }
    };
    
    /**获取头部信息*/
    [self GetCourseCourseId];
}

-(void)paymentSuccessNotification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD setImageViewSize:CGSizeMake(40, 40)];
        [SVProgressHUD setMinimumSize:CGSizeMake(160, 102)];
        [SVProgressHUD setMinimumDismissTimeInterval:2];
        if (_courseHeadermodel.course_prerogative == 1 && (_courseHeadermodel.user_prerogative = 1)) {
            [SVProgressHUD showImage:[UIImage imageNamed:@"icon_popup_medal"] status:@"  恭喜您，领取成功！ "];
        }else {
            [SVProgressHUD showImage:[UIImage imageNamed:@"buyCourseSuccess"] status:@"  恭喜您，支付成功！ "];
        }
    });
    [self GetCourseCourseId];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initDFPlayer];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
//    if (![DFPlayer shareInstance].currentAudioModel) {
//        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
//        if ([DFPlayer shareInstance].previousAudioModel.audio_image_url) {
//            [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].previousAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
//            self.playbutton.hidden = NO;
//        } else {
//            self.playbutton.hidden = YES;
//        }
//    } else{
//        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
//        self.playbutton.hidden = NO;
//    }
    
    if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
        self.playbutton.hidden = NO;
        [self.view bringSubviewToFront:self.playbutton];
        [_playbutton startRotating];
        [_playbutton resumeRotate];
        if ([DFPlayer shareInstance].currentAudioModel.audio_type == 2 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _coursemodel.course_id) {
            [_topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
        } else if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _coursemodel.course_id){
            [_topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
        }
    } else {
        self.playbutton.hidden = YES;
        [_playbutton stopRotating];
        [_topView.playButton setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault; //设置状态栏颜色为黑色
}


#pragma mark - 初始化DFPlayer
- (void)initDFPlayer{
    [DFPlayer shareInstance].dataSource  = self;
    [DFPlayer shareInstance].delegate    = self;
}


#pragma mark - 支付按钮
-(UIButton *)payButton {
    if (!_payButton) {
        _payButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _payButton.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        _payButton.backgroundColor = [UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1];
        _payButton.titleLabel.font = FontSize(16.0);
        [_payButton setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    }
    return _payButton;
}



#pragma mark 支付按钮判断 ---加入特权课
-(void)setPayButton {
    if (_courseHeadermodel.present_price > 0 && (_courseHeadermodel.is_hold == 0)){
        [self.payButton removeTarget:self action:@selector(payButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.payButton addTarget:self action:@selector(payButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.payButton];
        if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
            /**本页面刷新数据*/
            NSString *payString = [NSString stringWithFormat:@"( %.1f育币 ) 领取特权课", _courseHeadermodel.present_price];
            [_payButton setTitle:payString forState:(UIControlStateNormal)];
            
            NSString * priceLength = [NSString stringWithFormat:@"%.1f",_courseHeadermodel.present_price];
            NSString *oldPrice = payString;
            NSUInteger length = [oldPrice length];
            NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:oldPrice];
            [attri addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)} range:NSMakeRange(2,  [priceLength length]+1)];
            [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(2, [priceLength length]+1)];
            [attri addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, length)];
            [_payButton setAttributedTitle:attri forState:(UIControlStateNormal)];
        }else {
            /**跳转支付跳转*/
            NSString *payString = [NSString stringWithFormat:@"( %.1f育币 ) 立即支付", _courseHeadermodel.present_price];
            [_payButton setTitle:payString forState:(UIControlStateNormal)];
        }
    }else {
        self.payButton.backgroundColor = [UIColor clearColor];
        self.payButton.height = 0;
        self.payButton.hidden  = YES;
        self.payButton = nil;
        [self.payButton removeAllSubviews];
    }
}

- (void)payButtonClick{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
        [self getCourseUser];
    } else {
        /**收银台*/
        CashierViewController *vc = [CashierViewController new];
        vc.coursemodel = self.coursemodel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark 获取头部信息(点赞+播放器次数+收藏等数据)
- (void)GetCourseCourseId {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/courseHead?",KURL,KGetCourseCourseId,@(_coursemodel.course_id)];
    
    NSDictionary *parameters = @{
                                 @"userId":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            _courseHeadermodel = [CourseModel modelWithJSON:data];
            self.contentView.coursemodel = _courseHeadermodel;
            if (_courseHeadermodel.present_price == 0) {
                _courseHeadermodel.is_hold = YES;
            }
            self.contentView.coursemodel.course_id = _coursemodel.course_id;
            NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_courseHeadermodel.section_img];
            [self.topView.backgroundImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
            CGFloat H = [self calculateString:_courseHeadermodel.course_title Width:18]-21.5;
            self.contentView.height = contentViewH+H;
            self.bottomView.haveCollect = _courseHeadermodel.is_collect_true;

#pragma mark 根据支付结果时候隐藏支付按钮,改变约束
            [self initViewControllers];
#pragma mark 底部评论框 收藏 分享
            [self.view addSubview:self.bottomView];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-effectViewH, kScreenWidth, effectViewH)];
            view.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:view];
            
            [self setTextViewToolbar];
#pragma mark 支付按钮判断
            [self setPayButton];
#pragma mark  获取课程目录
            [self GetCourseSection];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //         [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark  获取课程目录
- (void)GetCourseSection{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KGetCourseSection,@(_coursemodel.course_id)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@"0",
                                 @"size":@"10000"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *audioList = json[@"data"];
            [self.audioListArray removeAllObjects];
            for (NSDictionary *dic in audioList) {
                AudioModel *model = [AudioModel modelWithJSON:dic];
                model.fk_parent_title = _coursemodel.course_title;
                NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,dic[@"section_img"]]];
                model.audio_image_url = url;
                model.share_url = _coursemodel.share_url;
                model.audio_id = [dic[@"section_id"] integerValue];
                model.audio_path = [NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"section_path"]]];
                model.audio_title = dic[@"section_title"];
                model.is_free = [dic[@"is_free"] integerValue];
                model.audio_sort = [dic[@"serial_number"] integerValue];
                model.audio_type = 3;
                model.fk_parent_id = _coursemodel.course_id;
                [self.audioListArray addObject:model];
            }

            [self archiveObjectwithlistenbook:self.audioListArray];
            _ContentViewC.audioListArray = self.audioListArray;

//            if ([DFPlayer shareInstance].currentAudioModel.fk_parent_id == _coursemodel.course_id) {
////                [self.scrollView setContentOffset:CGPointMake(0,120) animated:YES];
//            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-30, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.height;
}


#pragma mark - 添加评论
- (void)addVideoComment:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":@(_coursemodel.course_id)
                          };
    
    NSDictionary *parameters = [NSDictionary dictionary];
    parameters = @{
                   @"comment":[NSString convertToJsonData:dic],
                   @"comment_type":@(course_comment)
                   };
    
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HaveAddComment" object:nil userInfo:nil];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


/** 提示购买弹框 */
-(void)promptPurchaseAlertView {
    NSString *titleString;
    NSString *confirmString;
    if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
        titleString = @"无需购买该课程,请您立即领取该课程";
        confirmString = @"立即领取";
    }else  {
        titleString = @"需要购买该课程,才能观看哦";
        confirmString = @"立即支付";
    }
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:titleString delegate:self cancelButtonTitle:@"取消" otherButtonTitles:confirmString, nil];
    [alertView show];
}

/**特权课*/
-(void)getCourseUser {
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KCourseUser];
    NSDictionary *parameters = @{
                                 @"course_id":@(_courseHeadermodel.course_id)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            /**通知中心 --支付成功通知*/
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationPaymentSuccessNotification object:nil];
        }else {
            [UIView ShowInfo:TipCourseCollectionFailedMessage Inview:self.view];
        }
    } failure:^(NSError *error) {
        //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}


#pragma mark - BottomCommentDelegate
#pragma mark -收藏
- (void)collectButtonClicked{
    if (_courseHeadermodel.is_collect_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _courseHeadermodel.course_id],
                                     @"user_id":USER_ID,
                                     @"collect_type":@(collect_type_course)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _courseHeadermodel.is_collect_true = is_collect_true;
                _courseHeadermodel.collect_number++;
                [UIView ShowInfo:TipCollectSuccess Inview:self.view];
                [_contentView.collectButton setImage:ImageName(@"已收藏") forState:0];
                [_contentView.collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_courseHeadermodel.collect_number] forState:0];
                _bottomView.haveCollect = _courseHeadermodel.is_collect_true;
            }
        } failure:^(NSError *error) {
            //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        NSDictionary *parameters = @{
                                     @"idList":@(_courseHeadermodel.is_collect_true)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _courseHeadermodel.is_collect_true = 0;
                _courseHeadermodel.collect_number--;
                [UIView ShowInfo:TipUnCollectSuccess Inview:self.view];
                [_contentView.collectButton setImage:ImageName(@"收藏") forState:0];
                if (_courseHeadermodel.collect_number) {
                    [_contentView.collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_courseHeadermodel.collect_number] forState:0];
                } else {
                    [_contentView.collectButton setTitle:@"  收藏  " forState:0];
                }
                _bottomView.haveCollect = _courseHeadermodel.is_collect_true;
            }
        } failure:^(NSError *error) {
            //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}

#pragma mark -分享
- (void)shareButtonClicked{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _courseHeadermodel.course_title;
    //    model.descr = _courseHeadermodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    model.webpageUrl = _courseHeadermodel.share_url;
    model.accusationType = accusation_type_course;
    model.from_id = _coursemodel.course_id;
    model.shareType = share_course;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}




#pragma mark - 添加课程介绍 详情目录 课程评论
- (void)initViewControllers{
    __block typeof(self) WeakSelf = self;
    NSArray *titles = @[@"详情介绍", @"课程目录", @"课程评论"];
    
    _CVDetailVC = [[CVDetailViewController alloc] init];
    _CVDetailVC.type = _courseHeadermodel.is_hold;
    _CVDetailVC.coursemodel = _coursemodel;
    _CVDetailVC.PushBlock = ^(UIViewController *vc) {
        [WeakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    _ContentViewC = [[LSContentViewController alloc] init];
    _ContentViewC.type = _courseHeadermodel.is_hold;
    _ContentViewC.tableviewSelectIndex = ^(NSInteger index) {
        [WeakSelf archiveObjectwithlistenbook:WeakSelf.audioListArray];

        AudioModel *model = [WeakSelf.audioListArray objectAtIndex:index];
#pragma mark 课程目录播放逻辑--试看 URL存在,购买URL存在,不购买URL 为nil
        if (![[model.audio_path absoluteString] hasPrefix:@"http"]) {
            [WeakSelf promptPurchaseAlertView];
            return ;
        }
        if (model.audio_path) {
//            AudioModel *model = WeakSelf.df_ModelArray[index];
            [[DFPlayer shareInstance] df_audioPlay];
            if ([DFPlayer shareInstance].currentAudioModel.audio_id == model.audio_id) {
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [WeakSelf.navigationController presentViewController:VC animated:YES completion:nil];
            } else {
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:model.audioId];
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [WeakSelf.navigationController presentViewController:VC animated:YES completion:nil];
            }
        }
    };
    
    CVCommentViewController *CommentViewC = [[CVCommentViewController alloc] init];
    //    CommentViewC.type = _coursemodel.is_hold;
    CommentViewC.course_id = _courseHeadermodel.course_id;
    CommentViewC.PushBlock = ^(UIViewController *vc, BOOL toCourseList) {
        [WeakSelf.navigationController pushViewController:vc animated:YES];
        if (toCourseList) {
            //            侧滑返回时可直接返回到列表
            NSMutableArray *array = WeakSelf.navigationController.viewControllers.mutableCopy;
            [array removeObjectAtIndex:array.count-2];
            [WeakSelf.navigationController setViewControllers:array animated:NO];
        }
    };
    
    _listViewArray = @[_CVDetailVC, _ContentViewC, CommentViewC];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    self.categoryView.titles = titles;
    self.categoryView.titleLabelZoomScale = 1;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = [UIColor colorWithHexString:@"50D0F4"];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"767676"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.defaultSelectedIndex = 1;

    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorLineViewColor = [UIColor colorWithHexString:@"50D0F4"];
    lineView.indicatorLineWidth = 30;
    self.categoryView.indicators = @[lineView];
    
    _pagerView = [self preferredPagingView];
    [self.view addSubview:self.pagerView];
    [self.pagerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(TopViewH);
    }];
    self.categoryView.contentScrollView = self.pagerView.listContainerView.collectionView;

//    //扣边返回处理，下面的代码要加上
//    [self.pagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
//    [self.pagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    [self.view bringSubviewToFront:self.playbutton];

}


//监听点击事件 代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"你点击了取消");
    }else {
        if (_courseHeadermodel.course_prerogative == 1 && (_courseHeadermodel.user_prerogative == 1)) {
            /** 刷新本页数据 */
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            [self getCourseUser];
        }else {
            /**收银台*/
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            CashierViewController *vc = [CashierViewController new];
            vc.coursemodel = _coursemodel;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}



#pragma mark - 对音频课程归档
- (void)archiveObjectwithlistenbook:(NSArray *)audioArray{
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *string = [NSString stringWithFormat:@"archive_3_%ld",_coursemodel.course_id];
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *savearray = [NSMutableArray array];
    for (int i = 0; i < audioArray.count; i++) {
        AudioModel *model = audioArray[i];
        for (int j = 0; j < array.count; j++) {
            AudioModel *model2 = array[j];
            if (model.audio_id == model2.audio_id) {
                model.timeWithValue = model2.timeWithValue;
            }
        }
        if ([[model.audio_path absoluteString] hasPrefix:@"http"]) {
            [savearray addObject:model];
            //            self.audioListArray = savearray;
        }
    }
    if ([NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive]) {
        ZPLog(@"success");
    }
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）
}

#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.audioListArray.count; i++) {
        AudioModel *yourModel    = self.audioListArray[i];
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        if ([[yourModel.audio_path absoluteString] hasPrefix:@"http"]) {//网络音频
            model.audio_path  = yourModel.audio_path;
        }
        [_df_ModelArray addObject:model];
    }
    return self.df_ModelArray;
}


#pragma mark - DFPlayerDelegate  音频准备播放
- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    [_ContentViewC.tableView reloadData];
}


//旋转按钮
- (RotateButton *)playbutton{
    if (_playbutton == nil) {
        _playbutton = [RotateButton buttonWithType:UIButtonTypeCustom];
        _playbutton.frame = CGRectMake(kScreenWidth-85, kScreenHeight-effectViewH-85-46, 65, 65);
        [_playbutton setImage:ImageName(@"一键听_banner") forState:UIControlStateNormal];
        [self.view addSubview:_playbutton];
        @weakify (self)
        _playbutton.RotateButtonBlock = ^{
            @strongify (self)
            [self presentDetail];
        };
    }
    return _playbutton;
}


//跳转播放器页面
- (void)presentDetail{
    AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
    UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
}


#pragma mark - 评论输入框
-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    __weak __typeof(self) weakSelf = self;
    [[tap rac_gestureSignal] subscribeNext:^(id x) {
        [weakSelf.inputToolbar bounceToolbar];
        weakSelf.maskView.hidden = YES;
    }];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addVideoComment:text];
    }];
    [self.maskView addSubview:self.inputToolbar];
}






#pragma mark - 底部评论区域
- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _bottomView = apparray.firstObject;
        _bottomView.delegate = self;
        _bottomView.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        __weak __typeof(self) weakSelf = self;
        _bottomView.hidden = YES;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
        _bottomView.LoctionButtonBlock = ^{
            CVCommentViewController *CommentViewC = (CVCommentViewController *)weakSelf.listViewArray[2];
            [CommentViewC locaToComment];
        };
    }
    return _bottomView;
}


#pragma mark 导航栏返回键+分享功能
- (void)setupNavigationItem {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame =CGRectMake(5,NaviH-44, 60, 44);
    [backButton setImage:ImageName(@"back_white") forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(kScreenWidth-60-5,NaviH-44, 60, 44);
    [shareButton setImage:ImageName(@"更多_白色") forState:0];
    [shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareButton];
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)share{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _courseHeadermodel.course_title;
    //    model.descr = _courseHeadermodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    model.webpageUrl = _courseHeadermodel.share_url;
    model.accusationType = accusation_type_course;
    model.from_id = _coursemodel.course_id;
    model.shareType = share_course;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}

#pragma mark - 标题 介绍 收藏
//- (LSDHeaderView *)contentView{
//    if (!_contentView) {
//        _contentView = [LSDHeaderView theContentView];
//        _contentView.frame = CGRectMake(0, TopViewH-TopeffectViewH, kScreenWidth, contentViewH);
//    }
//    return _contentView;
//}

- (VideoHeaderView *)contentView {
    if (!_contentView) {
        _contentView = [[VideoHeaderView alloc] init];
        _contentView.frame = CGRectMake(0, TopViewH-TopeffectViewH, kScreenWidth, contentViewH);
    }
    return _contentView;
}

#pragma mark - 顶部view 图片 播放按钮
- (LSDHeaderView *)topView{
    if (_topView == nil) {
        _topView = [LSDHeaderView theTopView];
        _topView.frame = CGRectMake(0, 0, kScreenWidth, TopViewH);
    }
    return _topView;
}


/** 课程列表*/
- (NSMutableArray *)audioListArray{
    if (_audioListArray == nil) {
        _audioListArray = [NSMutableArray array];
    }
    return _audioListArray;
}


- (JXPagerView *)preferredPagingView {
    return [[JXPagerView alloc] initWithDelegate:self];
}


#pragma mark - JXPagerViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.contentView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return contentViewH;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 40;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSArray<id<JXPagerViewListViewDelegate>> *)listViewsInPagerView:(JXPagerView *)pagerView {
    return self.listViewArray;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    if (index == 2) {
        self.bottomView.hidden = NO;
        self.payButton.hidden = YES;
    } else {
        self.bottomView.hidden = YES;
        self.payButton.hidden = NO;
    }
}


@end
