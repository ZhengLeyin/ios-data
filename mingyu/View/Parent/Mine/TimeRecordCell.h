//
//  TimeRecordCell.h
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DiaryModel;

@interface TimeRecordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *albumButton;


@property (weak, nonatomic) IBOutlet UIImageView *markImage;

@property (weak, nonatomic) IBOutlet UIView *paddingView;

@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (nonatomic, strong) DiaryModel *diarymodel;


+ (instancetype)theTimeRecordHeaderView;

+ (instancetype)theTimeRecordCellWithTableView:(UITableView *)tableView;

+ (instancetype)theTimeRecordFooterView;

+ (instancetype)theTimeRecordCellSectionHeaderView;


+ (CGFloat)cellHeight;


@end
