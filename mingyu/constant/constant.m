//
//  constant.m
//  mingyu
//
//  Created by apple on 2018/7/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "constant.h"

NSNotificationName const NSNotificationPaymentSuccessNotification = @"NSNotificationPaymentSuccessNotification";

NSNotificationName const NSNotificationExpandNotification = @"NSNotificationExpandNotification";

NSNotificationName const NSNotificationShrinkNotification = @"NSNotificationShrinkNotification";

NSNotificationName const NSNotificationReceiveNotification = @"NSNotificationReceiveNotification";

NSNotificationName const NSNotificationReceiveSystemMessage = @"NSNotificationReceiveSystemMessage";

NSNotificationName const NSNotificationReceivePriVateMessage = @"NSNotificationReceivePriVateMessage";

NSNotificationName const NSNotificationSetTopTopic = @"NSNotificationSetTopTopic";

NSNotificationName const NSNotificationCancelTopTopic = @"NSNotificationCancelTopTopic";

NSNotificationName const NSNotificationAddCircle = @"NSNotificationAddCircle";

NSNotificationName const NSNotificationSelectIndex1 = @"NSNotificationSelectIndex1";

NSNotificationName const NSNotificationBirthdayChange = @"NSNotificationBirthdayChange";

NSNotificationName const NSNotificationAddTimeRecord = @"NSNotificationAddTimeRecord";

NSNotificationName const NSNotificationDeleteTimeRecord = @"NSNotificationDeleteTimeRecord";

NSNotificationName const NSNotificationDeletePhotoRecord = @"NSNotificationDeletePhotoRecord";

NSNotificationName const NSNotificationUserLogin = @"NSNotificationUserLogin";


@implementation constant

+ (NSString *)TransactionState:(TransactionState )state{
    switch (state) {
        case MODULE_AUDIO_TIMER: return @"MODULE_AUDIO_TIMER";
        case MODULE_ARTICLE_CLOSE: return @"MODULE_ARTICLE_CLOSE";
        case MODULE_CHILDCARE_CLOSE: return @"MODULE_CHILDCARE_CLOSE";
        case MODULE_RECOMMEND_CLICK: return @"MODULE_RECOMMEND_CLICK";
        case MODULE_VIDEO_CLOSE: return @"MODULE_VIDEO_CLOSE";

        default: return nil;
    }
}


//- (NSString*) convertToString:(FirstThreeAlpha) whichAlpha {
//    NSString *result = nil;
//
//    switch(whichAlpha) {
//        case a:
//            result = @"a";
//            break;
//        case b:
//            result = @"b";
//            break;
//        case c:
//            result = @"c";
//            break;
//
//        default:
//            result = @"unknown";
//    }
//
//    return result;
//}

@end
