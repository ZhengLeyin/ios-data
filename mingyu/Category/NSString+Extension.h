//
//  NSString+Extension.h
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>


@interface NSString (Extension)

//MD5加密
- (NSString *) md5HexDigest;


//Java后台同步MD5加密
+(NSString*)MD5:(NSString *)input;


// 手机号码验证正则表达式
+ (BOOL)validateMobile:(NSString *)mobileNum;


+ (NSString*)getCurrentTimes:(NSString *)format Time:(NSString *)sendtime;


//获取当前时间
+ (NSString*)getCurrentTimes:(NSString *)format;


//时间戳改为时间

+ (NSString *)transformTime:(NSString *)sendTime;   //  yyyy-MM-dd HH:mm

+ (NSString *)transformTime2:(NSString *)sendTime2;  //  yyyy-MM-dd

+ (NSString *)transformTime3:(NSString *)sendTime3;   //   HH:mm:ss

+ (NSString *)getWeekDayFordate:(NSString *)sendTime;

+ (NSString *)transformTime4:(NSString *)sendTime4;   // HH:mm

+ (NSString *)transformTime5:(NSString *)sendTime5;   //  yyyy-MM-dd HH:mm:ss

+ (NSString *)transformTime6:(NSString *)sendTime6;   //  MM-dd

+ (NSString *)transformTime7:(NSString *)sendTime7;   //  yyyy.MM.dd

/**返回更新时间 */
+ (NSString *)compareTimeForNow:(NSInteger)comment_time;

//时间改时间戳
+ (NSInteger )transformDate:(NSString *)sendTime;



//是否为银行卡
+ (BOOL) checkCardNo:(NSString*) cardNo;

//json转string
+ (NSString *)convertToJsonData:(NSDictionary *)dict;


+ (NSString *)connectUrl:(NSDictionary *)params url:(NSString *) urlLink;


/*
 *  判断用户输入的密码是否符合规范，符合规范的密码要求：
 1. 长度大于6位,小于20位
 2. 密码中必须同时包含数字和字母
 */
+(BOOL)judgePassWordLegal:(NSString *)pass;



/**
 搜索时关键字高亮

 @param totalString 总字符串
 @param substring 需要替换的字符串
 @return 返回替换后的字符串
 */
+ (NSMutableAttributedString *)stringWithHighLightSubstring:(NSString *)totalString substring:(NSString *)substring;


@end
