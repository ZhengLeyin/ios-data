//
//  CVDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CVDetailViewController.h"
#import "TeacherViewController.h"
/**老师介绍*/
#import "TeacherIntroduceCell.h"
#import "MarkNoteParser.h"
#import <WebKit/WebKit.h>

@interface CVDetailViewController ()<UITableViewDataSource,UITableViewDelegate,WKUIDelegate,WKNavigationDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) WKWebView *webView1;

@property (nonatomic, strong) WKWebView *webView2;


@end

@implementation CVDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (_type == 0) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 50);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);
        }
    }];
    [self GetViewDetail];
}

#pragma mark 获取详情介绍
- (void)GetViewDetail {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KGetCourseDetails,@(_coursemodel.course_id)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"course_id":@(_coursemodel.course_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
            NSDictionary *data = json[@"data"];
            CourseModel * coursemodel = [CourseModel modelWithJSON:data];
            [self setCoursemodel:coursemodel];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
        //[self.tableView reloadData];
    } failure:^(NSError *error) {
        // [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

- (void)setCoursemodel:(CourseModel *)coursemodel{
    _coursemodel = coursemodel;
    NSString *headerString = @"<header><meta name='viewport' content='width=device-width-16, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
    NSString *webviewText = @"<style>body{margin-left:30;margin-right:30;font:16px;line-height:30px;color:767676}</style>";

    NSString* introduce_result = [MarkNoteParser toHtml:_coursemodel.course_introduce imageWidth:kScreenWidth-60];
    [self.webView1 loadHTMLString:[NSString stringWithFormat:@"%@%@%@",headerString,webviewText,introduce_result] baseURL:nil];
    NSString* harvest_result = [MarkNoteParser toHtml:_coursemodel.course_harvest imageWidth:kScreenWidth-60];
    [self.webView2 loadHTMLString:[NSString stringWithFormat:@"%@%@%@",headerString,webviewText,harvest_result] baseURL:nil];
    
    
    [self.tableView reloadData];
}

    
  

//  页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    ZPLog(@"webViewDidFinishLoad");
    [webView evaluateJavaScript:@"document.body.scrollHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        //获取页面高度
        double webViewHeight = [result doubleValue];
        if (webView == self.webView1) {
            self.webView1.height = webViewHeight;
        } else {
            self.webView2.height = webViewHeight;
        }
        ZPLog(@"%f",webViewHeight);
        [self.tableView reloadData];
    }];
}





#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return 1;
    }
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.userInteractionEnabled = NO;
    
    if (section == 0) {
        [button setImage:ImageName(@"课程介绍") forState:UIControlStateNormal];
        return button;
    } else if (section == 1){
        [button setImage:ImageName(@"你将收获") forState:UIControlStateNormal];
        return button;
    } else{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        UIView *topview = [UIView new];
        topview.backgroundColor = [UIColor colorWithRed:247/255.0 green:248/255.0 blue:250/255.0 alpha:1];
        topview.frame = CGRectMake(0, 0, kScreenWidth, 10);
        [view addSubview:topview];
        /**线条view*/
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(topview.frame)+24, 4, 17)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#50D0F4"];
        [view addSubview:lineView];
        /**讲师介绍Label*/
        UILabel *teacherIntroduceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(lineView.frame) + 6, CGRectGetMaxY(topview.frame)+20, 80, 25)];
        teacherIntroduceLabel.text = @"讲师介绍";
        teacherIntroduceLabel.font = FontSize(18.0);
        teacherIntroduceLabel.textColor = [UIColor colorWithHexString:@"#2C2C2C"];
        [view addSubview:teacherIntroduceLabel];
        return view;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 2) {
        return 10+20+25;
    }
    return 60;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        self.webView1.mj_x = 30;
        return self.webView1;
    } else if (section == 1){
        self.webView2.mj_x = 30;
        return self.webView2;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return self.webView1.height;
    } else if (section == 1){
        return self.webView2.height;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        }
        [cell.contentView addSubview:self.webView1];
        self.webView1.mj_x = 30;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.section == 1){
        return nil;
    } else{
        TeacherIntroduceCell *cell = [TeacherIntroduceCell theTeacherIntroduceCellWithtableView:tableView];
        cell.model = _coursemodel;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.pushTeacherDetailsBlock = ^{
            [self pushTeacherDetailsVC];
        };
        cell.followButtonBlock = ^{
            [self followButtonAction];
        };

        return cell;
    }
}

#pragma mark 跳转老师详情界面
-(void)pushTeacherDetailsVC {
    
    TeacherViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"TeacherViewController"];
    VC.coursemodel = _coursemodel;
//    [self.navigationController pushViewController:VC animated:YES];
    if (self.PushBlock) {
        self.PushBlock(VC);
    }
}

#pragma mark 关注
-(void)followButtonAction {
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return self.webView1.height;
    } else if (indexPath.section == 1){
        return self.webView2.height;
    } else{
        return 110;
    }
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-60, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    return size.height;
}



//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 2) {
//        TeacherViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"TeacherViewController"];
//        VC.coursemodel = _coursemodel;
//        [self.navigationController pushViewController:VC animated:YES];
//    }
//
//}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}



- (WKWebView *)webView1{
    if (!_webView1) {
        _webView1 = [[WKWebView alloc]initWithFrame:CGRectMake(30, 0, kScreenWidth - 60, 10)];
        _webView1.UIDelegate = self;
        _webView1.navigationDelegate = self;
        _webView1.userInteractionEnabled = NO;
    }
    return _webView1;
}

- (WKWebView *)webView2{
    if (!_webView2) {
        _webView2 = [[WKWebView alloc]initWithFrame:CGRectMake(30, 0, kScreenWidth - 60, 10)];
        _webView2.UIDelegate = self;
        _webView2.navigationDelegate = self;
        _webView2.userInteractionEnabled = NO;
    }
    return _webView2;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}


@end
