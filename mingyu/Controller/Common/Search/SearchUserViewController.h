//
//  SearchUserViewController.h
//  mingyu
//
//  Created by apple on 2018/6/27.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchUserViewController : UIViewController

@property (nonatomic, copy) NSString *searchStr;

@end
