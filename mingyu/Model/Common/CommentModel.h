//
//  CommentModel.h
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CommentReplayModel;

@interface CommentModel : NSObject

/** 评论ID */
@property (nonatomic, assign) NSInteger comment_id;

/** 用户ID */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 评论内容 */
@property (nonatomic, copy) NSString *comment_content;

/** 目标ID */
@property (nonatomic, assign) NSInteger fk_from_id;

/** 评论内容 */
@property (nonatomic, copy) NSString *comment_time;

/** 用户名字 */
@property (nonatomic, copy) NSString *comment_name;

/** 用户头像 */
@property (nonatomic, copy) NSString *comment_head;

/** 点赞数 */
@property (nonatomic, assign) NSInteger praise_number;

/** 是否点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 回复数量 */
@property (nonatomic, assign) NSInteger replayNum;

/** 评论时间状态 */
@property (nonatomic, copy) NSString *comment_time_status;

/** 评论对应的回复信息 */
@property (nonatomic, strong) NSMutableArray *replayList;


@property(nonatomic ,assign)CGFloat height;

@property (nonatomic, assign) BOOL showMore;


@property (nonatomic, copy) NSString *news_title;
@property (nonatomic, assign) NSInteger news;




@end
