//
//  ParentArticleCell.m
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentArticleCell.h"

@implementation ParentArticleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

//收藏点赞
+ (instancetype)theCollertAndPraisCell{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentArticleCell" owner:nil options:nil][0];

}

//相关推荐header
+ (instancetype)theAboutArticleHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentArticleCell" owner:nil options:nil][1];
    
}

//相关推荐cell
+ (instancetype)theAboutArticleCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"AboutArticleCell";
    ParentArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentArticleCell" owner:nil options:nil][2];
    }
    return cell;
    
}


////设置收藏点赞
//- (void)setArticinfomodel:(ParentArticleModel *)articinfomodel{
//
//    _articinfomodel = articinfomodel;
//    _article_source_Lab.text = _articinfomodel.article_source;
//
//
//    [self changCollectButtonState:_articinfomodel];
//
//    [self changPraiseButtonState:_articinfomodel];
//}
//
////根据状态判断是否收藏
//- (void)changCollectButtonState:(ParentArticleModel *)articinfomodel{
//    //收藏
//    if (_articinfomodel.is_collect_true == 0) {
//        [_collect_Button setImage:ImageName(@"收藏") forState:UIControlStateNormal];
//    } else {
//        [_collect_Button setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
//    }
//    [_collect_Button setTitle:[NSString stringWithFormat:@"  收藏(%ld)  ",(long)_articinfomodel.collect_number] forState:UIControlStateNormal];
//}
//
////根据状态判断是否点赞
//- (void)changPraiseButtonState:(ParentArticleModel *)articinfomodel{
////    //点赞
//    if (_articinfomodel.click_praise_true == 0) {
//        [_praise_button setImage:ImageName(@"点赞") forState:UIControlStateNormal];
//    } else {
//        [_praise_button setImage:ImageName(@"已赞") forState:UIControlStateNormal];
//    }
//    [_praise_button setTitle:[NSString stringWithFormat:@"  赞(%ld)  ",(long)_articinfomodel.praise_number] forState:UIControlStateNormal];
//}


////设置相关推荐cell
//- (void)setArticlemodel:(ParentArticleModel *)articlemodel{
//    _articlemodel = articlemodel;
////
//    _titleLab.text = _articlemodel.article_title;
//    _contentLab.text = _articlemodel.article_abstract;
////
//    _sourceLab.text = _articlemodel.article_source;
//    [_pageviewButton setTitle:[NSString stringWithFormat:@"%ld",(long)_articlemodel.page_view] forState:UIControlStateNormal];
//
//    NSInteger Length = _articlemodel.imageList.count;
//    if (Length == 0) {
//        _LayoutConstraintHeight.constant = 0;
//    } else if (Length == 1){
//        _LayoutConstraintHeight.constant = kScale(220);
//    } else  if (Length == 2){
//        _LayoutConstraintHeight.constant = kScale(161);
//    } else{
//        _LayoutConstraintHeight.constant = kScale(106);
//    }
//    [_imgcontentView removeAllSubviews];
//    for (int i = 0; i < Length; i++) {
//        if (Length > 3) return;
//        NSDictionary *dic = _articlemodel.imageList[i];
//        ParentArticleImageModel *model = [ParentArticleImageModel modelWithJSON:dic];
//        CGFloat W = ((kScreenWidth -34) - 10*(Length-1))/Length;
//        UIImageView *imageview = [[UIImageView alloc] init];
//        imageview.layer.cornerRadius = 5;
//        imageview.layer.masksToBounds = YES;
//        imageview.contentMode = UIViewContentModeScaleAspectFill;
////        NSString *URLStr = [NSString stringWithFormat:@"%@%@",KIMGURL,model.image_name];
//        [imageview sd_setImageWithURL:[NSURL URLWithString:model.image_name] placeholderImage:nil];
//        [_imgcontentView addSubview:imageview];
//
//        [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self.imgcontentView);
//            make.left.mas_equalTo(i*(W+10));
//            make.width.mas_equalTo(W);
//
//        }];
//    }
//}
//
////搜索时用到
//- (void)showSearch{
//    NSString *title = _articlemodel.article_title;
//    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
//    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
//    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
//    title = [title stringByAppendingString:@"</font>"];
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _titleLab.attributedText = attrStr;
//    _titleLab.lineBreakMode = NSLineBreakByTruncatingTail;
//
//    NSString *content = _articlemodel.article_abstract;
//    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
//    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
//    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
//    content = [content stringByAppendingString:@"</font>"];
//    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _contentLab.attributedText = attrStr2;
//    _contentLab.lineBreakMode = NSLineBreakByTruncatingTail;
//
//}
//

////收藏资讯
//- (IBAction)CollectButton:(id)sender {
//    
//    if (_articinfomodel.is_collect_true == 0) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
//        NSDictionary *parameters = @{
//                                     @"from_id":[NSString stringWithFormat:@"%ld", (long)_articinfomodel.article_id],
//                                     @"user_id":USER_ID,
//                                     @"collect_type":@(collect_type_news)
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger is_collect_true = [json[@"data"] integerValue];
//                _articinfomodel.is_collect_true = is_collect_true;
//                _articinfomodel.collect_number++;
//                [self changCollectButtonState:_articinfomodel];
//                [UIView ShowInfo:@"收藏成功" Inview:[self viewController].view];
//                ParentArticleViewController *VC = (ParentArticleViewController *)[self viewController];
//                VC.bottomView.haveCollect = _articinfomodel.is_collect_true;
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
//        NSDictionary *parameters = @{
//                                     @"collect_id":[NSString stringWithFormat:@"%ld", (long)_articinfomodel.is_collect_true]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                _articinfomodel.is_collect_true = 0;
//                _articinfomodel.collect_number--;
//                [self changCollectButtonState:_articinfomodel];
//                [UIView ShowInfo:@"取消成功" Inview:[self viewController].view];
//                ParentArticleViewController *VC = (ParentArticleViewController *)[self viewController];
//                VC.bottomView.haveCollect = _articinfomodel.is_collect_true;
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    }
//}
//
////点赞资讯
//- (IBAction)PraiseButton:(id)sender {
//    if (_articinfomodel.click_praise_true == 0) {
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
//        NSDictionary *parameters = @{
//                                     @"from_id":[NSString stringWithFormat:@"%ld", (long)_articinfomodel.article_id],
//                                     @"user_id":USER_ID,
//                                     @"praise_type":@"4"
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//                NSInteger click_praise_true = [json[@"data"] integerValue];
//                _articinfomodel.click_praise_true = click_praise_true;
//                _articinfomodel.praise_number++;
//                [self changPraiseButtonState:_articinfomodel];
//                [UIView ShowInfo:@"点赞成功" Inview:[self viewController].view];
//
//            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//            }
//        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//        }];
//    } else {
////        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelClickPraise];
////        NSDictionary *parameters = @{
////                                     @"praise_id":[NSString stringWithFormat:@"%ld", (long)_articinfomodel.click_praise_true]
////                                     };
////        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
////            NSDictionary *json = responseObject;
////            ZPLog(@"%@\n%@",json,json[@"message"]);
////            BOOL success = [json[@"success"] boolValue];
////            if (success) {
////                _articinfomodel.click_praise_true = 0;
////                _articinfomodel.praise_number--;
////                [self changPraiseButtonState:_articinfomodel];
////            } else {
////                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
////            }
////        } failure:^(NSError *error) {
////            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
////        }];
//        [UIView ShowInfo:@"请勿重复点赞" Inview:[self viewController].view];
//    }
//}


//不喜欢
- (IBAction)DisLikeButton:(id)sender {
    
}




@end
