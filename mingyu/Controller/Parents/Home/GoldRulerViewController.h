//
//  GoldRulerViewController.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LWXDetailController.h"

@interface GoldRulerViewController : LWXDetailController

@property (nonatomic, assign) NSInteger ideal_money;

@end
