//
//  TeacherTopicViewController.h
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseModel;

@interface TeacherTopicViewController : UIViewController

@property (nonatomic, strong) CourseModel *coursemodel;


@end
