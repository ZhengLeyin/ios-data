//
//  VideoContentCell.h
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoContentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *video_img;

@property (weak, nonatomic) IBOutlet UILabel *video_title;

@property (weak, nonatomic) IBOutlet UILabel *time_length;


@property (nonatomic, strong) VideoModel *videomodel;



@end
