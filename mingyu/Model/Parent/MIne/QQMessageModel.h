//
//  QQMessageModel.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QQMessageModel : NSObject


/** 消息id */
@property (nonatomic, assign) NSInteger message_id;

/** 私信人列表id */
@property (nonatomic, assign) NSInteger fk_qq_id;

/** 发送私信用户id */
@property (nonatomic, assign) NSInteger from_user_id;

/** 用户id */
@property (nonatomic, assign) NSInteger to_user_id;

/** 信息内容 */
@property (nonatomic, copy) NSString *message_content;

/** 创建时间 */
//@property (nonatomic, copy) NSString *create_time;

/** 聊天对象饿头像 */
@property (nonatomic, copy) NSString *user_header;

/** 时间 */
@property (nonatomic, copy) NSString *timeStatus;

@end
