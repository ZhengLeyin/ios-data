//
//  SideBySideVideosCell.m
//  mingyu
//
//  Created by MingYu on 2018/12/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "SideBySideVideosCell.h"
#import "SideBySideVideoCollectionCell.h"
static NSString * FunctionModuleCellID = @"SideBySideVideoCollectionCell";
@interface SideBySideVideosCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@end

@implementation SideBySideVideosCell

//初始化方法
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"SideBySideVideosCell";
    
    SideBySideVideosCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell)
    {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
          [self createUI];
    }
    return self;
}

/** 懒加载 */
-(UICollectionView *)collectionView {
    if (_collectionView == nil) {
        //创建一个layout布局类
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //设置每个item的大小为100*100
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat wh = (kScreenWidth - 55) / 2.0;
        layout.itemSize = CGSizeMake(wh, 100);
        //layout.minimumLineSpacing = 15;
        //layout.minimumInteritemSpacing = 15;
        layout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
        //创建collectionView 通过一个布局策略layout来创建
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,10,kScreenWidth, 100) collectionViewLayout:layout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

#pragma mark /**设置基本变量UICollectionView基本变量**/
-(void)setCollectionView {
    
    [self.contentView addSubview:self.collectionView];
    
    [self.collectionView registerNib:[UINib nibWithNibName:FunctionModuleCellID bundle:nil] forCellWithReuseIdentifier:FunctionModuleCellID];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView reloadData];
}

-(void)createUI {
    [self setCollectionView];
}

#pragma mark DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SideBySideVideoCollectionCell *cell =  [collectionView dequeueReusableCellWithReuseIdentifier:FunctionModuleCellID forIndexPath:indexPath];
               VideoModel *model = self.dataArray[indexPath.row];
               cell.videomodel = model;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray.count > indexPath.row) {
        VideoModel *model = self.dataArray[indexPath.row];
        ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
        VC.video_id = model.video_id;
        VC.video_id = 1425;
        [[self viewController].navigationController pushViewController:VC animated:YES];
    }
}


-(NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
   return  _dataArray;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
@end
