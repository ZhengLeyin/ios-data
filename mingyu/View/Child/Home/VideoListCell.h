//
//  VideoListCell.h
//  mingyu
//
//  Created by apple on 2018/4/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoListCell : UITableViewCell

@property (nonatomic, strong) UIView *BGView;

@property (nonatomic, strong) UILabel *videotitleLib;

@property (nonatomic, strong) UIImageView *chooseIMG;

- (void)setDownloadStatus:(YCDownloadStatus)status;


@end
