//
//  ParentChildCareCell.m
//  mingyu
//
//  Created by apple on 2018/5/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentChildCareCell.h"

@implementation ParentChildCareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _childcare_head_Image.contentMode = UIViewContentModeScaleAspectFill;
    
    _childcare_head_Image.layer.cornerRadius = 5;
    _childcare_head_Image.layer.borderWidth = 0.5;
    _childcare_head_Image.layer.borderColor = [UIColor colorWithHexString:@"e2e2e2"].CGColor;
    _childcare_head_Image.layer.masksToBounds = YES;
}



+ (instancetype)theChildCareCellWithtableView:(UITableView *)tableview{
    static NSString *cellid = @"ParentChildCareCell";
    ParentChildCareCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentChildCareCell" owner:nil options:nil][0];
        cell.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    }
    return cell;
}


- (void)setNewsmodel:(NewsModel *)newsmodel{
    _newsmodel = newsmodel;
    _childcare_title_Lab.text = newsmodel.news_title;
    _page_view_lab.text = [NSString stringWithFormat:@"%ld阅读",newsmodel.page_view];
    NSString *iamgeURL = [NSString stringWithFormat:@"%@%@",KKaptcha,newsmodel.news_head];
    [_childcare_head_Image sd_setImageWithURL:[NSURL URLWithString:iamgeURL] placeholderImage:ImageName(@"placeholderImage")];
}



- (void)showSearch{
    NSString *title = _newsmodel.news_title;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _childcare_title_Lab.attributedText = attrStr;
    _childcare_title_Lab.lineBreakMode = NSLineBreakByTruncatingTail;
    
//    NSString *content = _childecaremodel.childcare_abstract;
//    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
//    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
//    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
//    content = [content stringByAppendingString:@"</font>"];
//    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _childcare_content_Lab.attributedText = attrStr2;
//    _childcare_content_Lab.lineBreakMode = NSLineBreakByTruncatingTail;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}

@end
