//
//  CourseModel.h
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseModel : NSObject

/** 课程ID */
@property (nonatomic, assign) NSInteger course_id;

/**  用户ID  */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 课程标题 */
@property (nonatomic, copy) NSString *course_title;

/**  课程类型  1.视频课    2.音频课 */
@property (nonatomic, assign) NSInteger course_type;

/**  课程简介  */
@property (nonatomic, copy) NSString *course_abstract;

/**  课程图片  */
@property (nonatomic, copy) NSString *course_img;

/** 课程分类 */
@property (nonatomic, assign) NSInteger course_sort;

/** 创建时间 */
@property (nonatomic, copy) NSString *create_time;

/** 你的收获*/
@property (nonatomic, copy) NSString *course_harvest;

/** 课程介绍  */
@property (nonatomic, copy) NSString *course_introduce;

/** 作者名字 */
@property (nonatomic, copy) NSString *real_name;

/**组织 */
@property (nonatomic, copy) NSString *teacher_organize;


/** 头像 */
@property (nonatomic, strong) NSString *user_head;

/** 职位 */
@property (nonatomic, copy) NSString *teacher_position;

/** 老师名字 */
@property (nonatomic, copy) NSString *teacher_name;

/** 老师ID */
@property (nonatomic, assign) NSInteger teacher_id;

/**简介 */
@property (nonatomic, copy) NSString *teacher_profile;

/**  点赞数 */
@property (nonatomic, assign) NSInteger praise_number;

/**  收藏数 */
@property (nonatomic, assign) NSInteger collect_number;

/**  已收藏 */
@property (nonatomic, assign) NSInteger is_collect_true;

/**  已点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 总播放数 */
@property (nonatomic, assign) NSInteger play_count;

/** 折扣 */
@property (nonatomic, assign) CGFloat discount;

/** 最大金币折扣 */
@property (nonatomic, assign) CGFloat max_integral_scale;

/** 原价 */
@property (nonatomic, assign) CGFloat original_price;

/** 现价 */
@property (nonatomic, assign) CGFloat present_price;

/** 购买数量 */
@property (nonatomic, assign) NSUInteger hold_num;

/** e更新几期 */
@property (nonatomic, assign) NSInteger section_num;

@property (nonatomic, assign) NSInteger directoryNumber;

/**视频+音频 播放地址*/
@property (nonatomic, strong) NSString *section_path;

/**视频+音频 图片地址*/
@property (nonatomic, strong) NSString *section_img;

/** 是否已购买*/
@property (nonatomic, assign) BOOL is_hold;

/** 关注 */
@property (nonatomic, assign) NSUInteger follow_status;

/** 分享链接 */
@property (nonatomic, copy) NSString *share_url;

/** 已购课程-购买时间 */
@property (nonatomic, copy) NSString *time_tatus;

/** 已购课程-月份 */
@property (nonatomic, assign) NSInteger month;

/** 已购课程-年份 */
@property (nonatomic, assign) NSInteger year;

/**特权课 ---该课程判断*/
@property (nonatomic, assign) NSInteger course_prerogative;
/**特权课 ---该用户是不是名校堂用户*/
@property (nonatomic, assign) NSUInteger user_prerogative;
@end
