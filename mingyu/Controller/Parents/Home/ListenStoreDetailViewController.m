//
//  ListenStoreDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ListenStoreDetailViewController.h"
#import "LSDetailViewController.h"
#import "LSContentViewController.h"
#import "LSCommentViewController.h"
#import "UINavigationController+NavAlpha.h"
#import "LSDHeaderView.h"
#import "RotateButton.h"
#import "UIView+CLSetRect.h"
#import "CLInputToolbar.h"
#import "CVDetailViewController.h"


@interface ListenStoreDetailViewController ()<DFPlayerDelegate,DFPlayerDataSource>

@property (nonatomic, strong) LSDHeaderView *topView;
@property (nonatomic, strong) LSDHeaderView *contentView;   //标题 介绍 收藏 下载 view
@property (nonatomic, strong) NSMutableArray *audioListArray;
@property (nonatomic, strong) NSMutableArray    *df_ModelArray;
@property (nonatomic, strong) LSContentViewController *ContentViewC;
@property (nonatomic, strong) RotateButton *playbutton;
@property (nonatomic, strong) NSMutableArray *playerModelArray;

@property (nonatomic, strong) BottomCommetView *bottomView;
@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong)CVDetailViewController *CVDetailVC;
@property (nonatomic, strong) LSDetailViewController *LSDetailVC;

@property (nonatomic, strong) BookListModel *bookmodel;

@property (nonatomic, strong) CourseModel *coursemodel;




@end

@implementation ListenStoreDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;

    [self.view addSubview:self.topView];
    
    __block typeof(self) WeakSelf = self;
    _topView.playButtonBlock = ^{
        [WeakSelf.playbutton startRotating];
        [WeakSelf.playbutton resumeRotate];
        if (_audio_type == 2) {
            if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 2 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
                    [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_audioPause];
                    [WeakSelf.playbutton stopRotating];
                } else if (WeakSelf.audioListArray && WeakSelf.audioListArray.count > 0){
                    [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                }
            } else{
                [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 2 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
                    [[DFPlayer shareInstance] df_audioPlay];
                } else if (self.audioListArray && self.audioListArray.count > 0){
                    [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                }
            }
        } else if (_audio_type == 3){
            if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
                    [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_audioPause];
                    [WeakSelf.playbutton stopRotating];
                } else if (WeakSelf.audioListArray && WeakSelf.audioListArray.count > 0){
//                    if (WeakSelf.coursemodel.is_hold) {
                        [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
//                    }
                }
            } else{
                if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
                    [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
                    [[DFPlayer shareInstance] df_audioPlay];
                } else if (self.audioListArray && self.audioListArray.count > 0){
//                    if (WeakSelf.coursemodel.is_hold) {
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:0];
                        [WeakSelf.topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
//                    }
                }
            }
        }
    };
    
    //添加课程介绍。详情目录 课程评论
    self.tabPageController.tabBar.itemTitleFont = [UIFont systemFontOfSize:13];
    self.tabPageController.tabBar.indicatorScrollFollowContent = YES;
    self.tabPageController.defaultSelectedControllerIndex = 1;
    self.tabPageController.PlayerViewH = 164;
    [self initViewControllers];
//
    [self.view addSubview:self.bottomView];
    [self setTextViewToolbar];

    [self getAudioList];   //获取目录

    [self getDetaile];  //获取听书  课程 详情
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initDFPlayer];

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (@available(iOS 11.0, *)) {
        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if (![DFPlayer shareInstance].currentAudioModel) {
        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].previousAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    } else{
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    }
    
    if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [_playbutton startRotating];
        [_playbutton resumeRotate];
        if ([DFPlayer shareInstance].currentAudioModel.audio_type == 2 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
            [_topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
//            self.topView.contentLab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
        } else if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3 && [DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id){
            [_topView.playButton setImage:ImageName(@"一键听_主题详情_开") forState:UIControlStateNormal];
//            self.topView.contentLab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
        }
    } else {
        [_playbutton stopRotating];
        [_topView.playButton setImage:ImageName(@"一键听_主题详情_关") forState:UIControlStateNormal];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault; //设置状态栏颜色为黑色

//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.alpha = 1;
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
}


#pragma mark - 初始化DFPlayer
- (void)initDFPlayer{
    [DFPlayer shareInstance].dataSource  = self;
    [DFPlayer shareInstance].delegate    = self;
}

//获取详情
- (void)getDetaile{
    NSString *URL = [NSString string];
    if (_audio_type == 2) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KListenBook];
        NSDictionary *parameters = @{
                                     @"book_id":@(_target_id)
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else{
        URL =[ NSString stringWithFormat:@"%@%@?",KURL,KGetCourseDetails];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"course_id":@(_target_id)
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            if (_audio_type == 2) {
                BookListModel *bookmodel = [BookListModel modelWithJSON:data];
                _LSDetailVC.bookmodel = bookmodel;
            } else {
                CourseModel *coursemodel = [CourseModel modelWithJSON:data];
                _CVDetailVC.coursemodel = coursemodel;
            }
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];

    }];
}


- (void)getAudioList{
    NSString *URL = [NSString string];
    if (_audio_type == 2) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetBookAudioList];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"book_id":@(_target_id),
                                     @"start":@"0",
                                     @"size":@"1000"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else{
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCourseDirectory];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"course_id":@(_target_id),
                                     @"start":@"0",
                                     @"size":@"100"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            if (_audio_type == 2) {
                NSDictionary *listen_book = data[@"listen_book"];
                _bookmodel = [BookListModel modelWithJSON:listen_book];
                _bookmodel.directoryNumber = [data[@"directoryNumber"] integerValue];
                self.topView.booklistmodel = _bookmodel;
                self.contentView.booklistmodel = _bookmodel;
                
                NSArray *audioList = data[@"audioList"];
                for (NSDictionary *dic in audioList) {
                    AudioModel *model = [AudioModel modelWithJSON:dic];
                    model.fk_parent_title = listen_book[@"book_title"];
                    NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,listen_book[@"book_head"]]];
                    model.audio_image_url = url;
                    [self.audioListArray addObject:model];
                }
                _ContentViewC.yp_tabItemTitle = [NSString stringWithFormat:@"目录(%ld)",(long)_bookmodel.directoryNumber];

            } else {
                NSDictionary *course = data[@"course"];
                _coursemodel = [CourseModel modelWithJSON:course];
                _coursemodel.directoryNumber = [data[@"directoryNumber"] integerValue];
                self.topView.coursemodel = _coursemodel;
                self.contentView.coursemodel = _coursemodel;
                
                NSArray *audioList = data[@"audioList"];
                for (NSDictionary *dic in audioList) {
                    AudioModel *model = [AudioModel modelWithJSON:dic];
                    model.fk_parent_title = course[@"course_title"];
                    NSURL *url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,course[@"course_img"]]];
                    model.audio_image_url = url;
                    [self.audioListArray addObject:model];
                }
                _ContentViewC.yp_tabItemTitle = [NSString stringWithFormat:@"课程目录(%ld)",(long)_coursemodel.directoryNumber];
            }
           
            [self archiveObjectwithlistenbook:self.audioListArray];
            _ContentViewC.audioListArray = self.audioListArray;
            
            if ([DFPlayer shareInstance].currentAudioModel.fk_parent_id == _target_id) {
                [self.scrollView setContentOffset:CGPointMake(0,120) animated:YES];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];

    }];
}


- (void)archiveObjectwithlistenbook:(NSArray *)audioArray{
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];

    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *string = [NSString stringWithFormat:@"archive_%ld_%ld",_audio_type,_target_id];
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *savearray = [NSMutableArray array];
    for (int i = 0; i < audioArray.count; i++) {
        AudioModel *model = audioArray[i];
        for (int j = 0; j < array.count; j++) {
            AudioModel *model2 = array[j];
            if (model.audio_id == model2.audio_id) {
                model.timeWithValue = model2.timeWithValue;
            }
        }
        [savearray addObject:model];
        self.audioListArray = savearray;
    }
    if ([NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive]) {
        ZPLog(@"success");
    }
//    [NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive];//归档(序列化)
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）
}


#pragma mark - 添加课程介绍 详情目录 课程评论
- (void)initViewControllers{
    NSMutableArray *vcArray = [NSMutableArray array];
    if (_audio_type == 2) {
        _LSDetailVC = [[LSDetailViewController alloc] init];
        _LSDetailVC.yp_tabItemTitle = @"详情";
        [vcArray addObject:_LSDetailVC];
    } else {
        _CVDetailVC = [[CVDetailViewController alloc] init];
        _CVDetailVC.yp_tabItemTitle = @"详情介绍";
        [vcArray addObject:_CVDetailVC];
    }

    _ContentViewC = [[LSContentViewController alloc] init];
    if (_audio_type == 2) {
        _ContentViewC.yp_tabItemTitle = @"目录";
    } else {
        _ContentViewC.yp_tabItemTitle = @"课程目录";
    }
    __block typeof(self) WeakSelf = self;
    _ContentViewC.tableviewSelectIndex = ^(NSInteger index) {
        AudioModel *model = [WeakSelf.audioListArray objectAtIndex:index];
        if (model.audio_path) {
            AudioModel *model = WeakSelf.df_ModelArray[index];
            [[DFPlayer shareInstance] df_audioPlay];
            if ([DFPlayer shareInstance].currentAudioModel.audio_id == model.audio_id) {
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [WeakSelf.navigationController presentViewController:VC animated:YES completion:nil];
            } else {
                [[DFPlayer shareInstance] df_playerPlayWithAudioId:model.audioId];
                AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                [WeakSelf.navigationController presentViewController:VC animated:YES completion:nil];
            }
        }
    };
    [vcArray addObject:_ContentViewC];
    
    LSCommentViewController *CommentViewC = [[LSCommentViewController alloc] init];
    if (_audio_type == 2) {
        CommentViewC.yp_tabItemTitle = @"评论";
    } else {
        CommentViewC.yp_tabItemTitle = @"课程评论";
    }
    CommentViewC.audio_type = _audio_type;
    CommentViewC.target_id = _target_id;
    [vcArray addObject:CommentViewC];

    self.tabPageController.viewControllers = vcArray;
}


//切换tabPageController
- (void)didSelectViewControllerIndex:(NSUInteger)index{
    if (index == 2) {
        self.bottomView.hidden = NO;
    } else {
        self.bottomView.hidden = YES;
    }
}


#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.audioListArray.count; i++) {
        AudioModel *yourModel    = self.audioListArray[i];
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        if ([[yourModel.audio_path absoluteString] hasPrefix:@"http"]) {//网络音频
            model.audio_path  = yourModel.audio_path;
        }
//        else{//本地音频
//            NSString *path = [[NSBundle mainBundle] pathForResource:[yourModel.audio_path absoluteString] ofType:@""];
//            if (path) {model.audio_path = [NSURL fileURLWithPath:path];}
//        }
        [_df_ModelArray addObject:model];
    }
    return self.df_ModelArray;
}



- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
//    self.topView.contentLab.text = [DFPlayer shareInstance].currentAudioModel.audio_title;
    
    [_ContentViewC.tableView reloadData];
}



//状态信息代理
- (void)df_player:(DFPlayer *)player didGetStatusCode:(DFPlayerStatusCode)statusCode{
    if (statusCode == 0) {
        [self showAlertWithTitle:@"没有网络连接" message:nil yesBlock:nil];
    }else if(statusCode == 1){
        [self showAlertWithTitle:@"继续播放将产生流量费用" message:nil noBlock:nil yseBlock:^{
            [DFPlayer shareInstance].isObserveWWAN = NO;
            [[DFPlayer shareInstance] df_playerPlayWithAudioId:player.currentAudioModel.audioId];
        }];
        return;
    }else if(statusCode == 2){
        [self showAlertWithTitle:@"请求超时" message:nil yesBlock:nil];
    }else if(statusCode == 8){
//        [self tableViewReloadData];return;
    }
}


//旋转按钮
- (RotateButton *)playbutton{
    if (_playbutton == nil) {
        _playbutton = [RotateButton buttonWithType:UIButtonTypeCustom];
        _playbutton.frame = CGRectMake(kScreenWidth-85, kScreenHeight-85, 65, 65);
        [_playbutton setImage:ImageName(@"一键听_banner") forState:UIControlStateNormal];
        [_playbutton addTarget:self action:@selector(presentDetail) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_playbutton];
    }
    return _playbutton;
}


//跳转播放器页面
- (void)presentDetail{
    AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
    UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
    
//    if (audio_type == 1) {
//        PlayerDetailViewController *PVC = [[PlayerDetailViewController alloc] init];
//        UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
//        [self.navigationController presentViewController:VC animated:YES completion:nil];
//    } else{
//        LSPlayerDetailViewController *PVC = [[LSPlayerDetailViewController alloc] init];
//        UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
//        [self.navigationController presentViewController:VC animated:YES completion:nil];
//    }
}

-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addVideoComment:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
}


-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}

//添加听书 视屏课评论
- (void)addVideoComment:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":@(_target_id)
                          };

    NSDictionary *parameters = [NSDictionary dictionary];
    if (_audio_type == 2) {
        parameters = @{
                        @"comment":[NSString convertToJsonData:dic],
                        @"comment_type":@(listen_book_comment)
                        };
    } else {
        parameters = @{
                       @"comment":[NSString convertToJsonData:dic],
                       @"comment_type":@(course_comment)
                       };
    }
    
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HaveAddComment" object:nil userInfo:nil];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _bottomView = apparray.firstObject;
        _bottomView.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        __weak __typeof(self) weakSelf = self;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
    }
    return _bottomView;
}


#pragma mark - 标题 介绍 收藏
- (UIView *)headerView {
    return self.contentView;
}


- (LSDHeaderView *)contentView{
    if (!_contentView) {
        _contentView = [LSDHeaderView theContentView];
        _contentView.frame = CGRectMake(0, 164, kScreenWidth, 118);
    }
    return _contentView;
}

- (LSDHeaderView *)topView{
    if (_topView == nil) {
        _topView = [LSDHeaderView theTopView];
        _topView.frame = CGRectMake(0, 0, kScreenWidth, 164);
    }
    return _topView;
}


- (NSMutableArray *)audioListArray{
    if (_audioListArray == nil) {
        _audioListArray = [NSMutableArray array];
    }
    return _audioListArray;
}



@end
