//
//  RotateButton.m
//  mingyu
//
//  Created by apple on 2018/5/22.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "RotateButton.h"

@implementation RotateButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    self.imageView.layer.cornerRadius = 32;

    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 32;
    view.layer.shadowColor=[UIColor blackColor].CGColor;
    view.layer.shadowOffset=CGSizeMake(0,0);
    view.layer.shadowOpacity=0.5;
    view.tag = 110;
    if ([[self viewWithTag:110] isKindOfClass:[UIView class]] == YES) {
        
    }else {
        [self addSubview:view];
    }
    
    [self sendSubviewToBack:view];
}

//+ (RotateButton *)shareInstance {
//    static RotateButton *instance = nil;
//    static dispatch_once_t predicate;
//    dispatch_once(&predicate, ^{
//        instance = [[[self class] alloc] init];
//    });
//    return instance;
//}


- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.RotateButtonBlock) {
        self.RotateButtonBlock();
    }
}

// 开始旋转
-(void) startRotating {
    CABasicAnimation* rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotateAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    rotateAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2];   // 旋转一周
    rotateAnimation.duration = 20.0;                                 // 旋转时间20秒
    rotateAnimation.repeatCount = MAXFLOAT;                      // 重复次数，这里用最大次数
    
    [self.layer addAnimation:rotateAnimation forKey:nil];
    
}

// 停止旋转
-(void) stopRotating {
    
    CFTimeInterval pausedTime = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.layer.speed = 0.0;                                          // 停止旋转
    self.layer.timeOffset = pausedTime;                              // 保存时间，恢复旋转需要用到
}

// 恢复旋转
-(void) resumeRotate {
    
    if (self.layer.timeOffset == 0) {
        [self startRotating];
        return;
    }
    
    CFTimeInterval pausedTime = self.layer.timeOffset;
    self.layer.speed = 1.0;                                         // 开始旋转
    self.layer.timeOffset = 0.0;
    self.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;  // 恢复时间
    self.layer.beginTime = timeSincePause;                          // 从暂停的时间点开始旋转
}

@end
