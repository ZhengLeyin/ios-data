//
//  CashierViewController.h
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CashierViewController : UIViewController
@property (nonatomic, strong) CourseModel *coursemodel;
@end

NS_ASSUME_NONNULL_END
