//
//  HttpRequest.m
//  基于AFNetWorking的再封装
//
//  Created by  on 16/1/2.
//  Copyright © 2016年 . All rights reserved.
//

#import "HttpRequest.h"
#import "UploadParam.h"
#import <UMPush/UMessage.h>

@implementation HttpRequest

static id _instance = nil;
+ (instancetype)sharedInstance {
    return [[self alloc] init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

- (instancetype)init {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super init];
        AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
        [manager startMonitoring];
        [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                {
                    // 位置网络
                    ZPLog(@"位置网络");
                }
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                {
                    // 无法联网
                    ZPLog(@"无法联网");
                }
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                {
                    // 手机自带网络
                    ZPLog(@"当前使用的是2G/3G/4G网络");
                }
                    break;
                case AFNetworkReachabilityStatusReachableViaWWAN:
                {
                    // WIFI
                    ZPLog(@"当前在WIFI网络下");
                }
            }
        }];
    });
    return _instance;
}



//这个函数是判断网络是否可用的函数（wifi或者蜂窝数据可用，都返回YES）
+ (BOOL)NetWorkIsOK{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    if (manager.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable || manager.networkReachabilityStatus == AFNetworkReachabilityStatusUnknown ) {
        return NO;
    }
    return YES;
//    if(
//       ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus]
//        != NotReachable)
//       ||
//       ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus]
//        != NotReachable)
//       ){
//        return YES;
//    }else{
//        return NO;
//    }
}


//+ (BOOL)reachableViaWWAN{
//    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
//    if (manager.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable || manager.networkReachabilityStatus == AFNetworkReachabilityStatusUnknown ) {
//        return NO;
//    }
//    return YES;
//}

+ (BOOL)reachableViaWiFi{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    if (manager.networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
        return YES;
    }
    return NO;
}


#pragma mark -- GET请求 --
+ (void)getWithURLString:(NSString *)URLString
              parameters:(id)parameters
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure {
    
    [self getWithURLString:URLString parameters:parameters viewcontroller:nil success:success failure:failure];
}


+ (void)getWithURLString:(NSString *)URLString
              parameters:(id)parameters
          viewcontroller:(UIViewController *)vc
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure {
    
    [self getWithURLString:URLString parameters:parameters viewcontroller:vc showalert:YES success:success failure:failure];
}


+ (void)getWithURLString:(NSString *)URLString
              parameters:(id)parameters
          viewcontroller:(UIViewController *)vc
               showalert:(BOOL)showalert
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果类型
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    manager.responseSerializer = response;
    
    //申明请求的数据类型
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;  //取消缓存
    manager.requestSerializer.timeoutInterval = 30;
    //af设置请求头
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDtoken] forHTTPHeaderField:@"token"];       //token
    [manager.requestSerializer setValue:[self getsignwithURL:URLString] forHTTPHeaderField:@"sign"];             //签名
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDdevice_id] forHTTPHeaderField:@"device_id"]; //设备ID
    [manager.requestSerializer setValue:IS_IPHONE ? @"ios":@"iosPad" forHTTPHeaderField:@"device_type"];            //设备类型
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",USER_ID] forHTTPHeaderField:@"user_id"];   //用户ID
    NSString *version = [[[NSBundle mainBundle]infoDictionary]valueForKey:@"CFBundleShortVersionString"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"version"];
    [manager GET:URLString parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if (success) {
            BOOL successed = [responseObject[@"success"] boolValue];
//            ZPLog(@"%@",responseObject);
            if (successed) {
                if ([responseObject[@"message"] isEqualToString:@"resetting token"]) {
                    [userDefault setObject:responseObject[@"data"] forKey:KUDtoken];
                    [userDefault synchronize];
                    [self getWithURLString:URLString parameters:parameters viewcontroller:vc showalert:showalert success:success failure:failure];
                    return;
                }
            } else {
                if ([responseObject[@"errorCode"] isEqual: @"DEVICE_ID_ERROR"] || [responseObject[@"errorCode"] isEqual: @"ILLEGAL_TOKEN_INVALID"] || [responseObject[@"errorCode"] isEqual:@"INVALID_TOKEN"] || [responseObject[@"errorCode"] isEqual:@"OVERDUE_TOKEN"]) {
                    [userDefault setBool:NO forKey:KUDhasLogin];
                    NSString *alias = [NSString stringWithFormat:@"mingyu0"];
                    [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
                    
                    CodeLoginViewController *LoginVC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:LoginVC] animated:YES completion:nil];
                    return;
                }
            }
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        if (failure) {
            if (![self NetWorkIsOK] && showalert == YES) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            failure(error);
        }
    }];
}


#pragma mark -- POST请求 --
+ (void)postWithURLString:(NSString *)URLString
               parameters:(id)parameters
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure {
    
    [self postWithURLString:URLString parameters:parameters viewcontroller:nil success:success failure:failure];
}

+ (void)postWithURLString:(NSString *)URLString
               parameters:(id)parameters
           viewcontroller:(UIViewController *)vc
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure {
    
    [self postWithURLString:URLString parameters:parameters viewcontroller:vc showalert:YES success:success failure:failure];
}


+ (void)postWithURLString:(NSString *)URLString
               parameters:(id)parameters
           viewcontroller:(UIViewController *)vc
                showalert:(BOOL)showalert
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果类型
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    manager.responseSerializer = response;
    
    //申明请求的数据类型
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30;
    //af设置请求头
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDtoken] forHTTPHeaderField:@"token"];   //token
    [manager.requestSerializer setValue:[self getsignwithURL:URLString] forHTTPHeaderField:@"sign"];        //签名
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDdevice_id] forHTTPHeaderField:@"device_id"];   //设备ID
    [manager.requestSerializer setValue:IS_IPHONE ? @"ios":@"iosPad" forHTTPHeaderField:@"device_type"];            //设备类型
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",USER_ID] forHTTPHeaderField:@"user_id"];   //用户ID
    NSString *version = [[[NSBundle mainBundle]infoDictionary]valueForKey:@"CFBundleShortVersionString"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"version"];
    [manager POST:URLString parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if (success) {
            BOOL successed = [responseObject[@"success"] boolValue];
            //            ZPLog(@"%@",responseObject);
            if (successed) {
                if ([responseObject[@"message"] isEqualToString:@"resetting token"]) {
                    [userDefault setObject:responseObject[@"data"] forKey:KUDtoken];
                    [userDefault synchronize];
                    [self postWithURLString:URLString parameters:parameters viewcontroller:vc showalert:showalert success:success failure:failure];
                    return;
                }
            } else {
                if ([responseObject[@"errorCode"] isEqual: @"DEVICE_ID_ERROR"] || [responseObject[@"errorCode"] isEqual: @"ILLEGAL_TOKEN_INVALID"] || [responseObject[@"errorCode"] isEqual:@"INVALID_TOKEN"] || [responseObject[@"errorCode"] isEqual:@"OVERDUE_TOKEN"]) {
                    [userDefault setBool:NO forKey:KUDhasLogin];
                    NSString *alias = [NSString stringWithFormat:@"mingyu0"];
                    [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
                    
                    CodeLoginViewController *LoginVC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:LoginVC] animated:YES completion:nil];
                    return ;
                }
            }
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        if (failure) {
            if (![self NetWorkIsOK] && showalert == YES) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            failure(error);
        }
    }];
}

//生成签名  规则 ：（user_id+当前时间+phone）MD5
+ (NSString *)getsignwithURL:(NSString *)URLstr {
    
    NSString *time =  [NSString getCurrentTimes:@"yyyy-MM-dd"];
    NSString *sign = [NSString stringWithFormat:@"%@%@%@",URLstr,USER_ID,time];
    NSString *md5sign = [NSString MD5:sign];
    
//    ZPLog(@"sign=====%@",md5sign);
//    ZPLog(@"token====%@",[userDefault objectForKey:KUDtoken]);
//    ZPLog(@"device_id===%@",[userDefault objectForKey:KUDdevice_id]);
    return md5sign;
}



 #pragma mark -- delete请求 --
+ (void)deleteWithURLString:(NSString *)URLString
                 parameters:(id)parameters
             viewcontroller:(UIViewController *)vc
                  showalert:(BOOL)showalert
                    success:(void (^)(id responseObject))success
                    failure:(void (^)(NSError *error))failure{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //申明返回的结果类型
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    manager.responseSerializer = response;
    
    //申明请求的数据类型
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30;
    //af设置请求头
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDtoken] forHTTPHeaderField:@"token"];       //token
    [manager.requestSerializer setValue:[self getsignwithURL:URLString] forHTTPHeaderField:@"sign"];             //签名
    [manager.requestSerializer setValue:[userDefault objectForKey:KUDdevice_id] forHTTPHeaderField:@"device_id"]; //设备ID
    [manager.requestSerializer setValue:IS_IPHONE ? @"ios":@"iosPad" forHTTPHeaderField:@"device_type"];            //设备类型
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",USER_ID] forHTTPHeaderField:@"user_id"];   //用户ID
    NSString *version = [[[NSBundle mainBundle]infoDictionary]valueForKey:@"CFBundleShortVersionString"];
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"version"];
    [manager DELETE:URLString parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if (success) {
            BOOL successed = [responseObject[@"success"] boolValue];
            //            ZPLog(@"%@",responseObject);
            if (successed) {
                if ([responseObject[@"message"] isEqualToString:@"resetting token"]) {
                    [userDefault setObject:responseObject[@"data"] forKey:KUDtoken];
                    [userDefault synchronize];
                    [self deleteWithURLString:URLString parameters:parameters viewcontroller:vc showalert:showalert success:success failure:failure];
                    return;
                }
            } else {
                if ([responseObject[@"errorCode"] isEqual: @"DEVICE_ID_ERROR"] || [responseObject[@"errorCode"] isEqual: @"ILLEGAL_TOKEN_INVALID"] || [responseObject[@"errorCode"] isEqual:@"INVALID_TOKEN"] || [responseObject[@"errorCode"] isEqual:@"OVERDUE_TOKEN"]) {
                    [userDefault setBool:NO forKey:KUDhasLogin];
                    NSString *alias = [NSString stringWithFormat:@"mingyu0"];
                    [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
                    
                    CodeLoginViewController *LoginVC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:[[UINavigationController alloc] initWithRootViewController:LoginVC] animated:YES completion:nil];
                    return ;
                }
            }
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        if (failure) {
            if (![self NetWorkIsOK] && showalert == YES) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            failure(error);
        }
    }];
}




#pragma mark -- POST/GET网络请求 --
+ (void)requestWithURLString:(NSString *)URLString
                  parameters:(id)parameters
                        type:(HttpRequestType)type
                     success:(void (^)(id))success
                     failure:(void (^)(NSError *))failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    switch (type) {
        case HttpRequestTypeGet:
        {
            [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (success) {
                    success(responseObject);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (failure) {
                    failure(error);
                }
            }];
        }
            break;
        case HttpRequestTypePost:
        {
            [manager POST:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (success) {
                    success(responseObject);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (failure) {
                    failure(error);
                }
            }];
        }
            break;
    }
}

+ (void)uploadWithURLString:(NSString *)URLString parameters:(id)parameters uploadParam:(NSArray<UploadParam *> *)uploadParams success:(void (^)())success failure:(void (^)(NSError *))failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (UploadParam *uploadParam in uploadParams) {
            [formData appendPartWithFileData:uploadParam.data name:uploadParam.name fileName:uploadParam.filename mimeType:uploadParam.mimeType];
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark - 下载数据
- (void)downLoadWithURLString:(NSString *)URLString parameters:(id)parameters progerss:(void (^)())progress success:(void (^)())success failure:(void (^)(NSError *))failure {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    NSURLSessionDownloadTask *downLoadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return targetPath;
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (failure) {
            failure(error);
        }
    }];
    [downLoadTask resume];
}


/**
 * 上传多张图片
 */
+(void)requestAFURL:(NSString *)URLString
         parameters:(id)parameters
     imageDataArray:(NSArray *)imageDataArray
            succeed:(void (^)(id))succeed
            failure:(void (^)(NSError *))failure
{
    // 1.创建请求管理者
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // 2.申明返回的结果是二进制类型
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // 3.如果报接受类型不一致请替换一致text/html  或者 text/plain
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data", nil];
    
    // 4.请求超时，时间设置
    manager.requestSerializer.timeoutInterval = 30;
    
    // 5. POST数据
    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        for (int i = 0; i<imageDataArray.count; i++) {
            NSData *imageData = imageDataArray[0];
            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
            //            // 要解决此问题，
            //            // 可以在上传时使用当前的系统事件作为文件名
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //            // 设置时间格式
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
            NSString *name = @"file";
            //
            //            //将得到的二进制图片拼接到表单中 /** data,指定上传的二进制流;name,服务器端所需参数名*/
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
            ZPLog(@"%@",formData);
//        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString *responseStr =  [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        ZPLog(@"%@",responseStr);
        //
//        succeed([AFNManagerRequest dictionaryWithJsonString:responseStr]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);

    }];

}

@end
