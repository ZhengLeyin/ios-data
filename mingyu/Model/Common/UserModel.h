//
// UserModel.h
//  mingyu
//
//  Created by apple on 2018/4/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

/** 主键ID */
@property (nonatomic, assign) NSInteger user_id;

/** 名称 */
@property (nonatomic, copy) NSString *user_name;

/** 名字更改状态 */
@property (nonatomic, assign) NSInteger name_status;

/** 真实姓名 */
@property (nonatomic, copy) NSString *real_name;

/** 性别 */
@property (nonatomic, copy) NSString *user_sex;

/** 头像 */
@property (nonatomic, copy) NSString *user_head;

/** 现金余额 */
@property (nonatomic, assign) CGFloat user_money;

/** 金币 */
@property (nonatomic, assign) NSInteger user_gold;

/** 手机号 */
@property (nonatomic, copy) NSString *user_phone;

/** 设备唯一标识 */
@property (nonatomic, copy) NSString *device_id;

/** 微信唯一标识 */
@property (nonatomic, copy) NSString *open_id;

/** 是否绑定微信 */
@property (nonatomic, assign) NSInteger is_true_binding;

/** 密码 */
@property (nonatomic, copy) NSString *user_password;

/** 用户创建时间 */
@property (nonatomic, assign) NSInteger user_join_time;

/** 加密密码的盐 */
@property (nonatomic, copy) NSString *salt;

/** 用户状态,0:创建未认证（比如没有激活，没有输入验证码等等）--等待验证的用户 , 1:正常状态,2：用户被锁定. */
@property (nonatomic, assign) NSInteger state;

/** 所在地区 */
@property (nonatomic, copy) NSString *area_name;

/** 地区ID */
@property (nonatomic, assign) NSInteger fk_area_id;

/** 用户身份 */
@property (nonatomic, assign) NSInteger user_identity;

/** 用户生日 */
@property (nonatomic, copy) NSString *user_birthday;

/** 用户粉丝数 */
@property (nonatomic, assign) NSInteger followUserNumber;

/** 用户关注数 */
@property (nonatomic, assign) NSInteger userFollowNumber;

/** 用户总收藏数 */
@property (nonatomic, assign) NSInteger collectNum;

/** 发帖数 */
@property (nonatomic, assign) NSInteger topicNumber;

/** 被关注数 */
@property (nonatomic, assign) NSInteger follow_number;

/** 详细地址 */
@property (nonatomic, copy) NSString *address_detail;


@property (nonatomic, copy) NSString *token;

@property (nonatomic, strong) NSString *baby_head;


/** 关注状态  1、未关注  2、已关注  3、相互关注 */
@property (nonatomic, assign) NSInteger follow_status;


/** 私信消息未读数 */
@property (nonatomic, assign) NSInteger qq_unread_number;

/** 系统消息未读数 */
@property (nonatomic, assign) NSInteger system_unread_number;

/** 是否设置密码 */
@property (nonatomic, assign) NSInteger is_true_password;

/** 用户角色  1、老师 */
@property (nonatomic, assign) NSInteger role_id;

@end
