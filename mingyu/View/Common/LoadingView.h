//
//  LoadingView.h
//  mingyu
//
//  Created by apple on 2018/7/14.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

- (instancetype)initWithFrame:(CGRect)frame inView:(UIView *)view;



/**
 显示
 */
- (void)show;

- (void)hidden;

@end
