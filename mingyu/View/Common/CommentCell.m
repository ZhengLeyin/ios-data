//
//  CommentCell.m
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CommentCell.h"
#import "CommentReplayCell.h"

@interface CommentCell ()<UITableViewDelegate,UITableViewDataSource,CommentReplayCellDelegate>

@end

static CGFloat cellHeight = 0;

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initControl];

}

+ (CGFloat)cellHeight {
    return cellHeight;
}
-(void)initControl{
    
    self.replayTableview.dataSource = self;
    self.replayTableview.delegate =self;
    self.replayTableview.scrollEnabled = NO;
}


- (void)setCommentmodel:(CommentModel *)commentmodel{
    _commentmodel = commentmodel;
    if (commentmodel.fk_user_id == [USER_ID integerValue]) {
        _commentButton.hidden = YES;
    } else {
        _commentButton.hidden = NO;
    }
    _userNameLabel.text = _commentmodel.comment_name;
//    NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,_commentmodel.comment_head];
    [_userHeadButton zp_setImageWithURL:_commentmodel.comment_head forState:0 placeholderImage:ImageName(@"默认头像")];
    _commentTimeLab.text = _commentmodel.comment_time_status;

    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle  setLineSpacing:5];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:_commentmodel.comment_content];
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_commentmodel.comment_content length])];
    _commentContentLab.attributedText = attrString;
    
    [self changePraiseButton:_commentmodel];
    
    CGFloat commentHeight = 0;
    if (_commentmodel.replayList.count > 2) {
        commentHeight =30;
    }
    NSString *replayString = [NSString string];
    if (_commentmodel.showMore) {
        for (int i=0; i<_commentmodel.replayList.count; i++) {
            if (_commentmodel.replayList.count > 0) {
                NSDictionary *dic = [_commentmodel.replayList objectAtIndex:i];
                CommentReplayModel *replaymodel = [CommentReplayModel modelWithJSON:dic];
                if (replaymodel.to_user_name) {
                    replayString = [NSString stringWithFormat:@"%@ > %@：%@",replaymodel.replay_name,replaymodel.to_user_name,replaymodel.replay_content];
                } else {
                    replayString = [NSString stringWithFormat:@"%@：%@",replaymodel.replay_name,replaymodel.replay_content];
                }
                CGFloat height =   [self hideLabelLayoutHeight:replayString withTextFontSize:12 andWidth:kScreenWidth-130]+45;
                commentHeight += height;
            }
        }
    } else {
        for (int i=0; i<2; i++) {
            if (_commentmodel.replayList.count > i) {
                NSDictionary *dic = [_commentmodel.replayList objectAtIndex:i];
                CommentReplayModel *replaymodel = [CommentReplayModel modelWithJSON:dic];
                if (replaymodel.to_user_name) {
                    replayString = [NSString stringWithFormat:@"%@ > %@：%@",replaymodel.replay_name,replaymodel.to_user_name,replaymodel.replay_content];
                } else {
                    replayString = [NSString stringWithFormat:@"%@：%@",replaymodel.replay_name,replaymodel.replay_content];
                }
                CGFloat height = [self hideLabelLayoutHeight:replayString withTextFontSize:12 andWidth:kScreenWidth-130]+45;
                commentHeight += height;
            }
        }
    }
    _replayTableviewHeight.constant = commentHeight;
    if (_commentmodel.replayList.count > 0) {
        commentHeight += 10;
    }
    [self.contentView updateConstraintsIfNeeded];
    if (_commentmodel.height==0) {
        _commentmodel.height = 100+commentHeight+[self hideLabelLayoutHeight:_commentmodel.comment_content withTextFontSize:14 andWidth:kScreenWidth-82];
    }
    
    cellHeight = _commentmodel.height;
    ZPLog(@"CommentCellH-------%f",cellHeight);
    [self.replayTableview reloadData];
}


- (void)changePraiseButton:(CommentModel *)commentmodel{
    if (_commentmodel.click_praise_true == 0) {
        [_praiseButton setImage:ImageName(@"评论赞") forState:0];
    } else {
        [_praiseButton setImage:ImageName(@"评论已赞") forState:0];
    }
    if (_commentmodel.praise_number) {
        [_praiseButton setTitle:[NSString stringWithFormat:@"%ld",_commentmodel.praise_number] forState:0];
    } else {
        [_praiseButton setTitle:@"赞" forState:0];
    }
}

-(CGFloat )hideLabelLayoutHeight:(NSString *)content withTextFontSize:(CGFloat)FontSize andWidth:(CGFloat)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;  // 段落高度
    NSMutableAttributedString *attributes = [[NSMutableAttributedString alloc] initWithString:content];
    [attributes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize] range:NSMakeRange(0, content.length)];
    [attributes addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];
    CGSize attSize = [attributes boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return attSize.height;
}

//用label模拟算高
- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    return sizeToFit.height+15;
}

#pragma mark - TableView data source
//两个tableview嵌套，父table放所有信息，子table放评论列表
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_commentmodel.showMore) {
        return _commentmodel.replayList.count;
    } else {
        if (_commentmodel.replayList.count > 2) {
            return 2;
        }
        return _commentmodel.replayList.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 75, 0, -75)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 15)];
    [button addTarget:self action:@selector(ShowHiddeMore) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:0];
    button.titleLabel.font = FontSize(12);
    if (_commentmodel.showMore) {
        [button setTitle:@"收起更多回复" forState:0];
        [button setImage:ImageName(@"回复收起") forState:0];
    } else {
        [button setTitle:@"查看更多回复" forState:0];
        [button setImage:ImageName(@"回复展开") forState:0];
    }
    return button;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_commentmodel.replayList.count > 2) {
        return 30;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZPLog(@"%f",[CommentReplayCell cellHeight]);
    return [CommentReplayCell cellHeight];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     CommentReplayCell *cell = (CommentReplayCell *) [tableView dequeueReusableCellWithIdentifier:@"CommentReplayCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentReplayCell" owner:nil options:nil] lastObject];
    }
    if (_commentmodel.replayList.count > indexPath.row) {
        NSDictionary *dic = [_commentmodel.replayList objectAtIndex:[indexPath row]];
        cell.CommentCellIndex = _CommentCellIndex;
        cell.ReplayCellIndex = indexPath.row;
        cell.delegae = self;
        cell.replaymodel = [CommentReplayModel modelWithJSON:dic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary *dic = [_commentmodel.replayList objectAtIndex:[indexPath row]];
    CommentReplayModel *model = [CommentReplayModel modelWithJSON:dic];
    if (self.delegate && [self.delegate respondsToSelector:@selector(CommentCellReplayCell:CommentCellIndex:didSelectRow:)]) {
        [self.delegate CommentCellReplayCell:model CommentCellIndex:_CommentCellIndex didSelectRow:indexPath.row];
    }
}


- (void)ShowHiddeMore{
    if (_commentmodel.showMore) {
        _commentmodel.showMore = NO;
    } else {
        _commentmodel.showMore = YES;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(CommentCellShowHiddeMore:row:)]) {
        [self.delegate CommentCellShowHiddeMore:_commentmodel row:_CommentCellIndex];
    }
}




#pragma mark - CommentReplayCellDelegate
//ReplayCell 评论点赞
- (void)ReplayCellPraiseButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex{
//        if (![userDefault boolForKey:KUDhasLogin]) {
//            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
//            [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
//            return;
//        }
        if (replaymodel.click_praise_true == 0) {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
            NSDictionary *parameters = @{
                                         @"user_id":USER_ID,
                                         @"from_id":[NSString stringWithFormat:@"%ld",replaymodel.replay_id],
                                         @"praise_type":@(_replaypraisetype)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    NSInteger click_praise_true = [json[@"data"] integerValue];
                    replaymodel.click_praise_true = click_praise_true;
                    replaymodel.praise_number++;
                    NSDictionary *dic = [replaymodel modelToJSONObject];
                    [_commentmodel.replayList replaceObjectAtIndex:replayindex withObject:dic];
                    [self.replayTableview reloadData];
                    [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];
    
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:self];
                }
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:self];
            }];
        } else{
            [UIView ShowInfo:TipDoNotPraise Inview:[self viewController].view];
        }
}

//ReplayCell 评论回复
- (void)ReplayCellReplayButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ReplayCellReplayButton:CommentCellIndex:ReplayCellIndex:)]) {
        [self.delegate ReplayCellReplayButton:replaymodel CommentCellIndex:commentindex ReplayCellIndex:replayindex];
    }
}



//CommentCell 用户头像
- (IBAction)CommentCellUserHeader:(id)sender {
    PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
    VC.user_id = _commentmodel.fk_user_id;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


//CommentCell 评论回复
- (IBAction)CommentCellReplayButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(CommentCellCommentButton:CommentCellIndex:)]) {
        [self.delegate CommentCellCommentButton:_commentmodel CommentCellIndex:_CommentCellIndex];
    }
}



//CommentCell 评论点赞
- (IBAction)CommentCellPraiseButton:(UIButton *)sender {
//    if (![userDefault boolForKey:KUDhasLogin]) {
//        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
//        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
//        return;
//    }
    if (_commentmodel.click_praise_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"from_id":[NSString stringWithFormat:@"%ld",_commentmodel.comment_id],
                                      @"praise_type":@(_commentpraisetype)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger click_praise_true = [json[@"data"] integerValue];
                _commentmodel.click_praise_true = click_praise_true;
                _commentmodel.praise_number++;
                [self changePraiseButton:_commentmodel];
                [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];
            } else {
                [UIView ShowInfo:json[@"message"] Inview:self];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self];
        }];
    } else{
        [UIView ShowInfo:TipDoNotPraise Inview:[self viewController].view];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
