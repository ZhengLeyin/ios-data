//
//  ShareView.m
//  mingyu
//
//  Created by apple on 2018/5/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//


#import "ShareView.h"
#import <UShareUI/UShareUI.h>

@interface ShareView ()
{
    UIView * _darkView;
    UIView * _bottomView;
}
@end

@implementation ShareView


- (instancetype)initWithshareModel:(ShareModel *)sharemodel{
    if (self = [super init]) {
        _sharemodel = sharemodel;
        [self updatUI];
    }
    return self;
}


- (void)updatUI{
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [app.window addSubview:self];
    
    UIView *darkView = [[UIView alloc]init];
    darkView.backgroundColor = [UIColor colorWithHexString:@"202020" alpha:0.7];
    darkView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [self addSubview:darkView];
    _darkView = darkView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [_darkView addGestureRecognizer:tap];
    
    UIView *bottomView = [[UIView alloc]init];
    bottomView.backgroundColor = [UIColor whiteColor];
    bottomView.layer.cornerRadius = 5;
    [self addSubview:bottomView];
    _bottomView = bottomView;
    
//    [self setType:share_type];

}


- (void)setType:(ShareViewType)type{
    
    _type = type;
    NSArray *array = [NSArray array];
    NSArray *imagearray = [NSArray array];
    
    CGFloat Start_X = 20.0f; // 第一个按钮的X坐标
    CGFloat Start_Y = 70.0f; // 第一个按钮的Y坐标
    CGFloat Width_Space = 15.0f; // 2个按钮之间的横间距
    CGFloat Height_Space = 20.0f; // 竖间距
    CGFloat Button_Height = 80.0f; // 高
    CGFloat Button_Width = (kScreenWidth-30-40-46)/4 ; // 宽
    
    if (_type == share_type){
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.bottom.mas_equalTo(-effectViewH-15);
            make.height.mas_offset(266);
        }];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 27, kScreenWidth-30, 20)];
        lab.text = @"选择要分享的平台";
        lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
        lab.font = FontSize(12);
        lab.textAlignment = NSTextAlignmentCenter;
        [_bottomView addSubview:lab];
        
        array = @[@"微信朋友圈",@"微信好友",@"QQ好友",@"QQ空间",@"新浪微博"];
        imagearray = @[@"朋友圈",@"微信",@"QQ",@"空间",@"微博"];
        
        Start_Y = 70.0f; // 第一个按钮的Y坐标
    } else {
        [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.bottom.mas_equalTo(-effectViewH-15);
            make.height.mas_offset(226);
        }];
        
        array = @[@"微信朋友圈",@"微信好友",@"QQ好友",@"QQ空间",@"新浪微博",@"举报"];
        imagearray = @[@"朋友圈",@"微信",@"QQ",@"空间",@"微博",@"举报"];
        
        Start_Y = 30.0f; // 第一个按钮的Y坐标
    }


    for (int i = 0 ; i < array.count; i++)
    { NSInteger index = i % 4;
        NSInteger page = i / 4;
        // 圆角按钮
        UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        mapBtn.tag = i+2000;
        [mapBtn setTitle:array[i] forState:UIControlStateNormal];
        [mapBtn.titleLabel setFont: FontSize(12)];
        [mapBtn setTitleColor:[UIColor colorWithHexString:@"4A4A4A"] forState:UIControlStateNormal];
        [mapBtn setImage:ImageName(imagearray[i]) forState:UIControlStateNormal];
        //这句话不写等于废了
        mapBtn.frame = CGRectMake(index * (Button_Width + Width_Space) + Start_X, page * (Button_Height + Height_Space)+Start_Y, Button_Width, Button_Height);
        mapBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -mapBtn.imageView.frame.size.width, -mapBtn.imageView.frame.size.height-10, 0);
        //                mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.frame.size.height-10, 0, 0, -mapBtn.titleLabel.frame.size.width);
        // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
        mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.intrinsicContentSize.height-10, 0, 0, -mapBtn.titleLabel.intrinsicContentSize.width);
        
        [_bottomView addSubview:mapBtn];
        //按钮点击方法
        [mapBtn addTarget:self action:@selector(mapBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

//点击按钮方法,这里容易犯错
-(void)mapBtnClick:(UIButton *)sender{
    //记住,这里不能写成"mapBtn.tag",这样你点击任何一个button,都只能获取到最后一个button的值,因为前边的按钮都被最后一个button给覆盖了
    ZPLog(@"%ld",(long)sender.tag);
    
    UMSocialPlatformType type;
    if (sender.tag == 2000) {
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatTimeLine]) {
            [self hidden];
            [self shAlertViewWithTitle:@"未安装微信"];
            return;
        }
        type = UMSocialPlatformType_WechatTimeLine;
    } else if (sender.tag == 2001){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]) {
            [self hidden];
            [self shAlertViewWithTitle:@"未安装微信"];
            return;
        }
        type = UMSocialPlatformType_WechatSession;
    }else if (sender.tag == 2002){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]) {
            [self hidden];
            [self shAlertViewWithTitle:@"未安装QQ"];
            return;
        }
        type = UMSocialPlatformType_QQ;
    }else if (sender.tag == 2003){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Qzone]) {
            [self hidden];
            [self shAlertViewWithTitle:@"未安装QQ"];
            return;
        }
        type = UMSocialPlatformType_Qzone;
    }else if (sender.tag == 2004){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]) {
            [self hidden];
            [self shAlertViewWithTitle:@"未安装微博"];
            return;
        }
        type = UMSocialPlatformType_Sina;
    } else {
        type = UMSocialPlatformType_UnKnown;
//        举报 或者 删除

    }
    [self shareMessage:type];
}


- (void)shareMessage:(UMSocialPlatformType )type{
    if (type == UMSocialPlatformType_UnKnown) {
        //举报
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"from_id":@(_sharemodel.from_id),
                                     @"type_id":@(_sharemodel.accusationType)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@",json);
            ZPLog(@"%@",json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                [self shAlertViewWithTitle:@"举报成功"];
                [self hidden];
            }
        } failure:^(NSError *error) {
            
        }];
    } else{
        //创建分享消息对象
        UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
        //创建网页内容对象
        NSURL *thumbURL = [NSURL URLWithString:self.sharemodel.thumbURL];
        NSData * data = [NSData dataWithContentsOfURL:thumbURL];
        if (!data) {
            data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://app.mingyu100.com/static/logo.png"]];
        }
        if (self.sharemodel.accusationType == accusation_type_audio && type != UMSocialPlatformType_WechatSession) {
            if (type == UMSocialPlatformType_Sina) {
                NSString* title = [NSString stringWithFormat:@"%@ @名育APP",self.sharemodel.titleStr];
                UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:self.sharemodel.descr.length > 0?self.sharemodel.descr:@"分享自「名育」" thumImage:data];
                //设置网页地址
                shareObject.webpageUrl = self.sharemodel.webpageUrl;
                //分享消息对象设置分享内容对象
                messageObject.shareObject = shareObject;
            } else {
                UMShareMusicObject *shareObject = [UMShareMusicObject shareObjectWithTitle:self.sharemodel.titleStr descr:self.sharemodel.descr.length > 0?self.sharemodel.descr:@"分享自「名育」" thumImage:data];
                shareObject.musicDataUrl = self.sharemodel.targetURL;
                shareObject.musicUrl = self.sharemodel.webpageUrl;
                messageObject.shareObject = shareObject;
            }
        } else {
            NSString *title = [NSString string];
            if (type == UMSocialPlatformType_Sina) {
                title = [NSString stringWithFormat:@"%@ @名育APP",self.sharemodel.titleStr];
            } else {
                title = self.sharemodel.titleStr;
            }
            UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:self.sharemodel.descr.length > 0?self.sharemodel.descr:@"分享自「名育」" thumImage:data];
            //设置网页地址
            shareObject.webpageUrl = self.sharemodel.webpageUrl;
            //分享消息对象设置分享内容对象
            messageObject.shareObject = shareObject;
        }
        
        //调用分享接口
        [[UMSocialManager defaultManager] shareToPlatform:type messageObject:messageObject currentViewController:[self viewController] completion:^(id data, NSError *error) {
            if (error) {
                [self hidden];
                [self shAlertViewWithTitle:@"分享失败"];
                ZPLog(@"************Share fail with error %@*********",error);
            }else{
                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                    UMSocialShareResponse *resp = data;
                    //分享结果消息
                    ZPLog(@"response message is %@",resp.message);
                    //第三方原始返回的数据
                    ZPLog(@"response originalResponse data is %@",resp.originalResponse);
                    [self hidden];
                    if (type != UMSocialPlatformType_WechatTimeLine && type != UMSocialPlatformType_WechatSession) {
                        [self shAlertViewWithTitle:@"分享成功"];
                    }
                    [self addShareLog];
                }else{
                    ZPLog(@"response data is %@",data);
                }
            }
        }];
    }
}


//用户每日第一次分享
- (void)addShareLog{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KFirstShare];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resource_id":@(_sharemodel.from_id),
                                 @"resource_type":@(_sharemodel.shareType)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
           
        } else {

        }
    } failure:^(NSError *error) {

    }];
}


- (void)show{
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _darkView.alpha = 1;
        CGRect frame = _bottomView.frame;
        frame.origin.y -=frame.size.height;
        _bottomView.frame = frame;
    } completion:nil];
}


- (void)hidden{
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        _darkView.alpha = 0;
        CGRect frame = _bottomView.frame;
        frame.origin.y += frame.size.height;
        _bottomView.frame = frame;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}


- (void)tapClick{
    
    [self hidden];
    
}



@end
