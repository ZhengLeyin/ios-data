//
//  ParentHomeCell.h
//  mingyu
//
//  Created by apple on 2018/4/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChildcareMessageModel,VideoModel;


@interface ParentHomeCell : UITableViewCell




@property (weak, nonatomic) IBOutlet UILabel *labelLab;
@property (weak, nonatomic) IBOutlet UILabel *childcare_title_Lab;
@property (weak, nonatomic) IBOutlet UILabel *childcare_content_Lab;
@property (weak, nonatomic) IBOutlet UIButton *page_view_Btn;
@property (weak, nonatomic) IBOutlet UIButton *praise_number_Btn;


@property (weak, nonatomic) IBOutlet UIImageView *video_img;
@property (weak, nonatomic) IBOutlet UILabel *video_title_Lab;
@property (weak, nonatomic) IBOutlet UILabel *video_abstract_Lab;
@property (weak, nonatomic) IBOutlet UILabel *play_number_Lab;
@property (weak, nonatomic) IBOutlet UILabel *price_Lab;


@property (weak, nonatomic) IBOutlet UILabel *header_title_lab;
@property (weak, nonatomic) IBOutlet UIButton *more_Btn;

@property (nonatomic, copy) void (^MoreButtonBlock)(void);



+ (instancetype)theFirstCellWithTableView:(UITableView *)tableView;


+ (instancetype)theSecondCellWithTableView:(UITableView *)tableView;


+ (instancetype)theThirdCellWithTableView:(UITableView *)tableView;

+ (instancetype)TheHeaderView;

- (void)setFirstCellWithModel:(NSArray *)array;

- (void)setSecondCellWithModel:(ChildcareMessageModel *)childcaremessagemodel;

- (void)setThirdCellWithModel:(VideoModel *)videomodel;






@end
