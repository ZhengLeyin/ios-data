//
//  MingYuCashierView.m
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "MingYuCashierView.h"
#import "DropDownList.h"
#import "YJDatePickerView.h"

@interface MingYuCashierView ()<YJDatePickerDelegate>


@end
@implementation MingYuCashierView

//+ (instancetype)theMingYuCashierView {
//    //return [[NSBundle mainBundle] loadNibNamed:@"MingYuCashierView" owner:nil options:nil][0];
//}

- (void)drawRect:(CGRect)rect {
    _integarlLabel.layer.borderColor = [UIColor colorWithHexString:@"#F2F2F2"].CGColor;
    _integarlLabel.layer.borderWidth = 0.5;
}

-(void)mingYuModel:(MingYuRechargeModel *)model courseModel:(CourseModel *)courseModel {
    _model = model;
    _courseModel = courseModel;
    _integarlLabel.text = @"0";
    [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:@"0"]];
    /**首先判断是否具备积分抵扣资格*/
    if (courseModel.max_integral_scale == 0 || ((_courseModel.max_integral_scale != 0) && (_model.ideal_money < 1))) {
        _integralDeductionSwitchButton.selected = NO ;
        [_integralDeductionSwitchButton setImage:[UIImage imageNamed:@"GroupCloseSwitch"] forState:(UIControlStateNormal)];
        
        [[_integralDeductionSwitchButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
            if (_courseModel.max_integral_scale == 0) {
                [UIView ShowInfo:@"该课程不能使用积分~" Inview:self];
            }else if (_courseModel.max_integral_scale != 0 && (_model.ideal_money < 1)) {
                [UIView ShowInfo:@"您的积分少,不能使用积分抵扣~" Inview:self];
            }else {
                
            }
        }];
    }else {
        [_integralDeductionSwitchButton setImage:[UIImage imageNamed:@"GroupCloseSwitch"] forState:(UIControlStateNormal)];
         [_integralDeductionSwitchButton setImage:[UIImage imageNamed:@"GroupOpenSwitch"] forState:(UIControlStateSelected)];
        
        [_integralDeductionSwitchButton addTarget:self action:@selector(deductionSwitchButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
        _integralDeductionSwitchButton.selected = YES;
        /* 添加一个轻拍手势 */
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        [[tap rac_gestureSignal] subscribeNext:^(id x) {
            if (_integralDeductionSwitchButton.selected == YES) {
                  [self integralCategoryButtonAction];
            }else {
                [UIView ShowInfo:@"请您先打开积分抵用开关~" Inview:self];
            }
          
        }];
        [_integralCategoryStackView addGestureRecognizer:tap];

    }
    _yuBiCountLabel.text = [NSString stringWithFormat:@"%.2f个",model.ios_money];
    _needToBuyYuBiCountLabel.text = [NSString stringWithFormat:@"%.2f",courseModel.present_price];

    [self calculateThePriceModel:model courseModel:courseModel];
    
}

-(void)calculateThePriceModel:(MingYuRechargeModel *)model courseModel:(CourseModel *)courseModel {
    /**判断支付金额*/
    if (model.ios_money >= courseModel.present_price) {
        CGFloat integarlMoney = [_integarlLabel.text floatValue] / 100.00;
        if (integarlMoney == 0.0) {
            _realPrice = courseModel.present_price;
            [_payRechargeButton setTitle:[NSString stringWithFormat:@"确认支付(%.2f个育币)",courseModel.present_price] forState:(UIControlStateNormal)];
        }else if (integarlMoney > 0 && (_integralDeductionSwitchButton.selected == YES)) {
            _realPrice = courseModel.present_price - integarlMoney;
            [_payRechargeButton setTitle:[NSString stringWithFormat:@"确认支付(%.2f个育币)",courseModel.present_price - integarlMoney] forState:(UIControlStateNormal)];
        }else {
            _realPrice = courseModel.present_price;
             [_payRechargeButton setTitle:[NSString stringWithFormat:@"确认支付(%.2f个育币)",courseModel.present_price] forState:(UIControlStateNormal)];
        }
    }else  {
        CGFloat integarlMoney = [_integarlLabel.text floatValue] / 100.00;
        if (((integarlMoney + model.ios_money) >= courseModel.present_price)  && (_integralDeductionSwitchButton.selected == YES)) {
            _realPrice = courseModel.present_price - integarlMoney;
            [_payRechargeButton setTitle:[NSString stringWithFormat:@"确认支付(%.2f个育币)",courseModel.present_price - integarlMoney] forState:(UIControlStateNormal)];
        }else {
            if (_integralDeductionSwitchButton.selected == YES) {
                _realPrice = model.ios_money - courseModel.present_price + integarlMoney;
            } else {
                _realPrice = model.ios_money - courseModel.present_price;
            }
            [_payRechargeButton setTitle:[NSString stringWithFormat:@"余额不足, 去充值"] forState:(UIControlStateNormal)];
        }
    }
}


-(NSMutableAttributedString *)attStrlabelText:(NSString *)labelText {
    NSString *string = [NSString stringWithFormat:@"减%@元",labelText];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:string];
    [attStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#E75858"],
                            NSFontAttributeName:FontSize(15.0)} range:NSMakeRange(1,string.length - 2)];//添加属性
    return attStr;
}

-(void)integralCategoryButtonAction {
    if (_courseModel.max_integral_scale == 0) {
          [UIView ShowInfo:@"该课程不能使用积分~" Inview:self];
    }else if (_courseModel.max_integral_scale != 0 && (_model.ideal_money < 1)) {
          [UIView ShowInfo:@"您的积分少,不能使用积分抵扣~" Inview:self];
    }else {
        //[self dorpList];
        [self setYJDatePickerView];
    }
}


#pragma mark 积分弹框UI 改动
/*1.判断改用户积分时候大于该课程的最大抵扣积分数
 *2.然后进行判断 最大值就是显示就是永远小于等于最大抵扣值
 */
-(void)setYJDatePickerView {
    NSUInteger  a;
    NSUInteger  b;
    NSUInteger  c;
    NSMutableArray *array0 = [NSMutableArray array];
    NSUInteger count = 0;
    
    if ((_model.ideal_money < (_courseModel.max_integral_scale * _courseModel.present_price * 100))) {
        
        count = _model.ideal_money;
        [array0 addObject:[NSString stringWithFormat:@"最大抵扣:%ld",_model.ideal_money]];
    }else {
        count = _courseModel.max_integral_scale * _courseModel.present_price * 100;
        [array0 addObject:[NSString stringWithFormat:@"最大抵扣:%.0f",(_courseModel.max_integral_scale * _courseModel.present_price * 100)]];
    }
    
    if (count > 100) {
        a = count / 100;
        if (a >= 10) {
            a = 10;
        }else {
            a = a+1;
        }
        for(int i = 1; i< a; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*100]];
        }
    }
    
    if (count > 1000) {
        b = count / 1000;
        if (b >= 10) {
            b = 10;
        }else {
            b = b+1;
        }
        for(int i = 1; i< b; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*1000]];
        }
    }
    
    if (count > 10000) {
        c = count / 10000;
        if (c >= 10) {
            c = 10;
        }else {
            c = c+1;
        }
        for(int i = 1; i< c; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*10000]];
        }
    }
    
    NSLog(@"array:%@",array0);
    NSArray *dataArray = array0.mutableCopy;
    
    NSMutableArray *timeArray = [NSMutableArray arrayWithArray:dataArray];
    YJDatePickerView *view = [YJDatePickerView pickCustomDataWithArray:@[timeArray] completionHandle:^(NSDictionary *indexDic, NSDictionary *valueDic) {
       NSString *title = valueDic[@"0"];
        title = [title stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"最大抵扣:"]];
       MingYuLoggerI(@"%@",title);
        
        NSString *integral = [NSString stringWithFormat:@"%.0f",_courseModel.max_integral_scale *_courseModel.present_price * 100];
        if ([title integerValue] > 0 &&  (_integralDeductionSwitchButton.selected == YES) && ([title integerValue] < [integral integerValue])) {
            _integarlLabel.text = title;
            CGFloat labelText = [title floatValue] / 100 ;
            [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",labelText]]];
            [self calculateThePriceModel:_model courseModel:_courseModel];
        }else if ([title integerValue] > [integral integerValue] &&  (_integralDeductionSwitchButton.selected == YES)) {
            [UIView ShowInfo:@"积分超过最大抵扣值~" Inview:self];
            return ;
        } else {
            _integarlLabel.text = title;
            CGFloat labelText = [title floatValue] / 100 ;
            [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",labelText]]];
            [self calculateThePriceModel:_model courseModel:_courseModel];
        }

    } defaultValue:nil];
    view.delegate = self;
}


-(void)dorpList {
    NSUInteger  a;
    NSUInteger  b;
    NSUInteger  c;
    NSMutableArray *array0 = [NSMutableArray array];
    NSUInteger count = _model.ideal_money;
    
    if (count > 100) {
        a = count / 100;
        if (a >= 10) {
            a = 10;
        }else {
            a = a+1;
        }
        for(int i = 1; i< a; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*100]];
        }
    }
    
    if (count > 1000) {
        b = count / 1000;
        if (b >= 10) {
            b = 10;
        }else {
           b = b+1;
        }
        for(int i = 1; i< b; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*1000]];
        }
    }
    
    if (count > 10000) {
        c = count / 10000;
        if (c >= 10) {
            c = 10;
        }else {
            c = c+1;
        }
        for(int i = 1; i< c; i++) {
            [array0 addObject:[NSString stringWithFormat:@"%d",i*10000]];
        }
    }
    
    NSLog(@"array:%@",array0);
    NSArray *dataArray = array0.mutableCopy;
    CGFloat drop_X = _integralCategoryStackView.frame.origin.x;
    CGFloat drop_Y = CGRectGetMaxY(_threeView.frame) - 10;
    CGFloat drop_W = _integralCategoryStackView.frame.size.width + 10;
    CGFloat drop_H;
    if (array0.count >= 5) {
         drop_H = 5 * 30 + 30 + 15;
    }else {
         drop_H = array0.count * 30 + 30 + 15;
    }
     //drop_H = 0 * 30 + 15 + 30;
    
    DropDownList *dropList = [[DropDownList alloc] initWithFrame:CGRectMake(drop_X, drop_Y, drop_W, drop_H) dataArray:dataArray onTheView:self];
#pragma mark 积分最大抵扣数
    if ((_model.ideal_money < (_courseModel.max_integral_scale * _courseModel.present_price * 100))) {
        
        dropList.maximumDiscount = _model.ideal_money;
    }else {
        
        dropList.maximumDiscount = _courseModel.max_integral_scale * _courseModel.present_price * 100;
    }

    dropList.myBlock = ^(NSInteger row, NSString *title, NSString *maximumDiscount)
    {
        if ([maximumDiscount integerValue] > 0 ) {
            /**用户积分小于最大折扣*/
            _integarlLabel.text = maximumDiscount;
            CGFloat labelText = [maximumDiscount floatValue] / 100 ;
            [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",labelText]]];
            [self calculateThePriceModel:_model courseModel:_courseModel];

        }else {
            
            if ([title integerValue] > 0 &&  (_integralDeductionSwitchButton.selected == YES) && ([title integerValue] < (_courseModel.max_integral_scale *_courseModel.present_price * 100))) {
                _integarlLabel.text = title;
                CGFloat labelText = [title floatValue] / 100 ;
                [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",labelText]]];
                [self calculateThePriceModel:_model courseModel:_courseModel];
            }else if ([title integerValue] > (_courseModel.max_integral_scale *_courseModel.present_price * 100) &&  (_integralDeductionSwitchButton.selected == YES)) {
                [UIView ShowInfo:@"积分超过最大抵扣值~" Inview:self];
                return ;
            } else {
                _integarlLabel.text = title;
                CGFloat labelText = [title floatValue] / 100 ;
                [_integralDeductionPriceLabel setAttributedText:[self attStrlabelText:[NSString stringWithFormat:@"%.2f",labelText]]];
                [self calculateThePriceModel:_model courseModel:_courseModel];
            }
        }
        
    };
    
    [self addSubview:dropList];

}

-(void)deductionSwitchButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
     [self calculateThePriceModel:_model courseModel:_courseModel];
    
}
@end
