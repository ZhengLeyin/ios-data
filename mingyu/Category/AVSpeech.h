//
//  AVSpeech.h
//  Seller
//
//  Created by apple on 2017/11/3.
//  Copyright © 2017年 Jys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AVSpeech : NSObject

//@property (nonatomic, strong) AVSpeechSynthesizer *AVSpeech;

+ (void)SpakeString:(NSString *)string;

@end
