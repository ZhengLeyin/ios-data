//
//  MYRefreshGifHeader.m
//  text
//
//  Created by apple on 2018/11/19.
//  Copyright © 2018 apple. All rights reserved.
//

#import "MYRefreshGifHeader.h"


@implementation MYRefreshGifHeader

#pragma mark - 实现父类的方法
- (void)prepare {
    [super prepare];
    // 初始化间距
    self.labelLeftInset = 20;
    // 资源数据（GIF每一帧）
    NSArray *Images = [self getRefreshingImageArray];
    // 普通状态
    [self setImages:@[Images.lastObject] forState:MJRefreshStateIdle];
    // 正在刷新状态
    [self setImages:Images duration:Images.count*0.04 forState:MJRefreshStateRefreshing];
    self.stateLabel.hidden = YES;
    self.lastUpdatedTimeLabel.hidden = YES;
}



- (NSArray *)getRefreshingImageArray{

    NSMutableArray *headerImages = [NSMutableArray array];

//    NSUInteger r = arc4random_uniform(2) + 1;
//    if (r == 1) {
        for (int i = 1; i <= 57; i++) {
            NSString *imagename = [NSString stringWithFormat:@"my%04d",i];
            UIImage *image = [UIImage imageNamed:imagename];
            if (image) {
                [headerImages addObject:image];
            }
        }
//    } else {
//        for (int i = 25; i <= 57; i++) {
//            NSString *imagename = [NSString stringWithFormat:@"my%04d",i];
//            UIImage *image = [UIImage imageNamed:imagename];
//            
//            [headerImages addObject:image];
//        } for (int i = 1; i <= 24; i++) {
//            NSString *imagename = [NSString stringWithFormat:@"my%04d",i];
//            UIImage *image = [UIImage imageNamed:imagename];
//            
//            [headerImages addObject:image];
//        }
//    }
  
    return headerImages;
    
}



@end
