//
//  VaccineInfoViewController.m
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VaccineInfoViewController.h"
#import "MarkNoteParser.h"

@interface VaccineInfoViewController ()<UIWebViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIWebView *webView;


@end

@implementation VaccineInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:@"%@(%@)", _vaccineDic[@"title"],_vaccineDic[@"count"]];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.bottom.mas_offset(-effectViewH-10);
    }];
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.webView.height = self.webView.scrollView.contentSize.height;
    [self.tableView reloadData];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.webView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.webView.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [UIView new];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    return cell;
}



- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth-20, 100)];
        _webView.delegate = self;
        _webView.userInteractionEnabled = NO;
        NSString *result = [MarkNoteParser toHtml:_vaccineDic[@"mark"] imageWidth:kScreenWidth];
//        result=[result stringByReplacingOccurrencesOfString:@"接种原因："withString:@"<h4>接种原因：</h4>"];
//        result=[result stringByReplacingOccurrencesOfString:@"接种原理："withString:@"<h4>接种原理：</h4>"];
//        result=[result stringByReplacingOccurrencesOfString:@"接种方法："withString:@"<h4>接种方法：</h4>"];

        [_webView loadHTMLString:result baseURL:nil];

    }
    return _webView;
}



-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
