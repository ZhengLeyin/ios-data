//
//  ParentVideoViewController.h
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentVideoViewController : UIViewController


/** 是否是从任务跳转过来 */
@property (nonatomic, assign) BOOL is_task;

@end
