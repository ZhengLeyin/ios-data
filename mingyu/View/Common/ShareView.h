//
//  ShareView.h
//  mingyu
//
//  Created by apple on 2018/5/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ShareModel;

typedef NS_ENUM(NSUInteger, ShareViewType) {
    share_type = 0,
    more_type = 1,
};

@interface ShareView : UIView


@property (nonatomic, assign) ShareViewType type;

@property (nonatomic, strong) ShareModel *sharemodel;


/**
 初始化
 shareDic  分享内容
 */
//- (instancetype)initWithDictionary:(NSDictionary *)shareDic;
- (instancetype)initWithshareModel:(ShareModel *)sharemodel;

/**
 初始化
 titleNameArray  名字数组
 imageArray      图片数组
 */
//- (instancetype)initWithTitleNameArray:(NSArray *)titleNameArray imageArray:(NSArray *)imageArray;

/**
 显示
 */
- (void)show;

@end
