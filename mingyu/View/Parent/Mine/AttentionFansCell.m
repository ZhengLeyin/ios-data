//
//  AttentionFansCell.m
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AttentionFansCell.h"

@implementation AttentionFansCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_head_image.bounds cornerRadius:_head_image.height/2];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.path = maskPath2.CGPath;
    maskLayer2.frame = _head_image.bounds;
    _head_image.layer.mask = maskLayer2;
    
}

+ (instancetype)theAttentionFansCellWithtableView:(UITableView *)tableview{
    static NSString *cellid = @"AttentionFansCell";
    AttentionFansCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"AttentionFansCell" owner:nil options:nil][0];
    }
    return cell;
}


- (void)setUsermodel:(UserModel *)usermodel{
    _usermodel = usermodel;
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,usermodel.user_head];
    [_head_image zp_setImageWithURL:usermodel.user_head placeholderImage:ImageName(@"默认头像")];
    _name_lab.text = _usermodel.user_name;
    if (_usermodel.area_name) {
        _introduce_lab.text = [NSString stringWithFormat:@"%@ | %@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈",_usermodel.area_name];
    } else {
        _introduce_lab.text = [NSString stringWithFormat:@"%@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈"];
    }
    
    [self changeattentionState_button:usermodel];
}



//搜索时用到
- (void)showSearch{
    NSString *title = _usermodel.user_name;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _name_lab.attributedText = attrStr;
    _name_lab.lineBreakMode = NSLineBreakByTruncatingTail;

    NSString *content = [NSString string];
    if (_usermodel.area_name) {
        content = [NSString stringWithFormat:@"%@ | %@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈",_usermodel.area_name];
    } else {
        content = [NSString stringWithFormat:@"%@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈"];
    }
    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    content = [content stringByAppendingString:@"</font>"];

    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _introduce_lab.attributedText = attrStr2;
    _introduce_lab.lineBreakMode = NSLineBreakByTruncatingTail;

    
}


//根据状态判断是否已关注
- (void)changeattentionState_button:(UserModel*)usermodel{
    
    _attentionState_button.layer.cornerRadius = 3;
    _attentionState_button.layer.borderWidth = 1;
    if (usermodel.follow_status == 1) {
        _attentionState_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_attentionState_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_attentionState_button setTitle:@"+关注" forState:UIControlStateNormal];
        _attentionState_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    } else if (usermodel.follow_status == 2){
        _attentionState_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attentionState_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attentionState_button setTitle:@"已关注" forState:UIControlStateNormal];
        _attentionState_button.backgroundColor = [UIColor whiteColor];
    } else{
        _attentionState_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attentionState_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attentionState_button setTitle:@"相互关注" forState:UIControlStateNormal];
        _attentionState_button.backgroundColor = [UIColor whiteColor];
    }
    _attentionState_button.layer.masksToBounds = YES;
    _attentionState_button.hidden = NO;

    if (usermodel.user_id == [USER_ID integerValue]) {
        _attentionState_button.hidden = YES;
    }
}



- (IBAction)AttentionButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _attentionState_button.userInteractionEnabled = NO;
    if (_usermodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_usermodel.user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attentionState_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _usermodel.follow_status = follow_status;
                [self changeattentionState_button:_usermodel];
                if (self.attentButtonBlock) {
                    self.attentButtonBlock(follow_status);
                }
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attentionState_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_usermodel.user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attentionState_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _usermodel.follow_status = 1;
                [self changeattentionState_button:_usermodel];
                if (self.attentButtonBlock) {
                    self.attentButtonBlock(1);
                }
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attentionState_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}





@end
