//
//  LSCommentViewController.m
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LSCommentViewController.h"

@interface LSCommentViewController ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *commentArraydata;

@property (nonatomic, strong) NSMutableArray *recommendArraydata;

@property (nonatomic, assign) NSInteger comment_number;



@end

@implementation LSCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView =[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.estimatedRowHeight = 97.0f;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-46);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getComment) name:@"HaveAddComment" object:nil];

    self.commentArraydata = [NSMutableArray array];
    self.recommendArraydata = [NSMutableArray array];
    
    [self getComment];
    [self getRecommend];
    
}


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HaveAddComment" object:nil];
}

//获取评论
- (void)getComment{
    self.commentArraydata = [NSMutableArray array];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KgetContentCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":@(_target_id),
                                 @"comment_type":@(_audio_type == 2? listen_book_comment : course_comment),
                                 @"start":@"0",
                                 @"size":@"2"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            for (NSDictionary *dic in commentList) {
                CommentModel *model = [CommentModel modelWithJSON:dic];
                [self.commentArraydata addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//获取推荐
- (void)getRecommend{
    NSString *URL = [NSString string];
    if (_audio_type == 2) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendBook];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"book_id":@(_target_id),
                                     @"start":@"0",
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCourseRecommend];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"course_id":@(_target_id),
                                     @"start":@"0",
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                if (_audio_type == 2) {
                    BookListModel *model = [BookListModel modelWithJSON:dic];
                    [self.recommendArraydata addObject:model];
                } else {
                    CourseModel *coursemodel = [CourseModel modelWithJSON:dic];
                    [self.recommendArraydata addObject:coursemodel];
                }
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_commentArraydata.count > 2) {
            return 2;
        } else{
            return _commentArraydata.count;
        }
    } else{
        return _recommendArraydata.count;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        CommentListCell *headerview = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
            [headerview.comment_number_Button setTitle:[NSString stringWithFormat:@"共%ld条评论",_comment_number] forState:0];
            headerview.arrowimage.hidden = NO;
            headerview.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _target_id;
                if (_audio_type == 2) {
                    VC.comment_type = listen_book_comment;
                } else {
                    VC.comment_type = course_comment;
                }
                [self.navigationController pushViewController:VC animated:YES];
            };
        }
//        headerview.title_lab.text = @"精彩评论";
        return headerview;
    } else{
        CommentListCell *headerview = [CommentListCell theRecommendHeaderView];
//        headerview.title_lab.text = @"相关推荐";
        return headerview;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 55;
    } else{
        return 60;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (self.commentArraydata.count == 0) {
            CommentListCell *foot = [CommentListCell theNoCommentCell];
            return foot;
        } else if (self.commentArraydata.count < 3){
            return nil;
        } else{
            CommentListCell *foot = [CommentListCell TheMoreCell];
            __block typeof(self) WeakSelf = self;
            foot.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _target_id;
                if (_audio_type == 2) {
                    VC.comment_type = listen_book_comment;
                } else {
                    VC.comment_type = course_comment;
                }
                VC.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
                [WeakSelf.navigationController pushViewController:VC animated:YES];
            };
            return foot;
        }
    } else{
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (self.commentArraydata.count == 0) {
            return 100;
        } else if (self.commentArraydata.count < 3){
            return 0;
        } else{
            return 50;
        }
    } else{
        return 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        if (self.commentArraydata && self.commentArraydata.count > indexPath.row) {
            CommentModel *model = [self.commentArraydata objectAtIndex:indexPath.row];
            [commentcell setcellWithModel:model praiseType: _audio_type==2?praise_book_comment:praise_course_comment];
//            [commentcell setcellWithModel:model praise_type:@"8"];
        }
        return commentcell;
    } else{
        if (_audio_type == 2) {
            ListenStoryCell *cell = [ListenStoryCell theCellWithTableView:tableView];
            if (self.recommendArraydata && self.recommendArraydata.count > indexPath.row) {
                BookListModel *model = [self.recommendArraydata objectAtIndex:indexPath.row];
                cell.booklistmodel = model;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return cell;
        } else {
            ParentCouresCell *courescell = [ParentCouresCell theCellWithTableView:tableView];
            if (self.recommendArraydata && self.recommendArraydata.count > indexPath.row) {
                CourseModel *model = self.recommendArraydata[indexPath.row];
                courescell.coursemodel = model;
                courescell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return courescell;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CommentModel *model = [_commentArraydata objectAtIndex:[indexPath row]];
        if (model.height==0) {
            return [CommentListCell cellHeight];
        }else{
            return model.height;
        }
    } else{
        return 130;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (UIScrollView *)scrollView{
    return self.tableView;
}




@end
