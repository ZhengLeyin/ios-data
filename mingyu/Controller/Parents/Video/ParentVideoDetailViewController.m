//
//  ParentVideoDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/6/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoDetailViewController.h"
#import "ParentVideoDetailBottomView.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"
#import "VideoPlayEndVIew.h"
#import "ParentVideoDetailHeaderView.h"
#import "ParentVideoDetailCell.h"
#import "ZFPlayerControlView.h"
#import "ZFAVPlayerManager.h"
#import "CommentCell.h"


#define PlayerHeight  kScreenWidth*9/16
#define bottomViewHeight   60
#define commentviewHeight  46

@interface ParentVideoDetailViewController ()<UITableViewDelegate,UITableViewDataSource,BottomCommentDelegate,CommentCellDelegate,DFPlayerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *playerView;           
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) ZFPlayerController *player;

@property (nonatomic, strong) BottomCommetView *commentview;
@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) VideoPlayEndVIew *playendview;

@property (nonatomic, strong) ParentVideoDetailHeaderView *headerView;

@property (nonatomic, strong) SomeCircleFooterView *footerView;

@property (nonatomic, assign) BOOL showMore;

@property (nonatomic, strong) VideoModel *videomodel;

@property (nonatomic, strong) NSMutableArray *commentListArray;
@property (nonatomic, assign) NSInteger comment_number;

@property (nonatomic, strong) NSMutableArray *recommendArray;

@property (nonatomic, assign)  NSTimeInterval currentTime;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@end

@implementation ParentVideoDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResignActiveNotification) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(back:) name:NSNotificationReceiveNotification object:nil];

    self.view.backgroundColor = [UIColor whiteColor];
    //    如果音频在播放 先暂停音频
    [DFPlayer shareInstance].delegate = self;
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [[DFPlayer shareInstance] df_audioPause];
    }
    

    //处理tabview下移问题
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //处理键盘问题
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;

    [self getShortVideo];  //获取视频详情
}


/** 解决视频播放来电话时，视频被打断，当通话结束后视频 音频一起播放的问题  */
- (void)df_player:(DFPlayer *)player isInterrupted:(BOOL)isInterrupted{
    if (!isInterrupted) {
        [[DFPlayer shareInstance] df_audioPause];
    }
}


- (void)ResignActiveNotification{
    [self.player enterFullScreen:NO animated:YES];
}

//获取视频详情
- (void)getShortVideo{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetShortVideo];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"video_id":@(_video_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _videomodel = [VideoModel modelWithJSON:json[@"data"]];

            [self setupSubiew];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
     } failure:^(NSError *error) {
//         [UIView ShowInfo:TipWrongMessage Inview:self.view];
         [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
         [_backButton setImage:ImageName(@"back_black") forState:0];
     }];
}


- (void)setupSubiew{
    
    [self setupPlayerView];

    [self setupTableView];

    [self.view addSubview:self.commentview];

    [self setTextViewToolbar];

    [self getRecommendVideoList];

    [self getCommentList]; //获取评论列表

    [self refreshTableview];
}


- (void)setupPlayerView{
    [self.view addSubview:self.playerView];
    
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    // 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:playerManager containerView:self.playerView];
    self.player.controlView = self.controlView;
    [self.view addSubview:self.playendview];
    @weakify(self)
    self.playendview.playAgainBlock = ^{
        @strongify(self)
        self.shareButton.hidden = NO;
        self.player.allowOrentitaionRotation = YES; //重播 可以全屏
        if ([self.videomodel.video_path pathExtension].length > 0){
            self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
        }
        self.playendview.hidden = YES;
    };
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
    };
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
        self.shareButton.hidden = YES;
        self.currentTime = 0;
        self.player.allowOrentitaionRotation = NO; //播完之后 禁止全屏
        if (self.player.isFullScreen) {
            [self.player enterFullScreen:NO animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.player.orientationObserver.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.playendview.hidden = NO;
            });
        } else {
            self.playendview.hidden = NO;
        }
    };
    
    if ([self.videomodel.video_path pathExtension].length > 0){
        if ([HttpRequest reachableViaWiFi]) {
            self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
        } else {
            AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (app.allowPlay) {
                self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
            } else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:@"您正在使用移动网络播放"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"继续" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          app.allowPlay = YES;
                                                                          self.player.assetURL = [NSURL URLWithString:self.videomodel.video_path];
                                                                      }];
                UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                                     handler:nil];
                
                [alert addAction:cancelAction];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"VideoSeekTime.plist"];
            NSMutableArray *VideoArray = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
            for (NSDictionary *dic in VideoArray) {
                NSInteger video_id = [dic[@"video_id"] integerValue];
                if (video_id == self.video_id) {
                    self.player.currentPlayerManager.seekTime = [dic[@"seekTime"] integerValue];
                }
            }
        }
    }
    self.player.playerPlayTimeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSTimeInterval currentTime, NSTimeInterval duration) {
        @strongify(self)
        self.currentTime = currentTime;
    };

    [self.controlView showTitle:self.videomodel.video_title coverURLString:[NSString stringWithFormat:@"%@%@",KKaptcha,self.videomodel.video_img] fullScreenMode:ZFFullScreenModeLandscape];
    
}


- (void)setupTableView{
    [self.view addSubview:self.tableView];
    CGFloat titleHeight = 25;
    @weakify(self)
    if (_videomodel.fk_user_id) {
        _headerView = [ParentVideoDetailHeaderView haveUserVideoDetailHeaderView];
        _headerView.height = 150+titleHeight;
        _headerView.videomodel = _videomodel;

        _headerView.showAbstractBlock = ^{
            @strongify(self)
            if (self.headerView.time_lab.hidden == NO ) {
                self.tableView.tableHeaderView.height = 150+titleHeight;
                self.headerView.title_lab.numberOfLines = 1;
                self.headerView.abstract_lab.hidden = YES;
                self.headerView.time_lab.hidden = YES;
                [self.headerView.more_button setImage:ImageName(@"展开") forState:0];
            } else {
                CGFloat height = 190+[self calculateString:self.videomodel.video_title Width:kScreenWidth-80 Font:20]+[self calculateString:self.videomodel.video_abstract Width:kScreenWidth-30 Font:13];
                self.tableView.tableHeaderView.height = height;
                self.headerView.title_lab.numberOfLines = 0;
                self.headerView.abstract_lab.hidden = NO;
                self.headerView.time_lab.hidden = NO;
                [self.headerView.more_button setImage:ImageName(@"收起") forState:0];
            }
            [self.tableView reloadData];
        };
    } else {
        _headerView = [ParentVideoDetailHeaderView noUserVideoDetailHeaderView];
        _headerView.videomodel = _videomodel;
        _headerView.height = 90 + titleHeight;

        _headerView.showAbstractBlock = ^{
            @strongify(self)
            if (self.headerView.time_lab.hidden == NO ) {
                self.tableView.tableHeaderView.height = titleHeight+90;
                self.headerView.title_lab.numberOfLines = 1;
                self.headerView.abstract_lab.hidden = YES;
                self.headerView.time_lab.hidden = YES;
                [self.headerView.more_button setImage:ImageName(@"展开") forState:0];
            } else {
                CGFloat height = 140+[self calculateString:self.videomodel.video_title Width:kScreenWidth-80 Font:20]+[self calculateString:self.videomodel.video_abstract Width:kScreenWidth-30 Font:13];
                self.tableView.tableHeaderView.height = height;
                self.headerView.title_lab.numberOfLines = 0;
                self.headerView.abstract_lab.hidden = NO;
                self.headerView.time_lab.hidden = NO;
                [self.headerView.more_button setImage:ImageName(@"收起") forState:0];
            }
            [self.tableView reloadData];
        };
    }
    _headerView.abstract_lab.hidden = YES;
    _headerView.time_lab.hidden = YES;
    self.tableView.tableHeaderView = _headerView;
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)width Font:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    return size.height;
}



-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
    [self.maskView addSubview:self.inputToolbar];
}


-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (IS_iPhoneXStyle) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    }
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    //去掉导航栏底部的黑线
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    if (self.player.currentPlayerManager.isPreparedToPlay) {
        [self.player addDeviceOrientationObserver];
        if (!self.player.currentPlayerManager.isPlaying) {
            self.player.pauseByEvent = NO;
        }
    }
    self.player.allowOrentitaionRotation = YES; //可以全屏

}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    self.player.allowOrentitaionRotation = NO; //禁止全屏

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认

    self.navigationController.navigationBar.translucent = NO;

    [self.player.currentPlayerManager pause]; //暂停

//    if (self.player.currentPlayerManager.isPreparedToPlay) {
//        [self.player removeDeviceOrientationObserver];
//        if (self.player.currentPlayerManager.isPlaying) {
//            self.player.pauseByEvent = YES;
//        }
//    }
    
    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"VideoSeekTime.plist"];
    
    NSDictionary *dict = @{
                           @"video_id":@(self.videomodel.video_id),
                           @"seekTime":@(_currentTime)
                           };
    
    NSMutableArray *VideoSeekTimeArray = [NSMutableArray arrayWithCapacity:3];
    VideoSeekTimeArray = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    if (VideoSeekTimeArray == nil) {
        VideoSeekTimeArray = [NSMutableArray array];
        [VideoSeekTimeArray addObject:dict];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *dic in VideoSeekTimeArray) {
        [array addObject:@([dic[@"video_id"] integerValue])];
    }
    if(![array containsObject:@(self.videomodel.video_id)]) {
        [VideoSeekTimeArray insertObject:dict atIndex:0];
    }
    
    for (int i = 0; i < VideoSeekTimeArray.count; i++) {
        NSDictionary *dic = VideoSeekTimeArray[i];
        if ([dic[@"video_id"] integerValue] == self.videomodel.video_id) {
            [VideoSeekTimeArray replaceObjectAtIndex:i withObject:dict];
        }
    }
    if (VideoSeekTimeArray.count > 5) {
        NSRange range = NSMakeRange(0, 5);
        VideoSeekTimeArray = [VideoSeekTimeArray subarrayWithRange:range];
    }
    
    if ([VideoSeekTimeArray writeToFile:filePatch atomically:YES]){
        ZPLog(@"sucess");
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceiveNotification object:self];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    self.navigationController.navigationBar.translucent = NO;
    [self addReadTime];
}

- (void)addReadTime{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KRecordLog];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resources_type":@(resources_video),
                                 @"resources_id":@(_video_id),
                                 @"status":@(1),
                                 @"message_type":[constant TransactionState:(MODULE_VIDEO_CLOSE)],
                                 };
    URL = [NSString connectUrl:parameters url:URL];

    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
        } else {
        }
    } failure:^(NSError *error) {
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.player.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)shouldAutorotate {
    return NO;
}


//刷新
-(void)refreshTableview {
    
    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getCommentList)];

}


//获取评论列表
- (void)getCommentList{
    if (_lastPage) {
        [_tableView.mj_footer endRefreshingWithNoMoreData];
        return ;
    }
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":[NSString stringWithFormat:@"%ld",_video_id],
                                 @"comment_type":@(video_comment),
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        [self.tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            if (commentList && commentList.count == 10) {
                _lastPage = NO;
            } else{
                _lastPage = YES;
            }
            for (NSDictionary *dic in commentList) {
                CommentModel *commentmodel = [CommentModel modelWithJSON:dic];
                [self.commentListArray addObject:commentmodel];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];

        [self.tableView.mj_footer endRefreshing];
    }];
}


//获取相关推荐
- (void)getRecommendVideoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendVideo];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"video_id" : @(_video_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                VideoModel *model = [VideoModel modelWithJSON:dic];
                [self.recommendArray addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
#pragma tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_showMore) {
            return _recommendArray.count;
        } else {
            if (_recommendArray.count > 3){
                return 3;
            } else {
                return _recommendArray.count;
            }
        }
    } else {
        if (_commentListArray && _commentListArray.count > 0) {
            return _commentListArray.count;
        }
        return 0;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        if (_recommendArray.count > 0){
            CommentListCell *head = [CommentListCell theRecommendHeaderView];
            return head;
        } else {
            return nil;
        }
    } else {
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
            head.comment_number_lab.text = [NSString stringWithFormat:@"评论(%ld)",_comment_number];
        }
        return head;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        if (_recommendArray.count > 0){
            return 60;
        } else {
            return 0;
        }
    } else {
        return 55;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (_recommendArray.count > 3){
            return self.footerView;
        } else {
            return nil;
        }
    } else {
        if (self.commentListArray.count == 0) {
            CommentListCell *foot = [CommentListCell theNoCommentCell];
            return foot;
        }
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (_recommendArray.count > 3) {
            return 50;
        } else {
            return 0;
        }
    } else {
        if (self.commentListArray.count == 0) {
            return 100;
        }
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ParentVideoDetailCell *detailcell = [ParentVideoDetailCell theParentVideoDetailCellWithTableView:tableView];
        if (_recommendArray && _recommendArray.count > indexPath.row) {
            detailcell.model = [_recommendArray objectAtIndex:indexPath.row];
            detailcell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return detailcell;
    } else {
        CommentCell *cell = (CommentCell *) [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentCell" owner:nil options:nil] lastObject];
        }
        if (self.commentListArray && self.commentListArray.count > indexPath.row) {
            cell.CommentCellIndex =[indexPath row];
            cell.commentmodel = [_commentListArray objectAtIndex:[indexPath row]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.commentpraisetype = praise_video_comment;
            cell.replaypraisetype = praise_video_replay;
            cell.delegate = self;
        }
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 105;
    } else {
        CommentModel *model = [_commentListArray objectAtIndex:[indexPath row]];
        if (model.height==0) {
            return [CommentListCell cellHeight];
        }else{
            return model.height;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (_recommendArray.count > indexPath.row) {
            [self.player.currentPlayerManager pause];
            VideoModel *model = _recommendArray[indexPath.row];
            [self recommendClickNewsId:model.video_id];
            ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
            VC.video_id = model.video_id;
            [self.navigationController pushViewController:VC animated:YES];
//            侧滑返回时可直接返回到列表
            NSMutableArray *array = self.navigationController.viewControllers.mutableCopy;
            [array removeObjectAtIndex:array.count-2];
            [self.navigationController setViewControllers:array animated:NO];
        }
    } else {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        CommentModel *commentmodel = [_commentListArray objectAtIndex:[indexPath row]];
        if (commentmodel.fk_user_id == [USER_ID integerValue]) {
            [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"复制" block1:^{
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = commentmodel.comment_content;
                [UIView ShowInfo:@"复制成功" Inview:self.view];
            } title2:@"删除" block2:^{
                NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
                NSDictionary *parameters = @{
                                             @"user_id":USER_ID,
                                             @"comment_id":[NSString stringWithFormat:@"%ld",(long)commentmodel.comment_id],
                                             @"comment_type":@(video_comment)
                                             };
                [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                    NSDictionary *json = responseObject;
                    ZPLog(@"%@",json);
                    ZPLog(@"%@",json[@"message"]);
                    BOOL success = [json[@"success"]boolValue];
                    if (success) {
                        _comment_number --;
//                        self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
                        [_commentListArray removeObjectAtIndex:indexPath.row];
                        [_tableView reloadData];
                    } else {
                        [UIView ShowInfo:json[@"message"] Inview:self.view];
                    }
                } failure:^(NSError *error) {
                    
                }];
            }];
        } else {
            [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"回复" block1:^{
                [self CommentCellCommentButton:commentmodel CommentCellIndex:indexPath.row];
            } title2:@"举报" block2:^{
                [self addAccusationTargetid:commentmodel.comment_id];
                
            }];
        }
    }
}

//点击相关推荐
- (void)recommendClickNewsId:(NSInteger )video_id{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KRecordLog];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resources_type":@(resources_video),
                                 @"resources_id":@(video_id),
                                 @"status":@(1),
                                 @"message_type":[constant TransactionState:(MODULE_RECOMMEND_CLICK)],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
        } else {
        }
    } failure:^(NSError *error) {
    }];
}

#pragma mark - BottomViewCellDelegate
- (void)commentButtonClick{
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addCommentwithInputString:text];
    }];
}

//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":[NSString stringWithFormat:@"%ld" ,_video_id]
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(video_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];

            CommentModel *model = [CommentModel modelWithJSON:json[@"data"]];
            [_commentListArray insertObject:model atIndex:0];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark - CommentCellDelegate
/** 查看更多 收起更多 */
- (void)CommentCellShowHiddeMore:(CommentModel *)commentmodel row:(NSInteger )index{
    commentmodel.height = 0.0;
    [_commentListArray replaceObjectAtIndex:index withObject:commentmodel];
    [_tableView reloadData];
    
    NSIndexPath * indexpath = [NSIndexPath indexPathForRow:index inSection:1];
    [self performSelector:@selector(tableviewScrollToRowAtIndexPath:) withObject:indexpath afterDelay:0.2];
}

- (void)tableviewScrollToRowAtIndexPath:(NSIndexPath *)indexpath{
    [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:NO];

}

/** ReplayCell 点击事件 */
- (void)CommentCellReplayCell:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex didSelectRow:(NSInteger)index{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (replaymodel.replay_user_id == [USER_ID integerValue]) {
        [self showActionSheetWithmessage:replaymodel.replay_content Title1:@"复制" block1:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = replaymodel.replay_content;
            [UIView ShowInfo:@"复制成功" Inview:self.view];
        } title2:@"删除" block2:^{
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteReplayById];
            NSDictionary *parameters = @{
                                         @"user_id":USER_ID,
                                         @"replay_id":[NSString stringWithFormat:@"%ld",(long)replaymodel.replay_id],
                                         @"replay_type":@(video_comment)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@",json);
                ZPLog(@"%@",json[@"message"]);
                BOOL success = [json[@"success"]boolValue];
                if (success) {
                    _comment_number -- ;
//                    self.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
                    CommentModel *commentmodel = [_commentListArray objectAtIndex:commentindex];
                    [commentmodel.replayList removeObjectAtIndex:index];
                    commentmodel.height = 0;
                    [_tableView reloadData];
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:self.view];
                }
            } failure:^(NSError *error) {
                
            }];
        }];
    } else {
        [self showActionSheetWithmessage:replaymodel.replay_content Title1:@"回复" block1:^{
            [self ReplayCellReplayButton:replaymodel CommentCellIndex:commentindex ReplayCellIndex:index];
        } title2:@"举报" block2:^{
            [self addAccusationTargetid:replaymodel.replay_id];
        }];
    }
}

/** CommentCell 回复 点击事件 */
- (void)CommentCellCommentButton:(CommentModel *)commentmodel CommentCellIndex:(NSInteger)commentindex{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
    NSString *commnentStr = [userDefault objectForKey:KUDLastCommentString];
    if (commnentStr.length == 0) {
        self.inputToolbar.placeholder = [NSString stringWithFormat:@"回复 %@:",commentmodel.comment_name];
    }
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf replayComment:commentmodel index:commentindex InputString:text];
    }];
}

/** ReplayCell 回复 点击事件 */
- (void)ReplayCellReplayButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    self.maskView.hidden = NO;
    [self.inputToolbar popToolbar];
    NSString *commnentStr = [userDefault objectForKey:KUDLastCommentString];
    if (commnentStr.length == 0) {
        self.inputToolbar.placeholder = [NSString stringWithFormat:@"回复 %@:",replaymodel.replay_name];
    }
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf replayCommentReplay:replaymodel CommentCellIndex:commentindex ReplayCellIndex:replayindex InputString:text];
    }];
}



//对一级评论进行回复
- (void)replayComment:(CommentModel *)commentmodel index:(NSInteger )index InputString:(NSString *)inputstr{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCommentReplay];
    NSDictionary *dic = @{
                          @"replay_user_id":USER_ID,
                          @"replay_content":inputstr,
                          @"fk_comment_id":@(commentmodel.comment_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment_repaly":[NSString convertToJsonData:dic],
                                 @"replay_type":@(video_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];

            NSDictionary *data = json[@"data"];
            [commentmodel.replayList addObject:data];
            commentmodel.height = 0;
            [_commentListArray replaceObjectAtIndex:index withObject:commentmodel];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//对二级评论进行回复
- (void)replayCommentReplay:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex InputString:(NSString *)inputstr{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCommentReplay];
    NSDictionary *dic = @{
                          @"replay_user_id":USER_ID,
                          @"fk_to_user_id":@(replaymodel.replay_user_id),
                          @"replay_content":inputstr,
                          @"fk_comment_id":@(replaymodel.fk_comment_id),
                          @"parent_id":@(replaymodel.replay_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment_repaly":[NSString convertToJsonData:dic],
                                 @"replay_type":@(video_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            _comment_number ++ ;
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];

            NSDictionary *data = json[@"data"];
            CommentModel *commentmodel = [_commentListArray objectAtIndex:commentindex];
            [commentmodel.replayList addObject:data];
            commentmodel.height = 0;
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

//举报评论
- (void)addAccusationTargetid:(NSInteger )targetid{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"from_id":@(targetid),
                                 @"type_id":@(accusation_video_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"举报成功" Inview:self.view];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        
    }];
}



#pragma -mark- BottomCommetViewDelegate
//收藏
- (void)collectButtonClicked{
    if (_videomodel.is_collect_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _videomodel.video_id],
                                     @"user_id":USER_ID,
                                     @"collect_type":@(collect_type_video)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _videomodel.is_collect_true = is_collect_true;
                _videomodel.collect_number++;
                [UIView ShowInfo:TipCollectSuccess Inview:self.view];
                _commentview.haveCollect = _videomodel.is_collect_true;
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        NSDictionary *parameters = @{
                                     @"idList":@(_videomodel.is_collect_true)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _videomodel.is_collect_true = 0;
                _videomodel.collect_number--;
                [UIView ShowInfo:TipUnCollectSuccess Inview:self.view];
                _commentview.haveCollect = _videomodel.is_collect_true;
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}


- (void)shareButtonClicked{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _videomodel.video_title;
    model.descr = _videomodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    model.webpageUrl = _videomodel.share_url;
    model.accusationType = accusation_type_video;
    model.from_id = _video_id;
    model.shareType = share_video;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)share:(id)sender {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _videomodel.video_title;
    model.descr = _videomodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    model.webpageUrl = _videomodel.share_url;
    model.accusationType = accusation_type_video;
    model.from_id = _video_id;
    model.shareType = share_video;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}



- (UIView *)playerView{
    if (!_playerView) {
        if (IS_iPhoneXStyle) {
            UIView *effect = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, TopeffectViewH)];
            effect.backgroundColor = [UIColor blackColor];
            [self.view addSubview:effect];
        }
        _playerView = [[UIView alloc] initWithFrame:CGRectMake(0, TopeffectViewH, kScreenWidth, PlayerHeight)];
        if (IS_iPhoneXStyle) {
            _playerView.backgroundColor = [UIColor blackColor];
        } else {
            _playerView.backgroundColor = [UIColor grayColor];
        }
    }
    return _playerView;
}


- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        ShareModel *model = [[ShareModel alloc] init];
        model.titleStr = _videomodel.video_title;
        model.descr = _videomodel.video_abstract;
        model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
        model.webpageUrl = _videomodel.share_url;
        model.accusationType = accusation_type_video;
        model.from_id = _video_id;
        model.shareType = share_video;
        _controlView.landScapeControlView.sharemodel = model;
    }
    return _controlView;
}


- (VideoPlayEndVIew *)playendview{
    if (!_playendview) {
        _playendview = [[NSBundle mainBundle] loadNibNamed:@"VideoPlayEndVIew" owner:nil options:nil][0];
        _playendview.frame = CGRectMake(0, TopeffectViewH, kScreenWidth, PlayerHeight);
        ShareModel *model = [[ShareModel alloc] init];
        model.titleStr = _videomodel.video_title;
        model.descr = _videomodel.video_abstract;
        model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
        model.webpageUrl = _videomodel.share_url;
        model.from_id = _video_id;
        model.shareType = share_video;
        _playendview.sharemodel = model;
        _playendview.hidden = YES;
    }
    return _playendview;
}


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, TopeffectViewH+PlayerHeight, kScreenWidth, kScreenHeight-TopeffectViewH-effectViewH-PlayerHeight-commentviewHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //预计高度为143--即是cell高度
        _tableView.estimatedRowHeight = 150.0f;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        //自适应高度
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


- (BottomCommetView *)commentview{
    if (_commentview == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _commentview = apparray.firstObject;
        _commentview.haveCollect = _videomodel.is_collect_true;
        _commentview.frame = CGRectMake(0,  kScreenHeight-commentviewHeight-effectViewH, kScreenWidth, commentviewHeight);
        _commentview.delegate = self;
        @weakify(self)
//        _commentview.CommentButtonBlock = ^{
//            @strongify(self)
//            self.maskView.hidden = NO;
//            [self.inputToolbar popToolbar];
//        };
        _commentview.LoctionButtonBlock = ^{
            @strongify(self)
            if (self.commentListArray && self.commentListArray.count > 0) {
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                [self performSelector:@selector(tableviewScrollToRowAtIndexPath:) withObject:scrollIndexPath afterDelay:0.2];
            } else {
                if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
                    CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
                    [self.tableView setContentOffset:offset animated:YES];
                }
            }
        };
    }
    return _commentview;
}


- (SomeCircleFooterView *)footerView{
    if (!_footerView) {
        if (_footerView == nil) {
            _footerView = [[SomeCircleFooterView alloc] init];
            _footerView.frame = CGRectMake(0, 0, kScreenWidth, 50);
            _footerView.upTitle = @"收起更多视频";
            _footerView.downTitle = @"查看更多视频";
            __weak typeof(self) weakSelf = self;
            _footerView.MoreTopic = ^(BOOL more) {
                weakSelf.showMore = more;
                [weakSelf.tableView reloadData];
                if (!iOS12Later) {
                    [weakSelf.tableView setContentOffset:CGPointMake(0,64*2) animated:NO];
                }
            };
        }
    }
    return _footerView;
}


- (NSMutableArray *)commentListArray{
    if (!_commentListArray) {
        _commentListArray = [NSMutableArray array];
    }
    return _commentListArray;
}

- (NSMutableArray *)recommendArray{
    if (!_recommendArray) {
        _recommendArray = [NSMutableArray array];
    }
    return _recommendArray;
}





@end

