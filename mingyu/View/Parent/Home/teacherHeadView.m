//
//  teacherHeadView.m
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "teacherHeadView.h"

@implementation teacherHeadView


- (void)awakeFromNib{
    [super awakeFromNib];
    _user_head.layer.cornerRadius = 35;
    _user_head.layer.masksToBounds = YES;
    if (kScreenWidth == 320) {
        self.constraint.constant = 90;
    }
    
}



- (void)setTeachermodel:(TeacherModel *)teachermodel{
    _teachermodel = teachermodel;
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,teachermodel.user_head];
    [_user_head zp_setImageWithURL:teachermodel.user_head placeholderImage:ImageName(@"默认头像")];
    _real_name_lab.text = teachermodel.teacher_name;
    _teacherinfo_lab.text = [NSString stringWithFormat:@"%@ %@",teachermodel.teacher_organize,teachermodel.teacher_position];
    _playcount_lab.text = [NSString stringWithFormat:@"%ld播放",teachermodel.playCount];
    _followUserNumber_lab.text = [NSString stringWithFormat:@"%ld粉丝",teachermodel.followUserNumber];
    _profile_lab.text = _teachermodel.teacher_profile;
    
    [self changeAttentionButton];
    
    if (_teachermodel.fk_user_id == [USER_ID integerValue]) {
        _attention_button.hidden = YES;
    } else {
        _attention_button.hidden = NO;
    }
}

- (void)changeAttentionButton{
    _attention_button.layer.cornerRadius = 3;
    _attention_button.layer.borderWidth = 1;
    if (_teachermodel.follow_status == 1) {
        _attention_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_attention_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_attention_button setTitle:@" + 关注 " forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
//
//        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"50D0F4"].CGColor;
//        [_attention_button setTitleColor:[UIColor colorWithHexString:@"50D0F4"] forState:UIControlStateNormal];
//        [_attention_button setTitle:@"+关注" forState:UIControlStateNormal];
    } else if (_teachermodel.follow_status == 2){
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"已关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    } else {
        _attention_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_attention_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_attention_button setTitle:@"互相关注" forState:UIControlStateNormal];
        _attention_button.backgroundColor = [UIColor whiteColor];
    }
}




- (IBAction)attentionButton:(id)sender {
    
    _attention_button.userInteractionEnabled = NO;
    if (_teachermodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.fk_user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _teachermodel.follow_status = follow_status;
                [self changeAttentionButton];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":[NSString stringWithFormat:@"%ld",(long)_teachermodel.fk_user_id]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _attention_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _teachermodel.follow_status = 1;
                [self changeAttentionButton];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _attention_button.userInteractionEnabled = YES;
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}


@end
