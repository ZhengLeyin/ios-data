//
//  MessageModelFrame.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>
@class QQMessageModel;

@interface MessageModelFrame : NSObject

/** 时间的Frame */
@property (nonatomic,assign)CGRect timeFrame;
/** 头像的Frame */
@property (nonatomic,assign)CGRect headImageFrame;
/** 按钮(内容)的Frame */
@property (nonatomic,assign)CGRect btnFrame;
/** cell的高度 */
@property (nonatomic,assign)CGFloat cellHeight;
/** 模型数据 */
@property (nonatomic,strong)QQMessageModel *messageModel;

/** 通过模型数据计算出对应的Frame,并且返回出modelFrame自己这个对象 */
-(instancetype)initWithFrameModel:(QQMessageModel *)messageModel timeIsEqual:(BOOL)timeBool;
//和上面同理
+(instancetype)modelFrame:(QQMessageModel *)messageModel timeIsEqual:(BOOL)timeBool;


@end
