//
//  UserInfoViewController.m
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "UserInfoViewController.h"
#import "TZImagePickerController.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "STPickerArea.h"
#import <PGDatePickManager.h>
#import <UMShare/UMShare.h>
#import "QiniuSDK.h"

@interface UserInfoViewController ()<TZImagePickerControllerDelegate,UINavigationControllerDelegate,STPickerAreaDelegate,PGDatePickerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *user_head_image;

//@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;

@property (nonatomic, strong) NSArray *keyArray;
@property (nonatomic, strong) NSArray *valueArray;

@property (nonatomic, strong) UserModel *usermodel;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _keyArray = @[@"昵称",@"地区",@"收货地址",@"我的状态",@"宝宝生日",@"绑定手机"];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    NSString *head_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDuser_head]];
    [_user_head_image zp_setImageWithURL:[userDefault objectForKey:KUDuser_head] placeholderImage:ImageName(@"默认头像")];
    [self getUserMessage];
}


- (void)getUserMessage {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserMessage];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _usermodel = [UserModel modelWithJSON:json[@"data"]];
            
            NSString *user_name        = [NSString stringWithFormat:@"%@",_usermodel.user_name?_usermodel.user_name:@""];
            NSString *area_name        = [NSString stringWithFormat:@"%@",_usermodel.area_name?_usermodel.area_name:@""];
            NSString *address_detail   = [NSString stringWithFormat:@"%@",_usermodel.address_detail?_usermodel.address_detail:@""];
            NSString *user_identity    = [NSString stringWithFormat:@"%@",_usermodel.user_identity == 1?@"我是宝爸":@"我是宝妈"];
            NSString *user_birthday    = [NSString stringWithFormat:@"%@",_usermodel.user_birthday?_usermodel.user_birthday:@""];
            NSString *phone            = _usermodel.user_phone ? [_usermodel.user_phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"] :@"";
//            NSString *weixin           = [NSString stringWithFormat:@"%@",_usermodel.is_true_binding?@"已绑定":@"去绑定"];

            _valueArray = @[user_name,area_name,address_detail,user_identity,user_birthday,phone];
            if (_usermodel.user_birthday) {
                [userDefault setObject:_usermodel.user_birthday forKey:KUDuser_birthday];
                [userDefault synchronize];
            }
            
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



- (IBAction)changeUserHead:(id)sender {
    
    [self.navigationController presentViewController:self.imagePickerVc animated:YES completion:nil];

}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _keyArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"UserInfoViewCell";
    UserInfoViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UserInfoViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    [cell setcellWithkeyArray:_keyArray valueArray:_valueArray index:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        if (_usermodel && _usermodel.name_status == 0) {
            ChangeNameViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ChangeNameViewController"];
            VC.name = _usermodel.user_name;
            [self.navigationController pushViewController:VC animated:YES];
        } else{
            [UIView ShowInfo:@"昵称只能修改1次" Inview:self.view];
        }
    } else if (indexPath.row == 1){
        STPickerArea *pickerArea = [[STPickerArea alloc]init];
        pickerArea.heightPickerComponent = 60;
        pickerArea.numberOfComponents = 2;
        [pickerArea setDelegate:self];
        [pickerArea setContentMode:STPickerContentModeBottom];
        [pickerArea show];
        
    }else if (indexPath.row == 2){
        LocationViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"LocationViewController"];
        [self.navigationController pushViewController:VC animated:YES];
        
    }else if (indexPath.row == 3){
        IdentityViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"IdentityViewController"];
        VC.user_identity = _usermodel.user_identity;
        [self.navigationController pushViewController:VC animated:YES];
    }else if (indexPath.row == 4){
        PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
        PGDatePicker *datePicker = datePickManager.datePicker;
        datePicker.delegate = self;
        datePicker.datePickerMode = PGDatePickerModeDate;
        [self presentViewController:datePickManager animated:false completion:nil];
        
        //    datePickManager.titleLabel.text = @"PGDatePicker";
        //设置半透明的背景颜色
        datePickManager.isShadeBackgroud = true;
        //设置头部的背景颜色
        datePickManager.headerViewBackgroundColor = [UIColor whiteColor];
        //设置线条的颜色
        datePicker.lineBackgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
        //设置选中行的字体颜色
        datePicker.textColorOfSelectedRow = [UIColor colorWithHexString:@"2c2c2c"];
        //设置未选中行的字体颜色
        //设置取消按钮的字体颜色
        datePickManager.cancelButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
        //设置取消按钮的字
        //    datePickManager.cancelButtonText = @"取消";
        //    //设置取消按钮的字体大小
        //    datePickManager.cancelButtonFont = [UIFont boldSystemFontOfSize:17];
        //
        //    //设置确定按钮的字体颜色
        datePickManager.confirmButtonTextColor = [UIColor colorWithHexString:@"2c2c2c"];
        //    //设置确定按钮的字
        //    datePickManager.confirmButtonText = @"确定";
        //    //设置确定按钮的字体大小
        //    datePickManager.confirmButtonFont = [UIFont boldSystemFontOfSize:17];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        datePicker.minimumDate = [dateFormatter dateFromString: @"2003-01-01"];
        datePicker.maximumDate = [dateFormatter dateFromString: [NSString getCurrentTimes:@"yyyy-MM-dd"]];
//        STPickerDate *pickerDate = [[STPickerDate alloc]init];
//        pickerDate.heightPickerComponent = 60;
//        [pickerDate setDelegate:self];
//        [pickerDate show];
        
    }else if (indexPath.row == 5){
        NSString *message = [NSString stringWithFormat:@"您已绑定手机号：%@ \n确认要修改么？",_usermodel.user_phone];
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" "
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                             }];
        [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
        
        UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认修改" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  ChangePhoneViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ChangePhoneViewController"];
                                                                  [self.navigationController pushViewController:VC animated:YES];
                                                              }];
        [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
        
        [alert addAction:cancelAction];
        [alert addAction:confirmAction];
        
        [self presentViewController:alert animated:YES completion:nil];

    }
}



#pragma mark - --- STPickerAreaDelegate ---
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area
{
    NSString *text = [NSString stringWithFormat:@"%@ %@", province, city];
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{
                          @"area_name":text,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
            UserInfoViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
            cell.value_lab.text = text;
            [UIView ShowInfo:TipChangePlace Inview:self.view];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    //    ZPLog(@"dateComponents = %@", dateComponents);
    NSString *text = [NSString stringWithFormat:@"%zd-%02ld-%02ld",dateComponents.year,(long)dateComponents.month,(long)dateComponents.day];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
    UserInfoViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    cell.value_lab.text = text;
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{
                          @"user_birthday":text,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:TipChagneBirthday Inview:self.view];
            [userDefault setObject:text forKey:KUDuser_birthday];
            [userDefault synchronize];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
            UserInfoViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
            cell.value_lab.text = text;
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationBirthdayChange object:nil userInfo:nil];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 51;
}



#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    if (photos) {
//        [self uploadImage:photos[0]];
        NSData *imageData = UIImageJPEGRepresentation(photos[0], 1.0 );
        [self getUpToken:imageData];
    }
}

//获取七牛云凭证
- (void)getUpToken:(NSData *)imageData{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetUpToken];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
    NSDictionary *parameters = @{
                          @"bucketName":bucketName,
                          @"type":@"user",
                          @"fileName":fileName
                          };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            [self uploadImageToQNPutData:imageData key:data[@"key"] token:data[@"token"]];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

//上传图片到七牛云
- (void)uploadImageToQNPutData:(NSData *)data key:(NSString *)key token:(NSString *)token{
    QNUploadManager *upManger = [[QNUploadManager alloc] init];
    QNUploadOption *uploadoption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
        ZPLog(@"percent == %.2f", percent);
    } params:nil checkCrc:NO cancellationSignal:nil];
    
    [upManger putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        ZPLog(@"info ===== %@", info);
        ZPLog(@"resp ===== %@", resp);
        [self uploadUserHead:resp[@"key"]];

    } option:uploadoption];
}


////上传图像
//- (void)uploadImage:(UIImage *)image{
//
//    NSString *URL = [NSString stringWithFormat:@"%@%@?type=user",KURL,KImgUpload];
//
//    // 1.创建请求管理者
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
//                                                         @"text/html",
//                                                         @"image/jpeg",
//                                                         @"image/png",
//                                                         @"application/octet-stream",
//                                                         @"text/json",
//                                                         @"multipart/form-data",
//                                                         nil];
//    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
//
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"token"] forHTTPHeaderField:@"token"];   //token
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"device_id"] forHTTPHeaderField:@"device_id"];
//    [manager.requestSerializer setValue:[HttpRequest getsignwithURL:URL] forHTTPHeaderField:@"sign"];
//
//    [manager POST:URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        NSData *imageData = UIImageJPEGRepresentation(image, 1.0 );
//
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.dateFormat = @"yyyyMMddHHmmss";
//        NSString *str = [formatter stringFromDate:[NSDate date]];
//        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
//        NSString *name = @"file";
//        [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
//        ZPLog(@"%@",formData);
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        ZPLog(@"%@",json);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//
//            [self uploadUserHead:json[@"data"]];
////            [_user_head_image sd_setImageWithURL:[NSURL URLWithString:json[@"data"]]];
//
//        } else {
//            [UIView ShowInfo:json[@"message"] Inview:self.view];
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        ZPLog(@"%@",error);
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
//
//    }];
//}


//更新用户图像
- (void)uploadUserHead:(NSString *)userhead{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"user_head":userhead,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
//            NSString *url = [NSString stringWithFormat:@"%@%@",KKaptcha,userhead];
            [MobClick event:@"ChangeUserHeader"];  //友盟统计

            [_user_head_image zp_setImageWithURL:userhead placeholderImage:ImageName(@"默认头像")];
            [userDefault setObject:userhead forKey:KUDuser_head];
            [userDefault synchronize];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(TZImagePickerController *)imagePickerVc {
    _imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    _imagePickerVc.allowPickingOriginalPhoto = NO;
    _imagePickerVc.naviTitleColor = [UIColor blackColor];
    _imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"50D0F4"];
    _imagePickerVc.isStatusBarDefault = YES;
    _imagePickerVc.delegate = self;
    _imagePickerVc.allowTakePicture = YES;
    _imagePickerVc.allowPickingVideo = NO;
    _imagePickerVc.allowPickingGif = NO;
    _imagePickerVc.naviBgColor = [UIColor whiteColor];
    _imagePickerVc.maxImagesCount = 1;
    _imagePickerVc.showSelectBtn = NO;
    _imagePickerVc.allowCrop = YES;
    _imagePickerVc.cropRect = CGRectMake((kScreenWidth-240)/2 , (kScreenHeight-240)/2, 240, 240);
    return _imagePickerVc;
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end




@implementation UserInfoViewCell

- (void)setcellWithkeyArray:(NSArray *)key valueArray:(NSArray *)value index:(NSInteger)index{

    _key_lab.text =key[index];
    _value_lab.text = value[index];
}



@end
