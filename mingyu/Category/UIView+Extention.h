//
//  UIView+Extention.h
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extention)




+ (void)ShowInfo:(NSString *)info Inview:(UIView *)view;

+ (void)ShowInfo:(NSString *)info Inview:(UIView *)view Time:(NSInteger )time;


@end
