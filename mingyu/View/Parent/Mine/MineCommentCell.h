//
//  MineCommentCell.h
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyCommentMessageModel;

@interface MineCommentCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *content_lab;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;

@property (nonatomic, strong) MyCommentMessageModel *model;


+ (instancetype)theMineCommentCellWithtableView:(UITableView *)tableview;


@end

