//
//  LabelListModel.h
//  mingyu
//
//  Created by apple on 2018/5/5.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LabelListModel : NSObject<NSCoding>


/** //标签id */
@property (nonatomic, assign) NSInteger label_id;

/** 外键id */
@property (nonatomic, assign) NSInteger fk_classify_id;

/** 标签名 */
@property (nonatomic, copy) NSString *label_name;

/** 是否被选择 */
@property (nonatomic, assign) NSInteger is_true_select;

/** 最小时间 */
@property (nonatomic, strong) NSNumber *min_time;

/** 最大时间 */
@property (nonatomic, strong) NSNumber *max_time;

/** 最后一页 */
@property (nonatomic, assign) BOOL last_page;

@end




@interface ChildCareClassifyModel : NSObject

@property (nonatomic, copy) NSString *classify_name;

@property (nonatomic, strong) NSArray *labelList;

@end


