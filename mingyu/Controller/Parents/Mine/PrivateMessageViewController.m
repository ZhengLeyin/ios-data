//
//  PrivateMessageViewController.m
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PrivateMessageViewController.h"
#import "PrivateMessageCell.h"
#import "PrivateMessageDetailViewController.h"
#import "PPBadgeView.h"

@interface PrivateMessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;


@end

@implementation PrivateMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getQQUserList) name:NSNotificationReceivePriVateMessage object:nil];

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.arrayData removeAllObjects];
   if ([userDefault boolForKey:KUDhasLogin]) {
        [self getQQUserList];
   }

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceivePriVateMessage object:nil];
}

- (void)getQQUserList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetQQUserList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self.arrayData removeAllObjects];
            NSDictionary *data = json[@"data"];
            if ([data[@"qq_unread_number"] integerValue] > 0) {
                [self.navigationController.navigationBar pp_showBadge];
            } else {
                [self.navigationController.navigationBar pp_hiddenBadge];
            }
            NSArray *qq_userList = data[@"qq_userList"];
            for (NSDictionary *dic in qq_userList) {
                QQUserModel *model = [QQUserModel modelWithJSON:dic];
                ZPLog(@"%@",model.message_content);
                [self.arrayData addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return self.arrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PrivateMessageCell *cell = [PrivateMessageCell thePrivateMessageCellWithTableView:tableView];
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        cell.userModel = self.arrayData[indexPath.row];
        UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGR:)];
        longPressGR.minimumPressDuration = 1;
        [cell addGestureRecognizer:longPressGR];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        PrivateMessageDetailViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PrivateMessageDetailViewController"];
        QQUserModel *userModel = self.arrayData[indexPath.row];
        if (userModel.to_user_id == [USER_ID integerValue]) {
            VC.user_id = userModel.from_user_id;
        } else {
            VC.user_id = userModel.to_user_id;
        }
        VC.qq_id = userModel.qq_id;
        VC.user_head = userModel.user_head;
        VC.title = userModel.user_name;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


- (void)longPressGR:(UILongPressGestureRecognizer *)longPressGR{
    CGPoint point = [longPressGR locationInView:self.tableView];
    NSIndexPath *index = [self.tableView indexPathForRowAtPoint:point];  //可以获取我们在哪个cell上长按
    NSInteger indexNum = index.row;
    
    if (longPressGR.state == UIGestureRecognizerStateEnded) { //手势结束
        QQUserModel *model = _arrayData[indexNum];
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteQQUser];
        NSString *message = [NSString stringWithFormat:@"确认删除私信:%@",model.user_name];
        [self showAlertWithTitle:nil message:message noBlock:nil yseBlock:^{
            NSDictionary *parameters = @{
                                         @"qq_id": @(model.qq_id),
                                         @"user_id":USER_ID
                                         };
            [HttpRequest deleteWithURLString:URL parameters:parameters viewcontroller:nil showalert:NO success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    [self getQQUserList];
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:self.view];
                }
            } failure:^(NSError *error) {
                
            }];
//            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
//                NSDictionary *json = responseObject;
//                ZPLog(@"%@\n%@",json,json[@"message"]);
//                BOOL success = [json[@"success"] boolValue];
//                if (success) {
//                    [self getQQUserList];
//                } else {
//                    [UIView ShowInfo:json[@"message"] Inview:self.view];
//                }
//            } failure:^(NSError *error) {
//            }];
        }];
    }
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.rowHeight = 80;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}


- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


//- (void)didMoveToParentViewController:(UIViewController*)parent{
//    [super didMoveToParentViewController:parent];
//    ZPLog(@"%s,%@",__FUNCTION__,parent);
//    if(!parent){
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceivePriVateMessage object:nil];
//        ZPLog(@"页面pop成功了");
//    }
//}

@end
