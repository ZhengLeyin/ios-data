//
//  MineJinBiViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineJinBiViewController.h"
#import "OrderModel.h"

@interface MineJinBiViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) GoldHeaderView *headerView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@end

@implementation MineJinBiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableHeaderView = self.headerView;

    [self refreshTableview];
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.arrayData.count>0) {
            [_arrayData removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self OrderRechargeUserID];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self OrderRechargeUserID];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


- (void)OrderRechargeUserID{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KVirtualMoneyDetail,[userDefault objectForKey:KUDuser_id]];
    NSDictionary *parameters = @{
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 20;
            _headerView.goldLab.text =  [NSString stringWithFormat:@"%.2f",[json[@"data"][@"title_balance"] floatValue]];
            NSArray *virtualMoneyDetailList = json[@"data"][@"virtualMoneyDetailList"];
            if (virtualMoneyDetailList && virtualMoneyDetailList.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in virtualMoneyDetailList) {
                OrderModel *model = [OrderModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
    }];
}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayData.count;
}

- ( UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 40)];
    view.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = @"交易类型";
    [view addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = @"交易金额";
    [view addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.text = @"育币余额";
    [view addSubview:label3];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).offset(15);
        make.centerY.equalTo(view);
    }];
    
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-15);
        make.centerY.equalTo(view);
    }];
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.centerX.mas_equalTo(view.centerX).multipliedBy(0.2);
    }];
    
    label1.font = label2.font = label3.font = FontSize(11);
    label1.textColor = label2.textColor = label3.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    return  view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"MineJinBiCell";
    MineJinBiCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[MineJinBiCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        OrderModel *model = [self.arrayData objectAtIndex:indexPath.row];
        cell.ordermodel = model;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}




- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (GoldHeaderView *)headerView{
    if (_headerView == nil) {
        _headerView = [[NSBundle mainBundle] loadNibNamed:@"GoldHeaderView" owner:nil options:nil][0];
        _headerView.icon_image.image = ImageName(@"ios_money");
    }
    return _headerView;
}

@end


@implementation MineJinBiCell


- (void)setOrdermodel:(OrderModel *)ordermodel{
    _ordermodel = ordermodel;
    _title_lab.text = ordermodel.tips;
    _time_lab.text = ordermodel.time_status;
    _balance_lab.text = [NSString stringWithFormat:@"%.2f",ordermodel.balance];
    if (ordermodel.virtual_money > 0) {
        _usejinbi_lab.textColor = [UIColor colorWithHexString:@"F7A429"];
        _usejinbi_lab.text = [NSString stringWithFormat:@"+%.2f",ordermodel.virtual_money];
    } else {
        _usejinbi_lab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        _usejinbi_lab.text = [NSString stringWithFormat:@"%.2f",ordermodel.virtual_money];
    }
}

@end


