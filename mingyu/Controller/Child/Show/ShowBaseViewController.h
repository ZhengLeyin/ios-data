//
//  ShowBaseViewController.h
//  TZWY
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyHeaderView.h"



UIKIT_EXTERN NSNotificationName const ChildShowScrollViewDidScrollNSNotification;
UIKIT_EXTERN NSNotificationName const ChildShowScrollViewRefreshStateNSNotification;

@interface ShowBaseViewController : UIViewController

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) MyHeaderView *headerView;
@property (nonatomic,assign) CGPoint lastContentOffset;

@property (nonatomic,assign) BOOL isFirstViewLoaded;

@property (nonatomic,assign) BOOL refreshState;


@end
