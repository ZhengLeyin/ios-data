//
//  SideBySideVideoCollectionCell.m
//  mingyu
//
//  Created by MingYu on 2018/12/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "SideBySideVideoCollectionCell.h"

@implementation SideBySideVideoCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _videoImageView.userInteractionEnabled = YES;
    _playImageView.userInteractionEnabled = YES;
    _videoImageView.layer.cornerRadius = 4;
    _videoImageView.layer.masksToBounds = YES;
}

- (void)setVideomodel:(VideoModel *)videomodel{
    _videomodel = videomodel;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    [_videoImageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _contentVideoTitleLabel.text = _videomodel.video_title;

}


@end
