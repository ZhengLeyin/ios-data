//
//  MineViewCell.h
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon_image;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *VIP_lab;


@property (weak, nonatomic) IBOutlet UIImageView *user_image;

@property (weak, nonatomic) IBOutlet UIView *haslogin_view;

@property (weak, nonatomic) IBOutlet UILabel *username_lab;

@property (weak, nonatomic) IBOutlet UILabel *introduce_lab;

@property (weak, nonatomic) IBOutlet UILabel *shoucangcount_lab;

@property (weak, nonatomic) IBOutlet UILabel *tiezicount_lab;

@property (weak, nonatomic) IBOutlet UILabel *guanzhucount_lab;

@property (weak, nonatomic) IBOutlet UILabel *fensicount_lab;


@property (nonatomic, strong) UserModel *usermodel;



+ (instancetype)theMineCellWithTableview:(UITableView *)tableview;


+ (instancetype)theMineHeaderView;



@end
