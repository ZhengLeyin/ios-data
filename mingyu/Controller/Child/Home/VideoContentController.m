//
//  VideoContentController.m
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoContentController.h"
#import "LWXTabPageController.h"
#import "VideoContentCell.h"

@interface VideoContentController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end

@implementation VideoContentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.bounces = NO;
    
}

- (NSMutableArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}



#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataArray.count > 0) {
        return self.dataArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"VideoContentCell";
    VideoContentCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[VideoContentCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (self.dataArray && self.dataArray.count > 0) {
        VideoModel *model = [_dataArray objectAtIndex:indexPath.row];
        cell.videomodel = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArray && self.dataArray.count > 0) {
        VideoModel *model = [_dataArray objectAtIndex:indexPath.row];
        if (self.contentblock) {
            self.contentblock(model);
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 79;
}


//- (UIScrollView *)scrollView{
//    return self.tableView;
//}





@end
