//
//  ParentHomeHeaderView.h
//  mingyu
//
//  Created by apple on 2018/4/28.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDCycleScrollView.h>
#import "TZImagePickerController.h"
#import <Photos/Photos.h>



@interface ParentHomeHeaderView : UIView<SDCycleScrollViewDelegate,TZImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;


@property (nonatomic, strong) NSMutableArray *BabyRecordArray;


@property (weak, nonatomic) IBOutlet UIView *bannerView;


@property (weak, nonatomic) IBOutlet UIView *recordView;


@property (weak, nonatomic) IBOutlet UILabel *dayNum_lab;

@property (weak, nonatomic) IBOutlet UILabel *date_lab;

@property (weak, nonatomic) IBOutlet UILabel *babyLength_lab;

@property (weak, nonatomic) IBOutlet UILabel *babyWeight_lab;

@property (weak, nonatomic) IBOutlet UILabel *content_lab;

@property (weak, nonatomic) IBOutlet UIButton *babyicon_button;




@end
