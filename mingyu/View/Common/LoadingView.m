//
//  LoadingView.m
//  mingyu
//
//  Created by apple on 2018/7/14.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView



- (instancetype)initWithFrame:(CGRect)frame inView:(UIView *)view{
    self = [super initWithFrame:frame];
    if (self) {
        [self updatUIinView:view];
    }
    return self;
}


- (void)updatUIinView:(UIView *)view{
    [view addSubview:self];
    self.backgroundColor = [UIColor whiteColor];
}



- (void)dealloc{
    [SVProgressHUD dismiss];
}

- (void)show{
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [SVProgressHUD show];
    } completion:nil];
}

- (void)hidden{
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [SVProgressHUD dismiss];
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
    }];
}


@end
