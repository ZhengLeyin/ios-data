//
//  HomePageAgeRelatedOfHeaderView.h
//  mingyu
//
//  Created by MingYu on 2018/12/18.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomePageAgeRelatedOfHeaderView : UIView<SDCycleScrollViewDelegate,TZImagePickerControllerDelegate,UINavigationControllerDelegate>


#pragma mark 两岁前
+ (instancetype)TheBeforeAgeOfTwoOfHomePageAgeRelatedHeaderView;

@end

NS_ASSUME_NONNULL_END
