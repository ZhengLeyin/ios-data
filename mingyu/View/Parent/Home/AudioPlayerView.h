//
//  AudioPlayerView.h
//  mingyu
//
//  Created by apple on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DFPlayer.h"
@class AlbumListModel;

NS_ASSUME_NONNULL_BEGIN

@interface AudioPlayerView : UIView<DFPlayerDelegate,DFPlayerDataSource,UIAlertViewDelegate>

@property (nonatomic, assign) BOOL isDownloadPaly;

@property (nonatomic, copy) void (^ChangeAudio) (void);



- (void)initDFPlayer;

@end

NS_ASSUME_NONNULL_END
