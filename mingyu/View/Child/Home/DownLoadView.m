//
//  DownLoadView.m
//  mingyu
//
//  Created by apple on 2018/4/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "DownLoadView.h"
#import "VideoListCell.h"
#import "YCDownloadManager.h"
#import "downloadInfo.h"

@implementation DownLoadView



- (void)setArraydata:(NSMutableArray *)arraydata{
    _arraydata = arraydata;
    _DownTaleview.delegate = self;
    _DownTaleview.dataSource = self;
    [_DownTaleview reloadData];
}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arraydata.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"VideoListCell";
    VideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[VideoListCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    
    VideoModel *model = [_arraydata objectAtIndex:indexPath.row];
    cell.videotitleLib.text = model.video_title;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [cell setDownloadStatus:[YCDownloadManager downloasStatusWithId:model.video_path]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VideoModel *model = [_arraydata objectAtIndex:indexPath.row];
    
    downloadInfo *info = [[downloadInfo alloc] initWithUrl:model.video_path fileId:model.video_title];
    info.thumbImageUrl = model.video_img;
    info.fileName = model.video_title;
    info.date = [NSDate date];
    info.totletime = model.time_length;
    info.enableSpeed = true;
    [YCDownloadManager startDownloadWithItem:info priority:0.8];
    
    self.DownIngLab.text = [NSString stringWithFormat:@" %lu ",(unsigned long)[YCDownloadManager downloadList].count];

    [self.DownTaleview reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}


- (IBAction)ClooseButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(Cloose)]) {
        [self.delegate Cloose];
    }
}



- (IBAction)DownAllButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(DownAll)]) {
        [self.delegate DownAll];
    }
}

- (IBAction)DownIngButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(DownIng)]) {
        [self.delegate DownIng];
    }
}

@end
