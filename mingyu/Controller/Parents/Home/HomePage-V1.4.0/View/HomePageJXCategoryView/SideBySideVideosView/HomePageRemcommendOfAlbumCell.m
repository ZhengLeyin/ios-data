//
//  HomePageRemcommendOfAlbumCell.m
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageRemcommendOfAlbumCell.h"

@implementation HomePageRemcommendOfAlbumCell

+ (instancetype)theHomePageRemcommendOfAlbumCell:(UITableView *)tableView {
    static NSString *cellId = @"FirstCell";
    HomePageRemcommendOfAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HomePageRemcommendOfAlbumCell" owner:nil options:nil][0];
    }
    return cell;
}

-(void)setSubjectModel:(SubjectModel *)subjectModel {
   
    _subjectModel = subjectModel;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_subjectModel.subject_image];
    [_albumImageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    _albumNameLabel.text = subjectModel.subject_title;
    _numberOfListenersLabel.text = [NSString stringWithFormat:@"%ld人收听",_subjectModel.play_count];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _albumNameLabel.layer.cornerRadius = 2;
    _albumImageView.layer.masksToBounds = YES;
    _albumView.layer.cornerRadius = 2;
    _albumView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
   // MingYuLoggerD(<#...#>)
      _albumView.userInteractionEnabled = YES;
     _albumImageView.userInteractionEnabled = YES;
}

@end
