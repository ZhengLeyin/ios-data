//
//  CommentReplayModel.h
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentReplayModel : NSObject

/** 回复主键 */
@property (nonatomic, assign) NSInteger replay_id;

/** 评论ID */
@property (nonatomic, assign) NSInteger fk_comment_id;

/** 用户ID */
@property (nonatomic, assign) NSInteger replay_user_id;

/** 回复用户ID，0代表回复作者 */
@property (nonatomic, assign) NSInteger fk_to_user_id;

/** 回复用户名称 */
@property (nonatomic, copy) NSString *to_user_name;

/** 回复内容 */
@property (nonatomic, copy) NSString *replay_content;

/** 用户名字 */
@property (nonatomic, copy) NSString *replay_name;

/** 用户头像 */
@property (nonatomic, copy) NSString *replay_head;

/** 点赞数 */
@property (nonatomic, assign) NSInteger praise_number;

/** 是否点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 回复评论时间状态 */
@property (nonatomic, copy) NSString *replay_time_status;



@end
