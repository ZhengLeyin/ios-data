//
//  SearchSuggestionViewController.h
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SuggestSelectBlock)(NSString *searchTest);

@interface SearchSuggestionViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *searchArray;
@property (nonatomic, assign) BOOL lastPage;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, copy) SuggestSelectBlock searchBlock;

- (void)searchTestChangeWithTest:(NSString *)test;

@end
