//
//  ParentVideoDetailBottomView.h
//  mingyu
//
//  Created by apple on 2018/7/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentVideoDetailBottomView : UIView

@property (weak, nonatomic) IBOutlet UIButton *pinlun_button;

@property (weak, nonatomic) IBOutlet UIButton *dianzan_button;

@property (weak, nonatomic) IBOutlet UIButton *shoucang_button;

@property (nonatomic, copy) void (^showDetailBlock)(void);


@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *describe_lab;

@property (weak, nonatomic) IBOutlet UILabel *play_count_lab;

@property (weak, nonatomic) IBOutlet UILabel *comment_count_lab;

@property (weak, nonatomic) IBOutlet UIButton *collect_button;

@property (weak, nonatomic) IBOutlet UIButton *praise_button;

@property (nonatomic, copy) void (^clooseViewBlock)(void);





@end
