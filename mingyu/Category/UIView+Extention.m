//
//  UIImageView+AFNetworking.h
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "UIView+Extention.h"


@implementation UIView (Extention)


+ (void)ShowInfo:(NSString *)info Inview:(UIView *)view{
    
    __block MBProgressHUD *hud =[[MBProgressHUD alloc] initWithView:view];
    [UIView animateWithDuration:0.0f animations:^{
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.labelText = info;
        hud.mode = MBProgressHUDModeText;
        hud.color = [UIColor colorWithHexString:@"2C2C2C" alpha:0.8];
//        [UIColor colorWithHexString:@"#4A4646"];
        hud.labelFont = [UIFont systemFontOfSize:13];
        if (iOS9Later) {
            hud.yOffset = -100;
        }
//        hud.alpha = 0.5;
        hud.margin = 30;
    } completion:^(BOOL finished) {
        [hud hide:YES afterDelay:1.0f];
    }];

}

+ (void)ShowInfo:(NSString *)info Inview:(UIView *)view Time:(NSInteger )time{
    
    __block MBProgressHUD *hud =[[MBProgressHUD alloc] initWithView:view];
    [UIView animateWithDuration:0.0f animations:^{
        hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.labelText = info;
        hud.mode = MBProgressHUDModeText;
        hud.color = [UIColor colorWithHexString:@"#000000"];
        hud.labelFont = [UIFont systemFontOfSize:13];
        hud.yOffset = 20;
        hud.alpha = 0.5;

    } completion:^(BOOL finished) {
        [hud hide:YES afterDelay:time];
    }];
    
}



@end
