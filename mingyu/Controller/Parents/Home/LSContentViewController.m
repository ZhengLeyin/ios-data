//
//  LSContentViewController.m
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LSContentViewController.h"
#import "LSContentCollectionViewCell.h"

@interface LSContentViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);



@end

@implementation LSContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (_type == 0) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 50);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);;
        }
    }];
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 85;
        
    }
    return _tableView;
}


- (void)setupView{

    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 85;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (_type == 0) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 50);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);
        }
    }];
}




- (void)setAudioListArray:(NSMutableArray *)audioListArray{
    _audioListArray = audioListArray;

    [self performSelector:@selector(tableViewReloadData) withObject:nil afterDelay:0.5];

}



- (void)tableViewReloadData{

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];

        NSInteger audio_type =  [DFPlayer shareInstance].currentAudioModel.audio_type;
        NSInteger fk_parent_id =  [DFPlayer shareInstance].currentAudioModel.fk_parent_id;
        if (!audio_type) {
            audio_type = [DFPlayer shareInstance].previousAudioModel.audio_type;
            fk_parent_id  =  [DFPlayer shareInstance].currentAudioModel.fk_parent_id;
        }
        
        if (audio_type == 2 && _audioListArray && _audioListArray.count > 0) {
            AudioModel *model = _audioListArray[0];
            if (model.fk_parent_id == fk_parent_id) {
                NSInteger sourt = [DFPlayer shareInstance].currentAudioModel.audio_sort;
                NSIndexPath * index = [NSIndexPath indexPathForRow:sourt inSection:0];
                [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
                ZPLog(@"=======");
            }
        }
    });
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_audioListArray && _audioListArray.count > 0) {
        return _audioListArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BookAudioCell *cell = [BookAudioCell theCellWithTableView:tableView];
    if (_audioListArray.count > indexPath.row) {
        AudioModel *model = [_audioListArray objectAtIndex:indexPath.row];
        cell.totleNum = self.audioListArray.count;
        cell.audiomodel = model;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_audioListArray.count > indexPath.row) {
        if (self.tableviewSelectIndex) {
            self.tableviewSelectIndex(indexPath.row);
        }
    }
    
    [self.tableView reloadData];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}


@end
