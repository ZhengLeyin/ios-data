//
//  TagView.m
//  CustomTag
//
//  Created by za4tech on 2017/12/15.
//  Copyright © 2017年 Junior. All rights reserved.
//

#import "TagView.h"
#import "ParentCircleModel.h"

@implementation TagView

//- (instancetype)initWithFrame:(CGRect)frame{
//    if (self) {
//        [self setupUI];
//    }
//    return self;
//}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    
    UILabel *lab = [[UILabel alloc] init];
    lab.text = @"更改发布的圈子：";
    lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    lab.font = FontSize(12);
    [self addSubview:lab];
    
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(50);
        make.top.mas_offset(25);
    }];
}

-(void)setArr:(NSArray *)arr{
    _arr = arr;
    CGFloat marginX = 15;
    CGFloat marginY = 15;
    CGFloat height = 30;
    CGFloat FirstBtnX = 50;
    CGFloat FirstBtnY = 55;
    UIButton * markBtn;
    for (int i = 0; i < _arr.count; i++) {
        ParentCircleModel *model = _arr[i];
        CGFloat width =  [self calculateString:model.circle_name Width:12] +20;
        UIButton * tagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tagBtn.tag = model.circle_id+2000;
        if (!markBtn) {
            tagBtn.frame = CGRectMake(FirstBtnX, FirstBtnY, width, height);
        }else{
            if (markBtn.frame.origin.x + markBtn.frame.size.width + FirstBtnX + width + marginX > kScreenWidth) {
                tagBtn.frame = CGRectMake(FirstBtnX, markBtn.frame.origin.y + markBtn.frame.size.height + marginY, width, height);
            }else{
                tagBtn.frame = CGRectMake(markBtn.frame.origin.x + markBtn.frame.size.width + marginX, markBtn.frame.origin.y, width, height);
            }
        }
        [tagBtn setTitle:model.circle_name forState:UIControlStateNormal];
        tagBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [tagBtn setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];

        [self makeCornerRadius:6 borderColor:[UIColor colorWithHexString:@"A8A8A8"] layer:tagBtn.layer borderWidth:.5];
        markBtn = tagBtn;
        
        [tagBtn addTarget:self action:@selector(clickTo:) forControlEvents:UIControlEventTouchUpInside];

        [self addSubview:markBtn];
    }
    CGRect rect = self.frame;
    rect.size.height = markBtn.frame.origin.y + markBtn.frame.size.height + marginY;
    self.frame = rect;
}


-(void)clickTo:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleSelectModel:)]) {
        for (int i = 0; i < _arr.count; i++) {
            ParentCircleModel *model = _arr[i];

            if (sender.tag == model.circle_id + 2000) {
                [sender setBackgroundColor:[UIColor colorWithHexString:@"FF96A4 "]];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self makeCornerRadius:6 borderColor:[UIColor colorWithHexString:@"FF96A4"] layer:sender.layer borderWidth:.5];

                [self.delegate handleSelectModel:model];

                continue;
            }
            UIButton *button = [(UIButton *)self viewWithTag:(2000+model.circle_id)];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
            [self makeCornerRadius:6 borderColor:[UIColor colorWithHexString:@"A8A8A8"] layer:button.layer borderWidth:.5];

        }
    }
}



-(void)makeCornerRadius:(CGFloat)radius borderColor:(UIColor *)borderColor layer:(CALayer *)layer borderWidth:(CGFloat)borderWidth {
    layer.cornerRadius = radius;
    layer.masksToBounds = YES;
    layer.borderColor = borderColor.CGColor;
    layer.borderWidth = borderWidth;
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    
    return size.width;
}

@end
