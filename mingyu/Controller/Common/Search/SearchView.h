//
//  SearchView.h
//  TZWY
//
//  Created by apple on 2018/3/21.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TapActionBlick)(NSString *str);

@interface SearchView : UIView

@property (nonatomic,copy) TapActionBlick tabAction;


- (instancetype)initWithFrame:(CGRect)frame hotArray:(NSMutableArray *)hotArr historyArray:(NSMutableArray *)historyArr;



@end
