//
//  DownloadViewController.m
//  mingyu
//
//  Created by apple on 2018/4/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "DownloadViewController.h"
#import "DownloadCell.h"
#import "downloadInfo.h"
#import "YCDownloadManager.h"

@interface DownloadViewController ()<DFPlayerDelegate,DFPlayerDataSource,EditorBottomViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *editorButton;

@property (nonatomic, strong) EditorBottomView *bottomView;
@property (nonatomic, strong) NothingView *nothingView;


@property (nonatomic, strong) NSMutableArray *allCacheList;
@property (nonatomic, strong) NSMutableArray *finishList;
@property (nonatomic, strong) NSMutableArray *df_ModelArray;

@property (assign, nonatomic) NSInteger deleteNum;
@property (nonatomic,strong)NSMutableArray *selectorArray;//存放选中数据
@property (nonatomic, strong) dispatch_semaphore_t semaphore;

@end

@implementation DownloadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    初始化播放器
//    [[DFPlayer shareInstance] df_initPlayerWithUserId:nil];
//    [DFPlayer shareInstance].isNeedCache = NO;
//    [DFPlayer shareInstance].category = DFPlayerAudioSessionCategoryPlayback;
//    if ([DFPlayer shareInstance].state != DFPlayerStatePlaying) {
//        [[DFPlayer shareInstance] df_audioPause];
//    }
    
    [self.view addSubview:self.bottomView];
    self.bottomView.hidden = YES;
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.mas_offset(IS_iPhoneXStyle?-34:0);
        make.height.mas_offset(50);
    }];

    //    初始化下载
    [YCDownloadManager setMaxTaskCount:1];
    [YCDownloadManager localPushOn:false];
    [YCDownloadManager resumeAllDownloadTask];
//    [YCDownloadManager allowsCellularAccess:YES];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getCacheVideoList];
    [DFPlayer shareInstance].dataSource  = self;
    [DFPlayer shareInstance].delegate    = self;
}


- (void)getCacheVideoList {
    [self.allCacheList removeAllObjects];
    [self.finishList removeAllObjects];
    [self.allCacheList addObjectsFromArray:[YCDownloadManager downloadList]];
    [self.allCacheList addObjectsFromArray:[YCDownloadManager finishList]];
//    [self.allCacheList enumerateObjectsUsingBlock:^(downloadInfo*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (obj.downloadStatus == YCDownloadStatusFailed) {
//            [YCDownloadManager stopDownloadWithItem:obj];
//            [self.allCacheList removeObject: obj];
//        }
//    }];
    [self.finishList addObjectsFromArray:[YCDownloadManager finishList]];
    if (self.allCacheList.count && self.allCacheList.count > 0){
        [self.tableView reloadData];
        self.nothingView.hidden = YES;
        self.editorButton.hidden = NO;
    } else {
        self.nothingView.hidden = NO;
        self.editorButton.hidden = YES;
    }
}


- (void)archiveObject:(NSMutableArray *)dataArray{
    //创建一个归档文件夹
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *string = @"downloadAudio";
    NSString *entityArchive = [strLib stringByAppendingPathComponent:string];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    NSMutableArray *savearray = [NSMutableArray array];
    for (int i = 0; i < dataArray.count; i++) {
        downloadInfo *infomodel = dataArray[i];
        AudioModel *model = infomodel.audiomodel;
        for (int j = 0; j < array.count; j++) {
            AudioModel *model2 = array[j];
            if (model.audio_id == model2.audio_id) {
                model.timeWithValue = model2.timeWithValue;
            }
        }
        [savearray addObject:model];
//        self.finishList = savearray;
    }
    if ( [NSKeyedArchiver archiveRootObject:savearray toFile:entityArchive]) {
        ZPLog(@"success");
    }
    [[DFPlayer shareInstance] df_reloadData];//须在传入数据源后调用（类似UITableView的reloadData）
}

#pragma mark - DFPLayer dataSource
- (NSArray<AudioModel *> *)df_playerModelArray{
    if (_df_ModelArray.count == 0) {
        _df_ModelArray = [NSMutableArray array];
    }else{
        [_df_ModelArray removeAllObjects];
    }
    for (int i = 0; i < self.finishList.count; i++) {
        downloadInfo *infomodel = self.finishList[i];
        AudioModel *yourModel    = infomodel.audiomodel;
        AudioModel *model        = [[AudioModel alloc] init];
        model = yourModel;
        model.audioId               = i;//****重要。AudioId从0开始，仅标识当前音频在数组中的位置。
        NSArray *array = [YCDownloadManager finishList];
        for (downloadInfo *item in array) {
            if (item.audiomodel.audio_id == model.audio_id) {
                model.audio_path = [NSURL fileURLWithPath:item.savePath];
            }
        }
        [_df_ModelArray addObject:model];
    }
    if (_semaphore) {
        dispatch_semaphore_signal(_semaphore);
    }
    return self.df_ModelArray;
}

- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self.tableView reloadData];
    
//    NSString *audioName = player.currentAudioModel.audio_title;
//    self.navigationItem.title = [NSString stringWithFormat:@"%@",audioName];
    
//    self.headerView.album_play_lab.text = player.currentAudioModel.audio_title;
//    [self.playbutton sd_setImageWithURL:player.currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    
//    if (_target_id == player.currentAudioModel.subject_id) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:player.currentAudioModel.audioId inSection:0];
//        DownloadCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//        cell.titleLab.textColor = [UIColor colorWithHexString:@"00D3F8"];
//    }
}


#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allCacheList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"DownloadCell";
    DownloadCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    cell.backgroundColor = [UIColor whiteColor];
    if (!cell) {
        cell = [[DownloadCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    downloadInfo *item = self.allCacheList[indexPath.row];
    cell.downloadinfo = item;
    item.delegate = cell;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing == YES) {
        NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                }
            }
        }
        [self.selectorArray addObject:[self.allCacheList objectAtIndex:indexPath.row]];
        self.deleteNum += 1;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%ld项",(long)self.deleteNum];
    } else {
        YCDownloadItem *item = self.allCacheList[indexPath.row];
        if (item.downloadStatus == YCDownloadStatusDownloading) {
//            [YCDownloadManager pauseDownloadWithItem:item];
        }else if (item.downloadStatus == YCDownloadStatusPaused){
            [YCDownloadManager resumeDownloadWithItem:item];
        }else if (item.downloadStatus == YCDownloadStatusFailed){
            [YCDownloadManager resumeDownloadWithItem:item];
        }else if (item.downloadStatus == YCDownloadStatusWaiting){
//            [YCDownloadManager pauseDownloadWithItem:item];
        }else if (item.downloadStatus == YCDownloadStatusFinished){
            _semaphore = dispatch_semaphore_create(0);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self archiveObject:self.finishList];    // 归档
            });
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
                dispatch_async(dispatch_get_main_queue(), ^{
                    downloadInfo *item = self.allCacheList[indexPath.row];
                    if (item.downloadType == 1) {
                        AudioModel *model = item.audiomodel;
                        [[DFPlayer shareInstance] df_playerPlayWithAudioId:model.audioId];
                        AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
                        PVC.isDownloadPaly = YES;
                        UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
                        [self.navigationController presentViewController:VC animated:YES completion:nil];
                    }
                });
            });
        }
        [self.tableView reloadData];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing == YES) {
        NSArray *subviews = [[tableView cellForRowAtIndexPath:indexPath] subviews];
        for (id subCell in subviews) {
            if ([subCell isKindOfClass:[UIControl class]]) {
                for (UIImageView *circleImage in [subCell subviews]) {
                    circleImage.image = [UIImage imageNamed:@"weixuanz"];
                }
            }
        }
        [self.selectorArray removeObject:[self.allCacheList objectAtIndex:indexPath.row]];
        self.deleteNum -= 1;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%ld项",(long)self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    } else {
        
    }
}


- (NSMutableArray *)allCacheList{
    if (!_allCacheList) {
        _allCacheList = [NSMutableArray array];
    }
    return _allCacheList;
}


- (NSMutableArray *)finishList{
    if (!_finishList) {
        _finishList = [NSMutableArray array];
    }
    return _finishList;
}




- (IBAction)EditorButton:(id)sender {
    [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
    [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    _selectorArray = [NSMutableArray array];
    self.deleteNum = 0;
    _bottomView.chooseCountLab.text = @"已选0项";
    if (_bottomView.hidden == NO) {
        [_editorButton setTitle:@"编辑" forState:0];
        _tableView.editing = NO;
        _bottomView.hidden = YES;
    } else {
        [_editorButton setTitle:@"取消" forState:0];
        _tableView.editing = YES;
        _bottomView.hidden = NO;
    }
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (EditorBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"EditorBottomView" owner:nil options:nil][0];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

#pragma -mark- EditorBottomViewDelegate
- (void)chooseAllButonClicked{
    if ([_bottomView.chooseAllButton.titleLabel.text  isEqual: @"  全选"]) {
        for (int i = 0; i < self.allCacheList.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"xuanzhe"];
                    }
                }
            }
        }
        self.selectorArray = [NSMutableArray arrayWithArray:self.allCacheList];
        self.deleteNum  = self.allCacheList.count;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%ld项",(long)self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选 " forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"xuanzhe") forState:0];
    } else {
        for (int i = 0; i< self.allCacheList.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tableView deselectRowAtIndexPath:indexPath animated:NO];
            NSArray *subviews = [[_tableView cellForRowAtIndexPath:indexPath] subviews];
            for (id subCell in subviews) {
                if ([subCell isKindOfClass:[UIControl class]]) {
                    for (UIImageView *circleImage in [subCell subviews]) {
                        circleImage.image = [UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
        [self.selectorArray removeAllObjects];
        self.deleteNum  = 0;
        _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%ld项",(long)self.deleteNum];
        [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
        [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
    }
}

- (void)deleteButtonClicked{
    if (self.tableView.editing && self.deleteNum > 0) {
        [self showAlertWithTitle:@"是否确定删除已选音频" message:nil noBlock:nil yseBlock:^{
            for (YCDownloadItem *item in _selectorArray) {
                [YCDownloadManager stopDownloadWithItem:item];
                [self.allCacheList removeObject:item];
                
                [_bottomView.chooseAllButton setTitle:@"  全选" forState:0];
                [_bottomView.chooseAllButton setImage:ImageName(@"weixuanz") forState:0];
                _selectorArray = [NSMutableArray array];
                self.deleteNum = 0;
                _bottomView.chooseCountLab.text = [NSString stringWithFormat:@"已选%ld项",(long)self.deleteNum];
            }
            [self getCacheVideoList];
        }];
    }
}


- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无下载内容";
        _nothingView.imageView.image = ImageName(@"无下载");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}


@end
