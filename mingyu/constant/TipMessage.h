//
//  TipMessage.h
//  mingyu
//
//  Created by apple on 2018/9/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipMessage : NSObject

//异常提示
#define TipWrongMessage    @"服务器异常"

/** 收藏成功 */
#define TipCollectSuccess        @"收藏成功"

/** 已取消收藏 */
#define TipUnCollectSuccess     @"已取消收藏"

/** 点赞成功 */
#define  TipPraiseSuccess        @"点赞成功"

/** 你已经点过赞咯~ */
#define TipDoNotPraise          @"你已经点过赞咯~"

/** 写评论…*/
#define TipWriteComment            @"写评论…"

/** 关注成功 */
#define TipAttentionSuccess     @"关注成功"

/** 已取消关注 */
#define TipUnAttentin        @"已取消关注"

//** 加入圈子成功 */
#define TipAddCircleSuccess  @"加入成功"

//** 加入圈子成功 */
#define TipDelCircleSuccess  @"已退出圈子"

//** 网络异常 */
#define TipNetWorkIsFiled  @"网络不给力，\n请检查网络连接～"

/** 修改生日 */
#define TipChagneBirthday @"生日修改成功～"

/** 修改地区 */
#define TipChangePlace  @"地区修改成功～"

/**特权课领取失败*/
#define TipCourseCollectionFailedMessage    @"课程领取失败,请稍后重试"
//#define
//#define
//#define
//#define
//#define
//#define




@end
