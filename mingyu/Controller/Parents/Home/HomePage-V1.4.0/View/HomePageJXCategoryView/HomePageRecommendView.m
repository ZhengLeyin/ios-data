//
//  HomePageRecommendView.m
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageRecommendView.h"
#import "ParentVideoDetailCell.h"
#import "HomePageRemcommendOfAlbumCell.h"
#import "SideBySideVideosCell.h"
#import "FooterBottomLineView.h"
#import "SideBySideVideosCell.h"
@interface HomePageRecommendView ()
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) NSMutableArray *subjectArray;  //一键听
@property (nonatomic, strong) NSMutableArray *chargeCourseArray;  //精品课
@property (nonatomic, strong) NSMutableArray *freeCourseArray;  //免费精品课
@property (nonatomic, strong) NSMutableArray *videoArray;  //精选视频
@property (nonatomic, strong) NSMutableArray *childcareArray;  //育儿必读
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, strong) NSArray *DataArray;

@property (nonatomic, strong) dispatch_group_t  customGroup;

@end

@implementation HomePageRecommendView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
       [self createUI];
    }
    return self;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH -40);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

-(void)createUI {
    self.backgroundColor = [UIColor yellowColor];
    [self addSubview:self.tableView];
//    [self refreshTableview];
    
    
    dispatch_queue_t customQuue = dispatch_queue_create("com.manman.network", DISPATCH_QUEUE_CONCURRENT);
    _customGroup = dispatch_group_create();
    dispatch_group_async(_customGroup, customQuue, ^{
        //一键听
        [self getHomePageSubject];
    });
    
    dispatch_group_async(_customGroup, customQuue, ^{
        //课程
        [self getHomePageCourses];
    });
    
    dispatch_group_async(_customGroup, customQuue, ^{
        //精选视频
        [self getHomeVideoList];
    });
    dispatch_group_async(_customGroup, customQuue, ^{
        //育儿必读
        [self getHomeChildcare];
    });
    
    dispatch_group_notify(_customGroup, dispatch_get_main_queue(), ^{
        self.DataArray = @[self.subjectArray,self.chargeCourseArray,self.videoArray,self.freeCourseArray,self.childcareArray];
        [self.tableView reloadData];
    });
}


//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //精选视频
            [self getHomeVideoList];
            //育儿必读
            [self getHomeChildcare];
            
            [_tableView.mj_header endRefreshing];
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
}

//一键听
- (void)getHomePageSubject {
    dispatch_group_enter(_customGroup);
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KHomePageSubject];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        dispatch_group_leave(_customGroup);
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                SubjectModel *model = [SubjectModel modelWithJSON:dic];
                [self.subjectArray addObject:model];
            }
        }
    } failure:^(NSError *error) {
        dispatch_group_leave(_customGroup);
    }];
}

//课程
- (void)getHomePageCourses {
    dispatch_group_enter(_customGroup);
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KHomePageCourses];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        dispatch_group_leave(_customGroup);
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                CourseModel *model = [CourseModel modelWithJSON:dic];
                if (model.present_price > 0) {
                    [self.chargeCourseArray addObject:model];
                } else {
                    [self.freeCourseArray addObject:model];
                }
            }
        }
    } failure:^(NSError *error) {
        dispatch_group_leave(_customGroup);
    }];
}


//育儿必读
- (void)getHomeChildcare{
    dispatch_group_enter(_customGroup);
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL, KGetHomeChildcare];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        dispatch_group_leave(_customGroup);
        NSDictionary *json = responseObject;
        [self.childcareArray removeAllObjects];
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                if (model.news_type == 2) {
                    [self.childcareArray addObject:model];
                }
            }
        }
    } failure:^(NSError *error) {
        dispatch_group_leave(_customGroup);
    }];
}

//精选视频
- (void)getHomeVideoList{
    dispatch_group_enter(_customGroup);
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetHomeVideoList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"4"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        dispatch_group_leave(_customGroup);
        NSDictionary *json = responseObject;
        [self.videoArray removeAllObjects];
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            _start += 4;
            [self.videoArray removeAllObjects];
            for (NSDictionary *dic in data) {
                VideoModel *videomodel = [VideoModel modelWithJSON:dic];
                [self.videoArray addObject:videomodel];
            }
        }
    } failure:^(NSError *error) {
        dispatch_group_leave(_customGroup);
    }];
}


#pragma tableView--delegate Section:5
/*1.一键听 (只有收听才会展示)
 *2.精品课 收费模块
 *3.视频
 *4.精品课 限时免费
 *5.育儿必读
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.DataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.DataArray[section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    __block typeof (self) WeakSelf = self;
    ParentHomeCell *header =  [ParentHomeCell TheHeaderView];
    header.backgroundColor = [UIColor whiteColor];
    if (section == 0){
        header.header_title_lab.text = @"一键听";
        header.MoreButtonBlock = ^{
            [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
        };
    }else if (section == 1){
        header.header_title_lab.text = @"精品课";
        header.MoreButtonBlock = ^{
            ParentCourseViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentCourseViewController"];
            [[WeakSelf viewController].navigationController pushViewController:VC animated:YES];
        };
    } else if (section == 2 ){
        header.header_title_lab.text = @"精选视频";
        header.MoreButtonBlock = ^{
            [self viewController].tabBarController.selectedIndex = 1;
        };
    } else if (section == 3){
        header.header_title_lab.text = @"限时免费";
        header.more_Btn.hidden = YES;

    }  else if (section == 4) {
        header.header_title_lab.text = @"育儿必读";
        header.MoreButtonBlock = ^{
            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
            VC.title = @"育儿必读";
            [[WeakSelf viewController].navigationController pushViewController:VC animated:YES];
        };
    }
    return header;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([self.DataArray[section] count] > 0) {
        return 70;
    }
    return 1;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == self.DataArray.count-1) {
        FooterBottomLineView *view = [FooterBottomLineView TheFooterBottomLineView];
        return view;
    }
//    else if (section == 2){
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.frame = CGRectMake(0, 0, kScreenWidth, 40);
//        [button setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:0];
//        [button setImage:ImageName(@"首页_刷新") forState:0];
//        [button setTitle:@" 换一组 " forState:0];
//        button.titleLabel.font = FontSize(12);
//        [button addTarget:self action:@selector(refreshVideo) forControlEvents:UIControlEventTouchUpInside];
//        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -button.imageView.size.width, 0, button.imageView.size.width)];
//        [button setImageEdgeInsets:UIEdgeInsetsMake(0, button.titleLabel.bounds.size.width, 0, -button.titleLabel.bounds.size.width)];
//        return button;
//    }
    UIView *view = [UIView new];
    return view;

}

- (void)refreshVideo {
    ZPLog(@"refreshVideo");
    [self getHomeVideoList];
    [self.tableView reloadData];
    [_tableView scrollToRow:0 inSection:2 atScrollPosition:UITableViewScrollPositionTop animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.DataArray.count-1) {
        return 57;
    }
//    else if (section == 2){
//        return 57;
//    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        HomePageRemcommendOfAlbumCell *cell = [HomePageRemcommendOfAlbumCell theHomePageRemcommendOfAlbumCell:tableView];
        if (self.subjectArray && self.subjectArray.count > indexPath.row) {
            cell.subjectModel = self.subjectArray[indexPath.row];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.section == 1) {
        ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
        if (self.chargeCourseArray && self.chargeCourseArray.count > indexPath.row) {
            CourseModel *model = self.chargeCourseArray[indexPath.row];
            cell.coursemodel = model;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;

    } else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
            cell.homePageVCOrOtherVCType = 1;
            if (self.videoArray && self.videoArray.count > indexPath.row) {
                cell.videomodel = [self.videoArray objectAtIndex:indexPath.row];
                cell.play_image.hidden = NO;
            }
            return cell;
        } else {
            ParentVideoDetailCell *cell = [ParentVideoDetailCell theParentVideoDetailCellWithTableView:tableView];
            if (self.videoArray && self.videoArray.count > indexPath.row) {
                cell.model = [self.videoArray objectAtIndex:indexPath.row];
            }
            return cell;
        }
    }else if (indexPath.section == 3) {
        ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
        if (self.freeCourseArray && self.freeCourseArray.count > indexPath.row) {
            CourseModel *model = self.freeCourseArray[indexPath.row];
            cell.coursemodel = model;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    } else {
       ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
       if (self.childcareArray && self.childcareArray.count > indexPath.row) {
             cell.newsmodel = [self.childcareArray objectAtIndex:indexPath.row];
         }
       return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 130;
    } else if (indexPath.section == 1 ){
         return 155;
    } else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            return 230;
        } else {
            return 105;
        }
    }  else if (indexPath.section == 3){
        return 155;
    } else {
        return 130;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.subjectArray && self.subjectArray.count > indexPath.row) {
            SubjectModel *model = [self.subjectArray objectAtIndex:indexPath.row];
            AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
            VC.target_id = model.subject_id;
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }
    } else if (indexPath.section == 1) {
        if (self.chargeCourseArray && self.chargeCourseArray.count > indexPath.row) {
            CourseModel *model = self.chargeCourseArray[indexPath.row];
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [[self viewController].navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [[self viewController].navigationController pushViewController:VC animated:YES];
            }
        }
    } else if (indexPath.section == 2) {
        if (self.videoArray && self.videoArray.count > indexPath.row) {
            VideoModel *model = self.videoArray[indexPath.row];
            ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
            VC.video_id = model.video_id;
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }
    } else if (indexPath.section == 3) {
        if (self.chargeCourseArray && self.chargeCourseArray.count > indexPath.row) {
            CourseModel *model = self.chargeCourseArray[indexPath.row];
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [[self viewController].navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [[self viewController].navigationController pushViewController:VC animated:YES];
            }
        }
    } else if (indexPath.section == 4) {
        if (self.childcareArray && self.childcareArray.count > indexPath.row) {
            NewsModel *model = [self.childcareArray objectAtIndex:indexPath.row];
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = model;
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }
    }
}


- (NSArray *)DataArray {
    if (!_DataArray) {
        _DataArray = [NSMutableArray array];
    }
    return _DataArray;
}

- (NSMutableArray *)chargeCourseArray {
    if (!_chargeCourseArray) {
        _chargeCourseArray = [NSMutableArray array];
    }
    return _chargeCourseArray;
}

- (NSMutableArray *)freeCourseArray {
    if (!_freeCourseArray) {
        _freeCourseArray = [NSMutableArray array];
    }
    return _freeCourseArray;
}

- (NSMutableArray *)subjectArray {
    if (!_subjectArray) {
        _subjectArray = [NSMutableArray array];
    }
    return _subjectArray;
}

- (NSMutableArray *)childcareArray {
    if (!_childcareArray) {
        _childcareArray = [NSMutableArray array];
    }
    return _childcareArray;
}

- (NSMutableArray *)videoArray {
    if (!_videoArray) {
        _videoArray = [NSMutableArray array];
    }
    return _videoArray;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

@end
