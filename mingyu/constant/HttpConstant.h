//
//  TipMessage.h
//  mingyu
//
//  Created by apple on 2018/4/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HttpConstant : NSObject

//---------------林德明本地测试地址--------------//
#define KURL          @"http://192.168.0.110:8080/"

//---------------王学纯本地测试地址--------------//
//#define KURL            @"http://192.168.0.106:8080/"


//-----------------测试服务器地址-------------//
//#define KURL            @"http://192.168.0.185/"
#define KKaptcha        @"http://test.mingyu100.com/"                //图片前缀
#define bucketName      @"mykj-test"


//-----------------外网正式服务器地址-------------//
//#define KURL            @"http://app.mingyu100.com/"
//#define KKaptcha        @"http://img.mingyu100.com/"                //图片前缀
//#define bucketName      @"mykj-public"


#define KUser @"UserController/user"                                            //添加用户


#define KLoginByPassword  @"api/1.1/login/password"                          //密码登录

#define KSendSMSCode @"api/1.1/login/sendSMSCode"                             //发送验证码

#define KLoginByCode @"api/1.1/login/code"                                    //验证码登录

#define KUpdatePassword @"UserController/updatePassword"                        //更新密码

#define KWeChatLoginUpdate @"UserController/weChatLoginUpdate"                   //微信登录

#define KCheckUserOpenId @"api/1.1/login/weChat/isExist"                      //判断是否绑定了手机



//公用  收藏 点赞 评论 相关操作
#define KAddCollect @"CollectController/addCollect"                             //添加收藏

#define KDelCollect @"CollectController/delCollect"                             //删除收藏

#define KAddAccusation @"AccusationController/addAccusation"                    //举报评论

#define KAddClickPraise @"ClickPraiseController/addClickPraise"                 //添加点赞记录

#define KDelClickPraise @"ClickPraiseController/delClickPraise"                 //删除点赞记录

#define KGetCommentList @"CommentController/getCommentList"                     //获取相关类型评论

#define KgetContentCommentList @"CommentController/getContentCommentList"       //获取内容详情页的评论

#define KAddComment @"CommentController/addComment"                             //添加相关类型评论

#define KDeleteCommentById @"CommentController/deleteCommentById"               //删除相关类型评论

#define KAddCommentReplay @"CommentReplayController/addCommentReplay"           //添加回复评论

#define KDeleteReplayById @"CommentReplayController/deleteReplayById"           //删除相关类型评论回复


#define KGetFollowTopicList @"TopicController/getFollowTopicList"              //获取关注的话题帖子

#define KGetRecommendFollowList @"FollowController/getRecommendFollowList"     //获取推荐相关的用户信息

#define KAddFollow @"FollowController/addFollow"                               //添加关注

#define KDelFollow @"FollowController/delFollow"                                //取消关注

#define KGetTopicList @"TopicController/getTopicList"                           //获取话题列表

#define KGetTopTopic @"TopicController/getTopTopic"                             //获取用户置顶的帖子

#define KGetTopic @"TopicController/getTopic"                                   //获取话题详情

#define KGetCircleMessage @"CircleController/getCircleMessage"                  //根据圈子id查询圈子信息

#define KGetRecommendTopic @"TopicController/getRecommendTopic"                 //获取相似话题帖子

#define KSetTop @"TopicSetTopController/setTop"                                 //添加置顶

#define KCancelTop @"TopicSetTopController/cancelTop"                           //取消置顶

#define KDelTopic @"TopicController/delTopic"                                   //删除自己的帖子

#define KGetCircleList @"CircleController/getCircleList"                        //获取分类下的圈子列表

#define KGetArticleList @"ArticleController/getArticleList"                     //获取相关咨询

#define KgetArticle @"ArticleController/getArticle"                             //获取咨询详情

#define KGetRecommendArticle @"ArticleController/getRecommendArticle"           //获取资讯相关推荐

#define KAddTopic @"TopicController/addTopic"                                   //发布话题

#define KImgUpload @"FileController/ImgUpload"                                  //上传图片

#define KFileUploadBatch @"FileController/fileUploadBatch"                     //多图片上传

#define KUpdateTopicIsCircle @"TopicController/updateTopicIsCircle"             //更新话题帖子所在的圈子

#define KGetRecommendCircleList @"CircleController/getRecommendCircleList"      //获取推荐圈子列表

#define KAddUserCircle @"CircleUserController/addUserCircle"                    //添加圈子

#define KDelUserCircle @"CircleUserController/delUserCircle"                    //用户删除圈子

#define KGetAddTopicCircle @"CircleController/getAddTopicCircle"                //发帖时，供选择的圈子

//#define KGetChildcareList @"ChildcareController/getChildcareList"               //获取育儿必读文章列表

#define KGetChildcare @"ChildcareController/getChildcare"                       //获取育儿必读详情

#define KGetRecommendChildcare @"ChildcareController/getRecommendChildcare"     //获取育儿必读的相关推荐

#define KGetChildcareLabelList @"LabelController/getChildcareLabelList"         //获取育儿必读标签列表

//#define KGetUserActionList @"UserActionController/getUserActionList"            //查询用户动作记录

#define KGetRegisterAction @"UserActionController/getRegisterAction"            //查询签到记录

#define KAddRegisterAction @"UserActionController/addRegisterAction"            //添加动作记录

#define KGetTaskList @"TaskController/getTaskList"                              //查询任务



#define KGetSubjectList @"SubjectController/getSubjectList"                   //获取所有主题

#define KGetSubjectAudioList @"AudioController/getSubjectAudioList"           //查询主题下的音频列表

#define KGetAlbumAudioList @"AudioController/getAlbumAudioList"               //查询专辑下的音频

#define KGetAudio @"AudioController/getAudio"                                 //查询音频详情

#define KGetRecommendAlbum @"AlbumController/getRecommendAlbum"              //专辑相关推荐

#define KGetArticleLabelList @"LabelController/getArticleLabelList"           //获取资讯的分类标签

#define KGetListenBookList @"ListenBookController/getListenBookList"          //获得听书各类型列表

#define KListenBook @"ListenBookController/listenBook"                        //获取听书详情

#define KGetBookAudioList @"AudioController/getBookAudioList"                 //获取音频书的音频目录

#define KGetRecommendBook @"ListenBookController/getRecommendBook"            //听书相关推荐

#define KGetChapterDetails @"AudioController/getChapterDetails"               //查询音频书的音频详情

#define KGetCourseList @"CourseController/getCourseList"                       //精品课程列表查询

#define KGetCourseDirectory @"CourseController/getCourseDirectory"             //课程下的视频目录查询

#define KAddAudioRedisCount @"AudioController/addAudioRedisCount"              //每次播放音频调用接口 用于后台统计播放量


#define KGetHomeUserMessage @"UserController/getHomeUserMessage"              //获取首页个人信息

#define KGetBabyRecordList @"BabyRecordController/getBabyRecordList"          //获得宝宝成长记录集合

#define KGetUserMessage @"UserController/getUserMessage"                      //获取用户详细信息

#define KUpdateOldPassword @"UserController/updateOldPassword"                 //更新用户密码

#define KUpdateUser @"UserController/updateUser"                              //更新用户部分信息

#define KGetAddressList @"AddressController/getAddressList"                    //获取收货地址列表

#define KAddAddress @"AddressController/addAddress"                           //增加收货地址

#define KDelAddress @"AddressController/delAddress"                            //删除单个收货地址

#define KSetDefaultAddress @"AddressController/setDefaultAddress"              //设置默认收货地址

#define KUpdateAddress @"AddressController/updateAddress"                      //修改收货地址信息

#define KUpdatePhone @"UserController/updatePhone"                             //修改绑定手机号

#define KGetMyArticleCollectList @"ArticleController/getMyArticleCollectList"   //我的资讯收藏列表

#define KGetMyCollectBookList @"ListenBookController/getMyCollectBookList"      //我的听书收藏列表

#define KGetMyCourseCollectList @"CourseController/getMyCourseCollectList"      //我的课程收藏列表

#define KGetMyChildcareCollectList @"ChildcareController/getMyChildcareCollectList"  //我的育儿必读收藏列表

#define KGetMyTopicCollectList @"TopicController/getMyTopicCollectList"             //我的帖子收藏列表

#define KGetUserTopicList @"TopicController/getUserTopicList"                       //个人发帖列表

#define KGetMyTopicCommentList @"CommentController/getMyTopicCommentList"           //获取我对别人帖子的评论列表

#define KGetFollowUserList @"UserController/getFollowUserList"                     //获取粉丝列表

#define KGetUserFollowList @"UserController/getUserFollowList"                     //获取用户关注列表

#define KGetOtherUserMessage @"UserController/getOtherUserMessage"                 //获取其他用户信息

#define KGetCourseRecommend @"CourseController/getCourseRecommend"                  //获得课程推荐

#define KGetMyHoldCourse @"CourseController/getMyHoldCourse"                        //获得我已购买课程列表

#define KGetTeacherCourseList @"CourseController/getTeacherCourseList"              //获取教师课程列表

#define KGetTeacherMessage @"TeacherController/getTeacherMessage"                   //获得教师主页信息

#define KSearchAssociation @"EsController/searchAssociation"                       //搜索联想

//#define KGetEsMessageList @"EsController/getEsMessageList"                         //综合全文搜索

#define KGetHotLabelList @"SearchRecordController/getHotLabelList"                  //获取最热搜索记录


#define KGetVideoLabelList @"LabelController/getVideoLabelList"                     //获取短视频标签

//#define KGetShortVideoList @"VideoController/getShortVideoList"                     //获取短视频列表

#define KGetShortVideo @"VideoController/getShortVideo"                            //获取单个短视频信息

#define KGetRecommendVideo @"VideoController/getRecommendVideo"                     //获取视频相关推荐

#define KGetMyVideoCollectList @"VideoController/getMyVideoCollectList"             //获取段视频收藏列表

#define KGetClassifyList  @"ClassifyController/getClassifyList"                     //获取一级标签列表

#define KGetUserCircleList @"CircleUserController/getUserCircleList"                //获取用户添加的圈子


#define KGetQQUserList @"QQUserController/getQQUserList"                            //获取私信人列表

#define KGetQQMessageByQQId @"QQMessageController/getQQMessageByQQId"               //获取聊天信息

#define KAddQQMessage @"QQMessageController/addQQMessage"                           //添加聊天信息

#define KDelQQUserById @"QQUserController/delQQUserById"                            //删除列表单个数据


#define KGetDiaryList @"DiaryController/getDiaryList"                               //获取时光印记列表

#define KGetYearMonthList @"DiaryController/getYearMonthList"                       //获取相册列表

#define KGetMonthPhotoList @"DiaryPhotoController/getMonthPhotoList"                //获取某月日记的图片集

#define KDelPhotoList @"DiaryPhotoController/delPhotoList"                          //批量删除时光印记图片

#define KDelDiary @"DiaryController/delDiary"                                       //删除时光印记

#define KAddDiary @"DiaryController/addDiary"                                       //添加时光印记

#define KUpdateDiary @"DiaryController/updateDiary"                                 //修改时光印记

#define KGetDiary @"DiaryController/getDiary"                                       //获取时光印记详情



#define KGetUserSubjectHistory @"SubjectController/getUserSubjectHistory"           //首页 - 获取用户播放主题历史

#define KGetInterestLabelList @"LabelController/getInterestLabelList"               //获取兴趣选择分类标签

#define KAddInterestSelect @"InterestSelectController/addInterestSelect"            //添加兴趣选择

#define KDelInterestSelect @"InterestSelectController/delInterestSelect"            //删除兴趣选择

#define KGetGuessUserLike @"ArticleController/getGuessUserLike"                     //获取首页猜你喜欢

#define KGetSystemMessageList @"SystemMessageController/getSystemMessageList"       //获取系统消息列表

#define KGetHomeVideoList @"VideoController/getHomeVideoList"                       //获取首页视频列表

#define KGetHomeChildcare @"ChildcareController/getHomeChildcare"                   //获取首页育儿必读


//#define KAddFirstShare @"UserActionController/addFirstShare"                        //用户每日第一次分享

#define KGetAdvertisement @"AdvertisementController/getAdvertisement"               //获取广告

#define KGetCollectTypeNum @"CollectController/getCollectTypeNum"                   //获取已收藏各类型

#define KGetUpToken @"FileController/getUpToken"                                    //获取七牛云凭证

#define KAddUserRemind @"UserRemindController/addUserRemind"                        //关闭提醒功能

#define KDelUserRemind @"UserRemindController/delUserRemind"                        //添加提醒功能

#define KGetUserRemind @"UserRemindController/getUserRemind"                        //获取提醒状态


#define KGetShortVideoList @"api/1.1/video/labelId"                              //获取段视频列表

//#define KAddShareLog @"api/1.1/userAction/firstShare"                            //添加用户分享日志和首次分享记录

#define KRecordLog @"api/1.1/log"                                                  //添加用户浏览退出时间日志

#define KClassifyId @"api/1.1/label/classifyId"                                 //获取育儿必读分类标签

#define KGetChildcareList @"api/1.1/childcares/labelId"                          //根据lablID获取育儿必读列表

#define KSearch @"api/1.1/search"                                               //综合全文搜索

#pragma mark 精品课模块接口

#define KAppMenuType @"api/1.1/appMenus/type"                             /**< 获取所属年龄段与用户已添加的分类标签 /editMenu 获取其余的分类标签 */

#define KUserMenu @"api/1.1/userMenu"                                    /**< 用户添加标签--用户删除标签 */

#define KCourseMenuId @"api/1.1/courses/menuId"                           /**< 二级目录--精品课ID */

#define KChildMenu  @"api/1.1/appMenus"                                   /**< 获取子标签 */

#define KGetCourseDetails @"api/1.1/course"                              /**< 精品课-课程介绍 */

#define KGetCourseCourseId @"api/1.1/course"                             /**< 精品课-获取课程详情头部信息 */

#define KGetCourseSection @"api/1.1/courseSections"                       /**< 精品课-课程章节目录 */

#define KGetCourseUserId @"api/1.1/courses/tUserId"                        /**< 精品课-获取老师发布课程的列表 */

#define KGetTeacherTUserId @"api/1.1/teachers/fkUserId"                    /**< 精品课-获取推荐老师 */

#define KGetStorePayMoney @"api/1.1/payMoney"                             /**< 获取内购商品列表 */

#define KGetVideoTUserId @"api/1.1/videos/userId"                         /**< 精品课-获取老师详情的短视频 */

#define KGetCourseUser @"api/1.1/courseUser/userId"                        /**< 精品课-已购课程 */

#define KCourseUser @"api/1.1/courseUser"                                  /**< 精品课-特权课 */

#pragma mark 充值模块 pay

#define KGetAmountOptions @"api/1.1/amountOptions"                         /**< 精品课-获取育币充值信息(育币充值) */

#define KGetUserBalance @"api/1.1/user"                                       /**< 精品课-查询用户育币和积分 */

#define KServerCreateoOrder @"api/1.1/order"                            /**<后台创建订单 */

#define KIAPOrderVerify @"api/1.1/iosPay/verify"                                /**<后台创建校验 */

#define KOrderRecharge @"api/1.1/orderRecharge"                             /** 添加充值订单*/

#define KDeleteQQUser @"api/1.1/qqUser"                                     /** 删除列表联系人会话*/


#define KDeleteQQUser @"api/1.1/qqUser"                                     /** 删除列表联系人会话*/

/** 第一次分享 */
#define KFirstShare @"api/1.1/userTask/firstShare"

/**获取用户的积分明细 */
#define KIntegralDetail @"api/1.1/IntegralDetail/userId"

/**退出登录 */
#define KSignOut @"api/1.1/login/signOut"

/** 获取育币明细 */
#define KVirtualMoneyDetail @"api/1.1/virtualMoneyDetail/userId"

#pragma mark 首页V1.4.0模块
/** 根据菜单id获取信息 */
#define KHomePageMenuId @"api/1.1/homePage/menuId"

/**首页头部信息如:视频 精品课模块*/
#define KHomePageApiIcon @"api/1.1/icon"

/** 首页 推荐模块 一键听 */
#define KHomePageSubject @"api/1.1/subject/homePage"

/** 首页 推荐模块 课程 */
#define KHomePageCourses @"api/1.1/courses/homePage"







#define USER_ID  [userDefault objectForKey:KUDuser_id] ? [userDefault objectForKey:KUDuser_id] : @(0)

#define userDefault [NSUserDefaults standardUserDefaults]

#define KUDhasLaunch @"hasLaunch"   //第一次启动

#define KUDhasLogin @"hasLogin1.3.1"    //是否登录

#define KUDrole @"role"            //家长端 儿童端

#define KUDdevice_id @"device_id"     //设备ID

#define KUDuser_id @"user_id"       //用户ID

#define KUDtoken @"token"           //token

#define KUDuser_name @"user_name"   //用户名
    
#define KUDuser_phone @"user_phone"  //电话号码

#define KUDuser_password @"user_password"   //密码

#define KUDuser_head @"user_head"           //用户头像

#define KUDfollow_number @"follow_number"   //粉丝数

#define KUDTokentime @"Tokentime"       //token有效期

#define KUDuser_birthday @"user_birthday"   //小孩生日

#define KUDrelationship @"relationship"     //与小孩关系

#define KUDQianDaoSwitch @"QianDaoSwitch"    //签到提醒

#define KUDbabyhead @"baby_head"           //小孩图像

#define KUDVaccineMessage @"VaccineMessage"  //疫苗提醒

#define KUDLastCommentString @"lastCommentString"  //最后一次输入评论内容 并且没有发布

#define KUDchildCareAgeIndex @"childCareAgeIndex"  //育儿必读年龄段
//
//#define KUDuser_birthday @"user_birthday"
//
//#define KUDuser_birthday @"user_birthday"
//
//#define KUDuser_birthday @"user_birthday"




@end


