//
//  VaccineMessageViewController.m
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VaccineMessageViewController.h"

@interface VaccineMessageViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *messageswitch;

@end

@implementation VaccineMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"提醒设置";
    _messageswitch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放
    
//    [_messageswitch setOn:![userDefault boolForKey:KUDVaccineMessage]];
    
    [self getUserRemind];
}

- (void)getUserRemind{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserRemind];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"type":@"2"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            BOOL data = [json[@"data"] boolValue];
            [_messageswitch setOn:data];
        }
    } failure:^(NSError *error) {

    }];
}

- (IBAction)messageswitch:(UISwitch *)sender {
    if (!sender.on) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"关闭疫苗提醒？"
                                                                       message:@"\n关闭提醒，可能会忘记打疫苗哦"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                 [sender setOn:YES];
                                                                 
                                                             }];
        [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
        
        UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //响应事件
                                                                  ZPLog(@"关闭疫苗提醒");
                                                                  [self ChangeUserRemindOn:NO];
                                                              }];
        [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
        
        [alert addAction:cancelAction];
        [alert addAction:confirmAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else{
        ZPLog(@"打开签到提醒");
        [self ChangeUserRemindOn:YES];

    }
}



- (void)ChangeUserRemindOn:(BOOL)on{
    if (on) { //开启
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"2"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_messageswitch setOn:YES];
            } else {
                [_messageswitch setOn:NO];
            }
        } failure:^(NSError *error) {
            [_messageswitch setOn:NO];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"2"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_messageswitch setOn:NO];
            } else{
                [_messageswitch setOn:YES];
            }
        } failure:^(NSError *error) {
            [_messageswitch setOn:YES];
        }];
    }
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
