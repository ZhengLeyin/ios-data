//
//  LeftTableViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LeftTableViewCell : UITableViewCell

//@property (nonatomic, strong) UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *classify_name_Lab;

@property (weak, nonatomic) IBOutlet UIView *chooseView;

@end
