//
//  TabBarController2.m
//  TZWY
//
//  Created by apple on 2018/3/19.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "TabBarController2.h"

@interface TabBarController2 ()<CYTabBarDelegate>

@property (nonatomic, strong) TabBarController *tabbar1;

//@property (nonatomic,strong) UIButton *AddButton;

@end

@implementation TabBarController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tabbar.delegate = self;
}


#pragma mark - CYTabBarDelegate
//中间按钮点击
- (void)tabbar:(CYTabBar *)tabbar clickForCenterButton:(CYCenterButton *)centerButton{
    if (!_tabbar1) {
        _tabbar1 = [[TabBarController alloc]init];
    }
    [CYTabBarConfig shared].selectIndex = 0;
    [CYTabBarConfig shared].centerBtnIndex = 4;
    [CYTabBarConfig shared].bulgeHeight = 4;
    [CYTabBarConfig shared].HidesBottomBarWhenPushedOption = HidesBottomBarWhenPushedAlone;
    NSString *role = [userDefault valueForKey:KUDrole];
    if ([role  isEqual: @"kid"]) {
        [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithHexString:@"50D0F4"];
        [CYTabBarConfig shared].textColor = [UIColor colorWithHexString:@"A8A8A8"];
        [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
        [TabBarController style2:_tabbar1];
        [userDefault setValue:@"parent" forKey:KUDrole];
    }else{
        [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithRed:249/255.0f green:218/255.0f blue:97/255.0f alpha:1];
        [CYTabBarConfig shared].textColor = [UIColor grayColor];
        [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
        [TabBarController style1:_tabbar1];
        [userDefault setValue:@"kid" forKey:KUDrole];
    }
    [userDefault synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (ContentView *)contentView{
    return nil;
}


- (void)AnimateBegin{
    //centet button rotation
    
    //创建动画
    CATransition *animation = [CATransition animation];
    //设置运动轨迹的速度
    //    animation.timingFunction = UIViewAnimationCurveEaseIn;
    //设置动画类型为立方体动画
    animation.type = kCATransitionMoveIn;
    //设置动画时长
    animation.duration =0.3f;
    //设置运动的方向
    animation.subtype =kCATransitionFromTop;
    //控制器间跳转动画
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:animation forKey:nil];
}


//是否允许切换
- (BOOL)tabBar:(CYTabBar *)tabBar willSelectIndex:(NSInteger)index{
//    ZPLog(@"将要切换到---> %ld",index);
    
    return YES;
}
//通知切换的下标
- (void)tabBar:(CYTabBar *)tabBar didSelectIndex:(NSInteger)index{
//    ZPLog(@"切换到---> %@",tabBar.items);
    NSString *role = [userDefault valueForKey:KUDrole];
    if ([role  isEqual: @"kid"]) {
        if (index == 2) {
            self.AddButton.hidden = NO;
        } else{
            self.AddButton.hidden = YES;
        }
    } else{
        self.AddButton.hidden = YES;
    }
}

//
- (UIButton *)AddButton{
    if (!_AddButton) {
        _AddButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _AddButton.frame = CGRectMake((self.view.width-60)/2, self.view.height-64, 60, 60);
//        _AddButton.backgroundColor = [UIColor redColor];
        [_AddButton setTitle:@"发布" forState:UIControlStateNormal];
        [_AddButton setTitleColor:[CYTabBarConfig shared].selectedTextColor forState:UIControlStateNormal];

        _AddButton.titleLabel.font = FontSize(10);
        [_AddButton setImage:[UIImage imageNamed:@"menu_index_gray"] forState:UIControlStateNormal];
        // button标题的偏移量
        _AddButton.titleEdgeInsets = UIEdgeInsetsMake(_AddButton.imageView.frame.size.height+5, -_AddButton.imageView.bounds.size.width, 0,0);
        // button图片的偏移量
        _AddButton.imageEdgeInsets = UIEdgeInsetsMake(0, _AddButton.titleLabel.frame.size.width/2, _AddButton.titleLabel.frame.size.height+5, -_AddButton.titleLabel.frame.size.width/2);

        [_AddButton addTarget:self action:@selector(fabu) forControlEvents:UIControlEventTouchUpInside];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        [window addSubview:_AddButton];
//        [self.view addSubview:_AddButton];
    }
    return _AddButton;
}


- (void)fabu{
    ZPLog(@"hahahah");

    [self presentViewController:[ViewController new] animated:YES completion:nil];
}



@end
