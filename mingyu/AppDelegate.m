//
//  AppDelegate.m
//  mingyu
//
//  Created by apple on 2018/3/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "DFPlayer.h"
#import <UMCommon/UMCommon.h>
#import <UMShare/UMShare.h>
#import <UserNotifications/UserNotifications.h>
#import <UMPush/UMessage.h>
#import <NSString+Base64.h>

#define UMKEY @"5b0d0a67a40fa32e940000b3"


@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate

- (void)applicationWillTerminate{
    [[DFPlayer shareInstance] df_setPreviousAudioModel];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
//设置全局状态栏字体颜色为黑色
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

//    处理苹果内购掉单问题
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:AppStoreInfoLocalFilePath]) {//如果在改路下存在文件，存在购买凭证，说明发送凭证失败，再次发起验证
        [self sendFailedIapFiles];
    }

//注册友盟相关信息 统计 第三方登录 分享 推送
    [self registerUMKeyWithOptions:launchOptions];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate) name:UIApplicationWillTerminateNotification object:nil];
    
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    navigationBar.translucent = NO;
    //    //    //    隐藏导航栏下面的黑线
    [navigationBar setBackgroundImage:[UIImage new]  //换个背景图片
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
//    [navigationBar setShadowImage:[UIImage new]];
    
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self setIQKeyboardManager];

    if (![userDefault boolForKey:KUDhasLaunch]) {
        FirstLaunchViewController *rootVC = [[FirstLaunchViewController alloc] init];
        self.window.rootViewController = rootVC;
    } else {
        TabBarController * tabbar = [[TabBarController alloc]init];
        tabbar.tabBar.translucent = NO;
        // 配置
        [CYTabBarConfig shared].centerBtnIndex = 4;
        [CYTabBarConfig shared].bulgeHeight = 4;
        [CYTabBarConfig shared].HidesBottomBarWhenPushedOption = HidesBottomBarWhenPushedAlone;
        NSString *role = [userDefault valueForKey:KUDrole];
        if ([role isEqual: @"kid"]) {
            [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithRed:249/255.0f green:218/255.0f blue:97/255.0f alpha:1];
            [CYTabBarConfig shared].textColor = [UIColor grayColor];
            [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
            [TabBarController style1:tabbar];
        } else {
            [CYTabBarConfig shared].selectedTextColor = [UIColor colorWithHexString:@"50D0F4"];
            [CYTabBarConfig shared].textColor = [UIColor colorWithHexString:@"A8A8A8"];
            [CYTabBarConfig shared].backgroundColor = [UIColor whiteColor];
            [TabBarController style2:tabbar];
        }
        
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
        self.window.rootViewController = tabbar;
        [self.window makeKeyAndVisible];
    }
    return YES;
}



- (void)sendFailedIapFiles{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    
    //搜索该目录下的所有文件和目录
    NSArray *cacheFileNameArray = [fileManager contentsOfDirectoryAtPath:AppStoreInfoLocalFilePath error:&error];
    if (cacheFileNameArray.count == 0) {
        [self sendAppStoreRequestSucceededWithData];
    }
    if (error == nil){
        for (NSString *name in cacheFileNameArray){
            if ([name hasSuffix:@".plist"]){//如果有plist后缀的文件，说明就是存储的购买凭证
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", AppStoreInfoLocalFilePath, name];
                [self sendAppStoreRequestBuyPlist:filePath];
            }
        }
    }
}


- (void)sendAppStoreRequestBuyPlist:(NSString *)plistPath{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    //这里的参数请根据自己公司后台服务器接口定制，但是必须发送的是持久化保存购买凭证
    NSData *receipt = [dic objectForKey:@"IAP_RECEIPT"];
    if(receipt==nil){
        [self sendAppStoreRequestSucceededWithData];
        return;
    }
    NSString *receiptBase64 = [NSString base64StringFromData:receipt length:[receipt length]];
    NSString *order_number = [dic objectForKey:@"order_number"];
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KIAPOrderVerify];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"order_number":order_number,
                                 @"payload":receiptBase64
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [self sendAppStoreRequestSucceededWithData];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];

}

//验证成功就从plist中移除凭证
- (void)sendAppStoreRequestSucceededWithData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:AppStoreInfoLocalFilePath]) {
        [fileManager removeItemAtPath:AppStoreInfoLocalFilePath error:nil];
        
    }
}


//设置键盘
- (void)setIQKeyboardManager{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
//    manager.shouldToolbarUsesTextFieldTintColor = YES;
//    manager.shouldShowToolbarPlaceholder = NO;
}


//注册友盟相关信息 统计 第三方登录 分享 推送
- (void)registerUMKeyWithOptions:(NSDictionary *)launchOptions {
    //友盟统计
    #ifdef DEBUG
        [UMConfigure initWithAppkey:UMKEY channel:@"TEST"];
    #else
        [UMConfigure initWithAppkey:UMKEY channel:@"App Store"];
    #endif
    
    //登录 分享
    //设置AppKey，是在友盟注册之后给到的key
    [[UMSocialManager defaultManager] setUmSocialAppkey:UMKEY];
    [self setupUSharePlatforms];   // required: setting platforms on demand

    
    //友盟推送 
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    //type是对推送的几个参数的选择，可以选择一个或者多个。默认是三个全部打开，即：声音，弹窗，角标
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionAlert|UMessageAuthorizationOptionSound;
    if (@available(iOS 10.0, *)) {
        [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    }
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
        }else
        {
        }
    }];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    [UMessage registerDeviceToken:deviceToken];
    if ([userDefault boolForKey:KUDhasLogin]) {
        NSString *alias = [NSString stringWithFormat:@"mingyu%@",USER_ID];
        ZPLog(@"%@",alias);
        [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
    } else {
        NSString *alias = [NSString stringWithFormat:@"mingyu0"];
        ZPLog(@"%@",alias);
        [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
    }
   
    ZPLog(@"%@",[[[[deviceToken description ] stringByReplacingOccurrencesOfString: @"<" withString:@"" ] stringByReplacingOccurrencesOfString: @">" withString:@""]  stringByReplacingOccurrencesOfString: @" " withString:@""]);
}


//iOS10以下使用这两个方法接收通知，
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    if (application.applicationState == UIApplicationStateActive) {
        ZPLog(@"active"); //前台
        if ([userInfo[@"jump_type"] integerValue] == 1) { //私信
            NSDictionary *dic = [NSDictionary dictionaryWithObject:userInfo[@"fk_from_id"] forKey:@"fk_from_id"];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceivePriVateMessage object:nil userInfo:dic];
        } else if ([userInfo[@"jump_type"] integerValue] == 2) { //系统消息
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveSystemMessage object:nil userInfo:nil];
        }
    }else if(application.applicationState == UIApplicationStateInactive){
        ZPLog(@"inactive"); //后台

        ZPLog(@"%@",userInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveNotification object:nil userInfo:nil];
        if ([userInfo[@"jump_type"] integerValue] == 1) {  //私信
            TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
            MineMessageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineMessageViewController"];
            VC.selectedItem = 1;
            [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceivePriVateMessage object:nil userInfo:nil];
        } else if ([userInfo[@"jump_type"] integerValue] == 2){ //系统消息
            TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
            MineMessageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineMessageViewController"];
            VC.selectedItem = 0;
            [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveSystemMessage object:nil userInfo:nil];
        } else if([userInfo[@"jump_type"] integerValue] == 3) {  //签到
            TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
            QianDaoViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"QianDaoViewController"];
            [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
        } else if([userInfo[@"jump_type"] integerValue] == 4) {  //疫苗提醒
            TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
            BabyVaccineViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"BabyVaccineViewController"];
            [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
        }
    }
    
    [UMessage setAutoAlert:NO];
    if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
        [UMessage didReceiveRemoteNotification:userInfo];

        completionHandler(UIBackgroundFetchResultNewData);
    }
}


//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    if (@available(iOS 10.0, *)) {
        completionHandler(UNNotificationPresentationOptionSound);
    } else {
        // Fallback on earlier versions
    }
    if (@available(iOS 10.0, *)) {
        if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            //应用处于前台时的远程推送接受
            //关闭U-Push自带的弹出框
            [UMessage setAutoAlert:NO];
            //必须加这句代码
            [UMessage didReceiveRemoteNotification:userInfo];
            if ([userInfo[@"jump_type"] integerValue] == 1) { //私信
                NSDictionary *dic = [NSDictionary dictionaryWithObject:userInfo[@"fk_from_id"] forKey:@"fk_from_id"];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceivePriVateMessage object:nil userInfo:dic];
            } else if ([userInfo[@"jump_type"] integerValue] == 2) { //系统消息
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveSystemMessage object:nil userInfo:nil];
            }
            
        }else{
            //应用处于前台时的本地推送接受
        }
    } else {
        // Fallback on earlier versions
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个

}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if (@available(iOS 10.0, *)) {
        if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            //应用处于后台时的远程推送接受
            //必须加这句代码
            [UMessage didReceiveRemoteNotification:userInfo];
            ZPLog(@"%@",userInfo);
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveNotification object:nil userInfo:nil];

            if ([userInfo[@"jump_type"] integerValue] == 1) {  //私信
                TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
                MineMessageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineMessageViewController"];
                VC.selectedItem = 1;
                [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceivePriVateMessage object:nil userInfo:nil];
            } else if ([userInfo[@"jump_type"] integerValue] == 2){ //系统消息
                TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
                MineMessageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineMessageViewController"];
                VC.selectedItem = 0;
                [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
                [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceiveSystemMessage object:nil userInfo:nil];
            } else if([userInfo[@"jump_type"] integerValue] == 3) {
                TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
                QianDaoViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"QianDaoViewController"];
                [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
            } else if([userInfo[@"jump_type"] integerValue] == 4) {
                TabBarController *baseTabBar = (TabBarController *)self.window.rootViewController;
                BabyVaccineViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"BabyVaccineViewController"];
                [baseTabBar.viewControllers[baseTabBar.selectedIndex] pushViewController:VC animated:NO];
            }
        }else{
            //应用处于后台时的本地推送接受
        }
    } else {
        // Fallback on earlier versions
    }
}


//第三方登录分享
- (void)setupUSharePlatforms {
    /*
     设置微信的appKey和appSecret
     [微信平台从U-Share 4/5升级说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_1
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxab503a5b4a8f7af2" appSecret:@"900cf33108ffb08d0c6e794ea8208c65" redirectURL:nil];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     100424468.no permission of union id
     [QQ/QZone平台集成说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_3
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1107802681"/*设置QQ平台的appID*/  appSecret:nil redirectURL:nil];
    
    /*
     设置新浪的appKey和appSecret
     [新浪微博集成说明]http://dev.umeng.com/social/ios/%E8%BF%9B%E9%98%B6%E6%96%87%E6%A1%A3#1_2
     */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:@"2781089464"  appSecret:@"3303fa6698348b9f27da66a360fb1f04" redirectURL:@"https://sns.whalecloud.com/sina2/callback"];
}

// 支持所有iOS系统版本回调
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.


}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
  
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
