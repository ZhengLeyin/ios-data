//
//  CollectBookViewController.h
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectBookViewController : UIViewController

- (void)editorButton:(BOOL )iseditor;

@end
