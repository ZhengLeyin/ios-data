//
//  FooterBottomLineView.h
//  mingyu
//
//  Created by MingYu on 2018/12/15.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FooterBottomLineView : UIView
/**我是有底线的View*/
+ (instancetype)TheFooterBottomLineView ;
@end

NS_ASSUME_NONNULL_END
