//
//  TeacherIntroduceCell.h
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CourseModel;
NS_ASSUME_NONNULL_BEGIN

@interface TeacherIntroduceCell : UITableViewCell
/**背景色*/
@property (weak, nonatomic) IBOutlet UIView *teacherIntroduceView;
/**老师用户头像*/
@property (weak, nonatomic) IBOutlet UIImageView *teacherUserImageView;
/**老师姓名*/
@property (weak, nonatomic) IBOutlet UILabel *teacherNameLabel;
/**老师职称*/
@property (weak, nonatomic) IBOutlet UILabel *teacherProfessionalLabel;
/**关注*/
@property (weak, nonatomic) IBOutlet UIButton *followButton;
/** 跳转老师详情界面 */
@property (nonatomic, copy) void (^pushTeacherDetailsBlock)(void);
/** 关注按钮 */
@property (nonatomic, copy) void (^followButtonBlock)(void);

@property (weak, nonatomic) IBOutlet UIView *lineBackView;


@property (nonatomic, strong) CourseModel *model;

+ (instancetype)theTeacherIntroduceCellWithtableView:(UITableView *)tableview ;
/**老师详情*/
+ (instancetype)theTeacherIntroduceCellOfDateilWithtableView:(UITableView *)tableview;
@end

NS_ASSUME_NONNULL_END
