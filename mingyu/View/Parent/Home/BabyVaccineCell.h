//
//  BabyVaccineCell.h
//  mingyu
//
//  Created by apple on 2018/6/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BabyVaccineCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *chooseButton;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *function_button;

@property (weak, nonatomic) IBOutlet UILabel *must_lab;

@property (nonatomic, strong) NSDictionary *dic;


@property (nonatomic, copy) void (^chooseButtonBlock)(void);


+ (instancetype)theBabyVaccineCellWithtableView:(UITableView *)tableview;


@end
