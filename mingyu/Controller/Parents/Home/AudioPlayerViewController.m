//
//  AudioPlayerViewController.m
//  mingyu
//
//  Created by apple on 2018/6/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "AudioPlayerViewController.h"
#import "BottomCommetView.h"
#import "CLInputToolbar.h"
#import "AudioPlayerView.h"

@interface AudioPlayerViewController ()<UITableViewDelegate,UITableViewDataSource,DFPlayerDelegate,CommentListCellDelegate,BottomCommentDelegate>


@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) AudioPlayerView *playerHeaderView;

@property (nonatomic, strong) BottomCommetView *bottomView;

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *recommenData;  //相关推荐

@property (nonatomic, strong) NSMutableArray *commentData;   //评论列表数据源

@property (nonatomic, assign) NSInteger comment_number;    //总评论数

@property (nonatomic, copy) NSString *parent_head;
@property (nonatomic, copy) NSString *parent_title;
@property (nonatomic, assign) NSInteger playNum;

@property (nonatomic, strong) AudioModel *audiomodel;
@property (nonatomic, assign) NSInteger audio_type;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, assign) BOOL firstLoad;

@end

@implementation AudioPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _lastPage = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Back) name:NSNotificationReceiveNotification object:nil];

    _audio_type = [DFPlayer shareInstance].currentAudioModel.audio_type;
    if (!_audio_type) {
        _audio_type = [DFPlayer shareInstance].previousAudioModel.audio_type;
    }
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupNavigationItem];
    [self.view addSubview:self.tableView];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];

    if (_audio_type != 3) {
        [self.view addSubview:self.bottomView];
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(-effectViewH);
            make.height.mas_equalTo(46);
        }];
    }
   
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.edges.equalTo(self.view);
        } else {
            make.top.equalTo(self.view).offset(64);
            make.left.right.bottom.equalTo(self.view);
        }
        if (_audio_type != 3) {
            make.bottom.equalTo(self.view).with.offset(-46-effectViewH);
        }
    }];

    self.playerHeaderView = [[AudioPlayerView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH)];
    self.playerHeaderView.isDownloadPaly = _isDownloadPaly;
    [self.playerHeaderView initDFPlayer];
    __weak typeof(self) WeakSelf = self;
    self.playerHeaderView.ChangeAudio = ^{
        if (_firstLoad == YES) {
            _firstLoad = NO;
            return ;
        }
        _start = 0;
        [WeakSelf.commentData removeAllObjects];
        [WeakSelf.recommenData removeAllObjects];
        [WeakSelf getChapterDetails];
        _lastPage = NO;
        if (_audio_type != 3) {
            [WeakSelf getAudioCommentList];
            [WeakSelf getRecommendAlbum];
            [WeakSelf refreshTableview];
        }
        [WeakSelf.tableView reloadData];
        [WeakSelf.inputToolbar bounceToolbar];
        WeakSelf.maskView.hidden = YES;
    };
    self.tableView.tableHeaderView = self.playerHeaderView;

    [self setTextViewToolbar];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if (_lastPage) {
    [self getChapterDetails];
    if (_audio_type != 3) {
        [self getAudioCommentList];
        [self getRecommendAlbum];
    }
    _firstLoad = YES;
//        }
//    });
  

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceiveNotification object:nil];
}


-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        // 清空输入框文字
        [weakSelf.inputToolbar bounceToolbar];
        weakSelf.maskView.hidden = YES;
        
        [weakSelf addCommentwithInputString:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
}

-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}


//上拉加载
-(void)refreshTableview {
    //上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        //        ++_pageNo;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getRecommendAlbum];
        });
    }];
    footer.stateLabel.font = FontSize(12);

    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}

//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSInteger audio_id = [DFPlayer shareInstance].currentAudioModel.audio_id;
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":@(audio_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(audio_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
//        ZPLog(@"%@",json);
//        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
//            [self.commentData removeAllObjects];
//            [self getAudioCommentList];
            
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            CommentModel *model = [CommentModel modelWithJSON:json[@"data"]];
            [self.commentData insertObject:model atIndex:0];
            _comment_number ++;
            [_tableView reloadData];
            NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



//音频详情
- (void)getChapterDetails{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetChapterDetails];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"audio_id":@([DFPlayer shareInstance].currentAudioModel.audio_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            _audiomodel = [AudioModel modelWithJSON:data[@"audio"]];
            _parent_head = data[@"img_url"];
            _parent_title = data[@"title"];
            _playNum = [data[@"playNum"] integerValue];
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        
    }];
}


//评论列表
- (void)getAudioCommentList{
    NSInteger audio_id = [DFPlayer shareInstance].currentAudioModel.audio_id;
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KgetContentCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":@(audio_id),
                                 @"comment_type":@(audio_comment),
                                 @"start":@"0",
                                 @"size":@"3"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
//        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            for (NSDictionary *dic in commentList) {
                CommentModel *model = [CommentModel modelWithJSON:dic];
                [self.commentData addObject:model];
            }
            [_tableView reloadData];
        }else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        
    }];
}


//相关推荐
- (void)getRecommendAlbum{
    
    NSString *URL = [NSString string];
    if (_audio_type == 1) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendAlbum];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"album_id":@([DFPlayer shareInstance].currentAudioModel.fk_parent_id),
                                     @"start":@(_start),
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else if (_audio_type == 2){
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendBook];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"book_id":@([DFPlayer shareInstance].currentAudioModel.fk_parent_id),
                                     @"start":@(_start),
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else{
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCourseRecommend];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"course_id":@([DFPlayer shareInstance].currentAudioModel.fk_parent_id),
                                     @"start":@(_start),
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        [self.tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            _start += 10;
            if (data && data.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                if (_audio_type == 1) {
                    AlbumListModel *circlemodel = [AlbumListModel modelWithJSON:dic];
                    [self.recommenData addObject:circlemodel];
                } else if( _audio_type == 2){
                    BookListModel *model = [BookListModel modelWithJSON:dic];
                    [self.recommenData addObject:model];
                } else {
                    CourseModel *coursemodel = [CourseModel modelWithJSON:dic];
                    [self.recommenData addObject:coursemodel];
                }
            }
            [_tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
        [self.tableView.mj_footer endRefreshing];

    }];
}



#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_audio_type == 3) {
        return 0;
    }
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1){
        if (self.commentData.count > 2) {
            return 2;
        } else{
            return self.commentData.count;
        }
    } else {
        return self.recommenData.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        return view;
    } else if (section == 1){
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
            [head.comment_number_Button setTitle:[NSString stringWithFormat:@"共%ld条评论",(long)_comment_number] forState:0];
            head.arrowimage.hidden = NO;
            head.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = [DFPlayer shareInstance].currentAudioModel.audio_id;
                VC.comment_type = audio_comment;
                [self.navigationController pushViewController:VC animated:YES];
            };
        }
        return head;
    } else {
        if (self.recommenData.count > 0) {
            return [CommentListCell theRecommendHeaderView];
        }
        return nil;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    } else if(section == 1){
        return 55;
    } else {
        if (self.recommenData.count > 0) {
            return 60;
        }
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
        view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        return view;
    } else if(section == 1){
        if (self.commentData.count == 0) {
            CommentListCell *foot = [CommentListCell theNoCommentCell];
            return foot;
        } else if (self.commentData.count < 3){
            return nil;
        } else{
            CommentListCell *foot = [CommentListCell TheMoreCell];
            __weak typeof(self) WeakSelf = self;
            foot.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = [DFPlayer shareInstance].currentAudioModel.audio_id;
                VC.comment_type =audio_comment;
                [WeakSelf.navigationController pushViewController:VC animated:YES];
            };
            return foot;
        } 
    } else {
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    } else if (section == 1){
        if (self.commentData.count == 0) {
            return 100;
        } else if (self.commentData.count < 3){
            return 0;
        } else{
            return 50;
        }    } else{
        return 0;
    }
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identify = @"cellIdentify1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        }
        [cell.contentView removeAllSubviews];
        
        UIImageView *imageview = [[UIImageView alloc] init];
        imageview.frame = CGRectMake(15, 20, 50, 50);
        [cell.contentView addSubview:imageview];
        
        UILabel *titlelab = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, kScreenWidth-120, 22)];
        titlelab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
        titlelab.font = FontSize(16);
        [cell.contentView addSubview:titlelab];
        
        UILabel *playcountlab = [[UILabel alloc] initWithFrame:CGRectMake(80, 50, 200, 22)];
        playcountlab.textColor = [UIColor colorWithHexString:@"A9A9A9"];
        playcountlab.font = FontSize(10);
        [cell.contentView addSubview:playcountlab];
        
        UIImageView *arrawimage = [[UIImageView alloc] init];
        arrawimage.frame = CGRectMake(kScreenWidth-27, 39, 12, 12);
        [cell.contentView addSubview:arrawimage];
        arrawimage.image = ImageName(@"更多");
        
        NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,_parent_head];
        imageview.backgroundColor = [UIColor lightGrayColor];
        [imageview sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:nil];
        titlelab.text = _parent_title;
        playcountlab.text = [NSString stringWithFormat:@"播放次数:%ld",(long)_playNum];
        return cell;
    } else if(indexPath.section ==1){
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        if (_commentData && _commentData.count > 0) {
            CommentModel *commenmodel = [_commentData objectAtIndex:indexPath.row];
            [commentcell setcellWithModel:commenmodel praiseType:praise_audio_comment];
            commentcell.selectionStyle = UITableViewCellSelectionStyleNone;
            commentcell.delegate = self;
        }
        return commentcell;
    } else{
        if (_audio_type == 1) {
            PlayerDatailCell *playdetailcell = [PlayerDatailCell theCellWithTableView:tableView];
            if (self.recommenData && self.recommenData.count > indexPath.row) {
                playdetailcell.albumlistModel = [self.recommenData objectAtIndex:indexPath.row];
            }
            return playdetailcell;
        } else if(_audio_type ==2){
            ListenStoryCell *cell = [ListenStoryCell theCellWithTableView:tableView];
            if (self.recommenData && self.recommenData.count > indexPath.row) {
                BookListModel *model = [self.recommenData objectAtIndex:indexPath.row];
                cell.booklistmodel = model;
            }
            return cell;
        } else{
            ParentCouresCell *courescell = [ParentCouresCell theCellWithTableView:tableView];
            if (self.recommenData && self.recommenData.count > indexPath.row) {
                CourseModel *model = self.recommenData[indexPath.row];
                courescell.coursemodel = model;
                courescell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return courescell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 90;
    } else if (indexPath.section == 1){
        if (_commentData.count > indexPath.row) {
            CommentModel *model = [_commentData objectAtIndex:[indexPath row]];
            if (model.height==0) {
                return [CommentListCell cellHeight];
            }else{
                return model.height;
            }
        } else {
            return 0;
        }
    } else {
        if (_audio_type == 1) {
            return 97;
        } else if(_audio_type ==2){
            return 130;
        } else{
            return 130;
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (_audiomodel) {
            if (_audio_type == 1) {
                AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
                VC.album_id = _audiomodel.fk_parent_id;
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
                VC.audio_type = _audiomodel.audio_type;
                VC.target_id = _audiomodel.fk_parent_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    } else if (indexPath.section == 1) {
        if (_commentData.count > indexPath.row) {
            CommentModel *commentmodel = [_commentData objectAtIndex:[indexPath row]];
            if (commentmodel.fk_user_id == [USER_ID integerValue]) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:commentmodel.comment_content
                                                                        preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction* copyAction = [UIAlertAction actionWithTitle:@"复制" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                                         pasteboard.string = commentmodel.comment_content;
                                                                         [UIView ShowInfo:@"复制成功" Inview:self.view];
                                                                     }];
                UIAlertAction* deleteAction = [UIAlertAction actionWithTitle:@"删除"
                                                                        style:UIAlertActionStyleDestructive
                                                                      handler:^(UIAlertAction * action) {
                                                                          NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
                                                                          NSDictionary *parameters = @{
                                                                                                       @"user_id":USER_ID,
                                                                                                       @"comment_id":[NSString stringWithFormat:@"%ld",(long)commentmodel.comment_id],
                                                                                                       @"comment_type":@(audio_comment)
                                                                                                       };
                                                                          [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                              NSDictionary *json = responseObject;
//                                                                              ZPLog(@"%@",json);
//                                                                              ZPLog(@"%@",json[@"message"]);
                                                                              BOOL success = [json[@"success"]boolValue];
                                                                              if (success) {
                                                                                  [_commentData removeObjectAtIndex:indexPath.row];
                                                                                  _comment_number--;
                                                                                  [_tableView reloadData];
                                                                                  if (_commentData && _commentData.count > 0) {
                                                                                      NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                                                                                      [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                                                                                  } else if (_recommenData && _recommenData.count > 0){
                                                                                      NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
                                                                                      [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                                                                                  }
                                                                              } else {
                                                                                  [UIView ShowInfo:json[@"message"] Inview:self.view];
                                                                              }
                                                                          } failure:^(NSError *error) {
                                                                              
                                                                          }];
                                                                      }];
                UIAlertAction* cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                                   handler:^(UIAlertAction * action) {
                                                                   }];
                [cancleAction setValue:[UIColor colorWithHexString:@"2C2C2C"] forKey:@"_titleTextColor"];
                
                [alert addAction:copyAction];
                [alert addAction:deleteAction];
                [alert addAction:cancleAction];
                [self presentViewController:alert animated:YES completion:nil];
            } else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:commentmodel.comment_content
                                                                        preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction* replayAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       [self commentReplayButtonClicked:commentmodel];
                                                                   }];
                UIAlertAction* accusationAction = [UIAlertAction actionWithTitle:@"举报"
                                                                        style:UIAlertActionStyleDestructive
                                                                      handler:^(UIAlertAction * action) {
                                                                          NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
                                                                          NSDictionary *parameters = @{
                                                                                                       @"user_id":USER_ID,
                                                                                                       @"from_id":@(commentmodel.comment_id),
                                                                                                       @"type_id":@(accusation_audio_comment)
                                                                                                       };
                                                                          [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                                                                              NSDictionary *json = responseObject;
//                                                                              ZPLog(@"%@",json);
//                                                                              ZPLog(@"%@",json[@"message"]);
                                                                              BOOL success = [json[@"success"] boolValue];
                                                                              if (success) {
                                                                                  [UIView ShowInfo:@"举报成功" Inview:self.view];
                                                                              }
                                                                          } failure:^(NSError *error) {
                                                                              
                                                                          }];
                                                                      }];
                UIAlertAction* cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                                     handler:^(UIAlertAction * action) {
                                                                     }];
                [cancleAction setValue:[UIColor colorWithHexString:@"2C2C2C"] forKey:@"_titleTextColor"];
                
                [alert addAction:replayAction];
                [alert addAction:accusationAction];
                [alert addAction:cancleAction];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
    } else if (indexPath.section == 2 && self.recommenData.count > indexPath.row){
        if (_audio_type == 1) {
            AlbumListModel *albummodel = self.recommenData[indexPath.row];
            AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
            VC.album_id = albummodel.album_id;
            [self.navigationController pushViewController:VC animated:YES];
        } else if(_audio_type == 2){
            BookListModel *bookmodel = self.recommenData[indexPath.row];
            ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
            VC.audio_type = _audio_type;
            VC.target_id = bookmodel.book_id;
            [self.navigationController pushViewController:VC animated:YES];
        } else{
            CourseModel *coursemodel = self.recommenData[indexPath.row];
            if (coursemodel.course_type == 2) {
                ListenStoreDetailViewController *VC = [ListenStoreDetailViewController new];
                VC.audio_type = _audio_type;
                VC.target_id = coursemodel.course_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
}

#pragma -mark- CommentListCellDelegate
/** 回复 */
- (void)commentReplayButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = [DFPlayer shareInstance].currentAudioModel.audio_id;
    VC.comment_type = audio_comment;
    VC.targetCommentmodel = commentmodel;
    [self.navigationController pushViewController:VC animated:YES];
}

/** 查看更多 */
- (void)commentDetailButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = [DFPlayer shareInstance].currentAudioModel.audio_id;
    VC.comment_type = audio_comment;
    [self.navigationController pushViewController:VC animated:YES];
}


#pragma -mark- BottomCommetViewDelegate
//分享
- (void)shareButtonClicked{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _audiomodel.audio_title;
    model.thumbURL = [[DFPlayer shareInstance].currentAudioModel.audio_image_url absoluteString];
    model.webpageUrl = _audiomodel.share_url;
    model.from_id = _audiomodel.audio_id;
    model.targetURL = [_audiomodel.audio_path absoluteString];
    model.descr = [DFPlayer shareInstance].currentAudioModel.fk_parent_title;
    model.accusationType = accusation_type_audio;
    model.shareType = share_audio;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_tableView.contentOffset.y <= 0) {
        _tableView.bounces = NO;
//        ZPLog(@"禁止下拉");
    }
    else if (_tableView.contentOffset.y >= 0){
            _tableView.bounces = YES;
//            ZPLog(@"允许上拉");
        }
}


#pragma mark lazy loading...
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 97.0f;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (BottomCommetView *)bottomView{
    if (_bottomView == nil) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil][2];
        _bottomView.delegate = self;
        __weak __typeof(self) weakSelf = self;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
        _bottomView.LoctionButtonBlock = ^{
            if (_commentData && _commentData.count > 0) {
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                [weakSelf.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            } else if (_recommenData && _recommenData.count > 0){
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
                [weakSelf.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
        };
    }
    return _bottomView;
}



- (void)setupNavigationItem{
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame =CGRectMake(0,0, 60, 44);
    [but setImage:ImageName(@"返回_下") forState:UIControlStateNormal];
    [but addTarget:self action:@selector(Back)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut = [[UIBarButtonItem alloc]initWithCustomView:but];
    self.navigationItem.leftBarButtonItem = barBut;
    
    UIButton *but2 = [UIButton buttonWithType:UIButtonTypeCustom];
    but2.frame =CGRectMake(0,0, 60, 44);
    [but2 setImage:ImageName(@"更多_黑色") forState:UIControlStateNormal];
    [but2 addTarget:self action:@selector(Share)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut2 = [[UIBarButtonItem alloc]initWithCustomView:but2];
    self.navigationItem.rightBarButtonItem = barBut2;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
}


- (void)Share{
    ShareModel *model = [[ShareModel alloc] init];
    if (_audio_type != 3) {
        model.titleStr = _audiomodel.audio_title;
        model.thumbURL = [[DFPlayer shareInstance].currentAudioModel.audio_image_url absoluteString];
        model.webpageUrl = _audiomodel.share_url;
        model.from_id = _audiomodel.audio_id;
        model.targetURL = [_audiomodel.audio_path absoluteString];
        model.accusationType = accusation_type_audio;
        model.descr = [DFPlayer shareInstance].currentAudioModel.fk_parent_title;
        model.shareType = share_audio;
    } else {
        model.titleStr = [DFPlayer shareInstance].currentAudioModel.fk_parent_title;
        model.thumbURL = [[DFPlayer shareInstance].currentAudioModel.audio_image_url absoluteString];
        model.webpageUrl = [DFPlayer shareInstance].currentAudioModel.share_url;
        model.from_id = [DFPlayer shareInstance].currentAudioModel.audio_id;
        model.targetURL = [[DFPlayer shareInstance].currentAudioModel.audio_path absoluteString];
        model.descr = [DFPlayer shareInstance].currentAudioModel.fk_parent_title;
        model.accusationType = accusation_type_audio;
        model.shareType = share_audio;
    }
  
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}

- (void)Back{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (NSMutableArray *)commentData{
    if (_commentData == nil) {
        _commentData = [NSMutableArray array];
    }
    return _commentData;
}


- (NSMutableArray *)recommenData{
    if (_recommenData == nil) {
        _recommenData = [NSMutableArray array];
    }
    return _recommenData;
}


@end
