//
//  teacherHeadView.h
//  mingyu
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeacherModel.h"
#import "TitleNavigationView.h"
@interface teacherHeadView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *user_head;

@property (weak, nonatomic) IBOutlet UILabel *real_name_lab;

@property (weak, nonatomic) IBOutlet UILabel *teacherinfo_lab;

@property (weak, nonatomic) IBOutlet UILabel *playcount_lab;

@property (weak, nonatomic) IBOutlet UILabel *followUserNumber_lab;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint;

@property (weak, nonatomic) IBOutlet UILabel *profile_lab;

@property (weak, nonatomic) IBOutlet UIButton *attention_button;


@property (nonatomic, strong) TeacherModel *teachermodel;

//@property (nonatomic, strong) TitleNavigationView *teachermodel;

@end
