//
//  VideoPlayEndVIew.m
//  mingyu
//
//  Created by apple on 2018/7/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoPlayEndVIew.h"
#import <UShareUI/UShareUI.h>

@implementation VideoPlayEndVIew

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib{
    [super awakeFromNib];
    
//    _shareButton1.frame = CGRectMake(index * (Button_Width + Width_Space) + Start_X, page * (Button_Height + Height_Space)+Start_Y, Button_Width, Button_Height);
    _shareButton1.titleEdgeInsets = UIEdgeInsetsMake(0, -_shareButton1.imageView.frame.size.width, -_shareButton1.imageView.frame.size.height-10, 0);
    //                mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.frame.size.height-10, 0, 0, -mapBtn.titleLabel.frame.size.width);
    // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
    _shareButton1.imageEdgeInsets = UIEdgeInsetsMake(-_shareButton1.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shareButton1.titleLabel.intrinsicContentSize.width);
    
    _shareButton2.titleEdgeInsets = UIEdgeInsetsMake(0, -_shareButton2.imageView.frame.size.width, -_shareButton2.imageView.frame.size.height-10, 0);
    _shareButton2.imageEdgeInsets = UIEdgeInsetsMake(-_shareButton2.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shareButton2.titleLabel.intrinsicContentSize.width);

    _shareButton3.titleEdgeInsets = UIEdgeInsetsMake(0, -_shareButton3.imageView.frame.size.width, -_shareButton3.imageView.frame.size.height-10, 0);
    _shareButton3.imageEdgeInsets = UIEdgeInsetsMake(-_shareButton3.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shareButton3.titleLabel.intrinsicContentSize.width);
    
    _shareButton4.titleEdgeInsets = UIEdgeInsetsMake(0, -_shareButton4.imageView.frame.size.width, -_shareButton4.imageView.frame.size.height-10, 0);
    _shareButton4.imageEdgeInsets = UIEdgeInsetsMake(-_shareButton4.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shareButton4.titleLabel.intrinsicContentSize.width);
    
    _shareButton5.titleEdgeInsets = UIEdgeInsetsMake(0, -_shareButton5.imageView.frame.size.width, -_shareButton5.imageView.frame.size.height-10, 0);
    _shareButton5.imageEdgeInsets = UIEdgeInsetsMake(-_shareButton5.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shareButton5.titleLabel.intrinsicContentSize.width);
    
    _jubaoButton.titleEdgeInsets = UIEdgeInsetsMake(0, -_jubaoButton.imageView.frame.size.width, -_jubaoButton.imageView.frame.size.height-10, 0);
    _jubaoButton.imageEdgeInsets = UIEdgeInsetsMake(-_jubaoButton.titleLabel.intrinsicContentSize.height-10, 0, 0, -_jubaoButton.titleLabel.intrinsicContentSize.width);
}


- (void)setSharemodel:(ShareModel *)sharemodel{
    _sharemodel = sharemodel;
}


- (IBAction)playAgain:(UIButton *)sender {
    if (self.playAgainBlock) {
        self.playAgainBlock();
    }
}


- (IBAction)shareButton:(UIButton *)sender {
    [self cancleButton:nil];
    UMSocialPlatformType type;
    if (sender.tag == 1001) {
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatTimeLine]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"未安装微信" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        type = UMSocialPlatformType_WechatTimeLine;
    } else if (sender.tag == 1002){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"未安装微信" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        type = UMSocialPlatformType_WechatSession;
    }else if (sender.tag == 1003){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"未安装QQ" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        type = UMSocialPlatformType_QQ;
    }else if (sender.tag == 1004){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Qzone]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"未安装QQ" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        type = UMSocialPlatformType_Qzone;
    }else if (sender.tag == 1005){
        if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"未安装微博" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        type = UMSocialPlatformType_Sina;
    } else {
        type = UMSocialPlatformType_UnKnown;

    }

    
    [self shareMessage:type];
}


- (IBAction)jubaoButton:(UIButton *)sender {
    [self cancleButton:nil];
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"from_id":@(_sharemodel.from_id),
                                 @"type_id":@(_sharemodel.accusationType)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
            MBProgressHUD *progress = [[MBProgressHUD alloc] initWithView:self.superview];
            [self.superview addSubview:progress];
            progress.labelText = @"举报成功";
            progress.mode = MBProgressHUDModeText;
            [progress showAnimated:YES whileExecutingBlock:^{
                sleep(1);
            } completionBlock:^{
                [progress removeFromSuperview];
            }];
        }
    } failure:^(NSError *error) {
        
    }];
}


- (IBAction)cancleButton:(UIButton *)sender {
    if (self.cancleShareBlock) {
        self.cancleShareBlock();
    }
}

- (void)shareMessage:(UMSocialPlatformType )type{
    if (type != UMSocialPlatformType_UnKnown) {
        
        UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
        //创建网页内容对象
        
        NSURL *thumbURL = [NSURL URLWithString:self.sharemodel.thumbURL];
        NSData * data = [NSData dataWithContentsOfURL:thumbURL];
        NSString *title = [NSString string];
        if (type == UMSocialPlatformType_Sina) {
            title = [NSString stringWithFormat:@"%@ @名育APP",self.sharemodel.titleStr];
        } else {
            title = self.sharemodel.titleStr;
        }
        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:self.sharemodel.descr.length > 0?self.sharemodel.descr:@"分享自「名育」" thumImage:data];

        //设置网页地址
        shareObject.webpageUrl = self.sharemodel.webpageUrl;
        //分享消息对象设置分享内容对象
        messageObject.shareObject = shareObject;
        
        //调用分享接口
        [[UMSocialManager defaultManager] shareToPlatform:type messageObject:messageObject currentViewController:[self viewController] completion:^(id data, NSError *error) {
            if (error) {
//                [self hidden];
//                [self shAlertViewWithTitle:@"分享失败"];
                ZPLog(@"************Share fail with error %@*********",error);
            }else{
                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                    UMSocialShareResponse *resp = data;
                    //分享结果消息
                    ZPLog(@"response message is %@",resp.message);
                    //第三方原始返回的数据
                    ZPLog(@"response originalResponse data is %@",resp.originalResponse);
//                    [self hidden];
//                    [self shAlertViewWithTitle:@"分享成功"];
                    [self addShareLog];
                }else{
                    ZPLog(@"response data is %@",data);
                }
            }
        }];
    }
}

//用户每日第一次分享
- (void)addShareLog{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KFirstShare];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resource_id":@(_sharemodel.from_id),
                                 @"resource_type":@(_sharemodel.shareType)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
        } else {
            
        }
    } failure:^(NSError *error) {
        
    }];
}



@end
