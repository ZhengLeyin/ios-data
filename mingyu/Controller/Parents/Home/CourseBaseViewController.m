
//
//  CourseBaseViewController.m
//  mingyu
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CourseBaseViewController.h"
#import "CourseVideoViewController.h"
#import "CourseAudioViewController.h"
#import "ExcellentClassModel.h"
#define pageMenuH 50

@interface CourseBaseViewController ()<UITableViewDelegate,UITableViewDataSource,SPPageMenuDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger age_group;  //年龄段(0不限定，1: 0-3岁，2: 3-6，3: 7-10, 4:小升初)

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) NSMutableArray *arrayPageMenuData;

@property (nonatomic, strong) NSMutableArray *arrayPageMenuIdData;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, assign) NSInteger level;

@end

@implementation CourseBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.headerView];
    [self.headerView addSubview:self.pageMenu];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(CGRectGetMaxY(_headerView.frame), 0, 0, 0));
    }];
    
    [self getChildMenu];
 
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
}

//刷新
-(void)refreshTableview {
    if (self.arrayData.count > 0) {
        [self.arrayData removeAllObjects];
    }
     _start = 0;
     [self getCourseMenuList];  //加载cell数据
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getCourseMenuList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
}

/*1.根据一级目录 self.meunId 来获取二级目录标签
 *2.level 二级目录 全部传入1  除了全部意外level==2
 *3.二级目录标签没有,二级目录PageMenu 隐藏
 */
#pragma mark 获取子标签
-(void)getChildMenu {
    NSString  *childMenuURL = @"/childMenu";
    NSString *urlString = [NSString stringWithFormat:@"%@%@/%@%@",KURL,KChildMenu,@(self.meunId),childMenuURL];
    [HttpRequest getWithURLString:urlString parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        BOOL success = [json[@"success"] boolValue];
        [self.arrayPageMenuIdData removeAllObjects];
        [self.arrayPageMenuData removeAllObjects];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                ExcellentClassModel *model = [ExcellentClassModel modelWithJSON:dic];
                [self.arrayPageMenuData addObject:model.menu_name];
                [self.arrayPageMenuIdData addObject:model];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
        if (_arrayPageMenuData.count > 0) {
            _pageMenu.height = 50;
            _pageMenu.hidden = NO;
            [_pageMenu setItems:_arrayPageMenuData selectedItemIndex:0];
        }else {
            _pageMenu.height = 0;
            _pageMenu.hidden = YES;
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                  make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
            /**重新请求*/
            _start = 0;
            [self refreshTableview];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma mark 精品课id
- (void)getCourseMenuList {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KCourseMenuId,@(_meunId)];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"level":@(_level),
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        if (_start == 0) {
             [self.arrayData removeAllObjects];
        }
        MingYuLoggerI(@"%@ \n%@",json,json[@"message"]);
//        _MingYuLoggerInfo(MingYuLoggerDomainNetwork,@"哈哈");
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 20;
            NSArray *data = json[@"data"];
            if (data && data.count == 20) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                CourseModel *model = [CourseModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            [_tableView reloadData];
        
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_arrayData && _arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

#pragma -mark- SPPageMenuDelegate
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedAtIndex:(NSInteger)index{
     //ZPLog(@"index==%ld",index);
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    if (self.arrayPageMenuIdData.count > 0) {
        //MingYuLoggerD(@"index%ld",index);
        ExcellentClassModel *model = self.arrayPageMenuIdData[index];
        _meunId = model.menu_id;
        _start = 0;
        if (_index == 0) {
            _level = 1;
        }else {
            _level = 2;
        }
        [self refreshTableview];
    }
  
   [self.tableView setContentOffset:CGPointMake(0,0) animated:NO];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        CourseModel *model = self.arrayData[indexPath.row];
        cell.coursemodel = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}
 
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData && _arrayData.count > indexPath.row) {
        CourseModel *model = [self.arrayData objectAtIndex:indexPath.row];
        if (model.course_type == 1) {
            CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
            VC.coursemodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else{
//            ListenStoreDetailViewController *VC = [[ListenStoreDetailViewController alloc] init];
//            VC.target_id = model.course_id;
//            VC.audio_type = 3;
            CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
            VC.coursemodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY > 10.0) {
        /**通知中心 --banner收缩*/
        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationShrinkNotification object:nil];
    }else if (offsetY < - 10.0) {
        /**通知中心 --banner扩张*/
        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationExpandNotification object:nil];
    }else {
        
    }
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 155;
    }
    return _tableView;
}

-(UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, pageMenuH)];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

- (SPPageMenu *)pageMenu {
    if (_pageMenu == nil) {
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, pageMenuH) trackerStyle:SPPageMenuTrackerStyleRoundedRect];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(14);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"767676"];
        //_pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"FFF5F7"];
        _pageMenu.tracker.backgroundColor = [UIColor whiteColor];
        _pageMenu.tracker.layer.borderWidth = 1;
        _pageMenu.tracker.layer.borderColor = [UIColor colorWithHexString:@"53D1F5"].CGColor;
        _pageMenu.permutationWay = SPPageMenuPermutationWayScrollAdaptContent;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}

- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

-(NSMutableArray *)arrayPageMenuData {
    if (_arrayPageMenuData == nil) {
        _arrayPageMenuData = [NSMutableArray array];
    }
    return _arrayPageMenuData;
}

-(NSMutableArray *)arrayPageMenuIdData {
    if (_arrayPageMenuIdData == nil) {
        _arrayPageMenuIdData = [NSMutableArray array];
    }
    return _arrayPageMenuIdData;
}
@end
