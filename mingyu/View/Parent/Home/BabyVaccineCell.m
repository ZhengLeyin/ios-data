//
//  BabyVaccineCell.m
//  mingyu
//
//  Created by apple on 2018/6/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BabyVaccineCell.h"

@implementation BabyVaccineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_must_lab.bounds cornerRadius:12];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _must_lab.bounds;
    maksLayer.path = maksPath.CGPath;
    _must_lab.layer.mask = maksLayer;
}


+ (instancetype)theBabyVaccineCellWithtableView:(UITableView *)tableview{
    static NSString *cellid = @"BabyVaccineCell";
    BabyVaccineCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"BabyVaccineCell" owner:nil options:nil][0];
    }
    return cell;
}


- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    _title_lab.text = [NSString stringWithFormat:@"%@(%@)",dic[@"title"],dic[@"count"]];
    _function_button.text = dic[@"function"];
    if ([dic[@"must"]  isEqual: @"必打"]) {
        _must_lab.hidden = NO;
    } else {
        _must_lab.hidden = YES;
    }
    
    if ([dic[@"down"]  isEqual: @"1"]) {
        [_chooseButton setImage:ImageName(@"choose_yellow") forState:UIControlStateNormal];
    } else {
        [_chooseButton setImage:ImageName(@"weixuan") forState:UIControlStateNormal];
    }
}


- (IBAction)ChooseButton:(id)sender {
    if (self.chooseButtonBlock) {
        if ([_dic[@"down"]  isEqual: @"1"]) {
            [_chooseButton setImage:ImageName(@"weixuan") forState:UIControlStateNormal];
        } else {
            [_chooseButton setImage:ImageName(@"choose_yellow") forState:UIControlStateNormal];
        }
        self.chooseButtonBlock();
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
