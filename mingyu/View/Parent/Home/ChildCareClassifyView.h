//
//  ChildCareClassifyView.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LabelListModel;

@interface ChildCareClassifyView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *ageArray;

@property (nonatomic, copy) void (^chooseItemBlock)(NSInteger index,NSString *agesection);

//@property (nonatomic, copy) void (^chooseItemBlock)(LabelListModel *model);

//@property (nonatomic, strong) NSMutableArray *classifyArray;
//@property (nonatomic, strong) NSMutableArray *labelArray;


@end
