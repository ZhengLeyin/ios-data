//
//  ParentVideoDetailHeaderView.h
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VideoModel;

@interface ParentVideoDetailHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *guanzhu_button;

@property (weak, nonatomic) IBOutlet UIButton *user_icon_button;

@property (weak, nonatomic) IBOutlet UILabel *user_name_lab;

@property (weak, nonatomic) IBOutlet UILabel *guanzhu_count_lab;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UIButton *more_button;

@property (weak, nonatomic) IBOutlet UILabel *abstract_lab;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;

@property (weak, nonatomic) IBOutlet UILabel *source_lab;

@property (weak, nonatomic) IBOutlet UIButton *zan_button;

@property (weak, nonatomic) IBOutlet UIButton *share_button;

@property (nonatomic, strong) VideoModel *videomodel;


@property (nonatomic, copy) void (^showAbstractBlock)(void);


+ (instancetype)haveUserVideoDetailHeaderView;

+ (instancetype)noUserVideoDetailHeaderView;

@end
