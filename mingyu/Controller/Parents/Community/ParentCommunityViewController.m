//
//  ParentCommunityViewController.m
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "ParentCommunityViewController.h"
#import "ParentNewCircleViewController.h"


@interface ParentCommunityViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger PageMenuIndex;

@end

@implementation ParentCommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];

    UIView *titleView = [[UIView alloc] init];
    titleView.frame = CGRectMake((kScreenWidth-200)/2, 0, 200, 44);
    self.navigationItem.titleView = titleView;
    [titleView addSubview:self.pageMenu];
    ParentAtentionViewController *AtentionVC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentAtentionViewController"];
    [self addChildViewController:AtentionVC];
    ParentRecommendViewController *RecommendVC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentRecommendViewController"];
    [self addChildViewController:RecommendVC];
//    ParentCircleViewController *CircleVC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentCircleViewController"];
    ParentNewCircleViewController *CircleVC = [[ParentNewCircleViewController alloc] init];
    [self addChildViewController:CircleVC];
    
    
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-TabbarH);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePageMenuItem) name:NSNotificationSelectIndex1 object:self];
}

- (void)changePageMenuItem{
    [self.pageMenu setSelectedItemIndex:2];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];

}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
    if (toIndex == 1) {
        [_rightButton setImage:ImageName(@"刷新") forState:UIControlStateNormal];
    } else{
        [_rightButton setImage:ImageName(@"发帖") forState:UIControlStateNormal];
    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-TabbarH);
    [_scrollView addSubview:VC.view];
}



- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"关注",@"头条",@"圈子"];
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0, 200, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.itemPadding = 20;
        _pageMenu.backgroundColor = [UIColor clearColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:1];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(17);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}

- (IBAction)searchButton:(id)sender {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchAll_type;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)RightButton:(id)sender {
    if (_PageMenuIndex == 1) {
        ParentRecommendViewController *VC = self.childViewControllers[1];
        [VC refreshTableview];
    } else{
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        } else {
            TopicEditorViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"TopicEditorViewController"];
    //        VC.circle_id = 1;
            [self.navigationController pushViewController:VC animated:YES];
        }

    }
}


// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}


@end
