//
//  MineFansViewController.h
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineFansViewController : UIViewController

/** type */
@property (nonatomic, assign) NSUInteger  type;

@property (nonatomic, assign) NSInteger from_id;

@property (nonatomic, copy) void (^PushBlock) (UIViewController *vc);

@end
