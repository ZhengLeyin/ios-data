//
//  PlayerDetailPopView.h
//  mingyu
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCDownloadItem.h"

@interface PlayerDetailPopView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *arrayData;


@property (nonatomic, copy) void (^clooseBlock)(void);

@property (nonatomic, copy) void (^chooseCell)(NSInteger index);

/**
 滚动到当前播放的音频
 */
- (void)scrollToCurrentAudio;

@end




@interface PlayerListCell : UITableViewCell <YCDownloadItemDelegate>

@property (nonatomic, strong) UIImageView *playImage;

@property (nonatomic, strong) UILabel *audio_title_lab;

@property (nonatomic, strong) UIButton *down_button;

@property (nonatomic, strong) AudioModel *audiomodel;

- (void)setDownloadStatus:(YCDownloadStatus)status;


@end




