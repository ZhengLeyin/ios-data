//
//  QuickListenViewController.m
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "QuickListenViewController.h"
#import "RotateButton.h"

@interface QuickListenViewController ()<SPPageMenuDelegate, UIScrollViewDelegate,DFPlayerDelegate>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) RotateButton *playbutton;

@end

@implementation QuickListenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupNavigationItem];
    
    UIView *titleView = [[UIView alloc] init];
    titleView.frame = CGRectMake(0, 0, 150, 44);
    self.navigationItem.titleView = titleView;
    [titleView addSubview:self.pageMenu];
    
   
    HandPickViewController *VC1 = [HandPickViewController  new];
    ListenStoryViewController *VC2 = [ListenStoryViewController new];
    [self addChildViewController:VC1];
    [self addChildViewController:VC2];
    
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];


    [DFPlayer shareInstance].delegate  = self;
//    [DFPlayer shareInstance].dataSource  = self;
    
    ZPLog(@"%ld",(long)[DFPlayer shareInstance].state);
    if (![DFPlayer shareInstance].currentAudioModel) {
        [[DFPlayer shareInstance] df_setPlayerWithPreviousAudioModel];
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].previousAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
        
    } else{
        [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
    }
    
    if ( [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [_playbutton startRotating];
        [_playbutton resumeRotate];
    } else {
        [_playbutton stopRotating];
    }
}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}



- (void)df_playerReadyToPlay:(DFPlayer *)player{
    [self.playbutton sd_setImageWithURL:[DFPlayer shareInstance].currentAudioModel.audio_image_url forState:UIControlStateNormal placeholderImage:nil];
}


- (NSMutableArray *)dataArray{

    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"archive"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    
    NSString *entityArchive = [strLib stringByAppendingPathComponent:@"entityArchive"];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    return array;
}




- (void)setupNavigationItem{
    UIButton *but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame =CGRectMake(0,0, 60, 44);
    [but setImage:ImageName(@"back_black") forState:UIControlStateNormal];
    [but addTarget:self action:@selector(Back)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem  *barBut = [[UIBarButtonItem alloc]initWithCustomView:but];
    self.navigationItem.leftBarButtonItem = barBut;
}

- (void)Back{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }

    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH);
    [_scrollView addSubview:VC.view];
}



- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"精选",@"听书"];
        
        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  150, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
//        _pageMenu.center = _pageMenu.superview.center;
        _pageMenu.backgroundColor = [UIColor clearColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:0];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
        _pageMenu.dividingLine.hidden = YES;
    }
    return _pageMenu;
}


- (RotateButton *)playbutton{
    if (_playbutton == nil) {
        _playbutton = [RotateButton buttonWithType:UIButtonTypeCustom];
        _playbutton.frame = CGRectMake(kScreenWidth-85, kScreenHeight-effectViewH-85, 65, 65);
        [_playbutton setImage:ImageName(@"一键听_banner") forState:UIControlStateNormal];
        [_playbutton addTarget:self action:@selector(presentDetail) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_playbutton];
        
    }
    return _playbutton;
}


- (void)presentDetail{
   
    AudioPlayerViewController *PVC = [[AudioPlayerViewController alloc] init];
    UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
    
//    if (audio_type == 1) {
//        PlayerDetailViewController *PVC = [[PlayerDetailViewController alloc] init];
//        UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
//        [self.navigationController presentViewController:VC animated:YES completion:nil];
//    } else{
//        LSPlayerDetailViewController *PVC = [[LSPlayerDetailViewController alloc] init];
//        UINavigationController *VC = [[UINavigationController alloc] initWithRootViewController:PVC];
//        [self.navigationController presentViewController:VC animated:YES completion:nil];
//    }
}



@end
