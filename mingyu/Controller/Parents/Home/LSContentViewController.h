//
//  LSContentViewController.h
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"

@interface LSContentViewController : UIViewController<JXPagerViewListViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *audioListArray;

@property (nonatomic, copy) void (^tableviewSelectIndex)(NSInteger index);

/** type 0 为需要支付  1 不需要支付 */
@property (nonatomic, assign) NSUInteger  type;


@end
