//
//  MinePublicViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MinePublicViewController.h"

@interface MinePublicViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, strong) NothingView *nothingView;


@end

@implementation MinePublicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self refreshTableview];
    
}




- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
}


//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.arrayData.count>0) {
            [self.arrayData removeAllObjects];
            _start = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self getTopicList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getTopicList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


- (void)getTopicList{
    NSString *URL = [NSString string];
    if (_user_id) {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserTopicList];
        NSDictionary *parameters = @{
                                     @"user_id":@(_user_id),
                                     @"start":@(_start),
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    } else {
        URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserTopicList];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"start":@(_start),
                                     @"size":@"10"
                                     };
        URL = [NSString connectUrl:parameters url:URL];
    }
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSArray *data = json[@"data"];
            if (data && data.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            
            if (self.arrayData && self.arrayData.count > 0) {
                self.nothingView.hidden = YES;
            } else{
                self.nothingView.hidden = NO;
            }
            
            [self.tableView reloadData];

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        if (self.arrayData && self.arrayData .count > 0) {
            self.nothingView.hidden = YES;
        } else{
            self.nothingView.hidden = NO;
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
    if (self.arrayData && self.arrayData.count > 0) {
//        cell.TopLab.hidden = YES;
        cell.newsmodel = [self.arrayData objectAtIndex:indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData.count > indexPath.row) {
        ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
        NewsModel *model = [_arrayData objectAtIndex:indexPath.row];
        VC.newsmodel = model;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *view = [UIView new];
        view.frame = CGRectMake(0, 0, kScreenWidth, 30);
        _tableView.tableFooterView = view;
        
    }
    return _tableView;
}

- (NothingView *)nothingView{
    if (!_nothingView) {
        _nothingView = [[NSBundle mainBundle] loadNibNamed:@"NothingView" owner:nil options:nil][0];
        _nothingView.frame = self.view.bounds;
        _nothingView.tipLab.text = @"暂无帖子";
        _nothingView.imageView.image = ImageName(@"资讯 copy");
        [self.view addSubview:_nothingView];
    }
    return _nothingView;
}


@end
