//
//  AlbumDetailCell.h
//  mingyu
//
//  Created by apple on 2018/5/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCDownloadItem.h"
@class AudioModel;

@interface AlbumDetailCell : UITableViewCell<YCDownloadItemDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *album_image;
@property (weak, nonatomic) IBOutlet UIView *maskView;

@property (weak, nonatomic) IBOutlet UILabel *album_title_lab;

@property (weak, nonatomic) IBOutlet UILabel *album_play_lab;

@property (weak, nonatomic) IBOutlet UIButton *album_play_button;




@property (weak, nonatomic) IBOutlet UILabel *audio_title_lab;

@property (weak, nonatomic) IBOutlet UIButton *play_count_button;

@property (weak, nonatomic) IBOutlet UIButton *time_length_button;

@property (weak, nonatomic) IBOutlet UIButton *download_button;

@property (weak, nonatomic) IBOutlet UIImageView *isplay_image;

@property (nonatomic, strong) AudioModel *audiomodel;

- (void)setDownloadStatus:(YCDownloadStatus)status;


@property (nonatomic, copy) void (^playBlockButtonBlock)(void);



+ (instancetype)theHeaderView;

+ (instancetype)theCellWithTableView:(UITableView *)tableView;




@end
