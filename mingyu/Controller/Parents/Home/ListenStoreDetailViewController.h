//
//  ListenStoreDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LWXDetailController.h"
@class BookListModel,CourseModel;

@interface ListenStoreDetailViewController : LWXDetailController



@property (nonatomic, assign) NSInteger target_id;

@property (nonatomic, assign) NSInteger audio_type;



//- (void)initWithaudiotype:(NSInteger )audio_type targetid:(NSInteger )target_id bookmodel:(BookListModel *)bookmodel;
//
//- (void)initWithaudiotype:(NSInteger )audio_type targetid:(NSInteger )target_id coursemodel:(CourseModel *)coursemodel;


@end
