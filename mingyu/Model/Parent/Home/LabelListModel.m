//
//  LabelListModel.m
//  mingyu
//
//  Created by apple on 2018/5/5.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LabelListModel.h"

@implementation LabelListModel

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.label_id = [aDecoder decodeIntegerForKey:@"label_id"];
        self.label_name = [aDecoder decodeObjectForKey:@"label_name"];
        self.min_time = [aDecoder decodeObjectForKey:@"min_time"];
        self.max_time = [aDecoder decodeObjectForKey:@"max_time"];
        self.last_page = [aDecoder decodeBoolForKey:@"last_page"];
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeInteger:self.label_id forKey:@"label_id"];
    [aCoder encodeObject:self.label_name forKey:@"label_name"];
    [aCoder encodeObject:self.min_time forKey:@"min_time"];
    [aCoder encodeObject:self.max_time forKey:@"max_time"];
    [aCoder encodeBool:self.last_page forKey:@"last_page"];
}


@end


@implementation ChildCareClassifyModel

@end
