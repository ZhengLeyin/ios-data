//
//  MineCommentCell.m
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineCommentCell.h"

@implementation MineCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (instancetype)theMineCommentCellWithtableView:(UITableView *)tableview{
    static NSString *cellid = @"MineCommentCell";
    MineCommentCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"MineCommentCell" owner:nil options:nil][0];
    }
    return cell;
}

- (void)setModel:(MyCommentMessageModel *)model{
    
    _model = model;
    if (model.user_name) {
        NSString *contentStr = [NSString stringWithFormat:@"我回复%@：%@",_model.user_name,_model.message];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"53D1F5"] range:NSMakeRange(0,1)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"53D1F5"] range:NSMakeRange(3,_model.user_name.length)];
        _content_lab.attributedText = str;
    } else {
//        NSString *contentStr = [NSString stringWithFormat:@"我评论：%@",_model.message];
//        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
//        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"53D1F5"] range:NSMakeRange(0,1)];
//        _content_lab.attributedText = str;
        _content_lab.text = _model.message;
    }

    _title_lab.text = _model.title;
    _time_lab.text = _model.time_status;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
