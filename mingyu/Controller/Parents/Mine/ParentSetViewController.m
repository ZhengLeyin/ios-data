//
//  ParentSetViewController.m
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentSetViewController.h"
#import <UMPush/UMessage.h>

@interface ParentSetViewController ()

@property (weak, nonatomic) IBOutlet UILabel *passworldLab;

@property (weak, nonatomic) IBOutlet UILabel *cacheLab;

@property (weak, nonatomic) IBOutlet UISwitch *qiandaoSwitch;

@property (weak, nonatomic) IBOutlet UISwitch *messageSwitch;

@property (weak, nonatomic) IBOutlet UILabel *versionLab;

@property (weak, nonatomic) IBOutlet UIButton *loginoutButton;

@end

@implementation ParentSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    CGFloat size = [self folderSizeAtPath:NSTemporaryDirectory()];
    _versionLab.text = [[[NSBundle mainBundle]infoDictionary]valueForKey:@"CFBundleShortVersionString"];
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"default"];
    CGFloat size = [self folderSizeAtPath: path];
    _cacheLab.text = [NSString stringWithFormat:@"%.2fMB",size];
    
    _qiandaoSwitch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放
    _messageSwitch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放

    [self getUserRemind];
    
    if (_is_true_password) {
        _passworldLab.text = @"修改密码";
    } else {
        _passworldLab.text = @"设置密码";
    }
    if (![userDefault boolForKey:KUDhasLogin]) {
        _loginoutButton.hidden = YES;
    } else {
        _loginoutButton.hidden = NO;
    }
}


- (void)getUserRemind{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserRemind];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"type":@"1"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            BOOL data = [json[@"data"] boolValue];
            [_qiandaoSwitch setOn:data];
        }
    } failure:^(NSError *error) {
        
    }];
}


- (IBAction)chagePasswordButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (_is_true_password) {
        ChangePasswordViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
        [self.navigationController pushViewController:VC animated:YES];
    } else {
        SetPasswordViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"SetPasswordViewController"];
        VC.title = @"设置密码";
        [self.navigationController pushViewController:VC animated:YES];
    }
}

- (IBAction)removeCacheButton:(id)sender {
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"default"];
    CGFloat size = [self folderSizeAtPath:path];
    NSString *message = size > 1 ? [NSString stringWithFormat:@"缓存%.2fMB, 删除缓存\n是否清除缓存?", size] : [NSString stringWithFormat:@"缓存%.2fKB, 删除缓存", size * 1024.0];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self cleanCaches:path];
        _cacheLab.text = [NSString stringWithFormat:@"0.00MB"];

    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    // Add the actions.
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:sureAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark- 计算缓存大小
// 计算目录大小
- (CGFloat)folderSizeAtPath:(NSString *)path{
    // 利用NSFileManager实现对文件的管理
    NSFileManager *manager = [NSFileManager defaultManager];
    CGFloat size = 0;
    if ([manager fileExistsAtPath:path]) {
        // 获取该目录下的文件，计算其大小
        NSArray *childrenFile = [manager subpathsAtPath:path];
        for (NSString *fileName in childrenFile) {
            NSString *absolutePath = [path stringByAppendingPathComponent:fileName];
            size += [manager attributesOfItemAtPath:absolutePath error:nil].fileSize;
        }
        // 将大小转化为M
        return size / 1024.0 / 1024.0;
    }
    return 0;
}

// 根据路径删除文件
- (void)cleanCaches:(NSString *)path{
    // 利用NSFileManager实现对文件的管理
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        // 获取该路径下面的文件名
        NSArray *childrenFiles = [fileManager subpathsAtPath:path];
        for (NSString *fileName in childrenFiles) {
            // 拼接路径
            NSString *absolutePath = [path stringByAppendingPathComponent:fileName];
            // 将文件删除
            [fileManager removeItemAtPath:absolutePath error:nil];
        }
    }
}


- (IBAction)qiandao:(UISwitch *)sender {
    
    if (!sender.on) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"关闭签到提醒？"
                                                                       message:@"\n关闭后，手机将不再收到签到提醒，\n当心错过积分哦~～\n"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                 [sender setOn:YES];
//                                                                 [userDefault setBool:NO forKey:KUDQianDaoSwitch];
                                                                 
                                                             }];
        [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
        
        UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //响应事件
                                                                  ZPLog(@"关闭签到提醒");
                                                                  [self ChangeUserRemindOn:NO];
//                                                                  [userDefault setBool:YES forKey:KUDQianDaoSwitch];
                                                              }];
        [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
        
        [alert addAction:cancelAction];
        [alert addAction:confirmAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else{
        ZPLog(@"打开签到提醒");
        [self ChangeUserRemindOn:YES];
    }
}


- (void)ChangeUserRemindOn:(BOOL)on{
    if (on) { //开启
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"1"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_qiandaoSwitch setOn:YES];
                //添加标签
                [UMessage addTags:@"签到" response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
                    
                }];
                
            } else {
                [_qiandaoSwitch setOn:NO];
            }
        } failure:^(NSError *error) {
            [_qiandaoSwitch setOn:NO];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelUserRemind];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"type":@"1"
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_qiandaoSwitch setOn:NO];
                //删除标签
                [UMessage deleteTags:@"签到" response:^(id  _Nonnull responseObject, NSInteger remain, NSError * _Nonnull error) {
                    
                }];
            } else{
                [_qiandaoSwitch setOn:YES];
            }
        } failure:^(NSError *error) {
            [_qiandaoSwitch setOn:YES];
        }];
    }
}



- (IBAction)messageSwitch:(UISwitch *)sender {
    
    
}


- (IBAction)LoginOutButton:(id)sender {
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KSignOut];
    [HttpRequest postWithURLString:URL parameters:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            [userDefault setObject:nil forKey:KUDtoken];
            [userDefault setObject:nil forKey:KUDuser_name];
            //    [userDefault setObject:nil forKey:KUDuser_phone];
            [userDefault setObject:nil forKey:KUDuser_password];
            //    [userDefault setObject:nil forKey:KUDuser_id];
            [userDefault setObject:nil forKey:KUDuser_head];
            [userDefault setInteger:0 forKey:KUDfollow_number];
            [userDefault setObject:nil forKey:KUDbabyhead];
            [userDefault setBool:NO forKey:KUDhasLogin];
            [userDefault synchronize];
            
            NSString *alias = [NSString stringWithFormat:@"mingyu0"];
            ZPLog(@"%@",alias);
            [UMessage setAlias:alias type:@"UMENGTEST" response:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        
    }];
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
