//
//  RateListView.m
//  mingyu
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "RateListView.h"

@implementation RateListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self updataUI];
    }
    return self;
}


- (void)updataUI{
   
    self.backgroundColor = [UIColor whiteColor];
    
    
    _textArrayData = @[@"X0.5倍",@"X1倍（正常倍数）",@"X1.25倍",@"X1.5倍",@"X1.75倍",@"X2倍"];

    _rateArrayData = @[@0.5, @1.0, @1.25, @1.5,@1.75, @2.0];
    
    [self addSubview:self.tableView];
    [self addSubview:self.clooseButton];

}



#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _textArrayData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
//    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
//    }
    cell.textLabel.text = self.textArrayData[indexPath.row];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth-35,0, 17, 45)];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:imageview];
    CGFloat rate = [_rateArrayData[indexPath.row] floatValue];
    if ([DFPlayer shareInstance].ratevalue == rate) {
        imageview.image = ImageName(@"一键听_xuanz_gou");
    } else{
        imageview.image = ImageName(@"一键听_xuanz");
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [DFPlayer shareInstance].ratevalue = [_rateArrayData[indexPath.row] floatValue];
    [[DFPlayer shareInstance] df_audioPlay];
    [_tableView reloadData];
}




- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, self.height-50) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 45;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


- (UIButton *)clooseButton{
    if (_clooseButton == nil) {
        _clooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _clooseButton.frame = CGRectMake(0, self.height-50, kScreenWidth, 50);
        [_clooseButton setBackgroundColor:[UIColor whiteColor]];
        [_clooseButton setTitle:@"关闭" forState:UIControlStateNormal];
        _clooseButton.titleLabel.font = FontSize(15);
        [_clooseButton setTitleColor:[UIColor colorWithHexString:@"2C2C2C"] forState:UIControlStateNormal];
        [_clooseButton addTarget:self action:@selector(clooseButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _clooseButton;
}

- (void)clooseButtonClicked{
    if (self.clooseBlock) {
        self.clooseBlock();
    }
}


@end
