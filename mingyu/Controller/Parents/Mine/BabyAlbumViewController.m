//
//  BabyAlbumViewController.m
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BabyAlbumViewController.h"
#import "YearPhotoViewController.h"
#import "DiaryModel.h"

@interface BabyAlbumViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, strong) NSMutableArray *headerArray;


@end

@implementation BabyAlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:NSNotificationDeletePhotoRecord object:nil];

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-effectViewH);

        
    }];
    
    [self getYearMonthList];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.alpha = 1;

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationDeletePhotoRecord object:self];
}


- (void)refresh{
    [self.arrayData removeAllObjects];
    [self.headerArray removeAllObjects];
    [self getYearMonthList];
}

- (void)getYearMonthList {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetYearMonthList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(0),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                DiaryModel *model = [DiaryModel modelWithJSON:dic];
                if (model.year) {
                    [self.headerArray addObject:model.year];
                    NSMutableArray *array = [NSMutableArray array];
                    for (NSDictionary *dict in model.messageList) {
                        DiaryPhotoModel *photomodel = [DiaryPhotoModel modelWithJSON:dict];
                        [array addObject:photomodel];
                    }
                    [self.arrayData addObject:array];
                }
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.arrayData && self.arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > section) {
        return [self.arrayData[section] count];
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UILabel *label = [[UILabel alloc] init];
    label.tag = 101;
    label.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
    label.textColor = [UIColor colorWithHexString:@"2C2C2C"];
    label.font = FontSize(12);
    if (self.headerArray && self.headerArray.count > section) {
        label.text = [NSString stringWithFormat:@"     %@年",self.headerArray[section]];
    }
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BabyAlbumCell *cell = [BabyAlbumCell theBabyAlbumCellCellWithTableView:tableView];
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        DiaryPhotoModel *photomodel = self.arrayData[indexPath.section][indexPath.row];
        cell.model = photomodel;
    }
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        DiaryPhotoModel *photomodel = self.arrayData[indexPath.section][indexPath.row];
        YearPhotoViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"YearPhotoViewController"];
        VC.photomodel = photomodel;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 114;
        
        
    }
    return _tableView;
}


- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (NSMutableArray *)headerArray{
    if(!_headerArray){
        _headerArray = [NSMutableArray array];
    }
    return _headerArray;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end



@implementation BabyAlbumCell


+ (instancetype)theBabyAlbumCellCellWithTableView:(UITableView *)tableView{
    static NSString *cellid = @"MessageDetailCell";
    BabyAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[BabyAlbumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createView];
    }
    return self;
}

- (void)createView{
//    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 93, 93)];
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 93, 93)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
//    imageView.image = ImageName(@"pic");
    [self.contentView addSubview:imageView];
    
    mouthLab = [[UILabel alloc] initWithFrame:CGRectMake(15*2+93, 35, 100, 25)];
    mouthLab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
//    mouthLab.text = @"05月";
    mouthLab.font = FontSize(18);
    [self.contentView addSubview:mouthLab];
    
    totleLab = [[UILabel alloc] initWithFrame:CGRectMake(15*2+93, 70, 100, 20)];
    totleLab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    totleLab.font = FontSize(12);
//    totleLab.text = @"共30张照片";
    [self.contentView addSubview:totleLab];
    
}

- (void)setModel:(DiaryPhotoModel *)model{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.first_img];
    [imageView sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:ImageName(@"placeholderImage")];
    mouthLab.text = [NSString stringWithFormat:@"%@月",model.month];
    totleLab.text = [NSString stringWithFormat:@"共%ld张照片",model.count_img_number];
}

@end






