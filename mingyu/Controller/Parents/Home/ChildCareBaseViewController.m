//
//  ChildCareBaseViewController.m
//  mingyu
//
//  Created by apple on 2018/7/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChildCareBaseViewController.h"
#import "ChildCareDetailViewController.h"

@interface ChildCareBaseViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, strong) NSMutableArray *timeArray;

@property (nonatomic, assign) BOOL isScrollUp;

@property (nonatomic, strong) NSNumber *lastMaxtime;

@property (nonatomic, strong) NSNumber *lastMintime;

@property (nonatomic, assign) BOOL otherCircle;

@property (nonatomic, strong) NSNumber *record_min_time;
@property (nonatomic, assign) dispatch_once_t onceToken;

@end

@implementation ChildCareBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    _lastMintime = _labelModel.min_time?[_labelModel.min_time copy]:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    _lastMaxtime = _labelModel.max_time?_labelModel.max_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    
    [self refreshTableview];
    _timeArray = [NSMutableArray array];

}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [SVProgressHUD dismiss];
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _isScrollUp = NO;
            [self getChildcareList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            _isScrollUp = YES;
            [self getChildcareList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
//    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreChildcareList)];
}


- (void)getChildcareList{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL,KGetChildcareList,@(_labelModel.label_id)];
    NSNumber *min = _labelModel.min_time?_labelModel.min_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    NSNumber *max = _labelModel.max_time?_labelModel.max_time:[NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970]*1000];
    if([min compare:max] == NSOrderedDescending) {  //如果min > max
        min = max;
    }
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"age_group":@(_agesection+1),
                                 @"min_time":min,
                                 @"max_time":max,
                                 @"record_max_time":_lastMaxtime
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
//        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            NSArray *childcareList = data[@"childCareList"];
            _record_min_time = [NSNumber numberWithString:[NSString stringWithFormat:@"%@", data[@"record_min_time"]]];
            if (_record_min_time) {
                _otherCircle = YES;
                [_timeArray addObject:_record_min_time];
                for (NSNumber *num in [_timeArray copy]) {
                    if ([num floatValue] < [_record_min_time floatValue]) {
                        [_timeArray removeObject:num];
                    }
                }
            }
            for (NSDictionary *dic in childcareList) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                NSNumber *childcareTime = [NSNumber numberWithString:model.create_time];
                if (_otherCircle && [childcareTime floatValue] < [_lastMintime floatValue]) {
                    _lastPage = YES;
                    break;
                }
                if (_isScrollUp) {
                    [self.arrayData addObject:model];
                } else {
                    [self.arrayData insertObject:model atIndex:0];
                }
                [_timeArray addObject:childcareTime];
            }
            [self changeVideoTagMaxMineTime];
            [self.tableView reloadData];
        
            if (self.arrayData.count <= 3) {
                dispatch_once(&_onceToken, ^{
                    [self getChildcareList];
                });
            }
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

- (void)changeVideoTagMaxMineTime{
    NSNumber *max = [_lastMaxtime floatValue]>[[_timeArray valueForKeyPath:@"@max.self"] floatValue] ? _lastMaxtime:[_timeArray valueForKeyPath:@"@max.self"];
    NSNumber *min = _record_min_time?_record_min_time:[_timeArray valueForKeyPath:@"@min.self"];
    
    NSArray *library = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *strLib = [[library objectAtIndex:0] stringByAppendingPathComponent:@"Tag"];
    BOOL directory = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLib isDirectory:&directory]){
        [[NSFileManager defaultManager] createDirectoryAtPath:strLib
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:nil];
    }
    NSString *fileName = [NSString stringWithFormat:@"ChildCareTag%ld",(long)_agesection];
    NSString *entityArchive = [strLib stringByAppendingPathComponent:fileName];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:entityArchive];
    [array enumerateObjectsUsingBlock:^(LabelListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.label_id == _labelModel.label_id) {//数组中已经存在该对象
            obj.max_time = max;
            obj.min_time = min;
            _labelModel.min_time = min;
            _labelModel.max_time = max;
        }
    }];
    [NSKeyedArchiver archiveRootObject:array toFile:entityArchive];//归档(序列化)
}

#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsModel *model = [[NewsModel alloc] init];
    if (_arrayData && _arrayData.count > indexPath.row) {
        model = [_arrayData objectAtIndex:indexPath.row];
    }
    if (model.news_type == 1) {
        ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
        cell.newsmodel = model;
    return cell;
    } else if (model.news_type == 2){
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    } else {
        ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
        cell.newsmodel = model;
        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData && _arrayData.count > indexPath.row) {
        NewsModel *model = [_arrayData objectAtIndex:indexPath.row];
        if (model.news_type == 1) {
            ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.news_type == 2){
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        } else {
            ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}



- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}



- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


@end
