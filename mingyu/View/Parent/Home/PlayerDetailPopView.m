//
//  PlayerDetailPopView.m
//  mingyu
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PlayerDetailPopView.h"
#import "YCDownloadManager.h"
#import "downloadInfo.h"

@implementation PlayerDetailPopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setArrayData:(NSArray *)arrayData{
    _arrayData = arrayData;
    _title_lab.text = [DFPlayer shareInstance].currentAudioModel.fk_parent_title;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 40;
    _tableView.tableFooterView = [UIView new];
    [_tableView reloadData];
    
}


#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"PlayerListCell";
    PlayerListCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[PlayerListCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (self.arrayData.count > indexPath.row) {
        AudioModel *audiomodel = [self.arrayData objectAtIndex:indexPath.row];
        cell.audiomodel = audiomodel;
        [cell setDownloadStatus:[YCDownloadManager downloasStatusWithId:[NSString stringWithFormat:@"%ld",audiomodel.audio_id]]];
//        if ([[audiomodel.audio_path absoluteString]  hasPrefix:@"file"]) {
//            [cell.down_button setImage:ImageName(@"一键听_down_已下载") forState:0];
//        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.chooseCell) {
        self.chooseCell(indexPath.row);
    }
    
}



/**
 滚动到当前播放的音频
 */
- (void)scrollToCurrentAudio{
    [self.arrayData enumerateObjectsUsingBlock:^(AudioModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( obj.audio_id == [DFPlayer shareInstance].currentAudioModel.audio_id) {
            NSInteger index = [DFPlayer shareInstance].currentAudioModel.audioId;
            [_tableView scrollToRow:index inSection:0 atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            *stop = YES;
        }
    }];
}

- (IBAction)ClooseButton:(id)sender {
    if (self.clooseBlock) {
        self.clooseBlock();
    }
}


@end





@implementation PlayerListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setupUI];
    }
    return self;
}



- (void)setupUI{
    
    [self addSubview:self.playImage];

    [self addSubview:self.down_button];
    [_down_button addTarget:self action:@selector(downLoadbutton) forControlEvents:UIControlEventTouchUpInside];

    [self addSubview:self.audio_title_lab];

    if ([DFPlayer shareInstance].currentAudioModel.audio_type == 3) {
        self.down_button.hidden = YES;
    } else {
        self.down_button.hidden = NO;
    }
    [self.playImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.bottom.equalTo(self);
    }];
    
    [self.down_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.right.mas_equalTo(-10);
        make.width.height.mas_offset(30);
    }];
    
    [self.audio_title_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.mas_equalTo(50);
        make.right.mas_equalTo(-50);
    }];
}




- (UIImageView *)playImage{
    if (_playImage == nil) {
        _playImage = [[UIImageView alloc] init];
        _playImage.contentMode = UIViewContentModeScaleAspectFit;
        _playImage.image = ImageName(@"一键听_bofang_on");
    }
    return _playImage;
}



- (UILabel *)audio_title_lab{
    if (_audio_title_lab == nil) {
        _audio_title_lab = [[UILabel alloc] init];
        _audio_title_lab.font = FontSize(12);
        _audio_title_lab.textColor = [UIColor colorWithHexString:@"575757"];
    }
    return _audio_title_lab;
}


- (UIButton *)down_button{
    if (_down_button == nil) {
        _down_button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_down_button setImage:ImageName(@"一键听_down") forState:UIControlStateNormal];
    }
    return _down_button;
}


- (void)setAudiomodel:(AudioModel *)audiomodel{
    _audiomodel = audiomodel;
    _audio_title_lab.text = audiomodel.audio_title;
    if ([DFPlayer shareInstance].currentAudioModel.audio_id == audiomodel.audio_id) {
        _audio_title_lab.textColor = [UIColor colorWithHexString:@"50D0F4"];
        _playImage.hidden = NO;
    } else{
        _audio_title_lab.textColor = [UIColor colorWithHexString:@"575757"];
        _playImage.hidden = YES;
    }
}


- (void)setDownloadStatus:(YCDownloadStatus)status{
    NSString *statusStr = @"一键听_down";
    switch (status) {
        case YCDownloadStatusWaiting:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusDownloading:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusPaused:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusFinished:
            statusStr = @"一键听_down_已下载";
            break;
        case YCDownloadStatusFailed:
            statusStr = @"一键听_down_下载中";
            break;
            
        default:
            break;
    }
    [_down_button setImage:ImageName(statusStr) forState:0];
}

//下载
- (void)downLoadbutton{
    if (![YCDownloadManager isDownloadWithId:[NSString stringWithFormat:@"%ld",_audiomodel.audio_id]]) {
        downloadInfo *info = [[downloadInfo alloc] initWithUrl:[_audiomodel.audio_path absoluteString] fileId:[NSString stringWithFormat:@"%ld",_audiomodel.audio_id]];
        info.delegate = self;
        info.thumbImageUrl = [_audiomodel.audio_image_url absoluteString];
        info.downloadType = AudioType;
        info.fileName = _audiomodel.audio_title;
        info.date = [NSDate date];
        info.totletime = _audiomodel.time_length;
        info.enableSpeed = true;
        info.audiomodel =_audiomodel;
        [YCDownloadManager startDownloadWithItem:info priority:0.8];
        [self setDownloadStatus:info.downloadStatus];
    }
}

//#pragma -mark- YCDownloadItemDelegate
//- (void)downloadItemStatusChanged:(YCDownloadItem *)item {
//    [self setDownloadStatus:item.downloadStatus];
//}


@end






