//
//  QianDaoRulerView.h
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QianDaoRulerView : UIView


@property (weak, nonatomic) IBOutlet UIButton *knowButton;

@property (nonatomic, copy) void (^knowButtonBlock)(void);

@end
