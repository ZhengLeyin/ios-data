//
//  MayBeInterestedView.m
//  mingyu
//
//  Created by MingYu on 2018/12/7.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "MayBeInterestedView.h"

@implementation MayBeInterestedView

+ (instancetype)theMayBeInterestedView {
    return [[NSBundle mainBundle] loadNibNamed:@"MayBeInterestedView" owner:nil options:nil][0];
}

@end
