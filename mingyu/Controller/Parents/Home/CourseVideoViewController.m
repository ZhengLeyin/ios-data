//
//  CourseVideoViewController.m
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CourseVideoViewController.h"
#import "VideoHeaderView.h"
#import "UIView+CLSetRect.h"
#import "CLInputToolbar.h"
#import "CVDetailViewController.h"
#import "CVContentViewController.h"
#import "CVCommentViewController.h"

#import "ZFAVPlayerManager.h"
#import "ZFPlayerController.h"
#import "ZFPlayerControlView.h"
/**收银台*/
#import "CashierViewController.h"
/**育币充值*/
#import "MingYuRechargeViewController.h"
#import "JXCategoryView.h"


#define PlayerHeight  kScreenWidth*9/16
#define kHeaderViewH 118
@interface CourseVideoViewController ()<UIAlertViewDelegate,BottomCommentDelegate,JXCategoryViewDelegate,JXPagerViewDelegate,DFPlayerDelegate>
@property (nonatomic, strong) JXPagerView *pagerView;
@property (nonatomic, strong) NSArray *listViewArray;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property(nonatomic,strong)VideoHeaderView *titleHeaderView;   //标题 介绍 收藏 下载 view
@property (nonatomic, strong) CVDetailViewController *DetailViewC;
@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) VideoModel *videomodel;

@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) VideoPlayEndVIew *playendview;

@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIButton *backButton;
/** 支付按钮 */
@property (nonatomic, strong) UIButton *payButton;

/**头部信息Model*/
@property (nonatomic, strong) CourseModel *courseHeadermodel;

@property (nonatomic, strong) UIImageView *noVideoPathImageView;


@end

@implementation CourseVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    /**监听支付成功返回刷新界面*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentSuccessNotification) name:NSNotificationPaymentSuccessNotification object:nil];
    /**监听登录成功返回刷新界面*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetCourseCourseId) name:NSNotificationUserLogin object:nil];
    
    /**监听从后台返回*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResignActiveNotification) name:UIApplicationWillResignActiveNotification object:nil];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    //    如果音频在播放 先暂停音频
    [DFPlayer shareInstance].delegate = self;
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [[DFPlayer shareInstance] df_audioPause];
    }
    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    //视屏播放器
    [self setupPlayerView];
     /**获取头部信息*/
    [self GetCourseCourseId];

    [self setupNavigationItem];
    
//    [self initViewControllers];
}

/** 解决视频播放来电话时，视频被打断，当通话结束后视频 音频一起播放的问题  */
- (void)df_player:(DFPlayer *)player isInterrupted:(BOOL)isInterrupted{
    if (!isInterrupted) {
        [[DFPlayer shareInstance] df_audioPause];
    }
}

- (void)ResignActiveNotification{
    [self.player enterFullScreen:NO animated:YES];
    if ([DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        [[DFPlayer shareInstance] df_audioPause];
    }
}

-(void)paymentSuccessNotification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD setImageViewSize:CGSizeMake(40, 40)];
        [SVProgressHUD setMinimumSize:CGSizeMake(160, 102)];
        [SVProgressHUD setMinimumDismissTimeInterval:2];
        if (_courseHeadermodel.course_prerogative == 1 && (_courseHeadermodel.user_prerogative = 1)) {
               [SVProgressHUD showImage:[UIImage imageNamed:@"icon_medal"] status:@"  恭喜您，领取成功！ "];
        }else {
               [SVProgressHUD showImage:[UIImage imageNamed:@"buyCourseSuccess"] status:@"  恭喜您，支付成功！ "];
        }
    });
    [self GetCourseCourseId];
}

#pragma mark 视频播放设置
- (void)setupPlayerView{
    [self.view addSubview:self.playerView];
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    // 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:playerManager containerView:self.playerView];
    self.player.controlView = self.controlView;
    [self.view addSubview:self.playendview];
    [self.view addSubview:self.noVideoPathImageView];
    @weakify(self)
    self.playendview.playAgainBlock = ^{
        @strongify(self)
        self.player.allowOrentitaionRotation = YES; //重播 可以全屏
        self.player.assetURL = self.player.currentPlayerManager.assetURL;  //重播
        self.playendview.hidden = YES;
    };
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
    };
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
//        self.currentTime = 0;
        self.player.allowOrentitaionRotation = NO; //播完之后 禁止全屏
        if (self.player.isFullScreen) {
            [self.player enterFullScreen:NO animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.player.orientationObserver.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.playendview.hidden = NO;
            });
        } else {
            self.playendview.hidden = NO;
        }
    };
}

#pragma mark 导航栏返回键+分享功能
- (void)setupNavigationItem {
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame =CGRectMake(5,NaviH-44, 60, 44);
    [_backButton setImage:ImageName(@"back_white") forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(back)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];
    
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.frame = CGRectMake(kScreenWidth-60-5,NaviH-44, 60, 44);
    [_shareButton setImage:ImageName(@"更多_白色") forState:0];
    [_shareButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_shareButton];
}

#pragma mark 评论设置
-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        [weakSelf addVideoComment:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
}

-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}

- (void)dealloc{
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationPaymentSuccessNotification object:nil];
    ZPLog(@"deallocc");
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (IS_iPhoneXStyle) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;   //设置状态栏颜色为白色
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    }
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (self.player.currentPlayerManager.isPreparedToPlay) {
        [self.player addDeviceOrientationObserver];
        if (!self.player.currentPlayerManager.isPlaying) {
            self.player.pauseByEvent = NO;
        }
    }
    self.player.allowOrentitaionRotation = YES; //可以全屏
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationPaymentSuccessNotification object:nil];
    self.player.allowOrentitaionRotation = NO; //可以全屏
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.player.currentPlayerManager pause]; //暂停
}

#pragma mark - 支付按钮
-(UIButton *)payButton {
    if (!_payButton) {
        _payButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _payButton.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        _payButton.backgroundColor = [UIColor colorWithRed:244/255.0 green:116/255.0 blue:78/255.0 alpha:1];
        _payButton.titleLabel.font = FontSize(16.0);
        [_payButton setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
      
    }
    return _payButton;
}

#pragma mark 支付按钮判断 ---加入特权课
-(void)setpayButton {
     if (_courseHeadermodel.present_price > 0 && (_courseHeadermodel.is_hold == 0)){
         [self.payButton removeTarget:self action:@selector(payButtonClick) forControlEvents:UIControlEventTouchUpInside];
         [self.payButton addTarget:self action:@selector(payButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.payButton];
         if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
             /**本页面刷新数据*/
             NSString *payString = [NSString stringWithFormat:@"( %.1f育币 ) 领取特权课", _courseHeadermodel.present_price];
               [_payButton setTitle:payString forState:(UIControlStateNormal)];
             
             NSString * priceLength = [NSString stringWithFormat:@"%.1f",_courseHeadermodel.present_price];
             NSString *oldPrice = payString;
             NSUInteger length = [oldPrice length];
             NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:oldPrice];
              [attri addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)} range:NSMakeRange(2,  [priceLength length]+1)];
              [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(2, [priceLength length]+1)];
              [attri addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, length)];
             [_payButton setAttributedTitle:attri forState:(UIControlStateNormal)];
         }else {
             /**跳转支付跳转*/
             NSString *payString = [NSString stringWithFormat:@"( %.1f育币 ) 立即支付", _courseHeadermodel.present_price];
               [_payButton setTitle:payString forState:(UIControlStateNormal)];
         }
    }else {
        self.payButton.backgroundColor = [UIColor clearColor];
        self.payButton.height = 0;
        self.payButton.hidden  = YES;
        self.payButton = nil;
        [self.payButton removeAllSubviews];
    }
}

- (void)payButtonClick{
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
        [self getCourseUser];
    } else {
        /**收银台*/
        CashierViewController *vc = [CashierViewController new];
        vc.coursemodel = self.coursemodel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark 获取头部信息(点赞+播放器次数+收藏等数据)
- (void)GetCourseCourseId {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/courseHead?",KURL,KGetCourseCourseId,@(_coursemodel.course_id)];
    NSDictionary *parameters = @{
                                 @"userId":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            _courseHeadermodel = [CourseModel modelWithJSON:data];
            if (_courseHeadermodel.present_price == 0) {
                _courseHeadermodel.is_hold = YES;
            }
            self.titleHeaderView.coursemodel = _courseHeadermodel;
            _titleHeaderView.coursemodel.course_id = _coursemodel.course_id;
            CGFloat H = [self calculateString:_courseHeadermodel.course_title Width:18];
            _titleHeaderView.height = kHeaderViewH + H - 21.5;
        
            self.bottomView.haveCollect = _courseHeadermodel.is_collect_true;
            NSLog(@"urlImage=%@ section_path=%@",_courseHeadermodel.section_img,_courseHeadermodel.section_path);
            #pragma mark 判断有试看功能
            if (_courseHeadermodel.section_path.length == 0) {
//                 [self promptPurchaseAlertView];
                self.noVideoPathImageView.hidden = NO;
                [self.noVideoPathImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KKaptcha,_courseHeadermodel.section_img]]];
            }else {
                self.playendview.hidden = YES;
                self.noVideoPathImageView.hidden = YES;
                [self.controlView showTitle:nil coverURLString:[NSString stringWithFormat:@"%@%@",KKaptcha,_courseHeadermodel.section_img] fullScreenMode:ZFFullScreenModeLandscape];
                self.player.assetURL = [NSURL URLWithString:_courseHeadermodel.section_path];
            }
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }

#pragma mark 根据支付结果时候隐藏支付按钮,改变约束
        [self initViewControllers];
#pragma mark 底部评论框 收藏 分享
        [self.view addSubview:self.bottomView];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-effectViewH, kScreenWidth, effectViewH)];
        view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view];
        
        [self setTextViewToolbar];
#pragma mark 支付按钮判断
        [self setpayButton];
    } failure:^(NSError *error) {
    //         [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-30, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    return size.height;
}

#pragma mark 精彩评论
- (void)addVideoComment:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":@(_coursemodel.course_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(course_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HaveAddComment" object:nil userInfo:nil];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

/** 提示购买弹框 */
-(void)promptPurchaseAlertView {
    NSString *titleString;
    NSString *confirmString;
    if (_courseHeadermodel.user_prerogative == 1 && (_courseHeadermodel.course_prerogative == 1)) {
        titleString = @"无需购买该课程,请您立即领取该课程";
        confirmString = @"立即领取";
    }else  {
        titleString = @"需要购买该课程,才能观看哦";
        confirmString = @"立即支付";
    }
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:titleString delegate:self cancelButtonTitle:@"取消" otherButtonTitles:confirmString, nil];
    [alertView show];
}

/**特权课*/
-(void)getCourseUser {
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KCourseUser];
    NSDictionary *parameters = @{
                                 @"course_id":@(_courseHeadermodel.course_id)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            /**通知中心 --支付成功通知*/
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationPaymentSuccessNotification object:nil];
        }else {
            [UIView ShowInfo:TipCourseCollectionFailedMessage Inview:self.view];
        }
    } failure:^(NSError *error) {
        //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}

#pragma mark - BottomCommentDelegate
#pragma mark -收藏
- (void)collectButtonClicked{
    if (_courseHeadermodel.is_collect_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _courseHeadermodel.course_id],
                                     @"user_id":USER_ID,
                                     @"collect_type":@(collect_type_course)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _courseHeadermodel.is_collect_true = is_collect_true;
                _courseHeadermodel.collect_number++;
                [UIView ShowInfo:TipCollectSuccess Inview:self.view];
                [_titleHeaderView.collectButton setImage:ImageName(@"已收藏") forState:0];
                [_titleHeaderView.collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_courseHeadermodel.collect_number] forState:0];
                _bottomView.haveCollect = _courseHeadermodel.is_collect_true;
            }
        } failure:^(NSError *error) {
            //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        NSDictionary *parameters = @{
                                     @"idList":@(_courseHeadermodel.is_collect_true)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _courseHeadermodel.is_collect_true = 0;
                _courseHeadermodel.collect_number--;
                [UIView ShowInfo:TipUnCollectSuccess Inview:self.view];
                [_titleHeaderView.collectButton setImage:ImageName(@"收藏") forState:0];
                if (_courseHeadermodel.collect_number) {
                    [_titleHeaderView.collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_courseHeadermodel.collect_number] forState:0];
                } else {
                    [_titleHeaderView.collectButton setTitle:@"  收藏  " forState:0];
                }
                _bottomView.haveCollect = _courseHeadermodel.is_collect_true;
            }
        } failure:^(NSError *error) {
            //            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}

#pragma mark -分享
- (void)shareButtonClicked{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _coursemodel.course_title;
    model.descr = _coursemodel.course_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    model.webpageUrl = _coursemodel.share_url;
    model.accusationType = accusation_type_course;
    model.from_id = _coursemodel.course_id;
    model.shareType = share_course;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}

#pragma mark - 添加课程介绍 详情目录 课程评论
- (void)initViewControllers {
    __weak typeof(self) weakSelf = self;

    NSArray *titles = @[@"详情介绍", @"课程目录", @"课程评论"];
    
    /**详情介绍界面*/
    _DetailViewC = [[CVDetailViewController alloc] init];
    _DetailViewC.type = _courseHeadermodel.is_hold;
    _DetailViewC.coursemodel = _coursemodel;
    _DetailViewC.PushBlock = ^(UIViewController *vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    /**课程目录界面*/
    CVContentViewController *ContentViewC = [[CVContentViewController alloc] init];
    ContentViewC.type = _courseHeadermodel.is_hold;
    ContentViewC.courseId = _coursemodel.course_id;
    ContentViewC.selectVideoBlock = ^(VideoModel *model) {
        weakSelf.videomodel = model;
        #pragma mark 课程目录播放逻辑--试看 URL存在,购买URL存在,不购买URL 为nil
        if (model.section_path.length == 0) {
        
            [weakSelf promptPurchaseAlertView];
        }else {
            weakSelf.playendview.hidden = YES;
            weakSelf.noVideoPathImageView.hidden = YES;
            weakSelf.player.assetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.videomodel.section_path]];
        }
    };
    
    CVCommentViewController *InteractViewC = [[CVCommentViewController alloc] init];
    InteractViewC.type = _courseHeadermodel.is_hold;
    InteractViewC.course_id = _coursemodel.course_id;
    InteractViewC.PushBlock = ^(UIViewController *vc, BOOL toCourseList) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
        if (toCourseList) {
            //            侧滑返回时可直接返回到列表
            NSMutableArray *array = weakSelf.navigationController.viewControllers.mutableCopy;
            [array removeObjectAtIndex:array.count-2];
            [weakSelf.navigationController setViewControllers:array animated:NO];
        }
    };
    
    _listViewArray = @[_DetailViewC, ContentViewC, InteractViewC];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    self.categoryView.titles = titles;
    self.categoryView.titleLabelZoomScale = 1;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = [UIColor colorWithHexString:@"50D0F4"];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"767676"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.defaultSelectedIndex = 1;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorLineViewColor = [UIColor colorWithHexString:@"50D0F4"];
    lineView.indicatorLineWidth = 30;
    self.categoryView.indicators = @[lineView];
    
    _pagerView = [self preferredPagingView];
    self.pagerView.frame = CGRectMake(0, PlayerHeight+TopeffectViewH, kScreenWidth, kScreenHeight-PlayerHeight-TopeffectViewH);
    [self.view addSubview:self.pagerView];
    
    self.categoryView.contentScrollView = self.pagerView.listContainerView.collectionView;

    //扣边返回处理，下面的代码要加上
//    [self.pagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
//    [self.pagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

 //监听点击事件 代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
     if (buttonIndex == 0) {
        NSLog(@"你点击了取消");
     }else {
         if (_courseHeadermodel.course_prerogative == 1 && (_courseHeadermodel.user_prerogative == 1)) {
             /** 刷新本页数据 */
             if (![userDefault boolForKey:KUDhasLogin]) {
                 CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                 [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                 return;
             }
             [self getCourseUser];
         }else {
             /**收银台*/
             if (![userDefault boolForKey:KUDhasLogin]) {
                 CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                 [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                 return;
             }
             CashierViewController *vc = [CashierViewController new];
             vc.coursemodel = _coursemodel;
             [self.navigationController pushViewController:vc animated:YES];
         }
     }
}


//#pragma mark - 标题 介绍 收藏 下载 view
//- (UIView *)headerView {
//    return self.titleHeaderView;
//}

- (VideoHeaderView *)titleHeaderView {
    if (!_titleHeaderView) {
        _titleHeaderView = [[VideoHeaderView alloc] init];
        _titleHeaderView.frame = CGRectMake(0, PlayerHeight, kScreenWidth, kHeaderViewH);
    }
    return _titleHeaderView;
}

#pragma mark - 底部评论区域
- (BottomCommetView *)bottomView {
    if (_bottomView == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _bottomView = apparray.firstObject;
        _bottomView.delegate = self;
        _bottomView.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        __weak __typeof(self) weakSelf = self;
        _bottomView.hidden = YES;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
        _bottomView.LoctionButtonBlock = ^{
            CVCommentViewController *CommentViewC = (CVCommentViewController *)weakSelf.listViewArray[2];
            [CommentViewC locaToComment];
        };
    }
    return _bottomView;
}

- (void)willMoveToParentViewController:(UIViewController*)parent {
    [super willMoveToParentViewController:parent];
    ZPLog(@"%s,%@",__FUNCTION__,parent);
    if (!parent) {
//        [_playerView destroyPlayer];
    }
}
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 分享功能
- (void)share {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _courseHeadermodel.course_title;
    //    model.descr = _courseHeadermodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    model.webpageUrl = _courseHeadermodel.share_url;
    model.accusationType = accusation_type_course;
    model.from_id = _courseHeadermodel.course_id;
    model.shareType = share_course;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}

#pragma mark 分享功能
- (VideoPlayEndVIew *)playendview {
    if (!_playendview) {
        _playendview = [[NSBundle mainBundle] loadNibNamed:@"VideoPlayEndVIew" owner:nil options:nil][0];
        _playendview.frame = CGRectMake(0, TopeffectViewH, kScreenWidth, PlayerHeight);
        ShareModel *model = [[ShareModel alloc] init];
        model.titleStr = _coursemodel.course_title;
        model.descr = _coursemodel.course_abstract;
        model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
        model.webpageUrl = _coursemodel.share_url;
        model.accusationType = accusation_type_course;
        model.from_id = _coursemodel.course_id;
        model.shareType = share_course;
        _playendview.sharemodel = model;
        _playendview.hidden = YES;
    }
    return _playendview;
}


- (UIImageView *)noVideoPathImageView{
    if (!_noVideoPathImageView) {
        _noVideoPathImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, TopeffectViewH, kScreenWidth, PlayerHeight)];
        _noVideoPathImageView.hidden = YES;
    }
    return _noVideoPathImageView;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        ShareModel *model = [[ShareModel alloc] init];
        model.titleStr = _coursemodel.course_title;
        model.descr = _coursemodel.course_abstract;
        model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
        model.webpageUrl = _coursemodel.share_url;
        model.accusationType = accusation_type_course;
        model.from_id = _coursemodel.course_id;
        model.shareType = share_course;
        _controlView.landScapeControlView.sharemodel = model;
    }
    return _controlView;
}

- (UIView *)playerView{
    if (!_playerView) {
        if (IS_iPhoneXStyle) {
            UIView *effect = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, TopeffectViewH)];
            effect.backgroundColor = [UIColor blackColor];
            [self.view addSubview:effect];
        }
        _playerView = [[UIView alloc] initWithFrame:CGRectMake(0, TopeffectViewH, kScreenWidth, PlayerHeight)];
        if (IS_iPhoneXStyle) {
            _playerView.backgroundColor = [UIColor blackColor];
        } else {
            _playerView.backgroundColor = [UIColor grayColor];
        }
    }
    return _playerView;
}


- (JXPagerView *)preferredPagingView {
    return [[JXPagerView alloc] initWithDelegate:self];
}


#pragma mark - JXPagerViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.titleHeaderView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return kHeaderViewH;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 40;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSArray<id<JXPagerViewListViewDelegate>> *)listViewsInPagerView:(JXPagerView *)pagerView {
    return self.listViewArray;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    if (index == 2) {
        self.bottomView.hidden = NO;
        self.payButton.hidden = YES;
    } else {
        self.bottomView.hidden = YES;
        self.payButton.hidden = NO;
    }
}



- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.player.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)shouldAutorotate {
    return NO;
}

@end
