//
//  LSDHeaderView.m
//  mingyu
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LSDHeaderView.h"
#import "CourseAudioViewController.h"

@implementation LSDHeaderView


+ (instancetype)theTopView{
    return [[NSBundle mainBundle] loadNibNamed:@"LSDHeaderView" owner:nil options:nil][0];
}

+ (instancetype)theContentView{
    return [[NSBundle mainBundle] loadNibNamed:@"LSDHeaderView" owner:nil options:nil][1];
}




- (void)setBooklistmodel:(BookListModel *)booklistmodel{
    _booklistmodel = booklistmodel;
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,booklistmodel.book_head];
//    [_backgroundImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    
    _titleLab.text = booklistmodel.book_title;
    _playcountLab.text = [NSString stringWithFormat:@"播放次数:%ld",booklistmodel.play_count];
    
    [self changeCollectButton];
}

- (void)setCoursemodel:(CourseModel *)coursemodel{
    _coursemodel = coursemodel;

    
    _titleLab.text = coursemodel.course_title;
    _playcountLab.text = [NSString stringWithFormat:@"%ld人已购 | 已更新%ld期",coursemodel.hold_num,coursemodel.section_num];
    
    [self changeCollectButton];
}


//- (void)setBackgroundImage:(UIImageView *)backgroundImage{
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,coursemodel.section_img];
//    [_backgroundImage sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
//}


#warning <#message#>
- (void)changeCollectButton{
    if (_booklistmodel) {
        //收藏
        if (_booklistmodel.is_collect_true == 0) {
            [_collectButton setImage:ImageName(@"收藏") forState:UIControlStateNormal];
        } else {
            [_collectButton setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
        }
        [_collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_booklistmodel.collect_number] forState:UIControlStateNormal];
    } else{
        //收藏
        if (_coursemodel.is_collect_true == 0) {
            [_collectButton setImage:ImageName(@"收藏") forState:UIControlStateNormal];
        } else {
            [_collectButton setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
        }
        [_collectButton setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_coursemodel.collect_number] forState:UIControlStateNormal];
    }
}



- (IBAction)backButtonClicked:(id)sender {
    
    [[self viewController].navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareButtonClicked:(id)sender {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _coursemodel.course_title;
//    model.descr = _coursemodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_coursemodel.course_img];
    model.webpageUrl = _coursemodel.share_url;
    model.accusationType = accusation_type_course;
    model.from_id = _coursemodel.course_id;
    model.shareType = share_course;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}


- (IBAction)playButtonClicked:(id)sender {
    if (self.playButtonBlock) {
        self.playButtonBlock();
    }
}

//收藏  取消收藏
- (IBAction)collectButtonClicked:(id)sender {
    if (_booklistmodel) {
        if (_booklistmodel.is_collect_true == 0) {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
            NSDictionary *parameters = @{
                                         @"from_id":[NSString stringWithFormat:@"%ld", _booklistmodel.book_id],
                                         @"user_id":USER_ID,
                                         @"collect_type":@(collect_type_book)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    NSInteger is_collect_true = [json[@"data"] integerValue];
                    _booklistmodel.is_collect_true = is_collect_true;
                    _booklistmodel.collect_number++;
                    [self changeCollectButton];
                    [UIView ShowInfo:TipCollectSuccess Inview:[self viewController].view];
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                }
                
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                
            }];
        } else {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
            NSDictionary *parameters = @{
                                         @"idList":@(_booklistmodel.is_collect_true)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    _booklistmodel.is_collect_true = 0;
                    _booklistmodel.collect_number--;
                    [self changeCollectButton];
                    [UIView ShowInfo:TipUnCollectSuccess Inview:[self viewController].view];

                } else {
                    [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                }
                
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                
            }];
        }
    } else {
        if (_coursemodel.is_collect_true == 0) {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
            NSDictionary *parameters = @{
                                         @"from_id":[NSString stringWithFormat:@"%ld", _coursemodel.course_id],
                                         @"user_id":USER_ID,
                                         @"collect_type":@(collect_type_course)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    NSInteger is_collect_true = [json[@"data"] integerValue];
                    _coursemodel.is_collect_true = is_collect_true;
                    _coursemodel.collect_number++;
                    [self changeCollectButton];
                    [UIView ShowInfo:TipCollectSuccess Inview:[self viewController].view];
                    CourseAudioViewController *VC = (CourseAudioViewController *)[self viewController];
                    VC.bottomView.haveCollect = _coursemodel.is_collect_true;
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                }
                
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                
            }];
        } else {
            NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
            NSDictionary *parameters = @{
                                         @"idList":@(_coursemodel.is_collect_true)
                                         };
            [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                NSDictionary *json = responseObject;
                ZPLog(@"%@\n%@",json,json[@"message"]);
                BOOL success = [json[@"success"] boolValue];
                if (success) {
                    _coursemodel.is_collect_true = 0;
                    _coursemodel.collect_number--;
                    [self changeCollectButton];
                    [UIView ShowInfo:TipUnCollectSuccess Inview:[self viewController].view];
                    CourseAudioViewController *VC = (CourseAudioViewController *)[self viewController];
                    VC.bottomView.haveCollect = _coursemodel.is_collect_true;
                } else {
                    [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
                }
                
            } failure:^(NSError *error) {
//                [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                
            }];
        }
    }
    
}




@end
