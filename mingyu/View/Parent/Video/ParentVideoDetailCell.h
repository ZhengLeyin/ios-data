//
//  ParentVideoDetailCell.h
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VideoModel;

@interface ParentVideoDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon_image;

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UIImageView *source_image;

@property (weak, nonatomic) IBOutlet UILabel *source_lab;

@property (nonatomic, strong) VideoModel *model;


+(instancetype)theParentVideoDetailCellWithTableView:(UITableView *)tableview;

@end
