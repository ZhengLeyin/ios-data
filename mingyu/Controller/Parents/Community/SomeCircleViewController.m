//
//  SomeCircleViewController.m
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "SomeCircleViewController.h"


@interface SomeCircleViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *topArrayData;

@property (nonatomic, strong) SomeCircleFooterView *footerView;

@property (nonatomic, assign) BOOL showMore;

@end


@implementation SomeCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    NSString *title = [self.circlemodel.circle_name stringByReplacingOccurrencesOfString:@"<h2>" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@""];
    self.title = title;

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tabPageController.tabBar.itemTitleFont = [UIFont systemFontOfSize:14];
    self.tabPageController.tabBar.indicatorScrollFollowContent = YES;
    [self initViewControllers];
    
    [self getTopTopic];

    //注册通知：
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getTopTopic) name:NSNotificationSetTopTopic object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getTopTopic) name:NSNotificationCancelTopTopic object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

//    _topArrayData  = [NSMutableArray array];
    
    [self getCircleMessage];
}

- (void)dealloc{
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationSetTopTopic object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationCancelTopTopic object:self];
}


- (void)getCircleMessage{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCircleMessage];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"circle_id":@(_circlemodel.circle_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _circlemodel = [ParentCircleModel modelWithJSON:json[@"data"]];
            _circlemodel.topicNum++;
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)getTopTopic{
    [self.topArrayData removeAllObjects];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetTopTopic];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"circle_id":@(_circlemodel.circle_id),
                                 @"start":@"0",
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                [self.topArrayData addObject:model];
            }
            [self.tableView reloadData];

        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)initViewControllers{
    
    NSMutableArray *vcArray = [NSMutableArray array];
    TopicListViewController *allVC = [TopicListViewController new];
    allVC.yp_tabItemTitle = @"最新";
    allVC.topic_type = @"0";
    allVC.circlemodel = _circlemodel;
    [vcArray addObject:allVC];
    
    TopicListViewController *newVC = [TopicListViewController new];
    newVC.yp_tabItemTitle = @"精华";
    newVC.topic_type = @"1";
    newVC.circlemodel = _circlemodel;
    [vcArray addObject:newVC];
    
    TopicListViewController *eliteVC = [TopicListViewController new];
    eliteVC.yp_tabItemTitle = @"达人";
    eliteVC.topic_type = @"2";
    eliteVC.circlemodel = _circlemodel;
    [vcArray addObject:eliteVC];
    
    self.tabPageController.viewControllers = vcArray;
    
}




#pragma tableView--delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_showMore) {
        self.tableView.height = _topArrayData.count*130+50+100;
        return _topArrayData.count;
    }else{
        if (self.topArrayData && self.topArrayData.count > 2) {
            self.tableView.height = 2 *130+50+100;
            return 2;
        } else{
            self.tableView.height = self.topArrayData.count *130+100+10;
            return self.topArrayData.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
    if (self.topArrayData && self.topArrayData.count > indexPath.row) {
        cell.newsmodel = [_topArrayData objectAtIndex:indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_topArrayData.count > indexPath.row) {
        ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
        NewsModel *model = [_topArrayData objectAtIndex:indexPath.row];
        VC.newsmodel = model;
        VC.hiddenCircle = YES;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    RightTableViewCell *header = [RightTableViewCell theCircleCellWithTableView:tableView];
    header.circlemodel = _circlemodel;
    header.backgroundColor = [UIColor whiteColor];
    header.frame = CGRectMake(0, 0, kScreenWidth, 90);
    [view addSubview:header];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (_topArrayData.count > 2) {
        return self.footerView;
    } else {
        UIView *view  = [UIView new];
        view.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];
        return view;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_topArrayData.count > 2) {
        return 50;
    } else if (_topArrayData.count == 0){
        return 1;
    } else {
        return 10;
    }
}


- (NSMutableArray *)topArrayData{
    if (_topArrayData == nil) {
        _topArrayData  = [NSMutableArray array];
    }
    return _topArrayData;
}



- (UIView *)headerView{
    return self.tableView;
}



- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.frame = CGRectMake(0, 0, kScreenWidth, 20);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (SomeCircleFooterView *)footerView{
    if (_footerView == nil) {
        _footerView = [[SomeCircleFooterView alloc] init];
        _footerView.frame = CGRectMake(0, 0, kScreenWidth, 50);
        __weak typeof(self) weakSelf = self;
        _footerView.MoreTopic = ^(BOOL more) {
            _showMore = more;
            [weakSelf.tableView reloadData];
        };
    }
    return _footerView;
}



- (IBAction)searchButton:(id)sender {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchAll_type;
    [self.navigationController pushViewController:VC animated:YES];
}

//发帖
- (IBAction)fatieButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
    } else {
        TopicEditorViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"TopicEditorViewController"];
        VC.circle_id = _circlemodel.circle_id;
        [self.navigationController pushViewController:VC animated:YES];
    }
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}




@end
