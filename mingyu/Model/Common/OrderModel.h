//
//  OrderModel.h
//  mingyu
//
//  Created by apple on 2018/11/26.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderModel : NSObject


/**订单 ID */
@property (nonatomic, assign) NSUInteger order_id;

/**商品 ID */
@property (nonatomic, assign) NSUInteger commodity_id;

/** 用户 ID */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 订单编号 */
@property (nonatomic, copy) NSString *order_number;

/** 余额 */
@property (nonatomic, assign) CGFloat balance;

/** 交易金额 */
@property (nonatomic, assign) CGFloat virtual_money;

/** 交易类型 */
@property (nonatomic, copy) NSString *tips;

/** 交易时间 */
@property (nonatomic, copy) NSString *create_time;

/** 交易时间 */
@property (nonatomic, copy) NSString *time_status;


@end

NS_ASSUME_NONNULL_END
