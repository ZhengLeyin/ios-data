//
//  HomePageMoreModel.m
//  mingyu
//
//  Created by MingYu on 2018/12/26.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageMoreModel.h"

@implementation HomePageMoreModel

@end
@implementation HomePageMoreData

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"subMenuMessageList" : MoreSubmenumessagelist.class};
}

@end


@implementation MoreSubmenumessagelist

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"objectList" : MoreObjectlist.class};
}

@end


@implementation MoreObjectlist

@end
