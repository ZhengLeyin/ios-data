//
//  DownLoadView.h
//  mingyu
//
//  Created by apple on 2018/4/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

@protocol DownLoadViewDelegate <NSObject>

- (void)Cloose;

- (void)DownAll;

- (void)DownIng;

@end

#import <UIKit/UIKit.h>


//typedef void(^ClooseBlock)(void);
//
//typedef void(^DownAllBlock)(void);
//
//typedef void(^DownIngBlock)(void);


@interface DownLoadView : UIView<UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, weak)id<DownLoadViewDelegate> delegate; //声明协议变量

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIView *ListView;

@property (weak, nonatomic) IBOutlet UITableView *DownTaleview;

@property (weak, nonatomic) IBOutlet UIButton *DownIngButton;

@property (nonatomic, strong) NSMutableArray *arraydata;

@property (weak, nonatomic) IBOutlet UILabel *DownIngLab;



@end
