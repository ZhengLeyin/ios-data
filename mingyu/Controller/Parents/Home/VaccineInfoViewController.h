//
//  VaccineInfoViewController.h
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VaccineInfoViewController : UIViewController

@property (nonatomic, strong) NSDictionary *vaccineDic;


@end
