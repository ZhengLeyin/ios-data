//
//  SideBySideVideoCollectionCell.h
//  mingyu
//
//  Created by MingYu on 2018/12/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SideBySideVideoCollectionCell : UICollectionViewCell
/**视频标题*/
@property (weak, nonatomic) IBOutlet UILabel *contentVideoTitleLabel;
/**视频背景图*/
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
/**播放按钮*/
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;

/**播放按钮*/
@property (nonatomic, strong) VideoModel  *videomodel;
@end

NS_ASSUME_NONNULL_END
