//
//  QQUserModel.h
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QQUserModel : NSObject

/** 列表ID */
@property (nonatomic, assign) NSInteger qq_id;

/** 发送者ID */
@property (nonatomic, assign) NSInteger from_user_id;

/** 接受者ID */
@property (nonatomic, assign) NSInteger to_user_id;

/** 最后查看时间标志 */
@property (nonatomic, copy) NSString *flag_read_time;

/** 最后一条消息记录 */
@property (nonatomic, copy) NSString *message_content;

/** 最后一条消息时间 */
@property (nonatomic, copy) NSString *message_time;

/** 用户名字 */
@property (nonatomic, copy) NSString *user_name;

/** 用户头像 */
@property (nonatomic, copy) NSString *user_head;

/** 未读消息数 */
@property (nonatomic, assign) NSInteger unread_number;

/** 时间 */
@property (nonatomic, copy) NSString *time_status;


@end
