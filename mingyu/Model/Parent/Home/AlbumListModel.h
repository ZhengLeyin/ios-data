//
//  AlbumListModel.h
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlbumListModel : NSObject


@property (nonatomic, assign) NSInteger album_id;

@property (nonatomic, copy) NSString *album_title;

@property (nonatomic, assign) NSInteger play_count;

@property (nonatomic, copy) NSString *album_image;

@end


@interface AlbumclassifyModel : NSObject

@property (nonatomic, copy) NSString *classify_name;

@property (nonatomic, strong) NSArray *audioAlbumList;


@end

