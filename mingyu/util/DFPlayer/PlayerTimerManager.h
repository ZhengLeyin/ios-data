//
//  PlayerTimerManager.h
//  mingyu
//
//  Created by apple on 2018/5/16.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayerTimerManager : NSObject


@property (nonatomic,assign)NSInteger timeout;

@property (nonatomic, strong) dispatch_source_t timer;


/**单利方法*/
+ (PlayerTimerManager *)shareInstance;

- (void)creatTiemr;


- (void)countDown;

@end
