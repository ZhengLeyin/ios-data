//
//  BabyAlbumViewController.h
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DiaryPhotoModel;

@interface BabyAlbumViewController : UIViewController

@end


@interface BabyAlbumCell :UITableViewCell

{
    UIImageView *imageView;
    UILabel *mouthLab;
    UILabel *totleLab;
}

@property (nonatomic, strong) DiaryPhotoModel *model;

+ (instancetype)theBabyAlbumCellCellWithTableView:(UITableView *)tableView;

@end
