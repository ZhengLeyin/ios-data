//
//  ListenStoryCell.m
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ListenStoryCell.h"

@implementation ListenStoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    _max_gold_scale_lab.layer.cornerRadius = 5;
    _max_gold_scale_lab.layer.borderWidth = 1;
    _max_gold_scale_lab.layer.borderColor = [UIColor colorWithHexString:@"FF92A0"].CGColor;

//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.frame =_max_gold_scale_lab.frame;
//    CAShapeLayer *borderLayer = [CAShapeLayer layer];
//    borderLayer.frame = _max_gold_scale_lab.frame;
//    borderLayer.lineWidth = 1.f;
//    borderLayer.strokeColor =[UIColor colorWithHexString:@"FF92A0"].CGColor;
//    borderLayer.fillColor = [UIColor clearColor].CGColor;
//    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect: _max_gold_scale_lab.frame cornerRadius:5];
//    maskLayer.path = bezierPath.CGPath;
//    borderLayer.path = bezierPath.CGPath;
//    [_max_gold_scale_lab.layer insertSublayer:borderLayer atIndex:0];
//    [_max_gold_scale_lab.layer setMask:maskLayer];
}




+ (instancetype)theCellWithTableView:(UITableView *)tableView{
    static NSString *cellId = @"ListenStoryCell";
    ListenStoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ListenStoryCell" owner:nil options:nil][0];
    }
    return cell;
}

+ (instancetype)theHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"ListenStoryCell" owner:nil options:nil][1];

}


- (void)setBooklistmodel:(BookListModel *)booklistmodel{
    _booklistmodel = booklistmodel;
    _book_title_lab.text = booklistmodel.book_title;
    _content_abstract_lab.text = booklistmodel.content_abstract;
    _user_name_lab.text = [NSString stringWithFormat:@"朗读者：%@",booklistmodel.user_name];
    [_play_count_button setTitle:[NSString stringWithFormat:@"%ld",booklistmodel.play_count] forState:UIControlStateNormal];
    [_praise_number_btton setTitle:[NSString stringWithFormat:@"%ld",booklistmodel.praise_number] forState:UIControlStateNormal];
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,booklistmodel.book_head];
    [_book_head_image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    
    if (booklistmodel.max_gold_scale) {
        _max_gold_scale_lab.hidden = NO;
        _max_gold_scale_lab.text = [NSString stringWithFormat:@" 可抵%.0f%% ",booklistmodel.max_gold_scale*100];
    } else{
        _max_gold_scale_lab.hidden = YES;
    }
    
    if (booklistmodel.present_price && booklistmodel.original_price) {
        _present_price_lab.hidden = _original_price_lab.hidden = NO;
        
        NSString *str = [NSString stringWithFormat:@"原价¥%.2f",booklistmodel.original_price];
        NSMutableAttributedString *attrstr = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
        _original_price_lab.attributedText = attrstr;
        
        _present_price_lab.text = [NSString stringWithFormat:@"¥%.2f", booklistmodel.present_price];
        _present_price_lab.textColor = [UIColor colorWithHexString:@"F7A429"];

    } else if (booklistmodel.original_price){
        _original_price_lab.hidden = YES;
        _present_price_lab.hidden = NO;
        _present_price_lab.text = [NSString stringWithFormat:@"¥%.2f",booklistmodel.original_price];
        _present_price_lab.textColor = [UIColor colorWithHexString:@"F7A429"];

    } else {
        _original_price_lab.hidden = YES;
        _present_price_lab.hidden = NO;
        _present_price_lab.text = @"免费";
        _present_price_lab.textColor = [UIColor colorWithHexString:@"7FEC9C"];
    }
}



- (IBAction)MoreButton:(id)sender {
    if (self.MoreButtonBlock) {
        self.MoreButtonBlock();
    }
}


@end
