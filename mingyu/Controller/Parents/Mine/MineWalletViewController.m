//
//  MineWalletViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineWalletViewController.h"
#import "MingYuRechargeViewController.h"
#import "MingYuRechargeModel.h"

@interface MineWalletViewController ()

@property (weak, nonatomic) IBOutlet UIButton *ios_money_lab;

@property (weak, nonatomic) IBOutlet UIButton *ideal_money_lab;


@end

@implementation MineWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([userDefault boolForKey:KUDhasLogin]) {
        [self getUserBalance];
    }
}

- (void)getUserBalance{
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/wallet",KURL,KGetUserBalance,[NSString stringWithFormat:@"%@",USER_ID]];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            MingYuRechargeModel *model = [MingYuRechargeModel modelWithJSON:responseObject[@"data"]];
            [_ios_money_lab setTitle:[NSString stringWithFormat:@" %.2f",model.ios_money] forState:0];
            [_ideal_money_lab setTitle:[NSString stringWithFormat:@" %lu",(unsigned long)model.ideal_money] forState:0];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


/** 充值育币 */
- (IBAction)AddIOS_money:(id)sender {
    MingYuRechargeViewController *VC = [[MingYuRechargeViewController alloc] init];
    VC.type = 1;
    [self.navigationController pushViewController:VC animated:YES];
}

/** 获得积分 */
- (IBAction)AddIdeal_money:(id)sender {
    GoldRulerViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"GoldRulerViewController"];
    VC.ideal_money = [_ideal_money_lab.titleLabel.text integerValue];
    [self.navigationController pushViewController:VC animated:YES];
}

/** 育币明细 */
- (IBAction)IOS_moneyDetail:(id)sender {
    MineJinBiViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineJinBiViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}

/** 积分明细 */
- (IBAction)Ideal_moneyDetail:(id)sender {
    GoldDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"GoldDetailViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}

/** 返回 */
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
