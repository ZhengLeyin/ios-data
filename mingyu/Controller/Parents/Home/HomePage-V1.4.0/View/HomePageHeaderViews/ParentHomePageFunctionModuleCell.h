//
//  ParentHomePageFunctionModuleCell.h
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExcellentClassModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ParentHomePageFunctionModuleCell : UICollectionViewCell
/**功能选择模块 imageView*/
@property (weak, nonatomic) IBOutlet UIImageView *functionModuleImageView;
/**功能选择模块 名称Label*/
@property (weak, nonatomic) IBOutlet UILabel *functionModelLabel;

-(void)parentHomePageFunctionModuleCellOfModel:(ExcellentClassModel *)model;
@end

NS_ASSUME_NONNULL_END
