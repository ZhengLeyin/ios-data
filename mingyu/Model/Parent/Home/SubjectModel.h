//
//  SubjectModel.h
//  mingyu
//
//  Created by apple on 2018/6/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubjectModel : NSObject


@property (nonatomic, assign) NSInteger subject_id;

@property (nonatomic, copy) NSString *subject_title;

@property (nonatomic, assign) NSInteger play_count;

@property (nonatomic, copy) NSString *subject_image;

@property (nonatomic, assign) NSInteger fk_classify_id;

@property (nonatomic, assign) NSInteger ident_type;


@end


@interface SubjectclassifyModel : NSObject

@property (nonatomic, copy) NSString *classify_name;

@property (nonatomic, strong) NSArray *subjectList;


@end
