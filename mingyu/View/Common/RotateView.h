//
//  RotateView.h
//  mingyu
//
//  Created by apple on 2018/9/12.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotateView : UIView

@property (nonatomic, strong) UIButton *playbutton;

@property (nonatomic, strong) UIImage *image;


-(void) startRotating;
-(void) stopRotating;
-(void) resumeRotate;


@end
