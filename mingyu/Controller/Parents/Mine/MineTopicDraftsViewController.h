//
//  MineTopicDraftsViewController.h
//  mingyu
//
//  Created by apple on 2018/6/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineTopicDraftsViewController : UIViewController

@end


@interface TopicDraftsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;

@property (nonatomic, strong) NSDictionary *draftsDic;


@end
