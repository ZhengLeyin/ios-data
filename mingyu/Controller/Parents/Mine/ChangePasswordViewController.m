//
//  ChangePasswordViewController.m
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTF;

@property (weak, nonatomic) IBOutlet UITextField *newpasswordTF1;

@property (weak, nonatomic) IBOutlet UITextField *newpasswordTF2;

@property (weak, nonatomic) IBOutlet UIButton *seeButton1;

@property (weak, nonatomic) IBOutlet UIButton *seeButton2;

@property (weak, nonatomic) IBOutlet UIButton *downButton;


@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _downButton.userInteractionEnabled = NO;

    // 给TextField注册内容变动通知,添加监听方法
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(judgeUserInputStatus)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    
}

// TextField监听方法
- (void)judgeUserInputStatus {
    if (_oldPasswordTF.text.length > 5 && _newpasswordTF1.text.length > 5 && _newpasswordTF2.text.length > 5 && _newpasswordTF1.text.length < 21 && _newpasswordTF2.text.length < 21) {
        [_downButton setBackgroundColor:[UIColor colorWithHexString:@"50D0F4"]];
        _downButton.userInteractionEnabled = YES;
    } else{
        [_downButton setBackgroundColor:[UIColor colorWithHexString:@"C6C6C6"]];
        _downButton.userInteractionEnabled = NO;
    }

}


- (IBAction)seeButton1:(id)sender {
    if (_newpasswordTF1.secureTextEntry) {
        [_seeButton1 setImage:ImageName(@"see") forState:UIControlStateNormal];
        _newpasswordTF1.secureTextEntry = NO;
    } else{
        [_seeButton1 setImage:ImageName(@"close") forState:UIControlStateNormal];
        _newpasswordTF1.secureTextEntry = YES;
    }
}

- (IBAction)seeButton2:(id)sender {
    if (_newpasswordTF2.secureTextEntry) {
        [_seeButton2 setImage:ImageName(@"see") forState:UIControlStateNormal];
        _newpasswordTF2.secureTextEntry = NO;
    } else{
        [_seeButton2 setImage:ImageName(@"close") forState:UIControlStateNormal];
        _newpasswordTF2.secureTextEntry = YES;
    }
}


- (IBAction)DownButton:(id)sender {

    if ([_oldPasswordTF.text isEqualToString: _newpasswordTF1.text]) {
        [UIView ShowInfo:@"新密码不能和旧密码一样" Inview:self.view];
        return;
    }
    
    if ( ![_newpasswordTF1.text isEqualToString: _newpasswordTF2.text]) {
        [UIView ShowInfo:@"两次密码输入不一致！" Inview:self.view];
        return;
    }
    
    if (![NSString judgePassWordLegal:_newpasswordTF1.text]) {
        [UIView ShowInfo:@"6-20位字母数字组合" Inview:self.view];
        return;
    }
    
    _downButton.userInteractionEnabled = NO;
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateOldPassword];
    NSDictionary *parameters = @{
                                 @"oldPassword":[NSString MD5: _oldPasswordTF.text],
                                 @"newPassword":[NSString MD5: _newpasswordTF1.text],
                                 @"user_id":USER_ID
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
        _downButton.userInteractionEnabled = YES;

        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [userDefault setObject:[NSString MD5: _newpasswordTF1.text] forKey:KUDuser_password];
            [userDefault synchronize];
            [UIView ShowInfo:@"修改成功" Inview:self.view];
            [self performSelector:@selector(back:) withObject:nil afterDelay:1];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        _downButton.userInteractionEnabled = YES;
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
