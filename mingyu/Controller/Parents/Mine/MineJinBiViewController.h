//
//  MineJinBiViewController.h
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OrderModel;

@interface MineJinBiViewController : UIViewController

@end


@interface MineJinBiCell: UITableViewCell
@property (nonatomic, strong) OrderModel *ordermodel;


@property (weak, nonatomic) IBOutlet UILabel *title_lab;

@property (weak, nonatomic) IBOutlet UILabel *time_lab;

@property (weak, nonatomic) IBOutlet UILabel *usejinbi_lab;

@property (weak, nonatomic) IBOutlet UILabel *balance_lab;


@end
