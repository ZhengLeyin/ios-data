//
//  ParentVideoPlayerCell.h
//  mingyu
//
//  Created by apple on 2018/6/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentVideoPlayerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *playerContentView;


+ (instancetype)theParentVideoPlayerCellWithTableView:(UITableView *)tableview;

@end
