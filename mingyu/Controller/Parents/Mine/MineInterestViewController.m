//
//  MineInterestViewController.m
//  mingyu
//
//  Created by apple on 2018/6/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "MineInterestViewController.h"

@interface MineInterestViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *arrayData;


@end

@implementation MineInterestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self getInterestLabelList];
}


- (void)getInterestLabelList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetInterestLabelList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                LabelListModel *model = [LabelListModel modelWithJSON:dic];
                [self.arrayData addObject:model];
            }
            [_tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayData && self.arrayData.count > 0) {
        return _arrayData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"MineInterestCell";
    MineInterestCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[MineInterestCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (self.arrayData && self.arrayData.count > 0) {
        cell.labModel = [_arrayData objectAtIndex:indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


@end


@implementation MineInterestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _choose_switch.transform = CGAffineTransformMakeScale( 0.75, 0.75);//缩放

}


- (void)setLabModel:(LabelListModel *)labModel{
    _labModel = labModel;
    _name_lab.text = labModel.label_name;
    [_choose_switch setOn:labModel.is_true_select];
}


- (IBAction)chooseSwitch:(UISwitch *)sender {
    if (_labModel.is_true_select) {
//        取消兴趣
        NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KDelInterestSelect];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"label_id":@(_labModel.label_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_choose_switch setOn:NO];
                _labModel.is_true_select = 0;
            } else{
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else {
//        添加兴趣
        NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KAddInterestSelect];
        NSDictionary *parameters = @{
                                     @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                     @"label_id":@(_labModel.label_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL succsess = [json[@"success"]boolValue];
            if (succsess) {
                [_choose_switch setOn:YES];
                _labModel.is_true_select = 1;
            } else{
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}



@end
