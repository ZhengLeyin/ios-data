//
//  HomePageNewHeaderView.m
//  mingyu
//
//  Created by MingYu on 2018/12/27.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageNewHeaderView.h"
#import "ParentHomePageBannerView.h"
#import "ParentHomePageFunctionModuleView.h"
#import "HomePageAgeRelatedOfHeaderView.h"

static CGFloat ageHeight = 135;
static CGFloat functionModuleHeight = 130;
static CGFloat timeRecordHeight = 70;
@interface HomePageNewHeaderView ()
@property (nonatomic, assign) CGFloat childAge;
/** 两岁和两岁后后共用一个最顶部View */
@property (nonatomic, strong) UIView *ageHeaderHomePageView;
/** 功能选择按钮 */
@property (nonatomic, strong) UIView *functionModuleHomePageView;
/** 功能选择按钮 */
@property (nonatomic, strong) UIView *timeRecordHomePageView;

/**两岁后 广告位*/
@property (nonatomic, strong) ParentHomePageBannerView * homePageBannerViewbannerView;

/**功能模块 单独View*/
@property (nonatomic, strong) ParentHomePageFunctionModuleView * homePageFunctionModuleView;

/**两岁前和年龄相关 单独View*/
@property (nonatomic, strong) HomePageAgeRelatedOfHeaderView * ageHeaderView; 
@end
@implementation HomePageNewHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    _childAge = (currentTime - data)/3600/24/30/12;
    if (_childAge > 2) {
        [self afterTwoYearsOldOfView];
    }else {
        [self beforeAgeOfTwoOfView];
    }
    
}

/**两岁后的界面*/
-(void)afterTwoYearsOldOfView {
    /**bannerView*/
    [self addSubview:self.ageHeaderHomePageView];
    [self.ageHeaderHomePageView addSubview:self.homePageBannerViewbannerView];
    [self addSubview:self.functionModuleHomePageView];
    [self.functionModuleHomePageView addSubview:self.homePageFunctionModuleView];
}

/**两岁前的界面*/
-(void)beforeAgeOfTwoOfView {
    [self addSubview:self.ageHeaderHomePageView];
    [self.ageHeaderHomePageView addSubview:self.ageHeaderView];
    [self addSubview:self.functionModuleHomePageView];
    [self.functionModuleHomePageView addSubview:self.homePageFunctionModuleView];
    /**时光印记*/
    [self addSubview:self.timeRecordHomePageView];
    
    UIView *timeRecordBackView  = [[UIView alloc]initWithFrame:CGRectMake(20, 12, kScreenWidth - 40, timeRecordHeight - 24)];
    [self.timeRecordHomePageView addSubview:timeRecordBackView];
    UIImageView *timeRecordImageView = [[UIImageView alloc]initWithFrame:CGRectMake(6, (timeRecordHeight - 24)/2 - 19/2, 21, 19)];
    timeRecordImageView.image = ImageName(@"icon_vaccine1-V1.4.0");
    timeRecordBackView.centerY = timeRecordBackView.centerY;
    [timeRecordBackView addSubview:timeRecordImageView];
    timeRecordBackView.backgroundColor = [UIColor whiteColor];

    UILabel *babyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(timeRecordImageView.frame) + 5, (timeRecordHeight - 24)/2 - 20/2, 140, 20)];
    babyTitleLabel.text = @"—记录宝宝美好时光";
    babyTitleLabel.font = FontSize(13.0);
    babyTitleLabel.textColor = [UIColor blackColor];
    [timeRecordBackView addSubview:babyTitleLabel];
    
    UILabel *goToLabel = [[UILabel alloc]initWithFrame:CGRectMake(kScreenWidth - 90, (timeRecordHeight - 24)/2 - 20/2, 40, 20)];
    goToLabel.text = @"去记录";
    //goToLabel.backgroundColor = [UIColor purpleColor];
    goToLabel.textAlignment = NSTextAlignmentCenter;
    goToLabel.font = FontSize(12.0);
    goToLabel.textColor = [UIColor colorWithHexString:@"90D3FF"];
    [timeRecordBackView addSubview:goToLabel];
    
#pragma mark 两岁前 属性处理
    timeRecordBackView.userInteractionEnabled = YES;
    timeRecordBackView.layer.cornerRadius = 5;
    timeRecordBackView.layer.masksToBounds = YES;
    goToLabel.layer.cornerRadius = 10;
    goToLabel.layer.masksToBounds = YES;
    goToLabel.layer.borderColor = [UIColor colorWithHexString:@"#90D3FF"].CGColor;
    goToLabel.layer.borderWidth = 1;
    
    /* 添加一个轻拍手势 */
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [[[tap rac_gestureSignal] takeUntil:self.rac_willDeallocSignal]subscribeNext:^(__kindof UIGestureRecognizer * _Nullable x) {
        [self TimeRecord:nil];
    }];
    [timeRecordBackView addGestureRecognizer:tap];

}

#pragma mark 时光印记跳转
- (void)TimeRecord:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    TimeRecordViewController *VC = [TimeRecordViewController new];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


-(UIView *)ageHeaderHomePageView {
    if (!_ageHeaderHomePageView) {
        _ageHeaderHomePageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, ageHeight)];
        _ageHeaderHomePageView.backgroundColor = [UIColor whiteColor];
    }
    return _ageHeaderHomePageView;
}

-(UIView *)functionModuleHomePageView {
    if (!_functionModuleHomePageView) {
        _functionModuleHomePageView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.ageHeaderHomePageView.frame), kScreenWidth, functionModuleHeight)];
        _functionModuleHomePageView.backgroundColor = [UIColor whiteColor];
    }
    return _functionModuleHomePageView;
}

-(UIView *)timeRecordHomePageView {
    if (!_timeRecordHomePageView) {
        _timeRecordHomePageView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.functionModuleHomePageView.frame), kScreenWidth, timeRecordHeight)];
        _timeRecordHomePageView.backgroundColor = [UIColor colorWithHexString:@"F7F8FA"];;
    }
    return _timeRecordHomePageView;
}

-(ParentHomePageBannerView *)homePageBannerViewbannerView {
    if (!_homePageBannerViewbannerView) {
        _homePageBannerViewbannerView = [[ParentHomePageBannerView alloc]initWithFrame:CGRectMake(20, 0, kScreenWidth - 40, 135)];
        _homePageBannerViewbannerView.backgroundColor = [UIColor purpleColor];
        _homePageBannerViewbannerView.layer.cornerRadius = 5;
        _homePageBannerViewbannerView.layer.masksToBounds = YES;
        //_bannerView.frame = _homePageBannerViewbannerView.frame;
    }
    return _homePageBannerViewbannerView;
}

-(ParentHomePageFunctionModuleView *)homePageFunctionModuleView {
    if (!_homePageFunctionModuleView) {
        _homePageFunctionModuleView = [[ParentHomePageFunctionModuleView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, functionModuleHeight)];
        //_homePageFunctionModuleView.backgroundColor = [UIColor yellowColor];
        //_bannerView.frame = _homePageBannerViewbannerView.frame;
    }
    return _homePageFunctionModuleView;
}

-(HomePageAgeRelatedOfHeaderView *)ageHeaderView {
    if (!_ageHeaderView) {
        _ageHeaderView = [[HomePageAgeRelatedOfHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, ageHeight)];
    }
    return _ageHeaderView;
}


@end
