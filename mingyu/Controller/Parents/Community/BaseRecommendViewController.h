//
//  BaseRecommendViewController.h
//  mingyu
//
//  Created by apple on 2018/11/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ArticleLabelModel;

NS_ASSUME_NONNULL_BEGIN

@interface BaseRecommendViewController : UIViewController

@property (nonatomic, strong) ArticleLabelModel *labModel;

- (void)refreshTableview;

@end

NS_ASSUME_NONNULL_END
