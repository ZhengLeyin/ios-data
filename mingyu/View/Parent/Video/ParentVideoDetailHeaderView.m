//
//  ParentVideoDetailHeaderView.m
//  mingyu
//
//  Created by apple on 2018/7/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoDetailHeaderView.h"

@implementation ParentVideoDetailHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (instancetype)haveUserVideoDetailHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentVideoDetailHeaderView" owner:nil options:nil][0];
}

+ (instancetype)noUserVideoDetailHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"ParentVideoDetailHeaderView" owner:nil options:nil][1];
}

- (void)setVideomodel:(VideoModel *)videomodel {
    _videomodel = videomodel;
    
    _title_lab.text = _videomodel.video_title;
    _abstract_lab.text = _videomodel.video_abstract;
    _time_lab.text = [NSString transformTime2:videomodel.create_time];
    
    if (_videomodel.fk_user_id) {
//        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.user_head];
        [_user_icon_button zp_setImageWithURL:_videomodel.user_head forState:0 placeholderImage:ImageName(@"默认头像")];
        _user_name_lab.text = _videomodel.user_name;
        _guanzhu_count_lab.text = [NSString stringWithFormat:@"%ld人关注",_videomodel.follow_number];

        _guanzhu_button.layer.cornerRadius = 3;
        _guanzhu_button.layer.borderWidth = 1;
        _guanzhu_button.layer.masksToBounds = YES;
        [self changefollowButtonState:_videomodel];
    } else {
        _source_lab.text = _videomodel.video_source;
    }
    
    [self changCollectButtonState:_videomodel];
    
    [self changPraiseButtonState:_videomodel];
}

//根据状态判断是否已关注
- (void)changefollowButtonState:(VideoModel *)model{
    
    if (model.follow_status == 1) {
        _guanzhu_button.layer.borderColor = [UIColor whiteColor].CGColor;
        [_guanzhu_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_guanzhu_button setTitle:@"+关注" forState:UIControlStateNormal];
        _guanzhu_button.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    } else if (model.follow_status == 2) {
        _guanzhu_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_guanzhu_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_guanzhu_button setTitle:@"已关注" forState:UIControlStateNormal];
        _guanzhu_button.backgroundColor = [UIColor whiteColor];
    }else {
        _guanzhu_button.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [_guanzhu_button setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [_guanzhu_button setTitle:@"相互关注" forState:UIControlStateNormal];
        _guanzhu_button.backgroundColor = [UIColor whiteColor];
    }

}


//根据状态判断是否收藏
- (void)changCollectButtonState:(VideoModel *)model{
//    //收藏
//    if (model.is_collect_true == 0) {
//        [_collect_button setImage:ImageName(@"收藏") forState:UIControlStateNormal];
//    } else {
//        [_collect_button setImage:ImageName(@"已收藏") forState:UIControlStateNormal];
//    }
//    [_collect_button setTitle:[NSString stringWithFormat:@"  %ld收藏  ",model.collect_number] forState:UIControlStateNormal];
}
//根据状态判断是否点赞
- (void)changPraiseButtonState:(VideoModel *)model{
    //点赞
    if (model.click_praise_true == 0) {
        [_zan_button setImage:ImageName(@"点赞") forState:UIControlStateNormal];
    } else {
        [_zan_button setImage:ImageName(@"已赞") forState:UIControlStateNormal];
    }
    if (model.praise_number) {
        [_zan_button setTitle:[NSString stringWithFormat:@"  %ld赞  ",model.praise_number] forState:UIControlStateNormal];
    } else {
        [_zan_button setTitle:@"  赞  " forState:UIControlStateNormal];

    }
}


- (IBAction)showAbstractView:(UIButton *)sender {
    if (self.showAbstractBlock) {
        self.showAbstractBlock();
    }
   
}


- (IBAction)zanButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (_videomodel.click_praise_true == 0) {
        _zan_button.userInteractionEnabled = NO;
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _videomodel.video_id],
                                     @"user_id":USER_ID,
                                     @"praise_type":@(praise_type_video)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _zan_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger click_praise_true = [json[@"data"] integerValue];
                _videomodel.click_praise_true = click_praise_true;
                _videomodel.praise_number++;
                [self changPraiseButtonState:_videomodel];
                [UIView ShowInfo:TipPraiseSuccess Inview:[self viewController].view];
                
            } else {
                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
            _zan_button.userInteractionEnabled = YES;
        }];
    } else {
        
        [UIView ShowInfo:TipDoNotPraise Inview:[self viewController].view];
    }
}


- (IBAction)shareButton:(id)sender {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _videomodel.video_title;
    model.descr = _videomodel.video_abstract;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_videomodel.video_img];
    model.webpageUrl = _videomodel.share_url;
    model.accusationType = accusation_type_video;
    model.from_id = _videomodel.video_id;
    model.shareType = share_video;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}


- (IBAction)guanzhuButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _guanzhu_button.userInteractionEnabled = NO;
    if (_videomodel.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id": @(_videomodel.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _guanzhu_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _videomodel.follow_status = follow_status;
                [self changefollowButtonState:_videomodel];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
            _guanzhu_button.userInteractionEnabled = YES;
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":@(_videomodel.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            _guanzhu_button.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _videomodel.follow_status = 1;
                [self changefollowButtonState:_videomodel];
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
            _guanzhu_button.userInteractionEnabled = YES;
        }];
    }
}

- (IBAction)userIconButton:(id)sender {
    PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
    VC.user_id = _videomodel.fk_user_id;
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


@end
