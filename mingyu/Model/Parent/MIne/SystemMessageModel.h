//
//  SystemMessageModel.h
//  mingyu
//
//  Created by apple on 2018/9/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemMessageModel : NSObject


/** 主键id */
@property (nonatomic, assign) NSInteger message_id;

/** 用户ID */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 来源用户id */
@property (nonatomic, assign) NSInteger from_user_id;

/** 来源用户名 */
@property (nonatomic, copy) NSString *from_user_name;

/** 来源id */
@property (nonatomic, assign) NSInteger fk_from_id;

/** 信息 */
@property (nonatomic, copy) NSString *message;

/** 子报文 */
@property (nonatomic, copy) NSString *sub_message;

/** 消息类型 */
@property (nonatomic, assign) SystemMessageType type;

/** 阅读状态 */
@property (nonatomic, assign) NSInteger read_status;

/** 时间状态 */
@property (nonatomic, copy) NSString *time_status;

@property (nonatomic, assign) CGFloat height;

@end
