//
//  NSString+Extension.m
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)


// MD5加密算法
- (NSString *)md5HexDigest
{
    const char *original_str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, (unsigned int)strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02x", result[i]];
    
    return [hash lowercaseString];
}


//Java后台同步MD5加密
+(NSString*)MD5:(NSString *)input{
    NSString* s = [self makeMd5:input];
    NSString* s1 = [s substringToIndex:16];
    NSString* s2 =[s substringFromIndex:16];
    s1 = [self makeMd5:s1];
    s2 = [self makeMd5:s2];
    NSMutableString* md5String = [NSMutableString stringWithString:s1];
    [md5String appendString:s2];
    for(int i =0;i<100;i++){
        md5String = [self makeMd5WithMutaleString:md5String];
    }
    return md5String;
}

+ (NSString*)makeMd5:(NSString*) str{
    const char * pointer = [str UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [string appendFormat:@"%02x",md5Buffer[i]];
    return string;
}

+ (NSMutableString*)makeMd5WithMutaleString:(NSMutableString*) str{
    const char * pointer = [str UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [string appendFormat:@"%02x",md5Buffer[i]];
    return string;
}


// 手机号码验证正则表达式
+ (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES)) {
        return YES;
    } else {
        return NO;
    }
}


+ (NSString*)getCurrentTimes:(NSString *)format Time:(NSString *)sendtime{
    if (!format) {
        format = @"yyyy-MM-dd HH:mm:ss";
    }
    
    NSTimeInterval time = [sendtime doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:format];
    NSDate *timeDate;
    if (!sendtime) {
        timeDate = [NSDate date];
    } else{
        timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    }
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}




+(NSString*)getCurrentTimes:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    if (!format) {
        format = @"yyyy-MM-dd HH:mm:ss";
    }
    [formatter setDateFormat:format];

    //现在时间
    NSDate *datenow = [NSDate date];
    
    //----------将nsdate按formatter格式转成nsstring
    
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    
//    ZPLog(@"currentTimeString =  %@",currentTimeString);
    
    return currentTimeString;
    
}



// 将时间戳转换成时间
+ (NSString *)transformTime5:(NSString *)sendTime5{
    
    NSTimeInterval time;
    if (sendTime5.length > 10) {
         time = [sendTime5 doubleValue]/1000.0;
    }else{
         time = [sendTime5 doubleValue];
    }
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
    
}



// 将时间戳转换成时间
+ (NSString *)transformTime:(NSString *)sendTime{
    
    NSTimeInterval time = [sendTime doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}


+ (NSString *)getWeekDayFordate:(NSString *)sendTime{
    NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六", nil];
    
    NSTimeInterval time = [sendTime doubleValue]/1000.0;
    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
    
    NSString *weekStr = [weekday objectAtIndex:components.weekday];
    return weekStr;
}

+ (NSString *)transformTime2:(NSString *)sendTime2{
    
    NSTimeInterval time = [sendTime2 doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}


+ (NSString *)transformTime3:(NSString *)sendTime3{
    
    NSTimeInterval time = [sendTime3 doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
    
}

+ (NSString *)transformTime4:(NSString *)sendTime4{
    
    NSTimeInterval time = [sendTime4 doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}


+ (NSString *)transformTime6:(NSString *)sendTime6{   //  MM-dd

    NSTimeInterval time = [sendTime6 doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}



+ (NSString *)transformTime7:(NSString *)sendTime7{  //  yyyy.MM.dd
    
    NSTimeInterval time = [sendTime7 doubleValue]/1000.0;
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy.MM.dd"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
}


+ (NSInteger )transformDate:(NSString *)sendTime{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init]; //指定时间显示样式: HH表示24小时制 hh表示12小时制
    [formatter setDateFormat:@"YYYY-MM-dd"];
//    NSString *lastTime = @"2017-01-23 17:22:00";
    NSDate *lastDate = [formatter dateFromString:sendTime]; //以 1970/01/01 GMT为基准，得到lastDate的时间戳
    NSInteger firstStamp = [lastDate timeIntervalSince1970];
    return firstStamp;
}


/**返回更新时间 */
+ (NSString *)compareTimeForNow:(NSInteger)comment_time {
    // 获取当前时时间戳 1466386762.345715 十位整数 6位小数
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    NSTimeInterval createTime = comment_time/1000;
    // 时间差
    NSTimeInterval time = currentTime - createTime;
    if (time < 60) {
        return [NSString stringWithFormat:@"刚刚"];
    }
    //秒转为分钟
    NSInteger min = time/60;
    if (min < 60) {
        return [NSString stringWithFormat:@"%ld分钟前",min];
    }
    
    // 秒转小时
    NSInteger hours = time/3600;
    if (hours<24) {
        return [NSString stringWithFormat:@"%ld小时前",hours];
    }
    
    //秒转天数
    NSInteger days = time/3600/24;
    if (days < 2) {
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"HH:mm"];
        NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:createTime];
        NSString *timeStr = [formatter stringFromDate:timeDate];
        
        return [NSString stringWithFormat:@"昨天 %@",timeStr];
    }
    
    if (days < 3) {
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"HH:mm"];
        NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:createTime];
        NSString *timeStr = [formatter stringFromDate:timeDate];
        
        return [NSString stringWithFormat:@"前天 %@",timeStr];
    }
    
//    if (days < 365) {
//        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
//        [formatter setDateFormat:@"MM-dd"];
//        NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:createTime];
//        NSString *timeStr = [formatter stringFromDate:timeDate];
//        return timeStr;
//    }
//
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:createTime];
    NSString *timeStr = [formatter stringFromDate:timeDate];
    return timeStr;
    
    
    
//    //秒转月
//    NSInteger months = time/3600/24/30;
//    if (months < 12) {
//        return [NSString stringWithFormat:@"%ld月前",months];
//
//    }
//    //秒转年
//    NSInteger years = time/3600/24/30/12;
//    return [NSString stringWithFormat:@"%ld年前",years];
    
}

//是否为银行卡
+ (BOOL) checkCardNo:(NSString*) cardNo{
    int oddsum = 0;     //奇数求和
    int evensum = 0;    //偶数求和
    int allsum = 0;
    int cardNoLength = (int)[cardNo length];
    int lastNum = [[cardNo substringFromIndex:cardNoLength-1] intValue];
    
    cardNo = [cardNo substringToIndex:cardNoLength - 1];
    for (int i = cardNoLength -1 ; i>=1;i--) {
        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(i-1, 1)];
        int tmpVal = [tmpString intValue];
        if (cardNoLength % 2 ==1 ) {
            if((i % 2) == 0){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }else{
            if((i % 2) == 1){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }
    }
    
    allsum = oddsum + evensum;
    allsum += lastNum;
    if((allsum % 10) == 0)
        return YES;
    else
        return NO;
}




/**
 json 转 string

 @param dict 目标json
 @return 转换完成后string
 */
+ (NSString *)convertToJsonData:(NSDictionary *)dict{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        ZPLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@"" withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
}


/**
 * 传入参数与url，拼接为一个带参数的url
 **/
+(NSString *) connectUrl:(NSDictionary *)params url:(NSString *) urlLink{
    // 初始化参数变量
    NSString *str = @"";
    
    // 快速遍历参数数组
    for(id key in params) {
//        ZPLog(@"key :%@  value :%@", key, [params objectForKey:key]);
        str = [str stringByAppendingString:key];
        str = [str stringByAppendingString:@"="];
        if (![[params objectForKey:key] isKindOfClass:[NSString class]]) {
            str = [str stringByAppendingString:[NSString stringWithFormat:@"%@", [params objectForKey:key]]];
        } else {
            str = [str stringByAppendingString:[params objectForKey:key]];

        }
        str = [str stringByAppendingString:@"&"];
    }
    // 处理多余的&以及返回含参url
    if (str.length > 1) {
        // 去掉末尾的&
        str = [str substringToIndex:str.length - 1];
        // 返回含参url
        urlLink = [urlLink stringByAppendingString:str];
        urlLink = [urlLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        return urlLink;
    }
    return Nil;
}



+(BOOL)judgePassWordLegal:(NSString *)pass{
    BOOL result = false;
    if ([pass length] >= 6 && [pass length] <= 20){
        // 判断长度大于6位后再接着判断是否同时包含数字和字符
        NSString * regex = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        result = [pred evaluateWithObject:pass];
    }
    return result;
}  


// 搜索时关键字高亮
+ (NSMutableAttributedString *)stringWithHighLightSubstring:(NSString *)totalString substring:(NSString *)substring{
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:totalString];
    NSString * copyTotalString = totalString;
    NSMutableString * replaceString = [NSMutableString stringWithCapacity:0];
    for (int i = 0; i < substring.length; i ++) {
        [replaceString appendString:@" "];
    }
    while ([copyTotalString rangeOfString:substring].location != NSNotFound) {
        NSRange range = [copyTotalString rangeOfString:substring];
        //颜色如果统一的话可写在这里，如果颜色根据内容在改变，可把颜色作为参数，调用方法的时候传入
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"50D0F4"] range:range];
        copyTotalString = [copyTotalString stringByReplacingCharactersInRange:range withString:replaceString];
    }
    return attributedString;
}


@end
