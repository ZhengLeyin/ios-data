//
//  ParentHomePageFunctionModuleView.h
//  mingyu
//
//  Created by MingYu on 2018/12/14.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageViewModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ParentHomePageFunctionModuleView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong) UICollectionView *collectionView;
@end

NS_ASSUME_NONNULL_END
