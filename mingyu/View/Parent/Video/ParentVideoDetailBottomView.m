//
//  ParentVideoDetailBottomView.m
//  mingyu
//
//  Created by apple on 2018/7/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentVideoDetailBottomView.h"

@implementation ParentVideoDetailBottomView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
//     Drawing code
     _pinlun_button.titleEdgeInsets = UIEdgeInsetsMake(-5, -_pinlun_button.imageView.frame.size.width, -_pinlun_button.imageView.frame.size.height-10, 0);
    //                mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.frame.size.height-10, 0, 0, -mapBtn.titleLabel.frame.size.width);
    // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
     _pinlun_button.imageEdgeInsets = UIEdgeInsetsMake(-_pinlun_button.titleLabel.intrinsicContentSize.height-10, 0, 0, -_pinlun_button.titleLabel.intrinsicContentSize.width);
    
    _dianzan_button.titleEdgeInsets = UIEdgeInsetsMake(-5, -_dianzan_button.imageView.frame.size.width, -_dianzan_button.imageView.frame.size.height-10, 0);
    //                mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.frame.size.height-10, 0, 0, -mapBtn.titleLabel.frame.size.width);
    // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
    _dianzan_button.imageEdgeInsets = UIEdgeInsetsMake(-_dianzan_button.titleLabel.intrinsicContentSize.height-10, 0, 0, -_dianzan_button.titleLabel.intrinsicContentSize.width);
    
    _shoucang_button.titleEdgeInsets = UIEdgeInsetsMake(-5, -_shoucang_button.imageView.frame.size.width, -_shoucang_button.imageView.frame.size.height-10, 0);
    //                mapBtn.imageEdgeInsets = UIEdgeInsetsMake(-mapBtn.titleLabel.frame.size.height-10, 0, 0, -mapBtn.titleLabel.frame.size.width);
    // 由于iOS8中titleLabel的size为0，用上面这样设置有问题，修改一下即可
    _shoucang_button.imageEdgeInsets = UIEdgeInsetsMake(-_shoucang_button.titleLabel.intrinsicContentSize.height-10, 0, 0, -_shoucang_button.titleLabel.intrinsicContentSize.width);
    
}



//评论
- (IBAction)PinlunButton:(id)sender {

    if (self.showDetailBlock) {
        self.showDetailBlock();
    }
}

//点赞
- (IBAction)DianzanButton:(id)sender {
    
}

//收藏
- (IBAction)Shoucang_button:(id)sender {
    
}

//关闭
- (IBAction)ClooseButton:(id)sender {
    if (_clooseViewBlock) {
        self.clooseViewBlock();
    }
}


//收藏
- (IBAction)CollectButton:(id)sender {
    
}

//点赞
- (IBAction)PraiseButton:(id)sender {
    
}


//分享朋友圈
- (IBAction)ShareBUtton1:(id)sender {
    
}

//分享朋QQ空间
- (IBAction)ShareBUtton2:(id)sender {
    
}

//分享新浪微博
- (IBAction)ShareBUtton3:(id)sender {
    
}





@end
