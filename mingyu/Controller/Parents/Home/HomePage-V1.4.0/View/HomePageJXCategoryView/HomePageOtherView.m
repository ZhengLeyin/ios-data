//
//  HomePageOtherView.m
//  mingyu
//
//  Created by MingYu on 2018/12/24.
//  Copyright © 2018 TZWY. All rights reserved.
//
/*除--推荐View--以外其他View*/
#import "HomePageOtherView.h"
#import "HomePageViewModel.h"
#import "HomePageModel.h"
#import "HomePageRemcommendOfAlbumCell.h"
#import "SideBySideVideosCell.h"
#import "HomePageMoreViewController.h"
@interface HomePageOtherView ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic, strong) HomePageViewModel *homePageVM;
@property(nonatomic, strong) NSMutableArray *sectionArray;
@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, strong) NSMutableArray *objectListArray;
@end

@implementation HomePageOtherView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor whiteColor];
        //self.tableView.tableFooterView = [UIView new];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [self addSubview:self.tableView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.tableView.frame = self.bounds;
    [self.homePageVM getKHomePageMenuId:self.menu_id];
    __weak typeof(self) weakSelf = self;
    self.homePageVM.returnReturnDataBlock = ^(NSArray * _Nonnull data) {
        for (HomePageData *pagedate  in data) {
            [weakSelf.sectionArray addObject:pagedate];
            NSMutableArray *mutarray = [NSMutableArray new];
            for (Submenumessagelist *listmodel in pagedate.subMenuMessageList) {
                if (listmodel.type == 1 && listmodel.objectList.count > 1) {
                        [mutarray addObject:listmodel];
                } else {
                    for (Objectlist *model in listmodel.objectList) {
                        model.type = listmodel.type;
                        [mutarray addObject:model];
                    }
                }
            }
            [weakSelf.dataArray addObject:mutarray];
        }

        [weakSelf.tableView reloadData];
          MingYuLoggerI(@"data===%ld   === %ld ",weakSelf.sectionArray.count,weakSelf.dataArray.count);
    };
}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionArray.count;
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ZPLog(@"section------%ld",[self.dataArray[section] count]);
    return [self.dataArray[section] count];
}

/**1.视频 2.资讯 3.主题(一键听) 4.课程 5.育儿必读*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    if (self.dataArray && self.dataArray.count > indexPath.section && [self.dataArray[indexPath.section] count] > indexPath.row) {
        Submenumessagelist *listmodel = self.dataArray[indexPath.section][indexPath.row];
        Objectlist *objectmodel = self.dataArray[indexPath.section][indexPath.row];
        
        if (listmodel.type == 1) {
            if (listmodel.objectList.count > 1) {
                SideBySideVideosCell *cell =  [SideBySideVideosCell cellWithTableView:tableView];
                NSMutableArray *arr = [NSMutableArray array];
                [arr addObjectsFromArray:listmodel.objectList];
                cell.dataArray = arr;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            } else {
                ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
                cell.homePageVCOrOtherVCType = 1;
                cell.videomodel = (VideoModel *)objectmodel;
                cell.play_image.hidden = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        } else if (listmodel.type == 2) {
            if (objectmodel.news_type == 1) { //资讯
                ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
                cell.newsmodel = (NewsModel *)objectmodel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            } else { //育儿必读
                ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
                cell.newsmodel = (NewsModel *)objectmodel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        } else if (listmodel.type == 3) {
            HomePageRemcommendOfAlbumCell *cell = [HomePageRemcommendOfAlbumCell theHomePageRemcommendOfAlbumCell:tableView];
            cell.subjectModel = (SubjectModel *)objectmodel;
           
            return cell;
        } else if (listmodel.type == 4) {
            ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
            cell.coursemodel = (CourseModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else if (listmodel.type == 5) {
            ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
            cell.newsmodel = (NewsModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else {
            ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
            cell.newsmodel = (NewsModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    } else {
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
       // cell.newsmodel = (NewsModel *)objectmodel;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.sectionArray && self.sectionArray.count > section) {
        HomePageData *model = self.sectionArray[section];
        ParentHomeCell *header =  [ParentHomeCell TheHeaderView];
        header.backgroundColor = [UIColor whiteColor];
        header.header_title_lab.text = model.menu_name;
        header.MoreButtonBlock = ^{
            HomePageMoreViewController *VC = [HomePageMoreViewController new];
            VC.title = model.menu_name;
             [[self viewController].navigationController pushViewController:VC animated:YES];
        };
        return header;
    }else {
        return  [UIView new];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *array = self.dataArray[indexPath.section];
    Submenumessagelist *listmodel = array[indexPath.row];
    if (listmodel.type == 1) {
        if (listmodel.objectList.count > 1) {
            return 110;
        } else {
            return 230;
        }
    } else if (listmodel.type == 2) {
        return 130;
    } else if (listmodel.type == 3) {
        return 130;
    } else if (listmodel.type == 4) {
        return 155;
    } else if (listmodel.type == 5) {
        return 130;
    } else {
        return 0;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.dataArray && self.dataArray.count > indexPath.section && [self.dataArray[indexPath.section] count] > indexPath.row) {
        Submenumessagelist *listmodel = self.dataArray[indexPath.section][indexPath.row];
        Objectlist *objectmodel = self.dataArray[indexPath.section][indexPath.row];
        
        if (listmodel.type  == 1) {
            if (listmodel.objectList.count > 1) {
                
            }else {
                VideoModel *model = (VideoModel *)objectmodel;
                ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
                VC.video_id = model.video_id;
                VC.video_id = 1425;
                [[self viewController].navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 2) {/**单独处理*/
            if (objectmodel.news_type == 1) {
                ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
                VC.newsmodel = (NewsModel *)objectmodel;
                 [[self viewController].navigationController pushViewController:VC animated:YES];
            } else {
                ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
                VC.newsmodel = (NewsModel *)objectmodel;
                 [[self viewController].navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 3) {
            SubjectModel *model = (SubjectModel *)objectmodel;
            AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
            VC.target_id = model.subject_id;
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }else if (listmodel.type  == 4) {
            CourseModel *model = (CourseModel *)objectmodel;
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                 [[self viewController].navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                 [[self viewController].navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 5) {
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = (NewsModel *)objectmodel;
             [[self viewController].navigationController pushViewController:VC animated:YES];
        }else {
            
        }
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.scrollCallback(scrollView);
}


#pragma mark - JXPagingViewListViewDelegate

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (UIView *)listView {
    return self;
}

-(HomePageViewModel *)homePageVM {
    if (!_homePageVM) {
        _homePageVM = [HomePageViewModel new];
    }
    return  _homePageVM;
}

-(NSMutableArray *)sectionArray {
    if (!_sectionArray) {
        _sectionArray = [NSMutableArray array];
    }
    return _sectionArray;
}

-(NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(NSMutableArray *)objectListArray {
    if (!_objectListArray) {
        _objectListArray = [NSMutableArray array];
    }
    return _objectListArray;
}

@end
