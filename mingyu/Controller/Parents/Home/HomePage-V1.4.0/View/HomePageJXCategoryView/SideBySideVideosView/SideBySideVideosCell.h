//
//  SideBySideVideosCell.h
//  mingyu
//
//  Created by MingYu on 2018/12/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePageMoreModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SideBySideVideosCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property(nonatomic, strong)NSMutableArray *dataArray;
@end

NS_ASSUME_NONNULL_END
