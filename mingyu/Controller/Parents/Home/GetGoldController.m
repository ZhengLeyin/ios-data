//
//  GetGoldController.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "GetGoldController.h"

@interface GetGoldController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *HeaderArray;
@property (nonatomic, strong) NSMutableArray *ListArray;



@end

@implementation GetGoldController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
                    
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getTaskList];
}

- (void)getTaskList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetTaskList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        _HeaderArray = [NSMutableArray array];
        _ListArray = [NSMutableArray array];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            NSArray *taskMessageList = data[@"taskMessageList"];
            for (NSDictionary *dic in taskMessageList) {
                NSString *task_type = [NSString stringWithFormat:@"%@",dic[@"task_type"]];
                NSInteger platform_type = [dic[@"platform_type"] integerValue];
                if (![self.HeaderArray containsObject:task_type] && platform_type == 1) {
                    [_HeaderArray addObject:task_type];
                }
            }
            for (int i = 0; i < self.HeaderArray.count; i++) {
                NSMutableArray * array2 = [NSMutableArray new];
                for (NSDictionary *dic2 in taskMessageList) {
                    NSString *task_type = [NSString stringWithFormat:@"%@",dic2[@"task_type"]];
                    if ([_HeaderArray[i] isEqualToString:task_type]) {
                        QianDaoTaskModel *model = [QianDaoTaskModel modelWithJSON:dic2];
                        if (model.platform_type == 1) {
                            [array2 addObject:model];
                        }
                    }
                }
                [self.ListArray addObject:array2];
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.HeaderArray && self.HeaderArray.count > 0) {
        return _HeaderArray.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return [self.ListArray[section] count];

    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lab = [[UILabel alloc]init];
    lab.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    lab.font = FontSize(11);
    NSString *title;
    if (section == 0) {
        title = @"     新手任务";
    } else if (section == 1) {
        title = @"     每日任务";
    } else{
        title = @"     其他任务";
    }
    lab.text = title;
    return lab;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    // 从重用队列里查找可重用的cell
    GetGoldCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[GetGoldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (_ListArray && _ListArray.count > indexPath.section) {
        QianDaoTaskModel *model = self.ListArray[indexPath.section][indexPath.row];
        cell.model = model;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_ListArray && _ListArray.count > indexPath.section) {
        QianDaoTaskModel *model = self.ListArray[indexPath.section][indexPath.row];
        if ( model.is_complete_true) {
            return;
        }
        if (model.module_type == 1) {
            UserInfoViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"UserInfoViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.module_type == 2) {
            MineInterestViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"MineInterestViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.module_type == 3) {
            ChangeNameViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"ChangeNameViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.module_type == 4) {
            SetPasswordViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"SetPasswordViewController"];
            VC.title = @"设置密码";
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.module_type == 5) {
            [self.navigationController pushViewController:[HandPickViewController new] animated:YES];
        } else if (model.module_type == 6) {
            ParentVideoViewController *VC = [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoViewController"];
            VC.is_task = YES;
            [self.navigationController pushViewController:VC animated:YES];
        } else if (model.module_type == 7) {
            ParentCourseViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentCourseViewController"];
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}


//
//- (NSMutableArray *)ListArray{
//    if (_ListArray == nil) {
//        _ListArray = [NSMutableArray array];
//    }
//    return _ListArray;
//}
//
//
//- (NSMutableArray *)HeaderArray{
//    if (_HeaderArray == nil) {
//        _HeaderArray = [NSMutableArray array];
//    }
//    return _HeaderArray;
//}


-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-44) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = 51;
        
    }
    return _tableView;
}

- (UIScrollView *)scrollView{
    return self.tableView;
}


@end


@implementation GetGoldCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 初始化子视图
        [self initLayout];
    }
    return self;
}


- (void)initLayout
{
    self.goldLabel = [[UILabel alloc] initWithFrame:CGRectMake((kScreenWidth-40)/2, 0, 40, self.height)];
    self.goldLabel.font = FontSize(16);
    self.goldLabel.textAlignment = NSTextAlignmentCenter;
   // self.goldLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.goldLabel];
    
    self.titleLab = [UILabel new];
    self.titleLab.font = FontSize(kScale(14));
   // self.titleLab.backgroundColor = [UIColor purpleColor];
    self.titleLab.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.contentView).offset(0);
        make.right.equalTo(self.goldLabel.mas_left).offset(0);
        make.height.mas_equalTo(self.goldLabel);
    }];
   
    self.stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(kScreenWidth-100, 0, 80, self.height)];
    self.stateLabel.font = FontSize(14);
    self.stateLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.stateLabel];

    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(15, 50.5, kScreenWidth, 0.5)];
    line.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    [self.contentView addSubview:line];
}


- (void)setModel:(QianDaoTaskModel *)model{
    self.titleLab.text = model.task_name;
    self.goldLabel.text = [NSString stringWithFormat:@"+%ld",(long)model.task_gold];
    if (model.is_complete_true) {
        self.goldLabel.textColor = [UIColor colorWithHexString:@"CECECE"];
        self.stateLabel.textColor = [UIColor colorWithHexString:@"CECECE"];
        self.stateLabel.text = @"已完成";
    } else {
        self.goldLabel.textColor = [UIColor colorWithHexString:@"F7A429"];
        self.stateLabel.textColor = [UIColor colorWithHexString:@"F7A429"];
        self.stateLabel.text = @"去完成";
    }
}



@end

