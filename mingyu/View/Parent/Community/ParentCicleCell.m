//
//  ParentCicleCell.m
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentCicleCell.h"

@implementation ParentCicleCell


+ (instancetype)theParentCicleCellwithtableView:(UITableView *)tableView{
    static NSString *identify = @"ParentCicleCell";
    ParentCicleCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ParentCicleCell" owner:self options:nil][0];
        cell.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    }
    return cell;
}


+(instancetype)theHeaderView{
    ParentCicleCell *headerview = [[NSBundle mainBundle] loadNibNamed:@"ParentCicleCell" owner:nil options:nil][1];
    return headerview;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
//
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_user_head_Image.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:_user_head_Image.bounds.size];
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = _user_head_Image.bounds;
//    maskLayer.path = maskPath.CGPath;
//    _user_head_Image.layer.mask = maskLayer;
    
    CGRect bounds = CGRectMake(0, 0, _user_head_Image.size.width, _user_head_Image.size.height);
    [self imageWithRoundCorner:bounds];
    
    _topic_head_Image.contentMode = UIViewContentModeScaleAspectFill;
 
    _topic_head_Image.layer.cornerRadius = 5;
    _topic_head_Image.layer.borderWidth = 0.5;
    _topic_head_Image.layer.borderColor = [UIColor colorWithHexString:@"e2e2e2"].CGColor;
    _topic_head_Image.layer.masksToBounds = YES;
}


- (void)imageWithRoundCorner:(CGRect)rect  {
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    dispatch_async(backgroundQueue, ^{
        CGRect pathRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
        UIBezierPath *bezier = [UIBezierPath bezierPathWithRoundedRect:pathRect byRoundingCorners:UIRectCornerAllCorners cornerRadii:rect.size];
        CGPathRef path = bezier.CGPath;
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
        CGContextRef context =  UIGraphicsGetCurrentContext();
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGContextDrawImage(context, pathRect, _user_head_Image.image.CGImage);
        // UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //        dispatch_async(mainQueue, ^{
        //            CALayer *imageLayer = [CALayer layer];
        //            imageLayer.frame = rect;
        //            imageLayer.contents = (__bridge id)image.CGImage;
        //            //[self.contentView.layer addSublayer:imageLayer];
        //        });
    });
    
}

- (void)setNewsmodel:(NewsModel *)newsmodel{
    _newsmodel = newsmodel;

//    NSString *headimageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_newsmodel.user_head];
    [_user_head_Image zp_setImageWithURL:_newsmodel.user_head placeholderImage:ImageName(@"默认头像")];
    _user_name_Lab.text = _newsmodel.user_name;
    if (_newsmodel.is_top_true){
        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_newsmodel.news_title]];
        //NSTextAttachment可以将要插入的图片作为特殊字符处理
        NSTextAttachment *attch = [[NSTextAttachment alloc] init];
        //定义图片内容及位置和大小
        attch.image = [UIImage imageNamed:@"置顶"];
        attch.bounds = CGRectMake(0, -4, 31, 20);
        //创建带有图片的富文本
        NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
        [attri insertAttributedString:string atIndex:0];
        //用label的attributedText属性来使用富文本
        _topic_title_Lab.attributedText = attri;
    } else {
        _topic_title_Lab.text = _newsmodel.news_title;
    }

    if (_newsmodel.news_head.length > 0) {
        _TitleLabConstraintRight.constant = 115;
        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_newsmodel.news_head];
        _topic_head_Image.hidden = NO;
        [_topic_head_Image sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
    } else {
        _TitleLabConstraintRight.constant = 15;
//        [_topic_head_Image sd_setImageWithURL:nil placeholderImage:nil];
        _topic_head_Image.hidden = YES;
    }
}


//搜索时用到
- (void)showSearch{
    NSString *title = _newsmodel.news_title;
    title = [@"<font style=\"font-size:15px\" color=\"#2C2C2C\">" stringByAppendingString:title];
    title = [title stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
    title = [title stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
    title = [title stringByAppendingString:@"</font>"];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _topic_title_Lab.attributedText = attrStr;
    _topic_title_Lab.lineBreakMode = NSLineBreakByTruncatingTail;

    
//    NSString *content = _topicmodel.topic_abstract;
//    content = [@"<font style=\"font-size:11px\" color=\"#A8A8A8\">" stringByAppendingString:content];
//    content = [content stringByReplacingOccurrencesOfString:@"<h2>" withString:@"<font color=\"#50D0F4\">"];
//    content = [content stringByReplacingOccurrencesOfString:@"</h2>" withString:@"</font>"];
//    content = [content stringByAppendingString:@"</font>"];
//
//    NSAttributedString * attrStr2 = [[NSAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _topic_abstract_Lab.attributedText = attrStr2;
//    _topic_abstract_Lab.lineBreakMode = NSLineBreakByTruncatingTail;

}


- (IBAction)disLikeButton:(id)sender {
    if (self.dislikeblock) {
        self.dislikeblock(_newsmodel);
    }
}

-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}

@end
