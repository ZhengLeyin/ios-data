//
//  TeacherIntroduceCell.m
//  mingyu
//
//  Created by MingYu on 2018/11/21.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "TeacherIntroduceCell.h"

@implementation TeacherIntroduceCell

+ (instancetype)theTeacherIntroduceCellWithtableView:(UITableView *)tableview {
    static NSString *cellid = @"TeacherIntroduceCell";
    TeacherIntroduceCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"TeacherIntroduceCell" owner:nil options:nil][0];
    }
    return cell;
}

+ (instancetype)theTeacherIntroduceCellOfDateilWithtableView:(UITableView *)tableview {
    static NSString *cellid = @"TeacherIntroduceCell";
    TeacherIntroduceCell *cell = [tableview dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"TeacherIntroduceCell" owner:nil options:nil][1];
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _lineBackView.hidden = YES;
    
    _followButton.layer.cornerRadius = 3;
    _followButton.layer.masksToBounds = YES;
    
    _followButton.layer.borderColor = [UIColor colorWithHexString:@"#50D0F4"].CGColor;
    _followButton.layer.borderWidth = 1;
    
    _teacherUserImageView.layer.cornerRadius = 35;
    _teacherUserImageView.layer.masksToBounds = YES;
    
    
    /* 添加一个轻拍手势 */
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    [[tap rac_gestureSignal] subscribeNext:^(id x) {
        self.pushTeacherDetailsBlock();
    }];
    [_teacherIntroduceView addGestureRecognizer:tap];
    
    [_followButton addTarget:self action:@selector(followButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
}

/**关注按钮*/
- (IBAction)followButtonAction:(UIButton *)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
     _followButton.userInteractionEnabled = NO;
    if (_model.follow_status == 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id": @(_model.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
             _followButton.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger follow_status = [json[@"data"] integerValue];
                _model.follow_status = follow_status;
                [self changefollowButtonState:_model];
            } else {
                //                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
             _followButton.userInteractionEnabled = YES;
            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    } else{
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelFollow];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"follow_user_id":@(_model.fk_user_id)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
             _followButton.userInteractionEnabled = YES;
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _model.follow_status = 1;
                [self changefollowButtonState:_model];
            } else {
                //                [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
            }
        } failure:^(NSError *error) {
            _followButton.userInteractionEnabled = YES;
            //            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
        }];
    }
}

/** 传值 */
-(void)setModel:(CourseModel *)model {
    _model = model;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_model.user_head];
    [self.teacherUserImageView sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"默认头像")];
    _teacherNameLabel.text = model.teacher_name;
    _teacherProfessionalLabel.text = [NSString stringWithFormat:@"%@ %@",model.teacher_organize,model.teacher_position];
    [self changefollowButtonState:model];
    if (_model.fk_user_id == [USER_ID integerValue]) {
        _followButton.hidden = YES;
    } else {
        _followButton.hidden = NO;
    }
}

//根据状态判断是否已关注
- (void)changefollowButtonState:(CourseModel *)model {
    if (model.follow_status == 1) {
         _followButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [ _followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [ _followButton setTitle:@" + 关注 " forState:UIControlStateNormal];
         _followButton.backgroundColor = [UIColor colorWithHexString:@"50D0F4"];
    } else if (model.follow_status == 2) {
         _followButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [ _followButton setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [ _followButton setTitle:@" 已关注 " forState:UIControlStateNormal];
         _followButton.backgroundColor = [UIColor whiteColor];
    } else {
         _followButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        [ _followButton setTitleColor:[UIColor colorWithHexString:@"E2E2E2"] forState:UIControlStateNormal];
        [ _followButton setTitle:@" 相互关注 " forState:UIControlStateNormal];
         _followButton.backgroundColor = [UIColor whiteColor];
    }
}



-(void)followButtonAction {
    self.followButtonBlock();
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
