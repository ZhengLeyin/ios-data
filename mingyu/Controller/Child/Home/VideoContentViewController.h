//
//  VideoContentViewController.h
//  mingyu
//
//  Created by apple on 2018/3/31.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoBaseViewController.h"

typedef void(^selectContentCell)(NSInteger index);

@interface VideoContentViewController : VideoBaseViewController


@property (nonatomic, copy) selectContentCell block;

@end
