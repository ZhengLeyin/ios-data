//
//  ChildShowViewController.h
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

UIKIT_EXTERN NSNotificationName const ChildShowScrollViewDidScrollNSNotification;
UIKIT_EXTERN NSNotificationName const ChildShowScrollViewRefreshStateNSNotification;

@interface ChildShowViewController : UIViewController

@end
