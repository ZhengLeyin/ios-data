//
//  SearchNewCell.h
//  mingyu
//
//  Created by MingYu on 2018/11/9.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchNewCell;
NS_ASSUME_NONNULL_BEGIN
//@protocol SearchNewCellDelegate <NSObject>
///** 删除单个最近搜索回调 */
//- (void)searchNewCell:(SearchNewCell *)searchNewCell deleteHistoryButtonClick:(UIButton *)deleteButton;
//
//@end
@interface SearchNewCell : UITableViewCell

//@property (nonatomic ,weak) id<SearchNewCellDelegate>delegate;

/** 搜索内容BEL */
@property (weak, nonatomic) IBOutlet UILabel *searchContentBEL;
/** 删除Btn */
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
+(instancetype)theSearchNewCellWithTableView:(UITableView *)tableview;

@end

NS_ASSUME_NONNULL_END
