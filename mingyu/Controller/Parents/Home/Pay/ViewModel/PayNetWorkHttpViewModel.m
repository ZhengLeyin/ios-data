//
//  PayNetWorkHttpViewModel.m
//  mingyu
//
//  Created by MingYu on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "PayNetWorkHttpViewModel.h"

@implementation PayNetWorkHttpViewModel
- (void)GetAmountOptions {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAmountOptions];
    NSDictionary *parameters = @{
                                 @"type":@"2"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            
        } else {
            //[UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}
@end
