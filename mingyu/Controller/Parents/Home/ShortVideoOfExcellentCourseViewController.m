//
//  ShortVideoOfExcellentCourseViewController.m
//  mingyu
//
//  Created by MingYu on 2018/11/22.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ShortVideoOfExcellentCourseViewController.h"
#import "JXPagerView.h"

@interface ShortVideoOfExcellentCourseViewController ()<UITableViewDelegate,UITableViewDataSource,JXPagerViewListViewDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayData;

@end

@implementation ShortVideoOfExcellentCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        /**type == 1 精品课传过来的*/
        if (_type == 1) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 45);
        }else {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-effectViewH);;
        }
       
    }];
    [self getVideoTUserId];
}

#pragma mark /** 获取老师详情段视频 模拟数据 3 */
- (void)getVideoTUserId {
    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/?",KURL,KGetVideoTUserId,@(_coursemodel.fk_user_id)];

    ZPLog(@"URL=%@",URL);
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            [self.arrayData removeAllObjects];
            for (NSDictionary *dic in data) {
                VideoModel *model = [VideoModel modelWithJSON:dic];
                 [self.arrayData addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return _arrayData.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_arrayData && _arrayData.count > indexPath.row) {
        cell.videomodel = [_arrayData objectAtIndex:indexPath.row];
    }
    return cell;}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_arrayData.count > indexPath.row) {
        //[MobClick event:@"VideoBaseViewController" label:_labModel.label_name];  //友盟统计
        VideoModel *model = _arrayData[indexPath.row];
        ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
        VC.video_id = model.video_id;
        if (self.PushBlock) {
            self.PushBlock(VC);
        }
//        [self.navigationController pushViewController:VC animated:YES];
    }
    
}

- (UIScrollView *)scrollView{
    return self.tableView;
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 230;
    }
    return _tableView;
}


- (NSMutableArray *)arrayData{
    if (_arrayData == nil) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}


@end
