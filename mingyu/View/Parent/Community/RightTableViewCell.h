//
//  RightTableViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//
#import <UIKit/UIKit.h>
@class ParentCircleModel;

@protocol RightTableViewDelegate<NSObject>;



/**
 添加圈子
 @param circlemodel <#circlemodel description#>
 */
- (void)addCircle:(ParentCircleModel *)circlemodel;


/**
 退出圈子

 @param circlemodel <#circlemodel description#>
 */
- (void)quitCircle:(ParentCircleModel *)circlemodel;


@end

@interface RightTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *circle_img;

@property (weak, nonatomic) IBOutlet UILabel *circle_name_Lab;

@property (weak, nonatomic) IBOutlet UILabel *circle_abstract_Lab;

@property (weak, nonatomic) IBOutlet UIButton *add_button;

@property (weak, nonatomic) IBOutlet UIButton *quit_button;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConstraintRight;

@property (nonatomic, strong) ParentCircleModel *circlemodel;


@property (nonatomic, weak) id <RightTableViewDelegate> delegate;

@property (nonatomic, copy) void (^addButonBlock)(ParentCircleModel *circlemodel);

+ (instancetype)theCircleCellWithTableView:(UITableView *)tableview;



- (void)showSearch;
@end
