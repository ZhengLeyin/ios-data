//
//  ParentChildCareCell.h
//  mingyu
//
//  Created by apple on 2018/5/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel;

@interface ParentChildCareCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *childcare_title_Lab;


@property (weak, nonatomic) IBOutlet UILabel *page_view_lab;

@property (weak, nonatomic) IBOutlet UIImageView *childcare_head_Image;



@property (nonatomic, strong) NewsModel *newsmodel;



+ (instancetype)theChildCareCellWithtableView:(UITableView *)tableview;


/**
 搜索时使用
 */
- (void)showSearch;

@end
