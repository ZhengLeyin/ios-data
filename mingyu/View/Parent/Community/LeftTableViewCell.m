//
//  LeftTableViewCell.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LeftTableViewCell.h"

@interface LeftTableViewCell ()

@end

@implementation LeftTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.chooseView.hidden = YES;
    self.classify_name_Lab.textColor = [UIColor colorWithHexString:@"A8A8A8"];
    self.classify_name_Lab.highlightedTextColor = [UIColor colorWithHexString:@"50D0F4"];

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state

    self.classify_name_Lab.highlighted = selected;
    self.chooseView.hidden = !selected;
}

@end
