//
//  PurchasedCourseViewController.m
//  mingyu
//
//  Created by MingYu on 2018/12/7.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "PurchasedCourseViewController.h"
#import "MayBeInterestedView.h"
#import "ParentCouresCell.h"
@interface PurchasedCourseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)  UITableView *tableView;
/**感兴趣的数组*/
@property (nonatomic, strong) NSMutableArray *arrayInterestData;
/**购买数组*/
@property (nonatomic, strong) NSMutableArray *arrayPurchasedData;
/**购买Section时间日期数组*/
@property (nonatomic, strong) NSMutableArray *arrayPurchasedSectionData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;
@end

@implementation PurchasedCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"已购课程";
    [self.view addSubview:self.tableView];
    [self setLeftNavigationItem];
    [self refreshTableview];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
}

/** 返回上一层 */
-(void)setLeftNavigationItem {
    UIImage *leftImage = [[UIImage imageNamed:@"Combined Shape Back"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
}

-(void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

//刷新
-(void)refreshTableview {
    _start = 0;
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
                if (self.arrayInterestData.count>0) {
                    [self.arrayInterestData removeAllObjects];
                    [self.arrayPurchasedSectionData removeAllObjects];
                    _start = 0;
                }
        [_tableView.mj_footer resetNoMoreData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getCourseUserList];  //加载cell数据
        });
    }];
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getCourseUserList];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
    
}

/** 已购课程 */
-(void)getCourseUserList {

    NSString *URL = [NSString stringWithFormat:@"%@%@/%@?",KURL, KGetCourseUser,[NSString stringWithFormat:@"%@",USER_ID]];
    NSDictionary *parameters = @{
                                 @"start":@(_start),
                                 @"size":@"20"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        BOOL succsess = [json[@"success"]boolValue];
        NSArray *courseListArr = json[@"data"][@"courseList"];
        NSArray *courseMessageListArr = json[@"data"][@"courseMessageList"];
        if (succsess) {
            /** 感兴趣 */
            if (courseListArr && courseListArr .count > 0) {
                [self.arrayInterestData removeAllObjects];
                                for (NSDictionary *dic in courseListArr) {
                                    CourseModel *model = [CourseModel modelWithJSON:dic];
                                    [self.arrayInterestData addObject:model];
                                }
            }
             /** 已购买课程 */
            if (courseMessageListArr && courseMessageListArr .count > 0) {
                            _start += 20;
                            if (courseMessageListArr && courseMessageListArr.count == 20) {
                                _lastPage = NO;
                            } else {
                                _lastPage = YES;
                            }
                for (NSDictionary *dic in json[@"data"][@"courseMessageList"]) {
                    CourseModel *model = [CourseModel modelWithJSON:dic];
                    [self.arrayPurchasedSectionData addObject:model];
                    NSMutableArray *array = [NSMutableArray array];
                    for (NSDictionary *dict in dic[@"courseList"]) {
                        CourseModel *model = [CourseModel modelWithJSON:dict];
                        [array addObject:model];
                    }
                    [self.arrayPurchasedData addObject:array];
                }
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        // [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.arrayPurchasedSectionData && self.arrayPurchasedSectionData.count > 0) {
        return self.arrayPurchasedSectionData.count;
    }else {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.arrayPurchasedData && self.arrayPurchasedData.count > section) {
        return [self.arrayPurchasedData[section] count];
    }else {
        return self.arrayInterestData.count;
        //return 2;
    }
}

/**已购课程+感兴趣的课程  二者互斥*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.arrayPurchasedSectionData && self.arrayPurchasedSectionData.count > 0) {
        ParentCouresCell *cell = [ParentCouresCell theCellWithPurchaseCourseTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.arrayPurchasedData && self.arrayPurchasedData.count > indexPath.section && [self.arrayPurchasedData[indexPath.section] count] > indexPath.row) {
            CourseModel *model = self.arrayPurchasedData[indexPath.section][indexPath.row];
            [cell purchasedCourseViewControllerPushModel:model];
        }
        return cell;
    }else {
        ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.arrayInterestData && self.arrayInterestData.count > indexPath.row) {
            CourseModel *model = self.arrayInterestData[indexPath.row];
            cell.coursemodel = model;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.arrayPurchasedSectionData && self.arrayPurchasedSectionData.count > section) {
        CourseModel *model = self.arrayPurchasedSectionData[section];
        UIView *view = [UIView new];
        UILabel *dateLabel = [UILabel new];
        dateLabel.frame = CGRectMake(15, 10, 100, 20);
        dateLabel.font = FontSize(18.0);
        if (model.month == 0) {
             dateLabel.text = [NSString stringWithFormat:@"%ld年",model.year];
        }else {
             dateLabel.text = [NSString stringWithFormat:@"%ld年%ld月",model.year,model.month];
        }
        dateLabel.textColor = [UIColor colorWithHexString:@"#2C2C2C"];
        [view addSubview:dateLabel];
        return view;
    }else {
        MayBeInterestedView *view = [MayBeInterestedView theMayBeInterestedView];
        return view;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.arrayPurchasedSectionData && self.arrayPurchasedSectionData.count > indexPath.section) {
        return 120;
    }else {
        return 155;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.arrayPurchasedSectionData && self.arrayPurchasedSectionData.count > section) {
        return 30;
    }else {
        return 300;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /**感兴趣跳转*/
    if (self.arrayInterestData.count > 0) {
        if (self.arrayInterestData && self.arrayInterestData.count > indexPath.row) {
            CourseModel *model = [self.arrayInterestData objectAtIndex:indexPath.row];
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
    
    /**已购买跳转*/
    if (self.arrayPurchasedSectionData.count > 0) {
        if (self.arrayPurchasedData && self.arrayPurchasedData.count > indexPath.section && [self.arrayPurchasedData[indexPath.section] count] > indexPath.row) {
           CourseModel *model = self.arrayPurchasedData[indexPath.section][indexPath.row];
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
}

-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - NaviH - effectViewH) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //_tableView.estimatedRowHeight = 0;
    }
    return _tableView;
}

-(NSMutableArray *)arrayInterestData {
    if (!_arrayInterestData) {
        _arrayInterestData = [NSMutableArray array];
    }
    return _arrayInterestData;
}

-(NSMutableArray *)arrayPurchasedData {
    if (!_arrayPurchasedData) {
        _arrayPurchasedData = [NSMutableArray array];
    }
    return _arrayPurchasedData;
}

-(NSMutableArray *)arrayPurchasedSectionData {
    if (!_arrayPurchasedSectionData) {
        _arrayPurchasedSectionData = [NSMutableArray array];
    }
    return _arrayPurchasedSectionData;
}

@end
