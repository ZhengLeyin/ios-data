//
//  VideoHeaderView.h
//  mingyu
//
//  Created by apple on 2018/4/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^DownBlock)(void);


@interface VideoHeaderView : UIView


@property (strong, nonatomic)  UILabel *video_titleLab;

@property (strong, nonatomic)  UILabel *video_abstractLab;

//@property (strong, nonatomic)  UILabel *play_countLab;

//@property (nonatomic, strong) UIButton *downButton;

@property (nonatomic, strong) UIButton *collectButton;



@property (nonatomic, copy) DownBlock downblock;


@property (nonatomic, strong) CourseModel *coursemodel;

/**特权课View*/
@property (nonatomic, strong) UIView *prerogativeView;

@property (nonatomic, strong) UIImageView *prerogativeImageView;

@property (nonatomic, strong) UILabel *prerogativeLabel;

@property (nonatomic, strong) UIButton *prerogativeButton;

@end
