//
//  PrivateMessageDetailViewController.m
//  mingyu
//
//  Created by apple on 2018/8/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "PrivateMessageDetailViewController.h"
#import "MessageDetailCell.h"
#import "MessageModelFrame.h"
#import "QQMessageModel.h"
#import "QQUserModel.h"
#import "UIView+CLSetRect.h"
#import "PPBadgeView.h"

@interface PrivateMessageDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
{
    CGFloat showTableViewH;
}

//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//
//@property (weak, nonatomic) IBOutlet UIView *bottomView;
//@property (weak, nonatomic) IBOutlet UITextView *textField;
//@property (weak, nonatomic) IBOutlet UIButton *sendMessageButton;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewH;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *bottomView;
/**顶部线条*/
@property (nonatomic, strong) UIView *topLine;
/**边框*/
@property (nonatomic, strong) UIView *edgeLineView;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIButton *sendMessageButton;


@property (nonatomic, strong) NSMutableArray *arrayData;

/**键盘高度*/
@property (nonatomic, assign) CGFloat keyboardHeight;

@end

@implementation PrivateMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar pp_hiddenBadge];

    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    [self setupSubView];
    // 监听键盘出现的出现和消失
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getQQMessageByQQId:) name:NSNotificationReceivePriVateMessage object:nil];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationReceivePriVateMessage object:nil];
}

- (void)setupSubView{
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-NaviH-40-effectViewH, kScreenWidth, 40)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bottomView];
    
    self.textView = [[UITextView alloc] init];
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.CLwidth = self.bottomView.CLwidth - 50 - 46;
    self.textView.CLleft = 18;
    self.textView.font = [UIFont systemFontOfSize:12];
    CGFloat lineH = self.textView.font.lineHeight;
    self.bottomView.CLheight = ceil(lineH) + 15 + 15;
    self.bottomView.CLy = kScreenHeight-NaviH-effectViewH-self.bottomView.CLheight;
    self.textView.CLheight = lineH;
    self.textView.CLcenterY = self.bottomView.CLheight * 0.5;
    self.textView.enablesReturnKeyAutomatically = YES;
    self.textView.delegate = self;
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    self.textView.scrollsToTop = NO;
    self.textView.textContainerInset = UIEdgeInsetsZero;
    self.textView.textContainer.lineFragmentPadding = 0;
    [self.bottomView addSubview:self.textView];
    
    //边框
    self.edgeLineView = [[UIView alloc]init];
    self.edgeLineView.CLwidth = self.bottomView.CLwidth - 50 - 30;
    self.edgeLineView.CLleft = 10;
    self.edgeLineView.CLheight = self.textView.CLheight +12;
    self.edgeLineView.CLcenterY = self.bottomView.CLheight * 0.5;
    self.edgeLineView.layer.cornerRadius = 10;
    self.edgeLineView.backgroundColor = [UIColor colorWithHexString:@"F6F6F6"];
    self.edgeLineView.layer.masksToBounds = YES;
    [self.bottomView addSubview:self.edgeLineView];
    
    [self.bottomView bringSubviewToFront:self.textView];
    
    self.sendMessageButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bottomView.CLwidth - 50 - 10, self.bottomView.CLheight - 30 - 10, 50, 30)];
    self.sendMessageButton.CLcenterY = self.bottomView.CLheight * 0.5;
    self.sendMessageButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.sendMessageButton setTitle:@"发送" forState:UIControlStateNormal];
    self.sendMessageButton.userInteractionEnabled = NO;
    [self.sendMessageButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.sendMessageButton addTarget:self action:@selector(sendMess:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.sendMessageButton];
    
    //顶部线条
    self.topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bottomView.CLwidth, 0.5)];
    self.topLine.backgroundColor = [UIColor colorWithHexString:@"E2E2E2"];
    [self.bottomView addSubview:self.topLine];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH-self.bottomView.height) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];

    UIView *headView = [UIView new];
    headView.frame = CGRectMake(0, 0, kScreenWidth, 10);
    headView.backgroundColor = [UIColor clearColor];
    [self.tableView setTableHeaderView:headView];

    UIView *bgview = [[UIView alloc] initWithFrame:self.tableView.bounds];
    bgview.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    [self.tableView setBackgroundView:bgview];
    
    [self.view bringSubviewToFront:self.bottomView];
    
    [self getQQMessageByQQId:nil];
}


- (void)getQQMessageByQQId:(NSNotification *)noti{
    if (noti && [noti.userInfo[@"fk_from_id"] integerValue] != _qq_id) {
        return;
    }
    
    [self.arrayData removeAllObjects];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetQQMessageByQQId];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"to_user_id":@(_user_id),
                                 @"qq_id":@(_qq_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                QQMessageModel *model = [QQMessageModel modelWithJSON:dic];
                ZPLog(@"%@",model.message_content);
                model.user_header = _user_head;
                BOOL isEquel = false;

                MessageModelFrame *frameModel=[[MessageModelFrame alloc]initWithFrameModel:model timeIsEqual:isEquel];//将模型里面的数据转为Frame,并且判断时间是否相等
                [self.arrayData addObject:frameModel];//添加Frame的模型到数组里面
            }
            [self.tableView reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(self.arrayData && self.arrayData.count > 0){
                    NSIndexPath *path=[NSIndexPath indexPathForItem:self.arrayData.count-1 inSection:0];
                    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:NO];//将tableView的行滚到最下面的一行
                }
            });
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark  -TableView的DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.arrayData && self.arrayData.count > 0) {
        return self.arrayData.count;
    }
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        MessageModelFrame *frameModel = self.arrayData[indexPath.row];
        return frameModel.cellHeight;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageDetailCell *cell = [MessageDetailCell theMessageDetailCellWithTableView:tableView];
    if (self.arrayData && self.arrayData.count > indexPath.row) {
        cell.frameModel = self.arrayData[indexPath.row];
    }
    return cell;
}


#pragma mark 键盘将要出现
-(void)keyboardWillShow:(NSNotification *)notifa{
    [self dealKeyboardFrame:notifa];
    
    CGRect keyboardFrame = [notifa.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardFrame.origin.y;
    CGFloat duration = [notifa.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.bottomView.CLy = keyboardFrame.origin.y - self.bottomView.CLheight-NaviH;
        self.tableView.frame=CGRectMake(0, 0, kScreenWidth, self.bottomView.CLy);

    }];
    if (self.arrayData && self.arrayData.count > 0) {
        NSIndexPath *path=[NSIndexPath indexPathForItem:self.arrayData.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];//将tableView的行滚到最下面的一行
    }
    
}
#pragma mark 键盘消失完毕
-(void)keyboardWillHide:(NSNotification *)notifa{
    [self dealKeyboardFrame:notifa];

    CGFloat duration = [notifa.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.bottomView.CLy = kScreenHeight-NaviH-effectViewH-self.bottomView.CLheight;
        self.tableView.frame=CGRectMake(0, 0, kScreenWidth, self.bottomView.CLy);

    }];
    if (self.arrayData && self.arrayData.count > 0) {
        NSIndexPath *path=[NSIndexPath indexPathForItem:self.arrayData.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];//将tableView的行滚到最下面的一行
    }
}

#pragma mark 处理键盘的位置
-(void)dealKeyboardFrame:(NSNotification *)changeMess{
    NSDictionary *dicMess=changeMess.userInfo;//键盘改变的所有信息
    CGFloat changeTime=[dicMess[@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];//通过userInfo 这个字典得到对得到相应的信息//0.25秒后消失键盘
    CGFloat keyboardMoveY=[dicMess[UIKeyboardFrameEndUserInfoKey]CGRectValue].origin.y-kScreenHeight;//键盘Y值的改变(字典里面的键UIKeyboardFrameEndUserInfoKey对应的值-屏幕自己的高度)
    [UIView animateWithDuration:changeTime animations:^{ //0.25秒之后改变tableView和bgView的Y轴
        self.tableView.frame=CGRectMake(0, 0, kScreenWidth, showTableViewH+keyboardMoveY);
        self.bottomView.transform=CGAffineTransformMakeTranslation(0, keyboardMoveY);
    }];
    
    if (self.arrayData && self.arrayData.count > 0) {
        NSIndexPath *path=[NSIndexPath indexPathForItem:self.arrayData.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];//将tableView的行滚到最下面的一行
    }
}


#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length == 0) {
        self.sendMessageButton.userInteractionEnabled = NO;
        [self.sendMessageButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }else{
        self.sendMessageButton.userInteractionEnabled = YES;
        [self.sendMessageButton setTitleColor:[UIColor colorWithHexString:@"5DD4F5"] forState:UIControlStateNormal];
    }
    
    if (textView.text.length > 1000) {
        textView.text = [textView.text substringToIndex:1000];
        [UIView ShowInfo:@"消息内容不能多于1000个字" Inview:self.view];
    }
    CGFloat contentSizeH = textView.contentSize.height;
    CGFloat lineH = textView.font.lineHeight;
    CGFloat maxTextViewHeight = ceil(lineH * 5 + textView.textContainerInset.top + textView.textContainerInset.bottom);
    if (contentSizeH <= maxTextViewHeight) {
        textView.CLheight = contentSizeH;
    }else{
        textView.CLheight = maxTextViewHeight;
    }
    self.bottomView.CLheight = ceil(textView.CLheight) + 15 + 15;
    self.bottomView.CLbottom = CLscreenHeight-NaviH - _keyboardHeight;
    self.edgeLineView.CLheight = self.textView.CLheight + 12;

    [textView scrollRangeToVisible:NSMakeRange(textView.selectedRange.location, 1)];
    
    self.bottomView.CLy = _keyboardHeight - self.bottomView.CLheight-NaviH;
    self.tableView.frame=CGRectMake(0, 0, kScreenWidth, self.bottomView.CLy);

    if (self.arrayData && self.arrayData.count > 0) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y)];
    }
}



#pragma mark 滚动TableView去除键盘
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.textView resignFirstResponder];
}


#pragma mark 发送消息,刷新数据
-(void)sendMess:(NSString *)messValues{
    messValues = [self.textView.text copy];
    messValues = [messValues stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (messValues.length < 1) {
        return;
    }
    _sendMessageButton.userInteractionEnabled = NO ;//设置发送按钮不可用
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddQQMessage];
    NSDictionary *parameters = @{
//                                 @"fk_qq_id":@(_userModel.qq_id),
                                 @"from_user_id": USER_ID,
                                 @"to_user_id": @(_user_id),
                                 @"message_content":self.textView.text
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters  viewcontroller:nil success:^(id responseObject) {
        _sendMessageButton.userInteractionEnabled = YES; //设置发送按钮可用
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            QQMessageModel *messageModel = [QQMessageModel modelWithJSON:json[@"data"]];

            BOOL isEquel = false;

            MessageModelFrame *frameModel=[[MessageModelFrame alloc]initWithFrameModel:messageModel timeIsEqual:isEquel];//将模型里面的数据转为Frame,并且判断时间是否相等
            [self.arrayData addObject:frameModel];//添加Frame的模型到数组里面

            [self.tableView reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(self.arrayData && self.arrayData.count > 0){
                    NSIndexPath *path=[NSIndexPath indexPathForItem:self.arrayData.count-1 inSection:0];
                    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:NO];//将tableView的行滚到最下面的一行
                    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:NO];//将tableView的行滚到最下面的一行
                }
            });

            self.textView.text = nil;
            [self textViewDidChange:self.textView];
             [self.textView becomeFirstResponder];
            self.sendMessageButton.userInteractionEnabled = NO;
            [self.sendMessageButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        _sendMessageButton.userInteractionEnabled = YES; //设置发送按钮可用
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

//#pragma mark 判断前后时间是否一致
//-(BOOL)timeIsEqual:(NSString *)comStrTime{
//
//    return NO;
//}

- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didMoveToParentViewController:(UIViewController*)parent{
    [super didMoveToParentViewController:parent];
    ZPLog(@"%s,%@",__FUNCTION__,parent);
    if(!parent){
//        [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationReceivePriVateMessage object:nil userInfo:nil];
        ZPLog(@"页面pop成功了");
    }
}



@end
