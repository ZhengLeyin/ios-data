//
//  CommentReplayCell.h
//  mingyu
//
//  Created by apple on 2018/8/20.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentReplayModel.h"

@protocol CommentReplayCellDelegate <NSObject>

@optional

/** 点赞 */
- (void)ReplayCellPraiseButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex;

/** 回复 */
- (void)ReplayCellReplayButton:(CommentReplayModel *)replaymodel CommentCellIndex:(NSInteger)commentindex ReplayCellIndex:(NSInteger)replayindex;

@end

@interface CommentReplayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *userHeadButton;

@property (weak, nonatomic) IBOutlet UILabel *replayContentLab;

@property (weak, nonatomic) IBOutlet UILabel *replayTimeLab;

@property (weak, nonatomic) IBOutlet UIButton *replayButton;

@property (weak, nonatomic) IBOutlet UIButton *praiseButton;

@property(nonatomic, strong)CommentReplayModel *replaymodel;

@property(nonatomic ,assign)NSInteger CommentCellIndex;
@property(nonatomic ,assign)NSInteger ReplayCellIndex;

@property (nonatomic, weak) id <CommentReplayCellDelegate> delegae;

+ (CGFloat)cellHeight;


@end
