//
//  CircleCollectionViewCell.m
//  mingyu
//
//  Created by apple on 2018/4/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CircleCollectionViewCell.h"

@implementation CircleCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *maksPath = [UIBezierPath bezierPathWithRoundedRect:_circle_img.bounds cornerRadius:5];
    CAShapeLayer *maksLayer = [[CAShapeLayer alloc] init];
    maksLayer.frame = _circle_img.bounds;
    maksLayer.path = maksPath.CGPath;
    _circle_img.layer.mask = maksLayer;
    
//    _delete_button.hidden = YES;
}


- (void)setCirclemodel:(ParentCircleModel *)circlemodel{
    _circlemodel = circlemodel;
    _circle_name_Lab.text = circlemodel.circle_name;
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,circlemodel.circle_img];
    _circle_img.backgroundColor = [UIColor lightGrayColor];
    [_circle_img sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:nil];


}


@end
