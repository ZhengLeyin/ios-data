//
//  VideoModel.h
//  mingyu
//
//  Created by apple on 2018/4/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject


/** 视频ID */
@property (nonatomic, assign) NSInteger video_id;

/** 课程ID */
@property (nonatomic, assign) NSInteger fk_course_id;

/** 视频标题 */
@property (nonatomic, copy) NSString *video_title;

/** 视频简介 */
@property (nonatomic, copy) NSString *video_abstract;

/** 视频图片 */
@property (nonatomic, copy) NSString *video_img;

/** 视频路径 */
@property (nonatomic, copy) NSString *video_path;

/** 视频价格 */
@property (nonatomic, assign) CGFloat video_price;

/** 视频介绍 */
@property (nonatomic, copy) NSString *video_introduce;

/** 视频收获 */
@property (nonatomic, copy) NSString *video_harvest;

/** 课程排序 */
@property (nonatomic, assign) NSInteger video_sort;

/**创建时间 */
@property (nonatomic, copy) NSString *create_time;

/** 来源 */
@property (nonatomic, copy) NSString *video_source;

/** 来源图片 */
@property (nonatomic, copy) NSString *source_head;

/** 播放人数 */
@property (nonatomic, assign) NSInteger play_number;

/** 播放次数 */
@property (nonatomic, assign) NSInteger play_count;

/** 时长 */
@property (nonatomic, copy) NSString *time_length;

/** 点赞数 */
@property (nonatomic, assign) NSInteger praise_number;

/** 收藏数 */
@property (nonatomic, assign) NSInteger collect_number;

/** 已收藏 */
@property (nonatomic, assign) NSInteger is_collect_true;

/** 已点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

/** 评论数 */
@property (nonatomic, assign) NSInteger comment_number;

/** 作者id */
@property (nonatomic, assign) NSInteger fk_user_id;

/** 作者名 */
@property (nonatomic, copy) NSString *user_name;

/** 作者头像 */
@property (nonatomic, copy) NSString *user_head;

/** 关注状态 */
@property (nonatomic, assign) NSInteger follow_status;

/** 被关注数 */
@property (nonatomic, assign) NSInteger follow_number;

/** 已播放时长 */
@property (nonatomic, assign) NSInteger seekTime;

/** 分享链接 */
@property (nonatomic, copy) NSString *share_url;

/**视频+音频 播放地址*/
@property (nonatomic, strong) NSString *section_path;

/**视频+音频 图片地址*/
@property (nonatomic, strong) NSString *section_img;

/** 章节标题 */
@property (nonatomic, strong) NSString *section_title;

/** 试听 */
@property (nonatomic, assign) NSUInteger is_free;

/** 选择的视频 */
@property (nonatomic, assign) BOOL selectvideo;

@end
