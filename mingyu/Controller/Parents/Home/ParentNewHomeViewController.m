//
//  ParentNewHomeViewController.m
//  mingyu
//
//  Created by apple on 2018/9/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentNewHomeViewController.h"
#import "ParentCourseViewController.h"
#import "YYFPSLabel.h"
#import "SearchViewController.h"
#import "CommentViewController.h"
#import "ParentNewHomeHeaderView.h"
#import "ParentVideoDetailCell.h"
#import "ParentVideoDetailViewController.h"

@interface ParentNewHomeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) ParentNewHomeHeaderView *tableHeaderView;

@property (nonatomic, strong) NSMutableArray *userLikeArray;
@property (nonatomic, strong) NSMutableArray *childcareArray;
@property (nonatomic, strong) NSMutableArray *videoArray;
@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) CGFloat childAge;

@end

@implementation ParentNewHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(BirthdayChange) name:NSNotificationBirthdayChange object:nil];

    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    [self setNavigationbar];
//  初始化音频播放器
    [[DFPlayer shareInstance] df_initPlayerWithUserId:nil];
    [DFPlayer shareInstance].isNeedCache = NO;
    [DFPlayer shareInstance].category = DFPlayerAudioSessionCategoryPlayback;
    
//    YYFPSLabel *label = [[YYFPSLabel alloc] initWithFrame:CGRectMake(40, 10, 100, 30)];
//    [[UIApplication sharedApplication].keyWindow addSubview:label];
    
    [self.view addSubview:self.tableView];
    
    [self refreshTableview];
  
}

//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
       
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //    获取猜你喜欢
            [self getGuessUserLike];
            //精选视频
            [self getHomeVideoList];
            //育儿必读
            [self getHomeChildcare];
            
            [_tableHeaderView getAdvertisement];
            [_tableView.mj_header endRefreshing];
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;   //设置状态栏颜色为默认
    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.view.backgroundColor = [UIColor whiteColor];
    
    BOOL showLoginView = [userDefault boolForKey:@"showLoginView"];
    if (!showLoginView) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        VC.RightItemButton.hidden = NO;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:NO completion:nil];
        
        [userDefault setBool:YES forKey:@"showLoginView"];
        [userDefault synchronize];
    }
    
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    _childAge = (currentTime - data)/3600/24/365;
//    获取一键听
    if (_childAge > 2) {
        [self getUserSubjectHistory];
    }
    
    [SVProgressHUD dismiss];
}

- (void)viewWillDisappear:(BOOL)animated{
    //    如果不想让其他页面的导航栏变为透明 需要重置
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationBirthdayChange object:self];
}


//一键听
- (void)getUserSubjectHistory{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetUserSubjectHistory];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSMutableArray *array = [NSMutableArray array];
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                SubjectModel *submodel = [SubjectModel modelWithJSON:dic];
                [array addObject:submodel];
            }
            _tableHeaderView.subjcArray = array;
            
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//育儿必读
- (void)getHomeChildcare{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL, KGetHomeChildcare];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        [self.childcareArray removeAllObjects];
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                if (model.news_type == 2) {
                    [self.childcareArray addObject:model];
                }
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//精选视频
- (void)getHomeVideoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetHomeVideoList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"start":@(_start),
                                 @"size":@"4"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        [self.videoArray removeAllObjects];
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            _start += 4;
            [self.videoArray removeAllObjects];
            for (NSDictionary *dic in data) {
                VideoModel *videomodel = [VideoModel modelWithJSON:dic];
                [self.videoArray addObject:videomodel];
            }
            [_tableView reloadData];
//            [_tableView reloadSection:1 withRowAnimation:UITableViewRowAnimationFade];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//猜你喜欢
- (void)getGuessUserLike{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL, KGetGuessUserLike];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [self.userLikeArray removeAllObjects];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succsess = [json[@"success"]boolValue];
        if (succsess) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                NewsModel *submodel = [NewsModel modelWithJSON:dic];
                [self.userLikeArray addObject:submodel];
            }
            [self.tableView reloadData];
        } else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _childcareArray.count;
    } else if (section == 1){
        return _videoArray.count;
    } else {
        return _userLikeArray.count;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    __block typeof (self) WeakSelf = self;
    ParentHomeCell *header =  [ParentHomeCell TheHeaderView];
    if (section == 0 && self.childcareArray.count > 0){
        header.header_title_lab.text = @"育儿必读";
        header.MoreButtonBlock = ^{
            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
            VC.title = @"育儿必读";
            [WeakSelf.navigationController pushViewController:VC animated:YES];
        };
    } else if (section == 1 && self.videoArray.count > 0){
        header.header_title_lab.text = @"精选视频";
        header.MoreButtonBlock = ^{
//            self.tabBarController.selectedIndex = 1;
        };
    } else if (section == 2 && self.userLikeArray.count > 0) {
        header.header_title_lab.text = @"猜你喜欢";
        header.MoreButtonBlock = ^{
//            self.tabBarController.selectedIndex = 2;
        };
    }
    return header;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 && self.childcareArray.count > 0){
        return 40;
    } else if (section == 1 && self.videoArray.count > 0){
        return 40;
    } else if (section == 2 && self.userLikeArray.count > 0) {
        return 40;
    }
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view = [UIView new];
        return view;
    } else if (section == 1 && self.videoArray.count > 0){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, kScreenWidth, 40);
        [button setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:0];
        [button setImage:ImageName(@"首页_刷新") forState:0];
        [button setTitle:@" 换一组 " forState:0];
        button.titleLabel.font = FontSize(12);
        [button addTarget:self action:@selector(refreshVideo) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -button.imageView.size.width, 0, button.imageView.size.width)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, button.titleLabel.bounds.size.width, 0, -button.titleLabel.bounds.size.width)];
        return button;
    } if (section == 2 && self.userLikeArray.count > 0){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, kScreenWidth, 40);
        [button setTitle:@"更多内容 >" forState:0];
        button.titleLabel.font = FontSize(12);
        [button setTitleColor:[UIColor colorWithHexString:@"60D5F5"] forState:0];
        [button addTarget:self action:@selector(moreUserLike) forControlEvents:UIControlEventTouchUpInside];
        return button;
    } else {
        UIView *view = [UIView new];
        return view;
    }
}

- (void)refreshVideo{
    ZPLog(@"refreshVideo");
    [self getHomeVideoList];
//    [_tableView reloadSection:1 withRowAnimation:UITableViewRowAnimationTop];
    [_tableView scrollToRow:0 inSection:1 atScrollPosition:UITableViewScrollPositionTop animated:NO];

}

- (void)moreUserLike{
    ZPLog(@"moreUserLike");
//    self.tabBarController.selectedIndex = 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0 && self.childcareArray.count > 0){
        return 10;
    } else if (section == 1 && self.videoArray.count > 0){
        return 40;
    } else if (section == 2 && self.userLikeArray.count > 0) {
        return 40;
    }
    return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
        if (self.childcareArray && self.childcareArray.count > indexPath.row) {
            cell.newsmodel = [self.childcareArray objectAtIndex:indexPath.row];
        }
         return cell;
    } else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
            if (self.videoArray && self.videoArray.count > indexPath.row) {
                cell.videomodel = [self.videoArray objectAtIndex:indexPath.row];
                cell.play_image.hidden = NO;
            }
            return cell;
        } else {
            ParentVideoDetailCell *cell = [ParentVideoDetailCell theParentVideoDetailCellWithTableView:tableView];
            if (self.videoArray && self.videoArray.count > indexPath.row) {
                cell.model = [self.videoArray objectAtIndex:indexPath.row];
            }
            return cell;
        }
    } else {
        ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
        if (self.userLikeArray && _userLikeArray.count > indexPath.row) {
            cell.newsmodel = [_userLikeArray objectAtIndex:indexPath.row];
        }
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 130;
    } else if (indexPath.section ==1){
        if (indexPath.row == 0) {
            return 230;
        } else {
            return 105;
        }
    } else {
        return 130;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (self.childcareArray && self.childcareArray.count > indexPath.row) {
            NewsModel *model = [self.childcareArray objectAtIndex:indexPath.row];
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = model;
            [self.navigationController pushViewController:VC animated:YES];
        }
    } else if (indexPath.section == 1) {
        if (self.videoArray && self.videoArray.count > indexPath.row) {
            VideoModel *model = self.videoArray[indexPath.row];
            ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
            VC.video_id = model.video_id;
            [self.navigationController pushViewController:VC animated:YES];
        }
    } else {
        if (_userLikeArray && _userLikeArray.count > indexPath.row) {
            NewsModel *model = [_userLikeArray objectAtIndex:indexPath.row];
            if (model.news_type == 1) {
                ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
                VC.newsmodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
}




//搜索
- (void)SearchButton {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchAll_type;
    [self.navigationController pushViewController:VC animated:YES];
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-TabbarH);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        _childAge = (currentTime - data)/3600/24/365;
        if (_childAge > 2) {
            _tableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"ParentNewHomeHeaderView" owner:nil options:nil][1];
            _tableView.tableHeaderView = self.tableHeaderView;
        } else {
            _tableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"ParentNewHomeHeaderView" owner:nil options:nil][0];
            _tableView.tableHeaderView = self.tableHeaderView;
        }
    }
    return _tableView;
}

- (void)BirthdayChange {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    _childAge = (currentTime - data)/3600/24/30/12;
    if (_childAge > 2) {
        _tableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"ParentNewHomeHeaderView" owner:nil options:nil][1];
        self.tableView.tableHeaderView = self.tableHeaderView;
    } else {
        _tableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"ParentNewHomeHeaderView" owner:nil options:nil][0];
        self.tableView.tableHeaderView = self.tableHeaderView;
    }
//    [_tableHeaderView getAdvertisement];
}

- (NSMutableArray *)userLikeArray {
    if (_userLikeArray == nil) {
        _userLikeArray = [NSMutableArray array];
    }
    return _userLikeArray;
}


- (NSMutableArray *)childcareArray {
    if (!_childcareArray) {
        _childcareArray = [NSMutableArray array];
    }
    return _childcareArray;
}

- (NSMutableArray *)videoArray {
    if (!_videoArray) {
        _videoArray = [NSMutableArray array];
    }
    return _videoArray;
}

- (void)setNavigationbar {
    UIButton *titleView = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleView setBackgroundColor:[UIColor colorWithHexString:@"F8F9FB"]];
    titleView.frame = CGRectMake(0, 0, self.view.width-kScale(150), 30);
    titleView.layer.cornerRadius = 15;
    titleView.layer.masksToBounds = YES;
    [titleView setImage:ImageName(@"搜索icon") forState:UIControlStateNormal];
    [titleView setTitle:@" 想找课程/育儿…" forState:UIControlStateNormal];
    [titleView setTitleColor:[UIColor colorWithHexString:@"A8A8A8"] forState:UIControlStateNormal];
    titleView.titleLabel.font = FontSize(15);
    [titleView addTarget:self action:@selector(SearchButton) forControlEvents:UIControlEventTouchUpInside];
    //self.navigationItem.titleView = titleView;
}

// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1) {
        return NO;
    }
    return YES;
}
@end
