//
//  GoldRulerViewController.m
//  mingyu
//
//  Created by apple on 2018/5/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "GoldRulerViewController.h"

@interface GoldRulerViewController ()

@property (nonatomic, strong) UIView *useView;

@property (nonatomic, strong) UIView *rulerView;


@property (nonatomic, strong) GoldHeaderView *goldheaderView;



@end

@implementation GoldRulerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabPageController.tabBar.itemTitleFont = [UIFont systemFontOfSize:14];
    self.tabPageController.tabBar.indicatorScrollFollowContent = YES;
    [self initViewControllers];
    
}



- (void)initViewControllers{
    
    NSMutableArray *vcArray = [NSMutableArray array];
    GetGoldController *getVC = [GetGoldController new];
    getVC.yp_tabItemTitle = @"如何获得";
    [vcArray addObject:getVC];
    
    UIViewController *useVC = [UIViewController new];
    useVC.yp_tabItemTitle = @"如何使用";
    [useVC.view addSubview:self.useView];
    [vcArray addObject:useVC];
    
    UIViewController *rulerVC = [UIViewController new];
    rulerVC.yp_tabItemTitle = @"扣减规则";
    [rulerVC.view addSubview:self.rulerView];
    [vcArray addObject:rulerVC];
    
    self.tabPageController.viewControllers = vcArray;
    
}

- (UIView *)headerView{
    if (!_goldheaderView) {
        _goldheaderView = [[NSBundle mainBundle] loadNibNamed:@"GoldHeaderView" owner:nil options:nil][0];
        _goldheaderView.width = kScreenWidth;
        _goldheaderView.goldLab.text = [NSString stringWithFormat:@"%ld",_ideal_money];
    }
    return _goldheaderView;;
}

- (UIView *)useView{
    if (_useView == nil) {
        _useView = [[UIView alloc] initWithFrame:self.view.bounds];
        _useView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, kScreenWidth-60, 250)];
        lab.textColor = [UIColor colorWithHexString:@"575757"];
        lab.font = FontSize(14);
        lab.numberOfLines = 0;
        [_useView addSubview:lab];
        
        NSString *dLabelString = @"1、使用积分兑换课程，请仔细阅读兑换页信息，非技术故障导致课程问题，不退还积分。\n2、通过非法途径获得积分后的正常兑换行为，或不按课程使用规则进行兑换，名育有权不提供服务。\n3、凡以不正当手段（包括但不限于舞弊、扰乱系统、实施网络攻击等）进行兑换，名育有权终止该次兑换。";
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:dLabelString];
        NSMutableParagraphStyle   *paragraphStyle   = [[NSMutableParagraphStyle alloc] init];
        
        //行间距
        [paragraphStyle setLineSpacing:5.0];
        //段落间距
        [paragraphStyle setParagraphSpacing:10.0];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [dLabelString length])];
        [lab setAttributedText:attributedString];
        
        [lab setLineBreakMode:NSLineBreakByTruncatingTail];
        
    }
    return _useView;
}


- (UIView *)rulerView{
    if (!_rulerView) {
        _rulerView = [[UIView alloc] initWithFrame:self.view.bounds];
        _rulerView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, kScreenWidth-60, 200)];
        lab.textColor = [UIColor colorWithHexString:@"575757"];
        lab.font = FontSize(14);
        lab.numberOfLines = 0;
        [_rulerView addSubview:lab];
        
        NSString *dLabelString = @"1、用户使用积分兑换课程时，将依据课程价格扣减相应积分。\n2、获取积分的行为，如果内容被删除，则会相应扣掉相应获得的积分。\n3、用户使用过程中违反使用规则（如发广告等）管理员将扣减一定数量积分作为惩罚。";
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:dLabelString];
        NSMutableParagraphStyle   *paragraphStyle   = [[NSMutableParagraphStyle alloc] init];
        
        //行间距
        [paragraphStyle setLineSpacing:5.0];
        //段落间距
        [paragraphStyle setParagraphSpacing:10.0];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [dLabelString length])];
        [lab setAttributedText:attributedString];
        
        [lab setLineBreakMode:NSLineBreakByTruncatingTail];
    }
    return _rulerView;
}


- (IBAction)share:(id)sender {
//    ShareView *shareview = [[ShareView alloc] initWithDictionary:nil];
//    shareview.type = share_type;
//    [shareview show];
}

- (IBAction)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
