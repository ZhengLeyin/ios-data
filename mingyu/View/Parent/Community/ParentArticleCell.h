//
//  ParentArticleCell.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel,commenModel;

@interface ParentArticleCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *article_source_Lab;
@property (weak, nonatomic) IBOutlet UIButton *collect_Button;
@property (weak, nonatomic) IBOutlet UIButton *praise_button;


@property (weak, nonatomic) IBOutlet UILabel *aboutarticleLab;


@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIView *imgcontentView;
@property (weak, nonatomic) IBOutlet UILabel *sourceLab;
@property (weak, nonatomic) IBOutlet UIButton *pageviewButton;
@property (weak, nonatomic) IBOutlet UIButton *disLikeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LayoutConstraintHeight;

@property (nonatomic, strong) NewsModel *newsmodel;


//@property (nonatomic, strong) ParentArticleModel *articinfomodel;
//
//@property (nonatomic, strong) ParentArticleModel *articlemodel;



@property (nonatomic, copy) void(^MoreButtonBlock)(void);



+ (instancetype)theCollertAndPraisCell;

+ (instancetype)theAboutArticleHeaderView;

+ (instancetype)theAboutArticleCellWithTableView:(UITableView *)tableView;


/**
搜索时用到
 */
- (void)showSearch;

@end
