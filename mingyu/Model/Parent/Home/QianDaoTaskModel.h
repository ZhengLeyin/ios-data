//
//  QianDaoTaskModel.h
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QianDaoTaskModel : NSObject

@property (nonatomic, assign) NSInteger task_id;

/** 任务名称 */
@property (nonatomic, copy) NSString *task_name;

/** 可获得积分 */
@property (nonatomic, assign) NSInteger task_gold;

/** 任务类型  1、新手任务 2、每日任务 3、其他任务*/
@property (nonatomic, assign) NSInteger task_type;

/** 任务是否完成 */
@property (nonatomic, assign) BOOL is_complete_true;

/** 跳转类型 1、 */
@property (nonatomic, assign) NSInteger module_type;

@property (nonatomic, assign) NSInteger platform_type;



@end

