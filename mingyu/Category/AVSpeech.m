//
//  AVSpeech.m
//  Seller
//
//  Created by apple on 2017/11/3.
//  Copyright © 2017年 Jys. All rights reserved.
//

#import "AVSpeech.h"

@implementation AVSpeech

+ (id) sharedBaiduVoiceManage
{
    static id manage;
    if (manage == nil)
    {
        manage = [[AVSpeechSynthesizer alloc] init];
        
    }
    return manage;
}


+ (void)SpakeString:(NSString *)string{
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:string];
        
    [[AVSpeech sharedBaiduVoiceManage] speakUtterance:utterance];

}




@end
