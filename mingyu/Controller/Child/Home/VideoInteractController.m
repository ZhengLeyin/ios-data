//
//  VideoInteractController.m
//  mingyu
//
//  Created by apple on 2018/4/10.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "VideoInteractController.h"
#import "VideoInteractCell.h"


@interface VideoInteractController ()<VideoInteractCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *totleCommentLab;
@property (weak, nonatomic) IBOutlet UIImageView *UserIconImag;

@property (nonatomic, strong) NSMutableArray *pinlunArray;



@property (nonatomic, assign) NSInteger size;

@property (nonatomic, assign) NSInteger start;

@end

@implementation VideoInteractController

- (void)viewDidLoad {
    [super viewDidLoad];
    _UserIconImag.layer.cornerRadius = _UserIconImag.height/2;
    _UserIconImag.layer.masksToBounds = YES;
//    NSString *head_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDuser_head]];
    [_UserIconImag zp_setImageWithURL:[userDefault objectForKey:KUDuser_head] placeholderImage:ImageName(@"默认头像")];

    _size = 20;
    _start = 1;
    //预计高度为143--即是cell高度
    self.tableView.estimatedRowHeight = 100.0f;
    //自适应高度
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.bounces = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RequestpinlunSArray) name:@"HaveAddComment" object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self RequestpinlunSArray];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HaveAddComment" object:nil];
}

- (void)RequestpinlunSArray{
    [_pinlunArray removeAllObjects];
//    NSString *URL = [NSString stringWithFormat:@"%@%@/%@/%ld/%ld/%ld",KURL,KGetVideoCommentList,USER_ID,(long)_video_id,(long)_start,(long)_size];
//    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
//        NSDictionary *json = responseObject;
//        ZPLog(@"%@",json);
//        ZPLog(@"%@",json[@"message"]);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//            NSDictionary *data = json[@"data"];
//            _totleCommentLab.text = [NSString stringWithFormat:@"全部评论|%@",data[@"comment_number"]];
//            NSArray *commentList = data[@"commentList"];
//            for (NSDictionary *dic in commentList) {
//                commenModel *commenmodel = [commenModel modelWithJSON:dic];
//                [self.pinlunArray addObject:commenmodel];
//            }
//            [self.tableView reloadData];
//        } else {
//            [UIView ShowInfo:json[@"message"] Inview:self.view];
//        }
//    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
//    }];
}

#pragma -mark- VideoInteractCellDelegate
- (void)zanButtonClicked:(CommentModel *)commenmodel button:(UIButton *)button{
    if (commenmodel.click_praise_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddClickPraise];
        NSDictionary *parameters = @{
                                     @"user_id":USER_ID,
                                     @"from_id":[NSString stringWithFormat:@"%ld",commenmodel.comment_id],
                                     @"praise_type":@"1"
        };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@",json);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                [button setImage:ImageName(@"zan") forState:UIControlStateNormal];
                [button setTitle:[NSString stringWithFormat:@" %ld ",commenmodel.praise_number++] forState:UIControlStateNormal];

                commenmodel.click_praise_true = [json[@"data"] integerValue];
                [UIView ShowInfo:TipPraiseSuccess Inview:self.view];

            } else {
                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else{
//        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelClickPraise];
//        NSDictionary *parameters = @{
//                                     @"praise_id": [NSString stringWithFormat:@"%ld",commenmodel.click_praise_true]
//                                     };
//        [HttpRequest postWithURLString:URL parameters:parameters success:^(id responseObject) {
//            NSDictionary *json = responseObject;
//            ZPLog(@"%@\n%@",json,json[@"message"]);
//            BOOL success = [json[@"success"] boolValue];
//            if (success) {
//
//                [button setImage:ImageName(@"zan_nor") forState:UIControlStateNormal];
//                [button setTitle:@" 赞 " forState:UIControlStateNormal];
//                
//                commenmodel.click_praise_true = 0;
//
//            }
//        } failure:^(NSError *error) {
//
//        }];
    
        [UIView ShowInfo:TipDoNotPraise Inview:self.view];
    }
}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_pinlunArray.count && _pinlunArray.count > 0) {
        return _pinlunArray.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"VideoInteractCell";
    VideoInteractCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[VideoInteractCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (_pinlunArray.count && _pinlunArray.count > 0) {
        cell.commenmodel = [_pinlunArray objectAtIndex:indexPath.row];
        cell.delegate = self;
        cell.selectionStyle = UITableViewScrollPositionNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentModel *commenmodel = [_pinlunArray objectAtIndex:indexPath.row];
    if (commenmodel.fk_user_id == [USER_ID integerValue]) {
        //显示弹出框列表选择
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:commenmodel.comment_content
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        
        UIAlertAction* copyAction = [UIAlertAction actionWithTitle:@"复制" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //复制评论内容
                                                               UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                               pasteboard.string = commenmodel.comment_content;
//                                                               [UIView ShowInfo:@"复制成功" Inview:self.view];
                                                           }];
        UIAlertAction* deleteAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  //删除自己的评论
                                                                   [self deletecommen:commenmodel AtIndexPath:indexPath];
                                                              }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                                
                                                             }];
        [cancelAction setValue:[UIColor colorWithHexString:@"939393"] forKey:@"_titleTextColor"];

        [alert addAction:copyAction];
        [alert addAction:deleteAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else{
        //显示弹出框列表选择
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               //举报别人评论
                                                               [self reportcommenm:commenmodel];
                                                           }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 //响应事件
                                                             }];
         [cancelAction setValue:[UIColor colorWithHexString:@"939393"] forKey:@"_titleTextColor"];
        
        [alert addAction:reportAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


//删除自己的评论
- (void)deletecommen:(CommentModel *)commenmodel AtIndexPath:(NSIndexPath *)indexPath{
//    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelVideoComment];
//    NSDictionary *parameters = @{
//                                 @"comment_id":[NSString stringWithFormat:@"%ld",(long)commenmodel.comment_id]
//                                 };
//    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
//        NSDictionary *json = responseObject;
//        ZPLog(@"%@",json);
//        ZPLog(@"%@",json[@"message"]);
//        BOOL success = [json[@"success"]boolValue];
//        if (success) {
//            [self.pinlunArray removeObjectAtIndex:indexPath.row];
//            // Delete the row from the data source.
//            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        } else {
//            [UIView ShowInfo:json[@"message"] Inview:self.view];
//        }
//    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
//    }];
}

//举报别人评论
- (void)reportcommenm:(CommentModel *)commenmodel{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
    NSDictionary *parameters = @{
                                 @"user_id":USER_ID,
                                 @"from_id":[NSString stringWithFormat:@"%ld", (long)commenmodel.comment_id],
                                 @"type_id":@"1"
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"举报成功" Inview:self.view];
        }else{
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}

- (UIScrollView *)scrollView{
    return self.tableView;
}


- (NSMutableArray *)pinlunArray{
    if (_pinlunArray == nil) {
        _pinlunArray = [NSMutableArray array];
    }
    return _pinlunArray;
}

- (IBAction)pinlunButton:(id)sender {

    if (self.commenblock) {
        self.commenblock();
    }

}

@end
