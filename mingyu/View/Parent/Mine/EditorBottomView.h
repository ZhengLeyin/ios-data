//
//  EditorBottomView.h
//  mingyu
//
//  Created by apple on 2018/9/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditorBottomViewDelegate <NSObject>

- (void)chooseAllButonClicked;

- (void)deleteButtonClicked;

@end

@interface EditorBottomView : UIView


@property (weak, nonatomic) IBOutlet UIButton *chooseAllButton;
@property (weak, nonatomic) IBOutlet UILabel *chooseCountLab;

@property (nonatomic, weak) id <EditorBottomViewDelegate> delegate;


@end
