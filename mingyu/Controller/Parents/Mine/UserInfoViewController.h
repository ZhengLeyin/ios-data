//
//  UserInfoViewController.h
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UserInfoViewController : UIViewController


@end



@interface UserInfoViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *key_lab;

@property (weak, nonatomic) IBOutlet UILabel *value_lab;

- (void)setcellWithkeyArray:(NSArray *)key valueArray:(NSArray *)value index:(NSInteger )index;


@end

