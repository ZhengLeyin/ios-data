//
//  CollectViewController.m
//  mingyu
//
//  Created by apple on 2018/6/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CollectViewController.h"
#import "CollectVideoViewController.h"

@interface CollectViewController ()<SPPageMenuDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *editorButton;
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger PageMenuIndex;

@property (nonatomic, assign) BOOL iseditor;

@end

@implementation CollectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];

    [self.view addSubview:self.pageMenu];
    CollectCourseViewController *courseVC = [[CollectCourseViewController alloc] init];
    [self addChildViewController:courseVC];
    
    CollectArticleViewController *ArticleVC = [[CollectArticleViewController alloc] init];
    [self addChildViewController:ArticleVC];
    
    CollectVideoViewController *videoVC = [[CollectVideoViewController alloc] init];
    [self addChildViewController:videoVC];

    CollectChildcareViewController *ChildcareVC = [[CollectChildcareViewController alloc] init];
    [self addChildViewController:ChildcareVC];
    
//    CollectTopicViewController *TopicVC = [[CollectTopicViewController alloc] init];
//    [self addChildViewController:TopicVC];
    
    [self.view addSubview:self.scrollView];
    
    // 这一行赋值，可实现pageMenu的跟踪器时刻跟随scrollView滑动的效果
    self.pageMenu.bridgeScrollView = self.scrollView;
    
    // pageMenu.selectedItemIndex就是选中的item下标
    if (self.pageMenu.selectedItemIndex < self.childViewControllers.count) {
        UIViewController *VC = self.childViewControllers[self.pageMenu.selectedItemIndex];
        [_scrollView addSubview:VC.view];
        VC.view.frame = CGRectMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
        _scrollView.contentOffset = CGPointMake(kScreenWidth*self.pageMenu.selectedItemIndex, 0);
        _scrollView.contentSize = CGSizeMake(self.dataArr.count*kScreenWidth, 0);
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"E2E2E2"]]];

//    [self getCollectTypeNum];
}

- (void)getCollectTypeNum{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCollectTypeNum];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
           
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {

//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


#pragma mark - SPPageMenu的代理方法
- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _PageMenuIndex = toIndex;
    if (self.childViewControllers && self.childViewControllers.count == 4) {
        _iseditor = YES;
        [_editorButton setTitle:@"编辑" forState:0];
        [self editorButton:nil];
    }

    ZPLog(@"%zd------->%zd",fromIndex,toIndex);
    // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
    if (labs(toIndex - fromIndex) >= 2) {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:NO];
    } else {
        [self.scrollView setContentOffset:CGPointMake(kScreenWidth * toIndex, 0) animated:YES];
    }
    
    if (self.childViewControllers.count <= toIndex) {return;}
    
    UIViewController *VC = self.childViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([VC isViewLoaded]) return;
    
    VC.view.frame = CGRectMake(kScreenWidth * toIndex, 0, kScreenWidth,  kScreenHeight-NaviH-effectViewH-44);
    [_scrollView addSubview:VC.view];
}




- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 44, kScreenWidth, kScreenHeight-NaviH-effectViewH-44);
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (SPPageMenu *)pageMenu{
    if (_pageMenu == nil) {
        self.dataArr = @[@"精品课",@"资讯",@"视频",@"育儿必读"];

        _pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0,  kScreenWidth, 44) trackerStyle:SPPageMenuTrackerStyleLineAttachment];
        _pageMenu.backgroundColor = [UIColor whiteColor];
        [_pageMenu setItems:self.dataArr selectedItemIndex:0];
        _pageMenu.delegate = self;
        _pageMenu.itemTitleFont = FontSize(16);
        _pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.unSelectedItemTitleColor = [UIColor colorWithHexString:@"575757"];
        _pageMenu.tracker.backgroundColor = [UIColor colorWithHexString:@"53D1F5"];
        _pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollAdaptContent;
    }
    return _pageMenu;
}




- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)editorButton:(UIButton *)sender {
    if (!_iseditor) {
        if (_PageMenuIndex == 0) {
            CollectCourseViewController *VC = self.childViewControllers[0];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 1) {
            CollectArticleViewController *VC = self.childViewControllers[1];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 2 ) {
            CollectVideoViewController *VC = self.childViewControllers[2];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 3) {
            CollectChildcareViewController *VC = self.childViewControllers[3];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 4){
            CollectTopicViewController *VC = self.childViewControllers[4];
            [VC editorButton:_iseditor];
        }
        _iseditor = YES;
        [_editorButton setTitle:@"完成" forState:0];
    } else {
        if (_PageMenuIndex == 0) {
            CollectCourseViewController *VC = self.childViewControllers[0];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 1) {
            CollectArticleViewController *VC = self.childViewControllers[1];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex ==2 ) {
            CollectVideoViewController *VC = self.childViewControllers[2];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 3) {
            CollectChildcareViewController *VC = self.childViewControllers[3];
            [VC editorButton:_iseditor];
        } else if (_PageMenuIndex == 4){
            CollectTopicViewController *VC = self.childViewControllers[4];
            [VC editorButton:_iseditor];
        }
        _iseditor = NO;
        [_editorButton setTitle:@"编辑" forState:0];
    }
}



@end
