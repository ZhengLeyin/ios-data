//
//  CVCommentViewController.m
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "CVCommentViewController.h"
#import "CourseAudioViewController.h"
#import "JXPagerView.h"

@interface CVCommentViewController ()<UITableViewDataSource,UITableViewDelegate,CommentListCellDelegate,JXPagerViewListViewDelegate>

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *commentArray;

@property (nonatomic, strong) NSMutableArray *recommendArray;

@property (nonatomic, assign) NSInteger comment_number;

@end

@implementation CVCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (_type == 0) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(- effectViewH - 50);
//        }else {
//            make.top.left.right.mas_equalTo(self.view);
//            make.bottom.mas_equalTo(-effectViewH);;
//        }
    }];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCourseCommentList) name:@"HaveAddComment" object:nil];

    [self getCourseCommentList];  //评论列表

    [self getCourseRecommend];   //相关推荐
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HaveAddComment" object:nil];
}


//获取评论
- (void)getCourseCommentList{
    [self.commentArray removeAllObjects];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KgetContentCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":@(_course_id),
                                 @"comment_type":@(course_comment),
                                 @"start":@"0",
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            for (NSDictionary *dic in commentList) {
                CommentModel *commenmodel = [CommentModel modelWithJSON:dic];
                [self.commentArray addObject:commenmodel];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//获取相关推荐
- (void)getCourseRecommend{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetCourseRecommend];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"course_id":@(_course_id),
                                 @"start":@"0",
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                CourseModel *coursemodel = [CourseModel modelWithJSON:dic];
                [self.recommendArray addObject:coursemodel];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}



#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (self.commentArray.count >2) {
            return 2;
        }
        return self.commentArray.count;
    } else {
        return self.recommendArray.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
            [head.comment_number_Button setTitle:[NSString stringWithFormat:@"共%ld条评论",_comment_number] forState:0];
            head.arrowimage.hidden = NO;
            head.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _course_id;
                VC.comment_type = course_comment;
//                [self.navigationController pushViewController:VC animated:YES];
                [self pushViewController:VC toCourseList:NO];
            };
        }
        return head;
    } else{
        if (self.recommendArray.count > 0) {
            CommentListCell *head = [CommentListCell theRecommendHeaderView];
            //        head.title_lab.text = @"推荐课程";
            return head;
        } else {
            UIView *view = [UIView new];
            return view;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 55;
    } else{
        if (self.recommendArray.count > 0) {
            return 60;
        }
        return 0;
    }
}



- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (self.commentArray.count == 0) {
            CommentListCell *foot = [CommentListCell theNoCommentCell];
            return foot;
        } else if (self.commentArray.count < 3){
            UIView *view = [UIView new];
            return view;
        } else{
            CommentListCell *foot = [CommentListCell TheMoreCell];
            foot.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _course_id;
                VC.comment_type = course_comment;
//                VC.CommentListBlock = ^(NSMutableArray *commentListArray, NSInteger comment_number) {
//                    _commentArray = commentListArray;
//                    _comment_number = comment_number;
//                    [_tableView reloadData];
//                };
//                VC.title = [NSString stringWithFormat:@"%ld条评论",_comment_number];
//                [self.navigationController pushViewController:VC animated:YES];
                [self pushViewController:VC toCourseList:NO];
            };
            return foot;
        }
    }else {
        UIView *view = [UIView new];
        return view;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if (self.commentArray.count == 0) {
            return 100;
        } else if (self.commentArray.count < 3){
            return 0;
        } else{
            return 50;
        }
    } else{
        return 0;
    } 
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        if (_commentArray && _commentArray.count > indexPath.row) {
            CommentModel *commenmodel = [_commentArray objectAtIndex:indexPath.row];
            [commentcell setcellWithModel:commenmodel praiseType:praise_course_comment];
            commentcell.delegate = self;
        }
        commentcell.selectionStyle = UITableViewCellSelectionStyleNone;
        return commentcell;
    } else {
        ParentCouresCell *courescell = [ParentCouresCell theCellWithTableView:tableView];
        if (self.recommendArray && self.recommendArray.count > indexPath.row) {
            CourseModel *model = self.recommendArray[indexPath.row];
            courescell.coursemodel = model;
            courescell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return courescell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CommentModel *model = [_commentArray objectAtIndex:[indexPath row]];
        if (model.height==0) {
            return [CommentListCell cellHeight];
        }else{
            return model.height;
        }
    } else {
        return 155;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        if (_commentArray.count > indexPath.row) {
            CommentModel *commentmodel = [_commentArray objectAtIndex:[indexPath row]];
            if (commentmodel.fk_user_id == [USER_ID integerValue]) {
                [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"复制" block1:^{
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = commentmodel.comment_content;
                    [UIView ShowInfo:@"复制成功" Inview:self.view];
                } title2:@"删除" block2:^{
                    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
                    NSDictionary *parameters = @{
                                                 @"user_id":USER_ID,
                                                 @"comment_id":[NSString stringWithFormat:@"%ld",(long)commentmodel.comment_id],
                                                 @"comment_type":@(course_comment)
                                                 };
                    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                        NSDictionary *json = responseObject;
                        ZPLog(@"%@",json);
                        ZPLog(@"%@",json[@"message"]);
                        BOOL success = [json[@"success"]boolValue];
                        if (success) {
                            [_commentArray removeObjectAtIndex:indexPath.row];
                            _comment_number --;
                            [_tableView reloadData];
                        } else {
                            [UIView ShowInfo:json[@"message"] Inview:self.view];
                        }
                    } failure:^(NSError *error) {
                        
                    }];
                }];
            } else {
                [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"回复" block1:^{
                    [self commentReplayButtonClicked:commentmodel];
                } title2:@"举报" block2:^{
                    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
                    NSDictionary *parameters = @{
                                                 @"user_id":USER_ID,
                                                 @"from_id":@(commentmodel.comment_id),
                                                 @"type_id":@(accusation_course_comment)
                                                 };
                    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                        NSDictionary *json = responseObject;
                        ZPLog(@"%@",json);
                        ZPLog(@"%@",json[@"message"]);
                        BOOL success = [json[@"success"] boolValue];
                        if (success) {
                            [UIView ShowInfo:@"举报成功" Inview:self.view];
                        }
                    } failure:^(NSError *error) {
                        
                    }];
                }];
            }
        }
    } else {
        if (_recommendArray && _recommendArray.count > indexPath.row) {
            CourseModel *model = [self.recommendArray objectAtIndex:indexPath.row];
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [self pushViewController:VC toCourseList:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [self pushViewController:VC toCourseList:YES];
                //            侧滑返回时可直接返回到列表
                NSMutableArray *array = self.navigationController.viewControllers.mutableCopy;
                [array removeObjectAtIndex:array.count-2];
                [self.navigationController setViewControllers:array animated:NO];
            }
        }
    }
}


#pragma -mark- CommentListCellDelegate
/** 回复 */
- (void)commentReplayButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = _course_id;
    VC.comment_type = course_comment;
    VC.targetCommentmodel = commentmodel;
//    [self.navigationController pushViewController:VC animated:YES];
    [self pushViewController:VC toCourseList:NO];
}

/** 查看更多 */
- (void)commentDetailButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = _course_id;
    VC.comment_type = course_comment;
//    [self.navigationController pushViewController:VC animated:YES];
    [self pushViewController:VC toCourseList:NO];
}

/** 点击老师头像 */
- (void)commentUserheaderClicked:(NSInteger)user_id{
    PersonalHomepageViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"PersonalHomepageViewController"];
    VC.user_id = user_id;
    [self pushViewController:VC toCourseList:NO];
}

- (void)pushViewController:(UIViewController *)vc toCourseList:(BOOL)tocourselist{
    if (self.PushBlock) {
        self.PushBlock(vc,tocourselist);
    }
}


//tableview 定位到评论位置
- (void)locaToComment{
    [self.tableView scrollToTop];
}



- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //预计高度为143--即是cell高度
        _tableView.estimatedRowHeight = 130.0f;
        //自适应高度
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}



- (NSMutableArray *)commentArray{
    if (_commentArray == nil) {
        _commentArray = [NSMutableArray array];
    }
    return _commentArray;
}

- (NSMutableArray *)recommendArray{
    if (_recommendArray == nil) {
        _recommendArray = [NSMutableArray array];
    }
    return _recommendArray;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}


@end
