//
//  UIImageView+Extension.m
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "UIImageView+Extension.h"

@implementation UIImageView (Extension)

- (void)zp_setImageWithURL:(nullable NSString *)url placeholderImage:(nullable UIImage *)placeholder{
    if (url && ![url hasPrefix:@"http"]) {
        url = [KKaptcha stringByAppendingString:url];
    }
    [self sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder];
}

@end
