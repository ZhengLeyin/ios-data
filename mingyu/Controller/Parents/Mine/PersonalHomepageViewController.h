//
//  PersonalHomepageViewController.h
//  mingyu
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalHomepageViewController : UIViewController

@property (nonatomic, assign) NSInteger user_id;

@end
