//
//  ParentSetViewController.h
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentSetViewController : UIViewController

@property (nonatomic, assign) NSInteger is_true_password;

@end
