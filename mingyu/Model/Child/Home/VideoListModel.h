////
////  VideoListModel.h
////  mingyu
////
////  Created by apple on 2018/4/8.
////  Copyright © 2018年 TZWY. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "VideoModel.h"
//
//
//@interface VideoListModel : NSObject
//
//@property (nonatomic, strong) VideoModel *video;
//
//@property (nonatomic, copy) NSString *course_author;
//
//@property (nonatomic, copy) NSString *author_organize;
//
//@property (nonatomic, assign) NSInteger play_number;
//
//@property (nonatomic, assign) NSInteger collect_id;
//
//
//
//@end

