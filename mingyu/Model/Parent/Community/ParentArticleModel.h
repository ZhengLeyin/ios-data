//
//  ParentArticleModel.h
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ParentArticleImageModel : NSObject


/**
 图片名称
 */
@property (nonatomic, copy) NSString *image_name;


/**
 图片序号
 */
@property (nonatomic, assign) NSInteger serial_number;


@end




@interface ParentArticleModel : NSObject

/** 资讯标题 */
@property (nonatomic, copy) NSString *article_title;

/** 咨询描述 */
@property (nonatomic, copy) NSString *article_abstract;

/** 所属圈子 */
@property (nonatomic, copy) NSString *article_source;

/** 创建时间 */
@property (nonatomic, assign) NSTimeInterval create_time;

/** 咨询ID */
@property (nonatomic, assign) NSInteger article_id;

/**分类ID */
@property (nonatomic, assign) NSInteger sort_id;

/** 浏览量 */
@property (nonatomic, assign) NSInteger page_view;

/** 咨询内容 */
@property (nonatomic, copy) NSString *article_content;


/** 图片 */
@property (nonatomic, strong) NSArray *imageList;

/** 收藏ID */
@property (nonatomic, assign) NSInteger is_collect_true;

/** 点赞量 */
@property (nonatomic, assign) NSInteger praise_number;

/** 收藏量 */
@property (nonatomic, assign) NSInteger collect_number;

/** 是否点赞 */
@property (nonatomic, assign) NSInteger click_praise_true;

@end



@interface ArticleLabelModel : NSObject

@property (nonatomic, copy) NSString *label_name;

@property (nonatomic, assign) NSInteger label_id;

@property (nonatomic, assign) NSInteger article_number;

@end

