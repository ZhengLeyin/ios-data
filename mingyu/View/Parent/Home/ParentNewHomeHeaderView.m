//
//  ParentNewHomeHeaderView.m
//  mingyu
//
//  Created by apple on 2018/9/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentNewHomeHeaderView.h"
#import "AdvertisementModel.h"
#import "QiniuSDK.h"

@implementation ParentNewHomeHeaderView

{
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;

    int i;
    
}

- (void)awakeFromNib{
    [super awakeFromNib];

    cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth, _bannerView.height) delegate:self placeholderImage:ImageName(@"placeholderImage")];
//    cycleView.layer.cornerRadius = 5;
//    cycleView.layer.masksToBounds  = YES;
    cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    // 同上做一个 其他图片
    cycleView.pageDotImage = [UIImage imageWithColor:[UIColor colorWithHexString:@"ffffff" alpha:0.5] forSize:CGSizeMake(15, 2)];
    // 一个Category给用颜色做ImageView 用15宽2高做一个长方形图片 当前图片
    cycleView.currentPageDotImage = [UIImage imageWithColor:[UIColor whiteColor] forSize:CGSizeMake(15, 2)];
    // 加载网络数组图片 我个人认为这个就有点坑了，理论上这里只能给网络加载的图片实现轮播，但是如果你要DIY一些图片上的文字，就要修改源码了
//    cycleView.imageURLStringsGroup = images;
    
    [_bannerView addSubview:cycleView];
    
    
    NSString *baby_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDbabyhead]];
    [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:baby_url] forState:0 placeholderImage:ImageName(@"默认头像")];
    //    if([userDefault objectForKey:KUDbabyhead]){
    //        [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:[userDefault objectForKey:KUDbabyhead]] forState:UIControlStateNormal];
    //    }
    
    _BabyRecordArray = [NSMutableArray array];
    
    [self getReasource];

}


- (void)getAdvertisement {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAdvertisement];
    NSDictionary *parameters = @{
                                 @"ad_type":@"1",
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
//        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            advertisementArray = [NSMutableArray array];
            NSMutableArray *imageArray = [NSMutableArray array];
            for (NSDictionary *dic in data) {
                AdvertisementModel *model = [AdvertisementModel modelWithJSON:dic];
                [advertisementArray addObject:model];
                [imageArray addObject:[NSString stringWithFormat:@"%@%@",KKaptcha,model.ad_url]];
            }
            cycleView.imageURLStringsGroup = imageArray;
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}


//一键听
- (IBAction)HandPick:(id)sender {
    
    [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
}


//时光印记
- (IBAction)TimeRecord:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    TimeRecordViewController *VC = [TimeRecordViewController new];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

//疫苗提醒
- (IBAction)BabyVaccine:(id)sender {
    BabyVaccineViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"BabyVaccineViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}



//给换宝宝图像
- (IBAction)babyiconButton:(id)sender {
    
    [[self viewController].navigationController presentViewController:self.imagePickerVc animated:YES completion:nil];
}




- (IBAction)leftButton:(id)sender {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    NSInteger day = (currentTime - data)/3600/24;
    
    if (day+i > 0) {
        i--;
        if (_BabyRecordArray.count > day+i+261) {
            [self setViewwithDic:_BabyRecordArray[day+i+261]];
        }
    }
}



- (IBAction)rightButton:(id)sender {
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建时间戳(后台返回的时间 一般是13位数字)
    NSInteger day = (currentTime - data)/3600/24;
    if ( day+i+1 < 365*2) {
        i++;
        if (day+i+261 < _BabyRecordArray.count) {
            [self setViewwithDic:_BabyRecordArray[day+i+261]];
        }
    }
}


- (void)getReasource{

    double tokentime = [userDefault doubleForKey:KUDTokentime];
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970];
    if ([userDefault boolForKey:KUDhasLogin] && time - tokentime > 24*60*60) {
        [self getBabyRecordFromNetwork];
    } else {
        [self getReasourceFromLocalfile];
    }
}


- (void)getReasourceFromLocalfile{
    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    
    if (!array) {
        [self getBabyRecordFromNetwork];
    } else {
        _BabyRecordArray = array;
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        NSInteger day = (currentTime - data)/3600/24;
        if (_BabyRecordArray.count > 261+day ) {
            [self setViewwithDic:_BabyRecordArray[261+day]];
        }
    }
}


- (void)getBabyRecordFromNetwork{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetBabyRecordList];
    NSDictionary *parameters = @{
                                 @"start":@"-260",
                                 @"size":@"800"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _BabyRecordArray = json[@"data"];
            
            NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
            if ([self.BabyRecordArray writeToFile:filePatch atomically:YES]){
                ZPLog(@"sucess");
                
                NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
                NSTimeInterval time = [date timeIntervalSince1970];
                [userDefault setDouble:time forKey:KUDTokentime];
                [userDefault synchronize];
            }
            
            NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            // 创建时间戳(后台返回的时间 一般是13位数字)
            NSInteger day = (currentTime - data)/3600/24;
            if (_BabyRecordArray.count > 261+day ) {
                [self setViewwithDic:_BabyRecordArray[261+day]];
            }
        } else{
            [self getReasourceFromLocalfile];
        }
    } failure:^(NSError *error) {
        [self getReasourceFromLocalfile];
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}


- (void)setViewwithDic:(NSDictionary *)dic{
    
    _babyLength_lab.text = [NSString stringWithFormat:@"平均身高:%@",dic[@"baby_length"]?dic[@"baby_length"]:@"暂无"];
    _babyWeight_lab.text = [NSString stringWithFormat:@"平均体重:%@",dic[@"baby_weight"]?dic[@"baby_weight"]:@"暂无"];
    _content_lab.text = dic[@"content"];
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSString *datastr = [NSString stringWithFormat:@"%f",(currentTime+i*24*60*60)*1000];
    NSString *str = [NSString getCurrentTimes:@"MM月dd日" Time:datastr];
    _date_lab.text = str;
    
    
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSDate *babydate = [NSDate dateWithTimeIntervalSince1970:data];
    
    NSTimeInterval time = currentTime+i*24*60*60+86400;
    NSString *newstr = [NSString transformTime2:[NSString stringWithFormat:@"%f",time*1000]];
    NSTimeInterval newdata = [NSString transformDate:newstr];
    NSDate *newdatas = [NSDate dateWithTimeIntervalSince1970:newdata];
    
    NSInteger day = [dic[@"day_num"] integerValue];
    
//    if (day == 0) {
//        _dayNum_lab.text = @"宝宝今天出生";
//    } else
    if (day >= 0) {
        _dayNum_lab.text = [self numberOfDaysWithFromDate:babydate toDate:newdatas];
    } else {
        NSInteger weak = floorf(-day/7);
        NSInteger days = fmodl(-day, 7);
        NSString *weakstr = @"";
        NSString *daysstr = @"";
        
        if (weak) {
            weakstr = [NSString stringWithFormat:@"%ld周",weak];
        }
        if (days) {
            daysstr = [NSString stringWithFormat:@"%ld天",days];
        }
        _dayNum_lab.text = [NSString stringWithFormat:@"还剩%@%@出生",weakstr,daysstr];
    }
}


- (NSString *)numberOfDaysWithFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate { // 创建一个标准国际时间的日历
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]; // 可根据需要自己设置时区.
    NSDateComponents *comp = [calendar components: NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate:fromDate toDate:toDate options:NSCalendarWrapComponents];
    
    NSString *year = @"";
    NSString *month = @"";
    NSString *day = @"";
    if (comp.year) {
        year = [NSString stringWithFormat:@"%ld岁",(long)comp.year];
    }
    if (comp.month) {
        month = [NSString stringWithFormat:@"%ld月",(long)comp.month];
    }
    if (comp.day) {
        day =  [NSString stringWithFormat:@"%ld天",(long)comp.day];
    }
    return [NSString stringWithFormat:@"%@%@%@",year,month,day];
}



- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (advertisementArray && advertisementArray.count > 0) {
        AdvertisementModel *model = advertisementArray[index];
        //1- 一键听 2- 育儿必读 3- 推荐 4- 视频 5-圈子 6- 时光印记
        if (model.to_type == 1) {
            if (model.from_id) {
                AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
                VC.target_id = model.from_id;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
            
        } else if (model.to_type == 2){
            if (model.from_id) {
                ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
                NewsModel *newsmodel = [[NewsModel alloc] init];
                newsmodel.news_id = model.from_id;
                VC.newsmodel = newsmodel;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
            VC.title = @"育儿必读";
            [[self viewController].navigationController pushViewController:VC animated:YES];
        } else if (model.to_type == 3){
//            [self viewController].tabBarController.selectedIndex = 2;
        } else if (model.to_type == 4){
            if (model.from_id) {
                ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
                VC.video_id = model.from_id;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            [self viewController].tabBarController.selectedIndex = 1;
        } else if (model.to_type == 5){

        } else if (model.to_type == 6){
            [self TimeRecord:nil];
        }
    }
}


-(TZImagePickerController *)imagePickerVc {
    _imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    _imagePickerVc.allowPickingOriginalPhoto = NO;
    _imagePickerVc.naviTitleColor = [UIColor blackColor];
    _imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"50D0F4"];
    _imagePickerVc.isStatusBarDefault = YES;
    _imagePickerVc.delegate = self;
    _imagePickerVc.allowTakePicture = YES;
    _imagePickerVc.allowPickingVideo = NO;
    _imagePickerVc.allowPickingGif = NO;
    _imagePickerVc.naviBgColor = [UIColor whiteColor];
    _imagePickerVc.maxImagesCount = 1;
    _imagePickerVc.showSelectBtn = NO;
    _imagePickerVc.allowCrop = YES;
    _imagePickerVc.cropRect = CGRectMake((kScreenWidth-240)/2 , (kScreenHeight-240)/2, 240, 240);
    return _imagePickerVc;
}



#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    if (photos) {
//        [self uploadImage:photos[0]];
        NSData *imageData = UIImageJPEGRepresentation(photos[0], 1.0 );
        [self getUpToken:imageData];
    }
}

//获取七牛云凭证
- (void)getUpToken:(NSData *)imageData{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetUpToken];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
    NSDictionary *parameters = @{
                                 @"bucketName":bucketName,
                                 @"type":@"user",
                                 @"fileName":fileName
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            [self uploadImageToQNPutData:imageData key:data[@"key"] token:data[@"token"]];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

//上传图片到七牛云
- (void)uploadImageToQNPutData:(NSData *)data key:(NSString *)key token:(NSString *)token{
    QNUploadManager *upManger = [[QNUploadManager alloc] init];
    QNUploadOption *uploadoption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
        ZPLog(@"percent == %.2f", percent);
    } params:nil checkCrc:NO cancellationSignal:nil];
    
    [upManger putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        ZPLog(@"info ===== %@", info);
        ZPLog(@"resp ===== %@", resp);
        [self uploadUserHead:resp[@"key"]];
        
    } option:uploadoption];
}


////上传图像
//- (void)uploadImage:(UIImage *)image{
//
//    NSString *URL = [NSString stringWithFormat:@"%@%@?type=baby",KURL,KImgUpload];
//
//    // 1.创建请求管理者
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
//                                                         @"text/html",
//                                                         @"image/jpeg",
//                                                         @"image/png",
//                                                         @"application/octet-stream",
//                                                         @"text/json",
//                                                         @"multipart/form-data",
//                                                         nil];
//    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
//
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"token"] forHTTPHeaderField:@"token"];   //token
//    [manager.requestSerializer setValue:[userDefault objectForKey:@"device_id"] forHTTPHeaderField:@"device_id"];
//    [manager.requestSerializer setValue:[HttpRequest getsignwithURL:URL] forHTTPHeaderField:@"sign"];
//
//    [manager POST:URL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        NSData *imageData = UIImageJPEGRepresentation(image, 1.0 );
//
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.dateFormat = @"yyyyMMddHHmmss";
//        NSString *str = [formatter stringFromDate:[NSDate date]];
//        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
//        NSString *name = @"file";
//        [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/png"];
//        ZPLog(@"%@",formData);
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        ZPLog(@"%@",json);
//        BOOL success = [json[@"success"] boolValue];
//        if (success) {
//            [self uploadUserHead:json[@"data"]];
//
//        } else {
//            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        ZPLog(@"%@",error);
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
//
//    }];
//}


//更新用户图像
- (void)uploadUserHead:(NSString *)userhead{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"baby_head":userhead,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSString *url = [NSString stringWithFormat:@"%@%@",KKaptcha,userhead];
            [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:ImageName(@"默认头像")];
            [userDefault setObject:userhead forKey:KUDbabyhead];
            [userDefault synchronize];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}


//一键听
- (void)setSubjcArray:(NSMutableArray *)subjcArray{
    _subjcArray = subjcArray;
    if (subjcArray.count > 0) {
        SubjectModel *model1 = subjcArray[0];
        NSString *URL1 = [NSString stringWithFormat:@"%@%@?width=504&height=330",KKaptcha,model1.subject_image];
        [_subjectImage1 sd_setImageWithURL:[NSURL URLWithString:URL1] placeholderImage:ImageName(@"placeholderImage")];
        if (model1.ident_type == 1) {
            _historyImage1.image = ImageName(@"最近");
        } else {
            _historyImage1.image = ImageName(@"推荐");
        }
        if ([DFPlayer shareInstance].currentAudioModel.subject_id == model1.subject_id && [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
            _playstateImage1.image = ImageName(@"一键听_主题列表_开");
        } else {
            _playstateImage1.image = ImageName(@"一键听_主题列表_关");
        }
        _subjecttitle_lab1.text = model1.subject_title;
        _listen_lab1.text = [NSString stringWithFormat:@"%ld人收听",model1.play_count];
    }
    
    if (subjcArray.count > 1) {
        SubjectModel *model2 = subjcArray[1];
        NSString *URL2 = [NSString stringWithFormat:@"%@%@?width=504&height=330",KKaptcha,model2.subject_image];
        [_subjectImage2 sd_setImageWithURL:[NSURL URLWithString:URL2] placeholderImage:ImageName(@"placeholderImage")];
        if (model2.ident_type == 1) {
            _historyImage2.image = ImageName(@"最近");
        } else {
            _historyImage2.image = ImageName(@"推荐");
        }
        if ([DFPlayer shareInstance].currentAudioModel.subject_id == model2.subject_id && [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
            _playstateImage2.image = ImageName(@"一键听_主题列表_开");
        } else {
            _playstateImage2.image = ImageName(@"一键听_主题列表_关");
        }
        _subjecttitle_lab2.text = model2.subject_title;
        _listen_lab2.text = [NSString stringWithFormat:@"%ld人收听",model2.play_count];

    }
}


- (IBAction)subjectButton1:(id)sender {
    if (_subjcArray.count > 0) {
        SubjectModel *model = _subjcArray[0];
        AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
        VC.target_id = model.subject_id;
        [[self viewController].navigationController pushViewController:VC animated:YES];
    }
}


- (IBAction)subjectButton2:(id)sender {
    if (_subjcArray.count > 1) {
        SubjectModel *model = _subjcArray[1];
        AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
        VC.target_id = model.subject_id;
        [[self viewController].navigationController pushViewController:VC animated:YES];
    }
}





@end
