//
//  HandPickCell.h
//  mingyu
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SubjectModel;

@protocol HandPickCellDelegate <NSObject>

- (void)playSubjict:(SubjectModel *)subjmodel;

@end

@interface HandPickCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *leftCellTitle;
@property (weak, nonatomic) IBOutlet UIView *leftCellchooseView;


@property (weak, nonatomic) IBOutlet UIView *rightCellcontentView;
@property (weak, nonatomic) IBOutlet UIImageView *album_image;
@property (weak, nonatomic) IBOutlet UILabel *album_title;
@property (weak, nonatomic) IBOutlet UILabel *play_count;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (nonatomic , weak) id <HandPickCellDelegate> delegate;

@property (nonatomic, strong) SubjectModel *subjectmodel;



+ (instancetype)theLeftCellWithTableView:(UITableView *)tableView;

+ (instancetype)theRightCellWithTableView:(UITableView *)tableView;


@end
