//
//  ParentArticleViewController.m
//  mingyu
//
//  Created by apple on 2018/4/25.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ParentArticleViewController.h"
#import "CLInputToolbar.h"
#import "MarkNoteParser.h"
#import "LoadingView.h"
#import "ChildCareDetailView.h"
#import <WebKit/WebKit.h>

@interface ParentArticleViewController ()<UITableViewDelegate,UITableViewDataSource,WKUIDelegate,WKNavigationDelegate,BottomCommentDelegate,CommentListCellDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, strong) ChildCareDetailView *footView;  //点赞 收藏 view

@property (nonatomic, strong) CLInputToolbar *inputToolbar;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *commentListArray;   //评论列表数据源

@property (nonatomic, strong) NSMutableArray *aboutArticleArray;   //相关资讯数据源

@property (nonatomic, assign) NSInteger comment_number;    //总评论数


@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@property (nonatomic, strong) LoadingView *loadview;

@end

@implementation ParentArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.enableAutoToolbar = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self setTextViewToolbar];

//    _loadview  = [[LoadingView alloc] initWithFrame:self.view.frame inView:self.view];
//    [_loadview show];
    
    [self getArticleInfo];  //获取详情
    
    [self getRecommendArticle];   //获取相关推荐
    
    [self getArticleCommentList];   //获取评论
    
    [self refreshTableview];

}

- (void)dealloc{
    [SVProgressHUD dismiss];
}




- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self addReadTime];
}

- (void)addReadTime{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KRecordLog];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resources_type":@(resources_article),
                                 @"resources_id":@(_newsmodel.news_id),
                                 @"status":@(1),
                                 @"message_type":[constant TransactionState:(MODULE_ARTICLE_CLOSE)],
                                 };
    URL = [NSString connectUrl:parameters url:URL];

    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
        } else {
        }
    } failure:^(NSError *error) {
    }];
}

-(void)setTextViewToolbar {
    self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapActions:)];
    [self.maskView addGestureRecognizer:tap];
    [self.view addSubview:self.maskView];
    self.maskView.hidden = YES;
    self.inputToolbar = [[CLInputToolbar alloc] init];
    self.inputToolbar.textViewMaxLine = 5;
    self.inputToolbar.fontSize = 12;
//    self.inputToolbar.placeholder = @"想勾搭，来评论";
    __weak __typeof(self) weakSelf = self;
    [self.inputToolbar inputToolbarSendText:^(NSString *text) {
        __typeof(&*weakSelf) strongSelf = weakSelf;
        // 清空输入框文字
        [strongSelf.inputToolbar bounceToolbar];
        strongSelf.maskView.hidden = YES;
        
        [weakSelf addCommentwithInputString:text];
    }];
    
    [self.maskView addSubview:self.inputToolbar];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, NaviH)];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
}

-(void)tapActions:(UITapGestureRecognizer *)tap {
    [self.inputToolbar bounceToolbar];
    self.maskView.hidden = YES;
}


//添加评论
- (void)addCommentwithInputString:(NSString *)inputstring{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddComment];
    NSDictionary *dic = @{
                          @"fk_user_id":USER_ID,
                          @"comment_content":inputstring,
                          @"fk_from_id":@(_newsmodel.news_id)
                          };
    NSDictionary *parameters = @{
                                 @"comment":[NSString convertToJsonData:dic],
                                 @"comment_type":@(article_comment)
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@",json);
        ZPLog(@"%@",json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [UIView ShowInfo:@"评论成功" Inview:self.view];
            [userDefault setObject:@"" forKey:KUDLastCommentString];
            [userDefault synchronize];
            CommentModel *model = [CommentModel modelWithJSON:json[@"data"]];
            [self.commentListArray insertObject:model atIndex:0];
            _comment_number ++;
            [_tableView reloadData];
            NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
            [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//刷新
-(void)refreshTableview {
    //上拉加载
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getRecommendArticle];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


//获取相关推荐
- (void)getRecommendArticle{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetRecommendArticle];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"news_id":@(_newsmodel.news_id),
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSArray *dataArray = json[@"data"];
            if (dataArray && dataArray.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in dataArray) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                [self.aboutArticleArray addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//获取评论
- (void)getArticleCommentList{
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KgetContentCommentList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"from_id":[NSString stringWithFormat:@"%ld",(long)_newsmodel.news_id],
                                 @"comment_type":@(article_comment),
                                 @"start":@"0",
                                 @"size":@"3"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL succcess = [json[@"success"] boolValue];
        if (succcess) {
            NSDictionary *data = json[@"data"];
            _comment_number = [data[@"comment_number"] integerValue];
            NSArray *commentList = data[@"commentList"];
            for (NSDictionary *dic in commentList) {
                CommentModel *model = [CommentModel modelWithJSON:dic];
                [self.commentListArray addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


//获取资讯详情
- (void)getArticleInfo{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KgetArticle];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"news_id":@(_newsmodel.news_id)
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
//        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            _newsmodel = [NewsModel modelWithJSON:data];
            NSString *headerString = @"<header><meta name='viewport' content='width=device-width-16, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>";
            NSString* result = [MarkNoteParser toHtml:_newsmodel.news_content imageWidth:kScreenWidth-32];
            ZPLog(@"%@",result);
            [self.webView loadHTMLString:[NSString stringWithFormat:@"%@%@",headerString,result] baseURL:nil];
            _bottomView.haveCollect = _newsmodel.is_collect_true;
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        [self.webView setNeedsLayout];
    }
    //...
}

//  页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    ZPLog(@"webViewDidFinishLoad");
    [webView evaluateJavaScript:@"document.body.scrollHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        //获取页面高度
        double webViewHeight = [result doubleValue];
        self.webView.height = webViewHeight;
        ZPLog(@"%f",webViewHeight);
        [self.tableView reloadData];
    }];
    
    //    [_loadview hidden];
}


//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//    ZPLog(@"webViewDidFinishLoad");
//    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
//
//    self.webView.height = self.webView.scrollView.contentSize.height;
//    [self.tableView reloadData];
//}
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
//    if ([keyPath isEqualToString:@"contentSize"]) {
//        CGSize size = [self.webView sizeThatFits:CGSizeZero];
//        self.webView.frame = CGRectMake(0, 0, size.width, size.height);
//        [self.tableView reloadData];
//        [_loadview hidden];
//    }
//}
//
////处理点击超链接问题
//-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
//    NSURL *requestURL =[ request URL ];
//    if ( ( [ [ requestURL scheme ] isEqualToString: @"http" ] || [ [ requestURL scheme ] isEqualToString: @"https" ] || [ [ requestURL scheme ] isEqualToString: @"mailto" ])
//        && ( navigationType == UIWebViewNavigationTypeLinkClicked ) ) {
//        return ![ [ UIApplication sharedApplication ] openURL:requestURL];
//    }
//    return YES;
//}


#pragma tableView--delegate
#pragma tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1){
        if (self.commentListArray.count >2) {
            return 2;
        }
        return self.commentListArray.count;
    } else {
        return self.aboutArticleArray.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        ChildCareDetailView *header = [ChildCareDetailView theChildCareDetailView1];
        if (_newsmodel) {
            header.model = _newsmodel;
        }
        return  header;
    } else if (section == 1){
        CommentListCell *head = [CommentListCell theCommentNumberCell];
        if (_comment_number) {
            [head.comment_number_Button setTitle:[NSString stringWithFormat:@"共%d条评论",_comment_number] forState:0];
            head.arrowimage.hidden = NO;
            head.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _newsmodel.news_id;
                VC.comment_type = article_comment;
                [self.navigationController pushViewController:VC animated:YES];
            };
        }
        return head;
    } else{
        if (self.aboutArticleArray && self.aboutArticleArray.count > 0) {
            CommentListCell *aboutheadview = [CommentListCell theRecommendHeaderView];
            return aboutheadview;
        } else {
            return nil;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        CGFloat height =  [self calculateString:_newsmodel.news_title Width:24]+20;
        return height + 100;
    } else if (section == 1){
        return 55;
    } else{
        if (self.aboutArticleArray.count > 0) {
            return 60;
        }
        return 0;
    }
}

-(CGFloat)calculateString:(NSString *)str Width:(NSInteger)font {
    CGSize size = [str boundingRectWithSize:CGSizeMake(kScreenWidth-40, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:font]} context:nil].size;
    return size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        _footView = [ChildCareDetailView theChildCareDetailView2];
        _footView.model = _newsmodel;
        return _footView;
    } else if (section == 1){
        if (self.commentListArray.count == 0) {
            CommentListCell *foot = [CommentListCell theNoCommentCell];
            return foot;
        } else if (self.commentListArray.count < 3){
            UIView *view = [UIView new];
            return view;
        } else{
            CommentListCell *foot = [CommentListCell TheMoreCell];
            foot.MoreButtonBlock = ^{
                CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
                VC.targetID = _newsmodel.news_id;
                VC.comment_type = article_comment;
                VC.CommentListBlock = ^(NSMutableArray *commentListArray, NSInteger comment_number) {
                    _commentListArray = commentListArray;
                    _comment_number = comment_number;
                    [_tableView reloadData];
                    if (commentListArray.count > 0) {
                        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                        [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    }
                };
                [self.navigationController pushViewController:VC animated:YES];
            };
            return foot;
        }
    } else{
        UIView *view = [UIView new];
        return view;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 50;
    } else if (section == 1){
        if (self.commentListArray.count == 0) {
            return 100;
        } else if (self.commentListArray.count < 3){
            return 1;
        } else{
            return 50;
        }
    } else{
        return 1;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
        }
        [cell.contentView addSubview:self.webView];
        self.webView.mj_x = 8;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.section == 1) {
        CommentListCell *commentcell = [CommentListCell theCommentCellWithTableView:tableView];
        if (_commentListArray && _commentListArray.count > indexPath.row) {
            CommentModel *commentmodel = [_commentListArray objectAtIndex:indexPath.row];
            [commentcell setcellWithModel:commentmodel praiseType:praise_article_comment];
            commentcell.delegate = self;
        }
        commentcell.selectionStyle = UITableViewCellSelectionStyleNone;
        return commentcell;
    } else {
        NewsModel *model = [[NewsModel alloc] init];
        if (self.aboutArticleArray && self.aboutArticleArray.count > indexPath.row) {
            model = [self.aboutArticleArray objectAtIndex:indexPath.row];
        }
        if (model.news_type == 1) {
            ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
            cell.newsmodel = model;
            return cell;
        } else if (model.news_type == 2){
            ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
            cell.newsmodel = model;
            return cell;
        } else {
            ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
            cell.newsmodel = model;
            return cell;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return self.webView.height;
    } else if (indexPath.section == 1) {
        CommentModel *model = [_commentListArray objectAtIndex:[indexPath row]];
        if (model.height==0) {
            return [CommentListCell cellHeight];
        }else{
            return model.height;
        }
    } else if (indexPath.section == 2) {
        return 130;
    }
    return _tableView.rowHeight;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (![userDefault boolForKey:KUDhasLogin]) {
            CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
            return;
        }
        if (_commentListArray.count > indexPath.row) {
            CommentModel *commentmodel = [_commentListArray objectAtIndex:[indexPath row]];
            if (commentmodel.fk_user_id == [USER_ID integerValue]) {
                [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"复制" block1:^{
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = commentmodel.comment_content;
                    [UIView ShowInfo:@"复制成功" Inview:self.view];
                } title2:@"删除" block2:^{
                    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDeleteCommentById];
                    NSDictionary *parameters = @{
                                                 @"user_id":USER_ID,
                                                 @"comment_id":[NSString stringWithFormat:@"%ld",(long)commentmodel.comment_id],
                                                 @"comment_type":@(article_comment)
                                                 };
                    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                        NSDictionary *json = responseObject;
                        ZPLog(@"%@",json);
                        ZPLog(@"%@",json[@"message"]);
                        BOOL success = [json[@"success"]boolValue];
                        if (success) {
                            [_commentListArray removeObjectAtIndex:indexPath.row];
                            _comment_number --;
                            [_tableView reloadData];
                            if (_commentListArray && _commentListArray.count > 0) {
                                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                                [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                            } else if (_aboutArticleArray && _aboutArticleArray.count > 0){
                                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
                                [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                            }
                        } else {
                            [UIView ShowInfo:json[@"message"] Inview:self.view];
                        }
                    } failure:^(NSError *error) {
                        
                    }];
                }];
            } else {
                [self showActionSheetWithmessage:commentmodel.comment_content Title1:@"回复" block1:^{
                    [self commentReplayButtonClicked:commentmodel];
                } title2:@"举报" block2:^{
                    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddAccusation];
                    NSDictionary *parameters = @{
                                                 @"user_id":USER_ID,
                                                 @"from_id":@(commentmodel.comment_id),
                                                 @"type_id":@(accusation_article_comment)
                                                 };
                    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
                        NSDictionary *json = responseObject;
                        ZPLog(@"%@",json);
                        ZPLog(@"%@",json[@"message"]);
                        BOOL success = [json[@"success"] boolValue];
                        if (success) {
                            [UIView ShowInfo:@"举报成功" Inview:self.view];
                        }
                    } failure:^(NSError *error) {
                        
                    }];
                }];
            }
        }
    } else if (indexPath.section == 2) {
        if (_aboutArticleArray && _aboutArticleArray.count > indexPath.row) {
            NewsModel *model = [_aboutArticleArray objectAtIndex:indexPath.row];
            if (model.news_type == 1) {
                [self recommendClickNewsId:model.news_id resourcesType:resources_article];
                ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
                VC.newsmodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            } else if (model.news_type == 2){
                [self recommendClickNewsId:model.news_id resourcesType:resources_childcare];
                ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
                VC.newsmodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                [self recommendClickNewsId:model.news_id resourcesType:resources_topic];
                ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
                VC.newsmodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
    }
}

//点击相关推荐
- (void)recommendClickNewsId:(NSInteger )news_id resourcesType:(NSInteger )resources_type{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KRecordLog];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"resources_type":@(resources_type),
                                 @"resources_id":@(news_id),
                                 @"status":@(1),
                                 @"message_type":[constant TransactionState:(MODULE_RECOMMEND_CLICK)],
                                 };
    URL = [NSString connectUrl:parameters url:URL];

    [HttpRequest postWithURLString:URL parameters:nil viewcontroller:nil showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
        } else {
        }
    } failure:^(NSError *error) {
    }];
}


#pragma -mark- CommentListCellDelegate
/** 回复 */
- (void)commentReplayButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = _newsmodel.news_id;
    VC.comment_type = article_comment;
    VC.targetCommentmodel = commentmodel;
    [self.navigationController pushViewController:VC animated:YES];
}

/** 查看更多 */
- (void)commentDetailButtonClicked:(CommentModel *)commentmodel{
    CommentViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"CommentViewController"];
    VC.targetID = _newsmodel.news_id;
    VC.comment_type = article_comment;
    [self.navigationController pushViewController:VC animated:YES];
}



#pragma -mark- BottomCommetViewDelegate
//收藏
- (void)collectButtonClicked{
    if (_newsmodel.is_collect_true == 0) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KAddCollect];
        NSDictionary *parameters = @{
                                     @"from_id":[NSString stringWithFormat:@"%ld", _newsmodel.news_id],
                                     @"user_id":USER_ID,
                                     @"collect_type":@(collect_type_news)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                NSInteger is_collect_true = [json[@"data"] integerValue];
                _newsmodel.is_collect_true = is_collect_true;
                _newsmodel.collect_number++;
                [UIView ShowInfo:TipCollectSuccess Inview:self.view];
                [_footView.collect_button setImage:ImageName(@"已收藏") forState:0];
                [_footView.collect_button setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_newsmodel.collect_number] forState:0];
                _bottomView.haveCollect = _newsmodel.is_collect_true;
                
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    } else {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelCollect];
        NSDictionary *parameters = @{
                                     @"idList":@(_newsmodel.is_collect_true)
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                _newsmodel.is_collect_true = 0;
                _newsmodel.collect_number--;
                [UIView ShowInfo:TipUnCollectSuccess Inview:self.view];
                [_footView.collect_button setImage:ImageName(@"收藏") forState:0];
                if (_newsmodel.collect_number) {
                    [_footView.collect_button setTitle:[NSString stringWithFormat:@"  %ld收藏  ",_newsmodel.collect_number] forState:0];
                } else {
                    [_footView.collect_button setTitle:@"  收藏  " forState:0];
                }
                _bottomView.haveCollect = _newsmodel.is_collect_true;
                
            } else {
//                [UIView ShowInfo:json[@"message"] Inview:self.view];
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:self.view];
        }];
    }
}

//分享
- (void)shareButtonClicked{
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _newsmodel.news_title;
    model.thumbURL =  [NSString stringWithFormat:@"%@%@",KKaptcha,_newsmodel.news_head];
    model.webpageUrl = _newsmodel.share_url;
    model.accusationType = accusation_type_news;
    model.from_id = _newsmodel.news_id;
    model.descr = _newsmodel.news_subtitle;
    model.shareType = share_article;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = share_type;
    [shareview show];
}

- (BottomCommetView *)bottomView{
    
    if (_bottomView == nil) {
        NSArray *apparray = [[NSBundle mainBundle] loadNibNamed:@"BottomCommetView" owner:nil options:nil];
        _bottomView = apparray.firstObject;
        _bottomView.frame = CGRectMake(0, kScreenHeight-effectViewH-46, kScreenWidth, 46);
        _bottomView.delegate = self;
        __weak __typeof(self) weakSelf = self;
        _bottomView.CommentButtonBlock = ^{
            weakSelf.maskView.hidden = NO;
            [weakSelf.inputToolbar popToolbar];
        };
        _bottomView.LoctionButtonBlock = ^{
            if (_commentListArray && _commentListArray.count > 0) {
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                [weakSelf.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            } else {
                if (@available(iOS 11.0,*)) {
                    [weakSelf.tableView setContentOffset:CGPointMake(0, self.webView.height+100+20+80) animated:YES];
                } else {
                    [weakSelf.tableView setContentOffset:CGPointMake(0, self.webView.height+100+20+80-64) animated:YES];
                }
            }
        };
    }
    return _bottomView;
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        if (@available(iOS 11.0, *)) {
            _tableView.frame = CGRectMake(0, NaviH, kScreenWidth, kScreenHeight-46-NaviH-effectViewH);
        } else {
            _tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-46-effectViewH);
        }
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        //预计高度为143--即是cell高度
        _tableView.estimatedRowHeight = 150.0f;
        //自适应高度
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
    }
    return _tableView;
}


- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc]initWithFrame:CGRectMake(8, 0, kScreenWidth-16, kScreenHeight-100)];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.userInteractionEnabled = NO;
    }
    return _webView;
}


- (NSMutableArray *)aboutArticleArray{
    if (_aboutArticleArray == nil) {
        _aboutArticleArray = [NSMutableArray array];
    }
    return _aboutArticleArray;
}


- (NSMutableArray *)commentListArray{
    if (_commentListArray == nil) {
        _commentListArray = [NSMutableArray array];
    }
    return _commentListArray;
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)share:(id)sender {
    ShareModel *model = [[ShareModel alloc] init];
    model.titleStr = _newsmodel.news_title;
    model.thumbURL = [NSString stringWithFormat:@"%@%@",KKaptcha,_newsmodel.news_head];
    model.webpageUrl = _newsmodel.share_url;
    model.accusationType = accusation_type_news;
    model.from_id = _newsmodel.news_id;
    model.descr = _newsmodel.news_subtitle;
    model.shareType = share_article;
    ShareView *shareview = [[ShareView alloc] initWithshareModel:model];
    shareview.type = more_type;
    [shareview show];
}

@end
