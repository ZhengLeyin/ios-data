//
//  HomePageAgeRelatedOfHeaderView.m
//  mingyu
//
//  Created by MingYu on 2018/12/18.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageAgeRelatedOfHeaderView.h"
#import "AdvertisementModel.h"
#import "QiniuSDK.h"
@interface HomePageAgeRelatedOfHeaderView()
{
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;
    int i;
}
#pragma mark 两岁前
/**宝宝头像*/
@property (strong, nonatomic)  UIButton *babyicon_button;
/**宝宝出生的天数*/
@property (strong, nonatomic)  UILabel *dayNum_lab;
/**宝宝出生距离天数日期*/
@property (strong, nonatomic)  UILabel *date_lab;
/**宝宝的身高*/
@property (strong, nonatomic)  UILabel *babyLength_lab;
/**宝宝的体重*/
@property (strong, nonatomic)  UILabel *babyWeight_lab;
/**有关宝宝的内容提示*/
@property (strong, nonatomic)  UILabel *content_lab;

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;

@property (nonatomic, strong) NSMutableArray *BabyRecordArray;

@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UIButton *leftButton;

@property (nonatomic, strong) UIButton *rightButton;
@end

@implementation HomePageAgeRelatedOfHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(UIImageView *)backImageView {
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 135)];
        _backImageView.image = [UIImage imageNamed:@"banner-V1.4.0"];
    }
    return _backImageView;
}

-(UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 135)];
        _backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}

-(UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.backgroundColor = [UIColor clearColor];
        _leftButton.frame = CGRectMake(kScale(5), 17, kScale(50), kScale(50));
        [_leftButton setImage:[UIImage imageNamed:@"Combined Shape1-V1.4.0"] forState:(UIControlStateNormal)];
    }
    return _leftButton;
}

-(UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.backgroundColor = [UIColor clearColor];
        _rightButton.frame = CGRectMake(kScreenWidth - kScale(55), 17, kScale(50), kScale(50));
        [_rightButton setImage:[UIImage imageNamed:@"Combined Shape-V1.4.0"] forState:(UIControlStateNormal)];
    }
    return _rightButton;
}

-(UIButton *)babyicon_button {
    if (!_babyicon_button) {
        _babyicon_button = [UIButton buttonWithType:UIButtonTypeCustom];
        _babyicon_button.layer.cornerRadius = kScale(33);
        _babyicon_button.layer.masksToBounds = YES;
        [_babyicon_button  setImage:[UIImage imageNamed:@"icon_headportrait_v1.4.0"] forState:(UIControlStateNormal)];
    }
    return _babyicon_button;
}

-(UILabel *)dayNum_lab {
    if (!_dayNum_lab) {
        _dayNum_lab = [UILabel new];
        _dayNum_lab.text = @"hahaa222222a";
        _dayNum_lab.textColor = [UIColor whiteColor];
        _dayNum_lab.font = FontSize(kScale(16.0));
    }
    return _dayNum_lab;
}

-(UILabel *)date_lab {
    if (!_date_lab) {
        _date_lab = [UILabel new];
        _date_lab.text = @"(12月18)";
        _date_lab.textColor = [UIColor whiteColor];
        _date_lab.font = FontSize(kScale(12.0));
    }
    return _date_lab;
}

-(UILabel *)babyLength_lab {
    if (!_babyLength_lab) {
        _babyLength_lab = [UILabel new];
        _babyLength_lab.text = @"平均身高 30~40cm";
        _babyLength_lab.textColor = [UIColor whiteColor];
        _babyLength_lab.font = FontSize(kScale(12.0));
    }
    return _babyLength_lab;
}

-(UILabel *)babyWeight_lab {
    if (!_babyWeight_lab) {
        _babyWeight_lab = [UILabel new];
        _babyWeight_lab.text = @"平均身高 30~40kg";
        _babyWeight_lab.textColor = [UIColor whiteColor];
        _babyWeight_lab.font = FontSize(kScale(12.0));
    }
    return _babyWeight_lab;
}

-(UILabel *)content_lab {
    if (!_content_lab) {
        _content_lab = [UILabel new];
        _content_lab.text = @"宝宝今天可以站立了，如果有家人的帮助，应该可以小走几步咯～你有这样做吗？";
        _content_lab.numberOfLines = 2;
        _content_lab.textColor = [UIColor whiteColor];
        _content_lab.font = FontSize(kScale(11.0));
    }
    return _content_lab;
}

-(void)createUI {
    [self addSubview:self.backImageView];
    
    [self addSubview:self.backView];

    [self.backView addSubview:self.leftButton];
    
    [self.backView addSubview:self.rightButton];
 
    [self.backView addSubview:self.babyicon_button];
    [_babyicon_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_leftButton);
        make.left.equalTo(_leftButton.mas_right).offset(kScale(6));
        make.size.mas_equalTo(CGSizeMake(kScale(66),kScale(66)));
    }];
   
    [self.backView addSubview:self.dayNum_lab];
    [_dayNum_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_backView).offset(kScale(13.0));
        make.left.equalTo(_babyicon_button.mas_right).offset(kScale(30));
        make.height.equalTo(@22);
        //make.size.mas_equalTo(CGSizeMake(kScale(66),kScale(66)));
    }];


    [_backView addSubview:self.date_lab];
    [_date_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_dayNum_lab);
        make.left.equalTo(_dayNum_lab.mas_right).offset(kScale(2));
        //make.height.equalTo(22.0));
        //make.size.mas_equalTo(CGSizeMake(kScale(66),kScale(66)));
    }];

 
    [_backView addSubview:self.babyLength_lab];
    [_babyLength_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_dayNum_lab.mas_bottom).offset(kScale(2));
        make.left.equalTo(_dayNum_lab);
        make.height.equalTo(@16.0);
    }];
    
    [self.backView addSubview:self.babyWeight_lab];
    [_babyWeight_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_babyLength_lab.mas_bottom).offset(kScale(2));
        make.left.equalTo(_dayNum_lab);
        make.height.equalTo(@16.0);
    }];

    [self.backView addSubview:self.content_lab];
    [_content_lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_babyicon_button.mas_bottom).offset(kScale(10));
        make.bottom.equalTo(_backView.mas_bottom).offset(kScale(-20));
        make.left.equalTo(_backView).offset(kScale(40));
        make.right.equalTo(_backView).offset(kScale(-40));
    }];
    
#pragma mark 两岁前 属性处理
    /**宝宝头像*/
    NSString *baby_url = [NSString stringWithFormat:@"%@%@",KKaptcha,[userDefault objectForKey:KUDbabyhead]];
    [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:baby_url] forState:0 placeholderImage:ImageName(@"icon_headportrait_v1.4.0")];
    
    /**相关数据处理*/
    #pragma mark 宝宝前一天
    [[_leftButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        NSInteger day = (currentTime - data)/3600/24;
        if (day+i > 0) {
            i--;
            if (_BabyRecordArray.count > day+i+261) {
                [self setViewwithDic:_BabyRecordArray[day+i+261]];
            }
        }
    }];
    #pragma mark 宝宝后一天
    [[_rightButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        NSInteger day = (currentTime - data)/3600/24;
        if ( day+i+1 < 365*2) {
            i++;
            if (day+i+261 < _BabyRecordArray.count) {
                [self setViewwithDic:_BabyRecordArray[day+i+261]];
            }
        }
    }];
    #pragma mark 宝宝头像
    [[_babyicon_button rac_signalForControlEvents:(UIControlEventTouchUpInside)]subscribeNext:^(__kindof UIControl * _Nullable x) {
           [[self viewController].navigationController presentViewController:self.imagePickerVc animated:YES completion:nil];
    }];
    
    
#pragma mark 两岁前数据请求
    [self getReasource];
}


#pragma mark 数据处理
- (void)getReasource {
    double tokentime = [userDefault doubleForKey:KUDTokentime];
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970];
    if ([userDefault boolForKey:KUDhasLogin] && time - tokentime > 24*60*60) {
        [self getBabyRecordFromNetwork];
    } else {
        [self getReasourceFromLocalfile];
    }
}

- (void)getReasourceFromLocalfile {
    NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePatch];
    if (!array) {
        [self getBabyRecordFromNetwork];
    } else {
        _BabyRecordArray = array;
        NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
        NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
        // 创建时间戳(后台返回的时间 一般是13位数字)
        NSInteger day = (currentTime - data)/3600/24;
        if (_BabyRecordArray.count > 261+day ) {
            [self setViewwithDic:_BabyRecordArray[261+day]];
        }
    }
}

- (void)getBabyRecordFromNetwork {
    
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetBabyRecordList];
    NSDictionary *parameters = @{
                                 @"start":@"-260",
                                 @"size":@"800"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _BabyRecordArray = json[@"data"];
            
            NSString *filePatch = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"BabyRecordList.plist"];
            if ([self.BabyRecordArray writeToFile:filePatch atomically:YES]){
                ZPLog(@"sucess");
                NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
                NSTimeInterval time = [date timeIntervalSince1970];
                [userDefault setDouble:time forKey:KUDTokentime];
                [userDefault synchronize];
            }
            NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            // 创建时间戳(后台返回的时间 一般是13位数字)
            NSInteger day = (currentTime - data)/3600/24;
            if (_BabyRecordArray.count > 261+day ) {
                [self setViewwithDic:_BabyRecordArray[261+day]];
            }
        } else{
            [self getReasourceFromLocalfile];
        }
    } failure:^(NSError *error) {
        [self getReasourceFromLocalfile];
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

/**两岁前 左右按钮 天数请求*/
- (void)setViewwithDic:(NSDictionary *)dic{
    _babyLength_lab.text = [NSString stringWithFormat:@"平均身高:%@",dic[@"baby_length"]?dic[@"baby_length"]:@"暂无"];
    _babyWeight_lab.text = [NSString stringWithFormat:@"平均体重:%@",dic[@"baby_weight"]?dic[@"baby_weight"]:@"暂无"];
    _content_lab.text = dic[@"content"];
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSString *datastr = [NSString stringWithFormat:@"%f",(currentTime+i*24*60*60)*1000];
    NSString *str = [NSString getCurrentTimes:@"MM月dd日" Time:datastr];
    _date_lab.text = [NSString stringWithFormat:@"(%@)",str];
    
    NSTimeInterval data = [NSString transformDate:[userDefault objectForKey:KUDuser_birthday]];
    NSDate *babydate = [NSDate dateWithTimeIntervalSince1970:data];
    
    NSTimeInterval time = currentTime+i*24*60*60+86400;
    NSString *newstr = [NSString transformTime2:[NSString stringWithFormat:@"%f",time*1000]];
    NSTimeInterval newdata = [NSString transformDate:newstr];
    NSDate *newdatas = [NSDate dateWithTimeIntervalSince1970:newdata];
    
    NSInteger day = [dic[@"day_num"] integerValue];
    
    //    if (day == 0) {
    //        _dayNum_lab.text = @"宝宝今天出生";
    //    } else
    if (day >= 0) {
        _dayNum_lab.text = [self numberOfDaysWithFromDate:babydate toDate:newdatas];
    } else {
        NSInteger weak = floorf(-day/7);
        NSInteger days = fmodl(-day, 7);
        NSString *weakstr = @"";
        NSString *daysstr = @"";
        
        if (weak) {
            weakstr = [NSString stringWithFormat:@"%ld周",weak];
        }
        if (days) {
            daysstr = [NSString stringWithFormat:@"%ld天",days];
        }
        _dayNum_lab.text = [NSString stringWithFormat:@"还剩%@%@出生",weakstr,daysstr];
    }
}

- (NSString *)numberOfDaysWithFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate { // 创建一个标准国际时间的日历
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]; // 可根据需要自己设置时区.
    NSDateComponents *comp = [calendar components: NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate:fromDate toDate:toDate options:NSCalendarWrapComponents];
    
    NSString *year = @"";
    NSString *month = @"";
    NSString *day = @"";
    if (comp.year) {
        year = [NSString stringWithFormat:@"%ld岁",(long)comp.year];
    }
    if (comp.month) {
        month = [NSString stringWithFormat:@"%ld个月",(long)comp.month];
    }
    if (comp.day) {
        day =  [NSString stringWithFormat:@"%ld天",(long)comp.day];
    }
    return [NSString stringWithFormat:@"%@%@%@",year,month,day];
}

-(TZImagePickerController *)imagePickerVc {
    _imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    _imagePickerVc.allowPickingOriginalPhoto = NO;
    _imagePickerVc.naviTitleColor = [UIColor blackColor];
    _imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"50D0F4"];
    _imagePickerVc.isStatusBarDefault = YES;
    _imagePickerVc.delegate = self;
    _imagePickerVc.allowTakePicture = YES;
    _imagePickerVc.allowPickingVideo = NO;
    _imagePickerVc.allowPickingGif = NO;
    _imagePickerVc.naviBgColor = [UIColor whiteColor];
    _imagePickerVc.maxImagesCount = 1;
    _imagePickerVc.showSelectBtn = NO;
    _imagePickerVc.allowCrop = YES;
    _imagePickerVc.cropRect = CGRectMake((kScreenWidth-240)/2 , (kScreenHeight-240)/2, 240, 240);
    return _imagePickerVc;
}

#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    if (photos) {
        //        [self uploadImage:photos[0]];
        NSData *imageData = UIImageJPEGRepresentation(photos[0], 1.0 );
        [self getUpToken:imageData];
    }
}

//获取七牛云凭证
- (void)getUpToken:(NSData *)imageData{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KGetUpToken];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
    NSDictionary *parameters = @{
                                 @"bucketName":bucketName,
                                 @"type":@"user",
                                 @"fileName":fileName
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:nil success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSDictionary *data = json[@"data"];
            [self uploadImageToQNPutData:imageData key:data[@"key"] token:data[@"token"]];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

//上传图片到七牛云
- (void)uploadImageToQNPutData:(NSData *)data key:(NSString *)key token:(NSString *)token{
    QNUploadManager *upManger = [[QNUploadManager alloc] init];
    QNUploadOption *uploadoption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
        ZPLog(@"percent == %.2f", percent);
    } params:nil checkCrc:NO cancellationSignal:nil];
    
    [upManger putData:data key:key token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        ZPLog(@"info ===== %@", info);
        ZPLog(@"resp ===== %@", resp);
        [self uploadUserHead:resp[@"key"]];
        
    } option:uploadoption];
}

//更新用户图像
- (void)uploadUserHead:(NSString *)userhead{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"baby_head":userhead,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSString *url = [NSString stringWithFormat:@"%@%@",KKaptcha,userhead];
            [_babyicon_button sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:ImageName(@"默认头像")];
            [userDefault setObject:userhead forKey:KUDbabyhead];
            [userDefault synchronize];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:[self viewController].view];
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}



@end
