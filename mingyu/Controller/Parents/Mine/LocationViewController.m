//
//  LocationViewController.m
//  mingyu
//
//  Created by apple on 2018/6/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "LocationViewController.h"

@interface LocationViewController ()
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *addressData;

@property (nonatomic, strong) UIView *nothingView;


@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
//    self.nothingView.hidden = YES;

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getaddress];

}

- (void)getaddress{
    _addressData = [NSMutableArray array];
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAddressList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID]
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *list = json[@"data"];
            if (list && list.count > 0) {
                self.nothingView.hidden = YES;
                _rightButton.hidden = NO;
            } else{
                self.nothingView.hidden = NO;
                _rightButton.hidden = YES;
            }
            for (NSDictionary *dic in list) {
                AddressModel *model = [AddressModel modelWithJSON:dic];
                [_addressData addObject:model];
            }
            [_tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}




#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_addressData && _addressData.count > 0) {
        return _addressData.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"LocationCell";
    LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[LocationCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identify];
    }
    if (_addressData && _addressData.count > indexPath.row) {
        cell.addressmodel = [_addressData objectAtIndex:indexPath.row];
    }
    cell.refreshTableview = ^(NSInteger type) {
        if (type == 0) {
            [UIView ShowInfo:@"删除地址成功" Inview:self.view];
        } else {
            [UIView ShowInfo:@"设置默认地址成功" Inview:self.view];
        }
        [self getaddress];
    };
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (_addressData && _addressData.count > indexPath.row) {
//        AddLocationViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddLocationViewController"];
//        VC.addressmodel = [_addressData objectAtIndex:indexPath.row];
//        [self.navigationController pushViewController:VC animated:YES];
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)addLocationButton:(id)sender {
    
    AddLocationViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddLocationViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (UIView *)nothingView{
    if (_nothingView == nil) {
        _nothingView = [[UIView alloc] initWithFrame:self.view.bounds];
        _nothingView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
        [self.view addSubview:_nothingView];

        UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 203)];
        topview.backgroundColor = [UIColor whiteColor];
        [_nothingView addSubview:topview];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-188)/2, 13, 188, 154)];
        imageview.image = ImageName(@"无地址");
        [topview addSubview:imageview];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 170, kScreenWidth, 30)];
        lab.text = @"还没有添加收货地址";
        lab.textColor = [UIColor colorWithHexString:@"D0D0D0"];
        lab.font = FontSize(12);
        lab.textAlignment = NSTextAlignmentCenter;
        [topview addSubview:lab];
        
        UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addButton.frame = CGRectMake(0, 213, kScreenWidth, 50);
        addButton.backgroundColor = [UIColor whiteColor];
        [addButton setImage:ImageName(@"添加地址") forState:UIControlStateNormal];
        [addButton setTitle:@" 添加收货地址" forState:UIControlStateNormal];
        [addButton.titleLabel setFont:FontSize(16)];
        [addButton setTitleColor:[UIColor colorWithHexString:@"2C2C2C"] forState:UIControlStateNormal];
        [addButton addTarget:self action:@selector(addLocationButton:) forControlEvents:UIControlEventTouchUpInside];
        [_nothingView addSubview:addButton];
    }
    return _nothingView;
}






@end


@implementation LocationCell


- (void)setAddressmodel:(AddressModel *)addressmodel{
    _addressmodel = addressmodel;
    _name_lab.text = addressmodel.address_name;
    if (addressmodel.address_phone.length == 11) {
        _phone_lab.text = [addressmodel.address_phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    } else{
        _phone_lab.text = addressmodel.address_phone;
    }
    _address_lab.text = [NSString stringWithFormat:@"%@%@",addressmodel.area_name, addressmodel.address_detail];
    [_default_btn setImage:ImageName(addressmodel.is_default == 1 ?@"一键听_xuanz_gou":@"一键听_xuanz") forState:UIControlStateNormal];
}


- (IBAction)DefaulButton:(id)sender {
    if (_addressmodel.is_default != 1) {
        NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateAddress];
        NSDictionary *dic = @{
                                 @"fk_user_id" : USER_ID,
                                 @"address_name":_addressmodel.address_name,
                                 @"address_phone":_addressmodel.address_phone,
                                 @"area_name":_addressmodel.area_name,
                                 @"address_detail":_addressmodel.address_detail,
                                 @"is_default": @(1),
                                 @"address_id" : @(_addressmodel.address_id)
                                 };

        NSDictionary *parameters = @{
                                     @"address":[NSString convertToJsonData:dic]
                                     };
        [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
            NSDictionary *json = responseObject;
            ZPLog(@"%@\n%@",json,json[@"message"]);
            BOOL success = [json[@"success"] boolValue];
            if (success) {
                if (self.refreshTableview) {
                    self.refreshTableview(1);
                }
            }
        } failure:^(NSError *error) {
//            [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view ];
        }];
    }
}


- (IBAction)EditButton:(id)sender {

    AddLocationViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddLocationViewController"];
    VC.addressmodel = _addressmodel;
    VC.title = @"编辑收货地址";
    [[self viewController].navigationController pushViewController:VC animated:YES];
    
}

- (IBAction)DeleteButton:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@"确定要删除该地址吗？"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
    [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
    
    UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              //响应事件
                                                              NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelAddress];
                                                              NSDictionary *parameters = @{
                                                                                           @"user_id" : USER_ID,
                                                                                           @"address_id" : @(_addressmodel.address_id)
                                                                                           };
                                                              [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:[self viewController] success:^(id responseObject) {
                                                                  NSDictionary *json = responseObject;
                                                                  ZPLog(@"%@\n%@",json,json[@"message"]);
                                                                  BOOL success = [json[@"success"] boolValue];
                                                                  if (success) {
                                                                      if (self.refreshTableview) {
                                                                          self.refreshTableview(0);
                                                                      }
                                                                  }
                                                              } failure:^(NSError *error) {
//                                                                  [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
                                                              }];
                                                          }];
    [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
    
    [alert addAction:cancelAction];
    [alert addAction:confirmAction];
    
    [[self viewController] presentViewController:alert animated:YES completion:nil];
    
}

@end





