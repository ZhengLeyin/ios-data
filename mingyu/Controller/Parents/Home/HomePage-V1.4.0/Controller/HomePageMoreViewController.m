//
//  HomePageMoreViewController.m
//  mingyu
//
//  Created by MingYu on 2018/12/26.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "HomePageMoreViewController.h"
#import "HomePageViewModel.h"
#import "HomePageModel.h"
#import "HomePageRemcommendOfAlbumCell.h"
#import "SideBySideVideosCell.h"
#import "HomePageMoreModel.h"
@interface HomePageMoreViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) HomePageViewModel *homePageVM;
@property(nonatomic, strong) NSMutableArray *sectionArray;
@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, strong) NSMutableArray *objectListArray;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger start;
@property (nonatomic, assign) NSInteger startAlbum;
@property (nonatomic, assign) BOOL lastPage;
@end

@implementation HomePageMoreViewController

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:self.tableView];
    [self setNavigationItem];
    [self refreshTableview];
}

/** 返回上一层 */
/** 搜索🔍 */
-(void)setNavigationItem {
    UIImage *leftImage = [[UIImage imageNamed:@"Combined Shape Back"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    
    UIImage *rightImage = [[UIImage imageNamed:@"ExcellentClass搜索icon"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(SearchButton)];
}

//搜索
- (void)SearchButton {
    SearchViewController *VC = [[SearchViewController alloc] init];
    VC.searchType = SearchCourse_type;
    [self.navigationController pushViewController:VC animated:YES];
}

-(void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

//刷新
-(void)refreshTableview {
    _start = 0;
    _startAlbum = 0;
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.objectListArray.count > 0) {
            [self.objectListArray removeAllObjects];
            _start = 0;
            _startAlbum = 0;
        }
        [_tableView.mj_footer resetNoMoreData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setRequestData];  //加载cell数据
        });
    }];
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
           [self setRequestData];
        });
    }];
    footer.stateLabel.font = FontSize(12);
    _tableView.mj_footer = footer;
    
}

-(void)setRequestData {
  
    [self.homePageVM getKHomePageMoreMenuId:144 start:_start startAlbum:_startAlbum lastPage:YES];
    __weak typeof(self) weakSelf = self;
    self.homePageVM.returnReturnDataMoreBlock = ^(NSArray * _Nonnull data, BOOL lastPage, NSUInteger start, NSUInteger startAlbum) {
        weakSelf.start = start;
        weakSelf.startAlbum = startAlbum;
        MingYuLoggerD(@"_start= %ld _startAlbum=%ld count = %ld",start,startAlbum,data.count);
        if (data && data.count >= 2) {
            _lastPage = NO;
        } else {
            _lastPage = YES;
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        for (MoreSubmenumessagelist *listmodel in data) {
            if (listmodel.type == 1 && listmodel.objectList.count > 1) {
                [weakSelf.objectListArray addObject:listmodel];
            } else {
                for (MoreObjectlist *model in listmodel.objectList) {
                    model.type = listmodel.type;
                    [weakSelf.objectListArray addObject:model];
                }
            }
        }
        
        MingYuLoggerI(@"list === %ld **** ",weakSelf.objectListArray.count);
        [weakSelf.tableView reloadData];
        
    };
//    self.homePageVM.returnReturnDataBlock = ^(NSArray * _Nonnull data) {
//        [weakSelf.tableView.mj_header endRefreshing];
//        [weakSelf.tableView.mj_footer endRefreshing];
//        for (MoreSubmenumessagelist *listmodel in data) {
//            if (listmodel.type == 1 && listmodel.objectList.count > 1) {
//                [weakSelf.objectListArray addObject:listmodel];
//            } else {
//                for (MoreObjectlist *model in listmodel.objectList) {
//                    model.type = listmodel.type;
//                    [weakSelf.objectListArray addObject:model];
//                }
//            }
//        }
//
//        MingYuLoggerI(@"list === %ld **** ",weakSelf.objectListArray.count);
//        [weakSelf.tableView reloadData];
//    };

}

#pragma tableView--delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     return self.objectListArray.count;
}

/**1.视频 2.资讯 3.主题(一键听) 4.课程 5.育儿必读*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.backgroundColor = [UIColor yellowColor];
    cell.textLabel.text = @"hahah";
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if ([self.objectListArray count] > indexPath.row) {
        MoreSubmenumessagelist *listmodel = self.objectListArray[indexPath.row];
        MoreObjectlist *objectmodel = self.objectListArray[indexPath.row];

        if (listmodel.type == 1) {
            if (listmodel.objectList.count > 1) {
                SideBySideVideosCell *cell =  [SideBySideVideosCell cellWithTableView:tableView];
                NSMutableArray *arr = [NSMutableArray array];
                [arr addObjectsFromArray:listmodel.objectList];
                cell.dataArray = arr;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            } else {
                ParentVideoCell *cell = [ParentVideoCell theParentVideoCellWithTableView:tableView];
                cell.homePageVCOrOtherVCType = 1;
                cell.videomodel = (VideoModel *)objectmodel;
                cell.play_image.hidden = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
          
        } else if (listmodel.type == 2) {
            
            if (objectmodel.news_type == 1) { //资讯
                ParentRecommenCell *cell = [ParentRecommenCell theParentRecommenCellWithTableView:tableView];
                cell.newsmodel = (NewsModel *)objectmodel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            } else { //育儿必读
                ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
                cell.newsmodel = (NewsModel *)objectmodel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
           
        } else if (listmodel.type == 3) {
            HomePageRemcommendOfAlbumCell *cell = [HomePageRemcommendOfAlbumCell theHomePageRemcommendOfAlbumCell:tableView];
            cell.subjectModel = (SubjectModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else if (listmodel.type == 4) {
            ParentCouresCell *cell = [ParentCouresCell theCellWithTableView:tableView];
            cell.coursemodel = (CourseModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else if (listmodel.type == 5) {
            ParentChildCareCell *cell = [ParentChildCareCell theChildCareCellWithtableView:tableView];
            cell.newsmodel = (NewsModel *)objectmodel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else {

            return cell;
        }
    } else {
        return cell;
    }

 
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    MoreSubmenumessagelist *listmodel = self.objectListArray[indexPath.row];
    //MoreObjectlist *objectmodel = self.objectListArray[indexPath.row];
    if (listmodel.type == 1) {
        if (listmodel.objectList.count > 1) {
            return 120;
        } else {
            return 230;
        }
    } else if (listmodel.type == 2) {
        return 130;
    } else if (listmodel.type == 3) {
        return 110;
    } else if (listmodel.type == 4) {
        return 155;
    } else if (listmodel.type == 5) {
        return 130;
    } else {
        return 0;
    }
}

#pragma mark 并排视频单独处理   一个视频不用单独处理
/**1.视频 2.资讯 3.主题(一键听) 4.课程 5.育儿必读*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.objectListArray count] > indexPath.row) {
        MoreSubmenumessagelist *listmodel = self.objectListArray[indexPath.row];
        MoreObjectlist *objectmodel = self.objectListArray[indexPath.row];
        if (listmodel.type  == 1) {
            if (listmodel.objectList.count > 1) {
                
            }else {
                VideoModel *model = (VideoModel *)objectmodel;
                ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
                VC.video_id = model.video_id;
                VC.video_id = 1425;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 2) {/**单独处理*/
            if (objectmodel.news_type == 1) {
                ParentArticleViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentArticleViewController"];
                VC.newsmodel = (NewsModel *)objectmodel;
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
                VC.newsmodel = (NewsModel *)objectmodel;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 3) {
            SubjectModel *model = (SubjectModel *)objectmodel;
            AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
            VC.target_id = model.subject_id;
            [self.navigationController pushViewController:VC animated:YES];
        }else if (listmodel.type  == 4) {
            CourseModel *model = (CourseModel *)objectmodel;
            if (model.course_type == 1) {
                CourseVideoViewController *VC = [[CourseVideoViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            } else{
                CourseAudioViewController *VC = [[CourseAudioViewController alloc] init];
                VC.coursemodel = model;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }else if (listmodel.type  == 5) {
            ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
            VC.newsmodel = (NewsModel *)objectmodel;
            [self.navigationController pushViewController:VC animated:YES];
        }else {
            
        }
    }
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH) style:UITableViewStylePlain];
        //_tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //_tableView.tableHeaderView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        //_tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    }
    return _tableView;
}

-(HomePageViewModel *)homePageVM {
    if (!_homePageVM) {
        _homePageVM = [HomePageViewModel new];
    }
    return  _homePageVM;
}

-(NSMutableArray *)sectionArray {
    if (!_sectionArray) {
        _sectionArray = [NSMutableArray array];
    }
    return _sectionArray;
}

-(NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(NSMutableArray *)objectListArray {
    if (!_objectListArray) {
        _objectListArray = [NSMutableArray array];
    }
    return _objectListArray;
}
@end
