//
//  CVDetailViewController.h
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"

@class CourseModel;


@interface CVDetailViewController : UIViewController <JXPagerViewListViewDelegate>

/** type 0 为需要支付  1 不需要支付 */
@property (nonatomic, assign) NSUInteger  type;

@property (nonatomic, strong) CourseModel *coursemodel;

@property (nonatomic, copy) void (^PushBlock)(UIViewController *vc);


@end
