//
//  ParentHomePageBannerView.m
//  mingyu
//
//  Created by MingYu on 2018/12/13.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import "ParentHomePageBannerView.h"
#import "AdvertisementModel.h"
@implementation ParentHomePageBannerView
{
    SDCycleScrollView *cycleView;
    NSMutableArray *advertisementArray;
    int i;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

-(void)createUI {
    cycleView =  [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenWidth - 40, 135) delegate:self placeholderImage:ImageName(@"placeholderImage")];
    cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    cycleView.pageDotImage = [UIImage imageWithColor:[UIColor colorWithHexString:@"ffffff" alpha:0.5] forSize:CGSizeMake(15, 2)];
    // 一个Category给用颜色做ImageView 用15宽2高做一个长方形图片 当前图片
    cycleView.currentPageDotImage = [UIImage imageWithColor:[UIColor whiteColor] forSize:CGSizeMake(15, 2)];
    [self addSubview:cycleView];
    /**请求接口*/
    [self getAdvertisement];
}

+ (instancetype)TheParentHomePageBannerView {
    return [[NSBundle mainBundle] loadNibNamed:@"ParentHomePageBannerView" owner:nil options:nil][0];
}

- (void)getAdvertisement {
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetAdvertisement];
    NSDictionary *parameters = @{
                                 @"ad_type":@"1",
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:[self viewController] showalert:NO success:^(id responseObject) {
        NSDictionary *json = responseObject;
        //        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            advertisementArray = [NSMutableArray array];
            NSMutableArray *imageArray = [NSMutableArray array];
            for (NSDictionary *dic in data) {
                AdvertisementModel *model = [AdvertisementModel modelWithJSON:dic];
                [advertisementArray addObject:model];
                [imageArray addObject:[NSString stringWithFormat:@"%@%@",KKaptcha,model.ad_url]];
            }
            cycleView.imageURLStringsGroup = imageArray;
        }
    } failure:^(NSError *error) {
        //        [UIView ShowInfo:TipWrongMessage Inview:[self viewController].view];
    }];
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (advertisementArray && advertisementArray.count > 0) {
        AdvertisementModel *model = advertisementArray[index];
        //1- 一键听 2- 育儿必读 3- 推荐 4- 视频 5-圈子 6- 时光印记
        if (model.to_type == 1) {
            if (model.from_id) {
                AlbumDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"AlbumDetailViewController"];
                VC.target_id = model.from_id;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            [[self viewController].navigationController pushViewController:[HandPickViewController new] animated:YES];
            
        } else if (model.to_type == 2){
            if (model.from_id) {
                ChildCareDetailViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ChildCareDetailViewController"];
                NewsModel *newsmodel = [[NewsModel alloc] init];
                newsmodel.news_id = model.from_id;
                VC.newsmodel = newsmodel;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            ParentChildCareViewController *VC = [KParentHomeStoryboard instantiateViewControllerWithIdentifier:@"ParentChildCareViewController"];
            VC.title = @"育儿必读";
            [[self viewController].navigationController pushViewController:VC animated:YES];
        } else if (model.to_type == 3){

        } else if (model.to_type == 4){
            if (model.from_id) {
                ParentVideoDetailViewController *VC =  [KParentVideoStoyboard instantiateViewControllerWithIdentifier:@"ParentVideoDetailViewController"];
                VC.video_id = model.from_id;
                [[self viewController].navigationController pushViewController:VC animated:YES];
                return ;
            }
            [self viewController].tabBarController.selectedIndex = 1;
            
        } else if (model.to_type == 5){
            
        } else if (model.to_type == 6){
            if (![userDefault boolForKey:KUDhasLogin]) {
                CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
                [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
                return;
            }
            if (![HttpRequest NetWorkIsOK]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
            TimeRecordViewController *VC = [TimeRecordViewController new];
            [[self viewController].navigationController pushViewController:VC animated:YES];
        }
    }
}
    


@end
