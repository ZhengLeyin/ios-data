//
//  UIImage+Extension.h
//  mingyu
//
//  Created by MingYu on 2018/11/17.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Extension)
/** 切圆角 */
+ (UIImage *)imageWithRoundCorner:(UIImage *)sourceImage cornerRadius:(CGFloat)cornerRadius size:(CGSize)size;
/** 切圆角 */
+ (UIImage *)imageWithSize:(CGSize)sizeToFit radius:(CGFloat)radius  Color:(UIColor *)color;
/** CGContext 裁剪 切圆角  */
+ (UIImage *)CGContextClip:(UIImage *)img cornerRadius:(CGFloat)c;
/** UIBezierPath 裁剪 切圆角 */
+ (UIImage *)UIBezierPathClip:(UIImage *)img cornerRadius:(CGFloat)c;
/** 非等比缩放，生成的图片可能会被拉伸 */
+(UIImage *)scaleNoImage:(UIImage *)image size:(CGSize)size;
/** 等比缩放 */
+ (UIImage *)scaleImage:(UIImage *)image size:(CGSize)size;
/**banner 轮播图占位图标*/
+ (UIImage *)imageWithColor:(UIColor *)color forSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
