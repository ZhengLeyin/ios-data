//
//  PayNetWorkHttpViewModel.h
//  mingyu
//
//  Created by MingYu on 2018/11/23.
//  Copyright © 2018 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PayNetWorkHttpViewModel : NSObject
/** 育币充值请求 */
- (void)GetAmountOptions;
@end

NS_ASSUME_NONNULL_END
