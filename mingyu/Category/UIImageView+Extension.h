//
//  UIImageView+Extension.h
//  mingyu
//
//  Created by apple on 2018/05/29.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extension)

- (void)zp_setImageWithURL:(nullable NSString *)url placeholderImage:(nullable UIImage *)placeholder;



@end
