//
//  ChangeNameViewController.m
//  mingyu
//
//  Created by apple on 2018/6/1.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "ChangeNameViewController.h"

@interface ChangeNameViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextFiled;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end

@implementation ChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _nameTextFiled.placeholder = _name;
    
}



- (IBAction)deleteButton:(id)sender {
    _nameTextFiled.text = nil;  
    
}


- (IBAction)saveButton:(id)sender {
    
    if (_nameTextFiled.text.length < 1 || _nameTextFiled.text.length > 16 ) {
        [UIView ShowInfo:@"用户名不合法" Inview:self.view];
        return;
    }
    
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KUpdateUser];
    NSDictionary *dic = @{@"user_name":_nameTextFiled.text,
                          @"user_id":USER_ID
                          };
    
    NSDictionary *parameters = @{
                                 @"user":[NSString convertToJsonData:dic]
                                 };
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [MobClick event:@"ChangeNameViewController"];  //友盟统计

            [UIView ShowInfo:@"修改成功" Inview:self.view];

            [self performSelector:@selector(back:) withObject:nil afterDelay:1.0f];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
    
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
