//
//  ChildcareMessageModel.h
//  mingyu
//
//  Created by apple on 2018/5/2.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChildcareMessageModel : NSObject


/**
 主键ID
 */
@property (nonatomic, assign) NSInteger childcare_id;

/**
 标签ID
 */
@property (nonatomic, assign) NSInteger fk_label_id;

/**
 育儿标题
 */
@property (nonatomic, copy) NSString *childcare_title;

/**
 育儿内容
 */
@property (nonatomic, copy) NSString *childcare_content;

/**
 育儿简介
 */
@property (nonatomic, copy) NSString *childcare_abstract;

/**
 育儿图片
 */
@property (nonatomic, copy) NSString *childcare_head;

/**
 创建时间
 */
@property (nonatomic, copy) NSString *create_time;

/**
 浏览量
 */
@property (nonatomic, assign) NSInteger page_view;

/**
 点赞数
 */
@property (nonatomic, assign) NSInteger praise_number;

/**
 收藏数
 */
@property (nonatomic, assign) NSInteger collect_number;

/**
 已收藏
 */
@property (nonatomic, assign) NSInteger is_collect_true;

/**
 已点赞
 */
@property (nonatomic, assign) NSInteger click_praise_true;


/**
 作者名
 */
@property (nonatomic, copy) NSString *user_name;

/**
 作者头像
 */
@property (nonatomic, copy) NSString *user_head;

/**
 关注状态
 */
@property (nonatomic, assign) NSInteger follow_status;

/**
 作者id
 */
@property (nonatomic, assign) NSInteger fk_user_id;



@end
