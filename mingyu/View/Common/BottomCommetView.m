//
//  BottomCommetView.m
//  mingyu
//
//  Created by apple on 2018/4/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "BottomCommetView.h"

@implementation BottomCommetView

//- (void)setUser_head_image:(UIImageView *)user_head_image{
//
//
//}


- (void)awakeFromNib{
    
    [super awakeFromNib];
    

}


- (void)setHaveCollect:(BOOL)haveCollect{
    if (haveCollect) {
        [_collect_button setImage:ImageName(@"底部_已收藏") forState:0];
    } else {
        [_collect_button setImage:ImageName(@"底部_收藏") forState:0];
    }
}


- (IBAction)CommentButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentButtonClick)]) {
        [self.delegate commentButtonClick];
    } else if (self.CommentButtonBlock) {
        self.CommentButtonBlock();
    }
}


- (IBAction)shareButton:(id)sender {

    if (self.delegate && [self.delegate respondsToSelector:@selector(shareButtonClicked)]) {
        [self.delegate shareButtonClicked];
    }
}


- (IBAction)collectButton:(id)sender {
    if (![userDefault boolForKey:KUDhasLogin]) {
        CodeLoginViewController *VC = [KCommonStoyrboard instantiateViewControllerWithIdentifier:@"CodeLoginViewController"];
        [[self viewController] presentViewController:[[UINavigationController alloc] initWithRootViewController:VC] animated:YES completion:nil];
        return;
    }
    _collect_button.userInteractionEnabled = NO;
    //处理逻辑
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _collect_button.userInteractionEnabled = YES;
    });
    

    if (self.delegate && [self.delegate respondsToSelector:@selector(collectButtonClicked)]) {
        [self.delegate collectButtonClicked];
    }
}

- (IBAction)locationButton:(id)sender {
    if (self.LoctionButtonBlock) {
        self.LoctionButtonBlock();
    }
}


@end
