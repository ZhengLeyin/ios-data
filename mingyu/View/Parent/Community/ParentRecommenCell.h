//
//  ParentRecommenCell.h
//  mingyu
//
//  Created by apple on 2018/4/17.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsModel;


@interface ParentRecommenCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *user_head_Image;

@property (weak, nonatomic) IBOutlet UILabel *user_name_Lab;

@property (weak, nonatomic) IBOutlet UILabel *topic_title_Lab;

@property (weak, nonatomic) IBOutlet UIImageView *topic_head_Image;

@property (weak, nonatomic) IBOutlet UIButton *disLikeBtton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TitleLabConstraintRight;





@property (nonatomic, strong) NewsModel *newsmodel;

@property (nonatomic, copy) void (^dislikeblock) (NewsModel *model);

+ (instancetype)theParentRecommenCellWithTableView:(UITableView *)tableView;



/**
 搜索
 */
- (void)showSearch;


@end
