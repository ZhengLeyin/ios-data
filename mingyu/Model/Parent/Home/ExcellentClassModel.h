//
//  ExcellentClassModel.h
//  mingyu
//
//  Created by MingYu on 2018/11/19.
//  Copyright © 2018 TZWY. All rights reserved.
//

/** 精品课Model */
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExcellentClassModel : NSObject
/** 一级目录id */
@property (nonatomic, assign) NSInteger menu_id;

/** 获取和年龄相关的组 */
@property (nonatomic, assign) NSInteger age_group;

/** 一级目录名称 */
@property (nonatomic, copy) NSString *menu_name;

/** 证明和年龄有关? */
@property (nonatomic, assign) NSInteger is_ban;

#pragma mark 首页HeaderView 功能模块model
/** id */
@property (nonatomic, assign) NSInteger id;

/** icon_name */
@property (nonatomic, copy) NSString *icon_name;

/** icon_image */
@property (nonatomic, copy) NSString *icon_image;
@end

NS_ASSUME_NONNULL_END
