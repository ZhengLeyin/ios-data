//
//  VideoPlayerViewController.h
//  mingyu
//
//  Created by apple on 2018/3/30.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCDownloadItem.h"

//UIKIT_EXTERN NSNotificationName const VideoPlayerViewDidScrollNSNotification;
//UIKIT_EXTERN NSNotificationName const VideoPlayerViewRefreshStateNSNotification;
@interface VideoPlayerViewController : UIViewController

@property (nonatomic, strong) YCDownloadItem *playerItem;

@end
