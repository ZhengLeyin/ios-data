//
//  CVContentViewController.h
//  mingyu
//
//  Created by apple on 2018/6/9.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"

@interface CVContentViewController : UIViewController<JXPagerViewListViewDelegate>

/** type 0 为需要支付  1 不需要支付 */
@property (nonatomic, assign) NSUInteger  type;

@property (nonatomic, assign) NSUInteger courseId;

@property (nonatomic, copy) void (^selectVideoBlock)(VideoModel *model );



@end
