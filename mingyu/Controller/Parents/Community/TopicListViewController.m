//
//  TopicListViewController.m
//  mingyu
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TopicListViewController.h"


@interface TopicListViewController ()<UITableViewDelegate,UITableViewDataSource,YBPopupMenuDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *ArrayData;

@property (nonatomic, assign) NSInteger start;

@property (nonatomic, assign) BOOL lastPage;

@end

@implementation TopicListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self refreshTableview];

    //注册通知：
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshTableview) name:NSNotificationSetTopTopic object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshTableview) name:NSNotificationCancelTopTopic object:nil];

}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationSetTopTopic object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationCancelTopTopic object:nil];

}


//- (void)TopTopicSuccess:(NSNotification*)noti{
//    NewsModel *model = noti.userInfo[@"newsmodel"];
//    if (self.ArrayData && [self.ArrayData containsObject:model]) {
//        [self.ArrayData removeObject:model];
//        [self.tableView reloadData];
//    }
//}



//刷新
-(void)refreshTableview {
    _tableView.mj_header = [MYRefreshGifHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        if (self.ArrayData.count>0) {
            [_ArrayData removeAllObjects];
        }
        _start = 0;
        [_tableView.mj_footer resetNoMoreData];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MinRefreshTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [self getTopicList];  //加载cell数据
        });
    }];
    // 马上进入刷新状态
    [_tableView.mj_header beginRefreshing];
    
    //上拉刷新
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            if (_lastPage) {
                [_tableView.mj_footer endRefreshingWithNoMoreData];
                return ;
            }
            [self getTopicList];
            
        });
    }];
    footer.stateLabel.font = FontSize(12);
    //    footer.hidden = YES;
    _tableView.mj_footer = footer;
}


- (void)getTopicList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetTopicList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"circle_id":@(_circlemodel.circle_id),
                                 @"topic_type":_topic_type,
                                 @"start":@(_start),
                                 @"size":@"10"
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            _start += 10;
            NSArray *data = json[@"data"];
            if (data && data.count == 10) {
                _lastPage = NO;
            } else {
                _lastPage = YES;
            }
            for (NSDictionary *dic in data) {
                NewsModel *model = [NewsModel modelWithJSON:dic];
                [self.ArrayData addObject:model];
            }
            [self.tableView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}



#pragma tableView--delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.ArrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ParentCicleCell *cell = [ParentCicleCell theParentCicleCellwithtableView:tableView];
    if (self.ArrayData && self.ArrayData.count > 0) {
        cell.newsmodel = [self.ArrayData objectAtIndex:indexPath.row];
        cell.disLikeBtton.hidden = YES;
//        cell.dislikeblock = ^(NewsModel *model) {
//            ParentCicleCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            YBPopupMenu *popupMenu = [YBPopupMenu showRelyOnView:cell.disLikeBtton titles:@[@"不感兴趣"] icons:nil menuWidth:118 delegate:self];
//            popupMenu.fontSize =14;
//            popupMenu.textColor = [UIColor colorWithHexString:@"575757"];
//        };
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_ArrayData.count > indexPath.row) {
        ParentTopicViewController *VC = [KParentCommunityStoyboard instantiateViewControllerWithIdentifier:@"ParentTopicViewController"];
        NewsModel *model = [_ArrayData objectAtIndex:indexPath.row];
        VC.newsmodel = model;
        VC.hiddenCircle = YES;
        [self.navigationController pushViewController:VC animated:YES];
    }    
}


#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenuDidSelectedAtIndex:(NSInteger)index ybPopupMenu:(YBPopupMenu *)ybPopupMenu {
    
    ZPLog(@"dislike");
    
}

- (NSMutableArray *)ArrayData{
    if (_ArrayData == nil) {
        _ArrayData = [NSMutableArray array];
    }
    return _ArrayData;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.height = self.view.bounds.size.height-NaviH-44;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 0;
        _tableView.rowHeight = 130;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    }
    return _tableView;
}


- (UIScrollView *)scrollView{
    return self.tableView;
}
@end
