//
//  ChildCareBaseViewController.h
//  mingyu
//
//  Created by apple on 2018/7/11.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildCareBaseViewController : UIViewController

@property (nonatomic, strong) LabelListModel *labelModel;

@property (nonatomic, assign) NSInteger agesection;

@end
