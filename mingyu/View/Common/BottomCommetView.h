//
//  BottomCommetView.h
//  mingyu
//
//  Created by apple on 2018/4/21.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BottomCommentDelegate <NSObject>

@optional
/** 收藏 */
- (void)collectButtonClicked;

/** 分享 */
- (void)shareButtonClicked;

/** 评论 */
- (void)commentButtonClick;

@end


@interface BottomCommetView : UIView

@property (weak, nonatomic) IBOutlet UIButton *collect_button;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectWidth;

@property (nonatomic, assign) BOOL haveCollect;

@property (nonatomic, copy) void (^CommentButtonBlock) (void);

@property (nonatomic, copy) void (^LoctionButtonBlock) (void);

@property (nonatomic , weak) id <BottomCommentDelegate> delegate;



@end
