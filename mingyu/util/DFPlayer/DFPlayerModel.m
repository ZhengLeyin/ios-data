//
//  DFPlayerModel.m
//  DFPlayer
//
//  Created by HDF on 2017/7/18.
//  Copyright © 2017年 HDF. All rights reserved.
//

#import "DFPlayerModel.h"
#import "DFPlayerFileManager.h"

//@implementation DFPlayerModel
//
//@end

@implementation DFPlayerInfoModel

@end

@implementation DFPlayerPreviousAudioModel

- (NSDictionary *)infoDic{
    return [DFPlayerArchiverManager df_unarchiveInfoModelDictionary];
}

- (NSString *)audioUrlAbsoluteString{
    return [[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioUrl];
}
- (CGFloat)currentTime{
    return [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelCurrentTime] floatValue];
}
- (CGFloat)totalTime{
    return [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelTotalTime] floatValue];
}
- (CGFloat)progress{
    return [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelProgress] floatValue];
}
//- (UIImage *)audio_image{
//    return [[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioImage];
//}
- (NSURL *)audio_image_url{
    return [[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioImage];
}

- (NSUInteger)fk_parent_id{
    return [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelParentId] integerValue];
}
- (NSString *)fk_parent_title{
    return [[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelParentName];
}
- (NSUInteger)audio_id{
    return [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioId] integerValue];
}
- (NSString *)audio_title{
    return [[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioName];
}
- (NSInteger)audio_type{
    NSInteger type = [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelAudioType] integerValue];
    return type;
}
- (NSInteger)subject_id{
    NSInteger type = [[[self infoDic] objectForKey:DFPlayerCurrentAudioInfoModelSubjectId] integerValue];
    return type;
}


@end
