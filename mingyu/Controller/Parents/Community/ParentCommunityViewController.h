//
//  ParentCommunityViewController.h
//  TZWY
//
//  Created by apple on 2018/3/14.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SPPageMenu;

@interface ParentCommunityViewController : UIViewController

@property (nonatomic, strong) SPPageMenu *pageMenu;


@end
