//
//  ListenStoryCell.h
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BookListModel;

@interface ListenStoryCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *book_head_image;
@property (weak, nonatomic) IBOutlet UILabel *book_title_lab;
@property (weak, nonatomic) IBOutlet UILabel *content_abstract_lab;
@property (weak, nonatomic) IBOutlet UILabel *user_name_lab;
@property (weak, nonatomic) IBOutlet UIButton *play_count_button;
@property (weak, nonatomic) IBOutlet UIButton *praise_number_btton;
@property (weak, nonatomic) IBOutlet UILabel *original_price_lab;
@property (weak, nonatomic) IBOutlet UILabel *present_price_lab;
@property (weak, nonatomic) IBOutlet UILabel *max_gold_scale_lab;


@property (weak, nonatomic) IBOutlet UILabel *head_title_lab;

@property (nonatomic, copy) void (^MoreButtonBlock)(void);


@property (nonatomic, strong)  BookListModel *booklistmodel;




+ (instancetype)theCellWithTableView:(UITableView *)tableView;

+ (instancetype)theHeaderView;

@end
