//
//  GoldHeaderView.h
//  mingyu
//
//  Created by apple on 2018/5/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoldHeaderView : UIView


@property (weak, nonatomic) IBOutlet UILabel *goldLab;


/** 用户头像 */
@property (weak, nonatomic) IBOutlet UIImageView *head_iamge;

@property (weak, nonatomic) IBOutlet UIImageView *icon_image;


@end
