//
//  ParentTopicModel.h
//  mingyu
//
//  Created by apple on 2018/4/18.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParentTopicModel : NSObject


/**
 话题id
 */
@property (nonatomic, assign) NSInteger topic_id;

/**
 话题标题
 */
@property (nonatomic, copy) NSString *topic_title;

/**
 话题描述
 */
@property (nonatomic, copy) NSString *topic_abstract;

/**
 话题内容
 */
@property (nonatomic, copy) NSString *topic_content;

/**
 话题发布时间
 */
@property (nonatomic, copy) NSString *topic_time;

/**
 话题评论数
 */
@property (nonatomic, assign) NSInteger comment_number;

/**
 话题点赞数
 */
@property (nonatomic, assign) NSInteger praise_number;

/**
 话题发布人姓名
 */
@property (nonatomic, copy) NSString *user_name;

/**
 话题发布人头像
 */
@property (nonatomic, copy) NSString *user_head;

/**
 话题发布人id
 */
@property (nonatomic, assign) NSInteger user_id;

/**
 所属圈子
 */
@property (nonatomic, copy) NSString *circle_name;


/**
 话题图片
 */
@property (nonatomic, copy) NSString *topic_head;


/**
 置顶id
 */
@property (nonatomic, assign) NSInteger is_top_true;


@end
