//
//  TimeRecordCell.m
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "TimeRecordCell.h"
#import "BabyAlbumViewController.h"
#import "AddRecordViewController.h"
#import "UIView+CLSetRect.h"

static CGFloat cellHeight = 0;

@implementation TimeRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_paddingView setAllCorner:4];
    _paddingView.layer.borderWidth = 0.5;
    _paddingView.layer.borderColor = [UIColor colorWithHexString:@"EEEEEE"].CGColor;

    [_albumButton setBackgroundColor: [UIColor colorWithHexString:@"000000" alpha:0.2]];
}

+ (CGFloat)cellHeight {
    return cellHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (instancetype)theTimeRecordHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"TimeRecordCell" owner:nil options:nil][0];
}

+ (instancetype)theTimeRecordCellWithTableView:(UITableView *)tableView{
    static NSString *cellid = @"TimeRecordCell";
    TimeRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"TimeRecordCell" owner:nil options:nil][1];
    }
    return cell;
}

+ (instancetype)theTimeRecordFooterView{
    return [[NSBundle mainBundle] loadNibNamed:@"TimeRecordCell" owner:nil options:nil][2];
}

+ (instancetype)theTimeRecordCellSectionHeaderView{
    return [[NSBundle mainBundle] loadNibNamed:@"TimeRecordCell" owner:nil options:nil][3];
}



- (void)setDiarymodel:(DiaryModel *)diarymodel{
    _diarymodel = diarymodel;

    NSInteger Length = diarymodel.imageList.count;
    if (Length > 3) {
        Length = 3;
    }
    for (int i = 0; i < Length; i++) {
        DiaryPhotoModel *model =[DiaryPhotoModel modelWithJSON:diarymodel.imageList[i]];

        CGFloat W = (kScreenWidth-65-10*(Length-1))/Length;
        UIImageView *imageview = [[UIImageView alloc] init];
        
        imageview.contentMode = UIViewContentModeScaleAspectFill;
        imageview.clipsToBounds = YES;

        NSString *imageURL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.image_name];
        [imageview sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:ImageName(@"placeholderImage")];
        [_paddingView addSubview:imageview];
        
        [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.paddingView).offset(10);
            make.left.mas_equalTo(i*(W+10)+10);
            make.width.height.mas_equalTo(W);
        }];
    }
    
    cellHeight = 10;
    if (diarymodel.imageList.count == 0) {
        //        cellHeight += 20;
    } else if (diarymodel.imageList.count == 1){
        cellHeight += kScreenWidth-65;
    } else if (diarymodel.imageList.count == 2) {
        cellHeight += (kScreenWidth-75)/2;
    } else{
        cellHeight += (kScreenWidth-85)/3;
    }
    cellHeight += 10;

    
    if (diarymodel.diary_content.length > 0) {
        NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        // 行间距设置为10
        [paragraphStyle  setLineSpacing:5];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:diarymodel.diary_content];
        [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [diarymodel.diary_content length])];
        _contentLab.attributedText = attrString;
        _contentLab.lineBreakMode = NSLineBreakByTruncatingTail;

        cellHeight += [self hideLabelLayoutHeight:diarymodel.diary_content withTextFontSize:17 andWidth:kScreenWidth-65]+20;
        if (diarymodel.imageList.count > 0) {
            cellHeight += 10;
        }
    }
    cellHeight += 25;
    _diarymodel.height = cellHeight;
//    ZPLog(@"%f",cellHeight);
}


-(CGFloat )hideLabelLayoutHeight:(NSString *)content withTextFontSize:(CGFloat)FontSize andWidth:(CGFloat)width{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;  // 段落高度
    NSMutableAttributedString *attributes = [[NSMutableAttributedString alloc] initWithString:content];
    [attributes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize] range:NSMakeRange(0, content.length)];
    [attributes addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];
    CGSize attSize = [attributes boundingRectWithSize:CGSizeMake(width, 100) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return attSize.height;
}




- (IBAction)BabyAlbumButton:(id)sender {
    BabyAlbumViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"BabyAlbumViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}


- (IBAction)BeginRecordButton:(id)sender {
    if (![HttpRequest NetWorkIsOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TipNetWorkIsFiled message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    AddRecordViewController *VC = [KParentMineStoyboard instantiateViewControllerWithIdentifier:@"AddRecordViewController"];
    [[self viewController].navigationController pushViewController:VC animated:YES];
}

@end
