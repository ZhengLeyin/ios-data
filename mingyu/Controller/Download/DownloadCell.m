
//
//  DownloadCell.m
//  mingyu
//
//  Created by apple on 2018/4/13.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "DownloadCell.h"
#import "UIImageView+WebCache.h"
#import "YCDownloadManager.h"
#import "ZFLoadingView.h"

@implementation DownloadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self addview];
}

//- (void)addview{
//    _progressview =[[UIView alloc]init];
//    _progressview.backgroundColor = [UIColor blueColor];
//    _progressview.alpha = 0.5;
//    _progressview.frame = self.videoImage.frame;
//    [self.contentView addSubview:self.progressview];
//}

- (void)setDownloadinfo:(downloadInfo *)downloadinfo{
    _downloadinfo = downloadinfo;
    self.titleLab.text = downloadinfo.fileName;
    self.timeLab.text = downloadinfo.totletime;
    if ([DFPlayer shareInstance].currentAudioModel.audio_id == downloadinfo.audiomodel.audio_id && [DFPlayer shareInstance].state == DFPlayerStatePlaying) {
        self.titleLab.textColor = [UIColor colorWithHexString:@"00D3F8"];
    } else {
        self.titleLab.textColor = [UIColor colorWithHexString:@"2C2C2C"];
    }
    
    [self changeSizeLblDownloadedSize:downloadinfo.downloadedSize totalSize:downloadinfo.fileSize];
    [self setDownloadStatus:downloadinfo.downloadStatus];

}

- (void)setDownloadStatus:(YCDownloadStatus)status {
    NSString *statusStr = @"一键听_down_下载中";

    switch (status) {
        case YCDownloadStatusWaiting:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusDownloading:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusPaused:
            statusStr = @"一键听_down_下载中";
            break;
        case YCDownloadStatusFinished:
            _progressview.alpha = 0;
            statusStr = @"一键听_down_已下载";
            break;
        case YCDownloadStatusFailed:
            statusStr = @"一键听_down_下载中";
            break;
            
        default:
            break;
    }
//    self.stateImage.image = ImageName(statusStr);
        [self.stateButton setImage:ImageName(statusStr) forState:0];

//    ZFLoadingView *activity = [[ZFLoadingView alloc] initWithFrame:self.stateButton.bounds];
//    activity.lineColor = [UIColor redColor];
//    [activity startAnimating];
//    [self.stateButton addSubview:activity];
}


- (IBAction)stateButton:(id)sender {
    YCDownloadStatus status = [YCDownloadManager downloasStatusWithId:_downloadinfo.fileId];
    if (status == YCDownloadStatusPaused) {
        [YCDownloadManager resumeDownloadWithItem:_downloadinfo];
    }
}


- (void)changeSizeLblDownloadedSize:(int64_t)downloadedSize totalSize:(int64_t)totalSize {
    
//    self.sizeLbl.text = [NSString stringWithFormat:@"%@ / %@",[YCDownloadManager fileSizeStringFromBytes:downloadedSize], [YCDownloadManager fileSizeStringFromBytes:totalSize]];
//
//    float progress = 0;
//    if (totalSize != 0) {
//        progress = (float)downloadedSize / totalSize;
//    }
//    self.progressView.progress = progress;
}


- (void)downloadItemStatusChanged:(YCDownloadItem *)item {
    [self setDownloadStatus:item.downloadStatus];
}

- (void)downloadItem:(YCDownloadItem *)item downloadedSize:(int64_t)downloadedSize totalSize:(int64_t)totalSize {
    
    [self changeSizeLblDownloadedSize:downloadedSize totalSize:totalSize];
}

- (void)downloadItem:(YCDownloadItem *)item speed:(NSUInteger)speed speedDesc:(NSString *)speedDesc {
    ZPLog(@"%zd ----- %@", speed, speedDesc);
}





-(void)layoutSubviews {
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (self.selected) {
                        image.image=[UIImage imageNamed:@"xuanzhe"];
                    }
                    else
                    {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
    
    [super layoutSubviews];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *view in control.subviews)
            {
                if ([view isKindOfClass: [UIImageView class]]) {
                    UIImageView *image=(UIImageView *)view;
                    if (!self.selected) {
                        image.image=[UIImage imageNamed:@"weixuanz"];
                    }
                }
            }
        }
    }
}

@end
