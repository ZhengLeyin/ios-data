//
//  ParentNewHomeHeaderView.h
//  mingyu
//
//  Created by apple on 2018/9/3.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDCycleScrollView.h>
#import "TZImagePickerController.h"
#import <Photos/Photos.h>

@interface ParentNewHomeHeaderView : UIView<SDCycleScrollViewDelegate,TZImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *bannerView;

@property (weak, nonatomic) IBOutlet UILabel *dayNum_lab;

@property (weak, nonatomic) IBOutlet UILabel *date_lab;

@property (weak, nonatomic) IBOutlet UILabel *babyLength_lab;

@property (weak, nonatomic) IBOutlet UILabel *babyWeight_lab;

@property (weak, nonatomic) IBOutlet UILabel *content_lab;

@property (weak, nonatomic) IBOutlet UIButton *babyicon_button;


@property (weak, nonatomic) IBOutlet UIImageView *subjectImage1;
@property (weak, nonatomic) IBOutlet UIImageView *subjectImage2;

@property (weak, nonatomic) IBOutlet UIImageView *historyImage1;
@property (weak, nonatomic) IBOutlet UIImageView *historyImage2;

@property (weak, nonatomic) IBOutlet UIImageView *playstateImage1;
@property (weak, nonatomic) IBOutlet UIImageView *playstateImage2;

@property (weak, nonatomic) IBOutlet UILabel *subjecttitle_lab1;
@property (weak, nonatomic) IBOutlet UILabel *subjecttitle_lab2;

@property (weak, nonatomic) IBOutlet UILabel *listen_lab1;
@property (weak, nonatomic) IBOutlet UILabel *listen_lab2;


@property (nonatomic, strong) NSMutableArray *subjcArray;

@property (nonatomic, strong) TZImagePickerController *imagePickerVc;

@property (nonatomic, strong) NSMutableArray *BabyRecordArray;


- (void)getAdvertisement;



@end
