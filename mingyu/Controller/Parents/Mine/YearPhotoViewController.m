//
//  YearPhotoViewController.m
//  mingyu
//
//  Created by apple on 2018/8/4.
//  Copyright © 2018年 TZWY. All rights reserved.
//

#import "YearPhotoViewController.h"
#import "YearPhotoCollectionCell.h"
#import "HZPhotoBrowser.h"
#import "DiaryPhotoModel.h"
#import "DiaryModel.h"

#define DeleteButtonH 46

@interface YearPhotoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *editorButton;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *arrayData;

@property (nonatomic, strong) NSMutableArray *headerArray;

@property (nonatomic, strong) UIButton *deleteButton;

@property (nonatomic, strong) NSMutableArray *chooseImageArray;

@property (nonatomic, assign) NSInteger chooseImage;

@end

@implementation YearPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh) name:NSNotificationDeletePhotoRecord object:nil];

    self.title = [NSString stringWithFormat:@"%@年%@月",_photomodel.year,_photomodel.month];
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.deleteButton];

    [self getMonthPhotoList];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSNotificationDeletePhotoRecord object:nil];

    
}


- (void)refresh{
    [self.headerArray removeAllObjects];
    [self.arrayData removeAllObjects];
    [self getMonthPhotoList];
}


- (void)getMonthPhotoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@?",KURL,KGetMonthPhotoList];
    NSDictionary *parameters = @{
                                 @"user_id":[NSString stringWithFormat:@"%@",USER_ID],
                                 @"year":_photomodel.year,
                                 @"month":_photomodel.month
                                 };
    URL = [NSString connectUrl:parameters url:URL];
    
    [HttpRequest getWithURLString:URL parameters:nil viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            NSArray *data = json[@"data"];
            for (NSDictionary *dic in data) {
                DiaryModel *model = [DiaryModel modelWithJSON:dic];
                [self.headerArray addObject:model];
                
                NSMutableArray *array = [NSMutableArray array];
                for (NSDictionary *dict in model.messageList) {
                    DiaryPhotoModel *model = [DiaryPhotoModel modelWithJSON:dict];
                    [array addObject:model];
                }
                [self.arrayData addObject:array];
            }
            [self.collectionView reloadData];
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.headerArray && self.headerArray.count > 0) {
        return self.headerArray.count;
    }
    return 0;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.arrayData && self.arrayData.count > section) {
        return [self.arrayData[section] count];
    }
    return 0;
//    return [self.arrayData[section] count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //自定义cell
    YearPhotoCollectionCell *cell=nil;
    
    if (cell==nil) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YearPhotoCollectionCell" forIndexPath:indexPath];
    }
    
    if (_collectionView.allowsMultipleSelection) {
        cell.selectImage.hidden = NO;
        cell.selectImage.image = ImageName(@"weixuanz");
    } else{
        cell.selectImage.hidden = YES;
    }
    if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
        DiaryPhotoModel *model = self.arrayData[indexPath.section][indexPath.row];
        NSString *URL = [NSString stringWithFormat:@"%@%@",KKaptcha,model.image_name];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:ImageName(@"placeholderImage")];
    }
    
    return cell;

}


//cell的header与footer的显示内容
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *reusableHeaderView = nil;
         if (reusableHeaderView==nil) {
             reusableHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader  withReuseIdentifier:@"reusableHeaderView" forIndexPath:indexPath];
            reusableHeaderView.backgroundColor = [UIColor whiteColor];
             //这部分一定要这样写 ，否则会重影，不然就自定义headview
            UILabel *label = (UILabel *)[reusableHeaderView viewWithTag:100];
            if (!label) {
                label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width, 48)];
                label.textColor = [UIColor colorWithHexString:@"2C2C2C"];
                label.font = FontSize(14);
                label.tag = 100;
                [reusableHeaderView addSubview:label];
            }
             if (self.headerArray && self.headerArray.count > indexPath.section) {
                 DiaryModel *model = self.headerArray[indexPath.section];
                 label.text = [NSString stringWithFormat:@"%@月%@日",model.month,model.day];
             }
        }
        return reusableHeaderView;
    }
    return nil;
}



//设置允许多选
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ZPLog(@"选中%ld按钮",indexPath.row);
    if (_collectionView.allowsMultipleSelection) {
        YearPhotoCollectionCell *cell = (YearPhotoCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.selectImage.image = ImageName(@"xuanzhe");
        _chooseImage++;
        [_deleteButton setTitle:[NSString stringWithFormat:@"(共%ld张)删除",_chooseImage] forState:0];
        if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
            DiaryPhotoModel *model = self.arrayData[indexPath.section][indexPath.row];
            [self.chooseImageArray addObject:model];
        }

    } else {
        HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = indexPath.row;
        NSArray *array = self.arrayData[indexPath.section];
        browser.imageArray = [NSMutableArray arrayWithArray:array];

        [browser show];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    ZPLog(@"取消选中%ld按钮",indexPath.row);
    if (_collectionView.allowsMultipleSelection) {
        YearPhotoCollectionCell *cell = (YearPhotoCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.selectImage.image = ImageName(@"weixuanz");
        _chooseImage--;
        [_deleteButton setTitle:[NSString stringWithFormat:@"(共%ld张)删除",_chooseImage] forState:0];
        if (self.arrayData && self.arrayData.count > indexPath.section && [self.arrayData[indexPath.section] count] > indexPath.row) {
            DiaryPhotoModel *model = self.arrayData[indexPath.section][indexPath.row];
            [self.chooseImageArray removeObject:model];
        }
    } else {
        
    }
}


- (void)deletePhoto{
    if (self.chooseImageArray.count == 0) {
        [UIView ShowInfo:@"请选择照片" Inview:self.view];
        return;
    }
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"确定要删除照片吗？"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             //响应事件
                                                             
                                                         }];
//    [cancelAction setValue:[UIColor lightGrayColor] forKey:@"titleTextColor"];
    
    UIAlertAction* confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              //响应事件
                                                              [self delPhotoList];
                                                          }];
//    [confirmAction setValue:[UIColor colorWithHexString:@"50D0F4"] forKey:@"titleTextColor"];
    
    [alert addAction:cancelAction];
    [alert addAction:confirmAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

//删除图片
- (void)delPhotoList{
    NSString *URL = [NSString stringWithFormat:@"%@%@",KURL,KDelPhotoList];
    NSMutableArray *idList = [NSMutableArray array];
    for (DiaryPhotoModel *model in _chooseImageArray) {
        [idList addObject:@(model.photo_id)];
    }
    NSString *string = [idList componentsJoinedByString:@","];
    NSDictionary *parameters = @{
                                 @"idList":[NSString stringWithFormat:@"%@",string]
                                 };
    
    [HttpRequest postWithURLString:URL parameters:parameters viewcontroller:self success:^(id responseObject) {
        NSDictionary *json = responseObject;
        ZPLog(@"%@\n%@",json,json[@"message"]);
        BOOL success = [json[@"success"] boolValue];
        if (success) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationDeletePhotoRecord object:nil userInfo:nil];

            [UIView ShowInfo:@"删除成功" Inview:self.view];

            [self EditorButton:_editorButton];
            
        } else {
            [UIView ShowInfo:json[@"message"] Inview:self.view];
        }
    } failure:^(NSError *error) {
//        [UIView ShowInfo:TipWrongMessage Inview:self.view];
    }];
}


- (UICollectionView *)collectionView{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.itemSize = CGSizeMake((kScreenWidth-6)/4, (kScreenWidth-6)/4);
        layout.minimumLineSpacing = 2;
        layout.minimumInteritemSpacing = 2;
        layout.headerReferenceSize = CGSizeMake(kScreenWidth, 48);
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[YearPhotoCollectionCell class] forCellWithReuseIdentifier:@"YearPhotoCollectionCell"];
        [_collectionView registerClass:[UICollectionReusableView class]  forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableHeaderView"];
        
    }
    return _collectionView;
}



- (UIButton *)deleteButton{
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.frame = CGRectMake(0, kScreenHeight-DeleteButtonH-NaviH-effectViewH, kScreenWidth, DeleteButtonH);
        [_deleteButton setTitleColor:[UIColor colorWithHexString:@"5BD3F5"] forState:0];
        _deleteButton.titleLabel.font = FontSize(15);
        [_deleteButton addTarget:self action:@selector(deletePhoto) forControlEvents:UIControlEventTouchUpInside];
        _deleteButton.hidden = YES;
        _deleteButton.layer.borderWidth = 0.5;
        _deleteButton.layer.borderColor = [UIColor colorWithHexString:@"E2E2E2"].CGColor;
        _deleteButton.layer.masksToBounds = YES;
    }
    return _deleteButton;
}


- (NSMutableArray *)chooseImageArray{
    if (!_chooseImageArray) {
        _chooseImageArray = [NSMutableArray array];
    }
    return _chooseImageArray;
}

- (NSMutableArray *)arrayData{
    if (!_arrayData) {
        _arrayData = [NSMutableArray array];
    }
    return _arrayData;
}

- (NSMutableArray *)headerArray{
    if (!_headerArray) {
        _headerArray = [NSMutableArray array];
    }
    return _headerArray;
}



//返回
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


//编辑
- (IBAction)EditorButton:(UIButton *)sender {
    if ([sender.titleLabel.text  isEqual: @"编辑"]) {
        _collectionView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH-DeleteButtonH);
        [sender setTitle:@"取消" forState:0];
        _collectionView.allowsMultipleSelection = YES;
        [_collectionView reloadData];
        _deleteButton.hidden = NO;
        [_deleteButton setTitle:@"(共0张)删除" forState:0];
    } else {
        _collectionView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-NaviH-effectViewH);
        [sender setTitle:@"编辑" forState:0];
        _collectionView.allowsMultipleSelection = NO;
        [_collectionView reloadData];
        _deleteButton.hidden = YES;
        _chooseImage = 0;
        [self.chooseImageArray removeAllObjects];
    }
}




@end
